﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.SalarySheet
{
    [Trait("Area", "Hr")]
    [Trait("Service", "SalarySheet")]
    public class SalarySheetLoadTest:SalarySheetBaseTest
    {
        #region LoadTeamMemberForSalarySheet

        #region basic Test

        [Fact]
        public void Should_Return_LoadTeamMemberForSalarySheet_InvalidException_For_FormModel()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1),
                new DateTime(2016, 12, 1));
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadTeamMemberForSalarySheet(userMenu, null));
        }

        [Fact]
        public void Should_Retutn_LoadTeamMemberForSalarySheet_Successfully()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, startDate, org, true, true);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            //_holidaySettingFactory.Create().WithHolidayRepeatationType((int)HolidayRepeatationType.Once).WithHolidayType((int)HolidayWorkApprovalStatus.Half)
            //  .WithDateFromAndDateTo(holyDayDate, holyDayDate).WithOrganization(org).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = startDate.Month;
            Assert.NotEqual(0, _salarySheetService.LoadTeamMemberForSalarySheet(userMenu, formModel).Count());
        }

        [Fact]
        public void Should_Retutn_LoadTeamMemberForSalarySheet_InvalidException_For_INvalidYear()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, startDate, org, true, true);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = 0;
            formModel.SalaryMonth = startDate.Month;
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadTeamMemberForSalarySheet(userMenu, formModel));
        }

        [Fact]
        public void Should_Retutn_LoadTeamMemberForSalarySheet_InvalidException_For_INvalidMonth()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, startDate, org, true, true);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            //_holidaySettingFactory.Create().WithHolidayRepeatationType((int)HolidayRepeatationType.Once).WithHolidayType((int)HolidayWorkApprovalStatus.Half)
            //  .WithDateFromAndDateTo(holyDayDate, holyDayDate).WithOrganization(org).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = 13;
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadTeamMemberForSalarySheet(userMenu, formModel));
        }

        [Fact]
        public void Should_Retutn_LoadTeamMemberForSalarySheet_InvalidException_For_OrganizationType()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, startDate, org, true, true);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = (OrganizationType)3;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = startDate.Month;
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadTeamMemberForSalarySheet(userMenu, formModel));
        }

        [Fact]
        public void Should_Retutn_LoadTeamMemberForSalarySheet_InvalidException_For_Invalid_Organization()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, startDate, org, true, true);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            //_holidaySettingFactory.Create().WithHolidayRepeatationType((int)HolidayRepeatationType.Once).WithHolidayType((int)HolidayWorkApprovalStatus.Half)
            //  .WithDateFromAndDateTo(holyDayDate, holyDayDate).WithOrganization(org).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = 0,
                BranchId = branch.Id,
                CampusId = campus.Id,
                DepartmentId = dept.Id,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadTeamMemberForSalarySheet(userMenu, formModel));
        }

        [Fact]
        public void Should_Retutn_LoadTeamMemberForSalarySheet_InvalidException_For_Invalid_branch()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, startDate, org, true, true);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));
            
            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = org.Id,
                BranchId = -1,
                CampusId = campus.Id,
                DepartmentId = dept.Id,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadTeamMemberForSalarySheet(userMenu, formModel));
        }

        [Fact]
        public void Should_Retutn_LoadTeamMemberForSalarySheet_InvalidException_For_Invalid_Campus()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, startDate, org, true, true);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            //_holidaySettingFactory.Create().WithHolidayRepeatationType((int)HolidayRepeatationType.Once).WithHolidayType((int)HolidayWorkApprovalStatus.Half)
            //  .WithDateFromAndDateTo(holyDayDate, holyDayDate).WithOrganization(org).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = org.Id,
                BranchId = branch.Id,
                CampusId = -1,
                DepartmentId = dept.Id,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Equal(0, _salarySheetService.LoadTeamMemberForSalarySheet(userMenu, formModel).Count());
        }

        [Fact]
        public void Should_Retutn_LoadTeamMemberForSalarySheet_InvalidException_For_Invalid_Department()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, startDate, org, true, true);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));
            
            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = org.Id,
                BranchId = branch.Id,
                CampusId = campus.Id,
                DepartmentId = -1,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Equal(0, _salarySheetService.LoadTeamMemberForSalarySheet(userMenu, formModel).Count());
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_LoadTeamMemberForSalarySheet_InvalidException_For_Usermenu()
        {
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadTeamMemberForSalarySheet(null, new SalarySheetFormViewModel()));
        }

        #endregion
        
        #endregion
        
        #region LoadSalarySheetList
        
        #region Basic Test

        [Fact]
        public void Should_Return_LoadSalarySheetList_InvalidDataException_For_FormModel()
        {
            var tm = PersistTeamMember(new DateTime(2016,12,1),new DateTime(2016,12,1));
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadSalarySheetList(userMenu, null));
        }

        [Fact]
        public void Should_Return_LoadSalarySheetList_Successfully()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));
            
            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = startDate.Month;

            Assert.NotNull(_salarySheetService.LoadSalarySheetList(userMenu,formModel));
        }
        
        [Fact]
        public void Should_Retutn_LoadSalarySheetList_InvalidException_For_INvalidYear()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = 0;
            formModel.SalaryMonth = startDate.Month;
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadSalarySheetList(userMenu, formModel));
        }

        [Fact]
        public void Should_Retutn_LoadSalarySheetList_InvalidException_For_INvalidMonth()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = 13;
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadTeamMemberForSalarySheet(userMenu, formModel));
        }

        [Fact]
        public void Should_Retutn_LoadSalarySheetList_InvalidException_For_OrganizationType()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = (OrganizationType)3;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = startDate.Month;
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadSalarySheetList(userMenu, formModel));
        }

        [Fact]
        public void Should_Retutn_LoadSalarySheetList_InvalidException_For_Invalid_Organization()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = 0,
                BranchId = branch.Id,
                CampusId = campus.Id,
                DepartmentId = dept.Id,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadSalarySheetList(userMenu, formModel));
        }

        [Fact]
        public void Should_Retutn_LoadSalarySheetList_InvalidException_For_Invalid_branch()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = org.Id,
                BranchId = -1,
                CampusId = campus.Id,
                DepartmentId = dept.Id,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.LoadSalarySheetList(userMenu, formModel));
        }

        [Fact]
        public void Should_Retutn_LoadSalarySheetList_InvalidException_For_Invalid_Campus()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = org.Id,
                BranchId = branch.Id,
                CampusId = -1,
                DepartmentId = dept.Id,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Equal(0, _salarySheetService.LoadSalarySheetList(userMenu, formModel).Count());
        }

        [Fact]
        public void Should_Retutn_LoadSalarySheetList_InvalidException_For_Invalid_Department()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = org.Id,
                BranchId = branch.Id,
                CampusId = campus.Id,
                DepartmentId = -1,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Equal(0, _salarySheetService.LoadSalarySheetList(userMenu, formModel).Count());
        }

        #endregion

        #region Business logic Test

        [Fact]
        public void Should_Return_LoadSalarySheetList_InvalidDataException_For_UserMenu()
        {
            Assert.Throws<InvalidDataException>(() => _salarySheetService.LoadSalarySheetList(null, new SalarySheetFormViewModel()));
        }
        
        #endregion

        #endregion

        #region GetSalarySheetForFirstMonthTeamMember

        #region Basic Test

        [Fact]
        public void Should_Return_GetSalarySheetForFirstMonthTeamMember_Successfully()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org, true, true);
            var employmentHistory = _employmentHistoryService.GetTeamMemberFirstMonth(tm.Id, startDate.Month, startDate.Year);
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, new DateTime(2017, 2, 1))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = startDate.Month;

            Assert.NotNull(_salarySheetService.GetSalarySheetForFirstMonthTeamMember(userMenu, formModel, employmentHistory));
        }

        [Fact]
        public void Should_Return_GetSalarySheetForFirstMonthTeamMember_InvalidException_For_EmptyForm()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org, true, true);
            var employmentHistory = _employmentHistoryService.GetTeamMemberFirstMonth(tm.Id, startDate.Month, startDate.Year);
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, new DateTime(2017, 2, 1))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);

            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.GetSalarySheetForFirstMonthTeamMember(userMenu, null, employmentHistory));
        }

        [Fact]
        public void Should_Retutn_GetSalarySheetForFirstMonthTeamMember_InvalidException_For_INvalidYear()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org, true, true);
            var employmentHistory = _employmentHistoryService.GetTeamMemberFirstMonth(tm.Id, startDate.Month, startDate.Year);
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, new DateTime(2017, 2, 1))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = 0;
            formModel.SalaryMonth = startDate.Month;
            Assert.Throws<InvalidDataException>(() => _salarySheetService.GetSalarySheetForFirstMonthTeamMember(userMenu, formModel, employmentHistory));
        }

        [Fact]
        public void Should_Retutn_GetSalarySheetForFirstMonthTeamMember_InvalidException_For_INvalidMonth()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = 13;
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.GetSalarySheetForFirstMonthTeamMember(userMenu, formModel, employmentHistory));
        }

        [Fact]
        public void Should_Retutn_GetSalarySheetForFirstMonthTeamMember_InvalidException_For_OrganizationType()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = (OrganizationType)3;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = startDate.Month;
            Assert.Throws<InvalidDataException>(() => _salarySheetService.GetSalarySheetForFirstMonthTeamMember(userMenu, formModel, employmentHistory));
        }

        [Fact]
        public void Should_Retutn_GetSalarySheetForFirstMonthTeamMember_InvalidException_For_Invalid_Organization()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = 0,
                BranchId = branch.Id,
                CampusId = campus.Id,
                DepartmentId = dept.Id,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Throws<InvalidDataException>(() => _salarySheetService.GetSalarySheetForFirstMonthTeamMember(userMenu, formModel, employmentHistory));
        }

        [Fact]
        public void Should_Retutn_GetSalarySheetForFirstMonthTeamMember_InvalidException_For_Invalid_branch()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = org.Id,
                BranchId = -1,
                CampusId = campus.Id,
                DepartmentId = dept.Id,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.GetSalarySheetForFirstMonthTeamMember(userMenu, formModel, employmentHistory));
        }

        [Fact]
        public void Should_Retutn_GetSalarySheetForFirstMonthTeamMember_InvalidException_For_Invalid_Campus()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = org.Id,
                BranchId = branch.Id,
                CampusId = -1,
                DepartmentId = dept.Id,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Null(_salarySheetService.GetSalarySheetForFirstMonthTeamMember(userMenu, formModel, employmentHistory));
        }

        [Fact]
        public void Should_Retutn_GetSalarySheetForFirstMonthTeamMember_InvalidException_For_Invalid_Department()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel
            {
                OrganizationType = OrganizationType.JobOrganization,
                OrganizationId = org.Id,
                BranchId = branch.Id,
                CampusId = campus.Id,
                DepartmentId = -1,
                SalaryYear = startDate.Year,
                SalaryMonth = startDate.Month
            };
            Assert.Null(_salarySheetService.GetSalarySheetForFirstMonthTeamMember(userMenu, formModel, employmentHistory));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_GetSalarySheetForFirstMonthTeamMember_InvalidException_For_EmploymentHistory()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org, true, true);
            var employmentHistory = _employmentHistoryService.GetTeamMemberFirstMonth(tm.Id, startDate.Month, startDate.Year);
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, new DateTime(2017, 2, 1))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = startDate.Month;

            Assert.Throws<InvalidDataException>(
                () => _salarySheetService.GetSalarySheetForFirstMonthTeamMember(userMenu, formModel, null));
        }

        [Fact]
        public void Should_Return_GetSalarySheetForFirstMonthTeamMember_InvalidException_For_teamMember()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org, true, true);
            var employmentHistory = _employmentHistoryService.GetTeamMemberFirstMonth(tm.Id, startDate.Month, startDate.Year);
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, new DateTime(2017, 2, 1))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = startDate.Month;

            employmentHistory.TeamMember = null;

            Assert.Throws<InvalidDataException>(() => _salarySheetService.GetSalarySheetForFirstMonthTeamMember(userMenu, formModel, employmentHistory));
        }

        [Fact]
        public void Should_Return_GetSalarySheetForFirstMonthTeamMember_InvalidException_For_userMenu()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(startDate, endDate, org, true, true);
            var employmentHistory = _employmentHistoryService.GetTeamMemberFirstMonth(tm.Id, startDate.Month, startDate.Year);
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, new DateTime(2017, 2, 1))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, startDate)
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            var formModel = new SalarySheetFormViewModel();
            formModel.OrganizationType = OrganizationType.JobOrganization;
            formModel.OrganizationId = org.Id;
            formModel.BranchId = branch.Id;
            formModel.CampusId = campus.Id;
            formModel.DepartmentId = dept.Id;
            formModel.SalaryYear = startDate.Year;
            formModel.SalaryMonth = startDate.Month;

            Assert.Throws<InvalidDataException>(() => _salarySheetService.GetSalarySheetForFirstMonthTeamMember(null, formModel, employmentHistory));
        }

        #endregion
        
        #endregion
    }
}