﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Test.AdminisTration.ProgramTest;
using Xunit;

namespace UdvashERP.Services.Test.Administration.ProgramTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "ProgramBranchSession")]
    public class AssignProgramTest : ProgramBaseTest
    {
        #region Basic Test
        [Fact]
        public void Should_Assign_Successfully()
        {
            Organization organization = organizationFactory.Create().Persist().Object;
            //Create New Program
            Program program =
                _programFactory.Create().WithOrganization(organization).WithNameAndRank(_programName, 1).WithProperties(_programName, _programFactory.GetRandomNumber(), _paidProgram)
                    .Persist().Object;

            //Create New Session
            Session sessionObj = _sessionFactory.Create().Persist().Object; 

            //Create New Branches and generate branchArray
            IList<Organization> organizations = new List<Organization>();
            organizations.Add(program.Organization);
            var branchFactory = _branchFactory.CreateMore(5, organizations).Persist();
            IList<Branch>  branchList = branchFactory.GetLastCreatedObjectList();
            var branchIdArray = branchList.Select(x => x.Id).ToArray();

            //Generate selected Student Exam List
            var studentExamList = _studentExamService.LoadBoardExam(true);
            int?[] selectedStudentExamList = studentExamList.Where(x => x.Name.Contains("SC")).Select(y =>(int?)Convert.ToInt32( y.Id)).ToArray();

            //generate selected Admission Type List
            var selectedAdmissionTypeList = new int?[] { 1,2 };

            List<long> nonDeletableBranchIds;
            List<ResponseMessage> messageList = _programBranchSessionService.AssignProgram(program.Organization.Id, program.Id, sessionObj.Id, 
                branchIdArray, selectedAdmissionTypeList, selectedStudentExamList, out nonDeletableBranchIds);

         
            Assert.NotEmpty(messageList.Where(x=>x.SuccessMessage!=null));

            CleanUpProgramStudentExamSession(false);
            CleanUpProgramBranchSession(false);
        }

        [Fact]
        public void Should_Assign_And_Reassign_Successfully()
        {
            Organization organization = organizationFactory.Create().Persist().Object;
            //Create New Program
            Program program =
                _programFactory.Create().WithOrganization(organization).WithNameAndRank(_programName, 1).WithProperties(_programName, _programFactory.GetRandomNumber(), _paidProgram)
                    .Persist().Object;

            //Create New Session
            Session sessionObj = _sessionFactory.Create().Persist().Object;

            //Create New Branches and generate branchArray
            IList<Organization> organizations = new List<Organization>();
            organizations.Add(program.Organization);
            var branchFactory = _branchFactory.CreateMore(5, organizations).Persist();
            IList<Branch> branchList = branchFactory.GetLastCreatedObjectList();
            var branchIdArray = branchList.Select(x => x.Id).ToArray();

            //Generate selected Student Exam List
            var studentExamList = _studentExamService.LoadBoardExam(true);
            int?[] selectedStudentExamList = studentExamList.Where(x => x.Name.Contains("SC")).Select(y => (int?)Convert.ToInt32(y.Id)).ToArray();

            //generate selected Admission Type List
            var selectedAdmissionTypeList = new int?[] { 1, 2 };

            //Assign Program
            List<long> nonDeletableBranchIds;
            List<ResponseMessage> messageListFirst = _programBranchSessionService.AssignProgram(program.Organization.Id, program.Id, sessionObj.Id,
                branchIdArray, selectedAdmissionTypeList, selectedStudentExamList, out nonDeletableBranchIds);


            //remove last 2 branch
            var branchIdArraySecond = branchIdArray.Take(branchIdArray.Count() - 2).ToArray();

            //remove last Student Exam
            var selectedStudentExamListSecond = selectedStudentExamList.Take(selectedStudentExamList.Count() - 1).ToArray();


            List<long> nonDeletableBranchIdsSecond; 
            List<ResponseMessage> messageListSecond = _programBranchSessionService.AssignProgram(program.Organization.Id, program.Id, sessionObj.Id,
                branchIdArraySecond, selectedAdmissionTypeList, selectedStudentExamListSecond, out nonDeletableBranchIdsSecond);

            Assert.NotEmpty(messageListSecond.Where(x => x.SuccessMessage != null));
            Assert.NotEmpty(messageListSecond.Where(x => x.SuccessMessage != null && x.SuccessMessage.Contains("unassigned sucessfully")));


            CleanUpProgramStudentExamSession(false);
            CleanUpProgramBranchSession(false);
        }

        [Fact(Skip = "Need to sudent create first")]
        public void Should_Check_dependency()  
        {
            //TODO: Need to sudent create first
           // Assert.True(true);
        }

        #endregion
    }
}
