﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using NHibernate;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class LeaveApprovalMentorController : Controller
    {
        // GET: Hr/MentorLeave
        private ILog _logger = LogManager.GetLogger("HrArea");
        private readonly ILeaveService _hrLeaveService;
        private ISession _session;
        private readonly ILeaveApplicationService _hrLeaveApplicationService;
        private readonly ITeamMemberService _hrMemberService;
        private readonly IMentorHistoryService _mentorHistoryService;
        private readonly ICommonHelper _commonHelper;
        public LeaveApprovalMentorController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _hrLeaveService = new LeaveService(_session);
            _hrLeaveApplicationService = new LeaveApplicationService(_session);
            _hrMemberService = new TeamMemberService(_session);
            _mentorHistoryService = new MentorHistoryService(_session);
            _commonHelper = new CommonHelper();
        }

        public ActionResult Index()
        {
            return RedirectToAction("LeaveApprovedByMentor");
        }

        #region Leave approval Mentor

        public ActionResult LeaveApprovedByMentor()
        {
            IList<LeaveApplicationViewModel> pendingLeaveApplicationViewModelList = new List<LeaveApplicationViewModel>();
            try
            {
                TeamMember currentMember = _hrMemberService.GetCurrentMember();
                if (currentMember == null)
                    throw new InvalidDataException("This Member has no Team Member!");
                IList<LeaveApplicationViewModel> teampPendingLeaveApplicationViewModelList = _hrLeaveApplicationService.LoadMentorLeave(currentMember.Id, null ,LeaveStatus.Pending).ToList();
                if (teampPendingLeaveApplicationViewModelList.Any())
                {
                    foreach (var teampPendingLeaveApplicationViewMode in teampPendingLeaveApplicationViewModelList)
                    {
                        var member = _hrMemberService.GetByPin(Convert.ToInt32(teampPendingLeaveApplicationViewMode.TeamMemberPin));
                        var leaveTypesList = _hrMemberService.LoadMembersPendingLeave(member.Id);
                        var leaveTypeDtoLists = new List<LeaveTypeDtoList>();
                        foreach (var leaveTypeDtoObj in leaveTypesList)
                        {
                            if (leaveTypeDtoObj.LeaveId == Convert.ToInt64(teampPendingLeaveApplicationViewMode.LeaveId))
                            {
                                leaveTypeDtoObj.IsSelected = true;
                                leaveTypeDtoLists.Add(leaveTypeDtoObj);
                            }
                            else
                            {
                                leaveTypeDtoLists.Add(leaveTypeDtoObj);
                            }
                        }
                        teampPendingLeaveApplicationViewMode.Leaves = leaveTypeDtoLists;
                        pendingLeaveApplicationViewModelList.Add(teampPendingLeaveApplicationViewMode);
                    }
                }

            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
            return View(pendingLeaveApplicationViewModelList);
        }

        [HttpPost]
        public JsonResult MentorLeaveApprove(List<LeaveApplicationViewModel> leaveApplicationViewModelList)
        {
            string message = "Error";
            bool successState = false;
            try
            {
                if (leaveApplicationViewModelList.Any())
                {
                    TeamMember mentor = _hrMemberService.GetCurrentMember();
                    List<LeaveApplication> leaveApplicationList = new List<LeaveApplication>();
                    List<long> mentorTeamMemberIdList = _mentorHistoryService.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now);
                    foreach (var leaveApplicationViewModel in leaveApplicationViewModelList)
                    {
                        TeamMember member = _hrMemberService.GetMember(Convert.ToInt32(leaveApplicationViewModel.TeamMemberPin));
                        if (!mentorTeamMemberIdList.Contains(member.Id))
                        {
                            throw new InvalidDataException(" Your are not authorized to access this pin(" + member.Pin + "). Please remove and request again.");
                        }
                        Leave leave = _hrLeaveService.GetById(leaveApplicationViewModel.LeaveId);

                        LeaveApplication leaveApplication = new LeaveApplication();
                        if (leaveApplicationViewModel.LeaveApplicationId != null && leaveApplicationViewModel.LeaveApplicationId > 0)
                        {
                            leaveApplication = _hrLeaveApplicationService.GetById(Convert.ToInt64(leaveApplicationViewModel.LeaveApplicationId));
                        }
                        leaveApplicationViewModel.FillLeaveApplicationValue(leaveApplication);
                        leaveApplication.Leave = leave;
                        leaveApplication.TeamMember = member;
                        leaveApplication.Status = LeaveApplication.EntityStatus.Active;
                        leaveApplication.LeaveStatus = Convert.ToInt32(leaveApplicationViewModel.LeaveStatus);
                        if (leaveApplicationViewModel.ResponsibleMemberPin != null && leaveApplicationViewModel.ResponsibleMemberPin > 0)
                        {
                            TeamMember responsiblePerson = _hrMemberService.GetByPin(leaveApplicationViewModel.ResponsibleMemberPin.Value);
                            leaveApplication.ResponsiblePerson = responsiblePerson;
                        }
                        leaveApplicationList.Add(leaveApplication);
                    }
                    _hrLeaveApplicationService.SaveOrUpdate(leaveApplicationList, true);
                    message = "Total " + leaveApplicationViewModelList.Count().ToString()+ " leave(s) Save Successfully.";
                    successState = true;
                }
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        [HttpGet]
        public ActionResult MentorRecentLeaveHistory()
        {
            ViewBag.LeaveStatus = new SelectList(_commonHelper.LoadEmumToDictionary<LeaveStatus>(new List<int> { (int)LeaveStatus.Pending }), "Key", "Value");
            return PartialView("Partial/_RecentLeaveHistory");
        }

        [HttpPost]
        public ActionResult MentorRecentLeaveHistoryDataTable(int draw, int start, int length, string pin = "", string leaveStatus = "", string searchingDate = "")
        {
            var data = new List<object>();
            int recordsTotal = 0;
            int recordsFiltered = 0;
            try
            {
                TeamMember member = _hrMemberService.GetCurrentMember();
                if (member == null)
                {
                    var str = new List<string>();
                    int i = 0;
                    for (i = 1; i <= 11; i++)
                    {
                        str.Add("N/A");
                    }
                    data.Add(str);
                }
                else
                {
                    List<int> memberPinList = new List<int>();
                    DateTime? dateTime = null;
                    LeaveStatus? convertedLeaveStatus = null;

                    if (!String.IsNullOrEmpty(leaveStatus))
                    {
                        convertedLeaveStatus = (LeaveStatus) Convert.ToInt32(leaveStatus);
                    }
                    if (!String.IsNullOrEmpty(pin))
                    {
                        memberPinList.Add(Convert.ToInt32(pin));
                    }
                    if (!String.IsNullOrEmpty(searchingDate))
                    {
                        DateTime dateTime2;
                        if (DateTime.TryParse(searchingDate, out dateTime2))
                        {
                            dateTime = dateTime2;
                        }
                    }

                    recordsTotal = _hrLeaveApplicationService.GetMentorLeaveCount(member.Id, dateTime, convertedLeaveStatus, memberPinList);
                    IList<LeaveApplicationViewModel> mentorLeaveApplicationViewModelList = _hrLeaveApplicationService.LoadMentorLeave(member.Id, dateTime, convertedLeaveStatus, memberPinList, start, length).ToList();
                    recordsFiltered = recordsTotal;
                    foreach (var mentorLeaveApplicationViewModel in mentorLeaveApplicationViewModelList)
                    {
                        Leave leave = _hrLeaveService.GetById(mentorLeaveApplicationViewModel.LeaveId);
                        string leaveStatusText = "-";
                        string leaveStatusActionLink = "-";
                        string responsiblePinName = "-";
                        if (mentorLeaveApplicationViewModel.ResponsibleMemberPin != null && !String.IsNullOrEmpty(mentorLeaveApplicationViewModel.ResponsibleMemberName))
                        {
                            responsiblePinName = mentorLeaveApplicationViewModel.ResponsibleMemberName + " (" + mentorLeaveApplicationViewModel.ResponsibleMemberPin + ") ";
                        }
                        var str = new List<string>();
                        str.Add(mentorLeaveApplicationViewModel.TeamMemberPin.ToString());
                        str.Add(mentorLeaveApplicationViewModel.TeamMemberName);
                        str.Add(leave.Name);
                        str.Add(mentorLeaveApplicationViewModel.DateFrom);
                        str.Add(mentorLeaveApplicationViewModel.DateTo);
                        str.Add(mentorLeaveApplicationViewModel.TotalLeaveDay.ToString());
                        str.Add(mentorLeaveApplicationViewModel.ApplicationDate);
                        str.Add(responsiblePinName);
                        str.Add(mentorLeaveApplicationViewModel.LeaveNote);
                        if (mentorLeaveApplicationViewModel.LeaveStatus != null)
                        {
                            if (mentorLeaveApplicationViewModel.LeaveStatus == (int) LeaveStatus.Approved)
                            {
                                leaveStatusText = LeaveStatus.Approved.ToString();
                                if (mentorLeaveApplicationViewModel.IsPost != null && mentorLeaveApplicationViewModel.IsPost == 0)
                                {
                                    leaveStatusActionLink = "<a title='Reject' href='#' data-id='" + mentorLeaveApplicationViewModel.LeaveApplicationId + "' data-pin='" + mentorLeaveApplicationViewModel.TeamMemberPin + "' data-leave='" + mentorLeaveApplicationViewModel.LeaveId + "' data-leaveStatus='" + mentorLeaveApplicationViewModel.LeaveStatus + "' data-note='" + mentorLeaveApplicationViewModel.LeaveNote + "' class='glyphicon glyphicon-remove historyReject'> </a>";
                                }
                            }
                            else if (mentorLeaveApplicationViewModel.LeaveStatus == (int) LeaveStatus.Cancelled)
                            {
                                leaveStatusText = LeaveStatus.Cancelled.ToString();
                            }
                            else if (mentorLeaveApplicationViewModel.LeaveStatus == (int) LeaveStatus.Pending)
                            {
                                leaveStatusText = LeaveStatus.Pending.ToString();
                            }
                            else if (mentorLeaveApplicationViewModel.LeaveStatus == (int) LeaveStatus.Rejected)
                            {
                                leaveStatusText = LeaveStatus.Rejected.ToString();
                                if (mentorLeaveApplicationViewModel.IsPost != null && mentorLeaveApplicationViewModel.IsPost == 0)
                                {
                                    leaveStatusActionLink = "<a title='Approve' href='#' data-id='" + mentorLeaveApplicationViewModel.LeaveApplicationId + "' data-pin='" + mentorLeaveApplicationViewModel.TeamMemberPin + "' data-leave='" + mentorLeaveApplicationViewModel.LeaveId + "' data-leaveStatus='" + mentorLeaveApplicationViewModel.LeaveStatus + "' data-note='" + mentorLeaveApplicationViewModel.LeaveNote + "' class='glyphicon glyphicon-ok historyApproved'> </a>";
                                }
                            }
                        }
                        str.Add("<span id='leaveStatus_" + mentorLeaveApplicationViewModel.LeaveApplicationId + "'>" + leaveStatusText + "</span>");
                        str.Add("<span id='leaveStatusActionLink_" + mentorLeaveApplicationViewModel.LeaveApplicationId + "'>" + leaveStatusActionLink + "</span>");    
                        data.Add(str);

                    }
                }
                //return Json(new
                //{
                //    draw = draw,
                //    recordsTotal = recordsTotal,
                //    recordsFiltered = recordsFiltered,
                //    start = start,
                //    length = length,
                //    data = data
                //});
            }
            catch (InvalidDataException e)
            {
                ViewBag.ErrorMessage = e.Message;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                //throw;
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        #endregion
    }
}