using System;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class MemberLoanFactory : ObjectFactoryBase<MemberLoan>
    {

        private readonly IMemberLoanService _prMemberLoanService;

        public MemberLoanFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _prMemberLoanService = new MemberLoanService(session);

        }

        #region Create

        public MemberLoanFactory Create()
        {
            Object = new MemberLoan()
            {
                LoanApprovedAmount = 8000,
                MonthlyRefund = 1000,
                RefundStart = DateTime.Now.AddMonths(5),
                ApprovedDate = DateTime.Now,
                
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanFactory CreateWithLoanApprovedAmount(decimal loanApprovedAmount,decimal monthlyRefund)
        {
            Object=new MemberLoan()
            {
                LoanApprovedAmount = loanApprovedAmount,
                MonthlyRefund = monthlyRefund,
                RefundStart = DateTime.Now.AddMonths(5),
                ApprovedDate = DateTime.Now,
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanFactory CreateWith(long id)
        {
            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public MemberLoanFactory WithMemberLoanApplication(MemberLoanApplication memberLoanApplication)
        {
            Object.MemberLoanApplication = memberLoanApplication;
            return this;
        }

        public MemberLoanFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _prMemberLoanService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_prMemberLoanService.SaveOrUpdate(x); });

            }
            return this;
        }

        public MemberLoanFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new MemberLoan() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public MemberLoanFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion

    }
}
