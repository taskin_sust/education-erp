﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class AreaControllersFactory : ObjectFactoryBase<AreaControllers>
    {
        #region Object Initialization

        protected readonly IAreaControllersService _areaControllersService;

        public AreaControllersFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _areaControllersService = new AreaControllersService(_session);
        }

        #endregion

        #region Create

        public AreaControllersFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }
        public AreaControllersFactory WithArea(string area)
        {
            Object.Area = area;
            return this;
        }
        public AreaControllersFactory Create()
        {
            Object = new AreaControllers()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Branch.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }


        public AreaControllersFactory Persist()
        {            
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _areaControllersService.Save(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _areaControllersService.Save(item);
                    }
                }
            }
            return this;
        }

        public AreaControllersFactory CreateMore(int numberOfAreaController)
        {
            CreateAreaControllerList(numberOfAreaController);
            return this;
        }

        private List<AreaControllers> CreateAreaControllerList(int numberOfAreaController)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<AreaControllers>();
            for (int i = 0; i < numberOfAreaController; i++)
            {
                var areaController = new AreaControllers()
                {
                    Id = 0,
                    Name = Guid.NewGuid() + "name_" + i,
                    Area = Guid.NewGuid() + "areaname_" + i,
                    Rank = i,
                    BusinessId = GetPcUserName(),
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = AreaControllers.EntityStatus.Active
                };
                list.Add(areaController);
            }
            ObjectList.AddRange(list);
            return list;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void CleanupAreaController(List<long> areaControllerIdList)
        {
            _session.CreateSQLQuery("Delete from AreaControllers where id in(" + string.Join(", ", areaControllerIdList) + ")").ExecuteUpdate();
        }
        
        public void Cleanup()
        {
            DeleteAll();
        }

        #endregion

    }
}
