﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.SalaryHistoryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "SalaryHistory")]
    public class SalaryHistoryDeleteTest:SalaryHistoryBaseTest
    {
        [Fact]
        public void Should_Return_Dependency_Exception_For_Delete_SalaryHistory()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object);
            Assert.Throws<DependencyException>(
                () => _salaryHistoryService.DeleteSalaryHistory(_salaryHistoryFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_Exception_For_Empty_SalaryHistory()
        {
            
            Assert.Throws<InvalidDataException>(
                () => _salaryHistoryService.SaveOrUpdateSalaryHistory(null));
        }

        [Fact]
        public void Should_Return_Invalid_Exception_For_Empty_teamMember()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org);
                //.WithTeamMember(teamMember);

            Assert.Throws<InvalidDataException>(
                () => _salaryHistoryService.SaveOrUpdateSalaryHistory(_salaryHistoryFactory.Object));
        }
    }
}
