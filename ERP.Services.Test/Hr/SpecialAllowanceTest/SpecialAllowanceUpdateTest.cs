﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.SpecialAllowanceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "SpecialAllowanceSetting")]
    public class SpecialAllowanceUpdateTest : SpecialAllowanceBaseTest
    {
        #region Basic Test
        
        [Fact]
        public void Update_SpecialAllowance_Successfull()
        {
            const string effectiveDate = "Oct 2016";
            const string closingDate = "Oct 2016";
            var teamMember = PersistTeamMember(MentorId);
            Branch branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).SingleOrDefault();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, branch);

            _specialAllowanceSettingFactory.Create(teamMember.Pin)
                .WithAmount(2000)
                .WithPaymentType(PaymentType.Monthly)
                .WithEffectiveClosingDate(effectiveDate)
                .WithRemarks("TEST REMARK");
            _specialAllowanceSettingService.SaveOrUpdate(userMenu, _specialAllowanceSettingFactory.Object);
            var savedEntity = _specialAllowanceSettingService.GetSpecialAllowanceSettings(_specialAllowanceSettingFactory.Object.Id);
            savedEntity.PaymentType = (int)PaymentType.Once;
            _specialAllowanceSettingService.SaveOrUpdate(userMenu, savedEntity, true);
            var updatedEntity = _specialAllowanceSettingService.GetSpecialAllowanceSettings(_specialAllowanceSettingFactory.Object.Id);
            Assert.NotNull(updatedEntity);
            Assert.StrictEqual(updatedEntity.PaymentType, (int)PaymentType.Once);
        }

        #endregion

        #region Business logic test

        [Fact]
        public void Update_SpecialAllowance_Should_Failed_For_Invalid_Date_Order()
        {
            const string effectiveDate = "Oct 2016";
            const string closingDate = "Sep 2016";
            var teamMember = PersistTeamMember(MentorId);
            Branch branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).SingleOrDefault();
            var userMenu = BuildUserMenu(organizationFactory.Object, (Program)null, branch);

            _specialAllowanceSettingFactory.Create(teamMember.Pin)
                .WithAmount(2000)
                .WithPaymentType(PaymentType.Monthly)
                .WithEffectiveClosingDate(effectiveDate)
                .WithRemarks("TEST REMARK");
            _specialAllowanceSettingService.SaveOrUpdate(userMenu, _specialAllowanceSettingFactory.Object);
            var savedEntity = _specialAllowanceSettingService.GetSpecialAllowanceSettings(_specialAllowanceSettingFactory.Object.Id);
            savedEntity.ClosingDate = new DateTime(savedEntity.EffectiveDate.Year, savedEntity.EffectiveDate.AddMonths(-1).Month, savedEntity.EffectiveDate.Day);
            Assert.Throws<InvalidDataException>(() => _specialAllowanceSettingService.SaveOrUpdate(userMenu, savedEntity, true));
        }

        #endregion
    }
}
