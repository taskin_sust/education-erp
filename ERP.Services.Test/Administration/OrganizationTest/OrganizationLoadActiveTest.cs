﻿using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.OrganizationTest
{
    interface IOrganizationLoadActiveTest
    {
        #region Basic Test
        void LoadActive_should_return_list();
       // void LoadActive_should_Not_return_list(); 
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Organization")]
    public class OrganizationLoadActiveTest : OrganizationBaseTest, IOrganizationLoadActiveTest
    {
        #region Basic Test
        
        [Fact]
        public void Load_should_return_list()
        {
            _organizationFactory.CreateMore(100).Persist();
            IList<Organization> organizationList = _organizationService.LoadOrganization(0, 100, "", "Asc", "", "", "", "1");
            Assert.Equal(100, organizationList.Count);
        }
        
        [Fact]
        public void LoadAuthorizedOrganization_should_return_list()
        {
            _organizationFactory.CreateMore(100).Persist();
            IList<Organization> organizationList = _organizationService.LoadOrganization(0, 100, "", "Asc", "", "", "", "1");
            Assert.Equal(100, organizationList.Count);
        }
        
        [Fact]
        public void LoadActive_should_return_list()
        {
            _organizationFactory.CreateMore(100).Persist();
            IList<Organization> organizationList = _organizationService.LoadOrganization(Organization.EntityStatus.Active);
            Assert.True(organizationList.Count>0);
        }
        [Fact]
        public void LoadInactive_should_return_list()
        {
            _organizationFactory.CreateMore(100,Organization.EntityStatus.Inactive).Persist();
            IList<Organization> organizationList = _organizationService.LoadOrganization(Organization.EntityStatus.Inactive);
            Assert.True(organizationList.Count > 0);
        }
        
        #endregion
    }
}
