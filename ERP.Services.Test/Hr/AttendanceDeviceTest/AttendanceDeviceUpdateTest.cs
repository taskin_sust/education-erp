﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceDeviceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceDevice")]
    public class AttendanceDeviceUpdateTest : AttendanceDeviceBaseTest
    {
        //TODO: Devide code with BasicTest and Businesslogic Test region

        #region Basic Test
        #endregion

        #region Invalid Property Check

        [Fact]
        public void Save_Should_Return_InValid_Model_For_EmptyCampus()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object).Persist();
            _attendanceDeviceFactory.Object.Campus = null;
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Update(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_EmptySynchronizer()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create().WithCampus(_campusFactory.Object).Persist();
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Update(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_EmptyDeviceModelNo()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object).Persist();
            _attendanceDeviceFactory.Object.DeviceModelNo = null;
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Update(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_Invalid_CommunicationType()
        {

            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object).Persist();
            _attendanceDeviceFactory.Object.CommunicationType = 0;
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Update(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_Invalid_MachineNumber()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object).Persist();
            _attendanceDeviceFactory.Object.MachineNo = -1;
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Update(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_Invalid_Ip()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object).Persist();
            _attendanceDeviceFactory.Object.IpAddress = "";
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Update(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Save_Should_Return_InValid_Model_For_Invalid_CommunicationKey()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object).Persist();
            _attendanceDeviceFactory.Object.CommunicationKey = "";
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Update(_attendanceDeviceFactory.Object));
        }


        #endregion

        #region Business Logic Test 

        
        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                .WithCampus(_campusFactory.Object)
                .WithAttendanceSyncronizer(_campusFactory.Object);
            Assert.Throws<NullObjectException>(() => _attendanceDeviceService.Update(null));
            Assert.Throws<NullObjectException>(() => _attendanceDeviceService.Update(_attendanceDeviceFactory.Object));
        }

        [Fact]
        public void Update_Should_Return_Model_inValid()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceDeviceFactory.Create()
                 .WithCampus(_campusFactory.Object)
                 .WithAttendanceSyncronizer(_campusFactory.Object).Persist();
            var obj = _attendanceDeviceService.GetAttendanceDevice(_attendanceDeviceFactory.Object.Id);
            obj.Campus = null;
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Update(obj));
            obj.IpAddress = "34543sdfg"; //Please Enter valid IP address
            Assert.Throws<MessageException>(() => _attendanceDeviceService.Update(obj));
        }

        [Fact]
        public void Save_Should_Check_Duplicate()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            var obj1 = _attendanceDeviceFactory.Create()
                  .WithCampus(_campusFactory.Object)
                  .WithAttendanceSyncronizer(_campusFactory.Object).WithDeviceMachineNumber(_deviceMachineNo).Persist().Object;
            var obj2 = _attendanceDeviceFactory.Create()
                 .WithCampus(_campusFactory.Object)
                 .WithAttendanceSyncronizer(obj1.AttendanceSynchronizer).WithDeviceMachineNumber(_deviceMachineNo + 1).Persist().Object;

            var objUpdate = _attendanceDeviceService.GetAttendanceDevice(obj2.Id);
            objUpdate.MachineNo = obj1.MachineNo;
            Assert.Throws<DuplicateEntryException>(() => _attendanceDeviceService.Update(objUpdate));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            var obj1 = _attendanceDeviceFactory.Create()
                  .WithCampus(_campusFactory.Object)
                  .WithAttendanceSyncronizer(_campusFactory.Object).WithDeviceMachineNumber(_deviceMachineNo).Persist().Object;

            var objUpdate = _attendanceDeviceService.GetAttendanceDevice(obj1.Id);
            objUpdate.Name = obj1.Name + "1";
            _attendanceDeviceService.Update(objUpdate);
            var objUpdate2 = _attendanceDeviceService.GetAttendanceDevice(objUpdate.Id);
            Assert.Equal(objUpdate2.Name, obj1.Name);
        }


        #endregion

        //TODO: write more test
        //1. device active/inactive test
        //2. campus change test
        //3. branch change test
    }
}
