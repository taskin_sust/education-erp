﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MenuTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Menu")]
    public class MenuLoadActiveTest : MenuBaseTest
    {
        #region Basic Test

        [Fact]
        public void LoadActive_should_return_list()
        {
            _menuFactory.CreateMore(100).Persist();
            var menuList = _menuService.GetMenuList(0, 100, "", "", "", "", "", "", "", "1", "");
            Assert.Equal(100, menuList.Count);
        }

        #endregion
    }
}
