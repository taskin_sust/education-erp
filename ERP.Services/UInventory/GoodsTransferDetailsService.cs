using log4net;
using NHibernate;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface IGoodsTransferDetailService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(GoodsTransferDetails goodsTransferDetail);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class GoodsTransferDetailsService : BaseService, IGoodsTransferDetailService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("InventoryService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IGoodsTransferDetailDao _goodsTransferDetailDao;
        public GoodsTransferDetailsService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _goodsTransferDetailDao = new GoodsTransferDetailsDao { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(GoodsTransferDetails goodsTransferDetail)
        {

            return true;
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}

