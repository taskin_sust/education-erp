using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IFestivalBonusSettingCalculationDao : IBaseDao<FestivalBonusSettingCalculation, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        IList<FestivalBonusSettingCalculation> LoadFestivalBonusList(int start, int length, string orderBy, string orderDir, List<long> authOrgList,
            string employmentStatus, string religion, string calculationOn);


        #endregion

        #region Others Function

        int LoadFestivalBonusListCount(List<long> authOrgList, string employmentStatus, string religion, string calculationOn);

        #endregion
    }

    public class FestivalBonusSettingCalculationDao : BaseDao<FestivalBonusSettingCalculation, long>, IFestivalBonusSettingCalculationDao
    {
        #region Properties & Object & Initialization

        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public IList<FestivalBonusSettingCalculation> LoadFestivalBonusList(int start, int length, string orderBy, string orderDir, List<long> authOrgList,
           string employmentStatus, string religion, string calculationOn)
        {
            ICriteria criteria = GetFestivalBonusCriteria(authOrgList, employmentStatus, religion, calculationOn);
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }
            else
            {
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc("Organization") : Order.Desc("Organization"));
            }
            return criteria.SetFirstResult(start).SetMaxResults(length).List<FestivalBonusSettingCalculation>();
        }

        #endregion

        #region Others Function

        public int LoadFestivalBonusListCount(List<long> authOrgList, string employmentStatus, string religion, string calculationOn)
        {
            ICriteria criteria = GetFestivalBonusCriteria(authOrgList, employmentStatus, religion, calculationOn);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        #endregion

        #region Helper Function

        private ICriteria GetFestivalBonusCriteria(List<long> authOrgList, string employmentStatus, string religion, string calculationOn)
        {
            ICriteria criteria = Session.CreateCriteria<FestivalBonusSettingCalculation>().Add(Restrictions.Not(Restrictions.Eq("Status", Quotation.EntityStatus.Delete)));
            criteria.CreateAlias("PrFestivalBonusSetting", "fb").Add(Restrictions.Not(Restrictions.Eq("fb.Status", FestivalBonusSetting.EntityStatus.Delete)));
            criteria.CreateAlias("fb.Organization", "org").Add(Restrictions.Not(Restrictions.Eq("org.Status", Organization.EntityStatus.Delete)));
            if (authOrgList != null && authOrgList.Any())
            {
                criteria.Add(Restrictions.In("org.Id", authOrgList));
            }
            if (!String.IsNullOrEmpty(employmentStatus))
            {
                criteria.Add(Restrictions.Eq("EmploymentStatus", Convert.ToInt32(employmentStatus)));
            }
            if (!String.IsNullOrEmpty(religion))
            {
                switch (Convert.ToInt32(religion))
                {
                    case 1:
                        criteria.Add(Restrictions.Eq("fb.IsIslam", true));
                        break;
                    case 2:
                        criteria.Add(Restrictions.Eq("fb.IsHinduism", true));
                        break;
                    case 3:
                        criteria.Add(Restrictions.Eq("fb.IsChristianity", true));
                        break;
                    case 4:
                        criteria.Add(Restrictions.Eq("fb.IsBuddhism", true));
                        break;
                    case 5:
                        criteria.Add(Restrictions.Eq("fb.IsOthers", true));
                        break;
                    default:
                        break;
                }
            }
            if (!String.IsNullOrEmpty(calculationOn))
            {
                criteria.Add(Restrictions.Eq("CalculationOn", Convert.ToInt32(calculationOn)));
            }
            return criteria;
        }

        #endregion
    }
}
