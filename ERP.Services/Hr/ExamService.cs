﻿using System;
using System.Collections;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{
    public interface IExamService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<BusinessModel.Entity.Hr.AcademicExam> LoadExamList();
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class ExamService : BaseService, IExamService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IAcademicExamDao _hrExamDao;
        private readonly IAcademicExamDao _hrBoardDao;

        public ExamService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrExamDao = new AcademicExamDao(){Session = session};
        }
        #endregion

        #region Operational Functions

        public bool Save()
        {

            return true;
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<BusinessModel.Entity.Hr.AcademicExam> LoadExamList()
        {
            try
            {
                var list = _hrExamDao.ExamList();
                return list ?? null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}
