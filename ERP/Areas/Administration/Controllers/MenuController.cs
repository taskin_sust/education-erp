﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using UdvashERP.App_code;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.Services;
using System.Collections.Specialized;
using System.Net;
using UdvashERP.BusinessRules;
using log4net;
using UdvashERP.MessageExceptions;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class MenuController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly IMenuService _menuService;
        private readonly ICommonHelper _commonHelper;
        private readonly IUserMenuService _userMenuService;
        private readonly IAreaControllersService _areaControllersService;
        private readonly IActionsService _actionsService;
       
        public MenuController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _menuService = new MenuService(session);
                _commonHelper = new CommonHelper();
                _userMenuService = new UserMenuService(session);
                _areaControllersService = new AreaControllersService(session);
                _actionsService = new ActionsService(session);
            }
            catch(Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
        }
        #endregion

        #region Index/Manage
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ManageMenu()
        {
            try
            {
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                ViewBag.MenuGroupFiter = new SelectList(_menuService.LoadAllMenuGroup(), "Id", "Name");
                ViewBag.MenuListFiter = new SelectList(string.Empty, "Value", "Text");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Menu Loading Fail";
            }

            ViewBag.PageSize = Constants.PageSize;
            return View();
        }

        /// <summary>
        /// Habibul Morsalin
        /// </summary>
        /// <param name="draw"></param>
        /// <param name="start"></param>
        /// <param name="length"></param>
        /// <param name="menuGroupId"></param>
        /// <param name="parentId"></param>
        /// <param name="name"></param>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <param name="status"></param>
        /// <param name="rank"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MenuListResult(int draw, int start, int length, string menuGroupId, string parentId, string name, string controllerName, string actionName, string status, string rank)
        {
            string cn = controllerName.ToString();

            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;

                    string orderBy = "";
                    string orderDir = "";
                    #region OrderBy and Direction
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {

                            case "0":
                                orderBy = "MenuGroup.Id";
                                break;
                            case "1":
                                orderBy = "MenuSelfMenu.Id";
                                break;
                            case "2":
                                orderBy = "Name";
                                break;
                            case "3":
                                orderBy = "Controller";
                                break;
                            case "4":
                                orderBy = "Action";
                                break;
                            case "5":
                                orderBy = "Staus";
                                break;
                            case "6":
                                orderBy = "Rank";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }


                    #endregion
                    int recordsTotal = _menuService.MenuRowCount(menuGroupId, parentId, name, controllerName, actionName, status, rank);
                    long recordsFiltered = recordsTotal;
                    List<Menu> menuList = _menuService.GetMenuList(start, length, orderBy, orderDir.ToUpper(), menuGroupId, parentId, name, controllerName, actionName, status, rank).ToList();
                    
                    var data = new List<object>();
                    long oldParentId = -1;
                    int maxRank = 0;
                    foreach (var c in menuList)
                    {

                        var str = new List<string>();
                        long tempParentId = 0;

                        str.Add(c.MenuGroup.Name);
                        if (c.MenuSelfMenu != null)
                        {
                            tempParentId = c.MenuSelfMenu.Id;
                            str.Add(c.MenuSelfMenu.Name);
                        }
                        else
                        {
                            str.Add("Root");
                        }
                        str.Add(c.Name);
                        str.Add(StatusTypeText.GetStatusText(c.Status));

                        if (tempParentId != oldParentId)
                        {
                            maxRank = _menuService.GetMenuMaximumRank(tempParentId);
                            oldParentId = tempParentId;
                        }
                        string rankbtn = c.Rank.ToString();
                        if (c.Rank > 1)
                        {
                            rankbtn += "<a href='#'  id='" + c.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                        }
                        if (c.Rank < maxRank)
                        {
                            rankbtn += "<a href='#'  id='" + c.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                        }
                        str.Add(rankbtn);

                        var menulink = "";
                        if (c.DefaultAction != null)
                        {
                            //menulink = "<a href='" + Url.Action(c.DefaultAction.Name, c.DefaultAction.AreaControllers.Name, new { area = c.DefaultAction.AreaControllers.Area }) + "' target='_blank' > " + c.DefaultAction.AreaControllers.Area + "/" + c.DefaultAction.AreaControllers.Name + "/" + c.DefaultAction.Name + "  </a>";
                            menulink = "<a href='" + Url.Action(c.DefaultAction.Name, c.DefaultAction.AreaControllers.Name, new { area = c.DefaultAction.AreaControllers.Area }) + "' target='_blank' > Click Here  </a>";

                        }

                        str.Add(menulink);
                        
                        if (c.DefaultAction != null)
                        {
                            str.Add(c.DefaultAction.Name);
                        }
                        else
                        {
                            str.Add("Empty");
                        }
                        str.Add("<a href='" + Url.Action("Details", "Menu") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp;" +
                                "<a href='" + Url.Action("Edit", "Menu") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp;<a id='" + c.Id.ToString() + "' href='#' data-name='" + c.Name.ToString() + "' class='glyphicon glyphicon-trash'> </a>");

                        data.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        iDisplayLength = length,
                        iTotalRecords = recordsTotal,
                        iTotalDisplayRecords = recordsTotal,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return Json(HttpNotFound());
        }
        #endregion

        #region Save Operation
        public ActionResult AddMenu()
        {
            try
            {
                ViewBag.MenuGroupList = new SelectList(_menuService.LoadAllMenuGroup(), "Id", "Name");
                ViewBag.MenuList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.areaList = new SelectList(_areaControllersService.LoadArea(), "Area", "Area");
                ViewBag.controllersList = new SelectList(string.Empty, "Id", "Name");
                ViewBag.actionsList = new SelectList(string.Empty, "Id", "Name");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult AddMenu(MenuForm menuForm)
        {
            try
            {
                ViewBag.MenuGroupList = new SelectList(_menuService.LoadAllMenuGroup(), "Id", "Name");
                ViewBag.MenuList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.areaList = new SelectList(_areaControllersService.LoadArea(), "Area", "Area");
                ViewBag.controllersList = new SelectList(string.Empty, "Id", "Name");
                ViewBag.actionsList = new SelectList(string.Empty, "Id", "Name");
                if (ModelState.IsValid)
                {
                    Menu menu = new Menu();
                    menu.Name = menuForm.Name;
                    menu.VariableNames = menuForm.VariableNames;
                    if (menuForm.ActionName != null)
                    {
                        menu.DefaultAction = _actionsService.LoadById((long) menuForm.ActionName);
                    }

                    MenuGroup menuGroup = _menuService.LoadById(menuForm.MenuGroupId);
                    menu.MenuGroup = menuGroup;
                    if (menuForm.ParentId != null)
                    {
                        Menu parentMenu = _menuService.MenuLoadById(Convert.ToInt64(menuForm.ParentId));
                        menu.MenuSelfMenu = parentMenu;
                    }
                    _menuService.Save(menu);

                    ViewBag.SuccessMessage = "Menu Addded Successfully";
                    ModelState.Clear();
                    return View(new MenuForm());
                }
            }
            
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Menu Addded Fail";
            }
            return View(menuForm);
        }
        #endregion
        
        #region Update Operation
        public ActionResult Edit(long id)
        {
            try
            {
                Menu menu = _menuService.MenuLoadById(id);
                if (menu == null)
                {
                    return HttpNotFound();
                }
                var menuGroupList = new SelectList(_menuService.LoadAllMenuGroup(), "Id", "Name");
                ViewBag.MenuGroupList = menuGroupList;

                var menuList = new SelectList(_menuService.LoadByMenuGroupId(Convert.ToInt32(menu.MenuGroup.Id)), "Id", "Name");
                ViewBag.MenuList = menuList;

                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                if (menu.DefaultAction != null)
                {
                    ViewBag.areaList = new SelectList(_areaControllersService.LoadArea(), "Area", "Area",
                        menu.DefaultAction.AreaControllers.Area);
                    ViewBag.controllersList =
                        new SelectList(
                            _areaControllersService.LoadControllersByArea(menu.DefaultAction.AreaControllers.Area), "Id",
                            "Name", menu.DefaultAction.AreaControllers.Id);
                    ViewBag.actionsList =
                        new SelectList(_actionsService.LoadActiveByControllers(menu.DefaultAction.AreaControllers.Id),
                            "Id", "Name", menu.DefaultAction.Id);
                }
                else
                {
                    ViewBag.areaList = new SelectList(_areaControllersService.LoadArea(), "Area", "Area");
                    ViewBag.controllersList = new SelectList(string.Empty, "Id", "Name");
                    ViewBag.actionsList = new SelectList(string.Empty, "Id", "Name");
                }

                MenuForm menuForm = new MenuForm();
                menuForm.Id = menu.Id;
                menuForm.Name = menu.Name;
                menuForm.VariableNames = menu.VariableNames;
                menuForm.Status = menu.Status;
                menuForm.MenuGroupId = menu.MenuGroup.Id;

                if (menu.MenuSelfMenu != null)
                {
                    menuForm.ParentId = menu.MenuSelfMenu.Id;
                }

                return View(menuForm);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MenuForm menuForm, long id)
        {
            try
            {
                Menu updateMenu = _menuService.MenuLoadById(id);
                if (updateMenu == null)
                {
                    return HttpNotFound();
                }
                var menuGroupList = new SelectList(_menuService.LoadAllMenuGroup(), "Id", "Name");
                ViewBag.MenuGroupList = menuGroupList;

                var menuList = new SelectList(_menuService.LoadByMenuGroupId(Convert.ToInt32(updateMenu.MenuGroup.Id)), "Id", "Name");
                ViewBag.MenuList = menuList;

                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                if (updateMenu.DefaultAction != null)
                {
                    ViewBag.areaList = new SelectList(_areaControllersService.LoadArea(), "Area", "Area",
                        updateMenu.DefaultAction.AreaControllers.Area);
                    ViewBag.controllersList =
                        new SelectList(
                            _areaControllersService.LoadControllersByArea(updateMenu.DefaultAction.AreaControllers.Area), "Id",
                            "Name", updateMenu.DefaultAction.AreaControllers.Id);
                    ViewBag.actionsList =
                        new SelectList(_actionsService.LoadActiveByControllers(updateMenu.DefaultAction.AreaControllers.Id),
                            "Id", "Name", updateMenu.DefaultAction.Id);
                }
                else
                {
                    ViewBag.areaList = new SelectList(_areaControllersService.LoadArea(), "Area", "Area");
                    ViewBag.controllersList = new SelectList(string.Empty, "Id", "Name");
                    ViewBag.actionsList = new SelectList(string.Empty, "Id", "Name");
                }
                if (ModelState.IsValid)
                {
                    updateMenu.Name = menuForm.Name;
                    updateMenu.VariableNames = menuForm.VariableNames;
                    updateMenu.Status = menuForm.Status;
                    int maxRank;
                    MenuGroup menuGroup = _menuService.LoadById(menuForm.MenuGroupId);
                    updateMenu.MenuGroup = menuGroup;
                    if (menuForm.ActionName != null)
                    {
                        updateMenu.DefaultAction = _actionsService.LoadById((long)menuForm.ActionName);
                    }
                    else
                    {
                        updateMenu.DefaultAction = null;
                    }
                    if (menuForm.ParentId != null)
                    {
                        Menu parentMenu = _menuService.MenuLoadById(Convert.ToInt64(menuForm.ParentId));

                        if (updateMenu.MenuSelfMenu != null)
                        {
                            if (menuForm.ParentId != updateMenu.MenuSelfMenu.Id)
                            {
                                maxRank = _menuService.GetMenuMaximumRank(Convert.ToInt64(menuForm.ParentId));
                                updateMenu.Rank = maxRank + 1;
                            }
                        }
                        else
                        {
                            maxRank = _menuService.GetMenuMaximumRank(Convert.ToInt64(menuForm.ParentId));
                            updateMenu.Rank = maxRank + 1;
                        }

                        updateMenu.MenuSelfMenu = parentMenu;
                    }
                    else
                    {
                        if (updateMenu.MenuSelfMenu != null)
                        {
                            maxRank = _menuService.GetMenuMaximumRank(0);
                            updateMenu.Rank = maxRank + 1;
                        }
                        updateMenu.MenuSelfMenu = null;
                    }
                    _menuService.Update(updateMenu);
                    ViewBag.SuccessMessage = "Menu Update Successfully";
                    ModelState.Clear();
                }
                return View(menuForm);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Menu Update Fail";
                return View(menuForm);
            }
        }
        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                Menu deleteMenu = _menuService.MenuLoadById(id);
                deleteMenu.Status = Menu.EntityStatus.Delete;
                _menuService.Delete(deleteMenu);
                return Json(new Response(true, "Menu Delete Successful !"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Menu Delete Fail !"));
            }
        }
        #endregion

        #region Details Operation
        public ActionResult Details(long id)
        {
            try
            {
                Menu menu = _menuService.MenuLoadById(id);
                if (menu == null)
                {
                    return HttpNotFound();
                }
                return View(menu);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Menu Loading Fail";
                return View();
            }
        }
        #endregion

        #region Rank Operation
        public ActionResult RankChange(int id, int current, string action)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    Menu menuUpdateObj = _menuService.MenuLoadById(Convert.ToInt64(id));
                    if (menuUpdateObj == null)
                    {
                        return HttpNotFound();
                    }
                    int newRank;
                    if (action == "up")
                        newRank = menuUpdateObj.Rank - 1;
                    else
                        newRank = menuUpdateObj.Rank + 1;
                    long parentId = 0;
                    if (menuUpdateObj.MenuSelfMenu != null)
                        parentId = menuUpdateObj.MenuSelfMenu.Id;
                    //var menuOldObj = _menuService.MenuLoadByRank(newRank, parentId);
                    var menuOldObj = _menuService.LoadByRankNextOrPrevious(newRank, action);

                    menuOldObj.Rank = menuUpdateObj.Rank;
                    menuUpdateObj.Rank = newRank;
                    bool isSuccessfullyUpdateRank = _menuService.UpdateRank(menuOldObj, menuUpdateObj);
                    if (isSuccessfullyUpdateRank)
                    {
                        return Json(new Response(true, "Successfully changed"));
                    }
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            return HttpNotFound();
        }
        #endregion

        #region Helper Function
        /// <summary>
        /// Habibul Morsalin
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="MenuGroupId"></param>
        /// <param name="parentId"></param>
        /// <returns>json</returns>
        public JsonResult doesMenuExistUnderParent(string Name, long? MenuGroupId, long? parentId, long? Id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    bool d = _menuService.DuplicationCheck(Name, MenuGroupId, parentId, Id);
                    return Json(d);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            return Json(false);
        }
        [HttpPost]
        public ActionResult GetMenu(int groupId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    //  throw new Exception("Hi");
                    IList<Menu> menuList = _menuService.LoadByMenuGroupId(groupId);
                    return PartialView("Partial/_Menu", menuList);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }

            }
            return HttpNotFound();

        }


        [HttpGet]
        [AuthorizeAccess]
        [AuthorizeRequired]
        public ActionResult GetUserMenuByGroup(long groupId)
        {
            try
            {
                Session["UserMenuGroupId"] = groupId;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }
        [HttpPost]
        public ActionResult GetMenuForDropDownList(int? groupId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (groupId != null)
                    {
                        SelectList menuGroupSelectList = new SelectList(_menuService.LoadByMenuGroupId(Convert.ToInt32(groupId)), "Id", "Name");
                        List<SelectListItem> _list = menuGroupSelectList.ToList();
                        _list.Insert(0, new SelectListItem() { Value = "", Text = "--Select Parent Menu--" });
                        _list.Insert(1, new SelectListItem() { Value = "-1", Text = "--Root--" });
                        ViewBag.MenuGroupList = new SelectList((IEnumerable<SelectListItem>)_list, "Value", "Text");
                    }
                    else
                    {
                        List<SelectListItem> _list = new List<SelectListItem>();
                        _list.Insert(0, new SelectListItem() { Value = "", Text = "--Select Parent Menu--" });
                        ViewBag.MenuGroupList = new SelectList((IEnumerable<SelectListItem>)_list, "Value", "Text");
                    }
                    return PartialView("Partial/_MenuDropDownList");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            return HttpNotFound();
        }
        [HttpPost]
        public ActionResult GetControllersForArea(string area)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (area == null) return PartialView("Partial/_ControllersList");
                    var controllersList = new SelectList(_areaControllersService.LoadControllersByArea(area), "Id", "Name");
                    var list = controllersList.ToList();
                    list.Insert(0, new SelectListItem() { Value = "", Text = "Select Controller" });
                    ViewBag.ControllersList = new SelectList(list, "Value", "Text");
                    return PartialView("Partial/_ControllersList");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            return null;
        }
        [HttpPost]
        public ActionResult GetActionsForController(long? controller)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (controller == null || controller < 1)
                    {
                        var actionList = new SelectList(string.Empty, "Value", "Text");
                        ViewBag.ActionsList = actionList;
                        ViewBag.ActionsListCount = actionList.Count();
                        return PartialView("Partial/_ActionsList");
                    }
                    var actionsList = new SelectList(_actionsService.LoadActiveByControllers(controller.Value), "Id", "Name");
                    var list = actionsList.ToList();
                    list.Insert(0, new SelectListItem() { Value = "", Text = "Select Action" });
                    ViewBag.ActionsList = new SelectList(list, "Value", "Text");
                    ViewBag.ActionsListCount = actionsList.Count();
                    return PartialView("Partial/_ActionsList");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            return null;
        }

        #endregion               
    }
}