using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{

    public partial class FestivalBonusSettingMap : BaseClassMap<FestivalBonusSetting,long>
    {

        public FestivalBonusSettingMap()
        {
			Table("HR_FestivalBonusSetting");
			LazyLoad();
			References(x => x.Organization).Column("OrganizationId");
			Map(x => x.Name).Column("Name").Not.Nullable();
			Map(x => x.EffectiveDate).Column("EffectiveDate").Not.Nullable();
			Map(x => x.ClosingDate).Column("ClosingDate");
			Map(x => x.IsIslam).Column("IsIslam").Not.Nullable();
			Map(x => x.IsHinduism).Column("IsHinduism").Not.Nullable();
			Map(x => x.IsChristianity).Column("IsChristianity").Not.Nullable();
			Map(x => x.IsBuddhism).Column("IsBuddhism").Not.Nullable();
			Map(x => x.IsOthers).Column("IsOthers").Not.Nullable();
            HasMany(x => x.PrFestivalBonusCalculation).KeyColumn("FestivalBonusSettingId").Cascade.AllDeleteOrphan().Inverse();
			//HasMany(x => x.PrMemberfestivalbonussheet).KeyColumn("FestivalBonusId");
        }
    }
}
