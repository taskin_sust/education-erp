﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{
    public class TeamMemberLeaveDetailsMap : BaseClassMap<LeaveApplicationDetails, long>
    {
        public TeamMemberLeaveDetailsMap()
        {
            Table("Hr_LeaveApplicationDetails");
			LazyLoad();			
            Map(x => x.DateFrom).Column("DateFrom");
            Map(x => x.DateTo).Column("DateTo");
            Map(x => x.IsPost).Column("IsPost");            
            References(x => x.LeaveApplication).Column("LeaveApplicationId");
        }
    }
}
