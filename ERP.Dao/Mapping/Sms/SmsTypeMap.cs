﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Mapping.Sms
{
    /// <summary>
    /// Habib
    /// </summary>
    public class SmsTypeMap : BaseClassMap<SmsType, long>
    {
        public SmsTypeMap()
        {
            Table("SmsType");
            LazyLoad();

            Map(x => x.Rank);
            Map(x => x.Name);
            HasManyToMany(x => x.DynamicOptions).Table("SmsTypeDynamicOption").ParentKeyColumn("SmsTypeId").ChildKeyColumn("SmsDynamicOptionId"); 
            HasMany(x => x.SmsSettingses).KeyColumn("SmsTypeId").Cascade.AllDeleteOrphan().Inverse().Where(ss => ss.Status != SmsSettings.EntityStatus.Delete).OrderBy("Rank");
            HasMany(x => x.SmsStudentRegistrationReceiverList).KeyColumn("SmsTypeId").Cascade.AllDeleteOrphan().Inverse();
            // HasManyToMany(x=>x.)
            //References(x => x.Branch).Column("BranchId");
            //HasMany(x => x.Batches).KeyColumn("CampusId").Cascade.AllDeleteOrphan().Inverse();
            //.Where(ss => ss.Status != MenuGroup.EntityStatus.Delete).OrderBy("Rank");
        }
    }
}
