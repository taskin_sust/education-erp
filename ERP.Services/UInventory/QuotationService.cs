using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FluentNHibernate.Utils;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface IQuotationService : IBaseService
    {

        #region Operational Function

        bool SaveOrUpdate(Quotation quotation);
        bool IsDelete(long id);

        #endregion

        #region Single Instances Loading Function

        Quotation GetQuotation(long id, long? itemId = null);

        #endregion

        #region List Loading Function

        IList<Quotation> LoadQuotation(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenus, string organization, string branch,
            string itemName, string deadLine, QuotationStatus? quotationStatus = null);
        IList<ManageQuotationDto> LoadSupplierManageQuotation(int start, int length, string supplierId, string organizationId, string branchId, string itemId, string status);
        List<QuotationReportDto> LoadQuotationReport(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string organizationId, long[] branchIdList, int[] purposeList, long[] itemGroupIdList, long[] itemIdList, string[] programSessionIds, int quotationStatus, string dateFrom, string dateTo);

        #endregion

        #region Others Function

        int LoadQuotationCount(string orderBy, string orderDir, List<UserMenu> userMenus, string organization, string branch, string itemName, string deadLine, QuotationStatus? quotationStatus = null);
        int LoadSupplierManageQuotationCount(string supplierId, string organizationId, string branchId, string itemId, string status);
        int LoadQuotationReportCount(List<UserMenu> userMenu, string organizationId, long[] branchIdList, int[] purposeList, long[] itemGroupIdList, long[] itemIdList,
            string[] programSessionIds, int quotationStatus, string dateFrom, string dateTo);

        #endregion

        #region Helper Function

        string GetLastQuotationNo(long branchId);

        #endregion

    }

    public class QuotationService : BaseService, IQuotationService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IQuotationDao _quotationDao;
        private readonly ISupplierPriceQuoteDao _supplierPriceQuoteDao;
        private readonly IWorkOrderDao _workOrderDao;
        public QuotationService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _quotationDao = new QuotationDao { Session = session };
            _supplierPriceQuoteDao = new SupplierPriceQuoteDao() { Session = session };
            _workOrderDao = new WorkOrderDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(Quotation quotation)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeSaveOrUpdate(quotation);
                Quotation obj = new Quotation();
                using (transaction = Session.BeginTransaction())
                {
                    var valuesAsArray = Enum.GetValues(typeof(Purpose)).Cast<Purpose>().ToList();
                    var itemType = quotation.Item.ItemType;
                    if (quotation.Id == 0)
                    {
                        obj = quotation;
                        obj.Status = Quotation.EntityStatus.Active;
                        
                        if (quotation.Purpose != null && !valuesAsArray.Contains((Purpose)quotation.Purpose))
                            obj.Purpose = null;
                    }
                    else
                    {
                        List<QuotationCriteria> clonedList = quotation.QuotationCriteriaList.CloneList().ToList();

                        obj = _quotationDao.LoadById(quotation.Id);
                        obj.Program = quotation.Program;
                        obj.Session = quotation.Session;
                        if (quotation.Purpose != null && !valuesAsArray.Contains((Purpose)quotation.Purpose))
                            obj.Purpose = null;
                        else
                            obj.Purpose = quotation.Purpose;
                        obj.QuotationQuantity = quotation.QuotationQuantity;
                        obj.QuotationStatus = quotation.QuotationStatus;
                        obj.Remarks = quotation.Remarks;
                        obj.SubmissionDeadLine = quotation.SubmissionDeadLine;
                        obj.DeliveryDate = quotation.DeliveryDate;
                        if (quotation.QuotationStatus == QuotationStatus.Running)
                            obj.PublishedDate = quotation.PublishedDate;
                        obj.Item = quotation.Item;
                        obj.Branch = quotation.Branch;
                        obj.QuotationCriteriaList.Clear();

                        foreach (var quotCriteria in clonedList)
                        {
                            var quotationCriteria = new QuotationCriteria();
                            quotationCriteria.Criteria = quotCriteria.Criteria;
                            quotationCriteria.CriteriaValue = quotCriteria.CriteriaValue;
                            quotationCriteria.Quotation = obj;
                            obj.QuotationCriteriaList.Add(quotationCriteria);
                        }
                    }
                    _quotationDao.SaveOrUpdate(obj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (MessageException)
            {
                throw;
            }

            catch (EmptyFieldException)
            {
                throw;
            }

            catch (DuplicateEntryException)
            {
                throw;
            }

            catch (InvalidDataException)
            {
                throw;
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
            return true;
        }

        public bool IsDelete(long id)
        {
            ITransaction trans = null;
            try
            {
                var obj = _quotationDao.LoadById(id);
                if (obj == null)
                    throw new NullObjectException("Quotation is not valid");

                CheckBeforeDelete(obj);
                using (trans = Session.BeginTransaction())
                {
                    obj.QuotationStatus = QuotationStatus.Cancelled;
                    _quotationDao.Update(obj);
                    trans.Commit();
                    return true;
                }
            }
            catch (DependencyException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        private void CheckBeforeDelete(Quotation obj)
        {
            bool count = _workOrderDao.CheckActiveQuotation(obj.Id);
            if (count == false)
            {
                throw new DependencyException("You can't delete this quotation , work order issued according to quotation.");
            }
        }

        #endregion

        #region Single Instances Loading Function

        public Quotation GetQuotation(long id, long? itemId = null)
        {
            try
            {
                return itemId != null ? _quotationDao.GetQuotationByItem(id, itemId) : _quotationDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<Quotation> LoadQuotation(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenus, string organizationId, string branchId,
            string itemName, string deadLine, QuotationStatus? quotationStatus = null)
        {
            try
            {
                if (userMenus == null || !userMenus.Any())
                    throw new InvalidDataException("Invalid User menu");

                var organizationIdList = !string.IsNullOrEmpty(organizationId) ? _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)) : null;
                var branchIdList = !string.IsNullOrEmpty(branchId) ? _commonHelper.ConvertIdToList(Convert.ToInt64(branchId)) : null;
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, null, branchIdList);
                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenus, organizationIdList);

                if (authBranchIdList == null || !authBranchIdList.Any())
                    throw new InvalidDataException("Invalid Branch");

                if (authOrganizationIdList == null || !authOrganizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                return _quotationDao.LoadQuotation(start, length, orderBy, orderDir, authOrganizationIdList, authBranchIdList, itemName, deadLine, quotationStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ManageQuotationDto> LoadSupplierManageQuotation(int start, int length, string supplierId, string organizationId, string branchId, string itemId, string status)
        {
            try
            {
                return _quotationDao.LoadSupplierManageQuotation(start, length, supplierId, organizationId, branchId, itemId, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<QuotationReportDto> LoadQuotationReport(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string organizationId, long[] branchIdList, int[] purposeList, long[] itemGroupIdList, long[] itemIdList, string[] programSessionIds, int quotationStatus, string dateFrom, string dateTo)
        {
            try
            {
                if (userMenu == null || !userMenu.Any())
                    throw new InvalidDataException("Invalid User menu");

                var organizationIdList = !string.IsNullOrEmpty(organizationId) ? _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)) : null;
                var branchIds = (branchIdList != null && branchIdList.Count() > 0 && !branchIdList.Contains(0)) ? branchIdList.ToList() : null;
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchIds);
                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organizationIdList);

                if (authOrganizationIdList == null || !authOrganizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                if (authBranchIdList == null || !authBranchIdList.Any())
                    throw new InvalidDataException("Invalid Branch");

                return _quotationDao.LoadQuotationReport(start, length, orderBy, orderDir, authOrganizationIdList,
                    authBranchIdList, purposeList, itemGroupIdList, itemIdList, programSessionIds, quotationStatus, dateFrom, dateTo);
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int LoadQuotationCount(string orderBy, string orderDir, List<UserMenu> userMenus, string organizationId, string branchId, string itemName, string deadLine,
            QuotationStatus? quotationStatus = null)
        {
            try
            {
                if (userMenus == null || !userMenus.Any())
                    throw new InvalidDataException("Invalid User menu");

                var organizationIdList = !string.IsNullOrEmpty(organizationId) ? _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)) : null;
                var branchIdList = !string.IsNullOrEmpty(branchId) ? _commonHelper.ConvertIdToList(Convert.ToInt64(branchId)) : null;
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, null, branchIdList);
                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenus, organizationIdList);

                if (authBranchIdList == null || !authBranchIdList.Any())
                    throw new InvalidDataException("Invalid Branch");

                if (authOrganizationIdList == null || !authOrganizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                return _quotationDao.LoadQuotationCount(orderBy, orderDir, authOrganizationIdList, authBranchIdList, itemName, deadLine, quotationStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int LoadSupplierManageQuotationCount(string supplierId, string organizationId, string branchId, string itemId, string status)
        {
            try
            {
                return _quotationDao.LoadSupplierManageQuotationCount(supplierId, organizationId, branchId, itemId, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int LoadQuotationReportCount(List<UserMenu> userMenu, string organizationId, long[] branchIdList, int[] purposeList,
            long[] itemGroupIdList, long[] itemIdList, string[] programSessionIds, int quotationStatus, string dateFrom,
            string dateTo)
        {
            try
            {
                if (userMenu == null || !userMenu.Any())
                    throw new InvalidDataException("Invalid User menu");

                var organizationIdList = !string.IsNullOrEmpty(organizationId) ? _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)) : null;
                var branchIds = (branchIdList != null && branchIdList.Count() > 0 && !branchIdList.Contains(0)) ? branchIdList.ToList() : null;
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchIds);
                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organizationIdList);

                if (authBranchIdList == null || !authBranchIdList.Any())
                    throw new InvalidDataException("Invalid Branch");

                if (authOrganizationIdList == null || !authOrganizationIdList.Any())
                    throw new InvalidDataException("Invalid Organization");

                return _quotationDao.LoadQuotationReportCount(authOrganizationIdList, authBranchIdList, purposeList, itemGroupIdList, itemIdList, programSessionIds,
                    quotationStatus, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public string GetLastQuotationNo(long branchId)
        {
            try
            {
                var quotationObj = Session.QueryOver<Quotation>().Where(x => x.Branch.Id == branchId).OrderBy(x => x.Id).Desc.List<Quotation>().FirstOrDefault();
                return quotationObj == null ? "" : quotationObj.QuotationNo;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        private void CheckBeforeSaveOrUpdate(Quotation quotation)
        {

            if (quotation == null)
                throw new NullObjectException("Object can not be null !!");

            CustomModelValidationCheck.DataAnnotationCheck(quotation, new Quotation());

            bool checkQuotationNo = _quotationDao.IsDuplicateQuotationNo(quotation.QuotationNo, quotation.Id);
            if (checkQuotationNo)
                throw new DuplicateEntryException("Sorry!! Quotation no duplicate.Please re-submit quotation");

            if (quotation.Item == null)
                throw new NullObjectException("Item can't be empty.");

            var itemType = quotation.Item.ItemType;

            //if (quotation.Purpose == null && quotation.Program != null && quotation.Session != null)
            //    throw new InvalidDataException("If purpose is N/A selected then Program & Session must be N/A");

            //if ((quotation.Purpose == (int)Purpose.ProgramPurpose || quotation.Purpose == (int)Purpose.MarketingPurpose
            //    || quotation.Purpose == (int)Purpose.GiftAndPromotionPurpose) && (quotation.Program == null || quotation.Session == null))
            //    throw new InvalidDataException("Program and Session must be selected while purpose is selected by program or marketing or giftAndPromotion purpose.");

            //if (itemType == (int)ItemType.ProgramItem && (quotation.Program == null || quotation.Session == null))
            //    throw new InvalidDataException("Program and Session must be selected while item type is program item.");

            //if (itemType == (int)ItemType.CommonItem && (quotation.Program != null || quotation.Session != null))
            //    throw new InvalidDataException("Program and Session must be selected by N/A while item type is common item.");

            var valuesAsArray = Enum.GetValues(typeof(Purpose)).Cast<Purpose>().ToList();
            if (quotation.Purpose != null && !valuesAsArray.Contains((Purpose)quotation.Purpose))
                throw new InvalidDataException("Purpose can't be empty");


            if (quotation.QuotationQuantity < 1)
                throw new InvalidDataException("Quotation quantity must be greater than 0");

            if (!quotation.Branch.IsCorporate && quotation.Purpose == null)
                throw new InvalidDataException("Branch select as individual branch, Purpose should not be empty.");

            if (quotation.QuotationCriteriaList == null || quotation.QuotationCriteriaList.Count < 1)
                throw new NullObjectException("Quotation criteria can't be empty.");

            var check = quotation.QuotationCriteriaList.Select(x => x.Quotation).Count();
            if (check < 1)
                throw new NullObjectException("Quotation can't be empty.");

            if (quotation.Item.SpecificationTemplate == null)
                throw new NullObjectException("Specification can't be empty.");
            
            if (itemType == (int)ItemType.ProgramItem && (quotation.Program == null || quotation.Session == null))
                throw new InvalidDataException("Program and Session must be selected while item type is program item.");

            if (itemType == (int)ItemType.CommonItem && (quotation.Program != null || quotation.Session != null))
                throw new InvalidDataException("Program and Session must be N/A when item type common item");
            
            if (itemType == (int)ItemType.ProgramItem && quotation.Purpose == null && (quotation.PurposeProgram != null && quotation.PurposeSession != null))
                throw new InvalidDataException("Program and Session must be selected while item type is program item.");

            if (itemType == (int)ItemType.ProgramItem && (quotation.Purpose <0 && quotation.Purpose<=5) && (quotation.PurposeProgram == null || quotation.PurposeSession == null))
                throw new InvalidDataException("Program and Session must be selected while item type is program item.");
            

            if(itemType == (int)ItemType.CommonItem &&(quotation.Purpose>0 && quotation.Purpose<=3) && (quotation.PurposeProgram == null || quotation.PurposeSession == null))
                throw new InvalidDataException("Program and Session must be selected while purpose is selected by program or marketing or giftAndPromotion purpose " +
                                               "and item type common item");
            
            //if(itemType==(int)ItemType.CommonItem && (quotation.Purpose==null || quotation.Purpose<=0) && (quotation.Program != null && quotation.Session != null))
            //    throw new InvalidDataException("Program and Session must be selected by N/A while item type is common item and purpose is N/A.");

            if (itemType == (int)ItemType.CommonItem && (quotation.Purpose == null || quotation.Purpose <= 0) && (quotation.PurposeProgram != null || quotation.PurposeSession != null))
                throw new InvalidDataException("Program and Session must be selected by N/A while item type is common item and purpose is N/A.");

            //if (itemType == (int)ItemType.CommonItem && (quotation.Purpose < 0 && quotation.Purpose <= 3) && (quotation.PurposeProgram == null || quotation.PurposeSession == null))
            //    throw new InvalidDataException("Program and Session must be selected by N/A while item type is common item and purpose is N/A.");


            DateTime currentDate = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
            DateTime submissionDeadLine = DateTime.ParseExact(quotation.SubmissionDeadLine.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

            string currentTime = currentDate.ToString("HH:mm:ss");
            string dDate = quotation.DeliveryDate.ToString("yyyy-MM-dd");
            string ddLine = dDate + " " + currentTime;

            DateTime deliveryDeadLine = DateTime.ParseExact(ddLine, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

            if (submissionDeadLine <= currentDate)
                throw new InvalidDataException("Submission date must be greater than current date");

            if (deliveryDeadLine <= submissionDeadLine)
                throw new InvalidDataException("Delivery date must be greater than submission date");
            

            if (quotation.Item.SpecificationTemplate.SpecificationCriteriaList.Count() != quotation.QuotationCriteriaList.Count())
                throw new NullObjectException("Quotation specification criteria can't be empty.");
        }

        #endregion

    }
}