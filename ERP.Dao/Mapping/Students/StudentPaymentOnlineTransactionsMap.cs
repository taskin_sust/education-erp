﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentPaymentOnlineTransactionsMap : BaseClassMap<StudentPaymentOnlineTransactions, long>
    {
        public StudentPaymentOnlineTransactionsMap()
        {
            Table("StudentPaymentOnlineTransactions");
            LazyLoad();

            Map(x => x.Referance).Column("Referance");
            Map(x => x.TransactionId).Column("TransactionId");
            Map(x => x.SenderMobile).Column("SenderMobile");
            Map(x => x.TransactionTime).Column("TransactionTime");
            Map(x => x.Amount).Column("Amount");
            Map(x => x.OnlinePaymentStatus).Column("OnlinePaymentStatus");
            References(x => x.StudentPayment).Column("StudentPaymentId"); 
        }
    }
}
//{"Association references unmapped class: UdvashERP.BusinessModel.Entity.Students.StudentPaymentOnlineTransactions"}