﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Administration.BankBranchTest
{
    public interface IBankBrachSaveTest
    {
        #region Basic test
        [Fact]
        void Save_Should_Return_Null_Exception();
        [Fact]
        void Should_Save_Successfully();

        #endregion

        #region Business logic test
        [Fact]
        void Should_Not_Save_Duplicate_Routing_No();
        [Fact]
        void Should_Not_Save_Duplicate_Swift_Code();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "BankBranch")]
    public class BankBranchSaveTest : BankBranchBaseTest, IBankBrachSaveTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _bankBranchService.IsSave(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var bankBranchFactory =_bankBranchFactory.Create().WithBank().Persist();
            BankBranch bankBranchObj = _bankBranchService.GetBankBranch(bankBranchFactory.Object.Id);
            Assert.NotNull(bankBranchObj);
            Assert.True(bankBranchFactory.Object.Id > 0);
        }

        #endregion

        #region Business Logic Test
        [Fact]
        public void Should_Not_Save_Duplicate_Routing_No()
        {
            _bankBranchFactory.Create().WithBank().With_Routing_No(_routingNo).Persist();
            var routingNo = _bankBranchFactory.Object.RoutingNo;
            var bankBranchFactory2 =
                _bankBranchFactory.Create().WithBank().With_Routing_No(routingNo);
            Assert.Throws<DuplicateEntryException>(() => bankBranchFactory2.Persist());
        }
        [Fact]
        public void Should_Not_Save_Duplicate_Swift_Code()
        {
            _bankBranchFactory.Create().WithBank().With_Swift_Code(_swiftCode).Persist();
            var swiftCode = _bankBranchFactory.Object.SwiftCode;
            var bankBranchFactory2 =
                _bankBranchFactory.Create().WithBank().With_Swift_Code(swiftCode);
            Assert.Throws<DuplicateEntryException>(() => bankBranchFactory2.Persist());
        }

        #endregion

    }
 
    
}
