﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using HibernatingRhinos.Profiler.Appender.Util;
using iTextSharp.text;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class DirectGoodsReceiveController : Controller
    {

        //
        // GET: /UInventory/DirectGoodsReceive/
        #region logger
        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private List<UserMenu> authorizeMenu;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly IProgramSessionItemService _programSessionItemService;
        private readonly IItemGroupService _itemGroupService;
        private readonly IItemService _itemService;
        private readonly IBranchService _branchService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ISupplierService _supplierService;
        private readonly ICommonHelper _commonHelper;
        private readonly IGoodsReceiveService _goodsReceiveService;

        public DirectGoodsReceiveController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _organizationService = new OrganizationService(session);
            _programBranchSessionService = new ProgramBranchSessionService(session);
            _programSessionItemService = new ProgramSessionItemService(session);
            _itemGroupService = new ItemGroupService(session);
            _itemService = new ItemService(session);
            _branchService = new BranchService(session);
            _programService = new ProgramService(session);
            _supplierService = new SupplierService(session);
            _sessionService = new SessionService(session);
            _goodsReceiveService = new GoodsReceiveService(session);

            _commonHelper = new CommonHelper();
        }

        #endregion

        #region create
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DirectGoodsReceive(string messType = "", string message = "")
        {
            try
            {
                Initialize();
                if (!String.IsNullOrEmpty(messType))
                {
                    if (messType.Equals("true"))
                        ViewBag.SuccessMessage = message;
                    else
                        ViewBag.ErrorMessage = message;
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult DirectGoodsReceive(DirectGoodsReceiveViewModel dGReceiveVm)
        {
            try
            {
                var goodsRec = new GoodsReceive()
                {
                    Branch = _branchService.GetBranch(Convert.ToInt64(dGReceiveVm.Branch)),
                    Supplier = _supplierService.LoadById(Convert.ToInt64(dGReceiveVm.Supplier)),
                    TotalCost = (decimal)Math.Floor(dGReceiveVm.ReceivedQuantity * dGReceiveVm.Rate),
                    GoodsReceivedDate = DateTime.Now,
                    GoodsReceiveStatus = (int)GoodsReceivedStatus.Received,
                    Remarks = dGReceiveVm.Remarks
                };
                var goodrecDetails = new GoodsReceiveDetails();
                if (dGReceiveVm.ProgramSession != null && dGReceiveVm.ProgramSession != "0")
                {
                    var pSAry = dGReceiveVm.ProgramSession.Split(new[] { "::" }, StringSplitOptions.None);
                    long pId = Convert.ToInt64(pSAry[0]);
                    long sId = Convert.ToInt64(pSAry[1]);
                    if (pSAry.Length < 2) return RedirectToAction("DirectGoodsReceive", new { messType = "false", message = "invalid program and session selected !!" });
                    goodrecDetails.Program = _programService.GetProgram(pId);
                    goodrecDetails.Session = _sessionService.LoadById(sId);
                }
                goodrecDetails.Item = _itemService.LoadById(Convert.ToInt64(dGReceiveVm.Item));
                goodrecDetails.ReceivedQuantity = dGReceiveVm.ReceivedQuantity;
                goodrecDetails.Rate = (decimal)dGReceiveVm.Rate;
                goodrecDetails.TotalCost = (decimal)Math.Floor(dGReceiveVm.ReceivedQuantity * dGReceiveVm.Rate);
                goodrecDetails.Purpose = dGReceiveVm.Purpose;
                goodsRec.GoodsReceiveDetails.Add(goodrecDetails);
                _goodsReceiveService.SaveOrUpdate(goodsRec);

                return View("Details", Details(goodsRec, dGReceiveVm));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("DirectGoodsReceive", new { messType = "false", message = "goods received failed" });
            }
        }


        #endregion

        #region details

        public DirectGoodReceiveDetails Details(GoodsReceive goodsReceive, DirectGoodsReceiveViewModel gReceiveVm)
        {
            var details = new DirectGoodReceiveDetails();
            try
            {
                details.Branch = goodsReceive.Branch;
                details.ReceivedQuantity = gReceiveVm.ReceivedQuantity;
                details.Item = _itemService.LoadById(Convert.ToInt64(gReceiveVm.Item));
                details.ItemGroup = _itemGroupService.LoadById(gReceiveVm.ItemGroup);
                details.Organization = _organizationService.LoadById(gReceiveVm.Organization);
                details.Supplier = goodsReceive.Supplier;
                details.Remarks = gReceiveVm.Remarks;
                List<KeyValuePair<int, string>> enumList = _commonHelper.LoadEmumToDictionary<Purpose>().ToList();
                foreach (var keyValuePair in enumList)
                {
                    if (keyValuePair.Key == gReceiveVm.Purpose)
                    {
                        details.Purpose = keyValuePair.Value;
                        break;
                    }
                }
                if (String.IsNullOrEmpty(details.Purpose))
                {
                    details.Purpose = "N/A";
                }
                details.Rate = gReceiveVm.Rate;
                details.Value = (long)Math.Floor(details.Rate * details.ReceivedQuantity);
                details.ReceivedNumber = goodsReceive.GoodsReceivedNo;
                details.ReceivedDate = goodsReceive.GoodsReceivedDate;
                if (gReceiveVm.ProgramSession != null && gReceiveVm.ProgramSession.Length > 0 && gReceiveVm.ProgramSession != "0")
                {
                    var pSAry = gReceiveVm.ProgramSession.Split(new[] { "::" }, StringSplitOptions.None);
                    long pId = Convert.ToInt64(pSAry[0]);
                    long sId = Convert.ToInt64(pSAry[1]);
                    details.Program = _programService.GetProgram(pId);
                    details.Session = _sessionService.LoadById(sId);
                    details.ProgramSession = details.Program.Name + details.Session.Name;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return details;
        }
        #endregion

        #region update
        #endregion

        #region delete
        #endregion

        #region helper
        private void Initialize()
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
            ViewBag.branchList = new SelectList(String.Empty, "Value", "Text");
            var enumList = _commonHelper.LoadEmumToDictionary<Purpose>().ToList();
            enumList.Insert(0, new KeyValuePair<int, string>(SelectionType.SelelectAll, "N/A"));

            ViewBag.purposeList = new SelectList(enumList, "key", "value");
            ViewBag.supplierList = new SelectList(_supplierService.LoadSupplierList(0, int.MaxValue, "", "", "", Supplier.EntityStatus.Active.ToString()), "Id", "Name");
            ViewBag.itemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
            ViewBag.itemList = new SelectList(String.Empty, "Value", "Text");
            ViewBag.programSessionList = new SelectList(String.Empty, "Value", "Text");

        }

        private List<SelectListItem> GenerateAppropriateProgramSession(long organizationId, int selectedPurpose, long branchId, long itemId)
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            var selectListItems = new List<SelectListItem>();

            const int pPurpose = (int)Purpose.ProgramPurpose;
            const int mPurpose = (int)Purpose.MarketingPurpose;
            const int gPurpose = (int)Purpose.GiftAndPromotionPurpose;
            const int aPurpose = (int)Purpose.AdministrativePurpose;
            const int bPurpose = (int)Purpose.BrandingPurpose;

            var programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, _commonHelper.ConvertIdToList(branchId), _commonHelper.ConvertIdToList(itemId)).ToList();
            Item item = _itemService.LoadById(itemId);

            if (item.ItemType == (int)ItemType.ProgramItem)
                selectListItems = programSessionList.Select(programBranchSession => new SelectListItem()
                {
                    Text = programBranchSession.ProgramName + " " + programBranchSession.SessionName,
                    Value = programBranchSession.ProgramId + "," + programBranchSession.SessionId
                }).ToList();
            else if (item.ItemType == (int)ItemType.CommonItem && selectedPurpose == 0)
                selectListItems.Add(new SelectListItem() { Text = "N/A", Value = SelectionType.SelelectAll.ToString() });
            else if (item.ItemType == (int)ItemType.CommonItem && (selectedPurpose.Equals(pPurpose) || selectedPurpose.Equals(mPurpose) || selectedPurpose.Equals(gPurpose)))
            {
                selectListItems = programSessionList.Select(programBranchSession => new SelectListItem()
                {
                    Text = programBranchSession.ProgramName + " " + programBranchSession.SessionName,
                    Value = programBranchSession.ProgramId + "," + programBranchSession.SessionId
                }).ToList();
            }
            else if (item.ItemType == (int)ItemType.CommonItem && (selectedPurpose.Equals(aPurpose) || selectedPurpose.Equals(bPurpose)))
            {
                selectListItems = programSessionList.Select(programBranchSession => new SelectListItem()
                {
                    Text = programBranchSession.ProgramName + " " + programBranchSession.SessionName,
                    Value = programBranchSession.ProgramId + "," + programBranchSession.SessionId
                }).ToList();
                selectListItems.Insert(0, new SelectListItem() { Text = "N/A", Value = SelectionType.SelelectAll.ToString() });
            }

            return selectListItems;
        }


        #endregion

        #region ajax request

        [HttpPost]
        public ActionResult FilterProgramSessionByBranch(long branchId)
        {
            try
            {
                bool isCorporate = _branchService.IsCorporateBranch(branchId);
                return Json(new Response(isCorporate, ""));
            }
            catch (InvalidDataException dataException)
            {
                _logger.Error(dataException);
                return Json(false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(false);
            }
        }

        [HttpPost]
        public ActionResult FilterProgramSession(long organization, int purpose, long branchId, long itemId)
        {
            var selectListItems = new List<SelectListItem>();
            try
            {
                if (!Request.IsAjaxRequest()) return Json(new Response(false, "Invalid Ajax Request !"));
                if (purpose < 0) return Json(new Response(false, "Invalid Purpose selected !"));
                if (branchId <= 0) return Json(new Response(false, "Invalid Branch selected !"));
                if (itemId <= 0) return Json(new Response(false, "Invalid Item selected !"));
                selectListItems = GenerateAppropriateProgramSession(organization, purpose, branchId, itemId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            ViewBag.programSessionList = selectListItems;
            return PartialView("Partial/_programSessionList");
        }

        [HttpPost]
        public JsonResult LoadItemGroup()
        {
            try
            {
                if (!Request.IsAjaxRequest()) return Json(new Response(false, "Invalid Ajax Request !"));
                var itemGrpList = new SelectList(_itemGroupService.LoadItemGroup(0, int.MaxValue, "", "", "", ItemGroup.EntityStatus.Active.ToString()), "Id", "Name");
                return Json(new { itemGrpList = itemGrpList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { itemGrpList = new SelectList(new List<SelectListItem>()), IsSuccess = false });
            }
        }

        #endregion

    }
}