﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class HomeController : Controller
    {
        [AuthorizeRequired]
        public ActionResult Index()
        {
            return View();
        }
    }
}