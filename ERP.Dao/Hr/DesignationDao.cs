﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IDesignationDao : IBaseDao<Designation, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
        Designation GetDesignationByStatus(int status);
        IList<Designation> LoadActive(int start, int length, string orderBy, string orderDir, List<long> organizationIds, string status);
        IList<Designation> LoadHrDesignationList(List<long> organizationIds);
        IList<Designation> LoadHrDesignationList(List<long> organizationIds, DateTime effectiveDate);
        #endregion

        #region Others Function
        int TotalRowCountOfDepartmentSettings(List<long> organizationIds, string status);
        bool HasDuplicateByRank(int rank,long organizationId, long? id);
        bool HasDuplicateByDesignationName(string name, long organizationId, long? id);
        bool HasDuplicateByDesignationShortName(string shortName, long organizationId, long? id);
        #endregion

        #region Helper Function
        bool HasDependency(Designation hrDesignation);
        #endregion
  
    }
    public class DesignationDao : BaseDao<Designation, long>, IDesignationDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public Designation GetDesignationByStatus(int status)
        {
            IList<Designation> entityList = Session.QueryOver<Designation>().Where(x => x.Status == status).List<Designation>();
            if (entityList.Count > 0)
                return entityList[0];
            return null;
        }
        
        #endregion

        #region List Loading Function
        public IList<Designation> LoadActive(int start, int length, string orderBy, string orderDir, List<long> organizationIds, string status)
        {
            ICriteria criteria = Session.CreateCriteria<Designation>().Add(Restrictions.Not(Restrictions.Eq("Status", Designation.EntityStatus.Delete)));
            criteria.CreateAlias("Organization", "Organization");
            if (organizationIds!=null)
            {
                criteria.Add(Restrictions.In("Organization.Id", organizationIds));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (orderDir == "ASC")
            {
                criteria.AddOrder(Order.Asc(orderBy));
            }
            else
            {
                criteria.AddOrder(Order.Desc(orderBy));
            }
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Designation>();
        }

        public IList<Designation> LoadHrDesignationList(List<long> organizationIds)
        {
            var departmenrtList =
               Session.QueryOver<Designation>().Where(x => x.Organization.Id.IsIn(organizationIds.ToArray()) && x.Status==Designation.EntityStatus.Active).OrderBy(x=>x.Rank).Asc.List<Designation>();
            return departmenrtList;
        }

        public IList<Designation> LoadHrDesignationList(List<long> organizationIds, DateTime effectiveDate)
        {
            string query = @"select x.Id,x.Name from (select distinct d.Id,d.Name,d.Rank from HR_EmploymentHistory eh inner join HR_Designation d on eh.DesignationId=d.Id
                             and eh.status="+EmploymentHistory.EntityStatus.Active+" and d.status="+Designation.EntityStatus.Active+
                            " and organizationId in("+string.Join(", ",organizationIds)+") and eh.EffectiveDate<='" + effectiveDate.Date.ToString("yyyy-MM-dd") + " 23:59:59')x order by x.rank";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<Designation>());
            var designationList = iQuery.List<Designation>().ToList();
            return designationList;
        }

        #endregion

        #region Others Function
        public int TotalRowCountOfDepartmentSettings(List<long> organizationIds, string status)
        {
            ICriteria criteria = Session.CreateCriteria<Designation>().Add(Restrictions.Not(Restrictions.Eq("Status", Designation.EntityStatus.Delete)));
            criteria.CreateAlias("Organization", "Organization");
            if (organizationIds!=null)
            {
                criteria.Add(Restrictions.In("Organization.Id", organizationIds));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public bool HasDuplicateByRank(int rank,long organizationId, long? id)
        {
            ICriteria criteria = Session.CreateCriteria<Designation>();
            criteria.CreateAlias("Organization", "org").Add(Restrictions.Eq("org.Status",Organization.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Rank", rank));
            criteria.Add(Restrictions.Eq("org.Id", organizationId));
            criteria.Add(Restrictions.Eq("Status", Designation.EntityStatus.Active));
            if (id!=null)
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            IList<Designation> rowList = criteria.List<Designation>();
            if (rowList == null || rowList.Count < 1)
                return true;
            return false;
        }

        public bool HasDuplicateByDesignationName(string name, long organizationId, long? id)
        {
            ICriteria criteria = Session.CreateCriteria<Designation>();
            criteria.CreateAlias("Organization", "org").Add(Restrictions.Eq("org.Status", Organization.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Name", name));
            criteria.Add(Restrictions.Eq("org.Id", organizationId));
            criteria.Add(Restrictions.Eq("Status", Designation.EntityStatus.Active));
            if (id != null)
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            IList<Designation> rowList = criteria.List<Designation>();
            if (rowList == null || rowList.Count < 1)
                return true;
            return false;
        }

        public bool HasDuplicateByDesignationShortName(string shortName, long organizationId, long? id)
        {
            ICriteria criteria = Session.CreateCriteria<Designation>();
            criteria.CreateAlias("Organization", "org").Add(Restrictions.Eq("org.Status", Organization.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("ShortName", shortName));
            criteria.Add(Restrictions.Eq("org.Id", organizationId));
            criteria.Add(Restrictions.Eq("Status", Designation.EntityStatus.Active));
            if (id != null)
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            IList<Designation> rowList = criteria.List<Designation>();
            if (rowList == null || rowList.Count < 1)
                return true;
            return false;
        }

        

        #endregion

        #region Helper Function

        public bool HasDependency(Designation hrDesignation)
        {
            var count = hrDesignation.EmploymentHistory.Count + hrDesignation.EmploymentHistoryLog.Count +
                        hrDesignation.MentorHistory.Count + hrDesignation.MentorHistoryLog.Count;
            if (count > 0)
                return true;
            return false;
        }

        #endregion

    }

}
