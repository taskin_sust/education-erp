using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr {


    public partial class MemberEbfBalanceMap : BaseClassMap<MemberEbfBalance,long>
    {

        public MemberEbfBalanceMap()
        {
			Table("HR_MemberEbfBalance");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
            Map(x => x.EmployeeAmount).Column("EmployeeAmount").Not.Nullable();
			Map(x => x.EmployerAmount).Column("EmployerAmount").Not.Nullable();
			Map(x => x.DepositDate).Column("DepositDate").Not.Nullable();
        }
    }
}
