﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ExtradayAllowanceSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ExtraDayAllowanceSetting")]
    public class ExtradayAllowanceSettingSaveTest : ExtradayAllowanceSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Null_Object_Exception_For_Null_EntityObject()
        {
            Assert.Throws<NullObjectException>(() => _extradayAllowanceSettingService.SaveOrUpdate(null, null));
        }

        [Fact]
        public void Invalid_Data_Exception_For_Null_UserMenu()
        {
            Assert.Throws<InvalidDataException>(() => _extradayAllowanceSettingService.SaveOrUpdate(new ExtradayAllowanceSetting(), null));
        }

        [Fact]
        public void Invalid_Data_Exception_For_Null_Model_Validation()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            Assert.Throws<InvalidDataException>(() => _extradayAllowanceSettingService.SaveOrUpdate(new ExtradayAllowanceSetting(), userMenu));
        }

        [Fact]
        public void Extraday_Allowance_Setting_Save_Successful()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            _extradayAllowanceSettingService.SaveOrUpdate(objFactory.Object, userMenu);
            var entity = _extradayAllowanceSettingService.GetExtradayAllowanceSetting(_extradayAllowanceSettingFactory.Object.Id);
            Assert.Equal(_extradayAllowanceSettingFactory.Object, entity);
        }

        [Fact]
        public void Extraday_Allowance_Setting_Previous_Date_Entry_Should_Save_Successful()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            _extradayAllowanceSettingService.SaveOrUpdate(objFactory.Object, userMenu);
            var entity = _extradayAllowanceSettingService.GetExtradayAllowanceSetting(_extradayAllowanceSettingFactory.Object.Id);
            
            var minDate = entity.EffectiveDate;
            var list = _extradayAllowanceSettingFactory.CreateMore(1, orgFactory.ObjectList[0]);
            list.ObjectList[0].ClosingDate = minDate.AddDays(-10);
            list.ObjectList[0].EffectiveDate = minDate.AddMonths(-5);
            _extradayAllowanceSettingService.SaveOrUpdate(list.ObjectList[0], userMenu);
            var preDateEntity = _extradayAllowanceSettingService.GetExtradayAllowanceSetting(list.ObjectList[0].Id);
            Assert.True(preDateEntity.Id > 0);
        }
        [Fact]
        public void Extraday_Allowance_Setting_Same_Date_Range_Entry_Should_Save_Successful_With_Diff_Combination()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            objFactory.Object.IsContractual = false;
            objFactory.Object.IsIntern = false;
            objFactory.Object.IsPartTime = false;
            objFactory.Object.IsGazetted = false;
            objFactory.Object.IsManagement = false;

            _extradayAllowanceSettingService.SaveOrUpdate(objFactory.Object, userMenu);
            var entity = _extradayAllowanceSettingService.GetExtradayAllowanceSetting(_extradayAllowanceSettingFactory.Object.Id);
            
            var list = _extradayAllowanceSettingFactory.CreateMore(1, orgFactory.ObjectList[0]);
            list.ObjectList[0].ClosingDate = entity.ClosingDate;
            list.ObjectList[0].EffectiveDate = entity.EffectiveDate;
            list.ObjectList[0].IsPermanent = false;
            list.ObjectList[0].IsProbation = false;
            list.ObjectList[0].IsWeekend = false;
            
            _extradayAllowanceSettingService.SaveOrUpdate(list.ObjectList[0], userMenu);
            var preDateEntity = _extradayAllowanceSettingService.GetExtradayAllowanceSetting(list.ObjectList[0].Id);
            Assert.NotNull(preDateEntity);
            Assert.True(preDateEntity.Id > 0);
        }

        #endregion

        #region Business logic Test
        
        [Fact]
        public void Extraday_Allowance_Setting_Save_Date_Invalid()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            objFactory.Object.ClosingDate = objFactory.Object.EffectiveDate.AddDays(-10);
            Assert.Throws<InvalidDataException>(() => _extradayAllowanceSettingService.SaveOrUpdate(objFactory.Object, userMenu));
        }

        [Fact]
        public void Invalid_Data_Exception_For_Equal_Dates()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            objFactory.Object.ClosingDate = objFactory.Object.EffectiveDate;
            Assert.Throws<InvalidDataException>(() => _extradayAllowanceSettingService.SaveOrUpdate(objFactory.Object, userMenu));
        }

        [Fact]
        public void Invalid_Data_Exception_For_Invalid_Dates()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            objFactory.Object.ClosingDate = objFactory.Object.EffectiveDate.AddDays(-10);
            Assert.Throws<InvalidDataException>(() => _extradayAllowanceSettingService.SaveOrUpdate(objFactory.Object, userMenu));
        }

        //TODO: Write test for further inverse date range

        [Fact]
        public void Invalid_Data_Exception_For_Name_Null()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            objFactory.Object.Name = null;
            Assert.Throws<InvalidDataException>(() => _extradayAllowanceSettingService.SaveOrUpdate(objFactory.Object, userMenu));
        }

        [Fact]
        public void Duplicate_Exception_For_Date_Range_Exist_As_Previous_ClosingDate_Null()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            objFactory.Object.ClosingDate = null;
            objFactory.Persist(userMenu);
            var objFactory1 = _extradayAllowanceSettingFactory.CreateMore(1, orgFactory.ObjectList[0]);
            Assert.Throws<DuplicateEntryException>(() => _extradayAllowanceSettingService.SaveOrUpdate(objFactory1.ObjectList[0], userMenu));
        }

        [Fact]
        public void Duplicate_Exception_For_Date_Range_Exist_As_In_Given_Range()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            objFactory.Persist(userMenu);
            var list = _extradayAllowanceSettingFactory.CreateMore(1, orgFactory.ObjectList[0]);
            list.ObjectList[0].EffectiveDate = objFactory.Object.ClosingDate != null ? objFactory.Object.ClosingDate.Value.AddDays(-1) : objFactory.Object.EffectiveDate.AddDays(10);

            Assert.Throws<DuplicateEntryException>(() => _extradayAllowanceSettingService.SaveOrUpdate(list.ObjectList[0], userMenu));
        }

        [Fact]
        public void Duplicate_Exception_For_In_Future_Date_As_Given_Closing_Date_Null()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            objFactory.Persist(userMenu);
            var list = _extradayAllowanceSettingFactory.CreateMore(1, orgFactory.ObjectList[0]);
            list.ObjectList[0].EffectiveDate = objFactory.Object.EffectiveDate.AddYears(-1);
            list.ObjectList[0].ClosingDate = null;

            Assert.Throws<DuplicateEntryException>(() => _extradayAllowanceSettingService.SaveOrUpdate(list.ObjectList[0], userMenu));
        }
        [Fact]
        public void Duplicate_Exception_For_Date_Range_Exist_As_Same_Or_Future_Range()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            objFactory.Persist(userMenu);
            var objFactory1 = _extradayAllowanceSettingFactory.CreateMore(1, orgFactory.ObjectList[0]);
            objFactory1.ObjectList[0].EffectiveDate = objFactory.Object.EffectiveDate;
            Assert.Throws<DuplicateEntryException>(() => _extradayAllowanceSettingService.SaveOrUpdate(objFactory1.ObjectList[0], userMenu));
        }

        [Fact]
        public void InvalidDataException_For_HolidayTypes_Invalid()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            objFactory.Object.IsGazetted = false;
            objFactory.Object.IsManagement = false;
            objFactory.Object.IsWeekend = false;
            Assert.Throws<InvalidDataException>(() => _extradayAllowanceSettingService.SaveOrUpdate(objFactory.Object, userMenu));
        }

        [Fact]
        public void InvalidDataException_For_EmploymentTypes_Invalid()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var userMenu = BuildUserMenu(orgFactory.ObjectList);
            var objFactory = _extradayAllowanceSettingFactory.Create(orgFactory.ObjectList[0]);
            objFactory.Object.IsContractual = false;
            objFactory.Object.IsPartTime = false;
            objFactory.Object.IsPermanent = false;
            objFactory.Object.IsProbation = false;
            objFactory.Object.IsIntern = false;
            Assert.Throws<InvalidDataException>(() => _extradayAllowanceSettingService.SaveOrUpdate(objFactory.Object, userMenu));
        }


        //TODO: Write test for 
        //1. without employment status combination.
        //2. holiday types combination according to business rule.       

        #endregion
    }
}
