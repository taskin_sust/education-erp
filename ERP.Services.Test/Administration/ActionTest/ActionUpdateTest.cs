﻿using System;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.AdminisTration.ProgramTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ActionTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Action")]
    public class ActionUpdateTest : ActionBaseTest
    {
        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var actionFactory =
                _actionsFactory.Create().WithController().WithNameAndRank(_name, 1)
                    .Persist();
            Assert.Throws<NullObjectException>(() => _actionsService.Update(actionFactory.Object.Id, null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var actionFactory =
                _actionsFactory.Create().WithController().WithNameAndRank(_name, 1)
                    .Persist();
            var controller = actionFactory.Object.AreaControllers;
            var newAction = new Actions()
            {
                Name = actionFactory.Name + " def",
                AreaControllers = controller
            };
            var success = _actionsService.Update(actionFactory.Object.Id, newAction);
            Assert.True(success);
        }

        [Fact]
        public void Update_Should_Check_Duplicate_Name_Entry()
        {
            var actionFactory =
                _actionsFactory.Create().WithController().WithNameAndRank(_name, 1)
                    .Persist();
            var controller = actionFactory.Object.AreaControllers;
            Actions action1 = actionFactory.Object;
            var action2 = new Actions()
            {
                Id = 0,
                BusinessId = Environment.MachineName,
                Name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)",
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Branch.EntityStatus.Active,
                AreaControllers = controller
            };
            _actionsService.Save(action2);
            var newAction = new Actions
            {
                Name = action1.Name,
                AreaControllers = controller
            };
            Assert.Throws<DuplicateEntryException>(() => _actionsService.Update(action2.Id, newAction));
            _actionsFactory.CleanupActions(new List<long>() { action2.Id });
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Not_Update_Empty_Name()
        {
            var actionFactory1 =
                _actionsFactory.Create()
                    .WithController()
                    .WithNameAndRank(_name, 1)
                    .Persist();
            Actions actions1 = actionFactory1.Object;
            var newProgram = new Actions
            {
                Id = 0,
                BusinessId =null,
                Name = "",
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Branch.EntityStatus.Active,
                AreaControllers = actionFactory1.Object.AreaControllers
            };
            Assert.Throws<EmptyFieldException>(() => _actionsService.Update(actions1.Id, newProgram));
        }
        [Fact]
        public void Should_Not_Update_Empty_Controller()
        {
            var actionFactory1 =
                _actionsFactory.Create()
                    .WithController()
                    .WithNameAndRank(_name, 1)
                    .Persist();
            Actions actions1 = actionFactory1.Object;
            var newProgram = new Actions
            {
                Id = 0,
                BusinessId = Environment.MachineName,
                Name = "",
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Branch.EntityStatus.Active,
                AreaControllers = null
            };
            Assert.Throws<EmptyFieldException>(() => _actionsService.Update(actions1.Id, newProgram));
        }
        #endregion
    }
}
