﻿using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using NHibernate.Linq;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Teachers
{
    public interface ITeacherAdmissionInfoDao : IBaseDao<TeacherAdmissionInfo, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<TeacherAdmissionInfo> LoadTeacherAdmissionInfo(long teacherId);
        #endregion

        #region Others Function

        #endregion
    }

    public class TeacherAdmissionInfoDao : BaseDao<TeacherAdmissionInfo, long>, ITeacherAdmissionInfoDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<TeacherAdmissionInfo> LoadTeacherAdmissionInfo(long teacherId)
        {
            var criteria = Session.CreateCriteria<TeacherAdmissionInfo>();
            criteria.CreateCriteria("Teacher", "teacher").Add(Restrictions.Eq("teacher.Status", Teacher.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Status", TeacherAdmissionInfo.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("teacher.Id", teacherId));
            return criteria.List<TeacherAdmissionInfo>();
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
