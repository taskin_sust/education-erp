﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.OrganizationTest
{
    public interface IOrganizationLoadByIdTest
    {
        #region Basic Test
        [Fact]
        void LoadById_Null_Check();

        [Fact]
        void LoadById_NotNull_Check();

        [Fact]
        void LoadActive_NotNull_Check();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Organization")]
    public class OrganizationLoadByIdTest : OrganizationBaseTest, IOrganizationLoadByIdTest
    {
        #region Basic Test
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _organizationService.LoadById(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            OrganizationFactory organizationFactory =
                 _organizationFactory.Create()
                     .WithNameAndRank("", 1)
                     .WithRequiredProperties(_name, _shortName, _website, _email, _contactNo, _logoImageUrl,
                         _reportImageUrl, _idFooterImageUrl).Persist();
            var obj = _organizationService.LoadById(_organizationFactory.Object.Id);
            Assert.NotNull(obj);
        }
        [Fact]
        public void LoadActive_NotNull_Check()
        {
            OrganizationFactory organizationFactory =
                 _organizationFactory.Create()
                     .WithNameAndRank("", 1)
                     .WithRequiredProperties(_name, _shortName, _website, _email, _contactNo, _logoImageUrl,
                         _reportImageUrl, _idFooterImageUrl).Persist();
            var obj = _organizationService.GetActiveOrganization();
            Assert.NotNull(obj);
        }
        #endregion
    }
}
