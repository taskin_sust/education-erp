﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class ComplementaryCourseLogMap :BaseClassMap<ComplementaryCourseLog, long>
    {
        public ComplementaryCourseLogMap ()
        {
            Table("ComplementaryCourseLog");
            LazyLoad();
            Map(x => x.ProgramId);
            Map(x => x.SessionId);
            Map(x => x.CourseId);
            Map(x => x.CompCourseId);
        }
    }
}
