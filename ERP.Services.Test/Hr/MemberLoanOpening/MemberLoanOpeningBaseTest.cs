﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.MemberLoanOpening
{
    public class MemberLoanOpeningBaseTest : TestBase, IDisposable
    {
        #region Propertise & Object Initialization

        protected readonly CommonHelper _commonHelper;
        protected readonly TeamMemberFactory _teamMemberFactory;
        protected readonly MemberLoanApplicationFactory _memberLoanApplicationFactory;
        protected readonly MemberLoanFactory _memberLoanFactory;

        protected readonly IMemberLoanService _memberLoanService;

        #endregion

        public MemberLoanOpeningBaseTest()
        {
            _teamMemberFactory = new TeamMemberFactory(null, Session);
            _memberLoanApplicationFactory = new MemberLoanApplicationFactory(null,Session);
            _memberLoanFactory = new MemberLoanFactory(null,Session);

            _memberLoanService = new MemberLoanService(Session);
        }
        
        #region disposal

        public void Dispose()
        {
            _memberLoanFactory.CleanUp();
            _memberLoanApplicationFactory.CleanUp();
            base.Dispose();
        }

        #endregion

    }
}
