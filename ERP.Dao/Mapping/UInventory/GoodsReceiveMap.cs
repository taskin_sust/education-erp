using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{


    public class GoodsReceiveMap : BaseClassMap<GoodsReceive, long>
    {

        public GoodsReceiveMap()
        {
            Table("UINV_GoodsReceive");
            LazyLoad();
            References(x => x.Workorder).Column("WorkOrderId");
            References(x => x.Branch).Column("BranchId");
            References(x => x.Supplier).Column("SupplierId");
            Map(x => x.TotalCost).Column("TotalCost").Not.Nullable();
            Map(x => x.GoodsReceivedDate).Column("GoodsReceivedDate").Not.Nullable();
            Map(x => x.GoodsReceivedNo).Column("GoodsReceivedNo").Not.Nullable();
            Map(x => x.GoodsReceiveStatus).Column("GoodsReceiveStatus").Not.Nullable();
            Map(x => x.Remarks).Column("Remarks");
            HasMany(x => x.GoodsReceiveDetails).KeyColumn("GoodsRecieveId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.GoodsReturnList).KeyColumn("GoodsReceivedId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
