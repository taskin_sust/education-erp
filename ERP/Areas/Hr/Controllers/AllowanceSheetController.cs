﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Hr.AttendanceSummaryServices;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class AllowanceSheetController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICampusService _campusService;
        private readonly IBranchService _branchService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IDepartmentService _departmentService;
        private readonly IAllowanceSheetService _allowanceSheetService;
        private List<UserMenu> _userMenu;
        public AllowanceSheetController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _campusService = new CampusService(session);
            _branchService = new BranchService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _departmentService = new DepartmentService(session);
            _allowanceSheetService = new AllowanceSheetService(session);
        }
        #endregion
        
        #region Allowance Sheet

        public ActionResult AllowanceSheet()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            try
            {
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.BranchList = new MultiSelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new MultiSelectList(new List<Campus>(), "Id", "Name");
                ViewBag.DepartmentList = new MultiSelectList(new List<Department>(), "Id", "Name");
                var t = new SelectList(_commonHelper.LoadEmumToDictionary<AllowanceSheetTeamMemberSearchType>(), "Key", "Value");
                ViewBag.EmploymentSearchOnList = t;
                ViewBag.AllowanceSheetMonthList = new SelectList(_commonHelper.LoadEmumToDictionary<MonthsOfYear>(), "Key", "Value");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult AllowanceSheet(AllowanceSheetFormViewModel allowanceSheetFormViewModel)
        {
            bool isSuccess = false;
            string message = "";
            List<AllowanceSheet> allowanceSheetList = new List<AllowanceSheet>();
            ViewBag.OrganizationFullName = "";
            ViewBag.BranchName = "";
            ViewBag.CampusName = "";
            ViewBag.DepartmentName = "";
            ViewBag.SheetName = "";
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(_userMenu, allowanceSheetFormViewModel);

                ViewBag.OrganizationFullName = (allowanceSheetFormViewModel.OrganizationId != SelectionType.SelelectAll) ? _organizationService.LoadById(allowanceSheetFormViewModel.OrganizationId).Name : "All Organization";
                ViewBag.BranchName = (allowanceSheetFormViewModel.BranchId != SelectionType.SelelectAll) ? _branchService.GetBranch(allowanceSheetFormViewModel.BranchId).Name : "All Branch ";
                ViewBag.CampusName = (allowanceSheetFormViewModel.CampusId != SelectionType.SelelectAll) ? _campusService.GetCampus(allowanceSheetFormViewModel.CampusId).Name : "All Campus ";
                ViewBag.DepartmentName = (allowanceSheetFormViewModel.DepartmentId != SelectionType.SelelectAll) ? _departmentService.LoadById(allowanceSheetFormViewModel.DepartmentId).Name : "All Department ";
                ViewBag.SheetName = "Allowance Sheet - " + _commonHelper.GetEmumIdToValue<MonthsOfYear>(allowanceSheetFormViewModel.Month) + " " + allowanceSheetFormViewModel.Year.ToString();
                isSuccess = true;
                return PartialView("Partial/_TeamMemberAllowanceSheetList", allowanceSheetList);
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                message = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return Json(new { IsSuccess = isSuccess, Message = message });
        }

        public ActionResult ExportAllowanceSheet(string allowanceSheetFormViewModelString)
        {
            AllowanceSheetFormViewModel allowanceSheetViewModel = new JavaScriptSerializer().Deserialize<AllowanceSheetFormViewModel>(allowanceSheetFormViewModelString);

            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var allowanceSheetExcelList = new List<List<object>>();
            try
            {
                var headerList = new List<string>();
                if (allowanceSheetViewModel.OrganizationId >= 0)
                {
                    string organizationFullName = (allowanceSheetViewModel.OrganizationId != SelectionType.SelelectAll) ? _organizationService.LoadById(allowanceSheetViewModel.OrganizationId).Name : "All Organization";
                    string branchName = (allowanceSheetViewModel.BranchId != SelectionType.SelelectAll) ? _branchService.GetBranch(allowanceSheetViewModel.BranchId).Name : "All Branch ";
                    string campusName = (allowanceSheetViewModel.CampusId != SelectionType.SelelectAll) ? _campusService.GetCampus(allowanceSheetViewModel.CampusId).Name : "All Campus ";
                    string departmentName = (allowanceSheetViewModel.DepartmentId != SelectionType.SelelectAll) ? _departmentService.LoadById(allowanceSheetViewModel.DepartmentId).Name : "All Department ";

                    string sheetName = "Allowance Sheet - " + _commonHelper.GetEmumIdToValue<MonthsOfYear>(allowanceSheetViewModel.Month) + " " + allowanceSheetViewModel.Year.ToString();

                    headerList.Add(organizationFullName);
                    headerList.Add(branchName);
                    headerList.Add(campusName);
                    headerList.Add(departmentName);
                    headerList.Add("");
                    headerList.Add("");
                    headerList.Add("");
                    headerList.Add(sheetName);
                }
                var columnList = new List<string> {
                                    "PIN"
                                    , "Name"
                                    , "Designation"
                                    , "Daily Support Allowance"
                                    , "Holiday Allowance"
                                    , "Overtime Allowance"
                                    , "NightWork Allowance"
                                    , "Children Education Allowance"
                                    , "Special Allowance"
                                    , "Zakat"
                                    , "CSR Allowance"
                                    , "Total Allowance"
                                };

                int start = 0;

                #region datatable

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(_userMenu, allowanceSheetViewModel);

                var footerList = new List<string>();
                footerList.Add("");
                decimal dailySupportAllowanceTotalAmount = 0;
                decimal hodidayAllowanceTotalAmount = 0;
                decimal overtimeAllowanceTotalAmount = 0;
                decimal nightWorkAllowanceTotalAmount = 0;
                decimal childrenEducationAllowanceTotalAmount = 0;
                decimal specialAllowanceTotalAmount = 0;
                decimal zakatTotalAmount = 0;
                decimal csrAllowanceTotalAmount = 0;
                decimal grandTotalAllowanceAmount = 0;

                foreach (AllowanceSheet allowanceSheet in allowanceSheetList)
                {
                    var xlsRow = new List<object>();
                    dailySupportAllowanceTotalAmount += allowanceSheet.DailySupportAllowanceAmount;
                    hodidayAllowanceTotalAmount += allowanceSheet.ExtradayAllowanceAmount;
                    overtimeAllowanceTotalAmount += allowanceSheet.OvertimeAllowanceAmount;
                    nightWorkAllowanceTotalAmount += allowanceSheet.NightWorkAllowanceAmount;
                    childrenEducationAllowanceTotalAmount += allowanceSheet.ChildrenEducationAllowanceAmount;
                    specialAllowanceTotalAmount += allowanceSheet.SpecialAllowanceAmount;
                    zakatTotalAmount += allowanceSheet.ZakatAllowanceAmount;
                    csrAllowanceTotalAmount += allowanceSheet.CsrAllowanceAmount;
                    grandTotalAllowanceAmount += allowanceSheet.TotalAllowanceAmount;

                    #region datatable
                    xlsRow.Add(allowanceSheet.TeamMember.Pin.ToString());
                    xlsRow.Add(allowanceSheet.TeamMember.Name);
                    xlsRow.Add(allowanceSheet.TeamMemberDesignation.Name);
                    xlsRow.Add(allowanceSheet.DailySupportAllowanceAmount);
                    xlsRow.Add(allowanceSheet.ExtradayAllowanceAmount);
                    xlsRow.Add(allowanceSheet.OvertimeAllowanceAmount);
                    xlsRow.Add(allowanceSheet.NightWorkAllowanceAmount);
                    xlsRow.Add(allowanceSheet.ChildrenEducationAllowanceAmount);
                    xlsRow.Add(allowanceSheet.SpecialAllowanceAmount);
                    xlsRow.Add(allowanceSheet.ZakatAllowanceAmount);
                    xlsRow.Add(allowanceSheet.CsrAllowanceAmount);
                    xlsRow.Add(allowanceSheet.TotalAllowanceAmount);
                    allowanceSheetExcelList.Add(xlsRow);
                    #endregion
                }
                if (allowanceSheetList.Any())
                {
                    var xlsRow2 = new List<object>();
                    xlsRow2.Add("");
                    xlsRow2.Add("Sub Total");
                    xlsRow2.Add("");
                    xlsRow2.Add(dailySupportAllowanceTotalAmount.ToString());
                    xlsRow2.Add(hodidayAllowanceTotalAmount.ToString());
                    xlsRow2.Add(overtimeAllowanceTotalAmount.ToString());
                    xlsRow2.Add(nightWorkAllowanceTotalAmount.ToString());
                    xlsRow2.Add(childrenEducationAllowanceTotalAmount.ToString());
                    xlsRow2.Add(specialAllowanceTotalAmount.ToString());
                    xlsRow2.Add(zakatTotalAmount.ToString());
                    xlsRow2.Add(csrAllowanceTotalAmount.ToString());
                    xlsRow2.Add(grandTotalAllowanceAmount.ToString());
                    allowanceSheetExcelList.Add(xlsRow2);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, allowanceSheetExcelList, footerList, "Allowance-sheet__" + _commonHelper.GetEmumIdToValue<MonthsOfYear>(allowanceSheetViewModel.Month) + "_" + allowanceSheetViewModel.Year.ToString());
                return View("AutoClose");

                #endregion
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        }

        [HttpPost]
        public JsonResult SaveAllowanceSheet(AllowanceSheetFormViewModel allowanceSheetFormViewModel)
        {
            bool isSuccess = false;
            string message = "";
            try
            {
                _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(_userMenu, allowanceSheetFormViewModel, true);
                int count = allowanceSheetList.Count(x => x.IsFinalSubmit = true && x.Id != 0);
                _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList);
                isSuccess = true;
                message = "Total " + (allowanceSheetList.Count - count) + " Allowance Sheet(s) Saved Successfully ";
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new { IsSuccess = isSuccess, Message = message });
        }

        #endregion

    }
}