﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Common;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class CommonFactory
    {
        private ISession _session;
        private readonly IAddressService _addressService;
        private readonly IExamService _examService;
        private readonly IHrBoardService _hrBoardService;

        public CommonFactory(IObjectFactoryBase caller, ISession session)
        {
            _session = session;
            _addressService = new AddressService(session);
            _examService = new ExamService(session);
            _hrBoardService = new HrBoardService(session);
        }

        public Postoffice WithPostOffice()
        {
            var postOffices = _addressService.LoadPostOffice();
            if (postOffices != null && postOffices.Count > 0)
            {
                Random random = new Random();
                var rnd = random.Next(postOffices.Count - 1);
                return postOffices[rnd];
            }
            return null;
        }

        public Postoffice WithPostOffice(List<long> divisionIds = null, List<long> districtIds = null,
            List<long> thanaIds = null)
        {
            var postOffices = _addressService.LoadPostOffice(divisionIds, districtIds);
            if (postOffices != null && postOffices.Count > 0)
            {
                Random random = new Random();
                var rnd = random.Next(postOffices.Count - 1);
                return postOffices[rnd];
            }
            return null;
        }

        public Thana WithThana()
        {
            var thana = _addressService.LoadThana();
            if (thana != null && thana.Count > 0)
            {
                Random random = new Random();
                var rnd = random.Next(thana.Count - 1);
                return thana[rnd];
            }
            return null;
        }

        public Thana WithThana(List<long> divisionIds = null, List<long> districtIds = null)
        {
            var thana = _addressService.LoadThana(divisionIds, districtIds);
            if (thana != null && thana.Count > 0)
            {
                Random random = new Random();
                var rnd = random.Next(thana.Count - 1);
                return thana[rnd];
            }
            return null;
        }

        public District WithDistrict()
        {
            var district = _addressService.LoadDistrict();
            if (district != null && district.Count > 0)
            {
                Random random = new Random();
                var rnd = random.Next(district.Count - 1);
                return district[rnd];
            }
            return null;
        }

        public District WithDistrict(List<long> divisionIds)
        {
            var district = _addressService.LoadDistrict(divisionIds);
            if (district != null && district.Count > 0)
            {
                Random random = new Random();
                var rnd = random.Next(district.Count - 1);
                return district[rnd];
            }
            return null;
        }

        public Division WithDivision()
        {
            var division = _addressService.LoadDivision();
            if (division != null && division.Count > 0)
            {
                Random random = new Random();
                var rnd = random.Next(division.Count - 1);
                return division[rnd];
            }
            return null;
        }

        public AcademicExam WithAcademicExam()
        {
            var examList = _examService.LoadExamList();
            if (examList != null && examList.Count > 0)
            {
                Random random = new Random();
                var rnd = random.Next(examList.Count - 1);
                return examList[rnd];
            }
            return null;
        }

        public Board WithBoard()
        {
            var boardList = _hrBoardService.LoadBoard();
            if (boardList != null && boardList.Count > 0)
            {
                Random random = new Random();
                var rnd = random.Next(boardList.Count - 1);
                return boardList[rnd];
            }
            return null;
        }
    }
}
