﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Axzkemkeeper;
using DeviceSynchronizer.Model;
using log4net;
using Newtonsoft.Json;
using DeviceSynchronizer.Model;
namespace DeviceSynchronizer.Helper
{
    public static class DbHelper
    {
        private static readonly ILog Log = LogManager.GetLogger("HelperLogger");
        public const string DatabaseName = "OslErp_Attendance.db3";
        internal const string DbPassword = "FX43PvGE";

        //If no database exists at client computer then create a new database
        public static bool CreateDb()
        {
            try
            {
                var source = Environment.CurrentDirectory + "\\" + DatabaseName;
                if (!File.Exists(source))
                {
                    SQLiteConnection.CreateFile(source);
                    using (var sqliteConnection = new SQLiteConnection("Data Source=" + source + ";Version=3;"))
                    {
                        //sqliteConnection.SetPassword(DbPassword);
                        sqliteConnection.Open();
                        var command = new SQLiteCommand();
                        command.Connection = sqliteConnection;

                        //create attendancelog table
                        string attendanceLogsql = @"CREATE TABLE [AttendanceLog] 
                                                    (
                                                         [Id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT
                                                        ,[EnrollNo] INT  NOT NULL
                                                        ,[PunchType] INT  NOT NULL
                                                        ,[PunchTime]  VARCHAR(50) NOT NULL
                                                        ,[DeviceId] INTEGER  NOT NULL
                                                    )";
                        command.CommandText = attendanceLogsql;
                        command.ExecuteNonQuery();

                        sqliteConnection.Close();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw ex;
            }
        }

        public static SQLiteConnection OpenDbConnection()
        {
            //var dbConnection = "data source= " + System.Environment.CurrentDirectory + "\\" + DbHelper.DatabaseName + ";Version=3;Password=" + DbHelper.DbPassword + ";";
            var dbConnection = "data source= " + System.Environment.CurrentDirectory + "\\" + DbHelper.DatabaseName + ";Version=3;";
            try
            {
                var sql_con = new SQLiteConnection(dbConnection);
                sql_con.Open();
                return sql_con;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
    }
}
