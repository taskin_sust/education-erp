using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class WorkOrderDetailsFactory : ObjectFactoryBase<WorkOrderDetails>
    {

        private readonly IWorkOrderDetailService _workOrderDetailService;
        internal WorkOrderCriteriaFactory _workOrderCriteriaFactory;

        public WorkOrderDetailsFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _workOrderDetailService = new WorkOrderDetailsService(session);
            _workOrderCriteriaFactory = new WorkOrderCriteriaFactory(null,session);
        }

        #region Create

        public WorkOrderDetailsFactory Create()
        {
            Object = new WorkOrderDetails()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                TotalCost = 123,
                Purpose = (int)Purpose.ProgramPurpose,
                OrderQuantity = 500,
                UnitCost = 2
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public WorkOrderDetailsFactory Create(Program program,Session session, Item item,WorkOrder workOrder)
        {
            var workOrderDetails = new WorkOrderDetails()
            {
                Item = item,
                Program = program,
                Session = session,
                WorkOrder = workOrder,
                Purpose = (int)Purpose.ProgramPurpose,
                OrderQuantity = 15,
                UnitCost = 10,
                TotalCost = 150
            };
            var workOrderCriteriaList = new List<WorkOrderCriteria>()
            {
                new WorkOrderCriteria()
                {
                    Criteria = "Brand",
                    CriteriaValue = "Bashundhara",
                    WorkOrderDetail = workOrderDetails
                },
                new WorkOrderCriteria()
                {
                    Criteria = "Amount",
                    CriteriaValue = "10",
                    WorkOrderDetail = workOrderDetails
                }
            };
            workOrderDetails.WorkOrderCriteriaList = workOrderCriteriaList;
            SingleObjectList.Add(Object);
            return this;
        }

        public WorkOrderDetailsFactory WithItem(Item item)
        {
            Object.Item = item;
            return this;
        }

        public WorkOrderDetailsFactory WithProgram(Program program)
        {
            Object.Program = program;
            return this;
        }

        public WorkOrderDetailsFactory WithSession(Session session)
        {
            Object.Session = session;
            return this;
        }

        public WorkOrderDetailsFactory WithPurposeOfProgramAndSession(Program program,Session session)
        {
            Object.PurposeProgram = program;
            Object.PurposeSession = session;
            return this;
        }

        public WorkOrderDetailsFactory WithWorkOrder(WorkOrder workOrder)
        {
            Object.WorkOrder = workOrder;
            return this;
        }

        public WorkOrderDetailsFactory WithWorkOrderCriteria(WorkOrderDetails workOrderDetails)
        {
            _workOrderCriteriaFactory.Create().WithWorkOrderDetails(workOrderDetails);
            Object.WorkOrderCriteriaList = _workOrderCriteriaFactory.SingleObjectList;
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public WorkOrderDetailsFactory CleanUp()
        {
            _workOrderCriteriaFactory.CleanUp();
            DeleteAll();
            return this;
        }

        #endregion

    }
}
