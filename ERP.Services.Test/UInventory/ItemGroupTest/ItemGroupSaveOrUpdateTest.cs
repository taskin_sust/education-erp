﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.UInventory.ItemGroupTest;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.ItemGroupTest
{
    interface IItemGroupSaveOrUpdateTest
    {
        void Save_Should_Return_Null_Exception();
        void Should_Save_Successfully();
        void Should_Update_Successfully();
        void Save_Should_Check_Duplicate_Name_Entry();
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "ItemGroup")]
    public class ItemGroupSaveOrUpdateTest : ItemGroupBaseTest,IItemGroupSaveOrUpdateTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _itemGroupService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var itemGroupFactory = _itemGroupFactory.Create().Persist();
            ItemGroup itemGroup = _itemGroupService.LoadById(itemGroupFactory.Object.Id);
            Assert.NotNull(itemGroup);
            Assert.True(itemGroup.Id > 0);
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var itemGroupFactory = _itemGroupFactory.Create().Persist();
            var itemGroupFactory2 = _itemGroupFactory.CreateWith(itemGroupFactory.Object.Id, _name+"Update", ItemGroup.EntityStatus.Active);
             _itemGroupService.SaveOrUpdate(itemGroupFactory2.Object);
             Assert.NotEqual(0, _itemGroupService.LoadById(itemGroupFactory2.Object.Id).Id);
        }

        [Fact]
        public void Save_Should_Check_Duplicate_Name_Entry()
        {
            var itemGroupFactory1 = _itemGroupFactory.Create().Persist();
            var itemGroupFactory2 = _itemGroupFactory.CreateWith(0, itemGroupFactory1.Object.Name,ItemGroup.EntityStatus.Active);
            Assert.Throws<DuplicateEntryException>(() => itemGroupFactory2.Persist());
        }

        #endregion
    }
}
