﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DailySupportAllowanceTest
{
    public interface IDailySupportAllowanceOperationalTest
    {
        void Should_Return_Invalid_Permission();
        void Should_Return_Invalid_Object();
        void Should_Return_Invalid_Organization();
        void Should_Return_Invalid_Designation();
        void Should_Return_Invalid_Amount();
        void Should_Return_Invalid_DateTime();
        void Should_Save_Successfully();
        void Should_Return_Duplicate_Entry_Exception();
        void Should_Return_MessageException_For_Generated_Allowance_Sheet();

    }

    [Trait("Area", "Hr")]
    [Trait("Service", "DailySupportAllowanceService")]
    public class DailySupportAllowanceSaveTest : DailySupportAllowanceBaseTest, IDailySupportAllowanceOperationalTest
    {
        #region Save validation check

        [Fact]
        public void Should_Return_Invalid_Permission()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            Assert.Throws<InvalidDataException>(() => _dailySupportAllowanceService.SaveOrUpdate(null, _dailySupportAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_Object()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            Assert.Throws<NullObjectException>(() => _dailySupportAllowanceService.SaveOrUpdate(menu, null));
        }

        [Fact]
        public void Should_Return_Invalid_Organization()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceSettingFactory.Object.Organization = null;
            Assert.Throws<InvalidDataException>(() => _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_Designation()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceSettingFactory.Object.Designation = null;
            Assert.Throws<InvalidDataException>(() => _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_Amount()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceSettingFactory.Object.Amount = 0;
            Assert.Throws<InvalidDataException>(() => _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_DateTime()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceSettingFactory.Object.EffectiveDate = DateTime.Now.AddDays(7);
            _dailySupportAllowanceSettingFactory.Object.ClosingDate = DateTime.Now;
            Assert.Throws<InvalidDataException>(() => _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object));
        }


        #endregion

        #region Validation Check AllowanceSheet

        [Fact]
        public void Should_Check_Empty_Department()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _dailySupportAllowanceSettingFactory.CreateWith(new DateTime(2016, 01, 01), new DateTime(2020, 01, 01))
                .WithOrgAndDesignation(org, member.EmploymentHistory[0].Designation).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 2,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { new AttendanceAdjustment()
            {
                AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved, Date = new DateTime(2016, 2, 1, 9, 0, 0), EndTime = new DateTime(2016, 2, 1, 18, 0, 0),TeamMember = member,
                StartTime = new DateTime(2016, 2, 1, 9, 0, 0),Reason = "Office Work"
            } });
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            allowanceSheetList[0].AllowanceSheetFirstDetails[0].TeamMemberDepartment = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_Check_Empty_Designation()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _dailySupportAllowanceSettingFactory.CreateWith(new DateTime(2016, 01, 01), new DateTime(2020, 01, 01))
                .WithOrgAndDesignation(org, member.EmploymentHistory[0].Designation).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 2,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { new AttendanceAdjustment()
            {
                AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved, Date = new DateTime(2016, 2, 1, 9, 0, 0), EndTime = new DateTime(2016, 2, 1, 18, 0, 0),TeamMember = member,
                StartTime = new DateTime(2016, 2, 1, 9, 0, 0),Reason = "Office Work"
            } });
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            allowanceSheetList[0].AllowanceSheetFirstDetails[0].TeamMemberDesignation = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_Check_Empty_Branch()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _dailySupportAllowanceSettingFactory.CreateWith(new DateTime(2016, 01, 01), new DateTime(2020, 01, 01))
                .WithOrgAndDesignation(org, member.EmploymentHistory[0].Designation).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 2,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { new AttendanceAdjustment()
            {
                AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved, Date = new DateTime(2016, 2, 1, 9, 0, 0), EndTime = new DateTime(2016, 2, 1, 18, 0, 0),TeamMember = member,
                StartTime = new DateTime(2016, 2, 1, 9, 0, 0),Reason = "Office Work"
            } });
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            allowanceSheetList[0].AllowanceSheetFirstDetails[0].TeamMemberBranch = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_Check_Empty_Campus()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _dailySupportAllowanceSettingFactory.CreateWith(new DateTime(2016, 01, 01), new DateTime(2020, 01, 01))
                .WithOrgAndDesignation(org, member.EmploymentHistory[0].Designation).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 2,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { new AttendanceAdjustment()
            {
                AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved, Date = new DateTime(2016, 2, 1, 9, 0, 0), EndTime = new DateTime(2016, 2, 1, 18, 0, 0),TeamMember = member,
                StartTime = new DateTime(2016, 2, 1, 9, 0, 0),Reason = "Office Work"
            } });
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            allowanceSheetList[0].AllowanceSheetFirstDetails[0].TeamMemberCampus = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

     #endregion

        #region Invalid Property Check


        #endregion

        #region Authorization Check

        [Fact]
        public void Should_Return_UnAuthorized_Menu_Message()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { _organizationFactory.Create().Persist().Object }, null, null);
            _dailySupportAllowanceSettingFactory.CreateWith(new DateTime(2016, 01, 01), new DateTime(2020, 01, 01))
                .WithOrgAndDesignation(org, member.EmploymentHistory[0].Designation).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 2,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { new AttendanceAdjustment()
            {
                AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved, Date = new DateTime(2016, 2, 1, 9, 0, 0), EndTime = new DateTime(2016, 2, 1, 18, 0, 0),TeamMember = member,
                StartTime = new DateTime(2016, 2, 1, 9, 0, 0),Reason = "Office Work"
            } });
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel));
        }

        #endregion

        #region Business Logic

        [Fact]
        public void Should_Save_Successfully()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object);
            Assert.True(_dailySupportAllowanceSettingFactory.Object.Id > 0);
        }

        [Fact]
        public void Should_Return_Duplicate_Entry_Exception()
        {
            _organizationFactory.Create().Persist();
            programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _designationFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(programFactory.Object));
            _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object);
            _dailySupportAllowanceSettingFactory.Create().WithOrgAndDesignation(_organizationFactory.Object, _designationFactory.Object);
            Assert.Throws<InvalidDataException>(() => _dailySupportAllowanceService.SaveOrUpdate(menu, _dailySupportAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_MessageException_For_Generated_Allowance_Sheet()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _dailySupportAllowanceSettingFactory.CreateWith(new DateTime(2015, 01, 01), new DateTime(2020, 01, 01))
                .WithOrgAndDesignation(org, member.EmploymentHistory[0].Designation).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { new AttendanceAdjustment()
            {
                AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved, 
                Date = new DateTime(2016, 1, 1).Date, 
                StartTime = new DateTime(2016, 1, 1, 9, 0, 0),
                EndTime = new DateTime(2016, 1, 1, 18, 0, 0),
                TeamMember = member,
                Reason = "Office Work"
            } });
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList);
            _dailySupportAllowanceSettingFactory.CreateWith(new DateTime(2016, 01, 01), new DateTime(2019, 01, 01))
                .WithOrgAndDesignation(org, member.EmploymentHistory[0].Designation);
            var obj = _dailySupportAllowanceSettingFactory.SingleObjectList[1];
            Assert.Throws<MessageException>(() => _dailySupportAllowanceService.SaveOrUpdate(menus, obj));
        }

        [Fact]
        public void Should_Return_MessageException_ForUpdate_Generated_Allowance_Sheet()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _dailySupportAllowanceSettingFactory.CreateWith(new DateTime(2016, 01, 01), new DateTime(2020, 01, 01))
                .WithOrgAndDesignation(org, member.EmploymentHistory[0].Designation).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 2,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { new AttendanceAdjustment()
            {
                AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved, Date = new DateTime(2016, 2, 1, 9, 0, 0), EndTime = new DateTime(2016, 2, 1, 18, 0, 0),TeamMember = member,
                StartTime = new DateTime(2016, 2, 1, 9, 0, 0),Reason = "Office Work"
            } });
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList);
            _dailySupportAllowanceSettingFactory.Object.Designation = _designationFactory.Create().WithRank(new Random().Next(100)).WithOrganization(org).Persist().Object;
            _dailySupportAllowanceSettingFactory.Object.ClosingDate = new DateTime(2018, 01, 01);
            Assert.Throws<InvalidDataException>(() => _dailySupportAllowanceService.SaveOrUpdate(menus, _dailySupportAllowanceSettingFactory.Object));
        }

        #endregion

    }
}
