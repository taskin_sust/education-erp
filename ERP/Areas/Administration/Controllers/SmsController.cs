﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using log4net;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.com.onnorokomsms.api2;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Sms;
using UdvashERP.Services.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;
using UdvashERP.BusinessModel.Dto;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class SmsController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        private readonly ILog _smsLogger = LogManager.GetLogger("SMSHelper");


        private void LogError(string message, Exception exception)
        {
            if (exception != null)
            {
                _logger.Error(message, exception);
                // _smsLogger.Error(message, exception);
            }
            else
            {
                _logger.Error(message);
                //_smsLogger.Error(message);
            }
        }

        #endregion

        #region Objects/Propertise/Services/Dao

        private readonly ICommonHelper _commonHelper;
        private readonly ISmsService _smsService;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISmsMaskService _smsMaskService;
        private readonly IUserService _userService;


        private List<UserMenu> _userMenu;

        #endregion

        #region Initialize

        public SmsController()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _smsService = new SmsService(nHsession);
            _organizationService = new OrganizationService(nHsession);
            _programService = new ProgramService(nHsession);
            _smsMaskService = new SmsMaskService(nHsession);
            _userService = new UserService(nHsession);
        }

        #endregion

        #region Send Pending SMS
        [AllowAnonymous]
        public ActionResult SendPendingSms()
        {
            //_logger.Error("Step 1");
            if (System.Web.HttpContext.Current.Application["RunningShcedule"] == null)
            {
                //_logger.Error("Step 2");
                System.Web.HttpContext.Current.Application["RunningShcedule"] = "Administration/Sms/SendPendingSms(" + DateTime.Now + ")";
                //_logger.Error((string)System.Web.HttpContext.Current.Application["RunningShcedule"]);
                try
                {

                    int maxPendingSms = Convert.ToInt32(ConfigurationManager.AppSettings["MaxPendingSMS"]);
                    int maxSendingTrySMS = Convert.ToInt32(ConfigurationManager.AppSettings["MaxSendingTrySMS"]);
                    // _logger.Error("maxPendingSms:" + maxPendingSms + " maxSendingTrySMS : " + maxSendingTrySMS);
                    IList<SmsHistory> smsHistoryList = _smsService.LoadSmsHistory(SmsHistory.EntityStatus.Pending, maxPendingSms, maxSendingTrySMS);
                    List<SmsHistory> newSmsHistoryList = new List<SmsHistory>();
                    var organization = new Organization();
                    string mask = "";
                    //_logger.Error("Step 3: Total SMS " + smsHistoryList.Count);
                    if (smsHistoryList.Any())
                    {
                        //var smsHistoryListGroup = smsHistoryList.GroupBy(m => m.Program.Organization, (key, group) => new { Key = key, Group = group.ToList() });
                        var smsHistoryListGroup = smsHistoryList.GroupBy(m => m.Organization, (key, group) => new { Key = key, Group = group.ToList() });
                        foreach (var shlg in smsHistoryListGroup)
                        {
                            organization = shlg.Key;
                            // smsHistoryList = shlg.Group;
                            // _logger.Error("Step 31: Organization: " + organization.Name);
                            var maskWizeGroup = shlg.Group.GroupBy(m => m.Mask, (key, group) => new { Key = key, Group = group.ToList() });

                            foreach (var mwg in maskWizeGroup)
                            {
                                mask = mwg.Key;
                                //_logger.Error("Step 32: Mask: " + mask);
                                newSmsHistoryList = mwg.Group;
                                break;
                            }
                            break;
                        }

                    }
                    //_logger.Error("Step 4: Total SMS " + newSmsHistoryList.Count);
                    if (newSmsHistoryList.Any())
                    {
                        for (int i = 0; i < newSmsHistoryList.Count; i++)
                        {
                            int sendingTryCount = newSmsHistoryList[i].SendingTryCount;
                            sendingTryCount = sendingTryCount + 1;
                            newSmsHistoryList[i].SendingTryCount = sendingTryCount;
                        }
                        //foreach (var newSmsHistory in newSmsHistoryList)
                        //{
                        //    int sendingTryCount = newSmsHistory.SendingTryCount;
                        //    sendingTryCount = sendingTryCount + 1;
                        //    newSmsHistory.SendingTryCount = sendingTryCount;

                        //}
                        Task task = new Task(() =>
                        {
                            if (_smsService != null)
                                _smsService.UpdateHistoryList(newSmsHistoryList);
                        });
                        task.Start();
                        Task.WaitAll(task);
                        DeliverOneToOneBulk(organization, newSmsHistoryList, mask);
                    }
                    System.Web.HttpContext.Current.Application.Remove("RunningShcedule");
                    //HttpContext.Cache.Remove("RunningShcedule");
                }
                catch (Exception ex)
                {

                    System.Web.HttpContext.Current.Application.Remove("RunningShcedule");
                    _logger.Error(ex);
                    //throw;
                }
            }
            else
            {
                var running = "Another Schedule is running.";
                try
                {
                    running = ", Schedule name: " + (string)System.Web.HttpContext.Current.Application["RunningShcedule"];
                }
                catch (Exception ex)
                {
                    _logger.Error("RunningShcedule Exception");
                }

                _logger.Error(running);
            }
            return View();
        }

        private void DeliverOneToOneBulk(Organization organization, List<SmsHistory> smsHistoryList, string mask = "")
        {
            try
            {
                if (smsHistoryList.Any() && organization != null)
                {
                    var messageHeader = new MessageHeader();
                    messageHeader.MarskText = mask;
                    messageHeader.UserName = organization.SmsApiUserName;
                    messageHeader.UserPassword = organization.SmsApiPassword;
                    messageHeader.CampingName = "";
                    WsSms[] wsSmsesList = new WsSms[smsHistoryList.Count];
                    int i = 0;
                    foreach (var smsHistory in smsHistoryList)
                    {
                        var wsSms = new WsSms
                        {
                            MobileNumber = smsHistory.ReceiverNumber,
                            SmsText = smsHistory.Sms,
                            Type = "TEXT",

                        };
                        wsSmsesList[i++] = wsSms;
                    }
                    IList<SmsHistory> historyList = new List<SmsHistory>();
                    if (wsSmsesList.Any())
                    {
                        var sendSms = new SendSms();
                        var returnResult = sendSms.OneToOneBulk(messageHeader, wsSmsesList);
                        var returnArray = returnResult.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var rMessagePerMobile in returnArray)
                        {
                            try
                            {
                                var exploded = rMessagePerMobile.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                                var receiverNumberfrmSplit = "88" + exploded[1];
                                var currentObj = smsHistoryList.FirstOrDefault(r => r.ReceiverNumber == receiverNumberfrmSplit && r.Status == SmsHistory.EntityStatus.Pending);
                                if (currentObj != null)
                                {
                                    currentObj.ErrorCode = exploded[0];
                                    if (exploded.Length == 3 && currentObj.ErrorCode == "1900")
                                    {
                                        if (!string.IsNullOrEmpty(exploded[2]))
                                        {
                                            currentObj.ResponseCode = exploded[2];
                                        }
                                        currentObj.Status = SmsHistory.EntityStatus.Active;
                                    }
                                    else
                                    {
                                        currentObj.Status = SmsHistory.EntityStatus.Pending;
                                    }
                                    historyList.Add(currentObj);
                                }
                            }
                            catch (Exception ex)
                            {
                                _smsLogger.Error(ex);
                            }
                        }

                        Task task = new Task(() =>
                        {
                            if (_smsService != null)
                                _smsService.UpdateHistoryList(historyList);
                        });
                        task.Start();
                        Task.WaitAll(task);
                    }
                }
            }
            catch (Exception ex)
            {
                _smsLogger.Error(ex);
            }
        }
        #endregion

        #region Send Schedule SMS
        [AllowAnonymous]
        public ActionResult SendScheduleSms()
        {
            System.Web.HttpContext.Current.Application.Remove("RunningShcedule");
            if (System.Web.HttpContext.Current.Application["RunningShcedule"] == null)
            {
                System.Web.HttpContext.Current.Application["RunningShcedule"] = "Administration/Sms/SendScheduleSms(" +
                                                                                DateTime.Now + ")";
                try
                {
                   //IList<SmsHistory> smsHistories = _smsService.SendScheduleSmsNew();
                    long smsUserId = Convert.ToInt64(ConfigurationManager.AppSettings["SmsUserId"]);
                    IList<SmsHistory> smsHistoryList = _smsService.SendScheduleSmsNew(smsUserId); 


                   //if (smsDtoList.Any())
                   //{
                   //    foreach (var smsDto in smsDtoList)
                   //    {
                   //        bool flag = SendSmsApi.GenarateScheduleSmsAndNumber(smsDto.SmsSettings, smsDto.Student, smsDto.StudentProgram);
                   //    }
                   //}
                    bool isSucess = _smsService.SaveSmsHistoryListAndDeleteTemporarySmsData(smsHistoryList, null, null);
                    System.Web.HttpContext.Current.Application.Remove("RunningShcedule");
                }
                catch (Exception ex)
                {
                    System.Web.HttpContext.Current.Application.Remove("RunningShcedule");
                    _logger.Error(ex);
                }
            }
            else
            {
                var running = "Another Schedule is running.";
                try
                {
                    running = ", Schedule name: " + (string)System.Web.HttpContext.Current.Application["RunningShcedule"];
                }
                catch (Exception ex)
                {
                    _logger.Error("RunningShcedule Exception");
                }

                _logger.Error(running);
            }
            return View();
        }

        #endregion

        #region SmsSettings
        public ActionResult SmsSettings()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //ViewBag.OrganizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            List<SelectListItem> _list = new List<SelectListItem>();
            ViewBag.ProgramList = new SelectList(_list, "Value", "Text");
            ViewBag.SmsTypeList = new SelectList(_smsService.LoadSmsTypeActive(), "Id", "Name");
            ViewBag.SmsReceiverList = new SelectList(_smsService.LoadSmsReceiverActive(), "Id", "Name");
            ViewBag.DynamicOptionsList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.SmsMaskName = new SelectList(new List<SmsMask>(), "Name", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SmsSettings(SmsSettings smsSetting, long[] SmsReceivers, string SmsTime_Hour, string SmsTime_Time)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //ViewBag.OrganizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            List<SelectListItem> _list = new List<SelectListItem>();
            //_list.Insert(0, new SelectListItem() { Value = "All", Text = "All" });
            ViewBag.ProgramList = new SelectList(_list, "Value", "Text");
            ViewBag.SmsMaskName = new SelectList(new List<SmsMask>(), "Name", "Name");
            ViewBag.SmsTypeList = new SelectList(_smsService.LoadSmsTypeActive(), "Id", "Name", smsSetting.SmsType.Id);
            ViewBag.SmsReceiverList = new SelectList(_smsService.LoadSmsReceiverActive(), "Id", "Name");
            ViewBag.DynamicOptionsList = new SelectList(_smsService.LoadSmsTypeById(smsSetting.SmsType.Id).DynamicOptions.ToList(), "Id", "Name");
            try
            {
                Organization organization = _organizationService.LoadById(smsSetting.Organization.Id);
                if (organization != null)
                {
                    SelectList plist = new SelectList(_smsService.LoadAuthorizedProgram(_userMenu, organization.Id), "Id", "Name", smsSetting.Program.Id);
                    List<SelectListItem> _list2 = plist.ToList();
                    _list2.Insert(0, new SelectListItem() { Value = "All", Text = "All" });
                    ViewBag.ProgramList = new SelectList(_list2, "Value", "Text");

                    smsSetting.Organization = organization;
                }
                else smsSetting.Organization = null;

                Program program = _smsService.LoadProgramById(smsSetting.Program.Id);
                if (program != null) smsSetting.Program = program;
                else smsSetting.Program = null;

                SmsType smsType = _smsService.LoadSmsTypeById(smsSetting.SmsType.Id);
                if (smsType != null) smsSetting.SmsType = smsType;

                if (SmsReceivers.Length > 0)
                {
                    if (SmsTime_Hour != null && SmsTime_Hour != "" && SmsTime_Time != null && SmsTime_Time != "")
                    {
                        smsSetting.SmsTime = new DateTime(2000, 01, 01, Convert.ToInt16(SmsTime_Hour), Convert.ToInt16(SmsTime_Time), 00);
                    }
                    _smsService.Save(smsSetting, SmsReceivers);
                    ViewBag.SuccessMessage = "SMS Template Added Successfully";
                    ModelState.Clear();
                    return View(new SmsSettings());
                }
            }
            catch (DuplicateEntryException d)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                //_logger.Error("SMS Template can't be saved  due to " + d.Message);

                Console.WriteLine("{0}", d.Message);
                ViewBag.ErrorMessage = d.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                ViewBag.ErrorMessage = "SMS Template Added Fail";
            }

            return View(smsSetting);
        }
        #endregion

        #region Update SmsSettings
        public ActionResult UpdateSmsSettings(long id)
        {
            ViewBag.SmsMaskName = new SelectList(new List<SmsMask>(), "Name", "Name");
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                SmsSettings smsSetting = _smsService.LoadSmsSettingsById(id);

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.SmsReceiverList = new SelectList(_smsService.LoadSmsReceiverActive(), "Id", "Name", smsSetting.SmsReceivers);
                ViewBag.DynamicOptionsList = new SelectList(_smsService.LoadSmsTypeById(smsSetting.SmsType.Id).DynamicOptions, "Id", "Name");
                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                ViewBag.SmsMaskName = new SelectList(_smsMaskService.LoadSmsMask(smsSetting.Organization != null ? _commonHelper.ConvertIdToList(smsSetting.Organization.Id) : null, 1), "Name", "Name");
                return View(smsSetting);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSmsSettings(SmsSettings smsSetting, long[] SmsReceivers, string SmsTime_Hour, string SmsTime_Time)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.SmsReceiverList = new SelectList(_smsService.LoadSmsReceiverActive(), "Id", "Name", smsSetting.SmsReceivers);
            ViewBag.DynamicOptionsList = new SelectList(_smsService.LoadSmsTypeById(smsSetting.SmsType.Id).DynamicOptions, "Id", "Name");
            ViewBag.SmsMaskName = new SelectList(new List<SmsMask>(), "Name", "Name");
            ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
            try
            {
                SmsSettings smsSettingUpdate = _smsService.LoadSmsSettingsById(smsSetting.Id);
                ViewBag.SmsMaskName = new SelectList(_smsMaskService.LoadSmsMask(smsSettingUpdate.Organization != null ? _commonHelper.ConvertIdToList(smsSettingUpdate.Organization.Id) : null, 1), "Name", "Name");

                if (smsSetting.Program.Id != 0)
                {
                    Program program = _smsService.LoadProgramById(smsSettingUpdate.Program.Id);
                    smsSetting.Program = program;
                }
                else smsSetting.Program = null;


                SmsType smsType = _smsService.LoadSmsTypeById(smsSettingUpdate.SmsType.Id);
                if (smsType != null) smsSettingUpdate.SmsType = smsType;


                smsSettingUpdate.MaskName = smsSetting.MaskName;
                smsSettingUpdate.Template = smsSetting.Template;
                smsSettingUpdate.Status = smsSetting.Status;

                smsSettingUpdate.DayBefore = smsSetting.DayBefore;
                smsSettingUpdate.DayAfter = smsSetting.DayAfter;
                smsSettingUpdate.IsRepeat = smsSetting.IsRepeat;
                smsSettingUpdate.RepeatDuration = smsSetting.RepeatDuration;
                if (SmsTime_Hour != null && SmsTime_Hour != "" && SmsTime_Time != null && SmsTime_Time != "")
                {
                    smsSettingUpdate.SmsTime = new DateTime(2000, 01, 01, Convert.ToInt16(SmsTime_Hour), Convert.ToInt16(SmsTime_Time), 00);
                }

                if (SmsReceivers.Length > 0)
                {
                    _smsService.Update(smsSettingUpdate, SmsReceivers);
                    SmsSettings smsSettingafterUpdate = _smsService.LoadSmsSettingsById(smsSettingUpdate.Id);
                    ViewBag.SuccessMessage = "SMS Template Update Successfully";
                    return View(smsSettingafterUpdate);
                }
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "SMS Template update Fail";
            }
            return View(smsSetting);

        }
        #endregion

        #region Manage SmsSettings
        [HttpPost]
        public ActionResult Details(long id)
        {
            if (id == 0)
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

            try
            {
                SmsSettings smsSettingdetails = _smsService.LoadSmsSettingsById(id);
                if (smsSettingdetails == null)
                {
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
                return View(smsSettingdetails);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Manage SmsSettings
        public ActionResult Index()
        {
            try
            {
                //bool flag = SendSmsApi.GenarateInstantSmsAndNumber(1, 57, 39, 669, 688, 130);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //ViewBag.OrganizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                //ViewBag.ProgramList = new SelectList(_smsService.LoadAuthorizedProgram(_userMenu), "Id", "Name");
                List<SelectListItem> _list = new List<SelectListItem>();
                ViewBag.ProgramList = new SelectList(_list, "Value", "Text");
                ViewBag.SmsTypeList = new SelectList(_smsService.LoadSmsTypeActive(), "Id", "Name");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                ViewBag.ErrorMessage = "SMS Template Loading Fail";
            }
            ViewBag.PageSize = Constants.PageSize;
            return View();
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                SmsSettings smsSettingDelete = _smsService.LoadSmsSettingsById(id);
                //smsSettingDelete.Status = SmsSettings.En
                _smsService.Delete(smsSettingDelete);
                return Json(new Response(true, "SMS Settings Delete Successful !"));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return Json(new Response(false, "SMS Settings Delete Fail !"));
            }
        }


        [HttpPost]
        public JsonResult SmsSettingsListResult(int draw, int start, int length, string programId, string smsTypeId, string status, long? organizationId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                                orderBy = "organizationId";
                                break;
                            case "1":
                                orderBy = "ProgramId";
                                break;
                            case "2":
                                orderBy = "smsType";
                                break;
                            case "4":
                                orderBy = "Mask";
                                break;
                            case "5":
                                orderBy = "Status";
                                break;

                            default:
                                orderBy = "";
                                break;
                        }
                    }
                    //int recordsTotal = _smsService.SmsSettingsRowCount(_userMenu, programId, smsTypeId, status, organizationId);
                    //long recordsFiltered = recordsTotal;
                    //List<SmsSettings> smsSettingsList = _smsService.SmsSettingsList(start, length, orderBy, orderDir.ToUpper(), _userMenu, programId, smsTypeId, status, organizationId).ToList();
                    int recordsTotal = _smsService.SmsSettingsRowCount(programId, smsTypeId, status, organizationId);
                    long recordsFiltered = recordsTotal;
                    List<SmsSettings> smsSettingsList = _smsService.SmsSettingsList(start, length, orderBy, orderDir.ToUpper(), programId, smsTypeId, status, organizationId).ToList();

                    var data = new List<object>();
                    foreach (var c in smsSettingsList)
                    {
                        var str = new List<string>();
                        if (c.Organization != null)
                            str.Add(c.Organization.ShortName);
                        else str.Add("All");
                        if (c.Program != null)
                            str.Add(c.Program.Name);
                        else str.Add("All");
                        str.Add(c.SmsType.Name);
                        string smsReceivers = "";
                        if (c.SmsReceivers.Count > 0)
                        {
                            foreach (var sr in c.SmsReceivers)
                            {
                                smsReceivers += sr.Name + ", ";
                            }

                            smsReceivers = smsReceivers.Remove(smsReceivers.Length - 2);
                        }
                        str.Add(smsReceivers);
                        str.Add(c.MaskName);
                        str.Add(StatusTypeText.GetStatusText(c.Status));
                        str.Add("<a href='#' id='" + c.Id.ToString() + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp;<a href='" + Url.Action("UpdateSmsSettings", "Sms") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-pencil' > </a>&nbsp;&nbsp;<a id='" + c.Id.ToString() + "' href='#' data-name='Sms Template' class='glyphicon glyphicon-trash'> </a>");
                        data.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    _logger.Error(ex);
                    Console.WriteLine("{0}", ex.Message);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else { return Json(HttpNotFound()); }
        }




        #endregion

        #region SMS History
        [HttpGet]
        public ActionResult ShowSmsHistory()
        {
            ViewBag.ProgramList = new SelectList(_programService.LoadProgram(), "Id", "Name");
            ViewBag.SmsTypeList = new SelectList(_smsService.LoadSmsTypeActive(), "Id", "Name");

            return View();
        }


        [HttpPost]
        public JsonResult LoadSmsHistory(int draw, int start, int length, string dateFrom, string dateTo, string programId, string smsType, string mobileNumber, string status, string sms, string userEmail)
        {
            var getData = new List<object>();
            try
            {

                NameValueCollection nvc = Request.Form;
                var orderBy = "";
                var orderDir = "DESC";
                #region OrderBy and Direction
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "1":
                            orderBy = "CreationDate";
                            break;
                        case "2":
                            orderBy = "ModificationDate";
                            break;
                        default:
                            orderBy = "ModificationDate";
                            break;
                    }
                }
                #endregion
                int recordsTotal = _smsService.CountSmsHistory(dateFrom, dateTo, programId, smsType, mobileNumber, sms, status, userEmail);
                var smsHistoryList = _smsService.LoadSmsHistory(start, length, orderBy, orderDir.ToUpper(), dateFrom, dateTo, programId, smsType, mobileNumber, sms, status, userEmail);
                long recordsFiltered = recordsTotal;
                int i = start;

                foreach (var smsHistory in smsHistoryList)
                {
                    var smsTypeObj = _smsService.LoadSmsTypeById(Convert.ToInt64(smsHistory.Type));
                    i++;
                    var str = new List<string>();
                    str.Add(i.ToString());
                    str.Add(smsHistory.CreationDate.ToString());
                    str.Add(smsHistory.ModificationDate.ToString());
                    str.Add((smsHistory.Status == 1) ? "Sent" : (smsHistory.Status == 2) ? "Pending" : "Deleted");
                    str.Add(smsHistory.ReceiverNumber);
                    str.Add((smsHistory.Program != null) ? smsHistory.Program.Name : "All Program");
                    str.Add(smsTypeObj.Name);
                    str.Add(smsHistory.Mask);
                    str.Add(smsHistory.Sms);
                    str.Add(_userService.GetUserNameByAspNetUserId(smsHistory.CreateBy));
                    getData.Add(str);

                }
                return Json(new
                {
                    draw,
                    recordsTotal,
                    recordsFiltered,
                    start,
                    length,
                    data = getData,
                    isSuccess = true
                });

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                //ViewBag.ProgramSetting = new List<ProgramSettings>();
                //InitializeManageProgramSetting();
            }
            return Json(new
            {
                draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start,
                length,
                data = getData,
                isSuccess = false
            });
        }

        public ActionResult ExportSmsHistory(string dateFrom, string dateTo, string programId, string smsType, string mobileNumber, string status, string sms, string userEmail)
        {

            try
            {
                var headerList = new List<string>();
                var footerList = new List<string>();
                var columnList = new List<string>();
                columnList.Add("Creation Date");
                columnList.Add("Send Date");
                columnList.Add("Status");
                columnList.Add("Number");
                columnList.Add("Program");
                columnList.Add("SMS Type");
                columnList.Add("Mask Name");
                columnList.Add("SMS");
                columnList.Add("Created By");
                var smsHistoryList = _smsService.LoadSmsHistory(0, 0, "CreationDate", "asc".ToUpper(), dateFrom, dateTo, programId, smsType, mobileNumber, sms, status, userEmail);
                var smsHistoryExcelList = new List<List<object>>();
                foreach (var smsHistory in smsHistoryList)
                {
                    var xlsRow = new List<object>();
                    var smsTypeObj = _smsService.LoadSmsTypeById(Convert.ToInt64(smsHistory.Type));
                    xlsRow.Add(smsHistory.CreationDate.ToString());
                    xlsRow.Add(smsHistory.ModificationDate.ToString());
                    xlsRow.Add((smsHistory.Status == 1) ? "Sent" : (smsHistory.Status == 2) ? "Pending" : "Deleted");
                    xlsRow.Add(smsHistory.ReceiverNumber);
                    xlsRow.Add((smsHistory.Program != null) ? smsHistory.Program.Name : "All Program");
                    xlsRow.Add(smsTypeObj.Name);
                    xlsRow.Add(smsHistory.Mask);
                    xlsRow.Add(smsHistory.Sms);
                    xlsRow.Add(_userService.GetUserNameByAspNetUserId(smsHistory.CreateBy));
                    smsHistoryExcelList.Add(xlsRow);

                }
                ExcelGenerator.GenerateExcel(headerList, columnList, smsHistoryExcelList, footerList, "sms_history_report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");

            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return View("AutoClose");
            }

        }
        #endregion

        #region Helper Function

        [HttpPost]
        public ActionResult GetDynamicOptionsSmsType(long smsType)
        {
            try
            {
                SmsType st = _smsService.LoadSmsTypeById(smsType);
                SelectList optionList = new SelectList(_smsService.LoadSmsTypeById(smsType).DynamicOptions, "Id", "Name");
                List<SelectListItem> selectoptionList = optionList.ToList();
                return Json(new { returnList = selectoptionList, IsSuccess = true, smsTypeBusinessId = st.BusinessId });

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion
    }
}