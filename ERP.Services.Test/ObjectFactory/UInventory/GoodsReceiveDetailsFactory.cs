using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class GoodsReceiveDetailsFactory : ObjectFactoryBase<GoodsReceiveDetails>
    {
        #region Object Initialization

        private readonly ItemFactory _itemFactory;
        private readonly ProgramFactory _programFactory;
        private readonly SessionFactory _sessionFactory;
        private readonly GoodsReceiveFactory _goodsReceiveFactory;

        private readonly IGoodsReceiveDetailService _goodsReceiveDetailService;

        public GoodsReceiveDetailsFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _itemFactory = new ItemFactory(null, _session);
            _programFactory = new ProgramFactory(this, _session);
            _sessionFactory = new SessionFactory(this, _session);
            _goodsReceiveFactory = new GoodsReceiveFactory(this, _session);

            _goodsReceiveDetailService = new GoodsReceiveDetailsService(session);
        }

        #endregion

        #region Create

        public GoodsReceiveDetailsFactory Create()
        {
            Object = new GoodsReceiveDetails()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Purpose = null,
                //do not change this value 
                ReceivedQuantity = 2,
                Rate = 100,
                TotalCost = 200,
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsReceiveDetailsFactory CreateMore(int num, GoodsReceive goodsReceive, Item item = null)
        {
            var program = new Program();
            program = item != null ? _programFactory.Create().WithOrganization(item.Organization).Persist().Object : _programFactory.Create().Persist().Object;
            var session = _sessionFactory.Create().Persist().Object;
            for (int i = 0; i < num; i++)
            {
                var obj = new GoodsReceiveDetails
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Purpose = (int)Purpose.ProgramPurpose,
                    ReceivedQuantity = 10 + i,
                    Rate = (i + 1) * 2,
                    TotalCost = 200,
                    Item = item,
                    Program = program,
                    Session = session
                };
                obj.GoodsReceive = goodsReceive;
                ObjectList.Add(obj);
            }
            return this;
        }

        public GoodsReceiveDetailsFactory WithItem()
        {
            Object.Item = _itemFactory.Create().WithItemGroup().WithOrganization().Persist().Object;
            return this;
        }

        public GoodsReceiveDetailsFactory WithItem(Item item)
        {
            Object.Item = item;
            return this;
        }

        public GoodsReceiveDetailsFactory WithProgram()
        {
            Object.Program = _programFactory.Create().WithOrganization().Persist().Object;
            return this;
        }

        public GoodsReceiveDetailsFactory WithProgram(Organization organization)
        {
            Object.Program = _programFactory.Create().WithOrganization(organization).Persist().Object;
            return this;
        }

        public GoodsReceiveDetailsFactory WithProgram(Program program)
        {
            Object.Program = program;
            return this;
        }

        public GoodsReceiveDetailsFactory WithSession()
        {
            Object.Session = _sessionFactory.Create().Persist().Object;
            return this;
        }
        public GoodsReceiveDetailsFactory WithSession(Session session)
        {
            Object.Session = session;
            return this;
        }

        public GoodsReceiveDetailsFactory WithGoodsReceive(bool isOpening, GoodsReceive goodsReceive)
        {
            //_goodsReceiveFactory.Create().WithBranch().WithSupplier().Persist(isOpening);
            Object.GoodsReceive = goodsReceive;
            return this;
        }

        public GoodsReceiveDetailsFactory WithProperty(int receivedQty)
        {
            Object.ReceivedQuantity = receivedQty;
            return this;
        }

        public GoodsReceiveDetailsFactory WithPurpose()
        {
            Object.Purpose = (int)Purpose.ProgramPurpose;
            return this;
        }
        public GoodsReceiveDetailsFactory WithPurpose(Purpose purpose)
        {
            Object.Purpose = (int) purpose;
            return this;
        }

        public GoodsReceiveDetailsFactory WithPurpose(int purpose)
        {
            Object.Purpose = purpose;
            return this;
        }

        public GoodsReceiveDetailsFactory WithProgramAndSession(Program program, Session session)
        {
            Object.Program = program;
            Object.Session = session;
            return this;
        }

        public GoodsReceiveDetailsFactory WithWorkOrderDetails(WorkOrderDetails workOrderDetails)
        {
            Object.WorkOrderDetail = workOrderDetails;
            return this;
        }
        public GoodsReceiveDetailsFactory Persist()
        {
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x => { if (x.Id == 0) _goodsReceiveDetailService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                foreach (var goodsReceiveDetailse in ObjectList)
                {
                    goodsReceiveDetailse.Item = WithItem().Object.Item;
                    goodsReceiveDetailse.Program = WithProgram().Object.Program;
                    goodsReceiveDetailse.Session = WithSession().Object.Session;
                }
                ObjectList.ForEach(x => { if (x.Id == 0)_goodsReceiveDetailService.SaveOrUpdate(x); });
            }
            return this;
        }

        #endregion

        #region CleanUp

        public GoodsReceiveDetailsFactory CleanUp()
        {
            DeleteAll();
            _programFactory.Cleanup();
            _sessionFactory.Cleanup();
            _itemFactory.CleanUp();
            return this;
        }

        public GoodsReceiveDetailsFactory OnlyGoodsReceiveDetailsCleanUp()
        {
            DeleteAll();
            return this;
        }

        #endregion

    }
}
