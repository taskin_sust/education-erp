using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IChildrenAllowanceBlockDao : IBaseDao<ChildrenAllowanceBlock, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<ChildrenAllowanceBlock> LoadByPinAndAllowanceTypeAndEffectiveDate(long teamMemberId, long caId, DateTime effectiveDate, long entityId = 0, bool effectiveDateIsLess = true);
        IList<ChildrenAllowanceBlock> LoadByPinAndAllowanceTypeAndDateRange(long teamMemberId, long caId, DateTime effectiveDate, DateTime? closingDate);
        IList<ChildrenAllowanceBlockViewModel> LoadAll(int draw, int start, int length, List<long> orgIds, string pinNo = "", string blockName = "", long? allowanceType = null);
        IList<ChildrenAllowanceBlock> LoadByChildrenAllIdAndDateDifference(long childrenAllowanceId, DateTime dateFrom, DateTime? closingdateTime);

        #endregion

        #region Others Function

        int CountBlocks(List<long> orgIds, string pinNo = "", string blockName = "", long? allowanceType = null);
        bool IsChildrenAllowanceBlock(long childrenAllowanceSettingId, long teamMemberId, DateTime dateTime);

        #endregion


    }

    public class ChildrenAllowanceBlockDao : BaseDao<ChildrenAllowanceBlock, long>, IChildrenAllowanceBlockDao
    {
        #region Properties & Object & Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<ChildrenAllowanceBlock> LoadByPinAndAllowanceTypeAndDateRange(long teamMemberId, long caId, DateTime effectiveDate, DateTime? closingDate)
        {
            var query = Session.QueryOver<ChildrenAllowanceBlock>();
            query.Where(
                x =>
                    x.TeamMember.Id == teamMemberId && x.ChildrenAllowanceSetting.Id == caId &&
                    x.EffectiveDate.Month >= effectiveDate.Month);
            if (closingDate != null)
            {
                query.Where(x => x.ClosingDate.Value.Month <= closingDate.Value.Month);
            }
            return
                query.List<ChildrenAllowanceBlock>();
        }

        public IList<ChildrenAllowanceBlock> LoadByPinAndAllowanceTypeAndEffectiveDate(long teamMemberId, long caId, DateTime effectiveDate, long id = 0, bool effectiveDateIsLess = true)
        {
            var query = Session.QueryOver<ChildrenAllowanceBlock>().Where(x => x.Status == ChildrenAllowanceBlock.EntityStatus.Active
                                                                    && x.TeamMember.Id == teamMemberId
                                                                    && x.ChildrenAllowanceSetting.Id == caId
                                                                    && x.Id != id);
            if (effectiveDateIsLess)
            {
                query.Where(x => x.EffectiveDate.Month <= effectiveDate.Month);
            }
            else
            {
                query.Where(x => x.EffectiveDate.Month > effectiveDate.Month);
            }
            var list = query.List<ChildrenAllowanceBlock>().OrderByDescending(x => x.EffectiveDate);
            return list.ToList();
        }

        public IList<ChildrenAllowanceBlockViewModel> LoadAll(int draw, int start, int length, List<long> orgIds, string pinNo = "", string blockName = "", long? allowanceType = null)
        {
            string queryStr = GetQueryString(orgIds, pinNo, blockName, allowanceType);
            queryStr += " order by  rest.Pin OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            IQuery iQuery = Session.CreateSQLQuery(queryStr);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ChildrenAllowanceBlockViewModel>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<ChildrenAllowanceBlockViewModel>();
            return list;
        }

        public IList<ChildrenAllowanceBlock> LoadByChildrenAllIdAndDateDifference(long childrenAllowanceId,
            DateTime dateFrom, DateTime? closingdateTime)
        {
            var query =
                Session.QueryOver<ChildrenAllowanceBlock>()
                    .Where(x => x.Status == ChildrenAllowanceBlock.EntityStatus.Active && x.ChildrenAllowanceSetting.Id == childrenAllowanceId && x.EffectiveDate >= dateFrom);
            if (closingdateTime != null && closingdateTime != DateTime.MinValue)
                query.Where(x => x.ClosingDate >= closingdateTime);
            return query.List<ChildrenAllowanceBlock>();
        }

        public bool IsChildrenAllowanceBlock(long childrenAllowanceSettingId, long teamMemberId, DateTime dateTime)
        {
            ICriteria criteria = Session.CreateCriteria<ChildrenAllowanceBlock>().Add(Restrictions.Eq("Status", ChildrenAllowanceBlock.EntityStatus.Active));

            criteria.CreateAlias("ChildrenAllowanceSetting", "childrenAllowanceSetting").Add(Restrictions.Eq("childrenAllowanceSetting.Status", ChildrenAllowanceSetting.EntityStatus.Active)).Add(Restrictions.Eq("childrenAllowanceSetting.Id", childrenAllowanceSettingId));
            criteria.CreateAlias("TeamMember", "member").Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active)).Add(Restrictions.Eq("member.Id", teamMemberId));

            criteria.Add(Restrictions.Le("EffectiveDate", dateTime.Date));
            var disjunction = Restrictions.Disjunction(); // for OR statement 
            disjunction.Add(Restrictions.IsNull("ClosingDate") || Restrictions.Ge("ClosingDate", dateTime.Date));
            criteria.Add(disjunction);

            criteria.SetTimeout(3000);
            criteria.SetProjection(Projections.RowCount());
            int rowNumber = criteria.UniqueResult<int>();
            if (rowNumber > 0)
                return true;
            return false;
        }

        #endregion

        #region Others Function

        public int CountBlocks(List<long> orgIds, string pinNo = "", string blockName = "", long? allowanceType = null)
        {
            string queryStr = "select Count(countNum.Pin) from ( ";
            queryStr += GetQueryString(orgIds, pinNo, blockName, allowanceType);
            queryStr += " ) as countNum ";
            IQuery iQuery = Session.CreateSQLQuery(queryStr);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function

        private string GetQueryString(IEnumerable<long> orgIds, string pinNo = "", string name = "", long? allowanceType = null)
        {
            string inner = @"SELECT
                              ab.Id,
                              rest.Pin,
                              rest.Name,
                              rest.desgnation DesignationName,
                              rest.dept DepartmentName,
                              rest.org OrganizationName,
                              cas.Name AllowanceType,
                              CONCAT(CAST(ab.EffectiveDate AS CHAR(3)), ', ', year(ab.EffectiveDate)) AS EffectiveDate,
                              up.UserName CreatedBy,
                              aup.UserName ModifiedBy,
                              CASE WHEN ab.ClosingDate IS NULL THEN '' ELSE CONCAT(CAST(ab.ClosingDate AS CHAR(3)), ', ', year(ab.ClosingDate)) END AS ClosingDate,
                              ab.CreationDate CreationDate,
                              ab.ModificationDate ModificationDate
                            FROM [dbo].[HR_ChildrenAllowanceBlock] ab
                            INNER JOIN [dbo].[HR_ChildrenAllowanceSetting] cas
                              ON ab.ChildrenAllowanceSettingId = cas.Id 
                                and  ab.Status=1";
            if (allowanceType != null && allowanceType > 0)
            {
                inner += "and cas.Id=" + allowanceType;
            }

            inner += @" INNER JOIN (SELECT
                              ff.*,
                              desig.ShortName desgnation,
                              dept.Name dept,
                              org.ShortName org
                            FROM (SELECT
                              *
                            FROM (SELECT
                              tm.Id,
                              tm.Pin,
                              tm.Name,
                              meh.DesignationId,
                              meh.DepartmentId,
                              RANK() OVER (PARTITION BY meh.TeamMemberId ORDER BY meh.EffectiveDate DESC, meh.Id DESC) AS rankNo
                            FROM [dbo].[HR_TeamMember] tm
                            INNER JOIN [HR_EmploymentHistory] meh
                              ON tm.Id = meh.TeamMemberId ";
            if (!string.IsNullOrEmpty(pinNo))
            {
                inner += "where tm.Pin =" + Convert.ToInt32(pinNo);
            }

            inner += @") AS ss
                            WHERE ss.rankNo = 1) AS ff
                            LEFT JOIN [dbo].[HR_Designation] desig ON desig.Id = ff.DesignationId
                            LEFT JOIN [dbo].[HR_Department] dept ON dept.Id = ff.DepartmentId
                            LEFT JOIN [dbo].[Organization] org ON org.Id = desig.OrganizationId";
            inner += @"     WHERE org.Id IN(" + string.Join(",", orgIds) + @")
                            ) AS rest ON rest.Id = ab.TeamMemberId 
                            Left join [dbo].AspNetUsers up on up.Id = ab.CreateBy  
							Left join [dbo].AspNetUsers aup on aup.Id = ab.ModifyBy ";

            if (!String.IsNullOrEmpty(name))
            {
                inner += " where rest.Name like '%" + name + "%' ";
            }
            return inner;
        }

        #endregion


    }
}
