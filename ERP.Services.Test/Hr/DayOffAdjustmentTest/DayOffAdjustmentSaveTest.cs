﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;
using Xunit.Sdk;


namespace UdvashERP.Services.Test.Hr.DayOffAdjustmentTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "DayOffAdjustment")]
    public class DayOffAdjustmentSaveTest : DayOffAdjustmentBaseTest
    {

        #region Basic Test

        [Fact]
        public void Should_Return_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016,12,12), new DateTime(2016,12,12), 
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            Assert.True(_dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object }));
        }

        [Fact]
        public void Should_Return_InvalidException_For_Reason()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 12),
                "", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            Assert.Throws<InvalidDataException>(() => _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object }));
        }

        [Fact]
        public void Should_Return_InvalidException_For_InsteadOfDate()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), null,
                "test p", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            Assert.Throws<InvalidDataException>(() => _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object }));
        }

        [Fact]
        public void Should_Return_InvalidException_For_DayOfDate()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(null, new DateTime(2016, 12, 12),
                "", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            Assert.Throws<InvalidDataException>(() => _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object }));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_InvalidException_For_DuplicateCheck()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 13),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm).Persist();

            var dayOffFactory = _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12),new DateTime(2016, 12, 13),
                "Test Purpose", (int) DayOffAdjustmentStatus.Approved).WithTeamMember(tm);
            
            Assert.Throws<InvalidDataException>(() => _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { dayOffFactory.Object }));

        }

        [Fact]
        public void Should_Return_InvalidException_For_Weekend_Check()
        {

            DateTime TempDate = new DateTime(2016, 12, 1);
            
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            
            // find member weekend
            while (TempDate.DayOfWeek.ToString() != commonHelper.GetEmumIdToValue<WeekDay>((int)tm.ShiftWeekendHistory.FirstOrDefault().Weekend))
                TempDate = TempDate.AddDays(1);
            
            _dayOffAdjustmentFactory.CreateWith(TempDate, new DateTime(2016,12,13), 
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            Assert.Throws<InvalidDataException>(() => _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object }));
        }

        [Fact]
        public void Should_Return_INvalidException_For_HolyDaySessting()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            
            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016,12,16), new DateTime(2016,12,13), 
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            Assert.Throws<InvalidDataException>(() => _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object }));
        }

        [Fact]
        public void Should_Return_INvalidException_For_HolyDayWork()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            var br = tm.EmploymentHistory.FirstOrDefault().Campus.Branch;
            var userMenu = BuildUserMenu(org, (Program) null, br);
            _holyHolidayWorkFactory.CreateWith(new DateTime(2016, 12, 16), (int)HolidayWorkApprovalStatus.Full).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 15), new DateTime(2016, 12, 16),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            Assert.Throws<InvalidDataException>(() => _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object }));
        }

        #endregion

    }
}