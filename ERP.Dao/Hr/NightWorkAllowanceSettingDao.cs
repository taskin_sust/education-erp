using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface INightWorkAllowanceSettingDao : IBaseDao<NightWorkAllowanceSetting, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        NightWorkAllowanceSetting GetTeamMemberNightWorkAllowance(long organizationId, int empStatus, DateTime dateTime);

        #endregion

        #region List Loading Function

        IList<NightWorkAllowanceSetting> LoadByOrgAndNameAndEmpStatusAndCalculationOnAndMulFactorAndDate(
            List<long> orgIds, string allowanceName, IList<MemberEmploymentStatus> list, int calculationOn, decimal multiplyingFactor, 
            decimal outHousemultiplyingFactor,
          DateTime effectiveDate, DateTime? closingDate, long aSettingId = 0);
        IList<NightWorkAllowanceSetting> LoadAll(int draw, int start, int length, List<long> authOrgList, int? empStatus);

        #endregion

        #region Others Function

        int CountAllowance(List<long> organizationId, int? empStatus);
        DateTime GetLastpostedDateFromNightWork(long orgId);
        DateTime GetEligibleClosingDateForPayrollSetting(long nightworkAllowanceId);
        bool IsStillValid(long id);

        #endregion
    }

    public class NightWorkAllowanceSettingDao : BaseDao<NightWorkAllowanceSetting, long>, INightWorkAllowanceSettingDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public NightWorkAllowanceSetting GetTeamMemberNightWorkAllowance(long organizationId, int empStatus, DateTime dateTime)
        {
            var query = Session.QueryOver<NightWorkAllowanceSetting>();
            switch (empStatus)
            {
                case (int)AllowanceSettingEmployeeStatus.Contractual: query.Where(x => x.IsContractual);
                    break;
                case (int)AllowanceSettingEmployeeStatus.PartTime: query.Where(x => x.IsPartTime);
                    break;
                case (int)AllowanceSettingEmployeeStatus.Permanent: query.Where(x => x.IsPermanent);
                    break;
                case (int)AllowanceSettingEmployeeStatus.Probation: query.Where(x => x.IsProbation);
                    break;
            }
            query.Where(x => x.Status == NightWorkAllowanceSetting.EntityStatus.Active);
            query.Where(x => x.Organization.Id == organizationId);
            query.Where(x => x.EffectiveDate <= dateTime);
            query = query.Where(x => x.ClosingDate == null || x.ClosingDate >= dateTime).OrderBy(x => x.EffectiveDate).Desc.ThenBy(x => x.InHouseMultiplyingFactor).Desc;
            return query.Take(1).SingleOrDefault<NightWorkAllowanceSetting>();
        }

        #endregion

        #region List Loading Function

        public IList<NightWorkAllowanceSetting> LoadByOrgAndNameAndEmpStatusAndCalculationOnAndMulFactorAndDate(List<long> orgIds, string allowanceName, IList<MemberEmploymentStatus> empStatus,
              int calculationOn, decimal inHousemultiplyingFactor, decimal outHousemultiplyingFactor, DateTime effectiveDate, DateTime? closingDate, long aSettingId = 0)
        {
            var criteria = Session.CreateCriteria<NightWorkAllowanceSetting>();
            criteria = GetQueryCriteria(criteria, orgIds, empStatus);
            //will be needed if requirement occurs 
            // criteria.Add(Restrictions.Eq("Name", allowanceName));
            //criteria.Add(Restrictions.Eq("CalculationOn", calculationOn));
            //criteria.Add(Restrictions.Eq("InHouseMultiplyingFactor", inHousemultiplyingFactor));
            //criteria.Add(Restrictions.Eq("OutHouseMultiplyingFactor", outHousemultiplyingFactor));
            if (aSettingId > 0) { criteria.Add(Restrictions.Not(Restrictions.Eq("Id", aSettingId))); }
            if (closingDate != null)
                criteria.Add(Restrictions.And(Restrictions.Le("EffectiveDate", effectiveDate), Restrictions.Ge("ClosingDate", closingDate)));
            var list = criteria.List<NightWorkAllowanceSetting>();
            return list.ToList();
        }

        private NHibernate.ICriteria GetQueryCriteria(NHibernate.ICriteria criteria, List<long> authOrgList, IList<MemberEmploymentStatus> empStatusList)
        {
            criteria.Add(Restrictions.Eq("Status", NightWorkAllowanceSetting.EntityStatus.Active));
            if (authOrgList.Count > 0)
                criteria.Add(Restrictions.In("Organization", authOrgList));
            var or = Restrictions.Disjunction();
            foreach (int memberEmploymentStatuse in empStatusList)
            {
                switch (memberEmploymentStatuse)
                {
                    case (int)MemberEmploymentStatus.Permanent:
                        var resF = Restrictions.Eq("IsPermanent", true);
                        or.Add(resF);
                        break;
                    case (int)MemberEmploymentStatus.Probation:
                        var resS = Restrictions.Eq("IsProbation", true);
                        or.Add(resS);
                        break;
                    case (int)MemberEmploymentStatus.PartTime:
                        var resT = Restrictions.Eq("IsPartTime", true);
                        or.Add(resT);
                        break;
                    case (int)MemberEmploymentStatus.Contractual:
                        var resFo = Restrictions.Eq("IsContractual", true);
                        or.Add(resFo);
                        break;
                    case (int)MemberEmploymentStatus.Intern:
                        var resFiv = Restrictions.Eq("IsIntern", true);
                        or.Add(resFiv);
                        break;
                    default: break;
                }
            }
            criteria.Add(or);
            return criteria;
        }

        public IList<NightWorkAllowanceSetting> LoadAll(int draw, int start, int length, List<long> authOrgList, int? empStatus)
        {
            var criteria = Session.CreateCriteria<NightWorkAllowanceSetting>();
            criteria = GetQueryCriteria(criteria, authOrgList, empStatus);

            return criteria.SetFirstResult(start).SetMaxResults(length).List<NightWorkAllowanceSetting>();
        }

        public int CountAllowance(List<long> authOrgList, int? empStatus)
        {
            var criteria = Session.CreateCriteria<NightWorkAllowanceSetting>();
            criteria = GetQueryCriteria(criteria, authOrgList, empStatus);

            return criteria.List<NightWorkAllowanceSetting>().Count;
        }

        #endregion

        #region Others Function

        public DateTime GetLastpostedDateFromNightWork(long orgId)
        {
            var query = @"select Top(1) nw.NightWorkDate from(
                        SELECT * FROM [HR_NightWork] where IsPost='true') as nw left join(
                        select ffportion.*, org.Id as orgId, org.Name from(
                        SELECT * FROM(
                        SELECT
                          [CampusId],
                          [DesignationId],
                          [DepartmentId],
                          [TeamMemberId],
                          [EffectiveDate],
                          [EmploymentStatus], 
                        RANK() OVER (PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC, Id DESC) AS R
                        FROM [HR_EmploymentHistory]
                        Where Status = 1 and 
                        EffectiveDate <= getDate()) as emphis where emphis.R=1 AND emphis.[EmploymentStatus] != 6) as ffportion
                        left join HR_Department as dept on ffportion.DepartmentId = dept.Id
                        left join Organization as org on dept.OrganizationId = org.Id) as ssportion on nw.TeamMemberId = ssportion.TeamMemberId
                        where ssportion.orgId=" + orgId;
            query += @"order by nw.NightWorkDate desc";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            return iQuery.UniqueResult<DateTime>();
        }

        public DateTime GetEligibleClosingDateForPayrollSetting(long nightworkAllowanceId)
        {
            var query = @"select top(1) aSheet.DateTo from(
                    select * from [HR_AllowanceSheetFirstDetails] where NightWorkAllowanceSettingId =" + nightworkAllowanceId;
            query += @") as aSheetSecDetail
                    left join HR_AllowanceSheet as aSheet on aSheet.Id= aSheetSecDetail.AllowanceSheetId and aSheet.IsFinalSubmit='true'
                    order by aSheet.DateTo desc ";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            return iQuery.UniqueResult<DateTime>();
        }

        public bool IsStillValid(long id)
        {
            var closingDate = this.GetEligibleClosingDateForPayrollSetting(id);
            if (closingDate == DateTime.MinValue) return false;
            if (closingDate != DateTime.MinValue && closingDate < DateTime.Now) return false;
            return true;
        }

        #endregion

        #region Helper Function

        private NHibernate.ICriteria GetQueryCriteria(NHibernate.ICriteria criteria, List<long> authOrgList, int? empStatus)
        {
            criteria.Add(Restrictions.Eq("Status", NightWorkAllowanceSetting.EntityStatus.Active));
            if (authOrgList.Count > 0)
                criteria.CreateAlias("Organization", "org");
            criteria.Add(Restrictions.In("org.Id", authOrgList));
            if (empStatus != null)
            {
                switch (empStatus)
                {
                    case (int)MemberEmploymentStatus.Permanent:
                        criteria.Add(Restrictions.Eq("IsPermanent", true));
                        break;
                    case (int)MemberEmploymentStatus.Probation:
                        criteria.Add(Restrictions.Eq("IsProbation", true));
                        break;

                    case (int)MemberEmploymentStatus.PartTime:
                        criteria.Add(Restrictions.Eq("IsPartTime", true));
                        break;

                    case (int)MemberEmploymentStatus.Contractual:
                        criteria.Add(Restrictions.Eq("IsContractual", true));
                        break;
                    case (int)MemberEmploymentStatus.Intern:
                        criteria.Add(Restrictions.Eq("IsIntern", true));
                        break;
                    default: break;
                }
            }

            return criteria;
        }

        #endregion

    }
}
