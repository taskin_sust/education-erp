(function ($) {
    //$.fn.myStringify = function (SearchCriteria) {
    //    var Text = "";      
    //    $.each(SearchCriteria, function (index, value) {          
    //        Text = Text + index + "====" + value + "@@@@";
    //    });
    //    return Text;
    //}


    $.fn.pagination = function (options) {

        var settings = $.extend({
            defaultPage: 1,
            ajaxUrl: "Give a Url for loading data by ajax",
            partialViewDiv: "Give a div Id for display loaded data",
            paginationDiv: "Give pagination div to display pagination",
            filtering: false
        }, options);


        //Filtering Preparation
        var filterData = {};
        var filterDataString = "";
        var PageSize = $("#PageSize").val();

        if (settings.filtering) {
            $(".filteringRow .filteringColumn, .filteringRow .filteringColumn2").each(function (index, value) {
                filterData[$(this).attr("id")] = $(this).val();
                //if (index == 0)
                //    filterDataString = filterDataString + "#" + $(this).attr("id");
                //else filterDataString = filterDataString + ", #" + $(this).attr("id");
            });
        }

        var SearchCriteria = JSON.stringify(filterData);

        var data = { searchCriteria: SearchCriteria, currentPage: settings.defaultPage, pageSize: PageSize };
     
        /* First Time Display */
        $.ajax({
            url: settings.ajaxUrl,
            data: data,
            success: function (data) {
                $("#" + settings.partialViewDiv).html(data);
                $("#" + settings.paginationDiv).html("");
                var paginationText = '<ul class="pagination"><li><a href="#" class="page-number" id="first" title="First">&laquo;&laquo;</a></li><li><a href="#" class="page-number" id="prev" title="Previous">&laquo;</a></li>';
                for (var j = 1; j <= $("#filterInfo").attr("data-totalpage") ; j++) {
                    paginationText += '<li><a href="#" class="page-number">' + j + '</a></li>';
                }
                paginationText += '<li><a href="#" class="page-number" id="next" title="Next">&raquo;</a></li><li><a href="#" class="page-number" id="last" title="Last">&raquo;&raquo;</a></li></ul>';
                $("#" + settings.paginationDiv).append(paginationText);
                $(".pagination").find(".active").removeClass("active");

                $(".pagination li a").each(function () {
                    if (settings.defaultPage == $(this).text()) {
                        $(this).parent().addClass("active");
                    }
                });
            }
        });

      

        //Filtering
        $(document).on("keyup", ".filteringColumn, .filteringRow .filteringColumn2", function () {
           
            if (settings.filtering) {
                $(".filteringRow .filteringColumn").each(function (index, value) {
                    filterData[$(this).attr("id")] = $(this).val();
                });
            }
            var PageSize = $("#PageSize").val();
            var SearchCriteria = JSON.stringify(filterData);
           
            var data = { searchCriteria: SearchCriteria, currentPage: settings.defaultPage, pageSize: PageSize };

            $.ajax({
                url: settings.ajaxUrl,
                data: data,
                success: function (data) {
                    $("#" + settings.partialViewDiv).html(data);
                    $("#" + settings.paginationDiv).html("");
                    
                    var paginationText = '<ul class="pagination"><li><a href="#" class="page-number" id="first" title="First">&laquo;&laquo;</a></li><li><a href="#" class="page-number" id="prev" title="Previous">&laquo;</a></li>';
                    for (var j = 1; j <= $("#filterInfo").attr("data-totalpage") ; j++) {
                        paginationText += '<li><a href="#" class="page-number">' + j + '</a></li>';
                    }
                    paginationText += '<li><a href="#" class="page-number" id="next" title="Next">&raquo;</a></li><li><a href="#" class="page-number" id="last" title="Last">&raquo;&raquo;</a></li></ul>';
                    $("#" + settings.paginationDiv).append(paginationText);
                    $(".pagination").find(".active").removeClass("active");

                    $(".pagination li a").each(function () {
                        if (settings.defaultPage == $(this).text()) {
                            $(this).parent().addClass("active");
                        }
                    });
                }
            });           
        });

        $(document).on("change", ".filteringColumn2", function () {

            if (settings.filtering) {
                $(".filteringRow .filteringColumn, .filteringRow .filteringColumn2").each(function (index, value) {
                    filterData[$(this).attr("id")] = $(this).val();
                });
            }
            var PageSize = $("#PageSize").val();
            var SearchCriteria = JSON.stringify(filterData);

            var data = { searchCriteria: SearchCriteria, currentPage: settings.defaultPage, pageSize: PageSize };

            $.ajax({
                url: settings.ajaxUrl,
                data: data,
                success: function (data) {
                    $("#" + settings.partialViewDiv).html(data);
                    $("#" + settings.paginationDiv).html("");

                    var paginationText = '<ul class="pagination"><li><a href="#" class="page-number" id="first" title="First">&laquo;&laquo;</a></li><li><a href="#" class="page-number" id="prev" title="Previous">&laquo;</a></li>';
                    for (var j = 1; j <= $("#filterInfo").attr("data-totalpage") ; j++) {
                        paginationText += '<li><a href="#" class="page-number">' + j + '</a></li>';
                    }
                    paginationText += '<li><a href="#" class="page-number" id="next" title="Next">&raquo;</a></li><li><a href="#" class="page-number" id="last" title="Last">&raquo;&raquo;</a></li></ul>';
                    $("#" + settings.paginationDiv).append(paginationText);
                    $(".pagination").find(".active").removeClass("active");

                    $(".pagination li a").each(function () {
                        if (settings.defaultPage == $(this).text()) {
                            $(this).parent().addClass("active");
                        }
                    });
                }
            });
        });



        /* Display Data on click */
        $(document).on("click", ".page-number", function () {
            var selector = $(this);
            var page = parseInt(selector.html());
            var maxPage = $(".pagination li").size() - 4;
            var PageSize = $("#PageSize").val();

            if (settings.filtering) {
                $(".filteringRow .filteringColumn, .filteringRow .filteringColumn2").each(function (index, value) {
                    filterData[$(this).attr("id")] = $(this).val();
                });
            }
           

             var action = $(this).attr("id");
            if (action != null && action != undefined) {
                if(action=="first"){
                    page = 1;
                }
                else if (action == "prev") {
                    page = parseInt($(".pagination").find(".active").find(".page-number").html()) - 1;
                    if(page==0)
                        page = 1;
                }
                else if (action == "next") {
                    page = parseInt($(".pagination").find(".active").find(".page-number").html()) + 1;
                    if (page == maxPage+1)
                        page = maxPage;
                }
                else if (action == "last") {
                    page = maxPage;
                }
            }

            //var PageSize = $("#PageSize").val();
            var SearchCriteria = JSON.stringify(filterData);
            var data = { searchCriteria: SearchCriteria, currentPage: page, pageSize: PageSize };

            //var SearchCriteria = JSON.stringify(filterData);
            //var data = { searchCriteria: SearchCriteria, currentPage: page, pageSize: PageSize };

            $(".pagination").find(".active").removeClass("active");
            $(".pagination li a").each(function () {
                if (page == $(this).text()) {
                    $(this).parent().addClass("active");
                }
            });
            
            $.ajax({
                url: settings.ajaxUrl,
                data: data,
                success: function (data) {
                    $("#" + settings.partialViewDiv).html(data);
                }
            });
        });
        ///* End */

        $(document).on("keyup", "#PageSize", function () {
            
            if (settings.filtering) {
                $(".filteringRow .filteringColumn, .filteringRow .filteringColumn2").each(function (index, value) {
                    filterData[$(this).attr("id")] = $(this).val();
                });
            }
            var PageSize = $("#PageSize").val();
            if ((/^\d+$/.test(PageSize)) && PageSize != "" && PageSize!=null) {
                var SearchCriteria = JSON.stringify(filterData);
                var data = { searchCriteria: SearchCriteria, currentPage: settings.defaultPage, pageSize: PageSize };

                $.ajax({
                    url: settings.ajaxUrl,
                    data: data,
                    success: function (data) {
                        $("#" + settings.partialViewDiv).html(data);
                        $("#" + settings.paginationDiv).html("");

                        var paginationText = '<ul class="pagination"><li><a href="#" class="page-number" id="first" title="First">&laquo;&laquo;</a></li><li><a href="#" class="page-number" id="prev" title="Previous">&laquo;</a></li>';
                        for (var j = 1; j <= $("#filterInfo").attr("data-totalpage") ; j++) {
                            paginationText += '<li><a href="#" class="page-number">' + j + '</a></li>';
                        }
                        paginationText += '<li><a href="#" class="page-number" id="next" title="Next">&raquo;</a></li><li><a href="#" class="page-number" id="last" title="Last">&raquo;&raquo;</a></li></ul>';
                        $("#" + settings.paginationDiv).append(paginationText);
                        $(".pagination").find(".active").removeClass("active");

                        $(".pagination li a").each(function () {
                            if (settings.defaultPage == $(this).text()) {
                                $(this).parent().addClass("active");
                            }
                        });
                    }
                });
            }
           
           
        });

    };
}(jQuery));