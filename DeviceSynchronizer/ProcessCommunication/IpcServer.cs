﻿using System;
using DeviceSynchronizer.Helper;
using System.ServiceModel;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace DeviceSynchronizer.ProcessCommunication
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class IpcServer : IIpcOperation
    {
        private readonly MainForm _from;
        private ServiceHost _serviceHost;
        public IpcServer(MainForm mainForm)
        {
            _from = mainForm;
        }

        public string GetSoftwareVersion()
        {
            return BusinessRules.AppVersion;
        }

        public string GetSoftwareName()
        {
            return BusinessRules.AppName;
        }

        public string GetApplicationTitle()
        {
            return _from.Text;
        }

        public bool IsServiceRunning()
        {
            return _from.IsServiceRunning();
        }

        public void StopService(bool exitApplication = false)
        {
            _from.StopService(exitApplication);
        }

        public void IpcServerStart()
        {
            //server address
            string ipcAddress = ConfigurationManager.AppSettings["IpcAddress"];

            //start service
            _serviceHost = new ServiceHost(this);
            NetNamedPipeBinding binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);
            _serviceHost.AddServiceEndpoint(typeof(IIpcOperation), binding, ipcAddress);
            _serviceHost.Open();
        }

        public void IpcServerStop()
        {
            if (_serviceHost != null)
                _serviceHost.Close();
        }
    }
}
