﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using NHibernate.Criterion;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Teachers;
using UdvashERP.Services.Hr;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class CommonAjaxController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IBatchService _batchService;
        private readonly ICourseService _courseService;
        private readonly ITeacherService _teacherService;
        private readonly ISmsMaskService _smsMaskService;
        private readonly IDepartmentService _hrDepartmentService;
        private readonly IDesignationService _hrDesignationService;
        private readonly IAddressService _addressService;
        private readonly IBankBranchService _bankBranchService;

        public CommonAjaxController()
        {
            var sessionFactory = NHibernateSessionFactory.OpenSession();
            _organizationService = new OrganizationService(sessionFactory);
            _programService = new ProgramService(sessionFactory);
            _sessionService = new SessionService(sessionFactory);
            _branchService = new BranchService(sessionFactory);
            _campusService = new CampusService(sessionFactory);
            _batchService = new BatchService(sessionFactory);
            _courseService = new CourseService(sessionFactory);
            _teacherService = new TeacherService(sessionFactory);
            _smsMaskService = new SmsMaskService(sessionFactory);
            _hrDepartmentService = new DepartmentService(sessionFactory);
            _hrDesignationService = new DesignationService(sessionFactory);
            _addressService = new AddressService(sessionFactory);
            _bankBranchService = new BankBranchService(sessionFactory);
        }

        #endregion

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult LoadOrganization(bool isAuhorized = true)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organizationList = (isAuhorized) ? _organizationService.LoadAuthorizedOrganization(userMenu) : _organizationService.LoadOrganization(1);
                var organizationSelectList = new SelectList(organizationList.ToList(), "Id", "ShortName");
                return Json(new { returnOrganizationList = organizationSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Organization";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadProgram(List<long> organizationIds = null, List<long> sessionIds = null, bool isAuthorized = false, List<long> branchIds = null)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //using ternary operator
                IList<Program> programList = isAuthorized ? _programService.LoadAuthorizedProgram(userMenu, organizationIds, sessionIds, null, null, branchIds) : _programService.LoadProgram(organizationIds, sessionIds);
                var programSelectList = new SelectList(programList.ToList(), "Id", "Name");
                return Json(new { returnProgramList = programSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Program";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadSession(List<long> organizationIds = null, List<long> programIds = null, bool isAuthorized = false)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Session> sessionList = isAuthorized ? _sessionService.LoadAuthorizedSession(userMenu, organizationIds, programIds) : _sessionService.LoadSession(organizationIds, programIds);
                var sessionSelectList = new SelectList(sessionList.ToList(), "Id", "Name");
                return Json(new { returnSessionList = sessionSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Session";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadBranch(List<long> organizationIds = null, List<long> programIds = null, List<long> sessionIds = null, bool isAuthorized = false, bool isProgramBranSession = true, bool? isCorporate = null)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Branch> branchList = isAuthorized ? _branchService.LoadAuthorizedBranch(userMenu, organizationIds, programIds, sessionIds, isProgramBranSession, isCorporate) :
                    _branchService.LoadBranch(organizationIds, programIds, sessionIds, null, null, isCorporate);
                var branchSelectList = new SelectList(branchList.ToList(), "Id", "Name");
                return Json(new { returnBranchList = branchSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred During Load Branch";
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadCampus(List<long> organizationIds = null, List<long> programIds = null, List<long> branchIds = null, List<long> sessionIds = null, bool isAuthorized = true, bool isProgramBranSession = true)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Campus> campusList = isAuthorized ? _campusService.LoadAuthorizeCampus(userMenu, organizationIds, programIds, branchIds, sessionIds, isProgramBranSession) : _campusService.LoadCampus(organizationIds, programIds, sessionIds, branchIds, null, isProgramBranSession);
                var campusSelectList = new SelectList(campusList.ToList(), "Id", "Name");
                return Json(new { returnCampusList = campusSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Campus";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadBatchDay(List<long> organizationIds, List<long> programIds, List<long> branchIds, List<long> sessionIds, List<long> campusIds, List<int> selectedVersions = null, List<int> selectedGenders = null, bool isAuthorized = true)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<string> batchDayList = isAuthorized ? _batchService.LoadAuthorizeBatchGroupByDays(userMenu, organizationIds, programIds, branchIds, sessionIds, campusIds, selectedVersions, selectedGenders) : _batchService.LoadBatchGroupByDays(organizationIds, programIds, branchIds, sessionIds, campusIds, selectedVersions, selectedGenders);
                List<SelectListItem> batchDaySelectListItems = (new SelectList(batchDayList)).ToList();
                return Json(new { returnBatchDays = batchDaySelectListItems, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Batch Days";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadBatchTime(List<long> organizationIds, List<long> programIds, List<long> branchIds, List<long> sessionIds, List<long> campusIds, string[] batchDays, List<int> selectedVersions = null, List<int> selectedGenders = null, bool isAuthorized = true)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<string> batchTimeList = isAuthorized ? _batchService.LoadAuthorizeBatchGroupByTimes(userMenu, organizationIds, programIds, branchIds, sessionIds, campusIds, batchDays, selectedVersions, selectedGenders) : _batchService.LoadBatchGroupByTimes(organizationIds, programIds, branchIds, sessionIds, campusIds, batchDays, selectedVersions, selectedGenders);
                List<SelectListItem> batchTimeSelectListItems = (new SelectList(batchTimeList)).ToList();
                return Json(new { returnBatchTime = batchTimeSelectListItems, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Batch Times";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadBatch(List<long> organizationIds, List<long> programIds, List<long> branchIds, List<long> sessionIds, List<long> campusIds, string[] batchDays, string[] batchTimes, List<int> selectedVersions = null, List<int> selectedGenders = null, bool isAuthorized = true)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = isAuthorized ? _batchService.LoadAuthorizeBatch(userMenu, organizationIds, programIds, branchIds, sessionIds, campusIds, batchDays, batchTimes, selectedVersions, selectedGenders).ToList() : _batchService.LoadBatch(organizationIds, programIds, sessionIds, branchIds, campusIds, null, batchDays, batchTimes, selectedGenders).ToList();
                //if (selectedGenders != null && selectedVersions != null)
                //{
                //    batchList = _batchService.LoadAuthorizeBatch(userMenu, organizationIds, programIds, branchIds, sessionIds, campusIds, batchDays, batchTimes, selectedVersions, selectedGenders).ToList();
                //}
                //else
                //{
                //    batchList = _batchService.LoadAuthorizeBatch(userMenu, organizationIds, programIds, branchIds, sessionIds, campusIds, batchDays, batchTimes).ToList();
                //}
                List<SelectListItem> batchSelecteList = (new SelectList(batchList.ToList(), "Id", "Name")).ToList();
                return Json(new { returnBatch = batchSelecteList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Batch";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadCourse(List<long> organizationIds = null, List<long> programIds = null, List<long> sessionIds = null)
        {
            try
            {
                IList<Course> courseList = _courseService.LoadCourse(organizationIds, programIds, sessionIds);
                List<SelectListItem> courseSelectListItems = (new SelectList(courseList.ToList(), "Id", "Name")).ToList();
                return Json(new { returnCourse = courseSelectListItems, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Course";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadTeacherName(string query, long organizationId, long? activityTypeId)
        {
            try
            {
                if (organizationId == 0)
                {
                    throw new MessageException("Please Select Organization.");
                }
                var teacherList =
                    new SelectList(_teacherService.LoadTeacherAutoComplete(query, organizationId, activityTypeId), "Id",
                        "DisplayTeacherName");
                List<SelectListItem> selectTeacherList = teacherList.ToList();
                return Json(new { returnList = selectTeacherList, IsSuccess = true });
            }
            catch (MessageException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            //return null;
        }

        public JsonResult LoadSmsMaskName(List<long> organizationIds = null, int? status = null)
        {
            try
            {
                IList<SmsMask> smsMaskNameList = _smsMaskService.LoadSmsMask(organizationIds, status);
                List<SelectListItem> smsMaskNameListItems = (new SelectList(smsMaskNameList.ToList(), "Name", "Name")).ToList();
                return Json(new { returnSmsMaskName = smsMaskNameListItems, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Sms Mask Naem";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadDepartment(List<long> organizationIds, bool isAuthorized = false)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Department> departmentList = (isAuthorized) ? _hrDepartmentService.LoadAuthorizedDepartment(userMenu, organizationIds) : _hrDepartmentService.LoadDepartment(organizationIds);
                var departmentSelectList = new SelectList(departmentList.ToList(), "Id", "Name");
                return Json(new { returndepartmentList = departmentSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Department";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadDesignation(List<long> organizationIds)
        {
            try
            {
                IList<Designation> designationList = _hrDesignationService.LoadDesignation(organizationIds);
                var designationListSelectList = new SelectList(designationList.ToList(), "Id", "Name");
                return Json(new { returnDesignationList = designationListSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Designation";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadDistrict(List<long> divisionIdList = null)
        {
            try
            {
                IList<District> districtList = _addressService.LoadDistrict(divisionIdList);
                var districtListSelectList = new SelectList(districtList.ToList(), "Id", "Name");
                return Json(new { returnDistrictList = districtListSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load District";
                return Json(new Response(false, ViewBag.ErrorMessage));
            }
        }
        public JsonResult LoadThana(List<long> divisionIdList = null, List<long> districtIdList = null)
        {
            try
            {
                IList<Thana> thanaList = _addressService.LoadThana(divisionIdList, districtIdList);
                var thanaListSelectList = new SelectList(thanaList.ToList(), "Id", "Name");
                return Json(new { returnThanaList = thanaListSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Thana";
                return Json(new Response(false, ViewBag.ErrorMessage));
            }
        }

        public JsonResult LoadPostOffice(List<long> divisionIdList = null, List<long> districtIdList = null)
        {
            try
            {
                IList<Postoffice> postOfficeList = _addressService.LoadPostOffice(divisionIdList, districtIdList);
                var postOfficeListSelectList = new SelectList(postOfficeList.ToList(), "Id", "DisplayName");
                return Json(new { returnPostOfficeList = postOfficeListSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Post Office";
                return Json(new Response(false, ViewBag.ErrorMessage));
            }
        }

        public JsonResult LoadBankBranch(List<long> bankIdList = null)
        {
            try
            {
                IList<BankBranch> bankBranchList = _bankBranchService.LoadBankBranchList((bankIdList != null) ? bankIdList.ToArray() : null);
                var bankBranchListSelectList = new SelectList(bankBranchList.ToList(), "Id", "Name");
                return Json(new { returnBankBranchList = bankBranchListSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Bank Branch";
                return Json(new Response(false, ViewBag.ErrorMessage));
            }
        }
    }
}