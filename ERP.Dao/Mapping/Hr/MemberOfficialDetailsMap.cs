using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class MemberOfficialDetailMap : BaseClassMap<MemberOfficialDetail,long> {
        
        public MemberOfficialDetailMap() {
			Table("HR_MemberOfficialDetail");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.CardNo).Column("CardNo");
			Map(x => x.OfficialContact).Column("OfficialContact");
			Map(x => x.OfficialEmail).Column("OfficialEmail");
			//HasMany(x => x.MemberOfficialDetailLog).KeyColumn("MemberOfficialDetailId");
        }
    }
}
