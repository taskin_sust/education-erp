﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.MessageExceptions;

namespace UdvashERP.Services.Administration
{
    public interface IProgramSettingService : IBaseService
    {
        #region Operational Function
        bool Save(ProgramSettings entity);
        bool Update(ProgramSettings obj, Program program, Session session, Program nextProgram, Session nextSession);
        bool Delete(ProgramSettings entity);
        #endregion

        #region Single Instances Loading Function
        ProgramSettings GetProgramSetting(int id);
        #endregion

        #region List Loading Function
        IList<ProgramSettings> LoadProgramSetting(long programId, long sessionId);
        IList<ProgramSettings> LoadPreviousProgramSettings(long programId, long sessionId);
        #endregion

        #region Others Function
        int GetProgramSettingCount(long programId, long sessionId);
        #endregion    
    }
    public class ProgramSettingService : BaseService, IProgramSettingService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IProgramSettingDao _programSettingDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;

        public ProgramSettingService(ISession session)
        {
            Session = session;
            _programSettingDao = new ProgramSettingDao() { Session = session };
            _programDao = new ProgramDao() { Session = session };
            _sessionDao = new SessionDao() { Session = session };
        }
        #endregion

        #region Operational Function
        public bool Save(ProgramSettings programSettings)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeSave(programSettings.Program.Id, programSettings.Session.Id, programSettings.NextProgram.Id,
                    programSettings.NextSession.Id);
                using (transaction = Session.BeginTransaction())
                {
                    _programSettingDao.Save(programSettings);
                    transaction.Commit();
                    return true;
                }
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        public bool Update(ProgramSettings programSettingObj, Program program, Session session, Program nextProgram, Session nextSession)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    CheckBeforeUpdate(programSettingObj, program.Id, session.Id, nextProgram.Id,
                        nextSession.Id);
                    programSettingObj.Program = program;
                    programSettingObj.Session = session;
                    programSettingObj.NextProgram = nextProgram;
                    programSettingObj.NextSession = nextSession;
                    _programSettingDao.Update(programSettingObj);
                    transaction.Commit();
                    return true;
                }
            }

            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        public bool Delete(ProgramSettings entity)
        {
            ITransaction transaction = null;
            try
            {
                if(entity == null)
                    throw new NullObjectException("Program Setting is invalid. Please try again!");

                using (transaction = Session.BeginTransaction())
                {
                    entity.Status = ProgramSettings.EntityStatus.Delete;
                    _programSettingDao.Update(entity);
                    transaction.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw new NullObjectException(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function
        public ProgramSettings GetProgramSetting(int id)
        {
            ProgramSettings programSettingsObj;
            if (id <= 0)
                throw new InvalidDataException("Invalid Program Setting. Pleae try for another!");
            try
            {
                programSettingsObj = _programSettingDao.LoadById(id);
                if (programSettingsObj == null)
                {
                    throw new NullObjectException("Invalid Program Setting. Pleae try for another!");
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return programSettingsObj;
        }
        #endregion

        #region List Loading Function
        public IList<ProgramSettings> LoadProgramSetting(long programId, long sessionId)
        {
            try
            {
                if (programId <= 0 || sessionId <= 0)
                {
                    throw new InvalidDataException("Something went wrong while loading Program Setting list. Please try for again!");
                }
                return _programSettingDao.LoadProgramSetting(programId, sessionId);
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }

        public IList<ProgramSettings> LoadPreviousProgramSettings(long programId, long sessionId)
        {
            try
            {
                if (programId <= 0 || sessionId <= 0)
                {
                    throw new InvalidDataException("Something went wrong while loading Program Setting list. Please try for again!");
                }
                return _programSettingDao.LoadPreviousProgramSettings(programId, sessionId);
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function
        public int GetProgramSettingCount(long programId, long sessionId)
        {
            try
            {
                if (programId <= 0 || sessionId <= 0)
                {
                    throw new InvalidDataException("Something went wrong while loading Program Setting list. Please try for again!");
                }
                return _programSettingDao.GetProgramSettingCount(programId, sessionId);
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        private void CheckBeforeUpdate(ProgramSettings programSettingObj, long programId, long sessionId, long nextProgramId, long nextSessionId)
        {
            if (programSettingObj == null || programSettingObj.Id <= 0)
                throw new NullObjectException("Can't edit while program setting object is invalid. please try again!");

            CheckEmptySelection(programId, sessionId, nextProgramId, nextSessionId);
            CheckSameProgramSelection(programId, nextProgramId);
            CheckDuplicateEntry(programId, sessionId, nextProgramId, nextSessionId, programSettingObj.Id);
            CheckReverseEntry(programId, sessionId, nextProgramId, nextSessionId);
        }
      
        private void CheckBeforeSave(long programId, long sessionId, long nextProgramId, long nextSessionId)
        {
            CheckEmptySelection(programId, sessionId, nextProgramId, nextSessionId);
            CheckSameProgramSelection(programId, nextProgramId);
            CheckDuplicateEntry(programId, sessionId, nextProgramId, nextSessionId);
            CheckReverseEntry(programId, sessionId, nextProgramId, nextSessionId);
        }

        private void CheckReverseEntry(long programId, long sessionId, long nextProgramId, long nextSessionId)
        {
            var reverseEntry = _programSettingDao.CheckReverseEntry(programId, sessionId, nextProgramId, nextSessionId);
            if (reverseEntry)
                throw new DuplicateEntryException("Reverse previous & next program found for same session.");
        }

        private void CheckSameProgramSelection(long programId, long nextProgramId)
        {
            if (programId == nextProgramId)
                throw new DuplicateEntryException("Please select different next program.");
        }

        private void CheckDuplicateEntry(long programId, long sessionId, long nextProgramId, long nextSessionId, long programSettingId = 0)
        {
            var dublicateEntry = _programSettingDao.CheckDuplicateEntry(programId, sessionId, nextProgramId, nextSessionId, programSettingId);
            if (dublicateEntry)
                throw new DuplicateEntryException("Same previous & next program found for same session.");
        }

        private void CheckEmptySelection(long programId, long sessionId, long nextProgramId, long nextSessionId)
        {
            if (programId <= 0)
                throw new EmptyFieldException("Please select Program.");
            if (sessionId <= 0)
                throw new EmptyFieldException("Please select Session.");
            if (nextProgramId <= 0)
                throw new EmptyFieldException("Please select Next Program.");
            if (nextSessionId <= 0)
                throw new EmptyFieldException("Please select Next Session.");
        }

        #endregion
    }
}
