﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Qmaster.Print.Forms;

namespace Qmaster.Print
{
    public partial class MainUi : BaseForm
    {
        #region object\initial\load 
       
        public bool loginStatus = false;
        
        public MainUi()
        {
            InitializeComponent();

            loginStatus = true;
        }

        private void MainUi_Load(object sender, EventArgs e)
        {
            smTopiLogin.Visible = false;
            smTopiLogout.Visible = false;
            ShowMenu(loginStatus);
            //smTopiLogin_Click(null, null);
            smTopiPrintQuestion_Click(null, null);
           // smTopiTest_Click(null, null);
        }
        #endregion 
        
        #region Menu operation

        #region Print question

        private void smTopiPrintQuestion_Click(object sender, EventArgs e)
        {
            try
            {

                if (!loginStatus)
                {
                    throw new InvalidDataException("Please Login!");
                }

                PrintQuestionForm printQuestionForm = new PrintQuestionForm();
                if (!checkFormOpen(printQuestionForm))
                {
                    printQuestionForm.MdiParent = this;
                    printQuestionForm.WindowState = FormWindowState.Maximized;
                    printQuestionForm.Show();
                }

            }
            catch ( InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region About Us Menu

        private void mTopAbout_Click(object sender, EventArgs e)
        {
            AboutUsForm aboutUsForm = new AboutUsForm();
            if (!checkFormOpen(aboutUsForm))
            {
                //aboutUsForm.MdiParent = this;
                //  aboutUsForm.WindowState = FormWindowState.Maximized;
                aboutUsForm.StartPosition = FormStartPosition.CenterParent; aboutUsForm.ShowDialog();
            }
        }

        #endregion

        #region Test Menu

        private void smTopiTest_Click(object sender, EventArgs e)
        {
            TestForm taForm = new TestForm();
            taForm.StartPosition = FormStartPosition.CenterParent;
            taForm.ShowDialog();
            //TestForm taForm = new TestForm();
            //if (!checkFormOpen(taForm))
            //{
            //   // taForm.MdiParent = this;
            //   // taForm.WindowState = FormWindowState.Maximized;
            //    taForm.Show();
            //}
        }

        #endregion

        #region Login Menu

        private void smTopiLogin_Click(object sender, EventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.StartPosition = FormStartPosition.CenterParent;
            loginForm.ShowDialog();
            loginStatus = loginForm.isLogin;
            ShowMenu(loginStatus);
        }

        #endregion

        #region LogOut Menu

        private void smTopiLogout_Click(object sender, EventArgs e)
        {
            CloseOpenForm();ShowMenu(false);
        }

        #endregion

        #region Exit menu

        private void smTopiExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you want to close?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        #endregion

        #endregion

        #region Helper Function

        private void ShowMenu(bool isLogin)
        {
            if (isLogin)
            {
               // smTopiLogin.Visible = false;
              //  smTopiLogout.Visible = true;
                smTopiPrintQuestion.Visible = true;
                smTopiTest.Visible = true;
                mTopHelp.Visible = true;
                mTopWhat.Visible = true;
            }
            else
            {
                smTopiLogin.Visible = true;
                smTopiLogout.Visible = false;
                smTopiPrintQuestion.Visible = false;
                smTopiTest.Visible = false;
                mTopHelp.Visible = false;
                mTopWhat.Visible = false;
            }
        }

        private bool checkFormOpen(Form form)
        {
            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                if (frm.Text == form.Text)
                {
                    return true;
                }
            }
            return false;
        }

        private void CloseOpenForm()
        {
            //FormCollection fc = Application.OpenForms;
            //MainUi mainUiForm = new MainUi();
            //if (fc.Count > 0)
            //{
            //    foreach (Form frm in fc)
            //    {
            //        if (frm != mainUiForm) frm.Close();
            //    }
            //}
        }

        #endregion

       
    }

}
