﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Hr.AttendanceSummaryServices;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class FestivalBonusSheetController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICampusService _campusService;
        private readonly IBranchService _branchService;
        private readonly ITeamMemberService _hrMemberService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IDepartmentService _departmentService;
        private readonly IAttendanceSummaryService _attendanceSummaryService;
        private readonly IShiftService _shiftService;
        private readonly IShiftWeekendHistoryService _shiftWeekendHistoryService;
        private readonly ISalaryHistoryService _hrSalaryHistoryService;
        private readonly IAllowanceSheetService _allowanceSheetService;
        private readonly IFestivalBonusSettingService _festivalBonusSettingService;
        private readonly IFestivalBonusSheetService _festivalBonusSheetService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IDesignationService _designationService;

        private List<UserMenu> _userMenu;
        public FestivalBonusSheetController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _campusService = new CampusService(session);
            _branchService = new BranchService(session);
            _userService = new UserService(session);
            _hrMemberService = new TeamMemberService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _departmentService = new DepartmentService(session);
            _attendanceSummaryService = new AttendanceSummaryService(session);
            _shiftService = new ShiftService(session);
            _shiftWeekendHistoryService = new ShiftWeekendHistoryService(session);
            _hrSalaryHistoryService = new SalaryHistoryService(session);
            _allowanceSheetService = new AllowanceSheetService(session);
            _festivalBonusSettingService = new FestivalBonusSettingService(session);
            _festivalBonusSheetService = new FestivalBonusSheetService(session);
            _teamMemberService = new TeamMemberService(session);
            _designationService = new DesignationService(session);
        }
        #endregion

        // GET: Hr/FestivalBonusSheet
        public ActionResult Index()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            try
            {
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.BranchList = new MultiSelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new MultiSelectList(new List<Campus>(), "Id", "Name");
                ViewBag.DepartmentList = new MultiSelectList(new List<Department>(), "Id", "Name");
                ViewBag.FestivalBonusSettingList = new MultiSelectList(new List<FestivalBonusSetting>(), "Id", "Name");

                var organizationTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<OrganizationType>(), "Key", "Value");

                ViewBag.OrganizationTypeList = organizationTypeList;
                ViewBag.FestivalBonusSheetMonthList = new SelectList(_commonHelper.LoadEmumToDictionary<MonthsOfYear>(), "Key", "Value");
                int initialYear = DateTime.Now.Date.Year - 1;
                int i;
                var yearDictionary = new Dictionary<int, string>();
                for (i = initialYear; i <= DateTime.Now.Date.Year; i++)
                {
                    yearDictionary.Add(i, i.ToString());
                }
                ViewBag.FestivalBonusSheetYearList = new SelectList(yearDictionary, "Key", "Value");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public PartialViewResult Index(FestivalBonusSheetFormViewModel viewModel)
        {
            try
            {
                List<FestivalBonusSheetDto> patients = new List<FestivalBonusSheetDto>();
                List<int> religionList = new List<int>();
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (ModelState.IsValid)
                {
                    var lastDayOfMonth = DateTime.DaysInMonth(viewModel.BonusYear,viewModel.BonusMonth);
                    DateTime startDate = new DateTime(viewModel.BonusYear, viewModel.BonusMonth, 1);
                    DateTime endDate = new DateTime(viewModel.BonusYear, viewModel.BonusMonth, lastDayOfMonth);
                    var model = _festivalBonusSettingService.GetFestivalBonusSetting(viewModel.FestivalBonusSettingId, viewModel.OrganizationId);
                    religionList = ReligionList(model);
                    patients = _teamMemberService.LoadTeamMemberListForFbSheet(_userMenu, startDate,endDate, religionList, viewModel);
                    bool isSubmit = false;
                    if (patients != null && patients.Any())
                    {
                        List<long> teamMemberIds = patients.Select(x => x.TeamMemberId).ToList();
                        var fbsList = _festivalBonusSheetService.LoadFestivalBonusSheetsList(teamMemberIds, viewModel.FestivalBonusSettingId, startDate, endDate);
                        if (fbsList != null && fbsList.Any())
                            isSubmit = true;
                    }
                    ViewBag.IsSubmit = isSubmit;
                    ViewBag.Organization = _organizationService.LoadById(viewModel.OrganizationId) != null ? _organizationService.LoadById(viewModel.OrganizationId).Name : "";
                    ViewBag.Branch = viewModel.BranchId == 0 ? "All Branch" : _branchService.GetBranch(viewModel.BranchId) == null ? "" : _branchService.GetBranch(viewModel.BranchId).Name;

                    ViewBag.Campus = viewModel.CampusId == 0 ? "All Campus" : _campusService.GetCampus(viewModel.CampusId) == null ? "" : _campusService.GetCampus(viewModel.CampusId).Name;
                    ViewBag.Department = viewModel.DepartmentId == 0 ? "All Department" : _departmentService.LoadById(viewModel.DepartmentId) == null ? "" : _departmentService.LoadById(viewModel.DepartmentId).Name;
                    ViewBag.Month = viewModel.BonusMonth;
                    ViewBag.Year = viewModel.BonusYear;
                    ViewBag.StartDate = startDate;
                    ViewBag.EndDate = endDate;
                    ViewBag.ViewModel = viewModel;
                }
                return PartialView("Partial/_TeamMemberListForFestivalBonusSheet", patients);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.Errormessage = ex.Message;
                throw;
            }
        }

        [HttpPost]
        public ActionResult SaveDraftFestivalBonusSheet(string feeTypeList)
        {
            string errorMessage = "";
            try
            {
                dynamic patientsList = JsonConvert.DeserializeObject(feeTypeList);
                var objectList = new List<FestivalBonusSheet>();
                bool checkIsSubmit = false;
                foreach (var patient in patientsList)
                {
                    checkIsSubmit = patient.IsSubmit;
                    var obj = new FestivalBonusSheet()
                    {
                        TeamMember = _teamMemberService.LoadById(Convert.ToInt64(patient.TeamMemberId)),
                        FestivalBonusSetting = _festivalBonusSettingService.LoadById(Convert.ToInt64(patient.FestivalBonusSettingId)),
                        SalaryOrganization = _organizationService.LoadById(Convert.ToInt64(patient.SalaryOrganizationId)),
                        SalaryBranch = _branchService.GetBranch(Convert.ToInt64(patient.SalaryBranchId)),
                        SalaryCampus = _campusService.GetCampus(Convert.ToInt64(patient.SalaryCampusId)),
                        SalaryDepartment = _departmentService.LoadById(Convert.ToInt64(patient.SalaryDepartmentId)),
                        SalaryDesignation = _designationService.LoadById(Convert.ToInt64(patient.SalaryDesignationId)),
                        JobOrganization = _organizationService.LoadById(Convert.ToInt64(patient.JobOrganizationId)),
                        JobBranch = _branchService.GetBranch(Convert.ToInt64(patient.JobBranchId)),
                        JobCampus = _campusService.GetCampus(Convert.ToInt64(patient.JobCampusId)),
                        JobDepartment = _departmentService.LoadById(Convert.ToInt64(patient.JobDepartmentId)),
                        JobDesignation = _designationService.LoadById(Convert.ToInt64(patient.JobDesignationId)),
                        BankAccount = patient.BankAmount,
                        CashAccount = patient.CashAmount,
                        Remarks = patient.Remarks,
                        IsSubmit = patient.IsSubmit,
                        StartDate = Convert.ToDateTime(patient.StartDate),
                        EndDate = Convert.ToDateTime(patient.EndDate),
                        Year = patient.Year,
                        Month = patient.Month
                    };
                    objectList.Add(obj);
                }
                _festivalBonusSheetService.SaveOrUpdate(objectList);
                return Json(new { returnResult = "Successfully save", IsSuccess = true, checkIsSubmit = checkIsSubmit });
            }
            catch (InvalidDataException ex)
            {
                errorMessage = ex.Message;
            }
            catch (NullObjectException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.Errormessage = ex.Message;
                errorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new { returnResult = errorMessage, IsSuccess = false });
        }
        
        public JsonResult ExportMemberListForFestivalBonusSheet(string fbViewModel)
        {
            try
            {
                if(fbViewModel==null)
                    throw new InvalidDataException("Invalid export.");
                var viewModel = JsonConvert.DeserializeObject<FestivalBonusSheetFormViewModel>(fbViewModel);
                
                List<FestivalBonusSheetDto> patients = new List<FestivalBonusSheetDto>();
                List<int> religionList = new List<int>();
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var lastDayOfMonth = DateTime.DaysInMonth(viewModel.BonusYear, viewModel.BonusMonth);
                DateTime startDate = new DateTime(viewModel.BonusYear, viewModel.BonusMonth, 1);
                DateTime endDate = new DateTime(viewModel.BonusYear, viewModel.BonusMonth, lastDayOfMonth);
                var model = _festivalBonusSettingService.GetFestivalBonusSetting(viewModel.FestivalBonusSettingId, viewModel.OrganizationId);
                religionList = ReligionList(model);
                patients = _teamMemberService.LoadTeamMemberListForFbSheet(_userMenu, startDate, endDate, religionList, viewModel);

                var data = new List<List<object>>();
                var headerList = new List<string>();
                var columnList = new List<string>();
                var footerList = new List<string>();
                string organizationName = _organizationService.LoadById(viewModel.OrganizationId) != null ? _organizationService.LoadById(viewModel.OrganizationId).Name : "";
                var branch = viewModel.BranchId == 0 ? "All Branch" : _branchService.GetBranch(viewModel.BranchId) == null ? "" : _branchService.GetBranch(viewModel.BranchId).Name;

                var campus = viewModel.CampusId == 0 ? "All Campus" : _campusService.GetCampus(viewModel.CampusId) == null ? "" : _campusService.GetCampus(viewModel.CampusId).Name;
                var department = viewModel.DepartmentId == 0 ? "All Department" : _departmentService.LoadById(viewModel.DepartmentId) == null ? "" : _departmentService.LoadById(viewModel.DepartmentId).Name;

                headerList.Add(organizationName);
                headerList.Add(branch);
                headerList.Add(campus);
                headerList.Add(department);
                headerList.Add("Festival Bonus Sheet-"+Enum.GetName(typeof(MonthsOfYear), viewModel.BonusMonth) + " " + viewModel.BonusYear);
                footerList.Add("");

                columnList.Add("PIN");
                columnList.Add("Name");
                columnList.Add("Designation");
                columnList.Add("Member Type");
                columnList.Add("Bank Amount");
                columnList.Add("Cash Amount");
                columnList.Add("Total Amount");
                columnList.Add("Remarks");

                if (patients != null && patients.Any())
                {
                    foreach (var teamMember in patients)
                    {
                        var str = new List<object>();
                        str.Add(teamMember.Pin);
                        str.Add(teamMember.Name);
                        str.Add(teamMember.Designation);
                        str.Add(teamMember.EmploymentStatus);
                        str.Add(teamMember.BankAmount);
                        str.Add(teamMember.CashAmount);
                        str.Add(teamMember.TotalAmount);
                        str.Add(teamMember.Remarks);
                        data.Add(str);
                    }
                    ExcelGenerator.GenerateExcel(headerList, columnList, data, footerList,
                    "festivalBonusSheet");
                }
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return Json(HttpNotFound());

        }

        private List<int> ReligionList(FestivalBonusSetting model)
        {
            var religionList = new List<int>();
            if (model.IsBuddhism) religionList.Add((int)Religion.Buddhism);
            if (model.IsChristianity) religionList.Add((int)Religion.Christianity);
            if (model.IsHinduism) religionList.Add((int)Religion.Hinduism);
            if (model.IsIslam) religionList.Add((int)Religion.Islam);
            if (model.IsOthers) religionList.Add((int)Religion.Other);
            return religionList;
        }
    }
}