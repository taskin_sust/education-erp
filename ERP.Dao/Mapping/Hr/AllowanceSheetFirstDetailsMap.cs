using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class AllowanceSheetFirstDetailsMap : BaseClassMap<AllowanceSheetFirstDetails,long> {

        public AllowanceSheetFirstDetailsMap()
        {
			Table("HR_AllowanceSheetFirstDetails");
			LazyLoad();
            References(x => x.AllowanceSheet).Column("AllowanceSheetId");
            References(x => x.TeamMemberDepartment).Column("TeamMemberDepartmentId");
            References(x => x.TeamMemberDesignation).Column("TeamMemberDesignationId");
            References(x => x.TeamMemberBranch).Column("TeamMemberBranchId");
            References(x => x.TeamMemberCampus).Column("TeamMemberCampusId");
            References(x => x.DailySupportAllowanceSetting).Column("DailySupportAllowanceSettingId");
            References(x => x.ExtradayAllowanceSetting).Column("ExtradayAllowanceSettingId");
            References(x => x.OvertimeAllowanceSetting).Column("OvertimeAllowanceSettingId");
            References(x => x.NightWorkAllowanceSetting).Column("NightWorkAllowanceSettingId");
            Map(x => x.DailySupportAllowanceAmount).Column("DailySupportAllowanceAmount");
            Map(x => x.ExtradayAllowanceAmount).Column("ExtradayAllowanceAmount");
            Map(x => x.OvertimeAllowanceAmount).Column("OvertimeAllowanceAmount");
            Map(x => x.NightWorkAllowanceAmount).Column("NightWorkAllowanceAmount");
            Map(x => x.TotalDailyAllowanceAmount).Column("TotalDailyAllowanceAmount");
            Map(x => x.AllowanceDate).Column("AllowanceDate");
        }
    }
}
