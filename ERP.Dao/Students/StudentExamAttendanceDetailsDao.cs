﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Students
{
    public interface IStudentExamAttendanceDetailsDao : IBaseDao<StudentExamAttendanceDetails, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

    }

    public class StudentExamAttendanceDetailsDao : BaseDao<StudentExamAttendanceDetails, long>, IStudentExamAttendanceDetailsDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function


        #endregion

        #region Helper Function

        #endregion
    }
}