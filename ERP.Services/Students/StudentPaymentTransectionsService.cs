﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Util;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IStudentPaymentTransectionsService : IBaseService
    {
        #region Operational Function

        bool TransactionsSettlementFinalPost(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, List<long> studentPaymentTransectionsIdList, long userId);
        void StudentPaymentTransectionsSave(StudentPaymentTransections paymentTransections);
        void Delete(StudentPaymentTransections paymentTransections);

        #endregion

        #region Single Instances Loading Function

        StudentPaymentTransections GetPaymentTransections(long id);

        #endregion

        #region List Loading Function
        IList<StudentPaymentTransections> GetTransactionReportVat(long[] sessionId, long[] programId, long[] branchId, long[] campusId, long[] authorizedPrograms, long[] authorizedSessions, List<long> authorizedBranches, long[] authorizedCampuses, DateTime datef, DateTime datet, int paymentMethod, int start, int length, int orderType);

        IList<StudentPaymentTransections> LoadTransactionsSettlementCalculate(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, decimal totalAmount);
        List<dynamic> LoadBranchWiseSummery(long branchId, DateTime dateFrom, DateTime dateTo);
        #endregion

        #region Others Function
        int GetTransactionReportCountVat(long[] sessionId, long[] programId, long[] branchId, long[] campusId, long[] authorizedPrograms, long[] authorizedSessions, List<long> authorizedBranches, long[] authorizedCampuses, DateTime datef, DateTime datet, int paymentMethod, int orderType);
        DateTime GetTransactionsSettlementStartDate(long sessionId, long programId, long branchId, long campusId);
        decimal GetTotalCollectionAmountByReferenceId(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, long referenceId);
        decimal GetTotalCollectionAmountByPaymentMethod(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, int paymentMethod);
        decimal GetPreSettledAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo);
        #endregion
        
    }
    public class StudentPaymentTransectionsService : BaseService, IStudentPaymentTransectionsService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentPaymentTransectionsDao _studentPaymentTransectionsDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly IStudentCourseDetailDao _studentCourseDetailDao;
        private readonly IProgramDao _programDao;
        private readonly IBranchDao _branchDao;
        private readonly ISessionDao _sessionDao;
        private readonly ICampusDao _campusDao;
        public StudentPaymentTransectionsService(ISession session)
        {
            Session = session;
            _studentPaymentTransectionsDao = new StudentPaymentTransectionsDao() { Session = Session };
            _studentProgramDao = new StudentProgramDao() { Session = Session };
            _studentCourseDetailDao = new StudentCourseDetailDao() { Session = Session };
            _programDao = new ProgramDao { Session = Session };
            _branchDao = new BranchDao { Session = Session };
            _sessionDao = new SessionDao { Session = Session };
            _campusDao = new CampusDao { Session = Session };
        }
        #endregion

        #region Operational Function
        public bool TransactionsSettlementFinalPost(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, List<long> studentPaymentTransectionsIdList, long userId)
        {
            try
            {
                return _studentPaymentTransectionsDao.TransactionsSettlementFinalPost(sessionId, programId, branchId, campusId, dateFrom, dateTo, studentPaymentTransectionsIdList, userId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public void StudentPaymentTransectionsSave(StudentPaymentTransections paymentTransections)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    //check before save
                    CheckBeforeSave(paymentTransections);
                    _studentPaymentTransectionsDao.SaveOrUpdate(paymentTransections);
                    transaction.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void Delete(StudentPaymentTransections paymentTransections)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    paymentTransections.Status = StudentPaymentOnlineTransactions.EntityStatus.Delete;
                    _studentPaymentTransectionsDao.SaveOrUpdate(paymentTransections);
                    transaction.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public StudentPaymentTransections GetPaymentTransections(long id)
        {
            try
            {
                return _studentPaymentTransectionsDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        private void CheckBeforeSave(StudentPaymentTransections paymentTransections)
        {
            if (paymentTransections.ProgramId <= 0)
                throw new InvalidDataException("Program Not Found!");
            Program program = _programDao.LoadById(paymentTransections.ProgramId);
            if (program == null)
                throw new InvalidDataException("Program Not Found!");

            if (paymentTransections.SessionId <= 0)
                throw new InvalidDataException("Session Not Found!");
            Session session = _sessionDao.LoadById(paymentTransections.SessionId);
            if (session == null)
                throw new InvalidDataException("Session Not Found!");

            if (paymentTransections.BranchId <= 0)
                throw new InvalidDataException("Branch Not Found!");
            Branch branch = _branchDao.LoadById(paymentTransections.BranchId);
            if (branch == null)
                throw new InvalidDataException("Branch Not Found!");

            if (paymentTransections.CampusId <= 0)
                throw new InvalidDataException("Campus Not Found!");
            Campus campus = _campusDao.LoadById(paymentTransections.CampusId);
            if (campus == null)
                throw new InvalidDataException("Campus Not Found!");

            if (String.IsNullOrEmpty(paymentTransections.StudentFullName))
                throw new InvalidDataException("Student Full Name Is Empty!");
            if (String.IsNullOrEmpty(paymentTransections.StudentNickName))
                throw new InvalidDataException("Student Nick Name Is Empty!");
            if (paymentTransections.Serial1 <= 0)
                throw new InvalidDataException("Serial1 Is less Then Or Equal To Zero!");
            if (paymentTransections.ReceivedAmount <= 0)
                throw new InvalidDataException("Received Amount Is less Then Or Equal To Zero!");

        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<StudentPaymentTransections> GetTransactionReportVat(long[] sessionId, long[] programId, long[] branchId, long[] campusId, long[] authorizedPrograms, long[] authorizedSessions, List<long> authorizedBranches, long[] authorizedCampuses, DateTime datef, DateTime datet, int paymentMethod, int start, int length, int orderType)
        {
            try
            {
                return _studentPaymentTransectionsDao.GetTransactionReportVat(sessionId, programId, branchId, campusId, authorizedPrograms, authorizedSessions, authorizedBranches, authorizedCampuses, datef, datet, paymentMethod, start, length, orderType);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<StudentPaymentTransections> LoadTransactionsSettlementCalculate(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, decimal totalAmount)
        {
            try
            {
                return _studentPaymentTransectionsDao.LoadTransactionsSettlementCalculate(sessionId, programId, branchId, campusId, dateFrom, dateTo, totalAmount);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<dynamic> LoadBranchWiseSummery(long branchId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return _studentPaymentTransectionsDao.LoadBranchWiseSummery(branchId, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function
        public int GetTransactionReportCountVat(long[] sessionId, long[] programId, long[] branchId, long[] campusId, long[] authorizedPrograms, long[] authorizedSessions, List<long> authorizedBranches, long[] authorizedCampuses, DateTime datef, DateTime datet, int paymentMethod, int orderType)
        {
            try
            {
                return _studentPaymentTransectionsDao.GetTransactionReportCountVat(sessionId, programId, branchId, campusId, authorizedPrograms, authorizedSessions, authorizedBranches, authorizedCampuses, datef, datet, paymentMethod, orderType);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        public DateTime GetTransactionsSettlementStartDate(long sessionId, long programId, long branchId, long campusId)
        {
            try
            {
                return _studentPaymentTransectionsDao.GetTransactionsSettlementStartDate(sessionId, programId, branchId, campusId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public decimal GetTotalCollectionAmountByReferenceId(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, long referenceId)
        {
            try
            {
                return _studentPaymentTransectionsDao.GetTotalCollectionAmountByReferenceId(sessionId, programId, branchId, campusId, dateFrom, dateTo, referenceId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public decimal GetTotalCollectionAmountByPaymentMethod(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo, int paymentMethod)
        {
            try
            {
                return _studentPaymentTransectionsDao.GetTotalCollectionAmountByPaymentMethod(sessionId, programId, branchId, campusId, dateFrom, dateTo, paymentMethod);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public decimal GetPreSettledAmount(long sessionId, long programId, long branchId, long campusId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                return _studentPaymentTransectionsDao.GetPreSettledAmount(sessionId, programId, branchId, campusId, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Helper Function

        #endregion
    }
}

