﻿using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Mapping.UserAuth
{
    public partial class MenuAccessMap:BaseClassMap<MenuAccess,long>
    {
        public MenuAccessMap()
        {
            Table("MenuAccess");
            LazyLoad();
            //Not.LazyLoad();
            Map(x => x.HasReferrer);
            References(x => x.Menu).Column("MenuId").LazyLoad();
            References(x => x.Actions).Column("ActionsId").LazyLoad();
        }
    }
}
