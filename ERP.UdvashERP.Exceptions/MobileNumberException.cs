﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.MessageExceptions
{
    public class MobileNumberException : Exception
    {

        public MobileNumberException() : base("Incorrect Mobile Number")
        {
            
        }


        public MobileNumberException(string message): base(message)
        {

        }
    }
}
