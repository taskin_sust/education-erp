﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class AttendanceDeviceTypeFactory : ObjectFactoryBase<AttendanceDeviceType>
    {
        private readonly IAttendanceDeviceTypeService _attendanceDeviceTypeService;
        public AttendanceDeviceTypeFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _attendanceDeviceTypeService = new AttendanceDeviceTypeService(session);
        }

        public AttendanceDeviceTypeFactory Create()
        {
            Object = new AttendanceDeviceType()
            {
                Name = "DeviceType" + Guid.NewGuid(),
                Code = "Code" + Guid.NewGuid(),
                IsFinger = true,
                FirmwareVersion = (float)1.50,
                AlgorithmVersion = (float)2.558,
                AttendanceLogCapacity = 8000,
                UserCapacity = 5000,
                TemplateCapacity = 7000
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public AttendanceDeviceTypeFactory Persist()
        {
            SingleObjectList.ForEach(item =>
            {
                if (item.Id == 0)
                {
                    _attendanceDeviceTypeService.SaveOrUpdate(item);
                }
            });
            ObjectList.ForEach(item =>
            {
                if (item.Id == 0)
                {
                    _attendanceDeviceTypeService.SaveOrUpdate(item);
                }
            });
            return this;
        }

        public void CleanUp()
        {
            DeleteAll();
        }
    }
}
