﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.AttendanceDeviceTypeTest
{
    public class AttendanceDeviceTypeBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        private ISession _session;
        protected readonly AttendanceDeviceTypeFactory AttendanceDeviceTypeFactory;
        protected readonly IAttendanceDeviceTypeService _attendanceDeviceTypeService;

        protected readonly IMemberFingerPrintService _memberFingerPrintService;

        #endregion
        public AttendanceDeviceTypeBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            AttendanceDeviceTypeFactory = new AttendanceDeviceTypeFactory(null, session: _session);
            _attendanceDeviceTypeService = new AttendanceDeviceTypeService(session: _session);
            _memberFingerPrintService = new MemberFingerPrintService(session: _session);
        }

        public void Dispose()
        {
            if (AttendanceDeviceTypeFactory != null && AttendanceDeviceTypeFactory.Object != null)
            {
                string query = "delete from Hr_MemberFingerPrint where AttendanceDeviceTypeId=" + AttendanceDeviceTypeFactory.Object.Id;
                _session.CreateSQLQuery(query).ExecuteUpdate();
                const string queryS = "delete from Hr_MemberFingerPrintLog ";
                _session.CreateSQLQuery(queryS).ExecuteUpdate();
            }
            AttendanceDeviceTypeFactory.CleanUp();
            base.Dispose();
        }

    }
}
