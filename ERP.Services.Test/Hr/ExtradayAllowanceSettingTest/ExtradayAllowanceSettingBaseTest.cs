﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.ExtradayAllowanceSettingTest
{
    public class ExtradayAllowanceSettingBaseTest : TestBase, IDisposable
    {
        #region Init
        //TODO: Remove those variable which is already declared in TestBase

        protected readonly ExtradayAllowanceSettingService _extradayAllowanceSettingService;
        protected readonly ExtradayAllowanceSettingFactory _extradayAllowanceSettingFactory;
        internal readonly string _name = "SettingName_" + DateTime.Now.Second + "_" + DateTime.Now.Millisecond;
        
        #endregion
        
        public ExtradayAllowanceSettingBaseTest()
        {
            //TODO: Remove initialization of those variable which is already initialized in TestBase
            _extradayAllowanceSettingService = new ExtradayAllowanceSettingService(Session);
            _extradayAllowanceSettingFactory = new ExtradayAllowanceSettingFactory(null, Session);
        }

        public ISession TestSession
        {
            get { return Session; }
            set { Session = value; }
        }

        #region disposal

        //TODO: use new keyword for hiding base Dispose method
        public new void Dispose()
        {
            _extradayAllowanceSettingFactory.CleanUp();
            base.Dispose();
        }

        #endregion
    }
}
