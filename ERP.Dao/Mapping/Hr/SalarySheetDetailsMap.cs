﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Hr
{
    public class SalarySheetDetailsMap:BaseClassMap<SalarySheetDetails,long>
    {
        public SalarySheetDetailsMap()
        {
            Table("HR_SalarySheetDetails");
            LazyLoad();
            Map(x => x.ZoneCount).Column("ZoneCount");
            Map(x => x.ZoneUnitAmount).Column("ZoneUnitAmount");
            Map(x => x.ZoneDeductionMultiplier).Column("ZoneDeductionMultiplier");
            Map(x => x.DeductionPerAbsent).Column("DeductionPerAbsent");
            Map(x => x.TotalAbsentDay).Column("TotalAbsentDay");
            Map(x => x.AbsentMultiplierFactor).Column("AbsentMultiplierFactor");
            Map(x => x.IsBasic).Column("IsBasic");
            References(x => x.SalarySheet).Column("SalarySheetId");
            References(x => x.ZoneSetting).Column("ZoneSettingId");
        }
    }
}
