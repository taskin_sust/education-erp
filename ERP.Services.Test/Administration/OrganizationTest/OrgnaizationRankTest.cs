﻿using System;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.OrganizationTest
{
    interface IOrganizationRankTest
    {
        #region Basic Test

        [Fact]
        void Get_Maximum_Rank_Not_Null_Check();
        void Get_Minimum_Rank_Not_Null_Check();
        #endregion

        #region Business Logic Test
        
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Organization")]
    public class OrganizationRankTest : OrganizationBaseTest, IOrganizationRankTest
    {
        #region Basic Test

        [Fact]
        public void Get_Maximum_Rank_Not_Null_Check()
        {
            OrganizationFactory organizationFactory =
                    _organizationFactory.Create()
                    .Persist();
            int maxRank = _organizationService.GetMaxRank(organizationFactory.Object);
            Assert.True(maxRank>0);

        }

        [Fact]
        public void Get_Minimum_Rank_Not_Null_Check()
        {
            OrganizationFactory organizationFactory =
                     _organizationFactory.Create()
                     .Persist();
            int minRank = _organizationService.GetMinRank(organizationFactory.Object);
            Assert.True(minRank > 0);

        }
        #endregion
        
        #region Business Logic Test
        
        #endregion
    }
}
