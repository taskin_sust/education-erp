﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.AdminisTration.OrganizationTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.GoodsReturnTest
{
    public interface IGoodsReturnSaveTest
    {
        #region Basic Test

        void Save_Should_Return_Null_Exception();
        void Should_Save_Successfully();

        #endregion

        #region Business Logic Test

        void Save_Should_Check_Returned_Quantity_Value();
        void Save_Should_Check_Invalid_Entry(int i);

        #endregion
    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReturn")]
    public class GoodsReturnSaveTest : GoodsReturnBaseTest, IGoodsReturnSaveTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<InvalidDataException>(() => _goodsReturnService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            _goodsReturnFactory.Create().WithGoodsReturnDetailsAndGoodsReceive().Persist();
            var goodsreturn = _goodsReturnService.GetGoodsReturn(_goodsReturnFactory.Object.Id);
            Assert.NotNull(goodsreturn);
            Assert.True(goodsreturn.Id > 0);
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Save_Should_Check_Returned_Quantity_Value()
        {
            var goodsReturn = _goodsReturnFactory.Create().WithGoodsReturnDetailsAndGoodsReceive().Object;
            goodsReturn.GoodsReturnDetails[0].ReturnedQuantity = goodsReturn.GoodsReceive.GoodsReceiveDetails[0].ReceivedQuantity + 1;
            Assert.Throws<OverflowException>(() => _goodsReturnFactory.Persist());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void Save_Should_Check_Invalid_Entry(int i)
        {
            var goodsReturn = _goodsReturnFactory.Create().WithGoodsReturnDetailsAndGoodsReceive().Object;
            if (i == 1)
            {
                goodsReturn.GoodsReturnDetails[0].ReturnedQuantity = 0;
            }
            if (i == 2)
            {
                goodsReturn.GoodsReturnDetails = null;
            }
            Assert.Throws<InvalidDataException>(() => _goodsReturnFactory.Persist());
        }

        #endregion
    }
}
