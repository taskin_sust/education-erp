﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class ChildrenInfoFactory : ObjectFactoryBase<ChildrenInfo>
    {
        private readonly IChildrenInfoService _childrenInfoService;
        private readonly TestBaseService<ChildrenInfoLog> _childrenInfoLogService; 
        private TeamMemberFactory _teamMemberFactory;
        public ChildrenInfoFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _childrenInfoService = new ChildrenInfoService(session);
            _childrenInfoLogService = new TestBaseService<ChildrenInfoLog>(session);
        }

        #region Create
        public ChildrenInfoFactory Create()
        {
            Object = new ChildrenInfo()
            {
                Name = "Jon",
                Dob = DateTime.Now,
                Gender = (int)Gender.Male,
                IsStudying = true,
                Remarks = "testing remarks",
                BusinessId = GetPcUserName()
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public ChildrenInfoFactory CreateWith(string name, DateTime dob, int? gender, bool isStudying, string remarks)
        {
            Object = new ChildrenInfo()
            {
                Name = name,
                Dob = dob,
                Gender = gender,
                IsStudying = isStudying,
                Remarks = remarks,
                BusinessId = GetPcUserName()
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public ChildrenInfoFactory WithTeamMember()
        {
            _teamMemberFactory.Create().Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }

        public ChildrenInfoFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }
      

        #endregion

        #region Others

        public IList<ChildrenInfo> CreateMore(int no)
        {
            for (int index = 0; index < no; index++)
            {
                var _object = new ChildrenInfo()
                {
                    Dob = new DateTime(2015, 10, 20),
                    IsStudying = false,
                    Status = SalaryHistory.EntityStatus.Active
                };
                ObjectList.Add(_object);
            }
            return ObjectList;
        }

        #endregion

        #region CleanUp
        public ChildrenInfoFactory CleanUp()
        {
            DeleteAll();
            _teamMemberFactory.Cleanup();
            return this;
        }
        public void LogCleanUp(TeamMember teamMember)
        {
            _childrenInfoLogService.DeleteChildrenInfoLogByChildrenInfo(teamMember);
        }
        public ChildrenInfoFactory CleanUpByTeamMember(long teamMemberId)
        {
            string queryFinal = "delete from HR_ChildrenInfo where TeamMemberId in ( " + string.Join(",", teamMemberId) + ")";
            _session.CreateSQLQuery(queryFinal).ExecuteUpdate();
            return this;
        }
        #endregion

        
    }

}
