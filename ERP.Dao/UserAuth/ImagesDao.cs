﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.UserAuth
{
    public interface IImagesDao : IBaseDao<Images, long>
    {

    }

    public class ImagesDao : BaseDao<Images, long>, IImagesDao
    {

    }
}
