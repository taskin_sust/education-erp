﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Teachers
{

    public interface ITeacherPublicRegistrationDao : IBaseDao<TeacherPublicRegistration, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function

        IList<TeacherPublicRegistration> LoadRegisterTeacherList(int start, int length, string orderBy, string orderDirection, List<long> authoOrganizationIdList, List<long> teacherActivityIdList, List<long> subjectIdList, List<long> religionIdList, List<long> versionOfStudies, List<long> genderIdList, int? academicStudentStatus, int? teacherApplicationStatus, string nickName, string institute, string mobileNumber);

        
        #endregion

        #region Others Function
        int GetRegisterTeacherCount(List<long> authoOrganizationIdList, List<long> teacherActivityIdList, List<long> subjectIdList, List<long> religionIdList, List<long> versionOfStudies, List<long> genderIdList, int? academicStudentStatus, int? teacherApplicationStatus, string nickName, string institute, string mobileNumber);
        bool CheckDublicateMobileNumber(string mobileNumber);
        
        #endregion

    }

    public class TeacherPublicRegistrationDao : BaseDao<TeacherPublicRegistration, long>, ITeacherPublicRegistrationDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function
        
        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function

        public IList<TeacherPublicRegistration> LoadRegisterTeacherList(int start, int length, string orderBy, string orderDirection, List<long> authoOrganizationIdList, List<long> teacherActivityIdList, List<long> subjectIdList, List<long> religionIdList, List<long> versionOfStudies, List<long> genderIdList, int? academicStudentStatus, int? teacherApplicationStatus, string nickName, string institute, string mobileNumber)
        {
            ICriteria criteria = GetRegisterTeacherCriteriaQuery(authoOrganizationIdList, teacherActivityIdList, subjectIdList, religionIdList, versionOfStudies, genderIdList, academicStudentStatus, teacherApplicationStatus, nickName, institute, mobileNumber);

            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(orderDirection == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }
            return criteria.SetFirstResult(start).SetMaxResults(length).List<TeacherPublicRegistration>();
        }
        
        #endregion

        #region Others Function

        public int GetRegisterTeacherCount(List<long> authoOrganizationIdList, List<long> teacherActivityIdList, List<long> subjectIdList, List<long> religionIdList, List<long> versionOfStudies, List<long> genderIdList, int? academicStudentStatus, int? teacherApplicationStatus, string nickName, string institute, string mobileNumber)
        {
            ICriteria criteria = GetRegisterTeacherCriteriaQuery(authoOrganizationIdList, teacherActivityIdList, subjectIdList, religionIdList, versionOfStudies, genderIdList, academicStudentStatus, teacherApplicationStatus, nickName, institute, mobileNumber);
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            criteria.SetProjection(Projections.CountDistinct("Id"));
            int count = Convert.ToInt32(criteria.UniqueResult());
            return count;
        }

        public bool CheckDublicateMobileNumber(string mobileNumber)
        {
            ICriteria criteria = Session.CreateCriteria<TeacherPublicRegistration>()
                .Add(Restrictions.Not(Restrictions.Eq("Status", TeacherPublicRegistration.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("MobileNumber1", mobileNumber));
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            criteria.SetProjection(Projections.CountDistinct("Id"));
            int count = Convert.ToInt32(criteria.UniqueResult());
            return count > 0;
        }

        #endregion

        #region Helper Function

        private ICriteria GetRegisterTeacherCriteriaQuery(List<long> authoOrganizationIdList, List<long> teacherActivityIdList, List<long> subjectIdList, List<long> religionIdList, List<long> versionOfStudies, List<long> genderIdList, int? academicStudentStatus, int? teacherApplicationStatus, string nickName, string institute, string mobileNumber)
        {
            ICriteria criteria = Session.CreateCriteria<TeacherPublicRegistration>().Add(Restrictions.Not(Restrictions.Eq("Status", TeacherPublicRegistration.EntityStatus.Delete)));

            criteria.CreateAlias("Organization", "o").Add(Restrictions.Eq("o.Status", Organization.EntityStatus.Active));
            criteria.CreateAlias("TeacherActivity1", "ta").Add(Restrictions.Eq("ta.Status", TeacherActivityPriority.EntityStatus.Active));
            criteria.CreateAlias("Subject1", "s").Add(Restrictions.Eq("s.Status", Subject.EntityStatus.Active));

            if (!String.IsNullOrEmpty(nickName))
            {
                criteria.Add(Restrictions.Like("NickName", nickName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(institute))
            {
                criteria.Add(Restrictions.Like("Institute", institute, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(mobileNumber))
            {
                var disjunctionMobileNumber = Restrictions.Disjunction(); // for OR statement 
                disjunctionMobileNumber.Add(Restrictions.Like("MobileNumber1", mobileNumber, MatchMode.Anywhere));
                disjunctionMobileNumber.Add(Restrictions.Like("MobileNumber2", mobileNumber, MatchMode.Anywhere));
                criteria.Add(disjunctionMobileNumber);
            }

            if (authoOrganizationIdList != null && !authoOrganizationIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("o.Id", authoOrganizationIdList));
            }
            if (teacherActivityIdList != null && !teacherActivityIdList.Contains(SelectionType.SelelectAll))
            {
                //criteria.Add(Restrictions.In("ta.Id", teacherActivityIdList));
                var disjunctionTeacherActivity = Restrictions.Disjunction(); // for OR statement 
                disjunctionTeacherActivity.Add(Restrictions.In("TeacherActivity1", teacherActivityIdList));
                disjunctionTeacherActivity.Add(Restrictions.In("TeacherActivity2", teacherActivityIdList));
                disjunctionTeacherActivity.Add(Restrictions.In("TeacherActivity3", teacherActivityIdList));
                criteria.Add(disjunctionTeacherActivity);
            }
            if (subjectIdList != null && !subjectIdList.Contains(SelectionType.SelelectAll))
            {
                var disjunctionSubject = Restrictions.Disjunction(); // for OR statement 
                disjunctionSubject.Add(Restrictions.In("Subject1", subjectIdList));
                disjunctionSubject.Add(Restrictions.In("Subject1", subjectIdList));
                disjunctionSubject.Add(Restrictions.In("Subject3", subjectIdList));
                criteria.Add(disjunctionSubject);
            }
            if (religionIdList != null && !religionIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("Religion", religionIdList));
            }
            if (versionOfStudies != null && !versionOfStudies.Contains(SelectionType.SelelectAll))
            {
                var disjunctionVersion = Restrictions.Disjunction(); // for OR statement 
                disjunctionVersion.Add(Restrictions.In("VersionOfStudy1", versionOfStudies));
                disjunctionVersion.Add(Restrictions.In("VersionOfStudy2", versionOfStudies));
                criteria.Add(disjunctionVersion);
            }
            if (genderIdList != null && !genderIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("Gender", genderIdList));
            }
            if (academicStudentStatus != null && academicStudentStatus != 0)
            {
                if (academicStudentStatus == 1)
                {
                    criteria.Add(Restrictions.Eq("IsUdvashStudent", true));
                }
                else if (academicStudentStatus == -1)
                {
                    criteria.Add(Restrictions.Eq("IsUdvashStudent", false));
                }
            }
            if (teacherApplicationStatus != null && teacherApplicationStatus != 0)
            {
                if (teacherApplicationStatus == 1)
                {
                    criteria.Add(Restrictions.Gt("Tpin", 0));
                    //criteria.Add(Restrictions.IsNotNull("Tpin"));
                }
                else if (teacherApplicationStatus == -1)
                {
                    //criteria.Add(Restrictions.IsNull("Tpin"));
                    var disjunctionApplicationStatus = Restrictions.Disjunction(); // for OR statement 
                    disjunctionApplicationStatus.Add(Restrictions.IsNull("Tpin"));
                    disjunctionApplicationStatus.Add(Restrictions.Le("Tpin", 0));
                    criteria.Add(disjunctionApplicationStatus);
                }
            }

            return criteria;
        }

        #endregion

    }
}
