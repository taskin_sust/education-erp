﻿using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IReferrerDao : IBaseDao<Referrer, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        Referrer LoadByRank(int rank);
        #endregion

        #region List Loading Function
        IList<Referrer> GetOnlyPageSizedReferrerList(int page, int pageSize, Referrer r);
        List<Referrer> GetReferrerList(int start, int length, string orderBy, string direction, string name, string code, string status, string rank);
        IList<Referrer> LoadReferrerByCode(string code, long id);
        IList<Referrer> LoadReferrerByName(string name, long id);
        #endregion

        #region Others Function
        bool HasDuplicateByName(string name, long id);
        int ReferrerRowCount(string name, string code, string status, string rank);
        #endregion
    }
    public class ReferrerDao : BaseDao<Referrer, long>, IReferrerDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public Referrer LoadByRank(int rank)
        {
            return Session.QueryOver<Referrer>().Where(x => x.Rank == rank).SingleOrDefault<Referrer>();
        }

        #endregion

        #region List Loading Function
        public IList<Referrer> LoadReferrerByCode(string code, long id)
        {
            return
                   Session.CreateCriteria<Referrer>()
                       .Add(Restrictions.Eq("Code", code))
                       .Add(Restrictions.Not(Restrictions.Eq("Status", Institute.EntityStatus.Delete)))
                       .Add(Restrictions.Not(Restrictions.Eq("Id", id)))
                       .List<Referrer>();
        }
        public IList<Referrer> LoadReferrerByName(string name, long id)
        {
            return
                   Session.CreateCriteria<Referrer>()
                       .Add(Restrictions.Eq("Name", name))
                       .Add(Restrictions.Not(Restrictions.Eq("Status", Institute.EntityStatus.Delete)))
                       .Add(Restrictions.Not(Restrictions.Eq("Id", id)))
                       .List<Referrer>();
        }
        public IList<Referrer> GetOnlyPageSizedReferrerList(int page, int pageSize, Referrer r)
        {
           
                string strvalue = "";
                int intValue;
                ICriteria criteria =Session.CreateCriteria<Referrer>().Add(Restrictions.Not(Restrictions.Eq("Status", Referrer.EntityStatus.Delete)));
                foreach (var data in r.GetType().GetProperties())
                {
                    object proval = data.GetValue(r);
                    if (data.GetValue(r) != null && data.GetValue(r) != "" && proval.ToString() != "0")
                    {
                        var type = data.PropertyType.Name;
                        if (type == "String")
                        {
                            strvalue = (string)data.GetValue(r);
                            criteria.Add(Restrictions.Like(data.Name, strvalue, MatchMode.Anywhere));
                        }
                        if (type == "Int32")
                        {
                            intValue = Convert.ToInt32(data.GetValue(r));
                            criteria.Add(Restrictions.Eq(data.Name, intValue));
                        }
                    }
                }
                return criteria.AddOrder(Order.Asc("Rank")).SetFirstResult((page - 1) * pageSize).SetMaxResults(pageSize).List<Referrer>();
          
        }
        public List<Referrer> GetReferrerList(int start, int length, string orderBy, string direction, string name, string code, string status, string rank)
        {
            ICriteria criteria = Session.CreateCriteria<Referrer>().Add(Restrictions.Not(Restrictions.Eq("Status", Referrer.EntityStatus.Delete)));
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(code))
            {
                criteria.Add(Restrictions.Like("Code", code, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }
            if (!string.IsNullOrEmpty(orderBy.Trim()))
            {
                if (direction == "ASC")
                {
                    criteria.AddOrder(Order.Asc(orderBy));
                }
                else
                {
                    criteria.AddOrder(Order.Desc(orderBy));
                } 
            }
            return (List<Referrer>)criteria.SetFirstResult(start).SetMaxResults(length).List<Referrer>();
        }
        #endregion

        #region Others Function

        public bool HasDuplicateByName(string name, long id)
        {
           
                IQuery query = Session.CreateQuery("from Referrer r where r.Name=:name and r.Status!=:status and r.Id!=:id");
                query.SetString("name", name);
                query.SetInt32("status", Referrer.EntityStatus.Delete);
                // if (id != null && id != 0)
                query.SetInt64("id", id);
                // else
                IList<Referrer> catList = query.List<Referrer>();

                if (catList == null || catList.Count < 1)
                    return false;
                else
                    return true;
          
        }
        public int ReferrerRowCount(string name, string code, string status, string rank)
        {
           
                ICriteria criteria =
                   Session.CreateCriteria<Referrer>();
                if (!String.IsNullOrEmpty(name))
                {
                    criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
                }
                if (!String.IsNullOrEmpty(code))
                {
                    criteria.Add(Restrictions.Like("Code", code, MatchMode.Anywhere));
                }
                if (!String.IsNullOrEmpty(status))
                {
                    criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
                }
                if (!String.IsNullOrEmpty(rank))
                {
                    criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
                }
                IList list = criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Referrer.EntityStatus.Delete))).List();
                return list.Count;
         
        }
        #endregion
    }
}