using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class NightWorkLogMap : ClassMap<NightWorkLog>
    {
        public NightWorkLogMap() {
			Table("HR_NightWorkLog");
			LazyLoad();
			References(x => x.NightWork).Column("NightWorkId");
			Map(x => x.NightWorkDate).Column("NightWorkDate");
			Map(x => x.ApprovalType).Column("ApprovalType");
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
        }
    }
}
