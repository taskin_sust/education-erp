using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class GoodsIssueDetailFactory : ObjectFactoryBase<GoodsIssueDetail>
   {

       private readonly IGoodsIssueDetailService _goodsIssueDetailService;

       public GoodsIssueDetailFactory(IObjectFactoryBase caller, ISession session): base(caller, session){

       _goodsIssueDetailService = new GoodsIssueDetailService(session);

       }

       #region Create 

       public GoodsIssueDetailFactory Create(){

       Object = null;
       SingleObjectList.Add(Object);
       return this;
       }

       public GoodsIssueDetailFactory CreateWith(long id)
       {

          Object = null;
          SingleObjectList.Add(Object);
          return this;

       }

       public GoodsIssueDetailFactory Persist()       {

          if (SingleObjectList != null && SingleObjectList.Count > 0){

              SingleObjectList.ForEach(x =>{if (x.Id == 0) _goodsIssueDetailService.SaveOrUpdate(x);});
          }

          if (ObjectList != null && ObjectList.Count > 0){

              ObjectList.ForEach(x =>{if (x.Id == 0)_goodsIssueDetailService.SaveOrUpdate(x);});

          }
          return this;
       }

       public GoodsIssueDetailFactory CreateMore(int num){

       for (int i = 0; i < num; i++){
       var obj = new GoodsIssueDetail(){};
       ObjectList.Add(obj);}
       return this;}

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public GoodsIssueDetailFactory CleanUp()
       {

          DeleteAll();
          return this;
       }

       #endregion

    }
}
