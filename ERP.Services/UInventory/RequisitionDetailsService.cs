using log4net;
using NHibernate;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
public interface IRequisitionDetailService : IBaseService
{
    #region Operational Function

    bool SaveOrUpdate(RequisitionDetails requisitionDetail);

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function

    #endregion

    #region Helper Function

    #endregion
}
public class RequisitionDetailsService : BaseService, IRequisitionDetailService {
    #region Logger
    private readonly ILog _logger = LogManager.GetLogger("InventoryService");
    #endregion

    #region Propertise & Object Initialization
    private readonly CommonHelper _commonHelper;
    private readonly IRequisitionDetailDao _requisitionDetailDao;
    public RequisitionDetailsService(ISession session)
    {
        Session = session;
        _commonHelper = new CommonHelper();
        _requisitionDetailDao=new RequisitionDetailsDao{Session = session};}
    #endregion

    #region Operational Functions

    public bool SaveOrUpdate(RequisitionDetails requisitionDetail)
    {

        return true;
    }

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function


    #endregion

    #region Helper function



    #endregion
}
}
                                    
