﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using Remotion.Linq.Parsing;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Dao.Base;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface ISalarySheetService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(List<SalarySheet> objectList);

        #endregion

        #region Single Instances Loading Function

        SalarySheet LoadById(long id);
        SalarySheetDto GetSalarySheetForFirstMonthTeamMember(List<UserMenu> _userMenu, SalarySheetFormViewModel viewModel, EmploymentHistory employmentHistory);

        #endregion

        #region List Loading Function

        List<SalarySheetDto> LoadTeamMemberForSalarySheet(List<UserMenu> userMenu, SalarySheetFormViewModel viewModel);

        List<SalarySheet> LoadSalarySheetList(List<UserMenu> _userMenu, SalarySheetFormViewModel viewModel);

        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion

    }
    public class SalarySheetService : BaseService, ISalarySheetService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly ISalarySheetDao _salarySheetDao;
        private readonly ILeaveApplicationDao _leaveApplicationDao;
        private readonly IAttendanceAdjustmentDao _attendanceAdjustmentDao;
        private readonly IAttendanceSummaryDao _attendanceSummaryDao;
        private readonly IDayOffAdjustmentDao _dayOffAdjustmentDao;
        private readonly ILeaveApplicationDetailsDao _leaveApplicationDetailsDao;

        public SalarySheetService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _salarySheetDao = new SalarySheetDao() { Session = session };
            _leaveApplicationDao = new LeaveApplicationDao() { Session = session };
            _attendanceAdjustmentDao = new AttendanceAdjustmentDao() { Session = session };
            _attendanceSummaryDao = new AttendanceSummaryDao(){Session = session};
            _dayOffAdjustmentDao = new DayOffAdjustmentDao(){Session = session};
            _leaveApplicationDetailsDao = new LeaveApplicationDetailsDao(){Session = session};
        }

        #endregion

        #region Operational Function

        public void SaveOrUpdate(List<SalarySheet> objectList)
        {
            ITransaction trans = null;
            try
            {
                string pendingText = "";
                if (objectList == null)
                    throw new InvalidDataException("Invalid data");
                using (trans = Session.BeginTransaction())
                {
                    foreach (var obj in objectList)
                    {
                        CheckBeforeSaveOrUpdate(obj);
                        
                        SalarySheet salarySheet = _salarySheetDao.GetTeamMemberSalarySheet(obj.TeamMember, obj.StartDate,
                            obj.EndDate, obj.SalaryHistory);

                        if (salarySheet == null)
                        {
                            obj.Status = SalarySheet.EntityStatus.Active;
                            salarySheet = obj;
                        }
                        else if (salarySheet != null && salarySheet.IsSubmit == false)
                        {

                            salarySheet.TeamMember = obj.TeamMember;
                            salarySheet.SalaryHistory = obj.SalaryHistory;
                            salarySheet.SalaryOrganization = obj.SalaryOrganization;
                            salarySheet.SalaryBranch = obj.SalaryBranch;
                            salarySheet.SalaryCampus = obj.SalaryCampus;
                            salarySheet.SalaryDepartment = obj.SalaryDepartment;
                            salarySheet.SalaryDesignation = obj.SalaryDesignation;
                            salarySheet.JobOrganization = obj.JobOrganization;
                            salarySheet.JobBranch = obj.JobBranch;
                            salarySheet.JobCampus = obj.JobCampus;
                            salarySheet.JobDepartment = obj.JobDepartment;
                            salarySheet.JobDesignation = obj.JobDesignation;
                            salarySheet.BasicSalaryAmount = obj.BasicSalaryAmount;
                            salarySheet.HouseRentAmount = obj.HouseRentAmount;
                            salarySheet.ConveyanceAmount = obj.ConveyanceAmount;
                            salarySheet.MedicalAmount = obj.MedicalAmount;
                            salarySheet.GrossSalaryAmount = obj.GrossSalaryAmount;
                            salarySheet.IncreamentAmount = obj.IncreamentAmount;
                            salarySheet.ArrearAmount = obj.ArrearAmount;
                            salarySheet.AutoZoneAmount = obj.AutoZoneAmount;
                            salarySheet.FinalZoneAmount = obj.FinalZoneAmount;
                            salarySheet.AutoAbsentAmount = obj.AutoAbsentAmount;
                            salarySheet.FinalAbsentAmount = obj.FinalAbsentAmount;
                            salarySheet.LeaveWithoutPayAmount = obj.LeaveWithoutPayAmount;
                            salarySheet.AutoLoanRefund = obj.AutoLoanRefund;
                            salarySheet.FinalLoanRefund = obj.FinalLoanRefund;
                            salarySheet.TdsAmount = obj.TdsAmount;
                            salarySheet.BankAmount = obj.BankAmount;
                            salarySheet.CashAmount = obj.CashAmount;
                            salarySheet.TotalSalaryAmount = obj.TotalSalaryAmount;
                            salarySheet.TotalAmount = obj.TotalAmount;
                            salarySheet.EbfAmount = obj.EbfAmount;
                            salarySheet.EbfForEmplyer = obj.EbfForEmplyer;
                            salarySheet.IsSubmit = obj.IsSubmit;
                            salarySheet.Remarks = obj.Remarks;

                            MemberEbfHistory memberEbfHistory = salarySheet.MemberEbfHistories.FirstOrDefault();
                            List<SalarySheetDetails> clonedList = salarySheet.SalarySheetDetailses.CloneList().ToList();
                            salarySheet.SalarySheetDetailses.Clear();
                            foreach (var ssDetails in clonedList)
                            {
                                var dtils = new SalarySheetDetails()
                                {
                                    SalarySheet = salarySheet,
                                    ZoneSetting = ssDetails.ZoneSetting,
                                    ZoneCount = ssDetails.ZoneCount,
                                    ZoneUnitAmount = ssDetails.ZoneUnitAmount,
                                    ZoneDeductionMultiplier = ssDetails.ZoneDeductionMultiplier,
                                    DeductionPerAbsent = ssDetails.DeductionPerAbsent,
                                    TotalAbsentDay = ssDetails.TotalAbsentDay,
                                    AbsentMultiplierFactor = ssDetails.AbsentMultiplierFactor,
                                };
                                salarySheet.SalarySheetDetailses.Add(dtils);
                            }

                            var ebfHistory = new MemberEbfHistory()
                            {
                                SalarySheet = salarySheet,
                                DateFrom = salarySheet.StartDate,
                                DateTo = salarySheet.EndDate,
                                EmployeeBenefitsFundSetting = memberEbfHistory.EmployeeBenefitsFundSetting,
                                CalculationOn = memberEbfHistory.CalculationOn,
                                EmployeeContributionAmount = memberEbfHistory.EmployeeContributionAmount,
                                EmployoyerContributionAmount = memberEbfHistory.EmployoyerContributionAmount
                            };
                            salarySheet.MemberEbfHistories.Add(ebfHistory);
                        }
                        _salarySheetDao.SaveOrUpdate(salarySheet);

                        if (salarySheet.IsSubmit)
                        {
                            List<AttendanceSummary> attenadnceSummarylist;
                            List<AttendanceAdjustment> attendanceAdjustmentList;
                            List<DayOffAdjustment> dayOffList;
                            List<LeaveApplicationDetails> leaveApplicationDetailList;

                            _salarySheetDao.GetHaveToUpdateTableInformation(out attenadnceSummarylist, out attendanceAdjustmentList, out dayOffList, out leaveApplicationDetailList,
                                salarySheet.TeamMember, salarySheet.StartDate, salarySheet.EndDate);

                            foreach (var attendanceSummary in attenadnceSummarylist)
                            {
                                attendanceSummary.IsPosted = salarySheet.IsSubmit;
                                _attendanceSummaryDao.SaveOrUpdate(attendanceSummary);
                            }

                            foreach (var attendanceAdjustment in attendanceAdjustmentList)
                            {
                                attendanceAdjustment.IsPost = salarySheet.IsSubmit;
                                _attendanceAdjustmentDao.SaveOrUpdate(attendanceAdjustment);
                            }

                            foreach (var dayOffAdjustment in dayOffList)
                            {
                                dayOffAdjustment.IsPost = salarySheet.IsSubmit;
                                _dayOffAdjustmentDao.SaveOrUpdate(dayOffAdjustment);
                            }

                            foreach (var leaveApplicationDetail in leaveApplicationDetailList)
                            {
                                leaveApplicationDetail.IsPost = salarySheet.IsSubmit;
                                _leaveApplicationDetailsDao.SaveOrUpdate(leaveApplicationDetail);
                            }
                        }
                    }
                    trans.Commit();
                }
            }
            catch (InvalidDatePassedException)
            {
                throw;
            }
            catch (MessageException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }
        
        private void CheckBeforeSaveOrUpdate(SalarySheet salarySheet)
        {
            DateTime lowdate = new DateTime(1971, 12, 16);
            string pendingText = "";

            if (salarySheet == null)
                throw new NullObjectException("Salary sheet can not be empty !!");

            if (salarySheet.SalarySheetDetailses == null || !salarySheet.SalarySheetDetailses.Any())
                throw new NullObjectException("Salary sheet Details can not be empty !!");

            if (salarySheet.MemberEbfHistories == null || !salarySheet.MemberEbfHistories.Any())
                throw new NullObjectException("EBF can not be empty !!");

            if (salarySheet.StartDate == null || salarySheet.StartDate <= lowdate)
                throw new InvalidDatePassedException("Invalid start date");

            if (salarySheet.EndDate == null || salarySheet.EndDate <= lowdate)
                throw new InvalidDatePassedException("Invalid start date");

            if (salarySheet.Month <= 0 || salarySheet.Month > 12)
                throw new InvalidDataException("Invalid Month");

            if (salarySheet.Year <= 0)
                throw new InvalidDataException("Invalid Month");

            CustomModelValidationCheck.DataAnnotationCheck(salarySheet, new SalarySheet());
            foreach (var salarySheetDetailse in salarySheet.SalarySheetDetailses)
            {
                CustomModelValidationCheck.DataAnnotationCheck(salarySheetDetailse, new SalarySheetDetails());
            }
            foreach (var memberEbfHistory in salarySheet.MemberEbfHistories)
            {
                CustomModelValidationCheck.DataAnnotationCheck(memberEbfHistory, new MemberEbfHistory());
            }

            int totalPendingLeave = _leaveApplicationDao.TeamMemberPendingLeaveCount(salarySheet.TeamMember,
                                   salarySheet.StartDate, salarySheet.EndDate, LeaveStatus.Pending);
            if (totalPendingLeave > 0)
                pendingText = "Please ensure your (" + salarySheet.TeamMember.FullNameEng +
                              ")team member pending leave.";

            int totalAttendanceAdjustment =
                _attendanceAdjustmentDao.TeamMemberPendingAttendanceAdjustmentCount(
                    salarySheet.TeamMember, salarySheet.StartDate, salarySheet.EndDate,
                    AttendanceAdjustmentStatus.Pending);
            if (totalAttendanceAdjustment > 0)
                pendingText += "Please ensure your (" + salarySheet.TeamMember.FullNameEng +
                               ")team member pending attendance adjustment.";
            if (totalPendingLeave > 0 || totalAttendanceAdjustment > 0)
                throw new MessageException(pendingText);
        }

        #endregion

        #region Single Instances Loading Function

        public SalarySheet LoadById(long id)
        {
            try
            {
                return _salarySheetDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public SalarySheetDto GetSalarySheetForFirstMonthTeamMember(List<UserMenu> userMenu, SalarySheetFormViewModel viewModel,
            EmploymentHistory employmentHistory)
        {
            try
            {
                DateTime lowdate = new DateTime(1971, 12, 16);

                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");

                if (viewModel == null)
                    throw new InvalidDataException("Invalid Salary sheet form value");

                if (employmentHistory == null)
                    throw new InvalidDataException("Invalid Object");

                if (employmentHistory.EffectiveDate < lowdate)
                    throw new InvalidDataException("Invalid effective date");

                if (employmentHistory.TeamMember == null)
                    throw new InvalidDataException("Invalid team member");

                if (viewModel.SalaryYear <= 0)
                    throw new InvalidDataException("Invalid Year.");

                if (viewModel.SalaryMonth <= 0 || viewModel.SalaryMonth > 12)
                    throw new InvalidDataException("Invalid Month.");

                if (viewModel.OrganizationType == OrganizationType.JobOrganization || viewModel.OrganizationType == OrganizationType.SalaryOrganization) { }
                else
                {
                    throw new InvalidDataException("Invalid Organization type.");
                }

                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu,
                    _commonHelper.ConvertIdToList(viewModel.OrganizationId));
                List<long> authBranchIdList = new List<long>();

                if (viewModel.BranchId == 0)
                    authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList);
                else
                    authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList, null,
                        _commonHelper.ConvertIdToList(viewModel.BranchId));

                if (authBranchIdList == null || !authBranchIdList.Any())
                    throw new InvalidDataException("Branch can not empty");

                int totalDaysOfMonth = DateTime.DaysInMonth(viewModel.SalaryYear, viewModel.SalaryMonth);
                var lastDayOfMonth = DateTime.DaysInMonth(viewModel.SalaryYear, viewModel.SalaryMonth);
                DateTime startDate = new DateTime(viewModel.SalaryYear, viewModel.SalaryMonth, 1);
                DateTime endDate = new DateTime(viewModel.SalaryYear, viewModel.SalaryMonth, lastDayOfMonth);

                CustomModelValidationCheck.DataAnnotationCheck(viewModel, new SalarySheetFormViewModel());

                return _salarySheetDao.GetSalarySheetForFirstMonthTeamMember(authOrganizationIdList, authBranchIdList, totalDaysOfMonth, startDate, endDate,
                    employmentHistory, viewModel);
            }
            catch (MessageException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public List<SalarySheetDto> LoadTeamMemberForSalarySheet(List<UserMenu> userMenu, SalarySheetFormViewModel viewModel)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu.");

                if (viewModel == null)
                    throw new InvalidDataException("Invalid Object.");

                if (viewModel.SalaryYear <= 0)
                    throw new InvalidDataException("Invalid Year.");

                if (viewModel.SalaryMonth <= 0 || viewModel.SalaryMonth > 12)
                    throw new InvalidDataException("Invalid Month.");

                if (viewModel.OrganizationType == OrganizationType.JobOrganization ||viewModel.OrganizationType == OrganizationType.SalaryOrganization){}
                else
                {
                    throw new InvalidDataException("Invalid Organization type.");
                }

                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu,
                    _commonHelper.ConvertIdToList(viewModel.OrganizationId));
                List<long> authBranchIdList = new List<long>();

                if (viewModel.BranchId == 0)
                    authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList);
                else
                    authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList, null,_commonHelper.ConvertIdToList(viewModel.BranchId));

                if(authBranchIdList==null ||!authBranchIdList.Any())
                    throw new InvalidDataException("Branch can not empty");
                
                int totalDaysOfMonth = DateTime.DaysInMonth(viewModel.SalaryYear, viewModel.SalaryMonth);

                var lastDayOfMonth = DateTime.DaysInMonth(viewModel.SalaryYear, viewModel.SalaryMonth);
                DateTime startDate = new DateTime(viewModel.SalaryYear, viewModel.SalaryMonth, 1);
                DateTime endDate = new DateTime(viewModel.SalaryYear, viewModel.SalaryMonth, lastDayOfMonth);

                return _salarySheetDao.LoadTeamMemberForSalarySheet(authOrganizationIdList, authBranchIdList, totalDaysOfMonth, startDate, endDate, viewModel);
            }
            catch (InvalidDataException)
            {
                throw;
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<SalarySheet> LoadSalarySheetList(List<UserMenu> _userMenu, SalarySheetFormViewModel viewModel)
        {
            try
            {
                if (_userMenu == null)
                    throw new InvalidDataException("Invalid menu");

                if (viewModel == null)
                    throw new InvalidDataException("Invalid Object");

                if (viewModel.SalaryYear <= 0)
                    throw new InvalidDataException("Invalid Year.");

                if (viewModel.SalaryMonth <= 0 || viewModel.SalaryMonth > 12)
                    throw new InvalidDataException("Invalid Month.");

                if (viewModel.OrganizationType == OrganizationType.JobOrganization || viewModel.OrganizationType == OrganizationType.SalaryOrganization) { }
                else
                {
                    throw new InvalidDataException("Invalid Organization type.");
                }

                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(_userMenu,
                    _commonHelper.ConvertIdToList(viewModel.OrganizationId));
                List<long> authBranchIdList = new List<long>();

                if (viewModel.BranchId == 0)
                    authBranchIdList = AuthHelper.LoadBranchIdList(_userMenu, authOrganizationIdList);
                else
                    authBranchIdList = AuthHelper.LoadBranchIdList(_userMenu, authOrganizationIdList, null,_commonHelper.ConvertIdToList(viewModel.BranchId));

                if(authBranchIdList==null || !authBranchIdList.Any())
                    throw new InvalidDataException("Invalid Branch.");
                
                int totalDaysOfMonth = DateTime.DaysInMonth(viewModel.SalaryYear, viewModel.SalaryMonth);
                var lastDayOfMonth = DateTime.DaysInMonth(viewModel.SalaryYear, viewModel.SalaryMonth);
                DateTime startDate = new DateTime(viewModel.SalaryYear, viewModel.SalaryMonth, 1);
                DateTime endDate = new DateTime(viewModel.SalaryYear, viewModel.SalaryMonth, lastDayOfMonth);

                return _salarySheetDao.LoadSalarySheetList(authOrganizationIdList, authBranchIdList, totalDaysOfMonth, startDate, endDate, viewModel);
            }
            catch (InvalidDataException)
            {
                throw;
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion
    }
}