﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Mapping.Sms
{
    /// <summary>
    /// Habib
    /// </summary>
    public class SmsDynamicOptionMap : BaseClassMap<SmsDynamicOption, long>
    {
        public SmsDynamicOptionMap()
        {
            Table("SmsDynamicOption");
            LazyLoad();

            Map(x => x.Rank);
            Map(x => x.Name);

            HasManyToMany(x => x.SmsTypes).Table("SmsTypeDynamicOption").ParentKeyColumn("SmsDynamicOptionId").ChildKeyColumn("SmsTypeId"); 
        }
    }
}
