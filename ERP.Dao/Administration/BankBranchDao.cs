using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.Administration
{
    public interface IBankBranchDao : IBaseDao<BankBranch, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        BankBranch GetBankBranchById(long bankBranchId);

        #endregion

        #region List Loading Function

        IList<BankBranch> LoadBankBranch(int start, int length, string orderBy, string orderDir, string bank, string name, string shortName, string contactPerson, string contactPersonMobile, string routingNo, string swifeCode, string rank, string status);
        IList<BankBranch> LoadBankBranchList(long[] bankIdArray);

        #endregion

        #region Others Function

        int GetBankBranchCount(string orderBy, string orderDir, string bank, string name, string shortName, string contactPerson, string contactPersonMobile, string routingNo, string swifeCode, string rank, string status);
        BankBranch IsSwiftCodeUnique(string code, long? id);
        BankBranch IsRouteNoUnique(string code, long? id = null);

        #endregion

    }

    public class BankBranchDao : BaseDao<BankBranch, long>, IBankBranchDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public BankBranch GetBankBranchById(long bankBranchId)
        {
            var bankBranch = Session.QueryOver<BankBranch>().Where(x => x.Id == bankBranchId).SingleOrDefault<BankBranch>();
            return bankBranch;
        }

        #endregion

        #region List Loading Function

        public IList<BankBranch> LoadBankBranch(int start, int length, string orderBy, string orderDir, string bank, string name, string shortName, string contactPerson, string contactPersonMobile, string routingNo, string swifeCode, string rank, string status)
        {
            ICriteria criteria = GetBankBranchCriteria(orderBy, orderDir, bank, name, shortName, contactPerson, contactPersonMobile, routingNo, swifeCode, rank, status);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<BankBranch>();
        }
        public IList<BankBranch> LoadBankBranchList(long[] bankIdArray)
        {
            if (bankIdArray!=null)
            {
                return Session.QueryOver<BankBranch>().Where(x => x.Bank.Id.IsIn(bankIdArray) && x.Status==BankBranch.EntityStatus.Active).List<BankBranch>(); 
            }
            return Session.QueryOver<BankBranch>().Where(x=> x.Status==BankBranch.EntityStatus.Active).List<BankBranch>(); 
            
        }
       
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        public IList<BankBranch> LoadBankBranch(long[] bankBranchIds)
        {
            ICriteria criteria = Session.CreateCriteria<BankBranch>().Add(Restrictions.Eq("Status", BankBranch.EntityStatus.Active));
            if (bankBranchIds.Any())
            {
                criteria.Add(Restrictions.In("Id", bankBranchIds));
            }
            return criteria.List<BankBranch>();
        }
        private ICriteria GetBankBranchCriteria(string orderBy, string orderDir, string bank, string name, string shortName, string contactPerson, string contactPersonMobile, string routingNo, string swifeCode, string rank, string status, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<BankBranch>().Add(Restrictions.Not(Restrictions.Eq("Status", BankBranch.EntityStatus.Delete)));
            criteria.CreateAlias("Bank", "bank").Add(Restrictions.Not(Restrictions.Eq("bank.Status", BankBranch.EntityStatus.Delete)));
            
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("bank.Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(bank))
            {
                criteria.Add(Restrictions.Eq("bank.Id", Convert.ToInt64(bank)));
            }
            if (!String.IsNullOrEmpty(shortName))
            {
                criteria.Add(Restrictions.Like("ShortName", shortName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(contactPerson))
            {
                criteria.Add(Restrictions.Like("ContactPerson", contactPerson, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(shortName))
            {
                criteria.Add(Restrictions.Like("ContactPersonMobile", contactPersonMobile, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(routingNo))
            {
                criteria.Add(Restrictions.Like("RoutingNo", routingNo, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(swifeCode))
            {
                criteria.Add(Restrictions.Like("SwiftCode", swifeCode, MatchMode.Anywhere));
            }
            if (!string.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Like("Rank", Convert.ToInt32(rank)));
            }           
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }

        #endregion

        #region Other Function

        public int GetBankBranchCount(string orderBy, string orderDir, string bank, string name, string shortName, string contactPerson, string contactPersonMobile, string routingNo, string swifeCode, string rank, string status)
        {
            ICriteria criteria = GetBankBranchCriteria(orderBy, orderDir, bank, name, shortName, contactPerson, contactPersonMobile, routingNo, swifeCode, rank, status, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());

        }
        public BankBranch IsRouteNoUnique(string code, long? id = null)
        {
            var entity = Session.QueryOver<BankBranch>().Where(x => x.RoutingNo == code).Where(x => x.Id != id).SingleOrDefault<BankBranch>();
            return entity;
        }
        public BankBranch IsSwiftCodeUnique(string code, long? id = null)
        {
            var entity = Session.QueryOver<BankBranch>().Where(x => x.SwiftCode == code).Where(x => x.Id != id).SingleOrDefault<BankBranch>();
            return entity;
        }

        #endregion
    }
}
