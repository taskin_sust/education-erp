﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class BankFactory : ObjectFactoryBase<Bank>
    {
        #region Object Initialization

        protected readonly IBankService BankService;
        public BankFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            BankService = new BankService(_session);
        }

        #endregion

        #region Create
        public BankFactory Create()
        {
            Object = new Bank()
            {
                Id = 0,
                VersionNumber = 1,
                BusinessId = GetPcUserName(),
                CreationDate = DateTime.Now,
                ModificationDate = DateTime.Now,
                Status = Bank.EntityStatus.Active,
                CreateBy = 1,
                ModifyBy = 1,
                Rank = 1,
                Name = Guid.NewGuid() + "_bankname",
                ShortName = Guid.NewGuid() + "_bankshortname",
                Address = Guid.NewGuid() + "address"
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public BankFactory WithName(string name)
        {
            Object.Name = name;
            return this;
        }
        public BankFactory WithShortName(string shortname)
        {
            Object.ShortName = shortname;
            return this;
        }
        public BankFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        BankService.SaveOrUpdate(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        BankService.SaveOrUpdate(item);
                    }
                }
            }
            return this;
        }

        public BankFactory CreateMore(int numberOfBank)
        {
            CreateBankList(numberOfBank);
            return this;
        }

        private List<Bank> CreateBankList(int numberOfBank)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Bank>();
            for (int i = 0; i < numberOfBank; i++)
            {
                var bank = new Bank()
                {
                    Id = 0,
                    VersionNumber = 1,
                    BusinessId = GetPcUserName(),
                    CreationDate = DateTime.Now,
                    ModificationDate = DateTime.Now,
                    Status = Bank.EntityStatus.Active,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Rank = 1,
                    Name = "bankname"+i,
                    ShortName = "bankshortname" + i,
                    Address =  "address" + i
                };
                list.Add(bank);
            }
            ObjectList.AddRange(list);
            return list;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void CleanupSubject(List<long> bankIdList)
        {
            _session.CreateSQLQuery("Delete from bank where id in(" + string.Join(", ", bankIdList) + ")").ExecuteUpdate();
        }
        public void Cleanup()
        {
            DeleteAll();
        }

        #endregion

    }
}
