﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model.BusinessModel;
using log4net;
using Newtonsoft.Json;

namespace DeviceSynchronizer.ApiReferences
{
    public class SyncAttendanceReceiveApiRequest : BaseApiRequest
    {
        #region Logger

        public static readonly ILog Log = LogManager.GetLogger("SyncAttendanceReceiveApiLogger");

        #endregion

        #region Properties & Object Initialization
        
        private const string uploadAttendanceDataController = "SyncAttendanceReceiveApi";
        private const string uploadAttendanceDataFuncName = "UploadAttendanceData";
        private const string updateTimeOfAttendanceSynchronizerFuncName = "UpdateTimeOfAttendanceDevice";

        private HttpHelper _httpHelper;

        public SyncAttendanceReceiveApiRequest()
        {
            _httpHelper = new HttpHelper();
        }

        #endregion
       
        //Use to push attendance data to server and get response as a Json object 
        //return true/false 
        public bool UploadAttendanceData(List<AttendanceLog> logList)
        {
            try
            {
                var serializeLogList = JsonConvert.SerializeObject(logList);
                var dictionary = new Dictionary<string, string> { { "attendanceString", serializeLogList } };
                //_baseUrl = _receivebaseUrl;
                string param = MakeParam(dictionary);
                string url = MakeUrl(uploadAttendanceDataController, uploadAttendanceDataFuncName, null);
                var jsonResponse = _httpHelper.HttpPostRequest(url, param);
                if (jsonResponse.IsSuccess)
                    return true;

                Log.Warn(jsonResponse.Data);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return false;
        }

        //Use to push lastUpdateTime to server and get response as a Json object 
        //return true/false 
        public bool UpdateTimeOfAttendanceDevice(int deviceId, string lastUpdateTime, List<int> enrolList)
        {
            try
            {
                var serializeEnrolList = JsonConvert.SerializeObject(enrolList);
                var dictionary = new Dictionary<string, string>
                {
                    {"deviceId", deviceId.ToString()},
                    {"updateTime", lastUpdateTime},
                    {"enroList",serializeEnrolList},
                };
                //_baseUrl = _receivebaseUrl;
                string param = MakeParam(dictionary);
                string url = MakeUrl(uploadAttendanceDataController, updateTimeOfAttendanceSynchronizerFuncName, null);
                var jsonResponse = _httpHelper.HttpPostRequest(url, param);
                if (jsonResponse.IsSuccess)
                    return true;

                Log.Warn(jsonResponse.Data);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return false;
        }

    }
}
