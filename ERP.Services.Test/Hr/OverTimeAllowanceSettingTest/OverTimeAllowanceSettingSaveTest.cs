﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.OverTimeAllowanceSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "OverTimeAllowanceSetting")]
    public class OverTimeAllowanceSettingSaveTest : OverTimeAllowanceSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Be_Successfull()
        {
           
            _organizationFactory.Create().Persist();
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var branch = branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var userMenu = BuildUserMenu(_organizationFactory.Object, (Program)null, branch.Object);

            _overtimeAllowanceSettingService.SaveOrUpdate(userMenu,_overtimeAllowanceSettingFactory.Object);
            Assert.True(_overtimeAllowanceSettingFactory.Object.Id > 0);
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Save_Should_Be_Fail_For_Object()
        {
            _organizationFactory.Create().Persist();
            var branch = branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var userMenu = BuildUserMenu(_organizationFactory.Object, (Program)null, branch.Object);
            Assert.Throws<NullObjectException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(userMenu,null));
        }

        [Fact]
        public void Save_Should_Be_Fail_For_Null_Model_Validation()
        {
            _organizationFactory.Create().Persist();
            var branch = branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var userMenu = BuildUserMenu(_organizationFactory.Object, (Program)null, branch.Object);
            Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(userMenu, new OvertimeAllowanceSetting()));
        }

        [Fact]
        public void Save_Should_Be_Fail_For_Null_Organization()
        {
            _overtimeAllowanceSettingFactory.Create();
            var branch = branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var userMenu = BuildUserMenu(_organizationFactory.Object, (Program)null, branch.Object);
            Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(userMenu,_overtimeAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Save_Should_Be_Fail_For_Null_Name()
        {
            _organizationFactory.Create().Persist();
            var branch = branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var userMenu = BuildUserMenu(_organizationFactory.Object, (Program)null, branch.Object);
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            _overtimeAllowanceSettingFactory.Object.Name = null;
            Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(userMenu,_overtimeAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Save_Should_Be_Fail_For_Employment_Status()
        {
            _organizationFactory.Create().Persist();
            var branch = branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var userMenu = BuildUserMenu(_organizationFactory.Object, (Program)null, branch.Object);
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object).WithoutEmploymentStatus();
            Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(userMenu,_overtimeAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Save_Should_Be_Fail_For_Overtime_Type()
        {
            _organizationFactory.Create().Persist();
            var branch = branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var userMenu = BuildUserMenu(_organizationFactory.Object, (Program)null, branch.Object);
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object).WithoutOvertimeType();
            Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(userMenu,_overtimeAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Save_Should_Be_Fail_For_Invalid_CalculationOn()
        {
            _organizationFactory.Create().Persist();
            var branch = branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var userMenu = BuildUserMenu(_organizationFactory.Object, (Program)null, branch.Object);
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            _overtimeAllowanceSettingFactory.Object.MultiplyingFactor = 0;
            Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(userMenu,_overtimeAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Save_Should_Be_Fail_For_Closing_Date()
        {
            _organizationFactory.Create().Persist();
            var branch = branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var userMenu = BuildUserMenu(_organizationFactory.Object, (Program)null, branch.Object);
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            _overtimeAllowanceSettingFactory.Object.ClosingDate = _overtimeAllowanceSettingFactory.Object.EffectiveDate.AddDays(-10);
            Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(userMenu,_overtimeAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Save_Should_Be_Fail_For_Equal_Date_Of_Effective_Date_Closing_Date()
        {
            var orgFactory = _organizationFactory.Create().Persist();
            var branch = branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            var userMenu = BuildUserMenu(_organizationFactory.Object, (Program)null, branch.Object);
            _overtimeAllowanceSettingFactory.Create().WithOrganization(orgFactory.Object);
            _overtimeAllowanceSettingFactory.Object.ClosingDate = _overtimeAllowanceSettingFactory.Object.EffectiveDate;
            Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(userMenu,_overtimeAllowanceSettingFactory.Object));
        }

  
        //need to fixing
        public void Save_Should_Be_Success_Using_IsPost()
        {
            TeamMember member = PersistTeamMember(new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, null);
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10).WithEffectiveAndCloxingDate(new DateTime(2017, 01, 01), new DateTime(2017, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2017
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel);
            _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList);
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10).WithEffectiveAndCloxingDate(new DateTime(2017, 01, 01), new DateTime(2017, 12, 30)).WithOrganization(org);
            var cldAllPbj = _childrenAllowanceSettingFactory.SingleObjectList[1];
            Assert.Throws<MessageException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menus, cldAllPbj));
        }

        #endregion
    }
}
