﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class ServiceBlockLogMap: BaseClassMap<ServiceBlockLog, long>
    {
        public ServiceBlockLogMap()
        {
            Table("ServiceBlockLog");
            LazyLoad();

            Map(x => x.ProgramId);
            Map(x => x.SessionId);
            Map(x => x.ServiceType).Column("ServiceType");
            Map(x => x.ConditionType).Column("ConditionType");
        }
    }
}
