﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Principal;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr.AttendanceSummaryServices
{
    public class AttendanceSummaryServiceHelper : BaseService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IAttendanceSummaryDao _attendanceSummaryDao;
        private readonly ITeamMemberDao _teamMemberDao;
        private readonly IShiftWeekendHistoryDao _shiftWeekendHistoryDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly IZoneSettingDao _zoneSettingDao;
        private readonly IHolidaySettingDao _holidaySettingDao;
        private readonly ILeaveApplicationDao _leaveApplicationDao;
        private readonly IDayOffAdjustmentDao _dayOffAdjustmentDao;
        private readonly IAttendanceAdjustmentDao _attendanceAdjustmentDao;
        private readonly IAttendanceDeviceSynchronizationDao _attendanceDeviceSynchronizationDao;
        private readonly IAllowanceDao _allowanceDao;
        private readonly IHolidayWorkDao _holidayWorkDao;
        private readonly INightWorkDao _nightWorkDao;
        private readonly IOvertimeDao _overtimeDao;
        public AttendanceSummaryServiceHelper(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _attendanceSummaryDao = new AttendanceSummaryDao() { Session = session };
            _teamMemberDao = new TeamMemberDao() { Session = session };
            _shiftWeekendHistoryDao = new ShiftWeekendHistoryDao() { Session = session };
            _organizationDao = new OrganizationDao() { Session = session };
            _zoneSettingDao = new ZoneSettingDao() { Session = session };
            _holidaySettingDao = new HolidaySettingDao() { Session = session };
            _dayOffAdjustmentDao = new DayOffAdjustmentDao() { Session = session };
            _leaveApplicationDao = new LeaveApplicationDao() { Session = session };
            _attendanceAdjustmentDao = new AttendanceAdjustmentDao { Session = session };
            _attendanceDeviceSynchronizationDao = new AttendanceDeviceSynchronizationDao { Session = session };
            _allowanceDao = new AllowanceDao { Session = session };
            _holidayWorkDao = new HolidayWorkDao { Session = session };
            _nightWorkDao = new NightWorkDao { Session = session };
            _overtimeDao = new OvertimeDao { Session = session };
        }

        #endregion

        #region Operational Functions

        public void UpdateForAttendanceAdjustment(TeamMember teamMember, DateTime startTime, DateTime? endTime, Organization organization)
        {
            try
            {
                DateTime dtBefore = new DateTime(2000, 01, 01, 23, 59, 59);
                DateTime dtAfter = new DateTime(2000, 01, 01, 0, 0, 0);
                DateTime orgAttendanceStartTime = organization.AttendanceStartTime;
                var zoneList = _zoneSettingDao.LoadZoneSetting("", "", _commonHelper.ConvertIdToList(organization.Id));
                var holydayList = _holidaySettingDao.LoadHolidayByOrganization(_commonHelper.ConvertIdToList(organization.Id));
                var getStartTime = DateTime.Parse("2000-01-01 " + startTime.ToString("H:mm"));

                //var getEndTime = endTime.Value.ToString("H:mm");

                var memberWeekendDay = teamMember.ShiftWeekendHistory.Where(x => x.EffectiveDate <= DateTime.Today).OrderByDescending(x => x.EffectiveDate).FirstOrDefault();
                List<AttendanceSummary> summaryList;
                //if (endTime != null)
                //{
                //    if (getStartTime >= dtAfter && getStartTime < orgAttendanceStartTime)
                //    {
                //        summaryList = (List<AttendanceSummary>)_attendanceSummaryDao.ListAttendanceSummary(teamMember, startTime.Date.AddDays(-1), startTime.Date.AddDays(-1));
                //    }
                //    else
                //    {
                //        summaryList = (List<AttendanceSummary>)_attendanceSummaryDao.ListAttendanceSummary(teamMember, startTime.Date, startTime.Date);
                //    }
                //}
                //else
                //{
                if (getStartTime >= dtAfter && getStartTime < orgAttendanceStartTime)
                {
                    summaryList = (List<AttendanceSummary>)_attendanceSummaryDao.ListAttendanceSummary(teamMember, startTime.Date.AddDays(-1), startTime.Date.AddDays(-1));
                }
                else
                {
                    summaryList = (List<AttendanceSummary>)_attendanceSummaryDao.ListAttendanceSummary(teamMember, startTime.Date, startTime.Date);
                }
                //}

                var lastShiftWeekend = _shiftWeekendHistoryDao.GetLastShiftWeekendHistory(teamMember, startTime.Date);
                if (lastShiftWeekend != null)
                {
                    var shift = lastShiftWeekend.Shift;
                    var memberSummary = new AttendanceSummary();
                    DateTime shiftStartTime = (DateTime)shift.StartTime;
                    DateTime shiftEndTime = (DateTime)shift.EndTime;

                    var psTime = startTime.ToString("H:mm");
                    var punchStartTime = DateTime.Parse("2000-01-01 " + psTime + "");
                    DateTime? punchEndTime = null;
                    if (endTime != null)
                    {
                        var peTime = endTime.Value.ToString("H:mm");
                        punchEndTime = DateTime.Parse("2000-01-01 " + peTime + "");
                    }

                    if (summaryList != null && summaryList.Count > 0)
                    {
                        memberSummary = summaryList.FirstOrDefault();
                        memberSummary.InTime = startTime;
                        memberSummary.OutTime = endTime;
                        if (punchStartTime <= shiftStartTime && punchStartTime > orgAttendanceStartTime)
                        {
                            //TimeSpan diffTimeSpan = shiftStartTime.Subtract(punchStartTime);
                            TimeSpan diffTimeSpan = punchStartTime.Subtract(shiftStartTime);
                            double tollaranceTime = diffTimeSpan.TotalMinutes;
                            memberSummary.EarlyEntry = DateTime.Parse("2000-01-01 " + diffTimeSpan.Duration() + "");
                            memberSummary.LateEntry = null;
                            memberSummary.TotalOvertime = DateTime.Parse("2000-01-01 " + diffTimeSpan.Duration() + "");
                            if (zoneList != null && zoneList.Count > 0)
                            {
                                memberSummary.ZoneSetting = zoneList.Where(x => x.ToleranceTime <= tollaranceTime).OrderByDescending(x => x.ToleranceTime).FirstOrDefault() ??
                                                       zoneList.OrderBy(x => x.ToleranceTime).FirstOrDefault();
                            }
                            else
                            {
                                memberSummary.ZoneSetting = null;
                            }
                        }
                        else if (punchStartTime > shiftStartTime && punchStartTime > orgAttendanceStartTime)
                        {
                            TimeSpan diffTimeSpan = punchStartTime.Subtract(shiftStartTime);
                            double tollaranceTime = Math.Abs(diffTimeSpan.TotalMinutes);
                            memberSummary.EarlyEntry = null;
                            memberSummary.LateEntry = DateTime.Parse("2000-01-01 " + diffTimeSpan.Duration() + "");
                            memberSummary.TotalOvertime = DateTime.Parse("2000-01-01 00:00:00.000");
                            if (zoneList != null && zoneList.Count > 0)
                            {
                                memberSummary.ZoneSetting = zoneList.Where(x => x.ToleranceTime >= tollaranceTime).OrderBy(x => x.ToleranceTime).FirstOrDefault() ??
                                                    zoneList.OrderByDescending(x => x.ToleranceTime).FirstOrDefault();
                            }
                            else
                            {
                                memberSummary.ZoneSetting = null;
                            }

                        }
                        else if (punchStartTime >= dtAfter && punchStartTime < orgAttendanceStartTime)
                        {
                            TimeSpan firstHalf = dtBefore.Subtract(shiftStartTime);
                            double firstHalfTollaranceTime = Math.Abs(firstHalf.TotalMinutes);
                            TimeSpan secondHalf = punchStartTime.Subtract(dtAfter);
                            double secondHalfTollaranceTime = Math.Abs(secondHalf.TotalMinutes);
                            double totalTollaranceTime = firstHalfTollaranceTime + secondHalfTollaranceTime;

                            memberSummary.EarlyEntry = null;
                            memberSummary.LateEntry = DateTime.Parse("2000-01-01 " + (firstHalf.Duration() + secondHalf) + "");
                            memberSummary.TotalOvertime = DateTime.Parse("2000-01-01 00:00:00.000");
                            if (zoneList != null && zoneList.Count > 0)
                            {
                                memberSummary.ZoneSetting = zoneList.Where(x => x.ToleranceTime >= totalTollaranceTime).OrderBy(x => x.ToleranceTime).FirstOrDefault() ??
                                    zoneList.OrderByDescending(x => x.ToleranceTime).FirstOrDefault();
                            }
                            else
                            {
                                memberSummary.ZoneSetting = null;
                            }

                        }
                        if (punchEndTime != null)
                        {
                            if (punchEndTime <= shiftEndTime && punchEndTime > orgAttendanceStartTime)
                            {
                                TimeSpan diffTimeSpan = shiftEndTime.Subtract(punchEndTime.Value);
                                memberSummary.EarlyLeave = DateTime.Parse("2000-01-01 " + diffTimeSpan.Duration() + "");
                                memberSummary.LateLeave = null;
                            }
                            else if (punchEndTime > shiftEndTime && punchEndTime < dtBefore) //brfore 12 am
                            {
                                TimeSpan diffTimeSpan = punchEndTime.Value.Subtract(shiftEndTime);
                                double tollaranceTime = Math.Abs(diffTimeSpan.TotalMinutes);
                                memberSummary.EarlyLeave = null;
                                memberSummary.LateLeave = DateTime.Parse("2000-01-01 " + diffTimeSpan.Duration() + "");
                                memberSummary.TotalOvertime = memberSummary.TotalOvertime.Value.AddMinutes(tollaranceTime);
                                //DateTime.Parse("2000-01-01 " + diffTimeSpan.Duration() + "");
                            }
                            else if (punchEndTime < orgAttendanceStartTime && punchEndTime > dtAfter) //After 12 am
                            {
                                TimeSpan firstHalf = dtBefore.Subtract(shiftEndTime);
                                double firstHalfTollaranceTime = Math.Abs(firstHalf.TotalMinutes);
                                TimeSpan secondHalf = punchEndTime.Value.Subtract(dtAfter);
                                double secondHalfTollaranceTime = Math.Abs(secondHalf.TotalMinutes);
                                double totalTollaranceTime = firstHalfTollaranceTime + secondHalfTollaranceTime;
                                memberSummary.EarlyLeave = null;
                                memberSummary.LateLeave = DateTime.Parse("2000-01-01 " + (firstHalf.Duration() + secondHalf) + "");
                                memberSummary.TotalOvertime = memberSummary.TotalOvertime.Value.AddMinutes(totalTollaranceTime);
                                //DateTime.Parse("2000-01-01 " + (firstHalf.Duration() + secondHalf) + "");
                            }
                        }
                        else
                        {
                            memberSummary.EarlyLeave = null;
                            memberSummary.LateLeave = null;
                        }

                        _attendanceSummaryDao.Update(memberSummary);
                    }
                    else
                    {
                        memberSummary.InTime = startTime;
                        memberSummary.OutTime = endTime;
                        memberSummary.TeamMember = teamMember;
                        if (getStartTime >= dtAfter && getStartTime < orgAttendanceStartTime)
                        {
                            memberSummary.AttendanceDate = DateTime.Parse(startTime.Date.AddDays(-1).ToString("yyyy-MM-dd") + " 00:00:00.000");
                        }
                        else
                        {
                            memberSummary.AttendanceDate = DateTime.Parse(startTime.Date.ToString("yyyy-MM-dd") + " 00:00:00.000");
                        }

                        var dayOfWeek = "";
                        if (getStartTime >= dtAfter && getStartTime < orgAttendanceStartTime)
                        {
                            dayOfWeek = startTime.AddDays(-1).ToString("dddd");
                        }
                        else
                        {
                            dayOfWeek = startTime.ToString("dddd");
                        }
                        int dayOfWeekId = (int)Enum.Parse(typeof(WeekDay), dayOfWeek);

                        var holyday = new HolidaySetting();

                        if (getStartTime >= dtAfter && getStartTime < orgAttendanceStartTime)
                        {
                            holyday =
                                holydayList.Where(
                                    x => x.DateFrom <= startTime.AddDays(-1) && x.DateTo >= startTime.AddDays(-1))
                                    .OrderByDescending(x => x.HolidayType)
                                    .FirstOrDefault();
                        }
                        else
                        {
                            holyday = holydayList.Where(x => x.DateFrom <= startTime && x.DateTo >= startTime).OrderByDescending(x => x.HolidayType).FirstOrDefault();
                        }
                        memberSummary.HolidaySetting = holyday ?? null;
                        if (memberWeekendDay != null && memberWeekendDay.Weekend == dayOfWeekId)
                        {
                            memberSummary.IsWeekend = true;
                        }
                        else
                        {
                            memberSummary.IsWeekend = false;
                        }
                        if (punchStartTime <= shiftStartTime && punchStartTime > orgAttendanceStartTime)
                        {
                            TimeSpan diffTimeSpan = punchStartTime.Subtract(shiftStartTime);
                            double tollaranceTime = diffTimeSpan.TotalMinutes;
                            memberSummary.EarlyEntry = DateTime.Parse("2000-01-01 " + diffTimeSpan.Duration() + "");
                            memberSummary.LateEntry = null;
                            memberSummary.TotalOvertime = DateTime.Parse("2000-01-01 " + diffTimeSpan.Duration() + "");
                            if (zoneList != null && zoneList.Count > 0)
                            {
                                memberSummary.ZoneSetting = zoneList.Where(x => x.ToleranceTime <= tollaranceTime).OrderByDescending(x => x.ToleranceTime).FirstOrDefault() ??
                                                    zoneList.OrderBy(x => x.ToleranceTime).FirstOrDefault();
                            }
                            else
                            {
                                memberSummary.ZoneSetting = null;
                            }
                        }
                        else if (punchStartTime >= dtAfter && punchStartTime < orgAttendanceStartTime)
                        {
                            TimeSpan firstHalf =
                                DateTime.Parse(shiftStartTime.ToString("yyyy-MM-dd") + " " +
                                               punchStartTime.ToString("H:mm")).Subtract(shiftStartTime);
                            //TimeSpan firstHalf = punchStartTime.Subtract(shiftStartTime);
                            double firstHalfTollaranceTime = firstHalf.TotalMinutes;
                            //TimeSpan secondHalf = punchStartTime.Subtract(dtAfter);
                            //double secondHalfTollaranceTime = Math.Abs(secondHalf.TotalMinutes);
                            double totalTollaranceTime = firstHalfTollaranceTime;//+ secondHalfTollaranceTime;
                            if (firstHalf.TotalMinutes < 0)
                            {
                                memberSummary.EarlyEntry = DateTime.Parse("2000-01-01 " + (firstHalf.Duration()) + "");
                                memberSummary.LateEntry = null;
                                memberSummary.TotalOvertime = DateTime.Parse("2000-01-01 00:00:00.000").AddMinutes(Math.Abs(firstHalf.TotalMinutes));
                                if (zoneList != null && zoneList.Count > 0)
                                {
                                    memberSummary.ZoneSetting = zoneList.OrderBy(x => x.ToleranceTime).FirstOrDefault();
                                }
                                else
                                {
                                    memberSummary.ZoneSetting = null;
                                }
                            }
                            else
                            {
                                memberSummary.EarlyEntry = null;
                                memberSummary.LateEntry = DateTime.Parse("2000-01-01 " + (firstHalf.Duration()) + "");
                                memberSummary.TotalOvertime = DateTime.Parse("2000-01-01 00:00:00.000");
                                if (zoneList != null && zoneList.Count > 0)
                                {
                                    memberSummary.ZoneSetting = zoneList.Where(x => x.ToleranceTime >= totalTollaranceTime).OrderBy(x => x.ToleranceTime).FirstOrDefault() ??
                                        zoneList.OrderByDescending(x => x.ToleranceTime).FirstOrDefault();
                                }
                                else
                                {
                                    memberSummary.ZoneSetting = null;
                                }
                            }

                        }
                        else if (punchStartTime > shiftStartTime && punchStartTime > orgAttendanceStartTime)
                        {
                            TimeSpan diffTimeSpan = punchStartTime.Subtract(shiftStartTime);
                            double tollaranceTime = Math.Abs(diffTimeSpan.TotalMinutes);
                            memberSummary.EarlyEntry = null;
                            memberSummary.LateEntry = DateTime.Parse("2000-01-01 " + diffTimeSpan.Duration() + "");
                            memberSummary.TotalOvertime = DateTime.Parse("2000-01-01 00:00:00.000");
                            if (zoneList != null && zoneList.Count > 0)
                            {
                                memberSummary.ZoneSetting = zoneList.Where(x => x.ToleranceTime >= tollaranceTime).OrderByDescending(x => x.ToleranceTime).FirstOrDefault() ??
                                     zoneList.OrderByDescending(x => x.ToleranceTime).FirstOrDefault();
                            }
                            else
                            {
                                memberSummary.ZoneSetting = null;
                            }
                        }
                        if (punchEndTime != null)
                        {
                            if (punchEndTime <= shiftEndTime && punchEndTime > orgAttendanceStartTime)
                            {
                                TimeSpan diffTimeSpan = shiftEndTime.Subtract(punchEndTime.Value);
                                memberSummary.EarlyLeave = DateTime.Parse("2000-01-01 " + diffTimeSpan.Duration() + "");
                                memberSummary.LateLeave = null;
                            }
                            else if (punchEndTime > shiftEndTime && punchEndTime < dtBefore)//brfore 12 am
                            {
                                TimeSpan diffTimeSpan = punchEndTime.Value.Subtract(shiftEndTime);
                                double tollaranceTime = Math.Abs(diffTimeSpan.TotalMinutes);
                                memberSummary.EarlyLeave = null;
                                memberSummary.LateLeave = DateTime.Parse("2000-01-01 " + diffTimeSpan.Duration() + "");
                                memberSummary.TotalOvertime = memberSummary.TotalOvertime.Value.AddMinutes(tollaranceTime);
                            }
                            else if (punchEndTime < orgAttendanceStartTime && punchEndTime >= dtAfter) //After 12 am
                            {
                                DateTime punETime = DateTime.Parse(shiftEndTime.ToString("yyyy-MM-dd") + " " +
                                                   punchEndTime.Value.ToString("H:mm"));

                                if (shiftEndTime >= dtAfter && shiftEndTime < orgAttendanceStartTime && punETime > shiftEndTime)
                                {
                                    TimeSpan firstHalfs = punchEndTime.Value.Subtract(shiftEndTime);
                                    double firstHalfTollaranceTime = Math.Abs(firstHalfs.TotalMinutes);
                                    memberSummary.EarlyLeave = null;
                                    memberSummary.LateLeave = DateTime.Parse("2000-01-01 " + (firstHalfs.Duration()) + "");
                                    memberSummary.TotalOvertime = memberSummary.TotalOvertime.Value.AddMinutes(firstHalfTollaranceTime);
                                }
                                if (shiftEndTime >= dtAfter && shiftEndTime < orgAttendanceStartTime && punETime < shiftEndTime)
                                {
                                    TimeSpan firstHalfs = shiftEndTime.Subtract(punchEndTime.Value);
                                    double firstHalfTollaranceTime = Math.Abs(firstHalfs.TotalMinutes);
                                    memberSummary.EarlyLeave = DateTime.Parse("2000-01-01 " + (firstHalfs.Duration()) + "");
                                    memberSummary.LateLeave = null;
                                    //memberSummary.TotalOvertime = memberSummary.TotalOvertime.Value.AddMinutes(firstHalfTollaranceTime);
                                }
                                else
                                {
                                    TimeSpan firstHalf = dtBefore.Subtract(shiftEndTime);
                                    double firstHalfTollaranceTime = Math.Abs(firstHalf.TotalMinutes);
                                    TimeSpan secondHalf = punchEndTime.Value.Subtract(dtAfter);
                                    double secondHalfTollaranceTime = Math.Abs(secondHalf.TotalMinutes);
                                    double totalTollaranceTime = firstHalfTollaranceTime + secondHalfTollaranceTime;
                                    memberSummary.EarlyLeave = DateTime.Parse("2000-01-01 " + (firstHalf.Duration() + secondHalf) + "");
                                    memberSummary.LateLeave = null;
                                    memberSummary.TotalOvertime = memberSummary.TotalOvertime.Value.AddMinutes(totalTollaranceTime);
                                }

                            }
                        }
                        else
                        {
                            memberSummary.EarlyLeave = null;
                            memberSummary.LateLeave = null;
                        }
                        _attendanceSummaryDao.Save(memberSummary);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public bool UpdateAttendenceSummeryByPinAndDateTime(int pin, DateTime attendenceDateTime)
        {
            bool stat = false;
            bool timeFound = false;
            DateTime BaseDate = new DateTime(2000, 01, 01, 0, 0, 0);
            DateTime attendenceDate = attendenceDateTime;

            try
            {
                TeamMember member = _teamMemberDao.GetMember(pin);

                EmploymentHistory memberEmploymentHistory = member.EmploymentHistory
                                .Where(s => s.EffectiveDate <= attendenceDateTime && s.Status == EmploymentHistory.EntityStatus.Active)
                                .OrderByDescending(s => s.EffectiveDate)
                                .ThenByDescending(s => s.Id)
                                .FirstOrDefault();

                Organization memberOrganization = memberEmploymentHistory.Department.Organization;
                DateTime orgStartTime = attendenceDate.Date.AddHours(memberOrganization.AttendanceStartTime.Hour).AddMinutes(memberOrganization.AttendanceStartTime.Minute);
                DateTime orgEndTime = orgStartTime.AddDays(1).AddMinutes(-1);

                //REAL ATTENDENCE DATE CALCULATION
                //if (orgStartTime > new DateTime(orgStartTime.Year, orgStartTime.Month, orgStartTime.Day, attendenceDate.Hour, attendenceDate.Minute, 0))
                //{
                //    attendenceDate = attendenceDate.AddDays(-1);
                //    attendenceDate = new DateTime(attendenceDate.Year, attendenceDate.Month, attendenceDate.Day, 0, 0, 0);
                //}
                //else
                //{
                attendenceDate = new DateTime(attendenceDate.Year, attendenceDate.Month, attendenceDate.Day, 0, 0, 0);
                //}

                //Previous Attendence Summery Loading OR New Summery Creation
                #region Previous Attendence Summery Loading OR New Summery Creation
                AttendanceSummary memberAttendanceSummary = _attendanceSummaryDao.GetByPinAndDate(pin, attendenceDate);
                if (memberAttendanceSummary == null)
                {
                    memberAttendanceSummary = new AttendanceSummary();
                    memberAttendanceSummary.AttendanceDate = attendenceDate;
                    memberAttendanceSummary.TeamMember = member;
                }

                memberAttendanceSummary.InTime = null;
                memberAttendanceSummary.LateEntry = null;
                memberAttendanceSummary.EarlyEntry = null;
                memberAttendanceSummary.InDevice = null;

                memberAttendanceSummary.OutTime = null;
                memberAttendanceSummary.EarlyLeave = null;
                memberAttendanceSummary.LateLeave = null;
                memberAttendanceSummary.OutDevice = null;

                memberAttendanceSummary.TotalOvertime = null;
                memberAttendanceSummary.Status = AttendanceSummary.EntityStatus.Active;
                #endregion

                #region Zone Weekend and Shift Load
                IList<ZoneSetting> memberZoneList = _zoneSettingDao.LoadZoneSetting("", "", _commonHelper.ConvertIdToList(memberOrganization.Id));

                var memberWeekend = member.ShiftWeekendHistory
                                .Where(s => s.EffectiveDate <= attendenceDateTime && s.Status == ShiftWeekendHistory.EntityStatus.Active)
                                .OrderByDescending(s => s.EffectiveDate)
                                .ThenByDescending(s => s.Id)
                                .Select(s => s.Weekend)
                                .FirstOrDefault();

                Shift membershift = member.ShiftWeekendHistory
                                .Where(s => s.EffectiveDate <= attendenceDateTime && s.Status == Shift.EntityStatus.Active)
                                .OrderByDescending(s => s.EffectiveDate)
                                .ThenByDescending(s => s.Id)
                                .Select(s => s.Shift)
                                .FirstOrDefault();

                DateTime shiftStartTime = attendenceDate.Date.AddHours(membershift.StartTime.Value.Hour).AddMinutes(membershift.StartTime.Value.Minute);
                DateTime shiftEndTime = attendenceDate.Date.AddHours(membershift.EndTime.Value.Hour).AddMinutes(membershift.EndTime.Value.Minute); // We only consider the one day shift, MUST REWRITE FOR 2 DAYS Shift
                int shiftTotalTime = (int)(shiftEndTime - shiftStartTime).TotalMinutes;
                #endregion

                //DateTime memberInTime = orgEndTime;
                //DateTime memberOutTime = orgStartTime;

                #region APPROVED ADJUSTMENT DATA
                IList<AttendanceAdjustment> memberAdjustmentList = _attendanceAdjustmentDao.LoadByPinAndDateRange(pin, orgStartTime, orgEndTime);
                if (memberAdjustmentList.Count > 0)
                {
                    memberAttendanceSummary.InTime = memberAdjustmentList.Min(a => a.StartTime).Value;
                    memberAttendanceSummary.OutTime = memberAdjustmentList.Max(a => a.EndTime).Value;
                    timeFound = true;
                }
                #endregion

                #region RAW DEVICE DATA
                IList<AttendanceDeviceRawData> memberRawDeviceEntryList = _attendanceDeviceSynchronizationDao.LoadByPinAndDateRange(pin, orgStartTime, orgEndTime);
                if (memberRawDeviceEntryList.Count > 0)
                {
                    AttendanceDeviceRawData minSync = memberRawDeviceEntryList.OrderBy(a => a.PunchTime).ThenBy(a => a.Id).FirstOrDefault();
                    if (memberAttendanceSummary.InTime == null || minSync.PunchTime < memberAttendanceSummary.InTime)
                    {
                        memberAttendanceSummary.InTime = minSync.PunchTime;
                        memberAttendanceSummary.InDevice = minSync.AttendanceDevice;
                        timeFound = true;
                    }
                    AttendanceDeviceRawData maxSync = memberRawDeviceEntryList.OrderByDescending(a => a.PunchTime).ThenBy(a => a.Id).FirstOrDefault();
                    if (memberAttendanceSummary.OutTime == null || maxSync.PunchTime > memberAttendanceSummary.OutTime)
                    {
                        memberAttendanceSummary.OutTime = maxSync.PunchTime;
                        memberAttendanceSummary.OutDevice = maxSync.AttendanceDevice;
                        timeFound = true;
                    }
                }
                #endregion

                if (timeFound == true)
                {
                    #region IN AND OUT TIME AND OTHER CALCULATION
                    if (memberAttendanceSummary.InTime > shiftStartTime)
                    {
                        memberAttendanceSummary.LateEntry = BaseDate.AddMinutes((memberAttendanceSummary.InTime.Value - shiftStartTime).TotalMinutes);
                    }
                    else
                    {
                        memberAttendanceSummary.EarlyEntry = BaseDate.AddMinutes((shiftStartTime - memberAttendanceSummary.InTime.Value).TotalMinutes);
                        //OVERTIME CALCULATION
                        memberAttendanceSummary.TotalOvertime = BaseDate.AddMinutes((shiftStartTime - memberAttendanceSummary.InTime.Value).TotalMinutes);
                    }

                    if (memberAttendanceSummary.InTime == memberAttendanceSummary.OutTime)/*&& memberAttendanceSummary.InDevice.Id == memberAttendanceSummary.OutDevice.Id*/
                    {
                        memberAttendanceSummary.OutTime = null;
                        memberAttendanceSummary.EarlyLeave = null;
                        memberAttendanceSummary.LateLeave = null;
                    }
                    else if (memberAttendanceSummary.InTime < memberAttendanceSummary.OutTime)
                    {
                        if (memberAttendanceSummary.OutTime > shiftEndTime)
                        {
                            memberAttendanceSummary.LateLeave = BaseDate.AddMinutes((memberAttendanceSummary.OutTime.Value - shiftEndTime).TotalMinutes);
                            //OVERTIME CALCULATION
                            if (memberAttendanceSummary.TotalOvertime != null)
                                memberAttendanceSummary.TotalOvertime = memberAttendanceSummary.TotalOvertime.Value.AddMinutes((memberAttendanceSummary.OutTime.Value - shiftEndTime).TotalMinutes);
                            else
                                memberAttendanceSummary.TotalOvertime = BaseDate.AddMinutes((memberAttendanceSummary.OutTime.Value - shiftEndTime).TotalMinutes);
                        }
                        else
                        {
                            int totalEarlyleave = (int)(shiftEndTime - memberAttendanceSummary.OutTime.Value).TotalMinutes;
                            if (totalEarlyleave > shiftTotalTime) totalEarlyleave = shiftTotalTime;
                            if (totalEarlyleave > 0) memberAttendanceSummary.EarlyLeave = BaseDate.AddMinutes(totalEarlyleave);
                        }
                    }
                    #endregion

                    #region ZONE CALCULATION
                    if (memberZoneList.Count > 0)
                    {
                        int totalLateOrEarlyMinute = (int)(memberAttendanceSummary.InTime.Value - shiftStartTime).TotalMinutes;
                        totalLateOrEarlyMinute = totalLateOrEarlyMinute < 0 ? 0 : totalLateOrEarlyMinute;

                        ZoneSetting memberZoneSetting = memberZoneList.Where(z => z.ToleranceTime >= totalLateOrEarlyMinute).OrderBy(z => z.Rank).ThenBy(z => z.ToleranceTime).FirstOrDefault();
                        if (memberZoneSetting != null)
                            memberAttendanceSummary.ZoneSetting = memberZoneSetting;
                    }
                    else
                    {
                        memberAttendanceSummary.ZoneSetting = null;
                    }
                    #endregion

                    #region WEEKEND CALCULATION
                    //if (((int)attendenceDate.DayOfWeek) - 2 == memberWeekend) //QUICK FIX
                    if ((int)Enum.Parse(typeof(WeekDay), attendenceDate.DayOfWeek.ToString()) == memberWeekend)
                    {
                        memberAttendanceSummary.IsWeekend = true;
                        memberAttendanceSummary.ZoneSetting = null;
                    }
                    else
                    {
                        memberAttendanceSummary.IsWeekend = false;
                    }
                    #endregion

                    #region HOLIDAY CALCULATION
                    HolidaySetting memberHolidaySetting = _holidaySettingDao.GetHolidayByDate(attendenceDate, memberOrganization);
                    if (memberHolidaySetting != null)
                    {
                        memberAttendanceSummary.HolidaySetting = memberHolidaySetting;
                        memberAttendanceSummary.ZoneSetting = null;
                    }
                    else
                    {
                        memberAttendanceSummary.HolidaySetting = null;
                    }

                    if (GetCurrentUserId() == 0)
                    {
                        var windowsIdentity = WindowsIdentity.GetCurrent();
                        if (windowsIdentity != null)
                            memberAttendanceSummary.BusinessId = windowsIdentity.Name;
                    }

                    #endregion

                    //using (var transaction = Session.BeginTransaction())
                    //{
                    //    try
                    //    {
                    _attendanceSummaryDao.SaveOrUpdate(memberAttendanceSummary);
                    //        transaction.Commit();
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        transaction.Rollback();
                    //    }
                    //}
                    Session.Flush();
                    stat = true;
                }
                else
                {
                    if (memberAttendanceSummary.Id > 0)
                    {
                        //using (var transaction = Session.BeginTransaction())
                        //{
                        //    try
                        //    {
                        // HARD DELETE from database table
                        Session.Delete(memberAttendanceSummary);
                        //_attendanceSummaryDao.Delete(memberAttendanceSummary);
                        //        transaction.Commit();
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        transaction.Rollback();
                        //    }
                        //}
                        Session.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                //if (transaction != null && transaction.IsActive)
                //    transaction.Rollback();
                _logger.Error(ex);
                //throw;  TEMPORARY FIX (NEED TO HANDEL LATER)
                stat = false;
            }
            return stat;
        }

        #endregion

        #region Single Instances Loading Function
        public AttendanceSummary GetAttendenceByPinAndDateTime(int pin, DateTime attendenceDateTime)
        {
            DateTime BaseDate = new DateTime(2000, 01, 01, 0, 0, 0);
            DateTime attendenceDate = attendenceDateTime;
            AttendanceSummary memberAttendanceSummary = new AttendanceSummary();

            //REAL ATTENDENCE DATE CALCULATION
            attendenceDate = new DateTime(attendenceDate.Year, attendenceDate.Month, attendenceDate.Day, 0, 0, 0);
            try
            {
                #region Member Org Start and End Time Loding
                TeamMember member = _teamMemberDao.GetMember(pin);

                EmploymentHistory memberEmploymentHistory = member.EmploymentHistory
                                .Where(s => s.EffectiveDate <= attendenceDateTime && s.Status == EmploymentHistory.EntityStatus.Active)
                                .OrderByDescending(s => s.EffectiveDate)
                                .ThenByDescending(s => s.Id)
                                .FirstOrDefault();

                //Write code for if memberEmploymentHistory is null
                if (memberEmploymentHistory == null)
                {
                    memberAttendanceSummary.AttendanceDate = attendenceDate;
                    memberAttendanceSummary.TeamMember = member;
                }
                else
                {
                    Organization memberOrganization = memberEmploymentHistory.Department.Organization;
                    DateTime orgStartTime = attendenceDate.Date.AddHours(memberOrganization.AttendanceStartTime.Hour).AddMinutes(memberOrganization.AttendanceStartTime.Minute);
                    DateTime orgEndTime = orgStartTime.AddDays(1).AddMinutes(-1);
                #endregion

                    //Previous Attendence Summery Loading OR New Summery Creation
                    memberAttendanceSummary = _attendanceSummaryDao.GetByPinAndDate(pin, attendenceDate);
                    if (memberAttendanceSummary == null)
                    {
                        #region New Summery Creation
                        memberAttendanceSummary = new AttendanceSummary();
                        memberAttendanceSummary.AttendanceDate = attendenceDate;
                        memberAttendanceSummary.TeamMember = member;


                        memberAttendanceSummary.InTime = null;
                        memberAttendanceSummary.LateEntry = null;
                        memberAttendanceSummary.EarlyEntry = null;
                        memberAttendanceSummary.InDevice = null;

                        memberAttendanceSummary.OutTime = null;
                        memberAttendanceSummary.EarlyLeave = null;
                        memberAttendanceSummary.LateLeave = null;
                        memberAttendanceSummary.OutDevice = null;
                        memberAttendanceSummary.Status = AttendanceSummary.EntityStatus.Active;
                        #endregion

                        #region WEEKEND CALCULATION
                        var memberWeekend = member.ShiftWeekendHistory
                                        .Where(s => s.EffectiveDate <= attendenceDateTime && s.Status == ShiftWeekendHistory.EntityStatus.Active)
                                        .OrderByDescending(s => s.EffectiveDate)
                                        .ThenByDescending(s => s.Id)
                                        .Select(s => s.Weekend)
                                        .FirstOrDefault();

                        //if (((int)attendenceDate.DayOfWeek) - 2 == memberWeekend) //QUICK FIX
                        if ((int)Enum.Parse(typeof(WeekDay), attendenceDate.DayOfWeek.ToString()) == memberWeekend)
                        {
                            memberAttendanceSummary.IsWeekend = true;
                            memberAttendanceSummary.ZoneSetting = null;
                        }
                        else
                        {
                            memberAttendanceSummary.IsWeekend = false;
                        }
                        #endregion

                        #region HOLIDAY CALCULATION
                        HolidaySetting memberHolidaySetting = _holidaySettingDao.GetHolidayByDate(attendenceDate, memberOrganization);
                        if (memberHolidaySetting != null)
                        {
                            memberAttendanceSummary.HolidaySetting = memberHolidaySetting;
                            memberAttendanceSummary.ZoneSetting = null;
                        }
                        else
                        {
                            memberAttendanceSummary.HolidaySetting = null;
                        }
                        #endregion
                    }

                    #region LEAVE CALCULATION
                    LeaveApplication memberLeaveApplication = _leaveApplicationDao.GetApprovedLeaveByPinAndDate(member.Id, attendenceDate);
                    if (memberLeaveApplication != null)
                    {
                        memberAttendanceSummary.BusinessId = "Leave: " + memberLeaveApplication.Leave.Name + ",<br />";
                    }
                    else
                    {
                        memberAttendanceSummary.BusinessId = "";
                    }
                    #endregion

                    #region DayOff Adjustment
                    DayOffAdjustment memberDayOffAdjustment = _dayOffAdjustmentDao.GetApprovedDayOffAdjustmentByPinAndDate(member.Id, attendenceDate);
                    if (memberDayOffAdjustment != null)
                    {
                        memberAttendanceSummary.BusinessId += "Dayoff adjustment against " + memberDayOffAdjustment.InsteadOfDate.Value.ToString("dd MMM, yyyy") + ",<br />";
                    }
                    #endregion

                    if (memberAttendanceSummary.InTime != null)
                    {
                        #region Daily Allowance
                        Allowance memberAllowance = _allowanceDao.GetAllowance(member.Pin.ToString(), attendenceDate);
                        if (memberAllowance != null && memberAllowance.IsApproved == true)
                        {
                            memberAttendanceSummary.BusinessId += "Daily allowance,<br />";
                        }
                        #endregion

                        #region Holiday Work
                        HolidayWork memberHolidayWork = _holidayWorkDao.GetTeamMemberHolidayWork(member.Id, attendenceDate);
                        if (memberHolidayWork != null && memberHolidayWork.ApprovalType != null)
                        {
                            if (memberHolidayWork.ApprovalType == 1)
                                memberAttendanceSummary.BusinessId += "Half holiday work,<br />";
                            else
                                memberAttendanceSummary.BusinessId += "Full holiday work,<br />";
                        }
                        #endregion

                        #region Night Work
                        NightWork memberNightWork = _nightWorkDao.GetNightWork(member.Pin.ToString(), attendenceDate);
                        if (memberNightWork != null && memberNightWork.ApprovalType != null)
                        {
                            if (memberNightWork.ApprovalType == 1)
                                memberAttendanceSummary.BusinessId += "Half night work,<br />";
                            else
                                memberAttendanceSummary.BusinessId += "Full night work,<br />";
                        }
                        #endregion

                        #region Overtime
                        Overtime memberOvertime = _overtimeDao.GetOverTime(member.Pin, attendenceDate);
                        if (memberOvertime != null && (memberOvertime.ApprovedTime.Value.Hour > 0 || memberOvertime.ApprovedTime.Value.Minute > 0))
                        {
                            memberAttendanceSummary.BusinessId += "Overtime " + memberOvertime.ApprovedTime.Value.Hour + ":" + memberOvertime.ApprovedTime.Value.Minute + ",<br />";
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

            return memberAttendanceSummary;
        }
        #endregion

        #region List Loading Function


        #endregion

        #region Others Function

        #endregion

        #region Helper function

        #endregion

    }
}
