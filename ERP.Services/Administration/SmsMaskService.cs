﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface ISmsMaskService : IBaseService
    {
        #region Operational Function
        bool Save(SmsMask smsMaskObj);
        bool Update(long id, SmsMask smsMask, int status);
        bool Delete(long id);
        bool UpdateRank(long id, int current, string action);
        #endregion

        #region Single Instances Loading Function
        SmsMask LoadById(long id);
        #endregion

        #region List Loading Function

        IList<SmsMask> LoadSmsMask(List<long> organization = null, int? status = null);
        IList<SmsMask> LoadSmsMask(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string organization, string name, string status);
        #endregion

        #region Others Function
        int GetMinRank(SmsMask smsMask);
        int GetMaxRank(SmsMask smsMask);
        //int GetSmsMaskCount(string orderBy, string orderDir, string organization, string name, string status);
        int GetSmsMaskCount(string orderBy, string orderDir, List<UserMenu> userMenu, string organization, string name, string status);

        #endregion
    }
    public class SmsMaskService : BaseService, ISmsMaskService
    {
         #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion
       
        #region Propertise & Object Initialization
        private readonly ISmsMaskDao _smsMaskDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly ICommonHelper _commonHelper;
        public SmsMaskService(ISession session)
        {
            Session = session;
            _smsMaskDao = new SmsMaskDao { Session = Session };
            _organizationDao = new OrganizationDao { Session = Session };
            _commonHelper = new CommonHelper();

        }
        #endregion

        #region Operational Function

        private void CheckDuplicateFields(SmsMask smsMaskObj, long id = 0)
        {
            if (_smsMaskDao.CheckDuplicateFields(smsMaskObj.Name, smsMaskObj.Organization.OrganizationReference, id))
                throw new DuplicateEntryException("Mask Name can not be duplicate");
        }

        private void CheckBeforeSave(SmsMask smsMaskObj)
        {
            CheckDuplicateFields(smsMaskObj);
            CheckNullEntry(smsMaskObj);
        }

        private void CheckNullEntry(SmsMask smsMaskObj)
        {
            if (string.IsNullOrEmpty(smsMaskObj.Name))
                throw new EmptyFieldException("Mask Name is empty!");
        }

        public bool Save(SmsMask smsMaskObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    CheckBeforeSave(smsMaskObj);
                    smsMaskObj.Organization = _organizationDao.LoadById(smsMaskObj.Organization.OrganizationReference);
                    smsMaskObj.Rank = _smsMaskDao.GetMaximumRank(smsMaskObj) + 1;
                    _smsMaskDao.Save(smsMaskObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }
     
        public bool Update(long id, SmsMask smsMask, int status)
        {
            ITransaction trans = null;
            try
            {
                var oldSmsMask = _smsMaskDao.LoadById(id);
                CheckDuplicateFields(smsMask, id);
                using (trans = Session.BeginTransaction())
                {
                    oldSmsMask.Name = smsMask.Name;
                    oldSmsMask.Status = status;
                    oldSmsMask.Organization = _organizationDao.LoadById(smsMask.Organization.OrganizationReference);
                    _smsMaskDao.Update(oldSmsMask);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                throw ex;
            }
        }

        private void CheckBeforeDelete(SmsMask smsMaskObj)
        {
            //if (smsMaskObj.Programs.Count > 0)
            //    throw new DependencyException("You can't delete this organization, program is already assigned.");
            //if (organizationObject.Branches.Count > 0)
            //    throw new DependencyException("You can't delete this organization, branch is already assigned.");
        }
        public bool Delete(long id)
        {
            ITransaction transaction = null;
            try
            {
                var smsMaskObj = _smsMaskDao.LoadById(id);
                CheckBeforeDelete(smsMaskObj);
                using (transaction = Session.BeginTransaction())
                {
                    smsMaskObj.Status = SmsMask.EntityStatus.Delete;
                    _smsMaskDao.Update(smsMaskObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        public bool UpdateRank(long id, int current, string action)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    SmsMask smsMaskObj = _smsMaskDao.LoadById(Convert.ToInt64(id));
                    if (smsMaskObj != null)
                    {
                        int newRank;
                        if (action == RankOperation.Up)
                            newRank = smsMaskObj.Rank - 1;
                        else
                            newRank = smsMaskObj.Rank + 1;
                        var orgOldObj = _smsMaskDao.LoadByRankDirection(newRank, action);
                        newRank = orgOldObj.Rank;
                        orgOldObj.Rank = smsMaskObj.Rank;
                        smsMaskObj.Rank = newRank;

                        _smsMaskDao.Update(orgOldObj);
                        _smsMaskDao.Update(smsMaskObj);
                        transaction.Commit();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public SmsMask LoadById(long id)
        {
            try
            {
                return _smsMaskDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region List Loading Function
        public IList<SmsMask> LoadSmsMask(List<long> organizationIdList = null, int? status = null)
        {
            try
            {
                return _smsMaskDao.LoadSmsMask(organizationIdList, status).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        //public IList<SmsMask> LoadSmsMask(int start, int length, string orderBy, string orderDir, string organization, string name, string status)
        public IList<SmsMask> LoadSmsMask(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string organization, string name, string status)
        {
            try
            {
                long? convertedOrganizationId = String.IsNullOrEmpty(organization) ? (long?)null : Convert.ToInt64(organization);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                return _smsMaskDao.LoadSmsMask(start, length, orderBy, orderDir, organizationIdList, name, status);
                //return _smsMaskDao.LoadSmsMask(start, length, orderBy, orderDir, organization, name, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region Others Function
        public int GetMaxRank(SmsMask smsMask)
        {
            try
            {
                return _smsMaskDao.GetMaximumRank(smsMask);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetMinRank(SmsMask smsMask)
        {
            try
            {
                return _smsMaskDao.GetMinimumRank(smsMask);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        //public int GetSmsMaskCount(string orderBy, string orderDir, string organization, string name, string status)
        public int GetSmsMaskCount(string orderBy, string orderDir, List<UserMenu> userMenu, string organization, string name, string status)
        {
            try
            {
                long? convertedOrganizationId = String.IsNullOrEmpty(organization) ? (long?)null : Convert.ToInt64(organization);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                return _smsMaskDao.GetSmsMaskCount(orderBy, orderDir, organizationIdList, name, status);
                //return _smsMaskDao.GetSmsMaskCount(orderBy, orderDir, organization, name, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion
    }
}
