﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.FingerPrint.BusinessModel
{
    public class MemberInfo
    {
        public MemberInfo()
        {
            AttendanceDeviceApiModel = new List<AttendanceDeviceApiModel>();
        }
        public long Id { get; set; }
        public string Name { get; set; }
        public int Pin { get; set; }
        public string Organization { get; set; }
        public string Branch { get; set; }
        public string Campus { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public byte[] Image { get; set; }
        public IList<AttendanceDeviceApiModel> AttendanceDeviceApiModel { get; set; }
    }

    public class AttendanceDeviceApiModel
    {
        public AttendanceDeviceApiModel()
        {
            Index0 = null;
            Index1 = null;
            Index2 = null;
            Index3 = null;
            Index4 = null;
            Index5 = null;
            Index6 = null;
            Index7 = null;
            Index8 = null;
            Index9 = null;
        }
        public string ModelName { get; set; }
        public long ModelId { get; set; }
        public bool IsFinger { get; set; }
        public string Index0 { get; set; }
        public string Index1 { get; set; }
        public string Index2 { get; set; }
        public string Index3 { get; set; }
        public string Index4 { get; set; }
        public string Index5 { get; set; }
        public string Index6 { get; set; }
        public string Index7 { get; set; }
        public string Index8 { get; set; }
        public string Index9 { get; set; }

        //public FingerIndex FingerIndices { get; set; }
    }

}
