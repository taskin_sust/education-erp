﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ShiftWeekendHistoryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ShiftWeekendHistoryService")]
    public class ShiftWeekendHistoryLoadTest : ShiftWeekendHistoryBaseTest
    {
        [Fact]
        public void Should_Return_Invalid_TeamMember()
        {
            var teamMember = PersistTeamMember();
            Assert.Throws<InvalidDataException>(() => _shiftWeekendHistoryService.LoadShiftWeekendHistory(0));
        }

        [Fact]
        public void Should_Return_valid_ShiftWeekendHistory()
        {
            var teamMember = PersistTeamMember();
            var list = _shiftWeekendHistoryService.LoadShiftWeekendHistory(teamMember.Id);
            Assert.True(list.Count > 0);
        }

        [Fact]
        public void Should_Return_Invalid_ShiftId()
        {
            var teamMember = PersistTeamMember();
            Assert.Throws<InvalidDataException>(() => _shiftWeekendHistoryService.LoadShiftWeekendHistory(0));
        }

        [Fact]
        public void Should_Return_valid_ShiftWeekendHistoryObject()
        {
            var teamMember = PersistTeamMember();
            var obj = _shiftWeekendHistoryService.GetShiftWeekendHistory(teamMember.ShiftWeekendHistory[0].Id);
            Assert.True(obj.Id > 0);
        }
    }
}
