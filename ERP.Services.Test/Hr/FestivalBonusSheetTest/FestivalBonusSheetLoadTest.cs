﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.FestivalBonusSheetTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "FestivalBonusSheet")]
    public class FestivalBonusSheetLoadTest:FestivalBonusSheetBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Successfully_Load()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;
            var lastDayOfMonth = DateTime.DaysInMonth(year, month);
            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Assert.Equal(1, _festivalBonusSheetService.LoadFestivalBonusSheetsList(commonHelper.ConvertIdToList(teamMember.Id),
                _festivalBonusSettingFactory.Object.Id, new DateTime(year, month, 1), new DateTime(year, month, lastDayOfMonth), false).Count());
        }

        [Fact]
        public void Should_Return_InvalidException_For_TeamMember()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;
            var lastDayOfMonth = DateTime.DaysInMonth(year, month);
            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Assert.Throws<NullObjectException>(() => _festivalBonusSheetService.LoadFestivalBonusSheetsList(null,
                _festivalBonusSettingFactory.Object.Id, new DateTime(year, month, 1), new DateTime(year, month, lastDayOfMonth), false));
        }

        #endregion

        #region
        #endregion
    }
}
