using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class DayOffAdjustmentLogMap : ClassMap<DayOffAdjustmentLog> {
        
        public DayOffAdjustmentLogMap() {
			Table("HR_DayOffAdjustmentLog");
			LazyLoad();
			References(x => x.DayOffAdjustment).Column("DayOffAdjustmentId");
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
			Map(x => x.DayOffDate).Column("DayOffDate");
			Map(x => x.InsteadOfDate).Column("InsteadOfDate");
			Map(x => x.Reason).Column("Reason");
			Map(x => x.DayOffStatus).Column("DayOffStatus");
        }
    }
}
