﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.Administration.BankBranchTest
{
    public interface IBankBranchRankTest
    {
        #region Basic Test

        [Fact]
        void Get_Maximum_Rank_Not_Null_Check();
        [Fact]
        void Get_Minimum_Rank_Not_Null_Check();
        #endregion

    }

    [Trait("Area", "Administration")]
    [Trait("Service", "BankBranch")]
    public class BankBranchRankTest : BankBranchBaseTest, IBankBranchRankTest
    {
        #region Basic Test

        [Fact]
        public void Get_Maximum_Rank_Not_Null_Check()
        {
            BankBranchFactory bankBranchFactory =
                    _bankBranchFactory.Create().WithBank()
                    .Persist();
            int maxRank = _bankBranchService.GetMaximumRank(bankBranchFactory.Object);
            Assert.True(maxRank > 0);

        }

        [Fact]
        public void Get_Minimum_Rank_Not_Null_Check()
        {
            BankBranchFactory bankBranchFactory =
                    _bankBranchFactory.Create().WithBank()
                    .Persist();
            int minRank = _bankBranchService.GetMinimumRank(bankBranchFactory.Object);
            Assert.True(minRank > 0);

        }
        #endregion

    }
}
