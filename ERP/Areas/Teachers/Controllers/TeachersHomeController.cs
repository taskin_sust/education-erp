﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;

namespace UdvashERP.Areas.Teachers.Controllers
{
    [UerpArea("Teachers")]
    [Authorize]
    [AuthorizeAccess]
    public class TeachersHomeController : Controller
    {
        // GET: Teacher/TeacherHome
        [AuthorizeRequired]
        public ActionResult Index()
        {
            return View();
        }
    }
}