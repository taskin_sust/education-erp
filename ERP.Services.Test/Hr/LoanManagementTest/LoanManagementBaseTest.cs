﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.LoanManagementTest
{
    public class LoanManagementBaseTest : TestBase, IDisposable
    {
        #region Propertise & Object Initialization

        protected readonly CommonHelper _commonHelper;
        protected readonly IMemberLoanApplicationService _memberLoanApplicationService;

        protected readonly MemberLoanApplicationFactory _memberLoanApplicationFactory;
        protected readonly MemberLoanCsrFactory _memberLoanCsrFactory;
        protected readonly MemberLoanZakatFactory _memberLoanZakatFactory;
        protected readonly MemberLoanFactory _memberLoanFactory;
        protected readonly TeamMemberFactory _teamMemberFactory;

        #endregion

        #region Constructor
        public LoanManagementBaseTest()
        {
            _commonHelper = new CommonHelper();
            _memberLoanApplicationService = new MemberLoanApplicationService(Session);

            _memberLoanApplicationFactory = new MemberLoanApplicationFactory(null, Session);
            _memberLoanCsrFactory = new MemberLoanCsrFactory(null, Session);
            _memberLoanZakatFactory = new MemberLoanZakatFactory(null, Session);
            _memberLoanFactory = new MemberLoanFactory(null, Session);
            _teamMemberFactory = new TeamMemberFactory(null, Session);
        }

        #endregion

        public void Dispose()
        {
            _teamMemberFactory.Cleanup();
            _memberLoanCsrFactory.CleanUp();
            _memberLoanZakatFactory.CleanUp();
            _memberLoanFactory.DeleteAll();
            _memberLoanApplicationFactory.DeleteAll();
            base.Dispose();

        }

    }
}
