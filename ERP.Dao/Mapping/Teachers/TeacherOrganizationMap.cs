﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.Model.Entity.Teacher;

namespace UdvashERP.Dao.Mapping.Teacher
{
    public class TeacherOrganizationMap : BaseClassMap<TeacherOrganization, long>
    {
        public TeacherOrganizationMap()
        {
            Table("TeacherOrganization");
            LazyLoad();
            References(x => x.Teacher);
            References(x => x.Organization);
        }
    }
}
