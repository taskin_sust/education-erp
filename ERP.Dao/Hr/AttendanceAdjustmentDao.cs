﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Hr
{
    public interface IAttendanceAdjustmentDao : IBaseDao<AttendanceAdjustment, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        AttendanceAdjustment GetByPinAndDate(int pin, DateTime adjustmentDate);
        AttendanceAdjustment GetTeamMemberLastIsPostAttendanceAdjustment(long? teamMemberId, int? pin);
        AttendanceAdjustment GetTeamMemberLastAppliedAttendanceAdjustment(long? teamMemberId = null, int? pin = null);

        #endregion

        #region List Loading Function

        IList<AttendanceAdjustment> LoadAllRowAttendanceAdjustmentSelfMember(TeamMember hrMember, int start, int length, string orderBy, string orderDir);
        IList<AttendanceAdjustment> LoadPendingListForMentor(TeamMember hrMember, List<long> teamMemberIdList);
        IList<AttendanceAdjustment> LoadAllMentorAttendanceAdjustmentRecentHistory(int start, int length, string orderBy, string toUpper, List<long> authoTeamMemberIdList, DateTime dateFrom, DateTime dateTo);
        IList<AttendanceAdjustmentHrDto> LoadPendingListForHr(string effectiveDate, List<long> organizationIdList, List<long> branchIdList, List<long> campusIdList, List<long> departmentIdList, string pin);
        IList<AttendanceAdjustmentHrDto> LoadAllHrAttendanceAdjustmentRecentHistory(int start, int length, string orderBy, string toUpper, string pin, DateTime dateFrom, DateTime dateTo, List<long> autorizedBranchIdList, List<long> campusIdList, List<long> departmentIdList);
        IList<AttendanceAdjustment> LoadAttendanceAdjustmentsForPinAndDate(TeamMember teamMember, DateTime dateTime);
        IList<AttendanceAdjustment> LoadAttendanceAdjustmentForAttendanceSummary(TeamMember teamMember, DateTime dateTime, Organization organization);
        IList<AttendanceAdjustment> LoadByPinAndDateRange(int pin, DateTime orgStartTime, DateTime orgEndTime);
        IList<AttendanceAdjustment> LoadAttendanceAdjustments(IList<long> pinList, DateTime dateFrom, DateTime dateTo);
        
        #endregion

        #region Others Function
        int CountMentorAttendanceAdjustmentRecentHistory(List<long> authoTeamMemberIdList, DateTime dateFrom, DateTime dateTo);
        int CountHrAttendanceAdjustmentRecentHistory(string pin, DateTime dateFrom, DateTime dateTo, List<long> autorizedBranchIdList, List<long> campusIdList, List<long> departmentIdList);
        int CountRowAttendanceAdjustmentSelfMember(TeamMember hrMember);
        bool HasDuplicate(AttendanceAdjustment hrAttendanceAd);
        int TeamMemberPendingAttendanceAdjustmentCount(TeamMember teamMember, DateTime dateTime1, DateTime dateTime2, AttendanceAdjustmentStatus attendanceAdjustmentStatus);

        #endregion

        #region Helper Function

        #endregion
    }
    public class AttendanceAdjustmentDao : BaseDao<AttendanceAdjustment, long>, IAttendanceAdjustmentDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public AttendanceAdjustment GetByPinAndDate(int pin, DateTime adjustmentDate)
        {

            ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>()
                                .Add(Restrictions.Eq("Date", adjustmentDate));
            criteria.CreateAlias("TeamMember", "member")
                                .Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
                                .Add(Restrictions.Eq("member.Pin", pin));
            criteria.SetTimeout(3000);
            return criteria.List<AttendanceAdjustment>().FirstOrDefault();
        }

        public AttendanceAdjustment GetTeamMemberLastIsPostAttendanceAdjustment(long? teamMemberId, int? pin)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>();
            criteria.CreateAlias("TeamMember", "member");
            criteria.Add(Restrictions.Eq("IsPost", true));
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("Date"));

                                //.Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
                                //.Add(Restrictions.Eq("member.Id", teamMemberId));
            criteria.SetTimeout(3000);
            return criteria.List<AttendanceAdjustment>().Take(1).FirstOrDefault();
           
        }

        public AttendanceAdjustment GetTeamMemberLastAppliedAttendanceAdjustment(long? teamMemberId, int? pin)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>();
            criteria.CreateAlias("TeamMember", "member");

            var disJoint = Restrictions.Disjunction();
            disJoint.Add(Restrictions.Eq("AdjustmentStatus", (int)AttendanceAdjustmentStatus.Approved));
            disJoint.Add(Restrictions.Eq("AdjustmentStatus", (int)AttendanceAdjustmentStatus.Pending));
            criteria.Add(disJoint);

            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("Date"));

            criteria.SetTimeout(3000);
            return criteria.List<AttendanceAdjustment>().Take(1).SingleOrDefault();

        }

        #endregion

        #region List Loading Function
        public IList<AttendanceAdjustment> LoadAllRowAttendanceAdjustmentSelfMember(TeamMember hrMember, int start, int length, string orderBy, string orderDir)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>().Add(Restrictions.Eq("Status", AttendanceAdjustment.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "hrMember");
            if (hrMember != null)
                criteria.Add(Restrictions.Eq("hrMember.Id", hrMember.Id));
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }
            return criteria.SetFirstResult(start).SetMaxResults(length).List<AttendanceAdjustment>();
        }

        public IList<AttendanceAdjustment> LoadPendingListForMentor(TeamMember hrMember, List<long> teamMemberIdList)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>().Add(Restrictions.Eq("Status", AttendanceAdjustment.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "member").Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active));
            if (!teamMemberIdList.Any())
            {
                teamMemberIdList.Add(0);
            }
            criteria.Add(Restrictions.In("TeamMember.Id", teamMemberIdList));

            criteria.Add(Restrictions.Eq("AdjustmentStatus", (int)AttendanceAdjustmentStatus.Pending));
            return criteria.List<AttendanceAdjustment>().ToList();
        }

        public IList<AttendanceAdjustment> LoadAllMentorAttendanceAdjustmentRecentHistory(int start, int length, string orderBy, string toUpper, List<long> authoTeamMemberIdList, DateTime dateFrom, DateTime dateTo)
        {
            ICriteria criteria = GetMentorAttendanceAdjustmentRecentHistoryQuery(authoTeamMemberIdList, dateFrom, dateTo);
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(toUpper == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }

            return criteria.SetFirstResult(start).SetMaxResults(length).List<AttendanceAdjustment>();

        }

        public IList<AttendanceAdjustmentHrDto> LoadPendingListForHr(string effectiveDate, List<long> organizationIdList, List<long> branchIdList, List<long> campusIdList, List<long> departmentIdList, string pin)
        {
            string query = @"Select  attAd.Id,tm.Id as TeamMemberId,tm.Pin,tm.Name,d.Name as Department,o.ShortName as Organization,b.Name as Branch
                                 ,c.Name as Campus,attAd.Date as Date,attAd.Reason as Reason,attAd.StartTime as StartTime,attAd.EndTime as EndTime
                                 ,attAd.AdjustmentStatus 
                                 from [dbo].[HR_TeamMember] as tm
                                    inner join(
	                                    select * from (
	                                    select eh.CampusId,eh.DepartmentId,eh.TeamMemberId
	                                     ,eh.EffectiveDate,eh.EmploymentStatus
	                                     ,RANK() OVER(PARTITION BY eh.TeamMemberId order by eh.EffectiveDate DESC) as rank
	                                     from [dbo].[HR_EmploymentHistory] as eh
	                                     where";
            if (!String.IsNullOrEmpty(effectiveDate))
            {
                query += " eh.EffectiveDate<='" + effectiveDate + "' AND";
            }
            query += @" eh.Status=1
	                                ) as empHistory where empHistory.rank=1
                                ) as a on a.TeamMemberId=tm.Id
                            left join [dbo].[Campus] as c on c.Id = a.CampusId
                            left join [dbo].[Branch] as b on b.Id = c.BranchId
                            left join [dbo].[Organization] as o on o.Id=b.OrganizationId
                            left join [dbo].[HR_Department] as d on d.Id = a.DepartmentId
                            inner join [dbo].[HR_AttendanceAdjustment] as attAd on attAd.TeamMemberId = tm.Id 
                            ";
            if (!String.IsNullOrEmpty(pin))
            {
                query += @" AND tm.Pin=" + Convert.ToInt32(pin) + "";
            }


            if (organizationIdList != null && organizationIdList.Any() && !organizationIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND o.Id IN(" + string.Join(",", organizationIdList) + ") and o.Status=1 ";
            }
            if (branchIdList != null && branchIdList.Any() && !branchIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND b.Id IN(" + string.Join(",", branchIdList) + ") and b.Status=1 ";
            }
            if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND c.Id IN(" + string.Join(",", campusIdList) + ") AND c.Status = 1 ";
            }
            if (departmentIdList != null && departmentIdList.Any() && !departmentIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND d.Id IN(" + string.Join(",", departmentIdList) + ") and d.Status=1 ";
            }

            query += @" AND attAd.AdjustmentStatus='" + (int)AttendanceAdjustmentStatus.Pending + "'";
            if (!String.IsNullOrEmpty(effectiveDate))
            {
                query += @" AND attAd.Date='" + effectiveDate + "'";
            }

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            iQuery.SetResultTransformer(Transformers.AliasToBean<AttendanceAdjustmentHrDto>());
            var list = iQuery.List<AttendanceAdjustmentHrDto>().ToList();
            return list;
        }

        public IList<AttendanceAdjustmentHrDto> LoadAllHrAttendanceAdjustmentRecentHistory(int start, int length, string orderBy, string toUpper, string pin, DateTime dateFrom, DateTime dateTo, List<long> autorizedBranchIdList, List<long> campusIdList, List<long> departmentIdList)
        {
            string query1 = GetHrAttendanceAdjustmentRecentHistoryQuery(pin, dateFrom, dateTo, autorizedBranchIdList, campusIdList, departmentIdList);
            string query = @"Select * FROM( " + query1 + " ) as a order by a.Id";
            if (length > 0)
            {
                query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            iQuery.SetResultTransformer(Transformers.AliasToBean<AttendanceAdjustmentHrDto>());
            var list = iQuery.List<AttendanceAdjustmentHrDto>().ToList();
            return list;
        }

        public IList<AttendanceAdjustment> LoadAttendanceAdjustmentsForPinAndDate(TeamMember teamMember, DateTime dateTime)
        {
            var empHistory = teamMember.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();
            var organization = empHistory != null ? empHistory.Campus.Branch.Organization : null;
            var orgAttendanceStartTime = organization.AttendanceStartTime.ToString("H:mm");
            var startTime = DateTime.Parse(dateTime.ToString("yyyy-MM-dd") + " " + orgAttendanceStartTime);
            var endTime = startTime.AddHours(23).AddMinutes(59).AddSeconds(59);

            ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>()
                    .Add(Restrictions.Eq("Status", AttendanceAdjustment.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("TeamMember", teamMember));
            //criteria.Add(Expression.Like(Projections.Cast(NHibernateUtil.String, Projections.Property("StartTime")), Convert.ToString(startTime), MatchMode.Anywhere));
            criteria.Add(Restrictions.Ge("StartTime", startTime))
                .Add(Restrictions.Le("EndTime", endTime));
            //criteria.Add(Restrictions.Eq("Date", dateTime));
            var results = criteria.List<AttendanceAdjustment>().ToList();
            return results.Count > 0 ? results : null;
        }

        public IList<AttendanceAdjustment> LoadAttendanceAdjustmentForAttendanceSummary(TeamMember teamMember, DateTime dateTime, Organization organization)
        {
            var orgAttendanceStartTime = organization.AttendanceStartTime.ToString("H:mm");
            var date = dateTime.Date;
            var time = dateTime.ToString("H:mm");
            var punchTime = DateTime.Parse("2001-01-01 " + " " + time);
            var after12AmCheck = DateTime.Parse("2000-01-01 00:00:00.000");
            if (punchTime >= after12AmCheck && punchTime < DateTime.Parse("2001-01-01 " + " " + orgAttendanceStartTime))
            {
                date = dateTime.Date.AddDays(-1);
            }

            var startTime = DateTime.Parse(date.ToString("yyyy-MM-dd") + " " + orgAttendanceStartTime);
            //var endTime = startTime.AddDays(1).AddSeconds(-1);
            var endTime = startTime.AddHours(23).AddMinutes(59).AddSeconds(59);
            ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>()
                    .Add(Restrictions.Eq("Status", AttendanceAdjustment.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("TeamMember", teamMember));
            criteria.Add(Restrictions.Ge("StartTime", startTime))
                .Add(Restrictions.Le("EndTime", endTime));
            criteria.Add(Restrictions.Eq("AdjustmentStatus", (int)AttendanceAdjustmentStatus.Approved));
            var results = criteria.List<AttendanceAdjustment>().ToList();
            return results.Count > 0 ? results : null;
        }
        
        public IList<AttendanceAdjustment> LoadByPinAndDateRange(int pin, DateTime orgStartTime, DateTime orgEndTime)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>()
                                .Add(Restrictions.Eq("Status", AttendanceAdjustment.EntityStatus.Active))
                                .Add(Restrictions.Eq("AdjustmentStatus", (int)AttendanceAdjustmentStatus.Approved))
                                .Add(Restrictions.Ge("StartTime", orgStartTime))
                                .Add(Restrictions.Le("StartTime", orgEndTime))
                                .Add(Restrictions.Ge("EndTime", orgStartTime))
                                .Add(Restrictions.Le("EndTime", orgEndTime));

            criteria.CreateAlias("TeamMember", "member")
                                .Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
                                .Add(Restrictions.Eq("member.Pin", pin));


            return criteria.List<AttendanceAdjustment>();
        }


        public IList<AttendanceAdjustment> LoadAttendanceAdjustments(IList<long> pinList, DateTime dateFrom, DateTime dateTo)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>();

                //.Add(Restrictions.Eq("Status", AttendanceAdjustment.EntityStatus.Active))
                //.Add(Restrictions.Eq("AdjustmentStatus", (int) AttendanceAdjustmentStatus.Approved));
            criteria.Add(Restrictions.In("TeamMember.Id", pinList.ToArray()));
            criteria.Add(Restrictions.Ge("Date", dateFrom));
            criteria.Add(Restrictions.Le("Date", dateTo));
            return criteria.List<AttendanceAdjustment>();
        }

        #endregion

        #region Others Function

        public int CountMentorAttendanceAdjustmentRecentHistory(List<long> authoTeamMemberIdList, DateTime dateFrom, DateTime dateTo)
        {
            //ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>().Add(Restrictions.Eq("Status", AttendanceAdjustment.EntityStatus.Active));
            //criteria.CreateAlias("TeamMember", "member").Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active));

            //DetachedCriteria memberId = DetachedCriteria.For<MentorHistory>().SetProjection(Projections.Property("TeamMember.Id"));
            //memberId.CreateAlias("Mentor", "m").Add(Restrictions.Eq("m.Status", TeamMember.EntityStatus.Active));
            //memberId.Add(Restrictions.Eq("Mentor.Id", hrMember.Id));
            //if (!String.IsNullOrEmpty(pin))
            //{
            //    criteria.Add(Restrictions.Eq("member.Pin", Convert.ToInt32(pin)));
            //}

            //criteria.Add(Subqueries.PropertyIn("TeamMember.Id", memberId));
            //criteria.Add(Restrictions.Not(Restrictions.Eq("AdjustmentStatus", (int)AttendanceAdjustmentStatus.Pending)));
            //if (!String.IsNullOrEmpty(date))
            //{
            //    criteria.Add(Restrictions.Eq("Date", Convert.ToDateTime(date)));
            //}
            ICriteria criteria = GetMentorAttendanceAdjustmentRecentHistoryQuery(authoTeamMemberIdList, dateFrom, dateTo);
            criteria.SetProjection(Projections.RowCount());
            return (int)criteria.UniqueResult();
        }

        public int CountHrAttendanceAdjustmentRecentHistory(string pin, DateTime dateFrom, DateTime dateTo, List<long> autorizedBranchIdList, List<long> campusIdList, List<long> departmentIdList)
        {
            string query = GetHrAttendanceAdjustmentRecentHistoryQuery(pin, dateFrom, dateTo, autorizedBranchIdList, campusIdList, departmentIdList);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            iQuery.SetResultTransformer(Transformers.AliasToBean<AttendanceAdjustmentHrDto>());
            var list = iQuery.List<AttendanceAdjustmentHrDto>().ToList();
            return list.Count;
        }

        public int CountRowAttendanceAdjustmentSelfMember(TeamMember hrMember)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>().Add(Restrictions.Eq("Status", AttendanceAdjustment.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "hrMember");
            if (hrMember != null)
                criteria.Add(Restrictions.Eq("hrMember.Id", hrMember.Id));
            criteria.SetProjection(Projections.RowCount());
            return (int)criteria.UniqueResult();
        }

        public bool HasDuplicate(AttendanceAdjustment hrAttendanceAd)
        {
            var rowList =
                Session.QueryOver<AttendanceAdjustment>()
                    .Where(x => x.Date == hrAttendanceAd.Date
                                && x.TeamMember.Id == hrAttendanceAd.TeamMember.Id
                                && x.StartTime == hrAttendanceAd.StartTime && x.EndTime == hrAttendanceAd.EndTime
                                && x.Status == AttendanceAdjustment.EntityStatus.Active
                                && x.AdjustmentStatus != (int) AttendanceAdjustmentStatus.Cancel);
            if (hrAttendanceAd.Id>0)
            {
                rowList.Where(x => x.Id != hrAttendanceAd.Id);
            }

            var totalRow =rowList.RowCount();
            if (totalRow < 1)
                return false;
            return true;
        }

        public int TeamMemberPendingAttendanceAdjustmentCount(TeamMember teamMember, DateTime dateFrom, DateTime dateTo,
            AttendanceAdjustmentStatus attendanceAdjustmentStatus)
        {
            return
                Session.QueryOver<AttendanceAdjustment>()
                    .Where(
                        x =>
                            x.TeamMember == teamMember && x.Date >= dateFrom && x.Date <= dateTo &&
                            x.AdjustmentStatus == (int) attendanceAdjustmentStatus)
                    .List<AttendanceAdjustment>()
                    .Count();
        }

        public int TeamMemberPendingAttendanceAdjustmentCount(TeamMember teamMember, DateTime dateTime1, DateTime dateTime2,
            LeaveStatus leaveStatus)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Helper Function

        public ICriteria GetMentorAttendanceAdjustmentRecentHistoryQuery(List<long> authoTeamMemberIdList, DateTime dateFrom, DateTime dateTo)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceAdjustment>().Add(Restrictions.Eq("Status", AttendanceAdjustment.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "member").Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active));
            
            criteria.Add(Restrictions.In("member.Id", authoTeamMemberIdList));

            criteria.Add(Restrictions.Not(Restrictions.Eq("AdjustmentStatus", (int)AttendanceAdjustmentStatus.Pending)));

            criteria.Add(Restrictions.Ge("Date", dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0)));
            criteria.Add(Restrictions.Le("Date", dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59)));
           
            return criteria;
        }

        private string GetHrAttendanceAdjustmentRecentHistoryQuery(string pin, DateTime dateFrom, DateTime dateTo, List<long> autorizedBranchIdList, List<long> campusIdList, List<long> departmentIdList)
        {
            string query = @"Select  attAd.Id,tm.Id as TeamMemberId,tm.Pin,tm.Name,d.Name as Department,o.ShortName as Organization,b.Name as Branch
                                 ,c.Name as Campus,attAd.Date as Date,attAd.Reason as Reason,attAd.StartTime as StartTime,attAd.EndTime as EndTime
                                 ,attAd.AdjustmentStatus, attAd.ModifyBy as ModifyBy, attAd.ModificationDate as LastModificationDateTime   
                                 from [HR_TeamMember] as tm
                                    inner join(
	                                    select * from (
	                                    select eh.CampusId,eh.DepartmentId,eh.TeamMemberId
	                                     ,eh.EffectiveDate,eh.EmploymentStatus
	                                     ,RANK() OVER(PARTITION BY eh.TeamMemberId order by eh.EffectiveDate DESC) as rank
	                                     from [HR_EmploymentHistory] as eh
	                                     where";
            
            query += " eh.EffectiveDate<='" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "' AND";
            
            query += @" eh.Status=1
	                                ) as empHistory where empHistory.rank=1
                                ) as a on a.TeamMemberId=tm.Id
                            left join [Campus] as c on c.Id = a.CampusId
                            left join [Branch] as b on b.Id = c.BranchId
                            left join [Organization] as o on o.Id=b.OrganizationId
                            left join [HR_Department] as d on d.Id = a.DepartmentId
                            inner join [HR_AttendanceAdjustment] as attAd on attAd.TeamMemberId = tm.Id
                            ";
            if (!String.IsNullOrEmpty(pin))
            {
                query += @" AND tm.Pin=" + Convert.ToInt32(pin) + "";
            }
            if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND c.Id IN(" + string.Join(",", campusIdList) + ") AND c.Status = 1";
            }
            if (autorizedBranchIdList != null && autorizedBranchIdList.Any() && !autorizedBranchIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND b.Id IN(" + string.Join(",", autorizedBranchIdList) + ") and b.Status=1";
            }
            if (departmentIdList != null && departmentIdList.Any() && !departmentIdList.Contains(SelectionType.SelelectAll))
            {
                query += @" AND d.Id IN(" + string.Join(",", departmentIdList) + ") and d.Status=1";
            }
            
            query += @" AND (attAd.AdjustmentStatus!='" + (int)AttendanceAdjustmentStatus.Pending + "' AND attAd.AdjustmentStatus!='" + (int)AttendanceAdjustmentStatus.Cancel + "')";
            
            query += @" AND attAd.Date >='" + dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "' AND attAd.Date <='" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "' ";

            return query;
        }
        
        #endregion
    }
}
