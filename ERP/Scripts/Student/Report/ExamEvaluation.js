﻿
$(document).ready(function () {
    $('#countBtn').attr('disabled', 'disabled');
    console.log("EXAM EVALUATION INITIALIZEDs");
    //organization change functionalities
    $("#OrganizationId").change(function () {
        var organizatonId = $(this).val();
        $("#ProgramId option").remove();
        $("#SelectedCourse option").remove();
        $("#SelectedBranch option").remove();
        $("#SelectedCampus option").remove();
        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();
        $('#SessionId').empty();
        $("FromTime").val('');
        $("#ToTime").val('');
        $('#SessionId').append("<option value=''>Select Session</option>");
        $('#SelectedCourse').append("<option value=''>Select Course</option>");
        $('#SelectedBranch').append("<option value=''>Select Branch</option>");
        $('#SelectedCampus').append("<option value=''>Select Campus</option>");
        $('#SelectedBatchDays').append("<option value=''>Select Batch Days</option>");
        $('#SelectedBatchTime').append("<option value=''>Select Batch Time</option>");
        $('#SelectedBatch').append("<option value=''>Select Batch</option>");
        if (organizatonId != "") {
            $.ajax({
                //url: 'GetProgramTeacherByOrganization',
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                type: "POST",
                data: { organizationIds: organizatonId, isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    if (response.IsSuccess) {
                        //load programs
                        $('#ProgramId').append('<option value="">Select Program</option>');
                        if (response.returnProgramList != null && response.returnProgramList.length > 0) {
                            $.each(response.returnProgramList, function (i, v) {
                                $('#ProgramId').append($('<option>').text(v.Text).attr('value', v.Value));
                            });

                            $("#ProgramId").find('option:eq(0)').prop('selected', true);
                        }

                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error:
                    function (response) {
                        var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a> Program and Branch load failed</div>';
                        $('.customMessage').append(errorMessage);

                    }
            });

        } else {

            $('#ProgramId').append('<option value="">Select Program</option>');
        }
    });
    //Program Change Functionalities
    $(document).on('change', '#ProgramId', function () {

        $("#missingImageCount").html("...");

        //remove all dependant dropdownlist & selection option
        $("#SelectedCourse option").remove();
        $("#SelectedBranch option").remove();
        $("#SelectedCampus option").remove();

        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();
        $("FromTime").val('');
        $("#ToTime").val('');

        $('#SessionId').empty();
        $('#SessionId').append("<option value=''>Select a session</option>");


        var programId = $("#ProgramId").val();

        if (programId != null && programId != "") {
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForSession",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                cache: false,
                async: true,
                data: { programIds: programId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    $.each(response.returnSessionList, function (i, v) {
                        $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                    //$("#SessionId").html(result);
                    console.log("Success -----");
                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    //console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    //console.log(result);
                }
            });
        }
    });


    //Session Change Functionalities
    $(document).on('change', '#SessionId', function () {

        $("#missingImageCount").html("...");

        //remove all dependant dropdownlist & selection option
        $("#SelectedCourse option").remove();
        $("#SelectedBranch option").remove();
        $("#SelectedCampus option").remove();

        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();
        $("FromTime").val('');
        $("#ToTime").val('');

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var organizationId = $("#OrganizationId").val();
        if (programId != null && programId != "" && sessionId != null && sessionId != "") {

            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForBranch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, organizationIds: organizationId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $('#SelectedBranch').append("<option value='0'>All Branch</option>");
                    if (result.IsSuccess) {
                        $.each(result.returnBranchList, function (i, v) {
                            $('#SelectedBranch').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        console.log("Success -----");

                        $.ajax({
                            //url: 'GetCourseList',
                            //url: '/Student/Attendance/GetCourseList',
                            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
                            type: "POST",
                            data: { programIds: programId, sessionIds: sessionId },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (result1) {
                                $.unblockUI();
                                $('#SelectedCourse').append("<option value='0'>All Course</option>");
                                if (result1.IsSuccess) {
                                    $.each(result1.returnCourse, function (i, v) {
                                        $('#SelectedCourse').append($('<option>').text(v.Text).attr('value', v.Value));
                                    });
                                    $("#SelectedCourse").find('option:eq(0)').prop('selected', true);

                                    $("#SelectedSubject option").remove();
                                    $("FromTime").val('');
                                    $("#ToTime").val('');

                                    var organizatonId = $("#OrganizationId").val();
                                    var programIdForCourse = $("#ProgramId").val();
                                    var sessionIdForSession = $("#SessionId").val();
                                    var courseId = $("#SelectedCourse").val();

                                    if (organizatonId.length > 0 && programId.length > 0 && sessionId.length > 0 && courseId.length > 0) {
                                        $.ajax({
                                            //url: 'GetCourseList',
                                            url: '/Student/StudentExamEvaluationReport/AjaxRequestForSubject',
                                            type: "POST",
                                            data: { organizatonId: organizatonId, programId: programIdForCourse, sessionId: sessionIdForSession, courseId: courseId },
                                            beforeSend: function () {
                                                $.blockUI({
                                                    timeout: 0,
                                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                                });
                                            },
                                            success: function (result2) {
                                                $.unblockUI();
                                                if (result2.IsSuccess) {
                                                    $.each(result2.subjectList, function (i, v) {
                                                        $('#SelectedSubject').append($('<option>').text(v.Text).attr('value', v.Value));
                                                    });
                                                    $("#SelectedSubject").find('option:eq(0)').prop('selected', true);
                                                    console.log("Success ------");
                                                } else {
                                                    console.log("Error -----");
                                                    console.log(result);
                                                }
                                            },
                                            complete: function () {
                                                $.unblockUI();
                                                console.log("Complete -----");
                                                console.log(result);
                                            },
                                            error: function (result) {
                                                $.unblockUI();
                                                console.log("Failed -----");
                                                console.log(result);
                                            }
                                        });
                                    }
                                    /*call back end */
                                    console.log("Success ------");
                                } else {
                                    console.log("Error -----");
                                    console.log(result);
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                                console.log("Complete -----");
                                console.log(result);
                            },
                            error: function (result) {
                                $.unblockUI();
                                console.log("Failed -----");
                                console.log(result);
                            }
                        });
                    } else {
                        $.unblockUI();
                        console.log("Error -----");
                        console.log(result);
                    }

                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });

    $(document).on('change', '#SelectedCourse', function () {
        $("#SelectedSubject option").remove();
        $("FromTime").val('');
        $("#ToTime").val('');

        var organizatonId = $("#OrganizationId").val();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var courseId = $("#SelectedCourse").val();

        if (organizatonId.length > 0 && programId.length > 0 && sessionId.length > 0 && courseId.length > 0) {
            $.ajax({
                //url: 'GetCourseList',
                url: '/Student/StudentExamEvaluationReport/AjaxRequestForSubject',
                type: "POST",
                data: { organizatonId: organizatonId, programId: programId, sessionId: sessionId, courseId: courseId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result2) {
                    $.unblockUI();
                    if (result2.IsSuccess) {
                        $.each(result2.subjectList, function (i, v) {
                            $('#SelectedSubject').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        $("#SelectedSubject").find('option:eq(0)').prop('selected', true);
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        console.log(result);
                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }

    });
    //Branch Change Functionalities
    $(document).on('change', '#SelectedBranch', function () {

        $("#missingImageCount").html("...");
        $("FromTime").val('');
        $("#ToTime").val('');
        //remove all dependant dropdownlist & selection option 
        $("#SelectedCampus option").remove();

        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();

        if (programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "") {
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForCampus",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, isAuhorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#SelectedCampus').append("<option value='0'>All campus</option>");
                    if (result.IsSuccess) {
                        $.each(result.returnCampusList, function (i, v) {
                            $('#SelectedCampus').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        /// $("#SelectedCampus").find('option:eq(0)').prop('selected', true); 
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        console.log(result);
                    }

                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });


    //Campus Change Functionalities
    $(document).on('change', '#SelectedCampus', function () {

        $("#missingImageCount").html("...");
        $("FromTime").val('');
        $("#ToTime").val('');
        //remove all dependant dropdownlist & selection option  
        $("#SelectedBatchDays option").remove();
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();

        if (programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "" && campusId != null && campusId != "") {
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForBatchDay",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchDay",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId, isAuhorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#SelectedBatchDays').append("<option value='0'>All Batch Days</option>");
                    if (result.IsSuccess) {
                        $.each(result.returnBatchDays, function (i, v) {
                            $('#SelectedBatchDays').append($('<option>').text(v.Text).attr('value', v.Text));
                            //if (v.Value == "0") {
                            //    $('#SelectedBatchDays').append($('<option>').text(v.Text).attr('value', v.Value));
                            //} else {
                            //    $('#SelectedBatchDays').append($('<option>').text(v.Text).attr('value', v.Text));
                            //}
                        });
                        /// $("#SelectedBatchDays").find('option:eq(0)').prop('selected', true);
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        console.log(result);
                    }

                }, complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });


    //Batch Day Change Functionalities
    $(document).on('change', '#SelectedBatchDays', function () {

        $("#missingImageCount").html("...");
        $("FromTime").val('');
        $("#ToTime").val('');
        //remove all dependant dropdownlist & selection option  
        $("#SelectedBatchTime option").remove();
        $("#SelectedBatch option").remove();
        $("#SelectedExam option").remove();
        $('#countBtn').attr('disabled', 'disabled');
        $('#countValue').val('0');

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var batchDays = $("#SelectedBatchDays").val();

        if (programId != null && programId != ""
            && sessionId != null && sessionId != ""
            && branchId != null && branchId != ""
            && campusId != null && campusId != ""
            && batchDays != null && batchDays != "") {
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForBatchTime",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchTime",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId, batchDays: batchDays, isAuhorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#SelectedBatchTime').append("<option value='0'>All Batch Times</option>");
                    if (result.IsSuccess) {
                        $.each(result.returnBatchTime, function (i, v) {
                            $('#SelectedBatchTime').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        /// $("#SelectedBatchTime").find('option:eq(0)').prop('selected', true);
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        console.log(result);
                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });


    //Batch Time Change Functionalities
    $(document).on('change', '#SelectedBatchTime', function () {

        $("#missingImageCount").html("...");
        $("FromTime").val('');
        $("#ToTime").val('');
        //remove all dependant dropdownlist & selection option  
        $("#SelectedBatch option").remove();
        $("#SelectedExam option").remove();
        $('#countBtn').attr('disabled', 'disabled');
        $('#countValue').val('0');

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var batchDays = $("#SelectedBatchDays").val();
        var batchTime = $("#SelectedBatchTime").val();

        var batchTimeText = [];
        $("#SelectedBatchTime option:selected").each(function () {
            var $this = $(this);
            if ($this.length) {
                batchTimeText.push($this.text());
            }
        });

        if (programId != null && programId != ""
            && sessionId != null && sessionId != ""
            && branchId != null && branchId != ""
            && campusId != null && campusId != ""
            && batchDays != null && batchDays != ""
            && batchTime != null && batchTime != "") {
            $.ajax({
                type: "post",
                //url: "/Student/Attendance/AjaxRequestForBatch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatch",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId, batchDays: batchDays, batchTimes: batchTime, isAuhorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#SelectedBatch').append("<option value='0'>All batch</option>");
                    if (result.IsSuccess) {
                        $.each(result.returnBatch, function (i, v) {
                            $('#SelectedBatch').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        /// $("#batchName").find('option:eq(0)').prop('selected', true);
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        console.log(result);
                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });
    $("#SelectedSubject").change(function () {

        //remove all dependant dropdownlist & selection option
	    $("#SelectedExam option").remove();       
	    $("#SelectedLecture option").remove();
        $('#SelectedLecture').append("<option value='0'>All Lecture</option>");
        var courseId = $("#SelectedCourse").val();
        var subjectId = $("#SelectedSubject").val();
        if (subjectId != null && subjectId != "") {
            $.ajax({
                type: "post",
                url: '@Url.Action("GetLecture", "StudentClassEvaluationReport")',
                cache: false,
                async: true,
                data: { courseIdList: courseId, subjectIdList: subjectId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.each(result.lectureList, function (i, v) {
                            $('#SelectedLecture').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");

                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }

    });
    $(document).on('change', '#SelectedSubject,#ToTime', function () {
        //$("#SelectedExam option").remove();
        $('#countBtn').attr('disabled', 'disabled');
        $('#countValue').val('0');
		$("#SelectedExam option").remove();
		var organizationId = $("#OrganizationId").val();
		var programId = $("#ProgramId").val();
		var sessionId = $("#SessionId").val();
		var courseId = $("#SelectedCourse").val();
		var branchId = $("#SelectedBranch").val();
		var campusId = $("#SelectedCampus").val();
		var batchDays = $("#SelectedBatchDays").val();
		var batchTime = $("#SelectedBatchTime").val();
		var batchId = $("#SelectedBatch").val();
        var subjectId = $("#SelectedSubject").val();
        var dateFrom = $("#FromTime").val();
        var dateTo = $("#ToTime").val();

        if (programId.length > 0 && sessionId.length > 0 & branchId.length > 0 && campusId.length > 0 && batchId.length > 0 && subjectId.length > 0 && dateFrom.length > 0 && dateTo.length > 0) {
            $.ajax({
                type: "post",
                url: "/Student/StudentExamEvaluationReport/LoadExams",
                cache: false,
                async: true,
                data: { organizationId: organizationId, programId: programId, sessionId: sessionId, courseId: courseId, branchId: branchId, campusId: campusId, batchDays: batchDays, batchTime: batchTime, batchId: batchId, subjectId: subjectId, dateFrom: dateFrom, dateTo: dateTo },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.each(result.examList, function (i, v) {
                            $('#SelectedExam').append($('<option>').text(v.Text).attr('value', v.Value));
                        });

                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        console.log(result);
                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });

    $(document).on('change', '#FromTime', function () {
        $("#SelectedExam option").remove();
        $('#countBtn').attr('disabled', 'disabled');
        $('#countValue').val('0');

        $("#ToTime").val('');
    });

    $(document).on('change', '#SelectedExam', function () {
        $('#countBtn').removeAttr('disabled', 'disabled');
    });
    $('#informationViewList').bootstrapDualListbox({
        nonselectedlistlabel: 'Non-selected',
        selectedlistlabel: 'Selected',
        preserveselectiononmove: 'moved',
        moveOnSelect: false,
        showFilterInputs: false,
        infoText: '',
    });
    $(document).on('click', '#countBtn', function () {
        var organizationId = $("#OrganizationId").val();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var courseId = $("#SelectedCourse").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var batchDays = $("#SelectedBatchDays").val();
        var batchTime = $("#SelectedBatchTime").val();
        var batchId = $("#SelectedBatch").val();
        var subjectId = $("#SelectedSubject").val();
        var dateFrom = $("#FromTime").val();
        var dateTo = $("#ToTime").val();

        var examIds = $('#SelectedExam').val();
        if (examIds.length > 0) {

            $.ajax({
                type: "post",
                url: "/Student/StudentExamEvaluationReport/GetTotalExamEvaluationRow",
                cache: false,
                async: true,
                data: { organizationId: organizationId, programId: programId, sessionId: sessionId, courseId: courseId, branchId: branchId, campusId: campusId, batchDays: batchDays, batchTime: batchTime, batchId: batchId, subjectId: subjectId, dateFrom: dateFrom, dateTo: dateTo, examIds: examIds },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $('#countValue').val(result.rowCount);
                    } else {
                        $('#countValue').val(0);
                    }


                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });
});