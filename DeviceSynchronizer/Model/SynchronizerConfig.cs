﻿namespace DeviceSynchronizer.Model
{
    public class SynchronizerConfig
    {
        public string SyncKey { get; set; }
        public string ApiKey { get; set; }
    }
}
