﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.FestivalBonusSettingTest
{
    
    [Trait("Area", "Hr")]
    [Trait("Service", "FestivalBonusSetting")]
    public class FestivalBonusSettingDeleteTest : FestivalBonusSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Successfully_Delete()
        {
            _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();
            Assert.True(_festivalBonusSettingService.IsDelete(_festivalBonusSettingFactory.Object.Id));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_InvalidDataException()
        {
            _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();
            Assert.Throws<InvalidDataException>(
                () => _festivalBonusSettingService.IsDelete(_festivalBonusSettingFactory.Object.Id + 1));
        }  

        #endregion
       
    }
}
