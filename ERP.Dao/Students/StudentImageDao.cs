﻿using System.Collections.Generic;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentImageDao : IBaseDao<StudentProgramImage, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<StudentProgramImage> ImageAlreadyExists(string hash);
        #endregion

        #region Others Function

        #endregion

    }


    public class StudentImageDao : BaseDao<StudentProgramImage, long>, IStudentImageDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<StudentProgramImage> ImageAlreadyExists(string hash)
        {
            return Session.QueryOver<StudentProgramImage>().Where(x => x.Hash == hash && x.Status != StudentProgramImage.EntityStatus.Delete).List<StudentProgramImage>();
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
}