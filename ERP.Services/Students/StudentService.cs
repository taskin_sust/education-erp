﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IStudentService : IBaseService
    {
        #region Operational Function
        void UpdateStudent(Student student);
        bool UpdateStudentinfo(string prnNo, StudentAcademicInfo studentAcademicInfo, StudentAcademicInfoBaseData studentAcademicInfoBase);
        #endregion

        #region Single Instances Loading Function
        Student LoadById(long id);
        Student GetStudentByRollNumber(string stdRollNumber);
        Student GetByMobileAndPrnNumber(string prnNo, string mobNumber);
        #endregion

        #region List Loading Function
       
        #endregion

        #region Others Function
        int GetMobileCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, 
            long[] batchId, long[] courseId, int version, int gender, int[] religion, string startDate, string endDate, int selectedStatus,int[] smsReciever, 
            string prnNo = null, string name = null, string mobile = null);
        #endregion

        #region Helper Function

        #endregion

    }

    public class StudentService : BaseService, IStudentService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentDao _studentDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly IStudentCourseDetailDao _studentCourseDetailDao;
        private readonly IInstituteDao _instituteDao;
        private CommonHelper _commonHelper;
        public StudentService(ISession session)
        {
            Session = session;
            _studentDao = new StudentDao() { Session = Session };
            _studentProgramDao = new StudentProgramDao() { Session = Session };
            _studentCourseDetailDao = new StudentCourseDetailDao() { Session = Session };
            _instituteDao = new InstituteDao() { Session = Session };
            _commonHelper=new CommonHelper();
        }
        #endregion

        #region Operational Function

        public void UpdateStudent(Student student)
        {
            using (var txn = Session.BeginTransaction())
            {
                _studentDao.Update(student);
                txn.Commit();
            }
        }

        public bool UpdateStudentinfo(string prnNo, StudentAcademicInfo studentAcademicInfo, StudentAcademicInfoBaseData studentAcademicInfoBase)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    Student student = studentAcademicInfo.Student;
                    bool check = false;
                    bool isInstitute = false;
                    if (!student.IsSync)
                    {
                        student.FullName = studentAcademicInfoBase.Name;
                        student.FatherName = studentAcademicInfoBase.FathersName;
                        student.IsSync = true;
                        _studentDao.Update(student);
                        check = true;
                    }
                    StudentProgram studentProgram = _studentProgramDao.GetStudentProgram(prnNo);
                    Institute institute = _instituteDao.LoadByInstituteName(studentAcademicInfoBase.Institute);
                    if (institute != null && !studentProgram.IsSync)
                    {
                        studentProgram.Institute = institute;
                        studentProgram.IsSync = true;
                        _studentProgramDao.Update(studentProgram);
                        isInstitute = true;
                    }
                    if (isInstitute && check)
                        transaction.Commit();
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                return false;
            }
            return true;
        }
        #endregion

        #region Single Instances Loading Function
        public Student LoadById(long id)
        {
            try
            {
                return _studentDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Student GetStudentByRollNumber(string stdRollNumber)
        {
            try
            {
                return _studentDao.GetStudentByRollNumber(stdRollNumber);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Student GetByMobileAndPrnNumber(string prnNo, string mobNumber)
        {
            return _studentDao.GetByMobileAndPrnNumber(prnNo, mobNumber);
        }

        #endregion

        #region List Loading Function
       
        #endregion

        #region Others Function

        public int GetMobileCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId,
            string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender,
            int[] religion, string startDate, string endDate, int selectedStatus,int[] smsReciever, string prnNo = null,
            string name = null, string mobile = null)
        {
            try
            {
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                var mobileCount = _studentDao.GetMobileCount(authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus, prnNo, name, mobile, smsReciever);
                return mobileCount;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Helper Function

        #endregion       
    }
}