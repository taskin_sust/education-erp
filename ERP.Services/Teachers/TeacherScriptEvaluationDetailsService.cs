﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherScriptEvaluationDetailsService : IBaseService
    {
        #region Operational Function

        bool Update(TeacherScriptEvaluationDetails scriptEvaluationDetails);

        #endregion

        #region Single Instances Loading Function

        TeacherScriptEvaluationDetails GetTeacherScriptEvaluationDetailById(long id);

        #endregion

        #region List Loading Function

        IList<TeacherScriptEvaluationDetails> TeacherScriptEvaluationsList(int start, int length, List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string paymentStatus, string date, long? selectCourseId, string teacherName, long? selectBranchId, string trackingId, string tpin);

        #endregion

        #region Others Function

        long CountTeacherScriptEvaluation(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string paymentStatus, string date, long? selectCourseId, string teacherName, long? selectBranchId, string trackingId, string tpin);

        #endregion

    }

    public class TeacherScriptEvaluationDetailsService : BaseService, ITeacherScriptEvaluationDetailsService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("TeacherScriptService");
        private readonly ITeacherScriptEvaluationDetailsDao _teacherScriptEvaluationDetailsDao;

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;

        public TeacherScriptEvaluationDetailsService(ISession session)
        {
            Session = session;
            _teacherScriptEvaluationDetailsDao = new TeacherScriptEvaluationDetailsDao() { Session = session };
            _commonHelper = new CommonHelper();

        }

        #endregion

        #region Operational Function

        public bool Update(TeacherScriptEvaluationDetails scriptEvaluationDetails)
        {
            ITransaction transaction = null;
            try
            {

                using (transaction = Session.BeginTransaction())
                {
                    _teacherScriptEvaluationDetailsDao.Update(scriptEvaluationDetails);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }

        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public TeacherScriptEvaluationDetails GetTeacherScriptEvaluationDetailById(long id)
        {
            try
            {
                return _teacherScriptEvaluationDetailsDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<TeacherScriptEvaluationDetails> TeacherScriptEvaluationsList(int start, int length, List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string paymentStatus, string date, long? selectCourseId, string teacherName, long? selectBranchId, string trackingId, string tpin)
        {
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, _commonHelper.ConvertIdToList(organizationId), null, _commonHelper.ConvertIdToList(programId));
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId));
                return _teacherScriptEvaluationDetailsDao.TeacherScriptEvaluationsList(start, length, programIdList, branchIdList, sessionId, paymentStatus, date, selectCourseId, teacherName, selectBranchId, trackingId, tpin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public long CountTeacherScriptEvaluation(List<UserMenu> userMenu, long organizationId, long programId, long sessionId, string paymentStatus, string date, long? selectCourseId, string teacherName, long? selectBranchId, string trackingId, string tpin)
        {
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, _commonHelper.ConvertIdToList(organizationId), null, _commonHelper.ConvertIdToList(programId));
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId));
                return _teacherScriptEvaluationDetailsDao.CountTeacherScriptEvaluation(programIdList, branchIdList, sessionId, paymentStatus, date, selectCourseId, teacherName, selectBranchId, trackingId, tpin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        #endregion

    }
}
