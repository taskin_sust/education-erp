﻿/*Written By Sajjad Raihan */
$(document).ready(function () {
    var today = new Date();

    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var formatedDate = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day;

    //console.log(today);
    // var formatedDate = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate();

    $('.form-date').val(formatedDate);
    $('.form-date').datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: false,
        showMeridian: true,
        initialDate: formatedDate,
        startView: 2,
        minView: 2,
        maxView: 4
    });
    $(document).on('click', '#Submit', function (e) {
        $('#omsg').text("");
        $('#pmsg').text("");
        $('#smsg').text("");
        $('#brmsg').text("");
        $('#campmsg').text("");
        $('#bdmsg').text("");
        $('#btmsg').text("");
        $('#bnmsg').text("");
        $('#cmsg').text("");
        $('#infomsg').text("");
        var organizationId = $("#OrganizationId").val();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#branchId").val();
        var campusId = $("#campusId").val();
        var batchDays = $("#batchDays").val();
        var batchTime = $("#batchTime").val();
        var batchName = $("#batchName").val();
        var course = $("#course").val();

        var informationViewList = $("#informationViewList").val();
        var isSuccess = true;
        if (organizationId == null || organizationId <= 0) {
            $('#omsg').text("");
            var organizationMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="omsg">Select Organization</span></span>';
            $('#omsg').append(organizationMessage);
            isSuccess = false;
        }
        if (programId == null || programId <= 0) {
            $('#pmsg').text("");
            var programMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="pmsg">Select Program</span></span>';
            $('#pmsg').append(programMessage);
            isSuccess = false;
        }

        if (sessionId == null || sessionId <= 0) {
            $('#smsg').text("");
            var sessionMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="smsg">Select Session</span></span>';

            $('#smsg').append(sessionMessage);
            isSuccess = false;
        }

        if (branchId == null || branchId == "") {
            $('#brmsg').text("");
            var branchMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="brmsg">Select Branch</span></span>';
            $('#brmsg').append(branchMessage);
            isSuccess = false;
        }

        if (campusId == null || campusId == "") {
            $('#campmsg').text("");
            var campusMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="campmsg">Select Campus</span></span>';
            $('#campmsg').append(campusMessage);
            isSuccess = false;
        }

        if (batchDays == null || batchDays == "") {
            $('#bdmsg').text("");
            var batchDayMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="bdmsg">Select a Batch Day</span></span>';
            $('#bdmsg').append(batchDayMessage);
            isSuccess = false;
        }

        if (batchTime == null || batchTime == "") {
            $('#btmsg').text("");
            var batchTimeMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="btmsg">Select Batch Time</span></span>';
            $('#btmsg').append(batchTimeMessage);
            isSuccess = false;
        }

        if (batchName == null || batchName == "") {
            $('#bnmsg').text("");
            var batchNameMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="bmsg">Select Batch</span></span>';
            $('#bnmsg').append(batchNameMessage);
            isSuccess = false;
        }


        if (course == null || course == "") {
            $('#cmsg').text("");
            var batchNameMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="coursemsg">Select Course</span></span>';
            $('#cmsg').append(batchNameMessage);
            isSuccess = false;
        }
        if (informationViewList == null || informationViewList == "") {
            $('#infomsg').text("");
            var batchNameMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="infomsg">Select at least one information</span></span>';
            $('#infomsg').append(batchNameMessage);
            isSuccess = false;
        }
        if (isSuccess == true) {
            $('form#generateDueListForm').submit();
            return true;
        } else {
            e.preventDefault();
            return false;
        }


    });

    //organization
    $(document).on('change', '#OrganizationId', function () {
        var organizationId = $("#OrganizationId").val();
        //console.log("GGGG",organizationId);

        $("#ProgramId option").remove();
        $("#SessionId option").remove();
        $("#course option").remove();
        $("#branchId option").remove();
        $("#campusId option").remove();
        $('#batchDays option').remove();
        $('#batchTime option').remove();
        $('#batchName option').remove();
        //var x =
        if (organizationId != null && organizationId != "") {
            $('#omsg').text("");
            $.ajax({
                //url: $("body").attr("data-project-root") + "Student/Payment/LoadProgram",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                type: "POST",
                data: { organizationIds: organizationId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $('#ProgramId').append('<option value="">Select program</option>');
                        $.each(response.returnProgramList, function (i, v) {
                            $('#ProgramId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error:
                    function (response) {
                        // alert("Session load failed");
                        var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a> Session load failed</div>';
                        $('.customMessage').append(errorMessage);
                        //console.log("Failed -----");
                        // console.log(response);
                    }
            });
        }

    });

    $(document).on('change', '#ProgramId', function () {

        var organizationId = $("#OrganizationId").val();
        var programId = $("#ProgramId").val();

        $("#SessionId option").remove();
        $("#course option").remove();
        $("#branchId option").remove();
        $("#campusId option").remove();
        $('#batchDays option').remove();
        $('#batchTime option').remove();
        $('#batchName option').remove();
        //var x =
        if (organizationId != null && organizationId != "" && programId != null && programId != "") {
            $('#pmsg').text("");
            $.ajax({
                //url: $("body").attr("data-project-root") + "Student/Payment/GetSession",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                type: "POST",
                data: { organizationIds: organizationId, programIds: programId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $('#SessionId').append('<option value="">Select Session</option>');
                        //$.each(response.sessionList, function (i, v) {
                        //    $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        $.each(response.returnSessionList, function (i, v) {
                            $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        //// $("#SessionId").find('option:eq(0)').prop('selected', true);
                    }
                    else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }

                },
                complete: function () {
                    $.unblockUI();
                },
                error:
                    function (response) {
                        // alert("Session load failed");
                        var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a> Session load failed</div>';
                        $('.customMessage').append(errorMessage);
                        //console.log("Failed -----");
                        // console.log(response);
                    }
            });
        }
        // SESSION
    });

    $(document).on('change', "#SessionId", function () {

        // BRANCH && COURSE
        var organizationIds = $("#OrganizationId").val();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();

        $("#course option").remove();
        $("#branchId option").remove();
        $("#campusId option").remove();
        $('#batchDays option').remove();
        $('#batchTime option').remove();
        $('#batchName option').remove();
        if (sessionId != null && sessionId != "") {
            $('#smsg').text("");
        }
        if (organizationIds != null && organizationIds != "" && programId != null && programId != "" && sessionId != null && sessionId != "") {
            // COURSE
            //$.ajax({

            //    //url: $("body").attr("data-project-root") + "Student/Payment/GetCourseList",
            //    url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
            //    type: "POST",
            //    //data: { "programId": programId, "sessionId": sessionId },
            //    data: { organizationIds: organizationIds, programIds: programId, sessionIds: sessionId },

            //    beforeSend: function () {
            //        $.blockUI({
            //            timeout: 0,
            //            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
            //        });
            //    },

            //    success: function (response) {
            //        if (response.IsSuccess) {
            //            $('#course').append('<option value="0">All Course</option>');
            //            $.each(response.returnCourse, function (i, v) {
            //                $('#course').append($('<option>').text(v.Text).attr('value', v.Value));
            //            });
            //            //$("#course").find('option:eq(0)').prop('selected', true);
            //        } $.unblockUI();
            //    }, complete: function () {
            //        $.unblockUI();
            //    },
            //    error: function (response) {
            //        var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Branch load failed</div>';
            //        $('.customMessage').append(errorMessage);
            //    }
            //});

           

            $.ajax({

                //url: $("body").attr("data-project-root") + "Student/Payment/AjaxRequestForBranch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                type: "POST",
                //data: { "programId": programId, "sessionId": sessionId },
                data: { organizationIds: organizationIds, programIds: programId, sessionIds: sessionId, isAuthorized: true },

                success: function (response) {
                    if (response.IsSuccess) {
                        $('#branchId').append('<option value="0">All Branch</option>');
                        $.each(response.returnBranchList, function (i, v) {
                            $('#branchId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                }, complete: function () {
                    $.unblockUI();
                },
                error: function (response) {
                    //alert("Branch load failed");
                    //console.log("Failed -----");
                    //console.log(response);
                    var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Branch load failed</div>';
                    $('.customMessage').append(errorMessage);
                }
            });
        }

        //BRANCH
    });

    $(document).on('change', "#branchId", function () {


        $("#campusId option").remove();
        $('#batchDays option').remove();
        $('#batchTime option').remove();
        $('#batchName option').remove();

        var organizationId = $("#OrganizationId").val();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#branchId").val();
        if (branchId != null && branchId != "") {
            $('#brmsg').text("");
        }
        if (organizationId != null && organizationId != "" && programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "") {
            $.ajax({

                //url: $("body").attr("data-project-root") + "Student/Payment/AjaxRequestForCampus",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                type: "POST",
                //data: { "branchId": branchId, "programId": programId, "sessionId": sessionId },
                data: { organizationIds: organizationId, programIds: programId, branchIds: branchId, sessionIds: sessionId, isAuthorized: true },

                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $('#campusId').append('<option value="0">All Campus</option>');
                        $.each(response.returnCampusList, function (i, v) {
                            $('#campusId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }

                }, complete: function () {
                    $.unblockUI();
                },
                error: function (response) {
                    //alert("Campus load failed");
                    //console.log("Failed -----");
                    //console.log(response);
                    var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Campus load failed</div>';
                    $('.customMessage').append(errorMessage);
                }
            });
        }

    });

    $(document).on('change', "#campusId", function () {

        $('#batchDays option').remove();
        $('#batchTime option').remove();
        $('#batchName option').remove();

        var organizationId = $("#OrganizationId").val();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#branchId").val();
        var campusId = $("#campusId").val();
        if (campusId != null && campusId != "") {
            $('#campmsg').text("");
        }
        if (organizationId != null && organizationId != "" && programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "" && campusId != null && campusId != "") {
            $.ajax({

                //url: $("body").attr("data-project-root") + "Student/Payment/AjaxRequestForBatchDay",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchDay",
                type: "POST",
                //data: { "programId": programId, "sessionId": sessionId, "branchId": branchId, "campusId": campusId },
                data: { organizationIds: organizationId, programIds: programId, branchIds: branchId, sessionIds: sessionId, campusIds: campusId, isAuthorized: true },

                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    if (response.IsSuccess) {
                        $('#batchDays').append('<option value="0">All Batch Day</option>');
                        $.each(response.returnBatchDays, function (i, v) {
                            if (v.Value == 0)
                                $('#batchDays').append($('<option>').text(v.Text).attr('value', v.Value));
                            else
                                $('#batchDays').append($('<option>').text(v.Text).attr('value', v.Text));
                        });

                    } $.unblockUI();
                }, complete: function () {
                    $.unblockUI();
                },
                error: function (response) {
                    //alert("Batch Days Time load failed");
                    //console.log("Failed -----");
                    //console.log(response);
                    var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Batch Days Time load failed</div>';
                    $('.customMessage').append(errorMessage);
                }
            });
        }
    });

    //Batch Day Change Functionalities
    $(document).on('change', '#batchDays', function () {

        var organizationId = $("#OrganizationId").val();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var campusId = $("#campusId").val();
        var batchDays = $("#batchDays").val();
        var branchId = $("#branchId").val();

        $("#batchTime option").remove();
        $("#batchName option").remove();
        if (batchDays != "" && batchDays != null) {
            $('#bdmsg').text("");
        }

        if (organizationId != null && organizationId != "" && sessionId != "" && programId != "" && campusId != "" && campusId != null && batchDays != "" && batchDays != null) {
            $.ajax({
                type: "POST",
                //url: $("body").attr("data-project-root") + "Student/Payment/AjaxRequestForBatchTime",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchTime",
                cache: false,
                async: true,
                data: { organizationIds: organizationId, programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId, batchDays: batchDays, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    if (result.IsSuccess) {
                        $('#batchTime').append('<option value="0">All Batch Time</option>');
                        $.each(result.returnBatchTime, function (i, v) {
                            $('#batchTime').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        //$.each(result.batchNames, function (i, v) {
                        //    $('#batchName').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                    }
                    else {

                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    //Batch Time Change Functionalities
    $(document).on('change', '#batchTime', function () {

        var organizationId = $("#OrganizationId").val();
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var campusId = $("#campusId").val();
        var batchDays = $("#batchDays").val();
        var batchTime = $("#batchTime").val();
        var branchId = $("#branchId").val();
        var batchTimeText = [];
        $("#batchTime option:selected").each(function () {
            var $this = $(this);
            if ($this.length) {
                batchTimeText.push($this.text());
            }
        });
        // console.log(batchTimeText);
        if (batchTime != "" || batchTime != null) {
            $('#btmsg').text("");
        }
        $("#batchName option").remove();


        if (organizationId != null && organizationId != "" && sessionId != "" && programId != "" && campusId != "" && campusId != null && batchTime != "" && batchTime != null) {
            $.ajax({
                type: "POST",
                //url: $("body").attr("data-project-root") + "Student/Payment/AjaxRequestForBatch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatch",
                cache: false,
                async: true,
                data: { organizationIds: organizationId, programIds: programId, sessionIds: sessionId, campusIds: campusId, branchIds: branchId, batchDays: batchDays, batchTimes: batchTime, batchTimeText: batchTimeText, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    if (result.IsSuccess) {
                        $('#batchName').append('<option value="0">All Batch</option>');
                        $.each(result.returnBatch, function (i, v) {
                            $('#batchName').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    else {

                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    $(document).on('change', '#batchName', function () {
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#branchId").val();
        var campusId = $("#campusId").val();
        var batchDays = $("#batchDays").val();
        var batchTime = $("#batchTime").val();
        var batchName = $("#batchName").val();
        if (batchName != null || batchName != "") {
            $('#bnmsg').text("");
        }

    });

    $(document).on('change', '#course', function () {
        if ($("#course").val() != null || $("#course").val() != "") {
            $('#cmsg').text("");
        }
    });
    $(document).on('change', '#informationViewList', function () {
        if ($("#informationViewList").val() != null || $("#informationViewList").val() != "") {
            $('#infomsg').text("");
        }
    });


    $('#informationViewList').bootstrapDualListbox({
        nonselectedlistlabel: 'Non-selected',
        selectedlistlabel: 'Selected',
        preserveselectiononmove: 'moved',
        moveOnSelect: false,
        showFilterInputs: false,
        infoText: '',
    });




});
