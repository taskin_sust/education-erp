﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr.AttendanceSummaryServices
{
    public interface IAttendanceSummaryService : IBaseService
    {
        #region Operational Function

        void PostAttendanceReset(List<UserMenu> userMenu, DateTime dateFrom, DateTime dateTo, List<long> organizationId, List<long> branchId, List<long> campusIds,
            List<long> departmentIds, string pin);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        
        IList<AttendanceSummary> LoadTeamMemberDayOffAttendanceSummary(TeamMember teamMember, long id = 0);
        IList<AttendanceSummary> ListAttendanceSummary(TeamMember teamMember, DateTime startDate, DateTime endDate);
        List<AttendanceSummeryReportDto> LoadAttendanceSummeryReport(List<UserMenu> userMenu, string organization, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin, int start, int length, string orderBy, string orderDir);

        #endregion

        #region Others Function

        int GetAttendanceSummeryReportCount(List<UserMenu> userMenu, string organization, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin);
        
        #endregion

        #region Helper Function
        #endregion 
        
    }
    public class AttendanceSummaryService : BaseService, IAttendanceSummaryService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IAttendanceSummaryDao _attendanceSummaryDao;
        private readonly ITeamMemberDao _teamMemberDao;
        private readonly IShiftWeekendHistoryDao _shiftWeekendHistoryDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly IZoneSettingDao _zoneSettingDao;
        private readonly IHolidaySettingDao _holidaySettingDao;
        private readonly IAttendanceAdjustmentDao _attendanceAdjustmentDao;
        private readonly IAttendanceDeviceSynchronizationDao _attendanceDeviceSynchronizationDao;
        private readonly ITeamMemberDao _hrMemberDao;
        private readonly AttendanceSummaryServiceHelper _attendanceSummaryServiceHelper;
        public AttendanceSummaryService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _attendanceSummaryDao = new AttendanceSummaryDao(){Session = session};
            _teamMemberDao = new TeamMemberDao(){Session = session};
            _shiftWeekendHistoryDao = new ShiftWeekendHistoryDao(){Session = session};
            _organizationDao = new OrganizationDao(){Session = session};
            _zoneSettingDao = new ZoneSettingDao() { Session = session };
            _holidaySettingDao = new HolidaySettingDao() { Session = session };
            _attendanceAdjustmentDao = new AttendanceAdjustmentDao { Session = session };
            _attendanceDeviceSynchronizationDao = new AttendanceDeviceSynchronizationDao { Session = session };
            _hrMemberDao = new TeamMemberDao{Session = session};
            _attendanceSummaryServiceHelper = new AttendanceSummaryServiceHelper(session);

        }

        #endregion

        #region Operational Functions
        
        public void PostAttendanceReset(List<UserMenu> userMenu, DateTime dateFrom, DateTime dateTo, List<long> organizationIdList = null, List<long> branchIdList = null,
            List<long> campusIdList = null, List<long> departmentIdList = null, string pin = "")
        {
            try
            {
                int userPin = 0;
                if (!String.IsNullOrEmpty(pin) && Convert.ToInt16(pin) > 0)
                {
                    userPin = Convert.ToInt16(pin);
                }

                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");

                List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu,
                    (organizationIdList != null && !organizationIdList.Contains(SelectionType.SelelectAll))
                        ? organizationIdList
                        : null);
                List<long> authoBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationIdList, null,
                    (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll)) ? branchIdList : null);

                List<TeamMember> teamMemberList =
                    _hrMemberDao.LoadHrAuthorizedTeamMember(null, authoBranchIdList, campusIdList, departmentIdList,
                        null, (userPin != 0) ? _commonHelper.ConvertIdToList(userPin) : null).ToList();
                if (teamMemberList.Any())
                {
                    foreach (var teamMember in teamMemberList)
                    {
                        //message = "";
                        //message = teamMember.Pin.ToString();
                        for (DateTime i = dateFrom.Date; i <= dateTo.Date; i = i.AddDays(1))
                        {
                            bool stats =
                                _attendanceSummaryServiceHelper.UpdateAttendenceSummeryByPinAndDateTime(teamMember.Pin,
                                    i);
                        }
                    }
                }

                //return _attendanceSummaryDao.ListAttendanceSummary(teamMember);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        
        public IList<AttendanceSummary> LoadTeamMemberDayOffAttendanceSummary(TeamMember teamMember, long id = 0)
        {
            try
            {
                return _attendanceSummaryDao.LoadTeamMemberDayOffAttendanceSummary(teamMember, id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<AttendanceSummary> ListAttendanceSummary(TeamMember teamMember, DateTime startDate, DateTime endDate)
        {
            try
            {
                return _attendanceSummaryDao.ListAttendanceSummary(teamMember, startDate, endDate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetAttendanceSummeryReportCount(List<UserMenu> userMenu, string organization, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo,string pin)
        {
            try
            {
                List<long> organizationIdList = null;
                List<long> branchIdList = null;

                if (userMenu == null)
                    throw new InvalidDataException("Invalid user menu");

                if (!String.IsNullOrEmpty(organization))
                    organizationIdList = _commonHelper.ConvertIdToList(Convert.ToInt64(organization));
                if (!String.IsNullOrEmpty(branch))
                    branchIdList = _commonHelper.ConvertIdToList(Convert.ToInt64(branch));

                List<long> authorizedOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organizationIdList);
                List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authorizedOrganizationIdList,
                    null, branchIdList);

                if (String.IsNullOrEmpty(dateTo))
                    throw new InvalidDataException("Invalid dateTo.");


                return _attendanceSummaryDao.GetAttendanceSummeryReportCount(authorizedOrganizationIdList,
                    authorizedBranchIdList, department, branch, informationViewList, campus, dateFrom, dateTo, pin);
                //return _attendanceSummaryDao.GetAttendanceSummeryReportCount(authorizedBranchIdList, organization, department, branch, informationViewList, campus, dateFrom, dateTo,pin);

            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<AttendanceSummeryReportDto> LoadAttendanceSummeryReport(List<UserMenu> userMenu, string organization, string department, string branch, string[] informationViewList, string campus, string dateFrom, string dateTo, string pin, int start, int length, string orderBy, string orderDir)
        {
            try
            {
                List<long> organizationIdList = null;
                List<long> branchIdList = null;
                if (!String.IsNullOrEmpty(organization))
                    organizationIdList = _commonHelper.ConvertIdToList(Convert.ToInt64(organization));
                if (!String.IsNullOrEmpty(branch))
                    branchIdList = _commonHelper.ConvertIdToList(Convert.ToInt64(branch));

                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu.");

                if(string.IsNullOrEmpty(dateTo))
                    throw new InvalidDataException("DateTo can't empty.");

                List<long> authorizedOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organizationIdList);
                List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authorizedOrganizationIdList,
                    null, branchIdList);

                return _attendanceSummaryDao.LoadAttendanceSummeryReport(authorizedOrganizationIdList,
                    authorizedBranchIdList, department, branch, informationViewList, campus, dateFrom, dateTo, pin,
                    start, length, orderBy, orderDir);
                //List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu);
                //return _attendanceSummaryDao.LoadAttendanceSummeryReport(authorizedBranchIdList, organization, department, branch, informationViewList, campus, dateFrom, dateTo, pin,start,length,orderBy,orderDir);

            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        #endregion

        #region Helper function
        #endregion
        
    }
}
