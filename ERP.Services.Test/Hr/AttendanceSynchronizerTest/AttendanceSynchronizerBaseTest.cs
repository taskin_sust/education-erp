﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.AttendanceSynchronizerTest
{
    public class AttendanceSynchronizerBaseTest:TestBase,IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper CommonHelper;
        internal AttendanceSynchronizerService _attendanceSynchronizerService;
        internal CampusService _campusService;
        internal List<long> IdList = new List<long>();

        private ISession _session;
        internal AttendanceSynchronizerFactory _attendanceSynchronizerFactory;
        internal CampusRoomFactory _campusRoomFactory;
        internal CampusFactory _campusFactory;
        internal AttendanceDeviceFactory _attendanceDeviceFactory;
        internal OrganizationFactory _organizationFactory;
        internal BranchFactory _branchFactory; 
        

        internal readonly string _name = Guid.NewGuid().ToString().Substring(0, 5)+DateTime.Now.ToString("yyyyMMddHHmmss") + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal readonly string _synchronizerKey = Guid.NewGuid().ToString().Substring(0, 5)+DateTime.Now.ToString("yyyyMMddHHmmss") + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
 
        #endregion
       
        public AttendanceSynchronizerBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _attendanceSynchronizerFactory = new AttendanceSynchronizerFactory(null, _session);
            _attendanceSynchronizerService = new AttendanceSynchronizerService(_session);
            _campusService = new CampusService(_session);
            _campusRoomFactory = new CampusRoomFactory(null, _session);
            _campusFactory = new CampusFactory(null,_session);
            _attendanceDeviceFactory = new AttendanceDeviceFactory(null,_session);     
            _organizationFactory = new OrganizationFactory(null,_session);
            _branchFactory = new BranchFactory(null,_session);
        }
               
        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }


        public void Dispose()
        {
            _campusRoomFactory.CleanUp();
            _attendanceDeviceFactory.CleanUp();
            _attendanceSynchronizerFactory.Cleanup();
            _campusFactory.Cleanup();
            _branchFactory.Cleanup();
            _organizationFactory.Cleanup();
            base.Dispose();
        }
    }
}
