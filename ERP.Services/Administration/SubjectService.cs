﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface ISubjectService : IBaseService
    {
        #region Operational Function
        bool SubjectSaveOrUpdate(Subject subjectObj);
        bool Delete(long id);
        bool UpdateRank(Subject subjectOldObj, Subject subjectUpdateObj);
        #endregion

        #region Single Instances Loading Function
        Subject GetSubject(long id);
        Subject GetSubjectByRankNextOrPrevious(int rank, string action);
        #endregion

        #region List Loading Function
        IList<Subject> LoadSubject();
        IList<Subject> LoadSubject(int start, int length, string orderBy, string orderDir, string name, string shortName, string rank, string status);
        #endregion

        #region Others Function
        int GetMaximumRank(Subject subjectObj);
        int GetMinimumRank(Subject obj);
        int GetSubjectCount(string name, string shortName, string rank, string status);
        #endregion
    }
    public class SubjectService : BaseService, ISubjectService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ISubjectDao _subjectDao;
        private readonly IProgramSessionSubjectDao _programSessionSubjectDao;
        public SubjectService(ISession session)
        {
            Session = session;
            _subjectDao = new SubjectDao() { Session = Session };
            _programSessionSubjectDao = new ProgramSessionSubjectDao() { Session = Session };
        }
        #endregion

        #region Operational Function
        private void CheckEmptyField(Subject erpSubject)
        {
            if (erpSubject == null) throw new NullObjectException("subject can not be null");
            var validationResult = ValidationHelper.ValidateEntity<Subject, Subject.SubjectMetaData>(erpSubject);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }
        
        private void CheckBeforeSave(Subject subjectObj)
        {
            ////check duplicate name
            //CheckDuplicateName(erpSession);
            ////check duplicate code
            //CheckDuplicateShortName(erpSession);
            string fieldName = "";
            var checkDuplicateName = _subjectDao.CheckDuplicateSubject(out fieldName, subjectObj.Name, null, subjectObj.Id);
            if (checkDuplicateName)
            {
                if (fieldName == "Name")
                    throw new DuplicateEntryException("Duplicate name found");
            }
            var checkDuplicateCode = _subjectDao.CheckDuplicateSubject(out fieldName, null, subjectObj.ShortName, subjectObj.Id);
            if (checkDuplicateCode)
            {
                if (fieldName == "ShortName")
                    throw new DuplicateEntryException("Duplicate short name found");
            }
        }

        public bool SubjectSaveOrUpdate(Subject subjectObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    CheckEmptyField(subjectObj);
                    if (subjectObj.Id < 1)
                    {
                        //check before save
                        CheckBeforeSave(subjectObj);
                        subjectObj.Rank = _subjectDao.GetMaximumRank(subjectObj) + 1;
                        _subjectDao.Save(subjectObj);
                    }
                    else
                    {
                        var tempSubject = _subjectDao.LoadById(subjectObj.Id);
                        //check before update
                        CheckBeforeUpdate(subjectObj);
                        tempSubject.Name = subjectObj.Name;
                        tempSubject.ShortName = subjectObj.ShortName;
                        tempSubject.Status = subjectObj.Status;
                        _subjectDao.Update(tempSubject);
                    }
                    transaction.Commit();
                    return true;
                }
            }
            catch (DependencyException ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
            catch (NullObjectException ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
            catch (InvalidDataException ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
            catch (EmptyFieldException ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        private void CheckBeforeUpdate(Subject subjectObj)
        {
            ////check duplicate
            //if (_subjectDao.HasDuplicateByName(erpsubject.Name, erpsubject.Id))
            //    throw new DuplicateEntryException("Duplicate subject found");
            //if (_subjectDao.HasDuplicateByShortName(erpsubject.ShortName, erpsubject.Id))
            //    throw new DuplicateEntryException("Duplicate short name found");
            string fieldName = "";
            var checkDuplicateName = _subjectDao.CheckDuplicateSubject(out fieldName, subjectObj.Name, null, subjectObj.Id);
            if (checkDuplicateName)
            {
                if (fieldName == "Name")
                    throw new DuplicateEntryException("Duplicate subject name found");
            }
            var checkDuplicateCode = _subjectDao.CheckDuplicateSubject(out fieldName, null, subjectObj.ShortName, subjectObj.Id);
            if (checkDuplicateCode)
            {
                if (fieldName == "ShortName")
                    throw new DuplicateEntryException("Duplicate short name found");
            }
            if (subjectObj.Status==Subject.EntityStatus.Active)
            {
                long count = _programSessionSubjectDao.HasManySubjectObject(subjectObj.Id);
                if (count > 0)
                    throw new DependencyException("Subject is assigned to program and session."); 
            }
        }

        private void CheckBeforeDelete(Subject subjectObj)
        {
            //dependency check
            long count = _programSessionSubjectDao.HasManySubjectObject(subjectObj.Id);
            if (count > 0)
                throw new DependencyException("Subject is assigned to program and session.");
        }

        public bool Delete(long id)
        {
            ITransaction transaction = null;
            try
            {
                var erpsubjectObj = _subjectDao.LoadById(id);
                //check before delete
                CheckBeforeDelete(erpsubjectObj);
                using (transaction = Session.BeginTransaction())
                {
                    var tempSubject = _subjectDao.LoadById(id);
                    tempSubject.Status = Subject.EntityStatus.Delete;
                    _subjectDao.Update(tempSubject);
                    transaction.Commit();
                    return true;
                }
            }
            catch (DependencyException ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool UpdateRank(Subject subjectOldObj, Subject subjectUpdateObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    _subjectDao.Update(subjectOldObj);
                    _subjectDao.Update(subjectUpdateObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public Subject GetSubjectByRankNextOrPrevious(int rank, string action)
        {
            Subject subject;
            try
            {
                subject = _subjectDao.LoadByRankDirection(rank, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return subject;
        }
        public Subject GetSubject(long id)
        {
            Subject subject;
            try
            {
                subject = _subjectDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return subject;
        }
        #endregion

        #region List Loading Function
        public IList<Subject> LoadSubject(int start, int length, string orderBy, string orderDir, string name, string shortName, string rank, string status)
        {
            IList<Subject> subjects;
            try
            {
                subjects = _subjectDao.LoadSubject(start, length, orderBy, orderDir, name, shortName, rank, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return subjects;
        }
        public IList<Subject> LoadSubject()
        {
            IList<Subject> subjects;
            try
            {
                subjects = _subjectDao.LoadSubject();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return subjects;
        }
        #endregion

        #region Others Function
        public int GetMaximumRank(Subject sessionObj)
        {
            int maximumRank;
            try
            {
                maximumRank = _subjectDao.GetMaximumRank(sessionObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return maximumRank;
        }
        public int GetMinimumRank(Subject obj)
        {
            int minimumRank;
            try
            {

                minimumRank = _subjectDao.GetMinimumRank(obj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return minimumRank;
        }
        public int GetSubjectCount(string name, string shortName, string rank, string status)
        {
            int subjectCount;
            try
            {
                subjectCount = _subjectDao.GetSubjectCount(name, shortName, rank, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return subjectCount;
        }
        #endregion
    }
}
