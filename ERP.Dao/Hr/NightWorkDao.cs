﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;

namespace UdvashERP.Dao.Hr
{
    public interface INightWorkDao : IBaseDao<NightWork, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        NightWork GetNightWork(string teamMemberPin, DateTime nightWorkDate);
        NightWork GetTeamMemberLastIsPostNightWork(long? teamMemberId, int? pin = null);
        NightWork GetTeamMemberLastApprovedNightWork(long? teamMemberId = null, int? pin = null);

        #endregion

        #region List Loading Function

        IList<NightWorkDto> LoadNightWork(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdList, long? campusId, long? departmentId, int start, int length, string orderBy, string orderDir, bool isHistory);
        IList<NightWork> LoadNightWork(DateTime dateFrom, DateTime dateTo, long teamMemberId);

        #endregion

        #region Others Function

        int LoadNightWorkCount(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdList, long? campusId, long? departmentId, bool isHistory);
        bool CheckHasNightWorkOnADate(long teamMemberId, DateTime dateTime);

        #endregion

        #region Helper Function

        #endregion

    }
    public class NightWorkDao : BaseDao<NightWork, long>, INightWorkDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public NightWork GetNightWork(string teamMemberPin, DateTime nightWorkDate)
        {
            var criteria = Session.CreateCriteria<NightWork>().Add(Restrictions.Eq("Status", NightWork.EntityStatus.Active));
            criteria.CreateAlias("TeamMember", "hm");
            criteria.Add(Restrictions.Eq("hm.Pin", Convert.ToInt32(teamMemberPin)));
            criteria.Add(Restrictions.Eq("NightWorkDate", nightWorkDate.Date));
            //criteria.SetMaxResults(1);
            return criteria.UniqueResult<NightWork>();
        }

        public NightWork GetTeamMemberLastIsPostNightWork(long? teamMemberId, int? pin = null)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<NightWork>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", NightWork.EntityStatus.Delete))); //Add(Restrictions.Eq("Status", NightWork.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("IsPost", true));
            criteria.CreateAlias("TeamMember", "member");//.Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("NightWorkDate"));
            return criteria.List<NightWork>().Take(1).FirstOrDefault();
        }

        public NightWork GetTeamMemberLastApprovedNightWork(long? teamMemberId = null, int? pin = null)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<NightWork>();
            criteria.Add(Restrictions.Eq("Status", NightWork.EntityStatus.Active));
            criteria.Add(Restrictions.Not(Restrictions.IsNull("ApprovalType")));
            criteria.CreateAlias("TeamMember", "member");
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("NightWorkDate"));
            return criteria.List<NightWork>().Take(1).FirstOrDefault();
        }

        #endregion

        #region List Loading Function

        public IList<NightWorkDto> LoadNightWork(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdList, long? campusId, long? departmentId, int start, int length, string orderBy, string orderDir, bool isHistory)
        {
            var filterPagination = "";
            if (length > 0)
            {
                filterPagination = " where pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }
            string sqlQuery = GetNightWorkQuery(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdList, campusId, departmentId, isHistory);
            string query = @"DECLARE @rowsperpage INT 
		                    DECLARE @start INT 
		                    SET @start = " + (start + 1) + @" 
		                    SET @rowsperpage = " + length + @" 
                Select * From (SELECT row_number() OVER (ORDER BY A.Id asc) AS RowNum, A.Id,A.Pin,A.TeamMemberId,A.Name,A.Department,A.Designation,A.Organization,A.Branch,A.Campus,A.NightWorkDate,A.TimeFrom,A.TimeTo,A.ApprovalType,A.ModifyBy, A.LastModificationDate, A.HouseWork 
	                            FROM(
		                            " + sqlQuery + @"
	                            ) A ) as pagination 
                            " + filterPagination;

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<NightWorkDto>());
            iQuery.SetTimeout(5000);
            return iQuery.List<NightWorkDto>().ToList();
        }

        public IList<NightWork> LoadNightWork(DateTime dateFrom, DateTime dateTo, long teamMemberId)
        {
            ICriteria criteria = Session.CreateCriteria<NightWork>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", NightWork.EntityStatus.Delete)));
            criteria.Add(Restrictions.Not(Restrictions.Eq("IsPost", true)));
            criteria.CreateAlias("TeamMember", "member");
            criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            criteria.Add(Restrictions.Ge("NightWorkDate", dateFrom.Date));
            criteria.Add(Restrictions.Le("NightWorkDate", dateTo.Date));
            return criteria.List<NightWork>();
        }
        
        #endregion

        #region Others Function

        public int LoadNightWorkCount(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdList, long? campusId, long? departmentId, bool isHistory)
        {
            string sqlQuery = GetNightWorkQuery(dateFrom, dateTo, mentorTeamMemberIdList, pinList, organizationIdList, branchIdList, campusId, departmentId, isHistory);
            string query = "Select Count(*) as totalRow From (" + sqlQuery + ") AS A";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(5000);
            return iQuery.UniqueResult<int>();
        }

        public bool CheckHasNightWorkOnADate(long teamMemberId, DateTime dateTime)
        {
            int nightWorkCount = Session.QueryOver<NightWork>().Where(x => x.NightWorkDate.Value.Date == dateTime.Date && (x.ApprovalType == (int)HolidayWorkApprovalStatus.Half || x.ApprovalType == (int)HolidayWorkApprovalStatus.Full) && x.Status == NightWork.EntityStatus.Active && x.TeamMember.Id == teamMemberId).RowCount();

            if (nightWorkCount > 0)
                return true;
            return false;
        }

        #endregion

        #region Helper Function
        private string GetNightWorkQuery(DateTime dateFrom, DateTime dateTo, List<long> mentorTeamMemberIdList, string pinList, List<long> organizationIdList, List<long> branchIdList, long? campusId, long? departmentId, bool isHistory)
        {
            string query = "";
            string pinFiltering = "";
            string filtering = "";
            string mentorTeamMemberFiltering = "";
            //string effectiveDateFilter = " EffectiveDate<='" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + @"' ";
            string attendanceDateFilter = " AttendanceDate >= '" + dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "' AND  AttendanceDate <= '" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "' ";
            string nightWorkDateFilter = " hra.NightWorkDate  >= '" + dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "'  AND hra.NightWorkDate  <= '" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "' ";

            if (!string.IsNullOrEmpty(pinList))
            {
                pinFiltering = " and [Pin] IN (" + pinList + ")";
            }

            if (mentorTeamMemberIdList.Any())
            {
                mentorTeamMemberFiltering += " and Id IN (" + String.Join(", ", mentorTeamMemberIdList) + ")";
            }
            if (organizationIdList != null && organizationIdList.Any())
            {
                filtering += " and o.Id IN (" + String.Join(", ", organizationIdList) + ")";
            }
            if (branchIdList != null && branchIdList.Any())
            {
                filtering += " and br.Id IN (" + String.Join(", ", branchIdList) + ")";
            }
            if (campusId != null && campusId > 0)
            {
                filtering += " and c.Id = " + campusId;
            }
            if (departmentId != null && departmentId > 0)
            {
                filtering += " and dp.Id = " + departmentId;
            }

            if (isHistory)
            {
                query = @"Select hras.*
	                            ,ISNULL(hra.ApprovalType,0) AS ApprovalType
	                            ,hrm.Name
	                            ,hrm.Pin
	                            ,c.Name AS Campus
	                            ,br.Name AS Branch
	                            ,o.ShortName AS Organization
	                            ,dp.Name AS Department
	                            ,dg.Name AS Designation
                                ,hra.ModifyBy as ModifyBy
                                ,hra.ModificationDate as LastModificationDate
                                ,hra.InHouseWork as HouseWork
                            from [dbo].[HR_NightWork] AS hra	 
                            INNER Join (
	                            Select Id,TeamMemberId AS TeamMemberId,AttendanceDate AS NightWorkDate, [InTime] as TimeFrom,[OutTime] as TimeTo 
	                            from [dbo].[HR_AttendanceSummary]
	                            Where " + attendanceDateFilter + @" AND [TeamMemberId] IN (
		                            SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1  " + mentorTeamMemberFiltering + pinFiltering + @" 
	                            ) AND [Status] = " + AttendanceSummary.EntityStatus.Active
                          + @") AS hras ON hra.TeamMemberId = hras.TeamMemberId and hra.NightWorkDate = hras.NightWorkDate and " + nightWorkDateFilter + @"                          
                            Left Join [dbo].[HR_TeamMember] AS hrm ON hrm.Id = hras.TeamMemberId AND hrm.[Status] = " + TeamMember.EntityStatus.Active + @"

                            CROSS APPLY  (
	                            Select * from (
		                            Select 
		                            emh.CampusId as CampusId
		                            , emh.DesignationId as DesignationId
		                            , emh.DepartmentId as DepartmentId
		                            , emh.TeamMemberId as TeamMemberId
		                            , emh.EffectiveDate As EffectiveDate
		                            , emh.EmploymentStatus as EmploymentStatus
		                            , Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		                            from HR_EmploymentHistory as emh
		                            where emh.Status = " + EmploymentHistory.EntityStatus.Active + @"
		                            and  emh.EffectiveDate <=  hras.NightWorkDate 
		                            and emh.TeamMemberId = hras.TeamMemberId
	                            ) as employmentHistory 
	                            where 1=1
	                            and employmentHistory.r = 1
                            ) as currentPosition

                            Left Join [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId and c.Status =  " + Campus.EntityStatus.Active + @"
                            Left Join [dbo].[Branch] AS br ON br.Id = c.BranchId and br.Status =  " + Branch.EntityStatus.Active + @"
                            Left Join [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  " + Organization.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  " + Department.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  " + Designation.EntityStatus.Active + " Where 1=1 " + " AND hra.Status = " + NightWork.EntityStatus.Active + filtering;
            }
            else
            {
                query = @"Select hras.*
	                            ,ISNULL(hra.ApprovalType,0) AS ApprovalType
	                            ,hrm.Name
	                            ,hrm.Pin
	                            ,c.Name AS Campus
	                            ,br.Name AS Branch
	                            ,o.ShortName AS Organization
	                            ,dp.Name AS Department
	                            ,dg.Name AS Designation
                                ,hra.ModifyBy as ModifyBy
                                ,hra.ModificationDate as LastModificationDate
                                ,hra.InHouseWork as HouseWork
                            from (
	                            Select Id,TeamMemberId AS TeamMemberId,AttendanceDate AS NightWorkDate, [InTime] as TimeFrom,[OutTime] as TimeTo 
	                            from [dbo].[HR_AttendanceSummary]
	                            Where " + attendanceDateFilter + @" AND [TeamMemberId] IN (
		                            SELECT Id FROM [dbo].[HR_TeamMember] Where 1=1  " + mentorTeamMemberFiltering + pinFiltering + @" 
	                            ) AND [Status] = " + AttendanceSummary.EntityStatus.Active
                           + @") AS hras
                            Left Join [dbo].[HR_NightWork] AS hra ON hra.TeamMemberId = hras.TeamMemberId AND " + nightWorkDateFilter +" AND hra.Status = "+ NightWork.EntityStatus.Active +@" 
                            Left Join [dbo].[HR_TeamMember] AS hrm ON hrm.Id = hras.TeamMemberId AND hrm.[Status] = " + TeamMember.EntityStatus.Active + @"

                            CROSS APPLY  (
	                            Select * from (
		                            Select 
		                            emh.CampusId as CampusId
		                            , emh.DesignationId as DesignationId
		                            , emh.DepartmentId as DepartmentId
		                            , emh.TeamMemberId as TeamMemberId
		                            , emh.EffectiveDate As EffectiveDate
		                            , emh.EmploymentStatus as EmploymentStatus
		                            , Rank() Over(Partition by emh.TeamMemberId order by emh.EffectiveDate DESC, emh.Id DESC) AS r
		                            from HR_EmploymentHistory as emh
		                            where emh.Status = " + EmploymentHistory.EntityStatus.Active + @"
		                            and  emh.EffectiveDate <=  hras.NightWorkDate 
		                            and emh.TeamMemberId = hras.TeamMemberId
	                            ) as employmentHistory 
	                            where 1=1
	                            and employmentHistory.r = 1
                            ) as currentPosition

                            Left Join [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId and c.Status =  " + Campus.EntityStatus.Active + @"
                            Left Join [dbo].[Branch] AS br ON br.Id = c.BranchId and br.Status =  " + Branch.EntityStatus.Active + @"
                            Left Join [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =  " + Organization.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status =  " + Department.EntityStatus.Active + @"
                            Left Join [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status =  " + Designation.EntityStatus.Active + " Where 1=1 " + filtering;
            }

            return query;
        }

        #endregion

    }
}
