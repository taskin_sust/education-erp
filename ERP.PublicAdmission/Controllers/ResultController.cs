﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.PublicAdmission.Models;
using UdvashERP.PublicAdmission.ResultService;

namespace UdvashERP.PublicAdmission.Controllers
{
    public class ResultController : Controller
    {
        readonly ResultClient _client  = new ResultClient();
        readonly string _key = ConfigurationManager.AppSettings["apiKey"];
        string org = ConfigurationManager.AppSettings["orgBussinessId"]; 
        // GET: Result
        public ActionResult Index()
        {
            ViewBag.CourseList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.ExamList = new SelectList(string.Empty, "Value", "Text"); 
            return View();
        }
        [HttpPost]
        public ActionResult Index(ResultFormViewModel resultFormViewModel)
        {
            ViewBag.CourseList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.ExamList = new SelectList(string.Empty, "Value", "Text"); 
            return View();
        }
        [HttpPost]
        public ActionResult AjaxRequestForCourse(string prnNo) 
        {
            try
            {
                if (string.IsNullOrEmpty(prnNo))
                    return Json(new { returnList = "Invalid Program Roll", IsSuccess = false });
                prnNo = prnNo.Trim();
                var courseList = _client.LoadCourse(_key, org, prnNo);
                if (courseList.Any())
                {
                    IList<SelectListItem> course = new SelectList(courseList, "Id", "Name").ToList();
                    return Json(new { returnList = course, IsSuccess = true });
                }
                return Json(new { returnList = "Invalid Program Roll", IsSuccess = false });
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }
        }

       

        [HttpPost]
        public ActionResult AjaxRequestForExamInformation( string courseId, string prnNo) 
        {
            try
            {
                if (String.IsNullOrEmpty(prnNo))
                    return Json(new { returnList = "Invalid Program Roll", IsSuccess = false });
                if (String.IsNullOrEmpty(courseId))
                {
                    return Json(new { returnList = "Please select a Course", IsSuccess = false });
                }
                prnNo = prnNo.Trim();
                var examList = _client.LoadExams(_key, org, courseId, prnNo).OrderBy(x => x.Name).ToList();
                if (examList.Any())
                {
                    IList<SelectListItem> exams = new SelectList(examList, "Id", "Name").ToList();
                    return Json(new { returnList = exams, IsSuccess = true });
                }
                return Json(new { returnList = "No exam found", IsSuccess = false });
            }
            catch (Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }
            
        }

        public ActionResult AjaxRequestForReport(string prnNo, string courseId, string examId)
        {
            try
            {
                if (!String.IsNullOrEmpty(prnNo) && !String.IsNullOrEmpty(courseId) && !String.IsNullOrEmpty(examId))
                {
                    IList<UdvashERP.PublicAdmission.ResultService.IndivisualViewModel> ivmList = _client.GetResult(_key, org, courseId, prnNo, examId);                    
                    ViewBag.Result = ivmList;
                    return PartialView("_indivisualResult");
                }
                else{
                    return Json(new { returnList = "Please fill all fields", IsSuccess = false });
                }
            }
            catch(Exception ex)
            {
                return Json(new { returnList = WebHelper.CommonErrorMessage, IsSuccess = false });
            }
        }
    }
}