using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    public interface IMemberLoanCsrDao : IBaseDao<MemberLoanCsr, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        List<MemberLoanCsr> LoadMemberLoanCsr(long teamMemberId, DateTime date);

        #endregion

        #region Others Function
        #endregion
    }

    public class MemberLoanCsrDao : BaseDao<MemberLoanCsr, long>, IMemberLoanCsrDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public List<MemberLoanCsr> LoadMemberLoanCsr(long teamMemberId, DateTime date)
        {
            ICriteria criteria = Session.CreateCriteria<MemberLoanCsr>().Add(Restrictions.Eq("Status", MemberLoanCsr.EntityStatus.Active));
            criteria.CreateAlias("MemberLoanApplication", "application").Add(Restrictions.Eq("application.Status", MemberLoanApplication.EntityStatus.Active)).Add(Restrictions.Eq("application.LoanStatus", 1));
            criteria.CreateAlias("application.TeamMember", "member").Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active)).Add(Restrictions.Eq("member.Id", teamMemberId));

            criteria.Add(Restrictions.Le("EffectiveDate", date.Date));
            var disjunction = Restrictions.Disjunction(); // for OR statement 
            disjunction.Add(Restrictions.IsNull("ClosingDate") || Restrictions.Ge("ClosingDate", date.Date));
            criteria.Add(disjunction);
            criteria.AddOrder(Order.Desc("EffectiveDate")).AddOrder(Order.Desc("Amount"));

            criteria.SetTimeout(3000);
            return criteria.List<MemberLoanCsr>().ToList();
        }

        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion
    }
}
