﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface ITrainingInfoDao : IBaseDao<TrainingInfo, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
        IList<TrainingInfo> LoadAllByMemberId(long memberId);
        IList<TrainingInfo> LoadAllProfessionalQByMemberId(long memberId);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class TrainingInfoDao : BaseDao<TrainingInfo, long>, ITrainingInfoDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
        public IList<TrainingInfo> LoadAllByMemberId(long memberId)
        {
            return Session.QueryOver<TrainingInfo>().Where(x => x.TeamMember.Id == memberId)
                .Where(x=>x.IsTraining == true)
                .WhereNot(x => x.Status == (TrainingInfo.EntityStatus.Delete))
                .List<TrainingInfo>();
        }
        public IList<TrainingInfo> LoadAllProfessionalQByMemberId(long memberId)
        {
            return Session.QueryOver<TrainingInfo>().Where(x => x.TeamMember.Id == memberId)
                .Where(x=>x.IsTraining == false)
                .WhereNot(x => x.Status == (TrainingInfo.EntityStatus.Delete))
                .List<TrainingInfo>();
        }
        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion

        
    }

}
