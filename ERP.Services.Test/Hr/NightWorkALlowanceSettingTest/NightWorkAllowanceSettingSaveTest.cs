﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.NightWorkALlowanceSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "NightWorkAllowanceSettingService")]

    public class NightWorkAllowanceSettingSaveTest : NightWorkAllowanceSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Save_Successfully()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            Assert.True(_nightworkAllowanceSettingFactory.Object.Id > 0);
        }

        #endregion

        #region Validation Test

        [Fact]
        public void Should_Check_Empty_Department()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30)).Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01),
                new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 8, 0, 0), new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();
            _nightWorkService.Save(new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01).ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = DateTime.Now.AddMonths(-1).Month,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            allowanceSheetList[0].AllowanceSheetFirstDetails[0].TeamMemberDepartment = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_Check_Empty_Designation()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30)).Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01),
                new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 8, 0, 0), new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();
            _nightWorkService.Save(new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01).ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = DateTime.Now.AddMonths(-1).Month,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            allowanceSheetList[0].AllowanceSheetFirstDetails[0].TeamMemberDesignation = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_Check_Empty_Branch()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            //new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch }
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30)).Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01),
                new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 8, 0, 0), new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();
            _nightWorkService.Save(new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01).ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = DateTime.Now.AddMonths(-1).Month,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            allowanceSheetList[0].AllowanceSheetFirstDetails[0].TeamMemberBranch = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_Check_Empty_Campus()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30)).Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01),
                new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 8, 0, 0), new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();
            _nightWorkService.Save(new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01).ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = DateTime.Now.AddMonths(-1).Month,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            allowanceSheetList[0].AllowanceSheetFirstDetails[0].TeamMemberCampus = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_Check_Empty_NightWork()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(2016, 1, 1).Date,
                new DateTime(2016, 1, 1, 9, 0, 0), new DateTime(2016, 1, 1, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();
            _nightWorkService.Save(new DateTime(2016, 1, 1).Date.ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            allowanceSheetList[0].AllowanceSheetFirstDetails[30].NightWorkAllowanceSetting = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        //[Fact]
        //public void Should_Check_Invalid_Total_Amount()
        //{
        //    TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));
        //    Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
        //    List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, null);
        //    _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30)).Persist(menus);
        //    _attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01),
        //        new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 8, 0, 0), new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 18, 0, 0),
        //        "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();
        //    _nightWorkService.Save(new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01).ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
        //    AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel
        //    {
        //        BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
        //        CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
        //        DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
        //        Month = DateTime.Now.AddMonths(-1).Month,
        //        OrganizationId = org.Id,
        //        PinList = member.Pin.ToString(),
        //        TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
        //        Year = 2016
        //    };
        //    List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
        //    allowanceSheetList[0].AllowanceSheetFirstDetails[30].TotalDailyAllowanceAmount = 0;
        //    Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        //}


        #endregion

        #region Invalid Value Test

        [Fact]
        public void Should_Check_CalculationOn_For_LessThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 0;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_CalculationOn_For_GreaterThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 3;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_InHouseMultiplyingFactor_For_LessThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 0;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_InHouseMultiplyingFactor_For_GreaterThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 4;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }
        [Fact]
        public void Should_Check_OutHouseMultiplyingFactor_For_LessThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 0;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_OutHouseMultiplyingFactor_For_GreaterThan_Valid_Range()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightworkAllowanceSettingFactory.Object.CalculationOn = 4;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }


        #endregion

        #region Authorization Check

        [Fact]
        public void Should_Return_UnAuthorized_Menu_Message()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { organizationFactory.Create().Persist().Object }, null, null);
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30)).Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01),
                new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 8, 0, 0), new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();

            _nightWorkService.Save(new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01).ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());

            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = DateTime.Now.AddMonths(-1).Month,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_Invalid_Object()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            Assert.Throws<NullObjectException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, null));
        }

        [Fact]
        public void Should_Returm_Invalid_EmployeeData()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            _nightworkAllowanceSettingFactory.Object.MemberEmploymentStatuses = null;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_Permission()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));
            Assert.Throws<AuthenticationException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(null, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_DateTime()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));

            _nightworkAllowanceSettingFactory.Object.EffectiveDate = DateTime.Now.AddDays(5);
            _nightworkAllowanceSettingFactory.Object.ClosingDate = DateTime.Now;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_Invalid_DateTime_FutureDateRange()
        {
            organizationFactory.Create().Persist();
            _nightworkAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object));

            _nightworkAllowanceSettingFactory.Object.EffectiveDate = DateTime.Now.AddDays(5);
            _nightworkAllowanceSettingFactory.Object.ClosingDate = DateTime.Now.AddDays(10);
            _nightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object);
            Assert.True(_nightworkAllowanceSettingFactory.Object.Id > 0);
            //Assert.Throws<InvalidDataException>(() => _NightWorkAllowanceSettingService.SaveOrUpdate(menu, _nightworkAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_IsPost_Condition_ForSave_And_Return_MessageException_Sheet_GeneratedFor_This_Month()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30)).Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01),
                new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 8, 0, 0), new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();

            _nightWorkService.Save(new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01).ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());

            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel
        {
            BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
            CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
            DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
            Month = DateTime.Now.AddMonths(-1).Month,
            OrganizationId = org.Id,
            PinList = member.Pin.ToString(),
            TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
            Year = 2016
        };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList);
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2016, 12, 30));
            Assert.Throws<MessageException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menus, _nightworkAllowanceSettingFactory.SingleObjectList[1]));
        }

        [Fact]
        public void Should_Check_Duplicate_Entry_For_Save_And_Return_InvalidDataException_BecauseOf_Invalid_ClosingDate()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, null);
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));
            _nightworkAllowanceSettingFactory.Object.ClosingDate = null;
            _nightworkAllowanceSettingFactory.Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01),
                new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 8, 0, 0), new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();

            _nightWorkService.Save(new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01).ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2016, 12, 30));
            _nightworkAllowanceSettingFactory.SingleObjectList[1].ClosingDate = null;
            _nightworkAllowanceSettingFactory.SingleObjectList[1].Name = _nightworkAllowanceSettingFactory.SingleObjectList[0].Name;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menus, _nightworkAllowanceSettingFactory.SingleObjectList[1]));
        }

        [Fact]
        public void Should_Check_Duplicate_Entry_For_Save_And_Return_InvalidDataException_BacauseOf_Already_Declared_One_For_EqualDate()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, null);
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));

            _nightworkAllowanceSettingFactory.Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01),
                new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 8, 0, 0), new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();

            _nightWorkService.Save(new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01).ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2016, 12, 30));
            _nightworkAllowanceSettingFactory.SingleObjectList[1].ClosingDate = new DateTime(2017, 12, 30);
            _nightworkAllowanceSettingFactory.SingleObjectList[1].Name = _nightworkAllowanceSettingFactory.SingleObjectList[0].Name;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menus, _nightworkAllowanceSettingFactory.SingleObjectList[1]));

        }

        [Fact]
        public void Should_Check_Duplicate_Entry_For_Save_And_Return_InvalidDataException_BacauseOf_Already_Declared_One_For_NotEqualDate()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, null);
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30));

            _nightworkAllowanceSettingFactory.Persist(menus);
            attendanceAdjustmentFactory.CreateWith("Bugatti", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01),
                new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 8, 0, 0), new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01, 18, 0, 0),
                "Traffic Jam", 1).WithTeamMember(teamMember: member).Persist();

            _nightWorkService.Save(new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 01).ToString(), member.Pin.ToString(), member.Pin.ToString(), "", "", "", member.Pin.ToString());
            _nightworkAllowanceSettingFactory.Create().WithOrganization(org).WithDatetime(new DateTime(2016, 01, 01), new DateTime(2016, 12, 30));
            _nightworkAllowanceSettingFactory.SingleObjectList[1].ClosingDate = new DateTime(2017, 12, 28);
            _nightworkAllowanceSettingFactory.SingleObjectList[1].Name = _nightworkAllowanceSettingFactory.SingleObjectList[0].Name;
            Assert.Throws<InvalidDataException>(() => _nightWorkAllowanceSettingService.SaveOrUpdate(menus, _nightworkAllowanceSettingFactory.SingleObjectList[1]));

        }


        #endregion
    }
}
