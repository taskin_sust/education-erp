﻿using System;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BankTest
{
    interface IBankRankTest
    {
        #region Basic Test

        [Fact]
        void Get_Maximum_Rank_Not_Null_Check();
        void Get_Minimum_Rank_Not_Null_Check();
        #endregion

        #region Business Logic Test
        
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Bank")]
    public class BankRankTest : BankBaseTest, IBankRankTest
    {
        #region Basic Test

        [Fact]
        public void Get_Maximum_Rank_Not_Null_Check()
        {
            BankFactory bankFactory =
                    _bankFactory.Create()
                    .Persist();
            int maxRank = _bankService.GetMaximumRank(bankFactory.Object);
            Assert.True(maxRank>0);

        }

        [Fact]
        public void Get_Minimum_Rank_Not_Null_Check()
        {
            BankFactory bankFactory =
                    _bankFactory.Create()
                    .Persist();
            int minRank = _bankService.GetMinimumRank(bankFactory.Object);
            Assert.True(minRank > 0);

        }
        #endregion
        
        #region Business Logic Test
        
        #endregion
    }
}
