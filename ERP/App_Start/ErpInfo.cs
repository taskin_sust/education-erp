﻿using UdvashERP.App_code;

namespace UdvashERP
{
    public static class ErpInfo
    {
        public static string ErpName
        {
            get { return AppConfigHelper.GetString("ErpName"); }
        }

        public static string ErpVersion
        {
            get { return AppConfigHelper.GetString("ErpVersion"); }
        }

        public static string OrganizationNameShort
        {
            get { return AppConfigHelper.GetString("OrganizationNameShort"); }
        }

        public static string OrganizationNameFull
        {
            get { return AppConfigHelper.GetString("OrganizationNameFull"); }
        }

        public static string OrganizationWebsite
        {
            get { return AppConfigHelper.GetString("OrganizationWebsite"); }
        }

        public static string OrganizationEmail
        {
            get { return AppConfigHelper.GetString("OrganizationEmail"); }
        }

        public static string OrganizationContactNo
        {
            get { return AppConfigHelper.GetString("OrganizationContactNo"); }
        }

        public static string LogoImageUrl
        {
            get { return AppConfigHelper.GetString("LogoImageUrl"); }
        }

        public static string ReportImageUrl
        {
            get { return AppConfigHelper.GetString("ReportImageUrl"); }
        }

        public static string IdFooterImageUrl
        {
            get { return AppConfigHelper.GetString("IdFooterImageUrl"); }
        }
    }
}