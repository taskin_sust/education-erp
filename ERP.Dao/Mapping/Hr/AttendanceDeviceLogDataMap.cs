﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Hr
{
    public class AttendanceDeviceLogDataMap : BaseClassMap<BusinessModel.Entity.Hr.AttendanceDeviceLogData, long>
    {
        public AttendanceDeviceLogDataMap()
        {
            Table("HR_AttendanceDeviceLogData");
            LazyLoad();

            Map(x => x.DeviceId).Column("DeviceId");
            Map(x => x.EnrollNo).Column("EnrollNo");
            Map(x => x.PunchType).Column("PunchType");
            Map(x => x.PunchTime).Column("PunchTime");

        }
    }
}
