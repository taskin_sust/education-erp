﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Dto.Students;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    /// <summary>
    /// <author>Habib</author>
    /// <description>Conver ICriteria to DetachedCriteria</description>
    /// </summary>
    public class ConvertedDetachedCriteria : DetachedCriteria
    {
        public ConvertedDetachedCriteria(ICriteria criteria)
            : base((CriteriaImpl)criteria, criteria)
        {
            var impl = (CriteriaImpl)criteria;
            impl.Session = null;
        }
    }

    public interface IStudentSurveyAnswerDao : IBaseDao<StudentSurveyAnswer, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        StudentSurveyAnswer LoadStudentAnswerByQuestionIdStudentProgramId(long questionId, long studentProgramId);

        #endregion

        #region List Loading Function

        IEnumerable<StudentSurveyAnswer> LoadStudentSurveyAnswersByCriteria(long sessionId, long[] branchId, long[] campusId,string[] batchDays,string[] batchTime, long[] batchId, long[] courseId, string[] answerName, SurveyQuestion question, List<long> authorizedProgramLists, List<long> authorizedBranchLists);
        IList<StudentMakeSurveyDto> LoadAuthorizedStudentsMakeSurveyDto(int start, int length, string orderBy, string orderDir, List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusIds, string[] batchDays, string[] batchTime, long[] batchId, long[] courseIds, int surveyStatus, string programRoll, string name);
        IList<StudentWiseSurveyReportDto> LoadAuthorizedStudentWiseSurveyReportDto(int start, int length, string orderBy, string orderDir, List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long[] questionIds, string[] answerNames, string programRoll, string name, string mobile);
        IList<StudentWiseSurveyQuestionAnswerDto> LoadStudentWiseSurveyQuestionAnswerDto(List<long> studentProgramIdList, List<long> surveyQuestionIdList);

        #endregion

        #region Others Function

        int GetAuthorizedStudentsCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, int surveyStatus, string programRoll, string name);
        int GetAuthorizedStudentWiseSurveyReportCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long[] questionIds, string[] answerNames, string programRoll, string nickName, string mobile);
        
        #endregion

    }


    public class StudentSurveyAnswerDao : BaseDao<StudentSurveyAnswer, long>, IStudentSurveyAnswerDao
    {	
        #region Propertise & Object Initialization

        #endregion
	
	    #region Operational Function
       
        #endregion

        #region Single Instances Loading Function
       
        public StudentSurveyAnswer LoadStudentAnswerByQuestionIdStudentProgramId(long questionId, long studentProgramId)
        {
            return Session.QueryOver<StudentSurveyAnswer>().Where(x => x.SurveyQuestion.Id == questionId && x.StudentProgram.Id ==  studentProgramId).SingleOrDefault<StudentSurveyAnswer>();
        }

        #endregion

        #region List Loading Function
        
        public IEnumerable<StudentSurveyAnswer> LoadStudentSurveyAnswersByCriteria(long sessionId, long[] branchId, long[] campusId, string[] batchDays,string[] batchTime, long[] batchId, long[] courseId, string[] answerName, SurveyQuestion question,
            List<long> authorizedProgramLists, List<long> authorizedBranchLists)
        {
            var surveyAnswer = question.StudentSurveyAnswers.Where(x => x.Status == StudentSurveyAnswer.EntityStatus.Active);
            if (authorizedProgramLists != null)
            {
                surveyAnswer = surveyAnswer.Where(
                    x => x.StudentProgram.Program.Id.In(authorizedProgramLists.ToArray()));
            }
            if (authorizedBranchLists != null)
            {
                surveyAnswer = surveyAnswer.Where(
                    x => x.StudentProgram.Batch.Branch.Id.In(authorizedBranchLists.ToArray()));
            }
            surveyAnswer = surveyAnswer.Where(x => x.StudentProgram.Batch.Session.Id == sessionId);

            if (!(Array.Exists(branchId, item => item == 0)))
            {
                surveyAnswer = surveyAnswer.Where(x => x.StudentProgram.Batch.Branch.Id.In(branchId));
            }
            if (!(Array.Exists(campusId, item => item == 0)))
            {
                surveyAnswer = surveyAnswer.Where(x => x.StudentProgram.Batch.Campus.Id.In(campusId));
            }

            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) &&
                !(Array.Exists(batchDays, item => item == "")))
            {
                surveyAnswer = surveyAnswer.Where(x => x.StudentProgram.Batch.Days.In(batchDays));
            }
            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                surveyAnswer = surveyAnswer.Where(x => x.StudentProgram.Batch.FormatedBatchTime.In(batchTime));
            }

            if (!(Array.Exists(batchId, item => item == 0)))
            {
                surveyAnswer = surveyAnswer.Where(x => x.StudentProgram.Batch.Id.In(batchId));
            }

            if (!(Array.Exists(courseId, item => item == 0)))
            {
                CourseSubject courseSubject = null;
                var studentProgramsIdsInCourseDetails =
                    Session.QueryOver<StudentCourseDetail>().JoinAlias(x => x.CourseSubject, () => courseSubject)
                        .Where(x => courseSubject.Course.Id.IsIn(courseId))
                        .Select(x => x.StudentProgram.Id).List<long>().ToArray();

                surveyAnswer = surveyAnswer.Where(x => x.StudentProgram.Id.In(studentProgramsIdsInCourseDetails));
            }
            if (!(Array.Exists(answerName, item => item == "0")))
            {
                surveyAnswer = surveyAnswer.Where(x => x.SurveyQuestionAnswer.Answer.In(answerName));
            }
            return surveyAnswer;
        }

        public IList<StudentMakeSurveyDto> LoadAuthorizedStudentsMakeSurveyDto(int start, int length, string orderBy, string orderDir, List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, int surveyStatus, string programRoll, string name)
        {
            string criteriaQuery = GetAuthorizedStudentCriteria(authorizedProgramLists, authorizedBranchLists, sessionId, campusIds, batchDays, batchTimes, batchIds, courseIds, surveyStatus, programRoll, name);

            if (length > 0)
            {
                criteriaQuery += " Order By sp.PrnNo ASC  OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }
            IQuery iQuery = Session.CreateSQLQuery(criteriaQuery);
            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentMakeSurveyDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<StudentMakeSurveyDto>().ToList();
            return list;

        }

        public IList<StudentWiseSurveyReportDto> LoadAuthorizedStudentWiseSurveyReportDto(int start, int length, string orderBy, string orderDir, List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long[] questionIds, string[] answerNames, string programRoll,
            string name, string mobile)
        {
            string criteriaQuery = GetAuthorizedStudentCriteria(authorizedProgramLists, authorizedBranchLists, sessionId, campusIds, batchDays, batchTimes, batchIds, courseIds, null, programRoll, name, mobile, questionIds, answerNames);

            if (length > 0)
            {
                criteriaQuery += " Order By sp.PrnNo ASC  OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }
            IQuery iQuery = Session.CreateSQLQuery(criteriaQuery);
            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentWiseSurveyReportDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<StudentWiseSurveyReportDto>().ToList();
            return list;
        }

        public IList<StudentWiseSurveyQuestionAnswerDto> LoadStudentWiseSurveyQuestionAnswerDto(List<long> studentProgramIdList, List<long> surveyQuestionIdList)
        {
            string studentProgramIdListFilter = " and ssa.StudentProgramId in (" + string.Join(",", studentProgramIdList) + @") ";
            string surveyQuestionIdListFilter = " and ssa.SurveyQuestionId in (" + string.Join(",", surveyQuestionIdList) + @") ";

            string query = @"select 
	                        ssa.StudentProgramId
	                        , ssa.SurveyQuestionId
	                        , ssa.SurveyQuestionAnswerId
	                        , ssa.Advice
	                        , ssa.Complain
	                        , sqa.Answer as Answer
	                         from StudentSurveyAnswer ssa
	                        inner join SurveyQuestionAnswer as sqa on sqa.Id = ssa.SurveyQuestionAnswerId and sqa.Status = " + SurveyQuestionAnswer.EntityStatus.Active + @"
	                        where 
                        ssa.Status = " + StudentSurveyAnswer.EntityStatus.Active + studentProgramIdListFilter + surveyQuestionIdListFilter;

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentWiseSurveyQuestionAnswerDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<StudentWiseSurveyQuestionAnswerDto>().ToList();
            return list;
        }

        #endregion

        #region Others Function       

        public int GetAuthorizedStudentsCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, int surveyStatus, string programRoll, string name)
        {
            string  criteriaQuery = GetAuthorizedStudentCriteria(authorizedProgramLists, authorizedBranchLists, sessionId, campusIds, batchDays, batchTimes, batchIds, courseIds , surveyStatus, programRoll, name);

            string query = @"Select count(*) from ( " + criteriaQuery + " ) as a ";

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2000);
            int count = iQuery.UniqueResult<int>();
            return count;
        }

        public int GetAuthorizedStudentWiseSurveyReportCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long[] questionIds, string[] answerNames, string programRoll, string nickName, string mobile)
        {
            string criteriaQuery = GetAuthorizedStudentCriteria(authorizedProgramLists, authorizedBranchLists, sessionId, campusIds, batchDays, batchTimes, batchIds, courseIds, null, programRoll, nickName, mobile, questionIds, answerNames);

            string query = @"Select count(*) from ( " + criteriaQuery + " ) as a ";

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2000);
            int count = iQuery.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function
        
        private string GetAuthorizedStudentCriteria(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, int? surveyStatus, string programRoll, string name, string mobile = "", long[] questionIds = null, string[] answerNames = null)
        {
            string studentSurveyOuterApply = "";
            string studentCourseDetailsOuterApply = "";
            string studentSurveyOuterApplyFilter = "";
            string studentCourseDetailsOuterApplyFilter = "";
            string programRollFilter = "";
            string nameFilter = "";
            string mobileFilter = "";

            string studetnQuestionServeyQuestionCrossApply = "";
            string studetnQuestionServeyQuestionFilter = "";
            string studetnQuestionServeyQuestionAnswerFilter = "";
            string studetnQuestionServeyQuestionInnerJoinFilter = "";

            if (surveyStatus != null && surveyStatus != SelectionType.SelelectAll)
            {
                studentSurveyOuterApply = @" left join 
                                            ( select stSuQ.StudentProgramId, count(stSuQ.id) as totalSurvey from StudentSurveyAnswer as stSuQ  where stSuQ.Status = " + StudentSurveyAnswer.EntityStatus.Active + @" group by stSuQ.StudentProgramId 
                                            ) as ssa on ssa.StudentProgramId = sp.Id ";
                studentSurveyOuterApplyFilter = surveyStatus == SelectionType.Complete ? @" and ssa.totalSurvey > 0 " : @" and (ssa.totalSurvey = 0 OR ssa.totalSurvey IS NULL) ";
            }

            if (!(Array.Exists(courseIds, item => item == 0)))
            {
                studentCourseDetailsOuterApply = @" inner join StudentCourseDetails as scd on scd.StudentProgramId = sp.Id and scd.Status =  " + StudentCourseDetail.EntityStatus.Active + @" ";
                studentCourseDetailsOuterApplyFilter = " and scd.courseId in (" + string.Join(",", courseIds) + @") ";
            }

            if (questionIds != null && !(Array.Exists(questionIds, item => item == 0)))
            {
                studetnQuestionServeyQuestionFilter = " and ssa.SurveyQuestionId in(" + string.Join(",", questionIds.ToArray()) + ") ";
            }

            if (answerNames != null && !(Array.Exists(answerNames, item => item.ToString() == "0")))
            {
                studetnQuestionServeyQuestionAnswerFilter = " and sqa.Answer in(N'" + string.Join("',N'", answerNames.ToArray()) + "') ";
            }

            if (questionIds != null && answerNames != null)
            {
                studetnQuestionServeyQuestionCrossApply = @" INNER JOIN (
	                            select StudentProgramId
	                            from StudentSurveyAnswer as ssa
                                inner join SurveyQuestionAnswer as sqa on sqa.Id = ssa.SurveyQuestionAnswerId and sqa.Status = " + SurveyQuestionAnswer.EntityStatus.Active + @" 
	                            where ssa.Status = " + SurveyQuestion.EntityStatus.Active + studetnQuestionServeyQuestionFilter + studetnQuestionServeyQuestionAnswerFilter + @" 
                             group by StudentProgramId
                            ) ssr ON ssr.StudentProgramId = sp.Id";
            }


            if (!String.IsNullOrEmpty(programRoll))
            {
                programRollFilter = " AND sp.PrnNo like '%" + programRoll.Trim() + @"%'";
            }
            if (!String.IsNullOrEmpty(name))
            {
                nameFilter = " AND ( st.NickName like '%" + name.Trim() + @"%' OR  st.FullName like '%" + name.Trim() + @"%' ) ";
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                mobileFilter = " AND st.Mobile like '%" + mobile.Trim() + @"%'";
            }

            string query = @"SELECT distinct
                                sp.Id
                                , sp.PrnNo
                                -- , sp.VersionOfStudy
                                , sp.StudentId
                                , sp.InstituteId
                                , b.Name as Batch
                                , b.Days as BatchDays
                                , CONCAT(FORMAT(b.StartTime, 'hh:mm tt '),'To', FORMAT(b.EndTime, ' hh:mm tt')) as BatchTime
                                , br.Name as Branch
                                , c.Name as Campus
                                , st.NickName
                                , st.FullName
                                , st.Mobile
                                , st.GuardiansMobile1 
                                , st.GuardiansMobile2 
                                , st.Email 
                                , st.FatherName
                            FROM StudentProgram sp
                            LEFT join Program p on p.Id = sp.ProgramId  and p.Status = " + Program.EntityStatus.Active + @"
                            LEFT join Batch b on b.Id = sp.BatchId  and b.Status = " + Batch.EntityStatus.Active + @"
                            LEFT join Student st on st.Id = sp.StudentId and st.Status = " + Student.EntityStatus.Active + @" 
                            LEFT join Branch br on br.Id = b.BranchId  and br.Status = " + Branch.EntityStatus.Active + @"
                            LEFT join [Session] s on s.Id = b.SessionId  and s.Status = " + BusinessModel.Entity.Administration.Session.EntityStatus.Active + @"
                            LEFT join Campus c on c.Id = b.CampusId and c.Status = " + Campus.EntityStatus.Active + @"  ";

            string whereClause = @"
                            Where 
                            sp.Status = " + StudentProgram.EntityStatus.Active + @" ";
            whereClause += " and s.Id = " + sessionId ;

            if (authorizedProgramLists != null && authorizedProgramLists.Any())
            {
                whereClause += " and p.Id in(" + string.Join(",", authorizedProgramLists.ToArray()) + ") ";
            }

            if (authorizedBranchLists != null && authorizedBranchLists.Any())
            {
                whereClause += " and br.Id in(" + string.Join(",", authorizedBranchLists.ToArray()) + ") ";
            }

            if (!(Array.Exists(campusIds, item => item == 0)))
            {
                whereClause += " and c.Id in(" + string.Join(",", campusIds) + ") ";
            }

            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {
                whereClause += " and b.Days in ('" + string.Join("','", batchDays) + "')";
            }

            if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTimes)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += " (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "') ";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "') ";
                    }
                    iteration++;
                }
                whereClause += " and (" + btQ + ")";
            }

            if (!(Array.Exists(batchIds, item => item == 0)))
            {
                whereClause += " and b.Id in (" + string.Join(",", batchIds) + ")";
            }
            query += studentCourseDetailsOuterApply + studentSurveyOuterApply + studetnQuestionServeyQuestionCrossApply + whereClause + studentSurveyOuterApplyFilter + programRollFilter + nameFilter + studentCourseDetailsOuterApplyFilter + studetnQuestionServeyQuestionInnerJoinFilter + mobileFilter + "  ";

            return query;
        }
        
        #endregion

        
    }
}
