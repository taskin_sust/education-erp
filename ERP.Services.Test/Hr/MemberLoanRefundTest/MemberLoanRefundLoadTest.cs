﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Helper;
using Xunit;

namespace UdvashERP.Services.Test.Hr.MemberLoanRefundTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "MemberLoanRefund")]
    public class MemberLoanRefundLoadTest : MemberLoanRefundBaseTest
    {
        [Fact]
        public void Load_Should_Be_Successfull()
        {
            #region prerequisit

            var teamMember = PersistTeamMember(2);
            var organization = teamMember.EmploymentHistory.Select(x => x.Campus.Branch.Organization).SingleOrDefault();
            var campus = teamMember.EmploymentHistory.Select(x => x.Campus).SingleOrDefault();
            var dept = teamMember.EmploymentHistory.Select(x => x.Department).FirstOrDefault();

            _salaryHistoryFactory.Create().WithTeamMember(teamMember).WithOrganization(organization).WithCampus(campus).WithDepartment(dept).Persist();

            #endregion

            #region loan application

            var memberLoanApplication = _memberLoanApplicationFactory.Create();
            memberLoanApplication.Object.TeamMember = teamMember;
            const decimal approvedAmount = 8000;
            const decimal monthlyRefund = 1000;
            _memberLoanFactory.CreateWithLoanApprovedAmount(approvedAmount, monthlyRefund).WithMemberLoanApplication(memberLoanApplication.Object);
            _memberLoanService.SaveOrUpdate(new List<MemberLoan>() { _memberLoanFactory.Object });

            #endregion

            #region search member

            var userMenu = BuildUserMenu(campus.Branch.Organization, (Program)null, campus.Branch);
            List<long> authOrganizationIds = AuthHelper.LoadOrganizationIdList(userMenu);
            List<long> authBranchIds = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIds);

            var authorizeTeamMember = _teamMemberService.LoadHrAuthorizedTeamMember(userMenu, null, authOrganizationIds, authBranchIds, null, null, null, commonHelper.ConvertIdToList(teamMember.Pin), false);

            #region salary info

            var organizationSalary = _teamMemberService.GetTeamMemberSalaryOrganization(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);
            var branchSalary = _teamMemberService.GetTeamMemberSalaryBranch(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);
            var campusSalary = _teamMemberService.GetTeamMemberSalaryCampus(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);
            var departmentSalary = _teamMemberService.GetTeamMemberSalaryDepartment(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);

            #endregion

            var memberLoanRefund = new MemberLoanRefundViewModel
            {
                TeamMemberId = authorizeTeamMember[0].Id,
                MemberPin = authorizeTeamMember[0].Pin,
                MemberName = authorizeTeamMember[0].Name,
                Organization = organizationSalary,
                Branch = branchSalary,
                Campus = campusSalary,
                Department = departmentSalary
            };
        


            #endregion

            #region Refund
        

            _memberLoanRefundFactory.Create().WithTeamMember(authorizeTeamMember[0]).WithRefundAmount(2000).WithRemark("Refund amount check")
                .WithSalaryBranch(branchSalary).WithSalaryCampus(campusSalary).WithSalaryOrganization(organizationSalary);
            var userMenu2 = BuildUserMenu(organizationSalary, (Program)null, branchSalary);
            _memberLoanRefundService.SaveOrUpdate(userMenu2, _memberLoanRefundFactory.Object);
            #endregion

            var previousLoan = _memberLoanRefundService.GetLoanBalance(authorizeTeamMember[0].Id);
            memberLoanRefund.LoanBalance = previousLoan.LoanBalance;

            Assert.True(memberLoanRefund.LoanBalance==6000);

        }
    }
}
