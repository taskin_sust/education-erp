﻿$(document).ready(function () {

    //Load Branch By Organization, then in success change Program & Session List
    $("#OrganizationId").change(function () {
        var organizationCtrl = $("#OrganizationId");
        var branchCtrl = $("#BranchId");

        branchCtrl.empty();
        branchCtrl.append("<option>Select Branch</option>");

        $("#RequisitionItemList tr").each(function () {
            var itemCtrl = $(this).find(".itemDetailsItemName");
            itemCtrl.empty();
            itemCtrl.append("<option>Select Item</option>");

            var programSessionCtrl = $(this).find(".itemDetailsProgramSession");
            programSessionCtrl.empty();
            programSessionCtrl.append($('<option>').text("Select Program & Session").attr('value', ""));
            $(this).find(".itemDetailsItemGroup").val($(".itemDetailsItemGroup option:first").val());

        });

        if (organizationCtrl.val() > 0 || organizationCtrl.val() != "") {
            $.ajax({
                type: "POST",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                data: { organizationIds: organizationCtrl.val(), isAuthorized: true, isProgramBranSession: false, isCorporate: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    var data = result.returnBranchList;

                    if (data.length > 0) {
                        $.each(data, function (index, r) {
                            branchCtrl.append('<option value="' + r.Value + '">' + r.Text + '</option>');
                        });
                    } else {
                        branchCtrl.empty();
                        branchCtrl.append("<option>Noting Found</option>");
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (rt) {
                    $.fn.customMessage({
                        displayMessage: "Unable to load Organization's Branch(s).",
                        displayMessageType: "e"
                    });
                }
            });
        }
    });

    //Load Item Name By ItemGroup
    $(document).on("change", ".itemDetailsItemGroup", function (event) {

        var organizationCtrl = $("#OrganizationId");
        var tr = $(this).closest(".requisitionSlipItemDetails");
        var itemGroupCtrl = tr.find(".itemDetailsItemGroup");

        /*
        var itemGroupCtrlValidatationCtrl = tr.find(".itemGroupValidationCls");
        console.log(itemGroupCtrlValidatationCtrl);
        if (itemGroupCtrlValidatationCtrl != undefined) {
            if (itemGroupCtrlValidatationCtrl.classList.contains("field-validation-error")) {
                console.log("Exists");
            }
        }*/

        var itemCtrl = tr.find(".itemDetailsItemName");
        itemCtrl.empty();
        itemCtrl.append("<option>Select Item</option>");

        var programSessionCtrl = tr.find(".itemDetailsProgramSession");
        programSessionCtrl.empty();
        programSessionCtrl.append("<option>Select Program & Session</option>");

        if (organizationCtrl.val() > 0 && itemGroupCtrl.val() > 0) {

            $.ajax({
                type: "POST",
                url: "/UInventory/Requisition/ItemListByItemGroup",
                datatype: "Json",
                data: { organizationId: organizationCtrl.val(), itemGroupId: itemGroupCtrl.val() },
                success: function (result) {
                    var data = result.returnList;
                    $.each(data, function (index, r) {
                        itemCtrl.append('<option value="' + r.Value + '">' + r.Text + '</option>');
                    });
                }
            });
        }
        else if (organizationCtrl.val() <= 0) {
            $.fn.customMessage({
                displayMessage: "Organization is not selected.",
                displayMessageType: "e"
            });
        }
        else if (itemGroupCtrl.val() <= 0) {
            $.fn.customMessage({
                displayMessage: "Please select ItemGroup.",
                displayMessageType: "i"
            });
        }
    });

    //Load Item's Program Session By Item
    $(document).on('change', ".itemDetailsItemName", function (event) {
        var tr = $(this).closest(".requisitionSlipItemDetails");
        var itemCtrl = tr.find(".itemDetailsItemName");
        var programSessionCtrl = tr.find(".itemDetailsProgramSession");
        var purpose = tr.find(".itemDetailsPurpose").val();
        var organizationId = $("#OrganizationId").val();
        var branchId = $("#BranchId").val();

        if (organizationId > 0 && branchId > 0 && itemCtrl.val() > 0) {
            $.ajax({
                type: "POST",
                url: "/UInventory/CommonAjaxUInventory/GetProgramAndSession",
                datatype: "Json",
                data: { organizationId: organizationId, branchIdList: branchId, itemIdList: itemCtrl.val(), purposeId: purpose, isReport: false },
                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function(result) {
                    if (result.isSuccess) {
                        var psData = result.programSessionList;
                        programSessionCtrl.empty();
                        programSessionCtrl.append($('<option>').text("Select Program & Session").attr('value', ""));
                        if (psData != null) {
                            $.each(psData, function(i, r) {
                                if (r.Value != -1 && r.Value != "0") {
                                    programSessionCtrl.append($('<option>').text(r.Text).attr('value', r.Value));
                                }
                            });
                        }
                    }
                    $.unblockUI();
                },
                complete: function() {
                    $.unblockUI();
                },
                error: function(rt) {
                    $.unblockUI();
                    $.fn.customMessage({
                        displayMessage: "Unable to load item's program session.",
                        displayMessageType: "e"
                    });
                }
            });
        } else {
            programSessionCtrl.empty();
            programSessionCtrl.append($('<option>').text("Select Program & Session").attr('value', ""));
        }
    });

    //Re-Load Item's Program Session For Purpose changes
    $(document).on('change', ".itemDetailsPurpose", function (event) {
        var tr = $(this).closest(".requisitionSlipItemDetails");
        var itemCtrl = tr.find(".itemDetailsItemName");
        var organizationId = $("#OrganizationId").val();
        var branchId = $("#BranchId").val();

        if (organizationId > 0 && branchId > 0 && itemCtrl.val() > 0) {
            var programSessionCtrl = tr.find(".itemDetailsProgramSession");
            var purpose = tr.find(".itemDetailsPurpose").val();

            $.ajax({
                type: "POST",
                url: "/UInventory/CommonAjaxUInventory/GetProgramAndSession",
                //url: "/UInventory/Requisition/GetProgramAndSessionItem",
                datatype: "Json",
                data: { organizationId: organizationId, branchIdList: branchId, itemIdList: itemCtrl.val(), purposeId: purpose, isReport: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    if (result.isSuccess) {
                        var psData = result.programSessionList;
                        programSessionCtrl.empty();
                        programSessionCtrl.append($('<option>').text("Select Program & Session").attr('value', ""));
                        if (psData != null) {
                            $.each(psData, function (i, r) {
                                if (r.Value != -1 && r.Value != "0") {
                                    programSessionCtrl.append($('<option>').text(r.Text).attr('value', r.Value));
                                }
                            });
                        }
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (rt) {
                    $.unblockUI();
                    $.fn.customMessage({
                        displayMessage: "Unable to load item's program session.",
                        displayMessageType: "e"
                    });
                }
            });
        }
    });

    //Add ItemDetails Row
    $(document).on("click", "#RequisitionItemDetailsAddButton", function (event) {
        var index = 0;
        $("#RequisitionItemList tr").each(function () {
            var indexCtrl = $(this).find(".itemDetailsIndex");
            var currentIndex = indexCtrl.val();
            if (currentIndex > index) {
                index = currentIndex;
            }
        });

        $.ajax({
            type: "POST",
            url: "/UInventory/Requisition/AddRequisitionItemDetailsRow",
            data: { index: index },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (dRow) {
                $.unblockUI();
                if ($('#RequisitionItemList tr').length > 0) {
                    $('#RequisitionItemList tr:last').after(dRow);
                } else {
                    $('#RequisitionItemList').append(dRow);
                }
                fnValidateDynamicContent();
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (rt) {
                $.unblockUI();
                $.fn.customMessage({
                    displayMessage: "Unable to add more item details row.",
                    displayMessageType: "e"
                });
            }

        });
    });

    //Remove ItemDetails Row
    $(document).on("click", ".requisitionItemDetailsRemoveButton", function (event) {
        var tr = $(this).closest(".requisitionSlipItemDetails");
        tr.remove();
        var index = 0;

        $("#RequisitionItemList tr").each(function () {
            $(this).find(".itemDetailsId").val(0);
            $(this).find(".itemDetailsIndex").val(index);
            var purposeLabel = "RequisitionDetails[" + index + "].Purpose";
            var purposeId = "RequisitionDetails_" + index + "__Purpose";
            var itemGroupLabel = "RequisitionDetails[" + index + "].ItemGroup";
            var itemGroupId = "RequisitionDetails_" + index + "__ItemGroup";
            var itemLabel = "RequisitionDetails[" + index + "].Item";
            var itemId = "RequisitionDetails_" + index + "__Item";
            var programSessionLabel = "RequisitionDetails[" + index + "].ProgramSession";
            var programSessionId = "RequisitionDetails_" + index + "__ProgramSession";
            var quantityLabel = "RequisitionDetails[" + index + "].Quantity";
            var quantityId = "RequisitionDetails_" + index + "__Quantity";

            $(this).find(".itemDetailsPurpose").attr('name', purposeLabel);
            $(this).find(".itemDetailsPurpose").attr('id', purposeId);
            $(this).find(".itemDetailsItemGroup").attr('name', itemGroupLabel);
            $(this).find(".itemDetailsItemGroup").attr('id', itemGroupId);
            $(this).find(".itemDetailsItemName").attr('name', itemLabel);
            $(this).find(".itemDetailsItemName").attr('id', itemId);
            $(this).find(".itemDetailsProgramSession").attr('name', programSessionLabel);
            $(this).find(".itemDetailsProgramSession").attr('id', programSessionId);
            $(this).find(".itemDetailsQuantity").attr('name', quantityLabel);
            $(this).find(".itemDetailsQuantity").attr('id', quantityId);
            index = index + 1;
        });
        //fnValidateDynamicContent();
    });

    $("#Submit").click(function (e) {
        var rowCount = $("#RequisitionItemList").find('tr').length;
        if (rowCount <= 0) {
            $.fn.customMessage({
                displayMessage: "No rows found!",
                displayMessageType: "e"
            });
            e.preventDefault();
        } else {
            $("#RequisitionForm").submit();
            /*var success = true;
            var rowList = [];
            var distinctList = [];
            var invalidList = false;
            $("#RequisitionItemList tr").each(function () {
                var row = [];
                var purpose = $(this).find(".itemDetailsPurpose").val();
                var itemGroup = $(this).find(".itemDetailsItemGroup").val();
                var itemName = $(this).find(".itemDetailsItemName").val();
                var programSession = $(this).find(".itemDetailsProgramSession").val();
                var itemQuantity = $(this).find(".itemDetailsQuantity").val();

                if (purpose == "" || itemGroup == "" || itemName == "" || itemQuantity == "" || programSession == "") {
                    invalidList = true;
                    if (purpose == "") {
                        var purposeCtrl = $(this).find(".itemDetailsPurpose");
                        var newlabel = document.createElement("Label");
                        newlabel.setAttribute("for", "");
                        newlabel.setAttribute("Class", "errorTxt");
                        newlabel.innerHTML = "Here goes the text";
                        purposeCtrl.parent.append(newlabel);
                    }
                    return false;
                } else {
                    row[0] = purpose;
                    row[1] = itemGroup;
                    row[2] = itemName;
                    row[3] = programSession;
                    rowList.push(row);
                    return true;
                }
            });

            if (invalidList) {
                $.fn.customMessage({
                    displayMessage: "Item(s) details not submitted properly!",
                    displayMessageType: "e"
                });
                e.preventDefault();
            }

            if (rowList.length > 1) {
                distinctList = uniqueBy(rowList, JSON.stringify);
                if (rowList.length != distinctList.length) {
                    success = false;
                }
            }

            if (!success) {
                $.fn.customMessage({
                    displayMessage: "Multiple item details found with same property!",
                    displayMessageType: "e"
                });
                e.preventDefault();
            }
            else {
                $("#RequisitionForm").submit();
            }*/
        }
    });

    function uniqueBy(a, key) {
        var seen = {};
        return a.filter(function (item) {
            var k = key(item);
            return seen.hasOwnProperty(k) ? false : (seen[k] = true);
        });
    }

    var today = new Date();
    today.setDate(today.getDate() - 0);
    $('.form_datetime').datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: false,
        showMeridian: true,
        initialDate: new Date(),
        startDate: new Date(),
        startView: 2,
        minView: 2,
        maxView: 4
    });

    function fnValidateDynamicContent() {
        var holder = $('#RequisitionItemList');
        var form = $(holder).closest('form');
        form.removeData('validator');
        form.removeData('unobtrusiveValidation');
        jQuery.validator.unobtrusive.parse(form);
        form.validate();
    }

    $('.removeZeroAtFirst').on('input', function () {
        var val = this.value;
        console.log(val);
        while (val.charAt(0) === '0') {
            val = val.substr(0, 0) + val.substring(1);
            break;
        }
        this.value = val.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    });

    /*(function ($) {
        // Walk through the adapters that connect unobstrusive validation to jQuery.validate.
        // Look for all adapters that perform number validation
        $.each($.validator.unobtrusive.adapters, function () {
            if (this.name === "number") {
                // Get the method called by the adapter, and replace it with one 
                // that changes the message to the jQuery.validate default message
                // that can be globalized. If that string contains a {0} placeholder, 
                // it is replaced by the field name.
                var baseAdapt = this.adapt;
                this.adapt = function (options) {
                    var fieldName = new RegExp("The field (.+) must be a number").exec(options.message)[1];
                    options.message = $.validator.format($.validator.messages.number, fieldName);
                    baseAdapt(options);
                };
            }
        });
    }(jQuery));*/

    /*$("#RequiredQuantity, #Rate").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });*/
});