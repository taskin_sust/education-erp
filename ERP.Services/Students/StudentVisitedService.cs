﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;
using UdvashERP.MessageExceptions;

namespace UdvashERP.Services.Students
{
    public interface IStudentVisitedService : IBaseService
    {
        #region Operational Function
        bool SaveOrUpdate(StudentVisited studentVisited);
        #endregion

        #region Single Instances Loading Function
        StudentVisited LoadById(long id);
        #endregion

        #region List Loading Function
        IList<Institute> LoadInstituteByQuery(string name);
        IList<StudentVisited> LoadStudentVisited(string classes, string year, string orderBy, string orderDir, int start, int length, string name, string mobile, string institute, string StudentVisitType, string UserId, string InterestedOrganizationId, long[] InterestedBranchIds);
        IList<StudentVisited> LoadStudentAdmissionVisited(string orderBy, string orderDir, int start, int length, string classes, string year, long ProgramId, long SessionId, string name, string mobile, string institute, string StudentVisitType, int StudentType, long[] UserIds, long[] BranchIds);
        IList<StudentVisited> LoadByMobile(string mobileNumber);
        IList<string> LoadYear(string Class);
        IList<long> LoadUniqueUser(string Class, string Year, string StudentVisitType);
        #endregion

        #region Others Function
        int GetStudentVisitedCount(string Class, string Year, string Name, string Mobile, string Institute, string StudentVisitType, string UserId, string InterestedOrganizationId, long[] InterestedBranchIds);
        int GetStudentAdmissionVisitedCount(string Class, string Year, long ProgramId, long SessionId, string Name, string Mobile, string Institute, string StudentVisitType, int StudentType, long[] UserIds, long[] BranchIds);
        #endregion
    }
    public class StudentVisitedService : BaseService, IStudentVisitedService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentVisitedDao _studentVisitedDao;
        private readonly IInstituteDao _instituteDao;

        public StudentVisitedService(ISession session)
        {
            Session = session;
            _studentVisitedDao = new StudentVisitedDao { Session = Session };
            _instituteDao = new InstituteDao { Session = Session };
        }
        #endregion

        #region Operational Function

        public bool SaveOrUpdate(StudentVisited studentVisited)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    CheckEmptyField(studentVisited);
                    //check before save
                    CheckBeforeSave(studentVisited);
                    _studentVisitedDao.SaveOrUpdate(studentVisited);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public StudentVisited LoadById(long id)
        {
            try
            {
                return _studentVisitedDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        #endregion

        #region List Loading Function
        public IList<Institute> LoadInstituteByQuery(string name)
        {
            try
            {
                return _instituteDao.LoadInstituteByQuery(name);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<StudentVisited> LoadStudentVisited(string classes, string year, string orderBy, string orderDir, int start, int length, string name, string mobile, string institute, string StudentVisitType, string UserId, string InterestedOrganizationId, long[] InterestedBranchIds)
        {
            try
            {
                return _studentVisitedDao.LoadStudentVisited(start, length, orderBy, orderDir, classes, year, name, mobile, institute, StudentVisitType, UserId, InterestedOrganizationId, InterestedBranchIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<StudentVisited> LoadStudentAdmissionVisited(string orderBy, string orderDir, int start, int length, string classes, string year, long ProgramId, long SessionId, string name, string mobile, string institute, string StudentVisitType, int StudentType, long[] UserIds, long[] BranchIds)
        {
            try
            {
                return _studentVisitedDao.LoadStudentAdmissionVisited(start, length, orderBy, orderDir, classes, year, ProgramId, SessionId, name, mobile, institute, StudentVisitType, StudentType, UserIds, BranchIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<StudentVisited> LoadByMobile(string mobileNumber)
        {
            try
            {
                return _studentVisitedDao.LoadByMobile(mobileNumber);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<string> LoadYear(string Class)
        {
            try
            {
                return _studentVisitedDao.LoadYear(Class);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public IList<long> LoadUniqueUser(string Class, string Year, string StudentVisitType)
        {
            try
            {
                return _studentVisitedDao.LoadUniqueUser(Class, Year, StudentVisitType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        public int GetStudentVisitedCount(string Class, string Year, string Name, string Mobile, string Institute, string StudentVisitType, string UserId, string InterestedOrganizationId, long[] InterestedBranchIds)
        {
            try
            {
                return _studentVisitedDao.GetStudentVisitedCount(Class, Year, Name, Mobile, Institute, StudentVisitType, UserId, InterestedOrganizationId, InterestedBranchIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public int GetStudentAdmissionVisitedCount(string Class, string Year, long ProgramId, long SessionId, string Name, string Mobile, string Institute, string StudentVisitType, int StudentType, long[] UserIds, long[] BranchIds)
        {
            try
            {
                return _studentVisitedDao.GetStudentAdmissionVisitedCount(Class, Year, ProgramId, SessionId, Name, Mobile, Institute, StudentVisitType, StudentType, UserIds, BranchIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Helper Function
        private void CheckBeforeSave(StudentVisited studentVisited)
        {
            if (_studentVisitedDao.CheckDuplicate(studentVisited) == true)
            {
                throw new DuplicateEntryException("Duplicate entry found");
            }
        }

        private void CheckEmptyField(StudentVisited studentVisited)
        {
            if (studentVisited.NickName == null)
                throw new EmptyFieldException("Nick Name name is empty");
            if (studentVisited.Mobile == null)
                throw new EmptyFieldException("Mobile is empty");
            if (studentVisited.Class == null)
                throw new EmptyFieldException("Class is empty");
            if (studentVisited.Year == null)
                throw new EmptyFieldException("Year is empty");
            if (studentVisited.Institute == null)
                throw new EmptyFieldException("Institute is empty");
            //if (studentVisited.LastMeritPosition == null)
            //    throw new EmptyFieldException("Last Merit Position code in empty");
            //if (studentVisited.Exam == null)
            //    throw new EmptyFieldException("Exam code in empty");
            //if (studentVisited.Section == null)
            //    throw new EmptyFieldException("Section code in empty");
        }
        #endregion
    }
}