﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.GoodsReturnTest
{
    public class GoodsReturnBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal GoodsReturnFactory _goodsReturnFactory;
        internal IGoodsReturnService _goodsReturnService;
        internal TestBaseService<GoodsReturn> _goodsReturnTestBaseService;
        #endregion

        public GoodsReturnBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _goodsReturnFactory = new GoodsReturnFactory(null, _session);
            _goodsReturnTestBaseService = new TestBaseService<GoodsReturn>(_session);
            _goodsReturnService = new GoodsReturnService(_session);
        }
        public void CleanUpSpecificationCriteria(IList<SpecificationCriteria> specificationCriterias)
        {
            _goodsReturnTestBaseService.DeleteSpecificationCriteria(specificationCriterias);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            if (_goodsReturnFactory.Object != null)
            {
                _goodsReturnTestBaseService.DeleteGoodsReturnAndReceiveDetails(new List<GoodsReturn>() { _goodsReturnFactory.Object });
            }
            if (_goodsReturnFactory.ObjectList != null && _goodsReturnFactory.ObjectList.Count > 0)
            {
                _goodsReturnTestBaseService.DeleteGoodsReturnAndReceiveDetails(_goodsReturnFactory.ObjectList);
            }
            _goodsReturnFactory.CleanUp();
            base.Dispose();
        }
    }
}
