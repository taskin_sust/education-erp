﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using NHibernate;
using NHibernate.Id;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.OrganizationTest
{
    public interface IOrganizationSaveTest
    {
        [Fact]
        void Save_Should_Return_Null_Exception();

        [Fact]
        void Should_Save_Successfully();

        [Fact]
        void Save_Should_Check_Duplicate_Entry();

        [Fact]
        void Save_Should_Check_Invalid_Entry(int i);

    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Organization")]
    public class OrganizationSaveTest : OrganizationBaseTest, IOrganizationSaveTest
    {
        #region Test Related Properties
        public static readonly List<Organization> TestCaseOrganizationList = OrganizationList();

        public static IEnumerable<object[]> OrganizationIndex
        {
            get
            {
                var indexList = new List<object[]>();
                for (int i = 0; i < TestCaseOrganizationList.Count; i++)
                    indexList.Add(new object[] { i });
                return indexList;
            }
        }
        #endregion

        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _organizationService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var organizationFactory =
                _organizationFactory.Create().WithNameAndRank(_name, 1).WithRequiredProperties(_name, _shortName, _website, _email, _contactNo, _logoImageUrl, _reportImageUrl, _idFooterImageUrl)
                    .Persist();
            Organization organization = _organizationService.LoadById(organizationFactory.Object.Id);
            Assert.NotNull(organization);
            Assert.True(organization.Id > 0);
        }


        [Theory]
        [MemberData("OrganizationIndex", MemberType = typeof(OrganizationSaveTest))]
        public void Save_Should_Check_Invalid_Entry(int i)
        {
            var organization = TestCaseOrganizationList[i];
            OrganizationFactory organizationFactory =
                _organizationFactory.Create()
                    .WithRequiredProperties(organization.Name, organization.ShortName, organization.Website,
                        organization.Email,
                        organization.ContactNo, organization.LogoImageUrl,
                        organization.ReportImageUrl, organization.IdFooterImageUrl, organization.BasicSalary,
                        organization.HouseRent, organization.MedicalAllowance, organization.Convenyance, organization.MonthlyWorkingDay,
                        organization.DeductionPerAbsent);
            Assert.Throws<InvalidDataException>(() => organizationFactory.Persist());
        }
        #endregion

        #region Business Logic Test
        [Fact]
        public void Save_Should_Check_Duplicate_Entry()
        {
            var organizationFactory1 =
                _organizationFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithRequiredProperties(_name, _shortName, _website, _email, _contactNo, _logoImageUrl,
                        _reportImageUrl, _idFooterImageUrl)
                    .Persist();
            var organizationFactory2 =
                _organizationFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithRequiredProperties(_name, _shortName, _website, _email, _contactNo, _logoImageUrl,
                        _reportImageUrl, _idFooterImageUrl);

            Assert.Throws<DuplicateEntryException>(() => organizationFactory2.Persist());
        }
        #endregion

        #region Helper Function
        public static List<Organization> OrganizationList()
        {
            var organizationList = new List<Organization>();
            for (var i = 0; i < 22; i++)
            {
                organizationList.Add(new Organization()
                {
                    Name = i == 0 ? null : Guid.NewGuid() + "_name",
                    ShortName = i == 1 ? null : Guid.NewGuid() + "_shortName",
                    Website = i == 2 ? null : Guid.NewGuid() + ".com",
                    Email = i == 3 ? null : i == 8 ? "test email" : Guid.NewGuid() + "@yahoo.com",
                    ContactNo = i == 4 ? null : i == 9 ? "12345678910" : "8801714963254",
                    LogoImageUrl = i == 5 ? null : Guid.NewGuid() + "_logoImageUrl",
                    ReportImageUrl = i == 6 ? null : Guid.NewGuid() + "_reportImageUrl",
                    IdFooterImageUrl = i == 7 ? null : Guid.NewGuid() + "_idFooterImageUrl",
                    DeductionPerAbsent = i == 10 ? -1.5M : 1.5M,
                    HouseRent = i == 11 ? -10.5M : 10.5M,
                    MedicalAllowance = i == 12 ? -10.5M : 10.5M,
                    MonthlyWorkingDay = i == 13 ? -1 : 1,
                    Convenyance = i == 14 ? -10.5M : 10.5M,
                    BasicSalary = i == 15 ? -10.5M : 10.5M
                });
            }
            organizationList[16].DeductionPerAbsent = null;
            organizationList[17].HouseRent = null;
            organizationList[18].MedicalAllowance = null;
            organizationList[19].MonthlyWorkingDay = null;
            organizationList[20].Convenyance = null;
            organizationList[21].BasicSalary = null;
            return organizationList;
        }
        #endregion

    }
}
