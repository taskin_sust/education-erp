﻿using System;
using System.Collections.Generic;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface ITdsHistoryDao : IBaseDao<TdsHistory, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<TdsHistory> LoadTdsHistories(TeamMember member, DateTime? searchingDate);
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion


    }
    public class TdsHistoryDao : BaseDao<TdsHistory, long>, ITdsHistoryDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        public IList<TdsHistory> LoadTdsHistories(TeamMember member, DateTime? searchingDate)
        {
            if (searchingDate != null)
                return Session.QueryOver<TdsHistory>().Where(x => x.TeamMember.Id == member.Id && x.Status == TdsHistory.EntityStatus.Active && x.EffectiveDate <= searchingDate.Value.Date).OrderBy(x => x.EffectiveDate).Desc.List<TdsHistory>();

            return Session.QueryOver<TdsHistory>().Where(x => x.TeamMember.Id == member.Id && x.Status == TdsHistory.EntityStatus.Active).OrderBy(x => x.EffectiveDate).Desc.List<TdsHistory>();
        }

        #endregion

        #region Helper Function

        #endregion
    }

}
