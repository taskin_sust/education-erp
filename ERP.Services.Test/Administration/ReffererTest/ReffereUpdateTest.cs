﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ReffererTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Refferer")]
    public class ReffererUpdateTest : ReffererBaseTest
    {
        #region Basic Test


        [Fact]
        public void Should_Update_Successfully()
        {
            var referrerFactory =
                _referrerFactory.Create().WithNameAndRank(_name, 1).WithCode(_code)
                    .Persist();
            Referrer referrerObj = _referenceService.LoadById(referrerFactory.Object.Id);
            referrerObj.Name = referrerObj.Name + " DEF GHI";
            referrerObj.Code = referrerObj.Name + " 91011";
            var returnValue = _referenceService.Update(referrerObj);
            Assert.Equal(0, returnValue);
        }

        [Fact]
        public void Update_Should_Check_Duplicate_Entry()
        {
            ReferrerFactory referrerFactory =
                _referrerFactory.CreateMore(2)
                    .Persist();
            var referrer1 = referrerFactory.GetLastCreatedObjectList()[0];
            var referrer2 = referrerFactory.GetLastCreatedObjectList()[1];
            referrer2.Name = referrer1.Name;
            referrer2.Code = referrer1.Code;
            var messageCode=_referenceService.Update(referrer2);
            Assert.NotEqual(0,messageCode);
        }
        #endregion
        
    }
}
