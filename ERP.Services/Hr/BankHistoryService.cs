﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface IBankHistoryService : IBaseService
    {
        #region Operational Function
        void SaveOrUpdateBankHistory(BankHistory teamMemberBankHistory);
        void DeleteBankHistory(BankHistory bankHistory);
        #endregion

        #region Single Instances Loading Function
        BankHistory GetBankHistory(long id);
        #endregion

        #region List Loading Function
       IList<BankHistory> LoadTeamMemberBankHistory(long teamMemberId, DateTime dateTime);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
    public class BankHistoryService : BaseService, IBankHistoryService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        
        private readonly CommonHelper _commonHelper;
        private readonly IBankHistoryDao _bankHistoryDao;
        private readonly ISalarySheetDao _salarySheetDao;

        public BankHistoryService(ISession session)
        {
            Session = session;
            _bankHistoryDao = new BankHistoryDao() { Session = session };
            _salarySheetDao = new SalarySheetDao(){Session = session};
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Functions

        public void SaveOrUpdateBankHistory(BankHistory teamMemberBankHistory)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeSave(teamMemberBankHistory);
                using (transaction = Session.BeginTransaction())
                {
                    _bankHistoryDao.SaveOrUpdate(teamMemberBankHistory);
                    transaction.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void DeleteBankHistory(BankHistory bankHistory)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeDelete(bankHistory);

                using (transaction = Session.BeginTransaction())
                {
                    bankHistory.Status = BankHistory.EntityStatus.Delete;
                    _bankHistoryDao.SaveOrUpdate(bankHistory);
                    transaction.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }


        #endregion

        #region Single Instances Loading Function

        public BankHistory GetBankHistory(long id)
        {
            try
            {
                return _bankHistoryDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<BankHistory> LoadTeamMemberBankHistory(long teamMemberId, DateTime dateTime)
        {
            try
            {
                return _bankHistoryDao.LoadTeamMemberBankHistory(teamMemberId, dateTime);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function


        #endregion

        #region Helper function

        private void CheckBeforeDelete(BankHistory teamMemberBankHistory)
        {
            #region Is Post check
            SalarySheet salarySheet = _salarySheetDao.GetTeamMemberLastSalarySheet(teamMemberBankHistory.TeamMember.Id, null);
            if (salarySheet != null && salarySheet.EndDate >= teamMemberBankHistory.EffectiveDate)
            {
                throw new InvalidDataException("Effective of bank history must be after Date (" + salarySheet.EndDate.ToString("d MMM, yyyy") + ") for Pin (" + teamMemberBankHistory.TeamMember.Pin + ") .");
            }
            #endregion
        }
        private void CheckBeforeSave(BankHistory teamMemberBankHistory)
        {
            if (String.IsNullOrEmpty(teamMemberBankHistory.AccountNumber))
                throw new InvalidDataException("Please Enter A/C No.");

            if (String.IsNullOrEmpty(teamMemberBankHistory.Name))
                throw new InvalidDataException("Please Enter A/C Title.");

            if (teamMemberBankHistory.TeamMember == null)
                throw new InvalidDataException("Invalid Team Member Found!");

            if (teamMemberBankHistory.BankBranch == null)
                throw new InvalidDataException("Invalid Bank Branch Found!");

            if (_bankHistoryDao.HasSameEffectiveDateEntry(teamMemberBankHistory.TeamMember.Id, teamMemberBankHistory.EffectiveDate, teamMemberBankHistory.Id))
                throw new InvalidDataException("This Effective date has one or more Bank Information for this Team Member");

            if (_bankHistoryDao.HasSameAccountNumber(teamMemberBankHistory.BankBranch.Bank.Id, teamMemberBankHistory.AccountNumber, teamMemberBankHistory.Id))
                throw new InvalidDataException("This A/C No. Already has been taken.");


            #region Is Post check
            SalarySheet salarySheet = _salarySheetDao.GetTeamMemberLastSalarySheet(teamMemberBankHistory.TeamMember.Id,null);
            if (salarySheet != null && salarySheet.EndDate >= teamMemberBankHistory.EffectiveDate)
            {
                throw new InvalidDataException("Effective of bank history must be after Date (" + salarySheet.EndDate.ToString("d MMM, yyyy") + ") for Pin (" + teamMemberBankHistory.TeamMember.Pin + ") .");
            }
            
            #endregion
        }

        #endregion

    }
}
