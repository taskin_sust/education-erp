﻿using System;
using System.Collections.Generic;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IDiscountDetailDao : IBaseDao<DiscountDetail, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<DiscountDetail> GetByDiscount(long disId);
        #endregion

        #region Others Function

        #endregion
    }
    public class DiscountDetailDao : BaseDao<DiscountDetail, long>, IDiscountDetailDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<DiscountDetail> GetByDiscount(long disId)
        {
            return Session.QueryOver<DiscountDetail>().Where(x => x.Discount.Id == disId && x.Status != DiscountDetail.EntityStatus.Delete).List<DiscountDetail>();
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}