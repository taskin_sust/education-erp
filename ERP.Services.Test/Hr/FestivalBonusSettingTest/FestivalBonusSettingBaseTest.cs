﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.FestivalBonusSettingTest
{
    public class FestivalBonusSettingBaseTest:TestBase,IDisposable
    {
        #region Object Initialization

        //private ISession _session;
        internal readonly CommonHelper CommonHelper;
        internal FestivalBonusSettingFactory _festivalBonusSettingFactory;
        internal FestivalBonusSettingService _festivalBonusSettingService;
        internal List<long> IdList = new List<long>();
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        
        #endregion

        public FestivalBonusSettingBaseTest()
        {
            //_session = NHibernateSessionFactory.OpenSession();
             CommonHelper = new CommonHelper();
            _festivalBonusSettingService = new FestivalBonusSettingService(Session);
            _festivalBonusSettingFactory = new FestivalBonusSettingFactory(null,Session);
        }

        public void Dispose()
        {
            _festivalBonusSettingFactory.FestivalBonusSettingCalculationCleanUp(_festivalBonusSettingFactory);
            _festivalBonusSettingFactory.CleanUp();
            organizationFactory.Cleanup();
            base.Dispose();
        } 
    }
}
