$(document).ready(function () {
        function dataTableRender() {
            var pageSize = 10;
            if ($('#pageSize').val() != "" && /^\d+$/.test($('#pageSize').val())) {
                pageSize = parseInt($('#pageSize').val());
            }
            else {
                $('#pageSize').val(pageSize);
            }
            $('#DataGrid').dataTable({
                destroy: true,
                "processing": true,
                searching: false,
                serverSide: true,
                "scrollX": true,
                "bLengthChange": false,
                "iDisplayLength": pageSize,
                //stateSave: true,
                order: [[0, "desc"]],
                ajax: {
                    url: 'MenuGroupListResult',
                    type: 'POST',
                    data: function (d) {
                        d.name = $('#name').val();
                        d.status = $('#status').val();
                        d.rank = $('#rank').val();
                    }
                }
            });
        }

        dataTableRender();

        $(document).on("click", "#search", function () {
            dataTableRender();
        });

        $(document).on("click", ".glyphicon-arrow-up, .glyphicon-arrow-down", function () {
            var id = $(this).attr("id");
            var current = $(this).parent().text().trim();
            var action = $(this).attr("data-action");
            //glyphicon glyphicon-arrow-up
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/MenuGroup/RankChange",
                cache: false,
                async: true,
                data: { "id": id, "current": current, "action": action },
                success: function (result) {
                    if (result.IsSuccess) {
                        dataTableRender();
                    }
                    else {                        
                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });

        });


        $(document).on("click", ".glyphicon-trash", function () {
            var id = $(this).attr("id");
            var name = $(this).attr("data-name");
            if (id != "") {
                bootbox.deleteConfirm("<h3>Are you sure you want to delete this <span class='confirm-message'>" + name + "</span> Menu Group?</h3>", function (result) {
                    if (result) {
                        $.ajax({
                            type: "post",
                            url: $("body").attr("data-project-root") + "Administration/MenuGroup/Delete",
                            cache: false,
                            async: true,
                            data: { "id": id },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (result) {
                                $.unblockUI();

                                if (result.IsSuccess) {                                   
                                    $.fn.customMessage({
                                        displayMessage: "Menu Group Delete Successfully",
                                        displayMessageType: "success",
                                    });
                                    dataTableRender();
                                }
                                else {
                                    $.fn.customMessage({
                                        displayMessage: result.Message,
                                        displayMessageType: "e",
                                    });                                    
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (result) {
                                $.fn.customMessage({
                                    displayMessage: "Menu Group Delete Failed",
                                    displayMessageType: "e",
                                });
                            }
                        });

                    }
                }).css({ 'margin-top': (($(window).height() / 4)) });
            }
            return false;
        });
});


