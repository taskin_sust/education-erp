﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Dialect.Function;
using NHibernate.Linq;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Students
{
    public interface IStudentClassAttendanceDao : IBaseDao<StudentClassAttendence, long>
    {
        #region Operational Function
        bool ClearClassAttendace(string lectureIds, string teacherIds, string userIds, DateTime dateFrom, DateTime dateTo);
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<ClassEvaluationReportDto> LoadClassEvaluation(long[] batchIdList, long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo, long generateBy, int start, int length);
        IList<ClassAttendanceReportDto> LoadClassAttendanceReport(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId,
        long[] campusId, long[] courseId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus,
        long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds);

        IList<ClassAttendanceReportDto> LoadClassAttendanceReport(int start, int length, long programId, long sessionId, List<long> programIdList, List<long> branchIdList, long[] courseId, DateTime startDate,
            DateTime endDate, string batchIds, long[] paymentStatus, long attendanceStatus, string lectureIds);
        IList<IndividualClassAttendanceDto> LoadIndividualReport(StudentProgram studentProgram, DateTime dateFrom, DateTime dateTo);
        #endregion

        #region Others Function
        int GetCountOfTakenClassByStudentProgramAndSubjectAndCourse(long subjectId, long stdProgramId, long courseId);
        int GetStudentCount(long[] batchIdList, long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo, bool? clearClassAttendance = null, long[] userIdList = null);
        int GetClassEvaluationReportCount(long[] batchIdList, long[] lectureIdList, long[] teacherIdList, DateTime fromTime, DateTime toTime, long generateBy);
        #endregion

        #region Clean
        //object ClassAttendanceCountByBatchLectureTeacherStudentProgram(long lectureId, long studentProgramId, long? teacherId = null, long? batchId = null, DateTime? heldDate = null, bool? returnCount = null);
        //Model.Dto.ClassAttendanceReportDto GetClassAttendanceReport(long studentProgramId, long batchId, DateTime startDate, DateTime endDate);
        #endregion

    }

    public class StudentClassAttendanceDao : BaseDao<StudentClassAttendence, long>, IStudentClassAttendanceDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        public bool ClearClassAttendace(string lectureIds, string teacherIds, string userIds, DateTime dateFrom, DateTime dateTo)
        {
            ITransaction trans = null;
            try
            {

                using (trans = Session.BeginTransaction())
                {
                    var deleteQuery =
                        "delete  from StudentClassAttendanceDetails where StudentClassAttendanceId in(select distinct id from StudentClassAttendence where lectureid in(" +
                        lectureIds + ") and teacherid in(" + teacherIds + ") and createby in(" + userIds + ") and HeldDate>='" + dateFrom +
                        "' and HeldDate<'" + dateTo.AddDays(1) +
                        "');delete from studentclassattendence where lectureid in(" + lectureIds +
                        ") and teacherid in(" + teacherIds + ") and createby in(" + userIds + ") and HeldDate>='" + dateFrom + "' and HeldDate<'" +
                        dateTo.AddDays(1) + "'";
                    Session.CreateSQLQuery(deleteQuery).SetTimeout(3700)
                    .ExecuteUpdate();
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                return false;
            }
        }
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<ClassEvaluationReportDto> LoadClassEvaluation(long[] batchIdList, long[] lectureIdList,
                       long[] teacherIdList, DateTime dateFrom, DateTime dateTo, long generateBy, int start, int length)
        {

            var sql =
                "select ";
            sql += " LectureId,";
            sql += "TeacherId,CAST(Good as nvarchar)+' ('+CAST(CAST (GoodPercentage AS decimal (10,0)) as nvarchar)+'%)' as Good " +
            ",CAST(Average as nvarchar)+' ('+CAST(CAST (AveragePercentage AS decimal (10,0)) as nvarchar)+'%)' as Average " +
            ",CAST(Bad as nvarchar)+' ('+CAST(CAST (BadPercentage AS decimal (10,0)) as nvarchar)+'%)' as Bad " +
            ",Total,TeacherLate " +

            "from(" +

            "select ";
            if (generateBy == 1)
            {
                sql += " sca.LectureId ,";
            }
            else if (generateBy == 2)
            {
                sql += " 0 as LectureId,";
            }
            sql += "sca.TeacherId,sum(distinct sca.TeacherLate) as TeacherLate, " +
            "sum((case " +
            "when scad.StudentFeedBack = 1 then 1 " +
            "else 0 " +
            "end))               as Good, " +
            "sum((case " +
            "when scad.StudentFeedBack = 2  then 1 " +
            "else 0 " +
            "end))               as Average, " +
            "sum((case " +
            "when scad.StudentFeedBack = 3  then 1 " +
            "else 0 " +
            "end))               as Bad, " +
            "sum((case " +
            "when scad.StudentFeedBack in (1,2,3)  then 1 " +
            "else 0 " +
            "end))               as Total, " +
            "(CAST(sum((case " +
            "when scad.StudentFeedBack = 1  then 1 " +
            "else 0 " +
            "end))  as decimal)/CAST(sum((case " +
            "when scad.StudentFeedBack in (1,2,3)  then 1 " +
            "else 0 " +
            "end))  as decimal)*100) as GoodPercentage, " +
            "(CAST(sum((case " +
            "when scad.StudentFeedBack = 2  then 1 " +
            "else 0 " +
            "end))  as decimal)/CAST(sum((case " +
            "when scad.StudentFeedBack in (1,2,3)  then 1 " +
            "else 0 " +
            "end))  as decimal)*100) as AveragePercentage, " +
            "(CAST(sum((case " +
            "when scad.StudentFeedBack = 3  then 1 " +
            "else 0 " +
            "end))  as decimal)/CAST(sum((case " +
            "when scad.StudentFeedBack in (1,2,3)  then 1 " +
            "else 0 " +
            "end))  as decimal)*100) as BadPercentage " +


            "from StudentClassAttendanceDetails scad inner join StudentProgram sp on scad.StudentProgramId=sp.Id " +
            "inner join StudentClassAttendence sca on scad.StudentClassAttendanceId=sca.Id " +
            "where sp.BatchId " +
            "in(" + string.Join(", ", batchIdList) + ") and " +
            "sca.LectureId in(" + string.Join(", ", lectureIdList) + ") " +
            "and sca.TeacherId in(" + string.Join(", ", teacherIdList) + ") " +
            "and sca.HeldDate>='" + dateFrom.Date + "' and sca.HeldDate<='" + dateTo.Date + "' " +
            "and sca.Status=1 and scad.Status=1 ";
            if (generateBy == 1)
            {
                sql += " group by sca.LectureId,sca.TeacherId ";
            }
            else if (generateBy == 2)
            {
                sql += " group by sca.TeacherId ";
            }
            if (length > 0)
            {
                sql += "order by goodPercentage desc offset " + start + " rows fetch next " + length + " rows only )x";
            }
            else
            {
                sql += ")x order by goodPercentage desc";
            }

            IQuery iQuery = Session.CreateSQLQuery(sql);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ClassEvaluationReportDto>());
            var list = iQuery.List<ClassEvaluationReportDto>().ToList();
            return list;
        }

        public IList<ClassAttendanceReportDto> LoadClassAttendanceReport(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, long[] branchId,
         long[] campusId, long[] courseId, string[] batchDays, string[] batchTime, long[] batchList, long[] paymentStatus,
         long attendanceStatus, DateTime startDate, DateTime endDate, string batchIds)
        {

            var query = @"with studentProgramCTEE as ( select
                           distinct Id,
                           prnNo,
                           batchid 
                        from
                           studentprogram 
                        where
                           id in(select sp.id from studentprogram sp inner join  batch b on sp.batchid=b.id where

                        sp.programid={0} and sp.programid in({1}) and b.sessionid={2} and b.branchid in({3}) and b.branchId in ({6})) select
                           id as StdProId,
                           PrnNo,
                           LectureHeld,
                           CAST(COALESCE(RegisteredPresent,
                           0) as int)+CAST(COALESCE(UnregisteredPresent,
                           0) as int) LectureAttend,
                           COALESCE(RegisteredPresent,
                           0)RegisteredPresent,
                           COALESCE(UnregisteredPresent,
                           0)UnregisteredPresent,
                           COALESCE(RegisteredAbsent,
                           0)RegisteredAbsent 
                        from
                           (select
                              swcp.prnNo,
                              swcp.id,
                              count(distinct cp.lectureid) as RegisteredAbsent 
                           from
                              studentProgramCTEE swcp 
                           left join
                              courseprogress cp 
                                 on swcp.batchid=cp.batchid 
                                 and cp.heldDate>='{4}' 
                                 and cp.heldDate<'{5}' 
                                 and cp.lectureid not in (
                                    select
                                       sca.lectureid 
                                 from
                                    [dbo].[StudentClassAttendanceDetails] scad 
                                 inner join
                                    [dbo].[StudentClassAttendence] sca 
                                       on scad.[StudentClassAttendanceId]=sca.id 
                                       and scad.studentprogramid=swcp.id 
                                 where
                                    sca.heldDate>='{4}'
                                    and sca.heldDate<'{5}'
                              ) 
                              and cp.lectureid in (
                                 select
                                    * 
                                 from
                                    (select
                                       distinct lectureid 
                                    from
                                       (select
                                          coursesubjectid,
                                          creationdate 
                                       from
                                          [dbo].[StudentCourseDetails] scd 
                                       where
                                          studentprogramid=swcp.id 
                                          and scd.[status]=1) x 
                                    inner join
                                       (
                                          select
                                             distinct ls.courseSubjectid,
                                             lectureid,
                                             helddate 
                                          from
                                             courseprogress cp  
                                          inner join
                                             lecture l 
                                                on cp.lectureid=l.id 
                                          inner join
                                             lecturesettings ls 
                                                on l.lecturesettingsid=ls.id 
                                                and ls.[status]=1 
                                                and cp.heldDate>='{4}' 
                                                and cp.heldDate<'{5}'
                                                and ls.coursesubjectid in(
                                                   select
                                                      coursesubjectid 
                                                from
                                                   [dbo].[StudentCourseDetails] 
                                                where
                                                   studentprogramid=swcp.id  
                                                   and [status]=1
                                             ) 
                                             and cp.batchid=swcp.batchid  
                                             and ls.coursesubjectid is not null";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += "and ls.courseId in(" + string.Join(", ", courseId) + ")";
            }
            query += @")y 
                                             on x.coursesubjectid=y.coursesubjectid 
                                             and x.creationdate<=y.helddate
                                       )x 
                                 union
                                 (
                                    select
                                       distinct l.id 
                                    from
                                       lecture l 
                                    inner join
                                       lecturesettings ls 
                                          on l.lecturesettingsid=ls.id 
                                          and ls.courseSubjectid is null";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " and ls.courseId in(" + string.Join(", ", courseId) + ")";
            }
            query += @" )
                                 ) 
                           group by
                              swcp.id,
                              swcp.prnNo) x 
                           left join
                              (
                                 select
                                    scad.studentprogramid,
                                    count(*) as RegisteredPresent 
                                 from
                                    [dbo].[StudentClassAttendanceDetails] scad 
                                 inner join
                                    [dbo].[StudentClassAttendence] sca 
                                       on scad.[StudentClassAttendanceId]=sca.id ";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " inner join lecture l on l.id=sca.lectureid inner join lecturesettings ls on l.lecturesettingsid=ls.id and ls.courseid in (" + string.Join(", ", courseId) + ")";
            }
            query += @" where
                                    scad.isregistered=1 
                                    and sca.heldDate>='{4}'
                                    and sca.heldDate<'{5}'  
                                 group by
                                    scad.studentprogramid
                              )y 
                                 on x.id=y.studentprogramid 
                           left join
                              (
                                 select
                                    scad.studentprogramid,
                                    count(*) as UnregisteredPresent 
                                 from
                                    [dbo].[StudentClassAttendanceDetails] scad 
                                 inner join
                                    [dbo].[StudentClassAttendence] sca 
                                       on scad.[StudentClassAttendanceId]=sca.id ";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " inner join lecture l on l.id=sca.lectureid inner join lecturesettings ls on l.lecturesettingsid=ls.id and ls.courseid in (" + string.Join(", ", courseId) + ")";
            }
            query += @" where
                                    scad.isregistered=0 
                                    and sca.heldDate>='{4}'
                                    and sca.heldDate<'{5}'
                                 group by
                                    scad.studentprogramid
                              )z 
                                 on x.id=z.studentprogramid 
                           left join
                              (
                                 select
                                    sp.id spid,
                                    COALESCE(classHeld,
                                    0) as LectureHeld 
                                 from
                                    studentProgramCTEE sp 
                                 left join
                                    (
                                       select
                                          batchid,
                                          count(*) classHeld 
                                       from
                                          courseprogress cp  ";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " inner join lecture l on l.id=cp.lectureid inner join lecturesettings ls on l.lecturesettingsid=ls.id and ls.courseid in (" + string.Join(", ", courseId) + ")";
            }
            query += @" where
                                          cp.heldDate>='{4}' 
                                          and cp.heldDate<'{5}' 
                                       group by
                                          batchid
                                    )x 
                                       on sp.batchid=x.batchid 
                                    )p 
                                       on p.spid=x.id where RegisteredPresent>0
                                 order by
                                    prnNo offset 10 rows fetch next 10
                rows only ;";
            string endQuery = String.Format(query, programId, string.Join(", ", programIdList), sessionId, string.Join(", ", branchIdList), startDate, endDate.AddDays(1), batchIds);
            var attendanceReport = new List<ClassAttendanceReportDto>();

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ClassAttendanceReportDto>()).SetTimeout(2700);
            attendanceReport = iQuery.List<ClassAttendanceReportDto>().ToList();

            return attendanceReport;
        }

        public IList<ClassAttendanceReportDto> LoadClassAttendanceReport(int start, int length, long programId, long sessionId, List<long> programIdList, List<long> branchIdList, long[] courseId, DateTime startDate, DateTime endDate, string batchIds, long[] paymentStatus, long attendanceStatus, string lectureIds)
        {
            var query = @"with studentProgramCTEE as ( select
   distinct Id,
   prnNo,
   batchid 
from
   studentprogram 
where
   id in(select sp.id from studentprogram sp inner join  batch b on sp.batchid=b.id where

sp.programid={0} and sp.programid in({1}) and b.sessionid={2} and b.branchid in({3}) and b.Id in ({6})";
            if (!paymentStatus.Contains(0))
            {
                if (paymentStatus.Contains(1))
                {
                    query += " and dueamount=0";
                }
                if (paymentStatus.Contains(2))
                {
                    query += " and dueamount>0";
                }
            }

            query += @" )) select
   x.id as StdProId,
   x.PrnNo,
   LectureHeld,
   CAST(COALESCE(RegisteredPresent,
   0) as int)+CAST(COALESCE(UnregisteredPresent,
   0) as int) LectureAttend,
   COALESCE(RegisteredPresent,
   0)RegisteredPresent,
   COALESCE(UnregisteredPresent,
   0)UnregisteredPresent,                                                                                                                                                                                                                                                                               
   (COALESCE(RegisteredAbsent,
      0)) +
	  (COALESCE(RegisteredAbsent2,
      0))+(COALESCE(complementaryAbsent,
      0))
	  RegisteredAbsent
from
   (select
      swcp.prnNo,
      swcp.id,
      count(distinct cp.lectureid) as RegisteredAbsent 
   from
      studentProgramCTEE swcp 
   left join
      courseprogress cp 
         on cp.batchid =swcp.batchid
--and cp.batchid  =(select batchid from studentprogram where id=swcp.BatchId)  
         and cp.heldDate>='{4}' 
         and cp.heldDate<'{5}' 
         and cp.lectureid not in (
            select
               sca.lectureid 
         from
            [dbo].[StudentClassAttendanceDetails] scad 
         inner join
            [dbo].[StudentClassAttendence] sca 
               on scad.[StudentClassAttendanceId]=sca.id 
               and scad.studentprogramid=swcp.id 
         where
            sca.heldDate>='{4}'
            and sca.heldDate<'{5}'
      ) 
      and cp.coursesubjectid in (
select courseSubjectid from studentcoursedetails scd where scd.studentprogramid=swcp.id  and status=1        
) ";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.courseid in (" + string.Join(", ", courseId) + ")";
            }
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and cp.lectureId in(" + lectureIds + ")";
            }
            query += @" group by
      swcp.id,
      swcp.prnNo) x 
   

left join
            (
               select
         swcp.prnNo,
         swcp.id,
         count(distinct cp.lectureid) as RegisteredAbsent2      
      from
         studentProgramCTEE swcp      
      left join
         courseprogress cp            
            on cp.batchid =swcp.batchid
                    
            and cp.heldDate>='{4}'           
            and cp.heldDate<'{5}'         
            and cp.lectureid not in (
               select
                  sca.lectureid            
            from
               [dbo].[StudentClassAttendanceDetails] scad            
            inner join
               [dbo].[StudentClassAttendence] sca                  
                  on scad.[StudentClassAttendanceId]=sca.id                  
                  and scad.studentprogramid=swcp.id            
            where
               sca.heldDate>='{4}'              
               and sca.heldDate<'{5}'        
         )         
         and cp.courseSubjectid is null";

            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " and cp.courseid in (" + string.Join(", ", courseId) + ")";
            }
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and cp.lectureId in(" + lectureIds + ")";
            }
            query += @" group by
            swcp.id,
            swcp.prnNo
			   )zyx            
                  on x.id=zyx.id
 --complementary absent

				  left join
            (
               select
         swcp.prnNo,
         swcp.id,
         count(distinct cp.lectureid) as complementaryAbsent   
      from
         studentProgramCTEE swcp      
      left join
         courseprogress cp            
            on cp.batchid = swcp.batchid            
            and cp.heldDate>='{4}'            
            and cp.heldDate<'{5}'            
            and cp.lectureid not in (
               select
                  sca.lectureid            
            from
               [dbo].[StudentClassAttendanceDetails] scad            
            inner join
               [dbo].[StudentClassAttendence] sca                  
                  on scad.[StudentClassAttendanceId]=sca.id                  
                  and scad.studentprogramid=swcp.id            
            where
               sca.heldDate>='{4}'              
               and sca.heldDate<'{5}'        
         )         
         and cp.courseid in (
            select compc.CompCourseId from studentcoursedetails scd inner join CourseSubject cs on scd.CourseSubjectId=cs.Id inner join Course c on cs.CourseId=c.Id 
			left join ComplementaryCourse compc on c.Id=compc.CourseId
			where scd.studentprogramid=swcp.id  and scd.status=1 and cs.Status=1 and c.Status=1 and compc.Status=1
			 ) ";

            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += "and cp.courseid in (" + string.Join(", ", courseId) + ")";
            }
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and cp.lectureId in(" + lectureIds + ")";
            }
            query += @" group by
            swcp.id,
            swcp.prnNo
			   )zyxx            
                  on x.id=zyxx.id

--complementary absent
left join
      (
         select
            scad.studentprogramid,
            count(*) as RegisteredPresent 
         from
            [dbo].[StudentClassAttendanceDetails] scad 
         inner join
            [dbo].[StudentClassAttendence] sca 
               on scad.[StudentClassAttendanceId]=sca.id ";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " inner join lecture l on l.id=sca.lectureid inner join lecturesettings ls on l.lecturesettingsid=ls.id and ls.courseid in (" + string.Join(", ", courseId) + ")";
            }
            query += @" where
            scad.isregistered=1 
            and sca.heldDate>='{4}'
            and sca.heldDate<'{5}' 
            and exists(select 1 from CourseProgress where LectureId=sca.LectureId";
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and lectureId in(" + lectureIds + ")";
            }
            query += @") 
         group by
            scad.studentprogramid
      )y 
         on x.id=y.studentprogramid 
   left join
      (
         select
            scad.studentprogramid,
            count(*) as UnregisteredPresent 
         from
            [dbo].[StudentClassAttendanceDetails] scad 
         inner join
            [dbo].[StudentClassAttendence] sca 
               on scad.[StudentClassAttendanceId]=sca.id ";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " inner join lecture l on l.id=sca.lectureid inner join lecturesettings ls on l.lecturesettingsid=ls.id and ls.courseid in (" + string.Join(", ", courseId) + ")";
            }
            query += @" where
            scad.isregistered=0 
            and sca.heldDate>='{4}'
            and sca.heldDate<'{5}'
            and exists(select 1 from CourseProgress where LectureId=sca.LectureId";
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and lectureId in(" + lectureIds + ")";
            }
            query += @") group by
            scad.studentprogramid
      )z 
         on x.id=z.studentprogramid 
   left join
      (
         select
            sp.id spid,
            COALESCE(classHeld,
            0) as LectureHeld 
         from
            studentProgramCTEE sp 
         left join
            (
               select
                  batchid,
                  count(*) classHeld 
               from
                  courseprogress cp  ";
            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                query += " inner join lecture l on l.id=cp.lectureid inner join lecturesettings ls on l.lecturesettingsid=ls.id and ls.courseid in (" + string.Join(", ", courseId) + ")";
            }
            query += @" where
                  cp.heldDate>='{4}' 
                  and cp.heldDate<'{5}' 
";
            if (!string.IsNullOrEmpty(lectureIds))
            {
                query += " and lectureId in(" + lectureIds + ")";
            }
            query += @"
               group by
                  batchid
            )x 
               on sp.batchid=x.batchid 
            )p 
               on p.spid=x.id where 1=1";

            if (!paymentStatus.Contains(0))
            {
                if (paymentStatus.Contains(3))
                {
                    query += " and unregisteredpresent>0";
                }

            }
            if (attendanceStatus != 0)
            {
                if (attendanceStatus == 1)
                {
                    query += " and registeredpresent>0";
                }
                if (attendanceStatus == 2)
                {
                    query += " and registeredabsent>0";
                }
                if (attendanceStatus == 3)
                {
                    query += " and unregisteredpresent>0";
                }
            }
            query += @" order by
            prnNo";
            if (length > 0)
            {
                query += " offset " + start + " rows fetch next " + length +
                        "rows only ;";
            }

            var attendanceReport = new List<ClassAttendanceReportDto>();
            string endQuery = String.Format(query, programId, string.Join(", ", programIdList), sessionId, string.Join(", ", branchIdList), startDate, endDate.AddDays(1), batchIds);
            IQuery iQuery = Session.CreateSQLQuery(endQuery);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ClassAttendanceReportDto>()).SetTimeout(2700);
            attendanceReport = iQuery.List<ClassAttendanceReportDto>().ToList();
            return attendanceReport;
        }
        public IList<IndividualClassAttendanceDto> LoadIndividualReport(StudentProgram studentProgram,
            DateTime dateFrom, DateTime dateTo)
        {
            var query = @"declare @batchid bigint;set @batchid={0}
                            declare @studentid bigint;set @studentid={1}
                            DECLARE @IndividualRegisteredAbsent TABLE (LectureId bigint,HeldDate datetime);
                            DECLARE @IndividualRegisteredPresent TABLE (LectureId bigint,HeldDate datetime,AttendedDate datetime);
                            DECLARE @IndividualUnRegisteredPresent TABLE (LectureId bigint,HeldDate datetime,AttendedDate datetime);
                            DECLARE @IndividualFinalAttendance TABLE (LectureName nvarchar(255),SubjectName nvarchar(255), HeldDate datetime,AttendingDate datetime,AttendanceStatus nvarchar(255));
                            insert into @IndividualRegisteredAbsent
                            select LectureId,HeldDate from CourseProgress where HeldDate>='{2}' and HeldDate<'{3}' and BatchId=@batchid and LectureId not in(select LectureId from StudentClassAttendanceDetails scad inner join StudentClassAttendence sca 
                            on scad.StudentClassAttendanceId=sca.Id
                            where StudentProgramId=@studentid
                            and sca.HeldDate>='{2}' and sca.HeldDate<'{3}'
                            )
                            and CourseSubjectId in (select distinct CourseSubjectId from StudentCourseDetails where StudentProgramId=@studentid and status=1 
                            --and CreationDate<'{3}'
                            )
                            union
                            select LectureId,HeldDate from CourseProgress where HeldDate>='{2}' and HeldDate<'{3}' and BatchId=@batchid and LectureId not in(select LectureId from StudentClassAttendanceDetails scad inner join StudentClassAttendence sca 
                            on scad.StudentClassAttendanceId=sca.Id
                            where StudentProgramId=@studentid
                            and sca.HeldDate>='{2}' and sca.HeldDate<'{3}'
                            )
                            and CourseSubjectId is null
                            union
                            select LectureId,HeldDate from CourseProgress where HeldDate>='{2}' and HeldDate<'{3}' and BatchId=@batchid and LectureId not in(select LectureId from StudentClassAttendanceDetails scad inner join StudentClassAttendence sca 
                            on scad.StudentClassAttendanceId=sca.Id
                            where StudentProgramId=@studentid
                            and sca.HeldDate>='{2}' and sca.HeldDate<'{3}'
                            )
                            and CourseId in (select  distinct compc.CompCourseId from studentcoursedetails scd 
                            inner join
                            CourseSubject cs 
                            on scd.CourseSubjectId=cs.Id 
                            inner join
                            Course c 
                            on cs.CourseId=c.Id      
                            left join
                            ComplementaryCourse compc 
                            on c.Id=compc.CourseId     
                            where
                            scd.studentprogramid=@studentid
                            --and scd.CreationDate>='{2}' 
                            --and scd.CreationDate<'{3}'                            
                            and scd.status=1 
                            and cs.Status=1 
                            and c.Status=1 
                            and compc.Status=1)
                            
                            insert into @IndividualRegisteredPresent
                            select x.LectureId,coalesce(y.HeldDate,'1901-01-01'),x.HeldDate from(select sca.LectureId,sca.HeldDate from
							StudentClassAttendence sca  inner join  StudentClassAttendanceDetails scad 
                            on scad.StudentClassAttendanceId=sca.Id
							where StudentProgramId=@studentid and IsRegistered=1
							and sca.HeldDate>='{2}' and sca.HeldDate<'{3}'
                              and sca.LectureId in (select distinct LectureId from CourseProgress where HeldDate>='{2}' and HeldDate<'{3}'))x
						      left join (select  LectureId,HeldDate from CourseProgress where HeldDate>='{2}' and HeldDate<'{3}' and BatchId=@batchid)y
						      on x.LectureId=y.LectureId
                            insert into @IndividualUnRegisteredPresent
                            select x.LectureId,coalesce(y.HeldDate,'1901-01-01'),x.HeldDate from(select sca.LectureId,sca.HeldDate from
							StudentClassAttendence sca  inner join  StudentClassAttendanceDetails scad 
                            on scad.StudentClassAttendanceId=sca.Id
							where StudentProgramId=@studentid and IsRegistered=0
							and sca.HeldDate>='{2}' and sca.HeldDate<'{3}' 
                              and sca.LectureId in (select distinct LectureId from CourseProgress where HeldDate>='{2}' and HeldDate<'{3}'))x
						      left join (select  LectureId,HeldDate from CourseProgress where HeldDate>='{2}' and HeldDate<'{3}' and BatchId=@batchid)y
						      on x.LectureId=y.LectureId
                            
                            insert into @IndividualFinalAttendance
                            select l.Name,COALESCE(s.Name,'Combined')SubjectName,rp.HeldDate,rp.AttendedDate,'Registered' from @IndividualRegisteredPresent rp inner join Lecture l on rp.LectureId=l.Id
                            inner join LectureSettings ls on ls.id=l.LectureSettingsId left join CourseSubject cs on cs.id=ls.CourseSubjectId
                            left join Subject s on s.Id=cs.SubjectId

                            union all

                            select l.Name,COALESCE(s.Name,'Combined')SubjectName,urp.HeldDate,urp.AttendedDate,'Unregistered' from @IndividualUnRegisteredPresent urp inner join Lecture l on urp.LectureId=l.Id
                            inner join LectureSettings ls on ls.id=l.LectureSettingsId left join CourseSubject cs on cs.id=ls.CourseSubjectId
                            left join Subject s on s.Id=cs.SubjectId

                            union all

                            select l.Name,COALESCE(s.Name,'Combined')SubjectName,ra.HeldDate,'1901-01-01','Registered' from @IndividualRegisteredAbsent ra inner join Lecture l on ra.LectureId=l.Id
                            inner join LectureSettings ls on ls.id=l.LectureSettingsId left join CourseSubject cs on cs.id=ls.CourseSubjectId
                            left join Subject s on s.Id=cs.SubjectId

                            select LectureName LectureName,SubjectName SubjectName,case when HeldDate='1901-01-01' then 'N/A' 
                            when HeldDate!='1901-01-01' then CONVERT(nvarchar(10),HeldDate, 20)
                            End HeldDate,
                            case when AttendingDate='1901-01-01' then 'N/A' 
                            when AttendingDate!='1901-01-01' then CONVERT(nvarchar(10),AttendingDate, 20)
                            End AttendingDate,
                            AttendanceStatus [Status] from @IndividualFinalAttendance";
            query = string.Format(query, studentProgram.Batch.Id, studentProgram.Id, dateFrom, dateTo.AddDays(1));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<IndividualClassAttendanceDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<IndividualClassAttendanceDto>().ToList();
            return list;
        }



        #endregion

        #region Others Function

        public int GetCountOfTakenClassByStudentProgramAndSubjectAndCourse(long subjectId, long stdProgramId, long courseId)
        {
            var query = @"select count(distinct scad.Id) from [dbo].[StudentClassAttendanceDetails] as scad
                                inner join [dbo].[StudentClassAttendence] as sca on scad.StudentClassAttendanceId = sca.Id
                                inner join [dbo].[Lecture] as lec on lec.Id = sca.LectureId
                                inner join [dbo].[CourseProgress] as cp on cp.LectureId = lec.Id
                                inner join [dbo].[CourseSubject] as cs on cs.id=cp.CourseSubjectId
                                where scad.StudentProgramId=" + stdProgramId + " and  cs.CourseId =" + courseId + " and cs.SubjectId =" + subjectId + "";

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int GetStudentCount(long[] batchIdList, long[] lectureIdList, long[] teacherIdList,
                    DateTime dateFrom, DateTime dateTo, bool? clearClassAttendance = null, long[] userIdList = null)
        {
            var query = @"  select count(distinct scad.StudentProgramId)  from 
                                StudentClassAttendanceDetails scad 
                                inner join StudentProgram sp 
                                on scad.StudentProgramId=sp.Id
                                inner join StudentClassAttendence sca
                                on sca.Id=scad.StudentClassAttendanceId";
            if (clearClassAttendance == null)
            {
                query += @"  inner join CourseProgress cp
                                                                on cp.LectureId=sca.LectureId and cp.status=1 ";

            }
            query += @" where 
                                sca.LectureId in(";
            query = query + string.Join(", ", lectureIdList.Distinct());
            query += @"  )
                                and
                                sp.BatchId in(";
            query += string.Join(", ", batchIdList.Distinct());
            query += " )";
            if (clearClassAttendance == null)
            {
                query += "  and cp.BatchId in(" + string.Join(", ", batchIdList.Distinct()) + ")";
            }
            query += "  and sca.TeacherId in(" + string.Join(", ", teacherIdList.Distinct()) + ")";
            query += "  and StudentFeedBack in(1,2,3)";
            query += "  and sca.HeldDate>='" + dateFrom + "'";
            query += "  and sca.HeldDate<='" + dateTo.AddDays(1) + "'";
            if (clearClassAttendance == null)
            {
                query += "  and cp.HeldDate>='" + dateFrom + "'";
                query += "  and cp.HeldDate<='" + dateTo.AddDays(1) + "'";
            }

            query += "  and sca.Status=1";
            if (clearClassAttendance == null)
            {
                query += "  and cp.Status=1";
            }


            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }
        public int GetClassEvaluationReportCount(long[] batchIdList, long[] lectureIdList, long[] teacherIdList, DateTime fromTime, DateTime toTime, long generateBy)
        {
            var sql =
                "select count(*) " +
                "from(" +
                "select ";
            if (generateBy == 1)
            {
                sql += " sca.LectureId ,";
            }
            else if (generateBy == 2)
            {
                sql += " 0 as LectureId,";
            }
            sql += "sca.TeacherId,sum(distinct sca.TeacherLate) as TeacherLate, " +
            "sum((case " +
            "when scad.StudentFeedBack = 1 then 1 " +
            "else 0 " +
            "end))               as Good, " +
            "sum((case " +
            "when scad.StudentFeedBack = 2  then 1 " +
            "else 0 " +
            "end))               as Average, " +
            "sum((case " +
            "when scad.StudentFeedBack = 3  then 1 " +
            "else 0 " +
            "end))               as Bad, " +
            "sum((case " +
            "when scad.StudentFeedBack in (1,2,3)  then 1 " +
            "else 0 " +
            "end))               as Total, " +
            "(CAST(sum((case " +
            "when scad.StudentFeedBack = 1  then 1 " +
            "else 0 " +
            "end))  as decimal)/CAST(sum((case " +
            "when scad.StudentFeedBack in (1,2,3)  then 1 " +
            "else 0 " +
            "end))  as decimal)*100) as GoodPercentage, " +
            "(CAST(sum((case " +
            "when scad.StudentFeedBack = 2  then 1 " +
            "else 0 " +
            "end))  as decimal)/CAST(sum((case " +
            "when scad.StudentFeedBack in (1,2,3)  then 1 " +
            "else 0 " +
            "end))  as decimal)*100) as AveragePercentage, " +
            "(CAST(sum((case " +
            "when scad.StudentFeedBack = 3  then 1 " +
            "else 0 " +
            "end))  as decimal)/CAST(sum((case " +
            "when scad.StudentFeedBack in (1,2,3)  then 1 " +
            "else 0 " +
            "end))  as decimal)*100) as BadPercentage " +


            "from StudentClassAttendanceDetails scad inner join StudentProgram sp on scad.StudentProgramId=sp.Id " +
            "inner join StudentClassAttendence sca on scad.StudentClassAttendanceId=sca.Id " +
            "where sp.BatchId " +
            "in(" + string.Join(", ", batchIdList) + ") and " +
            "sca.LectureId in(" + string.Join(", ", lectureIdList) + ") " +
            "and sca.TeacherId in(" + string.Join(", ", teacherIdList) + ") " +
            "and sca.HeldDate>='" + fromTime.Date + "' and sca.HeldDate<='" + toTime.Date + "' " +
            "and sca.Status=1 and scad.Status=1 ";
            if (generateBy == 1)
            {
                sql += " group by sca.LectureId,sca.TeacherId ";
            }
            else if (generateBy == 2)
            {
                sql += " group by sca.TeacherId ";
            }

            sql += ")x";

            IQuery iQuery = Session.CreateSQLQuery(sql);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }
        #endregion

        #region Helper Function

        #endregion

        #region Clean
        //            public Model.Dto.ClassAttendanceReportDto GetClassAttendanceReport(long studentProgramId, long batchId,
        //               DateTime startDate, DateTime endDate)
        //            {
        //                var sql = "declare @studentprogramid int=" + studentProgramId + ";declare @PrnNo nvarchar=(select PrnNo from studentprogram where id=" + studentProgramId + "); " +
        //" declare @RegisteredPresent int;set @RegisteredPresent=(select count(*) as RegisteredPresent from  [dbo].[StudentClassAttendanceDetails] where studentprogramid=" + studentProgramId + " and isregistered=1) " +
        //" declare @UnregisteredPresent int;set @UnregisteredPresent=(select count(*) as RegisteredPresent from  [dbo].[StudentClassAttendanceDetails] where studentprogramid=" + studentProgramId + " and isregistered=0) " +
        //" select * from (select @PrnNo PRN) as y,(select count(*) as LectureHeld from [dbo].[CourseProgress] where batchid=" + batchId + ")as a, " +
        //" (select @RegisteredPresent+@UnregisteredPresent LectureAttend ) as b,(select @RegisteredPresent RegisteredPresent) as c,(select @UnregisteredPresent UnregisteredPresent) as d," +
        //" (select count(distinct l.id) as RegisteredAbsent  from [dbo].[CourseProgress] cp inner join  [dbo].[Lecture] l on cp.lectureid=l.id inner join [dbo].[LectureSettings] ls on l.lecturesettingsid=ls.id inner join " +
        //" [dbo].[StudentCourseDetails] scd on scd.coursesubjectid=ls.coursesubjectid where studentprogramid=" + studentProgramId + " and scd.[status]=1 and cp.batchid= " + batchId +
        //" and l.id not in(select lectureid from [dbo].[StudentClassAttendanceDetails] scad " +
        //" inner join [dbo].[StudentClassAttendence] sca on scad.StudentClassAttendanceId=sca.id where studentprogramid=" + 10441 + ") and scd.creationdate<=cp.helddate) as e";
        //                IQuery iQuery = Session.CreateSQLQuery(sql);
        //                iQuery.SetResultTransformer(Transformers.AliasToBean<ClassAttendanceReportDto>());
        //                var attendanceReport = iQuery.List<ClassAttendanceReportDto>().SingleOrDefault();
        //                return attendanceReport;
        //            }
        //            public object ClassAttendanceCountByBatchLectureTeacherStudentProgram(long lectureId, long studentProgramId, long? teacherId = null, long? batchId = null, DateTime? heldDate = null, bool? returnCount = null)
        //            {
        //                ICriteria criteria = Session.CreateCriteria<StudentClassAttendence>();
        //                criteria.Add(Restrictions.Eq("Status", StudentClassAttendence.EntityStatus.Active));
        //                criteria.CreateAlias("StudentProgram", "sp");
        //                criteria.CreateAlias("Lecture", "l");
        //                criteria.Add(Restrictions.Eq("sp.Id", studentProgramId));
        //                if (batchId != null)
        //                {
        //                    criteria.CreateAlias("Batch", "b");
        //                    criteria.Add(Restrictions.Eq("b.Id", batchId));
        //                }
        //                if (heldDate != null)
        //                {
        //                    criteria.Add(Restrictions.Eq("HeldDate", heldDate));
        //                }
        //                criteria.Add(Restrictions.Eq("l.Id", lectureId));
        //                if (teacherId != null)
        //                {
        //                    criteria.CreateAlias("Teacher", "t");
        //                    criteria.Add(Restrictions.Eq("t.Id", teacherId));
        //                }
        //                if (returnCount == false)
        //                {
        //                    return criteria.List<StudentClassAttendence>().OrderByDescending(x => x.HeldDate).FirstOrDefault();
        //                }
        //                else
        //                {
        //                    criteria.SetProjection(Projections.RowCount());
        //                    var classAttendanceCount = Convert.ToInt32(criteria.UniqueResult());
        //                    return classAttendanceCount;
        //                }

        //            }
        #endregion

    }
}