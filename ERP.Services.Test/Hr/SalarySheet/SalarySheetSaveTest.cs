﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.SalarySheet
{
    [Trait("Area", "Hr")]
    [Trait("Service", "SalarySheet")]
    public class SalarySheetSaveTest : SalarySheetBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Successfully_Save()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);

            Assert.NotNull(_salarySheetService.LoadById(_salarySheetFactory.Object.Id));
        }

        [Fact]
        public void Should_Return_NullObject_Exception()
        {
            Assert.Throws<InvalidDataException>(() => _salarySheetService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Return_MessageException_For_TeamMember()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            //_holidaySettingFactory.Create().WithHolidayRepeatationType((int)HolidayRepeatationType.Once).WithHolidayType((int)HolidayWorkApprovalStatus.Half)
            //  .WithDateFromAndDateTo(holyDayDate, holyDayDate).WithOrganization(org).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create()
                //.WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_SalaryHistory()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            //_holidaySettingFactory.Create().WithHolidayRepeatationType((int)HolidayRepeatationType.Once).WithHolidayType((int)HolidayWorkApprovalStatus.Half)
            //  .WithDateFromAndDateTo(holyDayDate, holyDayDate).WithOrganization(org).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create()
                .WithTeamMember(tm)
                //.WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_SalaryOrganization()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                //.WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_SalaryBranch()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                //.WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_SalaryCampus()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                //.WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_SalaryDepartment()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                //.WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_SalaryDesignation()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                //.WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_JobOrganization()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                //.WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_JobBranch()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                //.WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_JobCampus()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                //.WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_JobDepartment()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                //.WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_JobDesignation()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                //.WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_InvaidDateException_For_empty_StartDate()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);
            _salarySheetFactory.Object.StartDate = new DateTime();

            Assert.Throws<InvalidDatePassedException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_InvaidDateException_For_empty_EndDate()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);
            _salarySheetFactory.Object.EndDate = new DateTime();
            Assert.Throws<InvalidDatePassedException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_InvaidDataException_For_invalid_Month()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);
            _salarySheetFactory.Object.Month = -1;
            Assert.Throws<InvalidDataException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_InvaidDataException_For_invalid_Year()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);
            _salarySheetFactory.Object.Year = -1;
            Assert.Throws<InvalidDataException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_NullObjectException_For_Details()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            //_holidaySettingFactory.Create().WithHolidayRepeatationType((int)HolidayRepeatationType.Once).WithHolidayType((int)HolidayWorkApprovalStatus.Half)
            //  .WithDateFromAndDateTo(holyDayDate, holyDayDate).WithOrganization(org).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create()
                .WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation);

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                //.WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<NullObjectException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_NullObjectException_For_Details_Null_SalarySheetObject()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            //_holidaySettingFactory.Create().WithHolidayRepeatationType((int)HolidayRepeatationType.Once).WithHolidayType((int)HolidayWorkApprovalStatus.Half)
            //  .WithDateFromAndDateTo(holyDayDate, holyDayDate).WithOrganization(org).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create()
                .WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation);

            _salarySheetDetailsFactory.Create()
                .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120);
            //.WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_NullObjectException_For_EBF()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            //_holidaySettingFactory.Create().WithHolidayRepeatationType((int)HolidayRepeatationType.Once).WithHolidayType((int)HolidayWorkApprovalStatus.Half)
            //  .WithDateFromAndDateTo(holyDayDate, holyDayDate).WithOrganization(org).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create()
                .WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation);

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList);
            //.WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<NullObjectException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_NullObjectException_For_Empty_SalarySheet_Of_EBF()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);

            //For holidaySettings
            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            //var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            //_holidaySettingFactory.Create().WithHolidayRepeatationType((int)HolidayRepeatationType.Once).WithHolidayType((int)HolidayWorkApprovalStatus.Half)
            //  .WithDateFromAndDateTo(holyDayDate, holyDayDate).WithOrganization(org).Persist();

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            _salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create()
                .WithTeamMember(tm)
                .WithSalaryHistory(_salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation);

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution,
                startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object);
            //.WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
            .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            Assert.Throws<MessageException>(() => _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList));
        }

        #endregion
        
    }
}