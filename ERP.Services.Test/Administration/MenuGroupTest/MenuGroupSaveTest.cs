﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MenuGroupTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "MenuGroup")]
    public class MenuGroupSaveTest : MenuGroupBaseTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _menuService.Save(menuGroup:null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var menuGroupFactory =
                _menugroupFactory.Create().WithNameAndRank(_name, 1).WithDisplayText(_displaytext)
                    .Persist();
            MenuGroup menuGroup = _menuService.LoadById(menuGroupFactory.Object.Id);
            Assert.NotNull(menuGroup);
            Assert.True(menuGroup.Id > 0);
        }
        
        //[Fact]
        //public void Save_Should_Check_Duplicate_Name_Entry()
        //{
        //    var menugroupFactory =
        //        _menugroupFactory.Create().WithNameAndRank(_name, 1).WithDisplayText(_displaytext)
        //            .Persist();
        //    var menugroupFactory2 =
        //        _menugroupFactory.Create().WithNameAndRank(_name, 1).WithDisplayText(_displaytext+"fgfghfh");
        //    Assert.Throws<DuplicateEntryException>(() => menugroupFactory2.Persist());
        //}
        //[Fact]
        //public void Save_Should_Check_Duplicate_Displaytext_Entry()
        //{
        //    var menugroupFactory =
        //       _menugroupFactory.Create().WithNameAndRank(_name, 1).WithDisplayText(_displaytext)
        //           .Persist();
        //    var menugroupFactory2 =
        //        _menugroupFactory.Create().WithNameAndRank(_name+" DFGDD", 1).WithDisplayText(_displaytext);
        //    Assert.Throws<DuplicateEntryException>(() => menugroupFactory2.Persist());
        //}
        #endregion

        #region Business Logic Test
        
        [Fact]
        public void Should_Not_Save_Empty_Name()
        {
            var menugroupFactory =
               _menugroupFactory.Create().WithNameAndRank("", 1).WithDisplayText(_displaytext);
            Assert.Throws<EmptyFieldException>(() => menugroupFactory.Persist());
        }
        [Fact]
        public void Should_Not_Save_Empty_Displaytext()
        {
            var menugroupFactory =
               _menugroupFactory.Create().WithNameAndRank(_name, 1).WithDisplayText("");
            Assert.Throws<EmptyFieldException>(() => menugroupFactory.Persist());
        }
        
        #endregion
    }
}
