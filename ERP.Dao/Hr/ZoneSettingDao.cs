﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IZoneSettingDao : IBaseDao<ZoneSetting, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<ZoneSetting> LoadZoneSetting(string orderBy, string direction, List<long> organizationId);
        IList<BusinessModel.Dto.Hr.TeamMemberZoneDto> TeamMemberZoneList(DateTime startDate, DateTime endTime, long teamMemberId, int employmentStatus, decimal salary,
            decimal basicSalary, int monthlyWorkingDay, long orgId);

        #endregion

        #region Others Function
        int GetZoneSettingRowCount(string orderBy, string direction, List<long> organizationId);
        bool CheckDublicateName(string name, long organizationId, long? id);
        bool CheckDublicateToleranceTime(int convertedtoleranceTime, long organizationId, long? id);
        #endregion

        #region Helper Function

        #endregion

        
    }
    public class ZoneSettingDao : BaseDao<ZoneSetting, long>, IZoneSettingDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<ZoneSetting> LoadZoneSetting(string orderBy, string direction, List<long> organizationId)
        {
            ICriteria criteria = GetShiftListCriteria(orderBy, direction, organizationId);
            //return criteria.SetFirstResult(start).SetMaxResults(length).List<HRShift>();
            return criteria.List<ZoneSetting>();
        }

        public IList<TeamMemberZoneDto> TeamMemberZoneList(DateTime startDate, DateTime endTime, long teamMemberId, int employmentStatus, decimal salary,
            decimal basicSalary, int monthlyWorkingDay, long orgId)
        {
            string query = @"
                            DECLARE @es AS INT,@salary AS DECIMAL,@basicSalary AS DECIMAL,@monthLyWorkingDay AS INT
                            Select @es = " + employmentStatus + @"
                            Select @salary = "+salary+@"
                            Select @basicSalary = " + basicSalary + @"
                            Select @monthLyWorkingDay = " + monthlyWorkingDay + @"
                            select a.ZoneSettingsId
	                            --,a.TeamMemberId	 	  	   
	                            ,ISNULL(
		                            CASE 
			                            WHEN (a.TotalToleranceDay - a.ToleranceDay)>=0 THEN (a.TotalToleranceDay - a.ToleranceDay)
			                            ELSE 0
			                            END
		                            ,0) as ZoneCount	    
	                            ,a.DeductionMultiplier	as ZoneDeductionMultiplier
	                            ,ISNULL( 
			                            CASE 
			                            WHEN @es =  1 or @es =  3 or @es =  4 or @es =  5 THEN CONVERT(DECIMAL(18,2),(@salary/@monthLyWorkingDay)*a.DeductionMultiplier)
									                                          
			                            WHEN @es =  2 THEN  CONVERT(DECIMAL(18,2),(((@basicSalary*@salary)/100)/@monthLyWorkingDay)*a.DeductionMultiplier)
			                           
			                            else 0
			                            END,0) as ZoneUnitAmount
                            from (
		                            select count(ISNULL(a.ToleranceDay,0)) as TotalToleranceDay
			                            ,ISNULL(a.ToleranceDay,0) as ToleranceDay
			                            ,CONVERT(DECIMAL(18,2),a.DeductionMultiplier) as DeductionMultiplier
			                            ,a.TeamMemberId,a.Id as ZoneSettingsId
		                            from(
			                            select zs.Id,zs.ToleranceDay,zs.ToleranceTime, zs.DeductionMultiplier,ats.TeamMemberId
			                            from HR_ZoneSetting as zs
			                            inner join HR_AttendanceSummary as ats on ats.ZoneId=zs.Id
			                            where zs.OrganizationId=" + orgId + @" and 
			                            (ats.AttendanceDate>='" + startDate + @"' and ats.AttendanceDate<='" + endTime + @"')
			                            and zs.DeductionMultiplier is not null
			                            and zs.Status = 1
			                            and ats.Status = 1
   
		                            ) as a group by a.TeamMemberId, a.Id,a.DeductionMultiplier,a.ToleranceDay
	                            ) as a where a.TeamMemberId = " + teamMemberId + "";

            var list = Session.CreateSQLQuery(query).SetResultTransformer(Transformers.AliasToBean<TeamMemberZoneDto>());
            return list.List<TeamMemberZoneDto>().ToList();
        }

        #endregion

        #region Others Function



        public int GetZoneSettingRowCount(string orderBy, string direction, List<long> organizationId)
        {
            ICriteria criteria = GetShiftListCriteria(orderBy, direction, organizationId, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }
        public bool CheckDublicateName(string name, long organizationId, long? id)
        {
            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<ZoneSetting>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", ZoneSetting.EntityStatus.Delete)));
            if (id != null)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (name != null)
            {
                criteria.Add(Restrictions.Eq("Name", name));
            }
            criteria.Add(Restrictions.Eq("Organization.Id", organizationId));

            var hrZoneSettingList = criteria.List<ZoneSetting>();
            if (hrZoneSettingList != null && hrZoneSettingList.Count > 0)
            {

                checkValue = true;
            }
            return checkValue;
        }
        public bool CheckDublicateToleranceTime(int convertedtoleranceTime, long organizationId, long? id)
        {
            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<ZoneSetting>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", ZoneSetting.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("ToleranceTime", convertedtoleranceTime));
            criteria.Add(Restrictions.Eq("Organization.Id", organizationId));
            if (id != null)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            var hrZoneSettingList = criteria.List<ZoneSetting>();
            if (hrZoneSettingList != null && hrZoneSettingList.Count > 0)
            {
                checkValue = true;
            }
            return checkValue;
        }
        #endregion

        #region Helper Function
        private ICriteria GetShiftListCriteria(string orderBy, string direction, List<long> organizationId, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<ZoneSetting>().Add(Restrictions.Not(Restrictions.Eq("Status", ZoneSetting.EntityStatus.Delete)));

            if (organizationId != null)
            {
                criteria.Add(Restrictions.In("Organization.Id", organizationId));
            }

            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    if (orderBy == "Organization.ShortName")
                    {
                        criteria.CreateAlias("Organization", "Organization");
                    }
                    //if (orderBy == "ShoftDuration")
                    //{
                    //    criteria.AddOrder(direction == "ASC" ? Order.Asc("StartTime") : Order.Desc("EndTime"));
                    //}
                    else
                    {
                        criteria.AddOrder(direction == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                    }
                }
            }

            return criteria;
        }



        #endregion
    }

}
