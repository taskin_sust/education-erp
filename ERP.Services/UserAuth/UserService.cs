﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.Dao.UserAuth;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.MediaServices;

namespace UdvashERP.Services.UserAuth
{
    public interface IUserService : IBaseService
    {
        #region Operational Function
        void Save(UserProfile erpUser, Images imgObj);
        void Update(long id, UserProfile erpUser, Images imgObj);
        #endregion

        #region Single Instances Loading Function
        UserProfile LoadById(long id);
        UserProfile LoadByAspNetUser(AspNetUser userObj, bool loadActive = true);
        UserProfile GetByAspNetUser(long aspnetUserId);
        AspNetUser LoadbyAspNetUserId(long id);
        AspNetUser LoadAspNetUserById(long id);
        UserProfile LoadByAspNetUserWithoutStatus(AspNetUser basicUser);
        #endregion

        #region List Loading Function

        IList<UserProfile> LoadUser(long[] branchIds=null,long[] campusIds=null);
        IList<UserProfile> LoadActive(IList<long> idList = null, bool eagerLoadAspnetUser = false);
        IList<Branch> LoadAuthorizedBranchByProgram(long[] authorizedProgramLists, bool allProgram, long[] authorizedBranchLists, bool allBranch);
        IList<AspNetUser> LoadAuthorizedUserBybranch(int start, int length, string orderBy, string orderDir, string userName, string contactNo, long? branchId, string status
            , long? organizationId, string rank, List<long> authorizedProgramLists, List<long> authorizedBranchLists);
        IList<UserProfile> LoadAuthorizedUser(List<UserMenu> userMenu, bool? userStatus = null, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> campusIdList = null);
        List<UserProfile> LoadUserProfileListByDepartmentBranch(List<UserMenu> authorizeMenu, List<long> branchIdList = null, List<long> departmentIdList = null, List<long> campusIdList = null);
        //x
        IList<UserProfile> LoadActiveAuthorizedUserByBranch(List<long> programIdList, List<long> branchIdList);


        IList<UserProfile> LoadActiveAuthorizedUserByBranch(List<UserMenu> authorizeMenu);
        IList<AspNetUser> LoadUsers(List<UserMenu> userMenu, long[] organizationIds, long[] programIds, long[] sessionIds, long[] courseIds, long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo);
        IList<AspNetUser> LoadUsers(List<UserMenu> _userMenu, long[] organizationIds, long[] programIds, long[] sessionIds, long[] courseIds, long[] examIdList, DateTime dateFrom, DateTime dateTo);
       
        #endregion

        #region Others Function
        int GetMaxRank(AspNetUser userObj);
        bool HasDuplicateByContactNo(string contactNo);
        bool HasDuplicateByContactNo(string contactNo, long id);
        bool HasDuplicateByEmail(string email, long id);
        string GetUserNameByAspNetUserId(long id);
        int RowCountAuthorizedUserBybranch(string orderBy, string orderDir, string userName, string contactNo, long? branchId, string status, long? organizationId, string rank, List<long> authorizedProgramLists, List<long> authorizedBranchLists);
        #endregion

    }
    public class UserService : BaseService, IUserService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UserAuthService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IUserProfileDao _userDao;
        private IAspNetUserDao _aspNetUserDao;
        private readonly IImagesDao _imagesDao;
        //private readonly IEmployeeImageMediaService _employeeImageMediaService;
        private ISession mediaSession = null;
        private readonly IEmployeeImageMediaService _employeeImageMediaService;
        private readonly IDepartmentDao _departmentDao;
        private readonly ITeamMemberDao _teamMemberDao;
        public UserService(ISession session)
        {
            mediaSession = NHibernateMediaSessionFactory.OpenSession();
            Session = session;
            _userDao = new UserProfileDao() { Session = Session };
            _aspNetUserDao = new AspNetUserDao() { Session = Session };
            _imagesDao = new ImagesDao() { Session = Session };
            _employeeImageMediaService = new EmployeeImageMediaService();
            //_employeeImageMediaService = new EmployeeImageMediaService();
            _departmentDao = new DepartmentDao { Session = Session };
            _teamMemberDao = new TeamMemberDao { Session = Session };
        }
        #endregion

        #region Operational Function
        public void Save(UserProfile erpUser, Images imgObj)
        {
            ITransaction transaction = null;
            EmployeeMediaImage employeeImages = new EmployeeMediaImage();
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    _userDao.Save(erpUser);
                    Images userImg = new Images();
                    //userImg.UserImages = imgObj.UserImages;
                    //userImg.GuidId = imgObj.GuidId;
                    userImg.User = erpUser;
                    userImg.Name = imgObj.Name;
                    
                    userImg.Status = Images.EntityStatus.Active;
                    userImg.CreateBy = GetCurrentUserId();
                    userImg.ModifyBy = GetCurrentUserId();

                    _imagesDao.Save(userImg);
                    {
                        var mediaTrans = mediaSession.BeginTransaction();
                        employeeImages.ImageGuid = Guid.NewGuid();
                        employeeImages.Images = imgObj.UserImages;
                        employeeImages.ImageRef = userImg.Id;
                        employeeImages.Status = 1;
                        mediaSession.Save(employeeImages);
                        mediaTrans.Commit();
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        public void Update(long id, UserProfile userObj, Images imgObj)
        {
            ITransaction transaction = null;
            EmployeeMediaImage employeeImages = new EmployeeMediaImage();
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    var tempUser = _userDao.LoadById(id);
                    tempUser.Id = id;
                    tempUser.Branch = userObj.Branch;
                    tempUser.Campus = userObj.Campus;
                    tempUser.NickName = userObj.NickName;
                    tempUser.Status = userObj.Status;
                    if (imgObj != null)
                    {
                        tempUser.UserImages.Clear();
                        //tempUser.UserImages.Add(imgObj);
                        _userDao.Update(tempUser);
                        Images userimage = new Images();
                        //userimage.GuidId = Guid.NewGuid();
                        userimage.Hash = imgObj.Hash;
                        //userimage.UserImages = imgObj.UserImages;
                        userimage.User = tempUser;

                        userimage.Status = Images.EntityStatus.Active;
                        userimage.CreateBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                        userimage.ModifyBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());


                        _imagesDao.Save(userimage);
                        {
                            var mediaTrans = mediaSession.BeginTransaction();
                            employeeImages.ImageGuid = Guid.NewGuid();
                            employeeImages.Images = imgObj.UserImages;
                            employeeImages.ImageRef = userimage.Id;
                            employeeImages.Status = 1;
                            mediaSession.Save(employeeImages);
                            mediaTrans.Commit();
                        }
                    }
                    else
                    {
                        _userDao.Update(tempUser);
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        #endregion

        #region Single Instances Loading Function
        public UserProfile LoadByAspNetUserWithoutStatus(AspNetUser basicUser)
        {
            try
            {
                return _userDao.LoadByAspNetUserWithoutStatus(basicUser);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public UserProfile LoadById(long id)
        {
            try
            {
                return _userDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public AspNetUser LoadbyAspNetUserId(long id)
        {
            try
            {
                return _aspNetUserDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public UserProfile LoadByAspNetUser(AspNetUser userObj, bool loadActive = true)
        {
            try
            {
                return _userDao.LoadByAspNetUser(userObj, loadActive);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public UserProfile GetByAspNetUser(long aspnetUserId)
        {
            try
            {
                return _userDao.GetByAspNetUser(aspnetUserId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public AspNetUser LoadAspNetUserById(long id)
        {
            try
            {
                return _aspNetUserDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function
        public IList<UserProfile> LoadUser(long[] branchIds = null, long[] campusIds = null)
        {
            return _userDao.LoadUser(branchIds,campusIds);
        }
        public IList<Branch> LoadAuthorizedBranchByProgram(long[] authorizedProgramLists, bool allProgram, long[] authorizedBranchLists, bool allBranch)
        {
            try
            {
                return _userDao.LoadAuthorizedBranchByProgram(authorizedProgramLists, allProgram, authorizedBranchLists, allBranch);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<AspNetUser> LoadAuthorizedUserBybranch(int start, int length, string orderBy, string orderDir, string userName, string contactNo
            , long? branchId, string status, long? organizationId, string rank, List<long> authorizedProgramLists, List<long> authorizedBranchLists)
        {
            try
            {
                return _aspNetUserDao.LoadAuthorizedUserBybranch(start, length, orderBy, orderDir, userName, contactNo, branchId, status, organizationId, rank, authorizedProgramLists, authorizedBranchLists);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<UserProfile> LoadAuthorizedUser(List<UserMenu> userMenus, bool? userStatus = null, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> campusIdList = null)
        {
            try
            {
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(programIdList), CommonHelper.ConvertSelectedAllIdList(branchIdList));

                return _userDao.LoadAuthorizedUser(userStatus, authBranchIdList, campusIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public List<UserProfile> LoadUserProfileListByDepartmentBranch(List<UserMenu> authorizeMenu, List<long> branchIdList = null, List<long> departmentIdList = null, List<long> campusIdList = null)
        {
            try
            {
                List<long> autorizedOrganizationIdList = AuthHelper.LoadOrganizationIdList(authorizeMenu);
                List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(authorizeMenu, autorizedOrganizationIdList, null, (branchIdList!=null && !branchIdList.Contains(SelectionType.SelelectAll)) ? branchIdList : null);
                List<long> aothorizedDepartmentIdList = _departmentDao.LoadHrDepartmentList(autorizedOrganizationIdList).Select(x=>x.Id).ToList();
                List<long> departmentIdListForDao = new List<long>();
                List<long> userProfileIdList = new List<long>();
                if (departmentIdList != null && !departmentIdList.Contains(SelectionType.SelelectAll) && aothorizedDepartmentIdList.Any())
                {
                    departmentIdListForDao.AddRange(departmentIdList.Where(departemntId => aothorizedDepartmentIdList.Contains(departemntId)));
                    userProfileIdList = _teamMemberDao.LoadHrAuthorizedTeamMember(null, authorizedBranchIdList, null, departmentIdListForDao).Where(x => x.UserProfile != null).Select(x => x.UserProfile.Id).ToList();
                    if(!userProfileIdList.Any())
                        userProfileIdList.Add(0);
                }

                List<UserProfile> userProfileList = _userDao.LoadUserProfileListByDepartmentBranch(authorizedBranchIdList, userProfileIdList, campusIdList);
                return userProfileList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        //x
        public IList<UserProfile> LoadActiveAuthorizedUserByBranch(List<long> programIdList, List<long> branchIdList)
        {
            try
            {
                return _userDao.LoadActiveAuthorizedUserByBranch(programIdList, branchIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public IList<UserProfile> LoadActive(IList<long> idList = null, bool eagerLoadAspnetUser = false)
        {
            try
            {
                return _userDao.LoadActive(idList, eagerLoadAspnetUser);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<UserProfile> LoadLike(string email)
        {
            try
            {
                return _userDao.LoadLike(email);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public IList<UserProfile> LoadActiveAuthorizedUserByBranch(List<UserMenu> authorizeMenu)
        {
            try
            {
                List<long> branchList = AuthHelper.LoadBranchIdList(authorizeMenu);
                return _userDao.LoadActiveAuthorizedUserByBranch(branchList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<AspNetUser> LoadUsers(List<UserMenu> userMenu, long[] organizationIds, long[] programIds, long[] sessionIds,
            long[] courseIds, long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo)
        {
            List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu,organizationIds.ToList());
            return _userDao.LoadUsers(authProgramIdList, programIds, sessionIds, courseIds, lectureIdList, teacherIdList, dateFrom, dateTo);
        }

        public IList<AspNetUser> LoadUsers(List<UserMenu> _userMenu, long[] organizationIds, long[] programIds,
            long[] sessionIds, long[] courseIds, long[] examIdList, DateTime dateFrom, DateTime dateTo)
        {
            List<long> authProgramIdList = AuthHelper.LoadProgramIdList(_userMenu, organizationIds.ToList());
            return _userDao.LoadUsers(authProgramIdList, programIds, sessionIds, courseIds, examIdList, dateFrom, dateTo);
        }
        #endregion

        #region Others Function
        public int RowCountAuthorizedUserBybranch(string orderBy, string orderDir, string userName, string contactNo, long? branchId, string status, long? organizationId, string rank
            , List<long> authorizedProgramLists, List<long> authorizedBranchLists)
        {
            try
            {
                return _aspNetUserDao.RowCountAuthorizedUserBybranch(orderBy, orderDir, userName, contactNo, branchId, status, organizationId, rank, authorizedProgramLists, authorizedBranchLists);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public bool HasDuplicateByContactNo(string contactNo)
        {
            try
            {
                return _aspNetUserDao.HasDuplicateByContactNo(contactNo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public bool HasDuplicateByContactNo(string contactNo, long id)
        {
            try
            {
                return _aspNetUserDao.HasDuplicateByContactNo(contactNo, id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool HasDuplicateByEmail(string email, long id)
        {
            try
            {
                return _userDao.HasDuplicateByEmail(email, id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public int GetMaxRank(AspNetUser userObj)
        {
            try
            {
                return _aspNetUserDao.GetMaximumRank(userObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public string GetUserNameByAspNetUserId(long id)
        {
            try
            {
                var aspNetUser = _aspNetUserDao.LoadById(id);
                return aspNetUser != null ? aspNetUser.UserName : "";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion
    }
}
