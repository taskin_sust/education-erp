﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.LoanManagementTest
{

    [Trait("Area", "Hr")]
    [Trait("Service", "MemberLoanApplication")]

    public class LoanManagementLoadTest : LoanManagementBaseTest
    {
        #region List Load Test

        [Fact]
        public void Should_Return_invalid_MenuPermission()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object);

            Assert.Throws<AuthenticationException>(() => _memberLoanApplicationService.LoadMemberLoanApplication(0, int.MaxValue, null));
        }

        [Fact]
        public void Should_Return_List_Successfully()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object);
            var userMenu = BuildUserMenu(new List<Organization>() { teamMember.EmploymentHistory[0].Department.Organization });

            var list = _memberLoanApplicationService.LoadMemberLoanApplication(0, int.MaxValue, userMenu);
            Assert.True(list.Count > 0);
        }
        #endregion

        #region Count Test

        [Fact]
        public void Should_Return_Invalid_MenuPermission()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object);
            var userMenu =
                BuildUserMenu(new List<Organization>() { teamMember.EmploymentHistory[0].Department.Organization });
            Assert.Throws<AuthenticationException>(() => _memberLoanApplicationService.GetMemberLoanApplicationCount(null));
        }

        [Fact]
        public void Should_Return_Count_Successfully()
        {
            var teamMember = PersistTeamMember();
            _memberLoanApplicationFactory.Create();

            _memberLoanCsrFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanCsr = new List<MemberLoanCsr>() { _memberLoanCsrFactory.Object };

            _memberLoanZakatFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoanZakat = new List<MemberLoanZakat>() { _memberLoanZakatFactory.Object };

            _memberLoanFactory.Create().WithMemberLoanApplication(_memberLoanApplicationFactory.Object);
            _memberLoanApplicationFactory.Object.MemberLoan = new List<MemberLoan>() { _memberLoanFactory.Object };

            _memberLoanApplicationFactory.Object.TeamMember = teamMember;
            _memberLoanApplicationService.SaveOrUpdate(_memberLoanApplicationFactory.Object);
            var userMenu = BuildUserMenu(new List<Organization>() { teamMember.EmploymentHistory[0].Department.Organization });

            Assert.Equal(1,
                _memberLoanApplicationService.GetMemberLoanApplicationCount(userMenu, teamMember.Pin, teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.EmploymentHistory[0].Department.Id, teamMember.EmploymentHistory[0].Campus.Branch.Id));
        }

        #endregion
    }
}
