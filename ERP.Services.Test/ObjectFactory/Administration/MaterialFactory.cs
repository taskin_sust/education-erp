﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class MaterialFactory : ObjectFactoryBase<Material>
    {
        #region Object Initialization
        private MaterialService _materialService;
        private OrganizationFactory _organizationFactory;
        private ProgramFactory _programFactory;
        private SessionFactory _sessionFactory;
        private MaterialTypeFactory _materialTypeFactory;
        private TestBaseService<Material> _materialTestBaseService;
        public MaterialFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _materialService = new MaterialService(session);
            _organizationFactory = new OrganizationFactory(this, session);
            _programFactory = new ProgramFactory(this, session);
            _sessionFactory = new SessionFactory(this, session);
            _materialTypeFactory = new MaterialTypeFactory(this, session);
        }

        #endregion

        #region Create

        public MaterialFactory Create()
        {
            var programFactory =
                _programFactory.Create()
                    .WithOrganization()
                    .WithProperties(Guid.NewGuid().ToString().Substring(0, 15) + "testProgramShortName", _programFactory.GetRandomNumber(),
                     Program.ProgramTypeStatus.Paid)
                    .Persist();
            Organization organization = programFactory.Object.Organization;
            var sessionFactory =
                _sessionFactory.Create()
                    .WithNameAndRank(Guid.NewGuid().ToString().Substring(0, 15) + "testSessionName", 1)
                    .WithCode(_sessionFactory.GetRandomNumber())
                    .Persist();
            var materialType = _materialTypeFactory.Create().WithOrganization(organization).Persist();
            Object = new Material()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid().ToString().Substring(0, 15) + "_test material name",
                ShortName = Guid.NewGuid().ToString().Substring(0, 15) + "_test material shortname",
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Material.EntityStatus.Active,
                Organization = organization,
                Program = programFactory.Object,
                Session = sessionFactory.Object,
                MaterialType = materialType.Object
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public MaterialFactory Create(Organization organization,Program program,Session session,MaterialType materialType)
        {
            
            Object = new Material()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid().ToString().Substring(0, 15) + "_test material name",
                ShortName = Guid.NewGuid().ToString().Substring(0, 15) + "_test material shortname",
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Material.EntityStatus.Active,
                Organization = organization,
                Program = program,
                Session = session,
                MaterialType = materialType
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public MaterialFactory WithRank(int rank)
        {
            Object.Rank = rank;
            return this;
        }

        public MaterialFactory CreateMore(int numberOfMaterials)
        {
            CreateMaterialList(numberOfMaterials);
            return this;
        }
        public MaterialFactory CreateMore(List<Material> materilaList)
        {
            CreateMaterialList(materilaList);
            return this;
        }

        private List<Material> CreateMaterialList(List<Material> materialList)
        {
            ListStartIndex = ObjectList.Count;
            var list = materialList.ToList();
            ObjectList.AddRange(list);
            return list;
        }

        private List<Material> CreateMaterialList(int numberOfMaterials)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Material>();
            var programFactory =
                _programFactory.Create()
                    .WithOrganization()
                    .WithProperties(Guid.NewGuid().ToString().Substring(0, 15) + "testProgramShortName", _programFactory.GetRandomNumber(),
                     Program.ProgramTypeStatus.Paid)
                    .Persist();
            Organization organization = programFactory.Object.Organization;
            var sessionFactory =
                _sessionFactory.Create()
                    .WithNameAndRank(Guid.NewGuid().ToString().Substring(0, 15) + "testSessionName", 1)
                //.WithCode(Guid.NewGuid() + "testSessionCode")
                    .Persist();
            var materialType = _materialTypeFactory.Create().WithOrganization(organization).Persist();
            for (int i = 0; i < numberOfMaterials; i++)
            {
                var batch = new Material()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Material.EntityStatus.Active,
                    Name = Guid.NewGuid().ToString().Substring(0, 15) + "_test material name" + i,
                    ShortName = Guid.NewGuid().ToString().Substring(0, 15) + "_test material shortname" + i,
                    Organization = organization,
                    Program = programFactory.Object,
                    Session = sessionFactory.Object,
                    MaterialType = materialType.Object
                };
                list.Add(batch);
            }
            ObjectList.AddRange(list);
            return list;
        }
        
        public MaterialFactory Persist()
        {
            if (ObjectList != null)
            {
                var objectListToSave = ObjectList.Where(x => x.Id == 0).ToList();
                if (objectListToSave.Count>0)
                {
                    _materialService.MaterialSaveOrUpdate(objectListToSave); 
                }
            }

            if (SingleObjectList != null)
            {
                var objectListToSave = SingleObjectList.Where(x => x.Id == 0).ToList();
                if (objectListToSave.Count>0)
                {
                    _materialService.MaterialSaveOrUpdate(objectListToSave); 
                }
            }
            return this;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void Cleanup()
        {
            DeleteAll();
            _materialTypeFactory.Cleanup();
            _programFactory.Cleanup();
            _sessionFactory.Cleanup();
            _organizationFactory.Cleanup();
        }
        #endregion

    }
}
