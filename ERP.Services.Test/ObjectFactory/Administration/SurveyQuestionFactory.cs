﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
   public class SurveyQuestionFactory : ObjectFactoryBase<SurveyQuestion>
    {
        #region Object Initialization

        ISurveyQuestionService _surveyQuestionService; 
        public SurveyQuestionFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _surveyQuestionService = new SurveyQuestionService(_session);
        }

        #endregion

        #region Create
        public SurveyQuestionFactory Create()
        {
            Object = CreateObject(
            "Survey_Question_"+Guid.NewGuid().ToString().Substring(0, 6),10
            );
            SingleObjectList.Add(Object);
            return this;
        }
        private SurveyQuestion CreateObject(string question, int rank)
        {
            var obj = new SurveyQuestion()
            {
                Id = 0,
                Name = "Question",
                Question = question,
                Rank = rank,
                CreateBy = 1,
                ModifyBy = 1,
                Status = SurveyQuestion.EntityStatus.Active,
                BusinessId = GetPcUserName()
            };
            return obj;
        }
        public SurveyQuestionFactory WithQuestionAndRank(string question, int rank) 
        {
            Object.Question = question;
            Object.Rank = rank;
            return this;
        }
        public SurveyQuestionFactory WithAnswer(List<SurveyQuestionAnswer> answerList)  
        {
            Object.SurveyQuestionAnswers = answerList; 
            return this;
        }

        public SurveyQuestionFactory CreateMore(int numberOfQuestion, List<SurveyQuestionAnswer> answerList, string questionPrefix) 
       {
           ListStartIndex = ObjectList.Count;
            var answerArray = answerList.ToArray();
            int j = 0;
           for (int i = 0; i < numberOfQuestion; i++)
            {
                var question = questionPrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss")+"_"+i;
                var surveyQuestion = CreateObject(question, i + 1);
                var newAnswerList = new List<SurveyQuestionAnswer>();

                    var answer = answerArray[j++];
                   // var newObj = (SurveyQuestionAnswer)answer.Clone(); 
                    var newObj = answer; 
                    newObj.SurveyQuestion = surveyQuestion;
                    newAnswerList.Add(newObj);

                    var answer2 = answerArray[j++];
                  //  var newObj2 = (SurveyQuestionAnswer)answer2.Clone();
                    var newObj2 = answer2;
                    newObj2.SurveyQuestion = surveyQuestion;
                    newAnswerList.Add(newObj2);

                //foreach (var answer in answerList)
                //{
                //    var newObj = (SurveyQuestionAnswer)answer.Clone(); 
                //    newObj.SurveyQuestion = surveyQuestion;
                //    newAnswerList.Add(newObj);
                //}
                surveyQuestion.SurveyQuestionAnswers = newAnswerList;
                ObjectList.Add(surveyQuestion);
            }
            return this;
       }

        public SurveyQuestionFactory CreateMore(int numberOfQuestion)
        {
            var questionPrefix = Guid.NewGuid().ToString().Substring(0, 6);
            ListStartIndex = ObjectList.Count;
            for (int i = 0; i < numberOfQuestion; i++)
            {
                var question = questionPrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + i;
                var surveyQuestion = CreateObject(question, i+1);
                ObjectList.Add(surveyQuestion);
            }
            return this;
        }

        public SurveyQuestionFactory Persist()
        {
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x =>
                {
                    if (x.Id == 0)
                        _surveyQuestionService.Save(x);
                });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x =>
                {
                    if (x.Id == 0)
                        _surveyQuestionService.Save(x);
                });
            }
            return this;
        }
        #endregion

        #region Others
        
        #endregion

        #region Cleanup

        public void Cleanup()
       {
           DeleteAll();
       }

       #endregion
    }
       
}
