﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class DailyAllowanceController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IAllowanceService _hrAllowanceService;
        private readonly IOrganizationService _organizationService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IMentorHistoryService _mentorHistoryService;
        private readonly ICommonHelper _commonHelper;
        private readonly IUserService _userService;
        public DailyAllowanceController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _hrAllowanceService = new AllowanceService(session);          
            _organizationService = new OrganizationService(session);
            _teamMemberService = new TeamMemberService(session);
            _mentorHistoryService = new MentorHistoryService(session);
            _commonHelper = new CommonHelper();
            _userService = new UserService(session);
        }

        #endregion

        #region Daily Allowance Approval By Mentor

        public ActionResult DailyAllowance()
        {
            return View();
        }

        public ActionResult DailyAllowanceApproval()
        {
            ViewBag.ShowNoData = false;
            return View();
        }

        [HttpPost]
        public ActionResult DailyAllowance(DailyAllowanceFormViewModel data )
        {
            ViewBag.ShowNodata = true;
            try
            {
                if (ModelState.IsValid)
                {
                    var pinList = (!String.IsNullOrEmpty(data.PinList)) ? data.PinList.Trim() : "";
                    var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    pinList = regex.Replace(pinList, @",");

                    var mentor = _teamMemberService.GetCurrentMember();
                    var memberPinList = pinList.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList();
                    var mentorTeamMemberIdList = _mentorHistoryService.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now, null, memberPinList);
                    if (!mentorTeamMemberIdList.Any())
                    {
                        mentorTeamMemberIdList.Add(0);
                    }
                    IList<DailyAllowanceDto> dailyAllowanceList =_hrAllowanceService.LoadDailyAllowance(data.Date, data.Date, mentorTeamMemberIdList);
                    ViewBag.DailyAllowanceList = dailyAllowanceList;
                }
                else
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
           
            return View();
        }

        //public ActionResult DailyAllowanceSaveByMentor(string date, string pinList, string allowValues, string disallowValues) 
        //{
        //    try
        //    {
        //        var userMenu = (List<UserMenu>) ViewBag.UserMenu;
        //        _hrAllowanceService.Save(userMenu, date, pinList.Trim(), allowValues, disallowValues, true);
        //        return Json(new Response(true, "Daily Allowance Approval Successful"));
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
        //    }
        //}
        public ActionResult DailyAllowanceSaveByMentor(string date, string pinList, string allowValues, string disallowValues)
        {
            try
            {
                _hrAllowanceService.Save(date, pinList.Trim(), allowValues, disallowValues);
                return Json(new Response(true, "Daily Allowance Approval Successful"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }
        
        #endregion

        #region Daily Allowance By HR

        public ActionResult DailyAllowanceByHr()
        {
            return View();
        }

        public ActionResult DailyAllowanceByHrApproval()
        {
            ViewBag.ShowNoData = false;
            return View();
        }
        
        [HttpPost]
        public ActionResult DailyAllowanceByHr(DailyAllowanceFormViewModel data)  
        {
            ViewBag.ShowNodata = true;
            try
            {
                if (ModelState.IsValid)
                {
                    var pinList = (!String.IsNullOrEmpty(data.PinList)) ? data.PinList.Trim() : "";
                    var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    pinList = regex.Replace(pinList, @",");
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var mentorTeamMemberIdList = new List<long>();

                    IList<DailyAllowanceDto> dailyAllowanceList = _hrAllowanceService.LoadDailyAllowance(data.Date, data.Date, mentorTeamMemberIdList, userMenu, pinList);
                    ViewBag.DailyAllowanceList = dailyAllowanceList;
                }
                else
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            
            return View();
        }

        //public ActionResult DailyAllowanceSaveByHr(string date, string pinList, string allowValues, string disallowValues)
        //{
        //    try
        //    {
        //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        _hrAllowanceService.Save(userMenu, date, pinList.Trim(), allowValues, disallowValues);
        //        return Json(new Response(true, "Daily Allowance Approval Successful"));
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
        //    }
        //}
        public ActionResult DailyAllowanceSaveByHr(string date, string pinList, string allowValues, string disallowValues)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                _hrAllowanceService.Save(date, pinList.Trim(), allowValues, disallowValues);
                return Json(new Response(true, "Daily Allowance Approval Successful"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Daily Allowance History

        public ActionResult DailyAllowanceHistory()
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                SelectList organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.organizationlist = organization;
                ViewBag.BranchId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.CampusId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.DepartmentId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.dateFromField = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
                ViewBag.dateToField = DateTime.Now.ToString("yyyy-MM-dd");

                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;

                return PartialView("_DailyAllowanceHistory");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

         [HttpPost]
        public ActionResult LoadDailyAllowanceHistory(int draw, int start, int length, long? organizationId, long? departmentId, long? branchId, long? campusId, string pin, string dateFrom, string dateTo)
        { 
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";

                    DateTime dateFromDateTime = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).Date;
                    DateTime dateToDateTime = DateTime.Today.Date;
                    if (!String.IsNullOrEmpty(dateFrom))
                    {
                        dateFromDateTime = Convert.ToDateTime(dateFrom).Date;
                    }
                    if (!String.IsNullOrEmpty(dateTo))
                    {
                        dateToDateTime = Convert.ToDateTime(dateTo).Date;
                    }

                    if (dateFromDateTime > dateToDateTime)
                    {
                        DateTime temp = dateToDateTime;
                        dateToDateTime = dateFromDateTime;
                        dateFromDateTime = temp;
                    }
                    var mentorTeamMemberIdList = new List<long>();

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = "Pin";
                        orderDir = nvc["order[0][dir]"];
                    }
                    #endregion

                    int recordsTotal = _hrAllowanceService.DailyAllowanceRowCount(dateFromDateTime, dateToDateTime, userMenu, mentorTeamMemberIdList, pin.Trim(), organizationId, branchId, campusId, departmentId, true);
                    long recordsFiltered = recordsTotal;
                    IList<DailyAllowanceDto> dataList = _hrAllowanceService.LoadDailyAllowance(dateFromDateTime, dateToDateTime, mentorTeamMemberIdList, userMenu, pin, organizationId, branchId, campusId, departmentId, start, length, orderBy, orderDir, true);
                   var data = new List<object>();
                   foreach (var c in dataList)
                   {
                       var str = new List<string>();
                       str.Add(c.Pin.ToString());
                       str.Add(c.Name);
                       str.Add(c.Department);
                       str.Add(c.Organization);
                       str.Add(c.Branch);
                       str.Add(c.Campus);
                       str.Add(c.DailyAllowanceDate.ToString("yyyy-MM-dd"));
                       str.Add(c.TimeFrom.ToString("hh:mm tt"));
                       str.Add(c.TimeTo.ToString("hh:mm tt"));
                       str.Add(c.ModifyBy != null ? _userService.GetUserNameByAspNetUserId((long)c.ModifyBy) : "");
                       str.Add(CommonHelper.GetReportCommonDateTimeFormat(c.LastModificationDate));
                       if (c.IsApproved == true)
                       {
                           str.Add("<a id='" + c.Id.ToString() + "' href='#' data-pin='" + c.Pin.ToString() + "' data-date='" + c.DailyAllowanceDate + "' class='glyphicon glyphicon-ok'> </a>");
                       }
                       else
                       {
                           str.Add("<a id='" + c.Id.ToString() + "' href='#' data-pin='" + c.Pin.ToString() + "' data-date='" + c.DailyAllowanceDate + "' class='glyphicon glyphicon-ban-circle'> </a>");
                       }
                       //(c.IsApproved != null && c.IsApproved == true) ? "Approved" : "Disapproved"
                       data.Add(str);
                   }
                   return Json(new
                   {
                       draw = draw,
                       recordsTotal = recordsTotal,
                       recordsFiltered = recordsFiltered,
                       start = start,
                       length = length,
                       data = data
                   });
                    
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return HttpNotFound();
        }
       
        #endregion

        #region Update Operation

         [HttpPost]
         public ActionResult DailyAllowanceUpdate(string pin, DateTime date, bool key)
         {
             try
             {
                 var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                 var hrAllowance = _hrAllowanceService.GetAllowance(pin, Convert.ToDateTime(date));
                 if (hrAllowance != null)
                 {
                     hrAllowance.IsApproved = key;
                     _hrAllowanceService.Update(hrAllowance);
                     return Json(new Response(true, "Daily Allowance Approval Successful"));
                 }
                 else
                 {
                     return Json(new Response(true, "Daily Allowance Approval Fail"));
                 }
             }

             catch (Exception ex)
             {
                 _logger.Error(ex);
                 return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
             }
         }

         #endregion

        #region Helper function
         #endregion

         // GET: HR/DailyAllowance
        public ActionResult Index()
        {
            return View();
        }
    }
}