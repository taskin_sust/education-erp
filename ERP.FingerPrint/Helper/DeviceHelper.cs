﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using UdvashERP.FingerPrint.BusinessModel;
using UdvashERP.FingerPrint.BusinessRule;

namespace UdvashERP.FingerPrint.Helper
{
    public static class DeviceHelper
    {
        public static List<Device> _deviceList;
        static ZktecoColorDriver _zktecoColorDriver;

        public static int MachineNo { get; set; }
        public static Device GetDeviceInfo(string deviceName)
        {
            Device device = null;
            try
            {
                LoadDeviceInfoFromXml();
                if (_deviceList == null && _deviceList.Count == 0)
                    return device;

                if (String.IsNullOrEmpty(deviceName))
                    return device;
                device = _deviceList.Where(x => x.Name == deviceName).FirstOrDefault();
                if (device == null)
                    return device;
                MachineNo = device.MachineNo;
            }
            catch (Exception ex)
            {

            }

            return device;
        }

        private static bool DeviceConnection(Device device)
        {
            bool returnValue = false;
            try
            {
                _zktecoColorDriver = new ZktecoColorDriver();
                returnValue = _zktecoColorDriver.Connect(device);
            }
            catch (Exception ex)
            {

            }
            return returnValue;
        }


        private static bool LoadDeviceInfoFromXml()
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(List<Device>));
                FileStream fs = new FileStream(Application.StartupPath + "\\" + FingerPrintConstant.GetXmlFileName, FileMode.Open);
                _deviceList = (List<Device>)xs.Deserialize(fs);
                fs.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

    }
}
