﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface INomineeInfoService : IBaseService
    {
        #region Operational Function
        bool UpdateNomineeInformation(TeamMember member, NomineeInfo nomineeInfoRow);
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        NomineeInfo GetTeamMemberNomineeInfo(long teamMemberId);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
    public class NomineeInfoService : BaseService, INomineeInfoService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly INomineeInfoDao _nomineeInfoDao;
        public NomineeInfoService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _nomineeInfoDao = new NomineeInfoDao() { Session = session };
        }
        #endregion

        #region Operational Functions
        public bool UpdateNomineeInformation(TeamMember member, NomineeInfo nomineeInfoRow)
        {
            ITransaction trans = null;
            if (member == null)
            {
                throw new NullObjectException("MemberEntityNotFound");
            }
            if (nomineeInfoRow == null)
            {
                throw new NullObjectException("NullObject");
            }
            try
            {
                if (!String.IsNullOrEmpty(nomineeInfoRow.MobileNo))
                {
                    if (nomineeInfoRow.MobileNo.Length == 11)
                        nomineeInfoRow.MobileNo = "88" + nomineeInfoRow.MobileNo;
                    else if (nomineeInfoRow.MobileNo.Length == 10)
                        nomineeInfoRow.MobileNo = "880" + nomineeInfoRow.MobileNo;
                }
                bool isNewEntity = false;

                var entity = new NomineeInfo();
                var oldEntity = GetTeamMemberNomineeInfo(member.Id);
                if (oldEntity != null)
                {
                    oldEntity.Name = nomineeInfoRow.Name;
                    oldEntity.MobileNo = nomineeInfoRow.MobileNo;
                    oldEntity.Relation = nomineeInfoRow.Relation;
                    oldEntity.Status = NomineeInfo.EntityStatus.Active;

                    oldEntity.TeamMember = member;
                }
                else
                {
                    isNewEntity = true;
                    entity.Name = nomineeInfoRow.Name;
                    entity.MobileNo = nomineeInfoRow.MobileNo;
                    entity.Relation = nomineeInfoRow.Relation;
                    entity.Status = NomineeInfo.EntityStatus.Active;
                    entity.TeamMember = member;
                }

                using (trans = Session.BeginTransaction())
                {
                    if (isNewEntity)
                    {
                        _nomineeInfoDao.Save(entity);
                        nomineeInfoRow.Id = entity.Id;
                    }
                    else
                    {
                        _nomineeInfoDao.Update(oldEntity);
                        nomineeInfoRow.Id = oldEntity.Id;
                    }
                    trans.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                {
                    trans.Rollback();
                }
            }
        }
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public NomineeInfo GetTeamMemberNomineeInfo(long teamMemberId)
        {
            try
            {
                return _nomineeInfoDao.GetNomineeInfoByMemberId(teamMemberId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}
