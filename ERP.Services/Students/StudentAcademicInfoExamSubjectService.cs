﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IStudentAcademicInfoExamSubjectService : IBaseService
    {
        #region Operational Function
        bool Save(CommonHelper.BoardSubjectResult boardResult, long studentExamId);
        #endregion

        #region Single Instances Loading Function
        StudentAcademicInfoExamSubject LoadExamSubject(string name, string code, long studentExamId);
        #endregion

        #region List Loading Function
        IList<StudentAcademicInfoExamSubject> LoadExamSubject(long studentExamId);
        #endregion

        #region Others Function

        #endregion
    }
    public class StudentAcademicInfoExamSubjectService : BaseService, IStudentAcademicInfoExamSubjectService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentAcademicInfoExamSubjectDao _studentAcademicInfoExamSubjectDao;
        private readonly IStudentExamDao _studentExamDao;
        public StudentAcademicInfoExamSubjectService(ISession session)
        {
            Session = session;
            _studentAcademicInfoExamSubjectDao = new StudentAcademicInfoExamSubjectDao() { Session = Session };
            _studentExamDao = new StudentExamDao() { Session = Session };
        }

        #endregion

        #region Operational Function
        public bool Save(CommonHelper.BoardSubjectResult boardResult, long studentExamId)
        {
            try
            {
                var boardExamSubject = new StudentAcademicInfoExamSubject
                {
                    CreationDate = DateTime.Now,
                    Name = boardResult.Subject,
                    Code = boardResult.Code,
                    StudentExam = _studentExamDao.LoadById(studentExamId)
                };
                _studentAcademicInfoExamSubjectDao.SaveBoardSubject(boardExamSubject);
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public StudentAcademicInfoExamSubject LoadExamSubject(string name, string code, long studentExamId)
        {
            StudentAcademicInfoExamSubject studentAcademicInfoExamSubject;
            try
            {
                studentAcademicInfoExamSubject=_studentAcademicInfoExamSubjectDao.LoadExamSubject(name, code, studentExamId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return studentAcademicInfoExamSubject;
        }
        #endregion

        #region List Loading Function
        public IList<StudentAcademicInfoExamSubject> LoadExamSubject(long studentExamId)
        {
            IList<StudentAcademicInfoExamSubject> studentAcademicInfoExamSubjects;
            try
            {
                studentAcademicInfoExamSubjects = _studentAcademicInfoExamSubjectDao.LoadExamSubject(studentExamId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return studentAcademicInfoExamSubjects;
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
