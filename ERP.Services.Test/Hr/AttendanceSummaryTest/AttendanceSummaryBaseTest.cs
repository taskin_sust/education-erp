﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Hr.AttendanceSummaryServices;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.AttendanceSummaryTest
{
    public class AttendanceSummaryBaseTest:TestBase,IDisposable
    {
        #region Propertise & Object Initilization

        protected readonly DepartmentFactory _departmentFactory;
        protected readonly CampusFactory _campusFactory;
        protected readonly CampusRoomFactory _campusRoomFactory;
        protected readonly DesignationFactory _designationFactory;
        protected readonly IZoneSettingService _zoneSettingService;
        protected readonly HolidayWorkFactory _holidayWorkFactory;
        protected readonly IHolidayWorkService _holidayWorkService;
        protected readonly SalarySheetService _salarySheetService;
        protected readonly IAttendanceAdjustmentService _attendanceAdjustmentService;
        protected readonly IAttendanceSummaryService _attendanceSummaryService;
        protected readonly IOrganizationService _organizationService;
        protected readonly AttendanceSummaryServiceHelper _attendanceSummaryServiceHelper;
        protected readonly HolidaySettingFactory _holidaySettingFactory;
        protected readonly IHolidaySettingService _HolidaySettingService;
        protected readonly INightWorkService _nightWorkService;
        protected readonly OverTimeFactory _overTimeFactory;
        protected readonly IOvertimeService _overtimeService;
        protected readonly DayOffAdjustmentFactory _dayOffAdjustmentFactory;

        #endregion

        public AttendanceSummaryBaseTest()
        {
            _departmentFactory = new DepartmentFactory(null, Session);
            _campusFactory = new CampusFactory(null, Session);
            _campusRoomFactory = new CampusRoomFactory(null, Session);
            _designationFactory = new DesignationFactory(null, Session);
            _zoneSettingService = new ZoneSettingService(Session);
            _holidayWorkFactory = new HolidayWorkFactory(null, Session);
            _holidayWorkService = new HolidayWorkService(Session);
            _salarySheetService = new SalarySheetService(Session);
            _attendanceAdjustmentService = new AttendanceAdjustmentService(Session);
            _attendanceSummaryService = new AttendanceSummaryService(Session);
            _attendanceSummaryServiceHelper = new AttendanceSummaryServiceHelper(Session);
            _holidaySettingFactory = new HolidaySettingFactory(null, Session);
            _HolidaySettingService = new HolidaySettingService(Session);
            _nightWorkService = new NightWorkService(Session);
            _overTimeFactory = new OverTimeFactory(null, Session);
            _overtimeService = new OvertimeService(Session);
            _organizationService = new OrganizationService(Session);
            _dayOffAdjustmentFactory = new DayOffAdjustmentFactory(null, Session);
        }

        #region Disposal

        public void Dispose()
        {
            attendanceAdjustmentFactory.LogCleanUp(attendanceAdjustmentFactory);
            attendanceAdjustmentFactory.CleanUp(attendanceAdjustmentFactory);
            _holidaySettingFactory.Cleanup();
            nightWorkFactory.CleanUp(nightWorkFactory);
            _overTimeFactory.LogCleanUp(_overTimeFactory);
            _overTimeFactory.CleanUp();
            allowanceFactory.LogCleanUp(allowanceFactory);
            allowanceFactory.CleanUp();
            _holidayWorkFactory.LogCleanUp(_holidayWorkFactory);
            _holidayWorkFactory.CleanUpFactory();
            _dayOffAdjustmentFactory.LogCleanUp(_dayOffAdjustmentFactory);
            _dayOffAdjustmentFactory.CleanUp();
            base.Dispose();
        }

        #endregion
    }
}