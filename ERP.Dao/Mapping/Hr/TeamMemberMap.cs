using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{


    public partial class TeamMemberMap : BaseClassMap<TeamMember, long>
    {

        public TeamMemberMap()
        {
            Table("HR_TeamMember");
            LazyLoad();
            References(x => x.PermanentThana).Column("PermanentThanaId");
            References(x => x.PresentThana).Column("PresentThanaId");
            References(x => x.PermanentPostOffice).Column("PermanentPostOfficeId");
            References(x => x.PresentPostOffice).Column("PresentPostOfficeId");
            Map(x => x.Rank).Column("Rank");
            Map(x => x.Name).Column("Name");
            Map(x => x.PersonalContact).Column("PersonalContact");
            Map(x => x.FullNameEng).Column("FullNameEng");
            Map(x => x.FullNameBang).Column("FullNameBang");
            Map(x => x.Facebook).Column("Facebook");
            Map(x => x.Skype).Column("Skype");
            Map(x => x.PersonalEmail).Column("PersonalEmail");
            Map(x => x.Gender).Column("Gender");
            Map(x => x.Religion).Column("Religion");
            Map(x => x.BloodGroup).Column("BloodGroup");
            Map(x => x.Nationality).Column("Nationality");
            Map(x => x.NID).Column("NID");
            Map(x => x.BCNo).Column("BCNo");
            Map(x => x.PassportNo).Column("PassportNo");
            Map(x => x.CertificateDOB).Column("CertificateDOB");
            Map(x => x.ActualDOB).Column("ActualDOB");
            Map(x => x.PresentAddress).Column("PresentAddress");
            Map(x => x.PermanentAddress).Column("PermanentAddress");
            Map(x => x.EmargencyContactName).Column("EmargencyContactName");
            Map(x => x.EmargencyContactRelation).Column("EmargencyContactRelation");
            Map(x => x.EmargencyContactCellNo).Column("EmargencyContactCellNo");
            Map(x => x.FatherNameBang).Column("FatherNameBang");
            Map(x => x.FatherNameEng).Column("FatherNameEng");
            Map(x => x.MotherNameBang).Column("MotherNameBang");
            Map(x => x.MotherNameEng).Column("MotherNameEng");
            Map(x => x.FatherContactNo).Column("FatherContactNo");
            Map(x => x.MotherContactNo).Column("MotherContactNo");
            Map(x => x.Specialization).Column("Specialization");
            Map(x => x.ExtracurricularActivities).Column("ExtracurricularActivities");
            Map(x => x.Pin).Column("Pin");
            Map(x => x.TaxesCircle).Column("TaxesCircle");
            Map(x => x.TaxesZone).Column("TaxesZone");
            Map(x => x.TinNo).Column("TinNo");
            Map(x => x.IsSalarySheet).Column("IsSalarySheet");
            Map(x => x.IsAutoDeduction).Column("IsAutoDeduction");
            HasMany(x => x.AcademicInfo).KeyColumn("TeamMemberId");
            HasMany(x => x.Allowance).KeyColumn("TeamMemberId");
            HasMany(x => x.AttendanceAdjustment).KeyColumn("TeamMemberId");
            HasMany(x => x.ChildrenInfo).KeyColumn("TeamMemberId");
            HasMany(x => x.ChildrenInfoLog).KeyColumn("TeamMemberId");
            HasMany(x => x.DayOffAdjustment).KeyColumn("TeamMemberId");
            HasMany(x => x.EmploymentHistory).KeyColumn("TeamMemberId");
            HasMany(x => x.EmploymentHistoryLog).KeyColumn("TeamMemberId");
            HasMany(x => x.HolidayWork).KeyColumn("TeamMemberId");
            HasMany(x => x.Images).KeyColumn("TeamMemberId");
            HasMany(x => x.JobExperience).KeyColumn("TeamMemberId");
            HasMany(x => x.LeaveApplication).KeyColumn("TeamMemberId");
            HasMany(x => x.LeaveApplicationLogs).KeyColumn("TeamMemberId");
            HasMany(x => x.MaritalInfo).KeyColumn("TeamMemberId");
            HasMany(x => x.MemberOfficialDetails).KeyColumn("TeamMemberId").Cascade.AllDeleteOrphan();
            HasMany(x => x.MembersLeaveSummary).KeyColumn("TeamMemberId");
            HasMany(x => x.MentorHistory).Cascade.All().KeyColumn("TeamMemberId");
            HasMany(x => x.MentorHistoryLogs).KeyColumn("TeamMemberId");
            HasMany(x => x.NightWork).KeyColumn("TeamMemberId");
            HasMany(x => x.NomineeInfo).KeyColumn("TeamMemberId");
            HasMany(x => x.Overtime).KeyColumn("TeamMemberId");
            HasMany(x => x.SalaryHistory).KeyColumn("TeamMemberId");
            HasMany(x => x.TdsHistory).KeyColumn("TeamMemberId");
            HasMany(x => x.TdsHistoryLog).KeyColumn("TeamMemberId");
            HasMany(x => x.ShiftWeekendHistory).KeyColumn("TeamMemberId");
            HasMany(x => x.ShiftWeekendHistoryLog).KeyColumn("TeamMemberId");
            HasMany(x => x.AttendanceSummary).KeyColumn("TeamMemberId");
            HasMany(x => x.TrainingInfo).KeyColumn("TeamMemberId");
            HasMany(x => x.BankHistory).KeyColumn("TeamMemberId");
            HasMany(x => x.MemberFingerPrints).KeyColumn("TeamMemberId");
            HasMany(x => x.AttendanceDeviceMembers).KeyColumn("TeamMemberId");

            References(x => x.UserProfile).Column("UserProfileId");

        }
    }
}
