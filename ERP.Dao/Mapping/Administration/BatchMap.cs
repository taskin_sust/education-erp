using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {
    
    
    public partial class BatchMap : BaseClassMap<Batch,long>
    {
        
        public BatchMap() {
			Table("Batch");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Rank);
            Map(x => x.Name);
			Map(x => x.Days).Column("Days");
			Map(x => x.StartTime).Column("StartTime");
			Map(x => x.EndTime).Column("EndTime");
			Map(x => x.Capacity).Column("Capacity");
            Map(x => x.Gender).Column("Gender");
            Map(x => x.VersionOfStudy).Column("VersionOfStudy");

            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.Branch).Column("BranchId");
            References(x => x.Campus).Column("CampusId");

			HasMany(x => x.StudentPrograms).KeyColumn("BatchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentTransfers).KeyColumn("BatchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentBatchLogs).KeyColumn("BatchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentClassAttendence).KeyColumn("BatchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentExamAttendances).KeyColumn("BatchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.CourseProgresses).KeyColumn("BatchId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ExamProgresses).KeyColumn("BatchId").Cascade.AllDeleteOrphan().Inverse();
            //HasManyToMany(x => x.Lectures).Table("CourseProgress")
            //  .ParentKeyColumn("BatchId").ChildKeyColumn("LectureId")
            //  .Cascade.SaveUpdate();
        }
    }
}
