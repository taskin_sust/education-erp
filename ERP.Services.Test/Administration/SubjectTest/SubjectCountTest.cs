﻿using System.Security.Cryptography.X509Certificates;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.SubjectTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Subject")]
    public class SubjectCountTest : SubjectBaseTest
    {
        [Fact]
        public void Subject_Count_Greater_Than_Zero_Check()
        {
            _subjectFactory.CreateMore(100).Persist();
            var subjectCount = _subjectService.GetSubjectCount("", "", "", Subject.EntityStatus.Active.ToString());
            Assert.True(subjectCount > 0);
        }
        
    }
}
