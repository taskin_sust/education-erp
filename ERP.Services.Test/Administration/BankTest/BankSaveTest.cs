﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;
using SubjectAssign = UdvashERP.BusinessModel.Dto.SubjectAssign;

namespace UdvashERP.Services.Test.AdminisTration.BankTest
{
    public interface IBankSaveTest
    {
        #region Basic test
        
        [Fact]
        void Should_Save_Successfully();

        [Fact]
        void Save_Should_Return_Null_Exception();

        #endregion

        #region Business logic test
        
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        void Should_Not_Save_Invalid_data(int index);

        [Fact]
        void Save_Should_Check_Duplicate_Name_Entry();

        [Fact]
        void Save_Should_Check_Duplicate_ShortName_Entry();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Bank")]
    public class BankSaveTest : BankBaseTest, IBankSaveTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _bankService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var bankFactory =
                _bankFactory.Create()
                    .Persist();
            Bank bankObj = _bankService.GetBank(bankFactory.Object.Id);
            Assert.NotNull(bankObj);
            Assert.True(bankObj.Id > 0);
        }
        
        #endregion

        #region Business Logic Test
        
        [Fact]
        public void Save_Should_Check_Duplicate_Name_Entry()
        {
            _bankFactory.Create().WithName(_name)
                    .Persist();
            var bankFactory2 =
                _bankFactory.Create().WithName(_name);
            Assert.Throws<DuplicateEntryException>(() => bankFactory2.Persist());
        }
        
        [Fact]
        public void Save_Should_Check_Duplicate_ShortName_Entry()
        {
            _bankFactory.Create().WithShortName(_shortName)
                   .Persist();
            var bankFactory2 =
                _bankFactory.Create().WithShortName(_shortName);
            Assert.Throws<DuplicateEntryException>(() => bankFactory2.Persist());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        public void Should_Not_Save_Invalid_data(int index)
        {
            if (index == 1)
            {
                _bankFactory.Create().WithName("");
            }
            else if (index == 2)
            {
                _bankFactory.Create().WithShortName("");
            }
            else if (index == 3)
            {
                _bankFactory.Create().WithName(new string('*', 256));
            }
            else if (index == 4)
            {
                _bankFactory.Create().WithShortName(new string('*', 256));
            }
            Assert.Throws<InvalidDataException>(() => _bankFactory.Persist());
        }
       
        #endregion

    }
}
