using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class AttendanceAdjustmentMap : BaseClassMap<AttendanceAdjustment,long> {
        
        public AttendanceAdjustmentMap() {
			Table("HR_AttendanceAdjustment");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Name).Column("Name");
			Map(x => x.Date).Column("Date");
			Map(x => x.StartTime).Column("StartTime");
			Map(x => x.EndTime).Column("EndTime");
			Map(x => x.Reason).Column("Reason");
			Map(x => x.AdjustmentStatus).Column("AdjustmentStatus");
            Map(x => x.IsPost).Column("IsPost");
			HasMany(x => x.AttendanceAdjustmentLog).KeyColumn("AttendanceAdjustmentId");
        }
    }
}
