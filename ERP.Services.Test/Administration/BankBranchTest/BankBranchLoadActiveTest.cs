﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using Xunit;

namespace UdvashERP.Services.Test.Administration.BankBranchTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "BankBranch")]
    public class BankBranchLoadActiveTest : BankBranchBaseTest
    {
        #region Basic Test
        [Fact]
        public void Load_Bank_Branch_should_return_list()
        {
            _bankBranchFactory.CreateMore(100).Persist();
            var bankId = _bankBranchFactory.GetLastCreatedObjectList()[0].Bank.Id;
            var bankBranchList = _bankBranchService.LoadBankBranchList(new[] { bankId });
            Assert.True(bankBranchList.Count > 0);
        }
        [Fact]
        public void LoadActive_should_return_list()
        {
            _bankBranchFactory.CreateMore(100).Persist();
            var bankBranchList = _bankBranchService.LoadBankBranch(0, 100, "Rank", "Asc", "","", "", "","","","","","1");
            Assert.Equal(100, bankBranchList.Count);
        }

        #endregion
    }
}
