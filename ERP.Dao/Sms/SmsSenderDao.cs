﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Sms
{
    public interface ISmsSenderDao : IBaseDao<SmsSender, long>
    {

        IList<SmsSender> LoadPendingSms();
    }
    public class SmsSenderDao : BaseDao<SmsSender, long>, ISmsSenderDao
    {
        //private ISession Session;
        public SmsSenderDao() { }
        public IList<SmsSender> LoadPendingSms()
        {
            return
                Session.QueryOver<SmsSender>().Where(x => x.Status == SmsSender.EntityStatus.Pending).List<SmsSender>();
        }
    }
}
