using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {

    
    public partial class InstituteCategoryMap : BaseClassMap<InstituteCategory, long>
    {
        
        public InstituteCategoryMap() {
			Table("InstituteCategory");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name);
			Map(x => x.IsUniversity).Column("IsUniversity");
            Map(x => x.Rank);
            HasMany(x => x.Institutes).KeyColumn("InstituteCategoryId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
