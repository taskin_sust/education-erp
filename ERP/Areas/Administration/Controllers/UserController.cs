﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FluentNHibernate.Testing.Values;
using log4net;
using Microsoft.AspNet.Identity;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Models;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Cache;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.UserAuth;
using Constants = UdvashERP.BusinessRules.Constants;
using System.Security.Cryptography;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class UserController : Controller
    {
        #region Objects/Propertise/Services/Dao & Initialization
        private ILog logger = LogManager.GetLogger("AdministrationArea");
        private UserStoreLongPk _userStore;
        private readonly IUserService _userService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly ICommonHelper _commonHelper;
        private readonly IProgramService _programService;
        private readonly IMenuService _menuService;
        private readonly IActionsService _actionsService;
        private readonly IUserMenuService _userMenuService;
        private readonly IOrganizationService _organizationService;
        private List<UserMenu> authorizeMenu;
        private readonly IEmployeeImageMediaService _employeeImageMediaService;
        private readonly IDepartmentService _departmentService;

        public UserManager<ApplicationUser, long> UserManager { get; private set; }
        public RoleManager<RoleLongPk, long> RoleManager { get; set; }

        public UserController()
        {
            try
            {
                UserManager = new UserManager<ApplicationUser, long>(new UserStoreLongPk(ApplicationDbContext.Create()));
                var session = NHibernateSessionFactory.OpenSession();
                _userService = new UserService(session);
                _branchService = new BranchService(session);
                _campusService = new CampusService(session);
                _programService = new ProgramService(session);
                _commonHelper = new CommonHelper();
                _menuService = new MenuService(session);
                _actionsService = new ActionsService(session);
                _userMenuService = new UserMenuService(session);
                _userStore = new UserStoreLongPk(ApplicationDbContext.Create());
                _organizationService = new OrganizationService(session);
                _employeeImageMediaService = new EmployeeImageMediaService();
                _departmentService = new DepartmentService(session);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
            }

        }
        #endregion

        #region Menu
        public List<Menu> AllMenuDisplay(Menu menu, string prefix)
        {
            List<Menu> chieldren = new List<Menu>();
            chieldren.Add(menu);

            if (menu.MenuSelfMenu == null)
            {
                menu.VariableNames = menu.Name;
            }
            else
            {
                menu.VariableNames = prefix + menu.Name;
            }


            if (menu.Menus != null)
            {
                prefix = prefix + prefix;

                foreach (Menu child in menu.Menus)
                {
                    var m = AllMenuDisplay(child, prefix);
                    chieldren.AddRange(m);
                }
            }

            return chieldren;
        }
        #endregion

        #region Index/Manage

        public ActionResult ManageUserPermission()
        {
            #region load allProgram & all Branch Lisis based on authorization
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            bool allOrganization = false;
            bool allBranch = false;
            bool allProgram = false;

            #endregion

            //var allUsers = _userService.LoadActiveAuthorizedUserByBranch(authorizeMenu);
            //allUsers = allUsers.Where(x => x.AspNetUser.UserName != ApplicationUsers.SuperAdmin).OrderBy(x => x.AspNetUser.Email).ToList();
            //var ms = new MultiSelectList(allUsers, "Id", "AspNetUser.Email");
            //ViewBag.AllActions = ms;

            //var organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
            //List<SelectListItem> oList = organizationList.ToList();
            //oList.Insert(0, new SelectListItem() { Value = "-1", Text = "All" });
            //ViewBag.OrganizationList = new MultiSelectList(oList, "Value", "Text");

            //IList<Program> programList = _programService.LoadAuthorizedProgram(authorizeMenu);
            //IList<Branch> branchList = _branchService.LoadAuthorizedBranch(authorizeMenu, null, null, null, false);

           // ViewBagAllActiveAutorizedProgram(programList, allProgram);

            //SelectList programList2 = new SelectList(programList, "Id", "Name");
            //List<SelectListItem> listProgram = programList2.ToList();
            //if (allProgram)
            //    listProgram.Insert(0, new SelectListItem() { Value = "-1", Text = "All" });

            ////ViewBag.ProgramList = new SelectList(listProgram, "Value", "Text");
            //ViewBag.ProgramList = new MultiSelectList(listProgram, "Value", "Text");
            
            //SelectList branchLists = new SelectList(branchList, "Id", "Name");
            //List<SelectListItem> list = branchLists.ToList();
            //list.Insert(0, new SelectListItem() { Value = "", Text = "Select Branch" });
            //ViewBag.BranchList2 = new MultiSelectList(list, "Value", "Text");

            //List<SelectListItem> list2 = branchLists.ToList();
            //list2.Insert(0, new SelectListItem() { Value = "-1", Text = "All" });
            //ViewBag.BranchList = new MultiSelectList(list2, "Value", "Text");

            //List<Department> departmentList = _departmentService.LoadAuthorizedDepartment(authorizeMenu).ToList();
            //SelectList departmentSelectList = new SelectList(departmentList, "Id", "Name");
            //List<SelectListItem> departmentSelectItemList = departmentSelectList.ToList();
            //departmentSelectItemList.Insert(0, new SelectListItem() { Value = "", Text = "Select Department" });
            //ViewBag.DepartmentSelectItemList = new MultiSelectList(departmentSelectItemList, "Value", "Text");

            // Permission menu Id = 88
            // Permission Menu All Check

            AspNetUser aspNetUser = _userService.LoadAspNetUserById(_userService.GetCurrentUserId());
            if (aspNetUser.UserName.ToLower() == ApplicationUsers.SuperAdmin)
            {
                allOrganization = true;
                allBranch = true;
                allProgram = true;
            }
            else
            {
                long menuId = 88;
                long userProfileId = _userService.GetByAspNetUser(_userService.GetCurrentUserId()).Id;
                _userMenuService.GetAllOrganizationProgramBranchMenuPermission(userProfileId, menuId, out allOrganization, out allBranch, out allProgram);
            }


            IList<Program> programList = _programService.LoadAuthorizedProgram(authorizeMenu);
            IList<Branch> branchList = _branchService.LoadAuthorizedBranch(authorizeMenu, null, null, null, false);
            IList<Campus> campusList = _campusService.LoadAuthorizeCampus(authorizeMenu, null, null, (branchList != null && branchList.Any()) ? branchList.Select(x => x.Id).ToList() : null, null, false);


            SelectList organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
            List<SelectListItem> oList = organizationList.ToList();
            if (allOrganization)
            {
                oList.Insert(0, new SelectListItem() { Value = "-1", Text = "All" });   
            }
            ViewBag.OrganizationList = new MultiSelectList(oList, "Value", "Text");

            #region Filter ViewBag

            //Department List
            List<Department> departmentList = _departmentService.LoadAuthorizedDepartment(authorizeMenu).ToList();
            SelectList departmentSelectList = new SelectList(departmentList, "Id", "Name");
            List<SelectListItem> departmentSelectItemList = departmentSelectList.ToList();
            departmentSelectItemList.Insert(0, new SelectListItem() { Value = "", Text = "Select Department" });
            ViewBag.DepartmentSelectItemList = new MultiSelectList(departmentSelectItemList, "Value", "Text");

            //Branch List
            SelectList branchLists = new SelectList(branchList, "Id", "Name");
            List<SelectListItem> list = branchLists.ToList();
            list.Insert(0, new SelectListItem() { Value = "", Text = "Select Branch" });
            ViewBag.BranchList2 = new MultiSelectList(list, "Value", "Text");

            //Campus List
            SelectList campusLists = new SelectList(campusList, "Id", "Name");
            List<SelectListItem> campusSelectList = campusLists.ToList();
            campusSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All Campus" });
            ViewBag.CampusList = new MultiSelectList(campusSelectList, "Value", "Text");
            
            //User List
            List<UserProfile> allUsers = _userService.LoadActiveAuthorizedUserByBranch(authorizeMenu).ToList();
            allUsers = allUsers.Where(x => x.AspNetUser.UserName != ApplicationUsers.SuperAdmin).OrderBy(x => x.AspNetUser.Email).ToList();
            MultiSelectList userMultiSelectList = new MultiSelectList(allUsers, "Id", "AspNetUser.Email");
            ViewBag.UserMultiSelectList = userMultiSelectList;

            

            #endregion

            #region Permission Criteria

            //Branch List
            List<SelectListItem> branchlist2 = branchLists.ToList();
            if (allBranch)
            {
                branchlist2.Insert(0, new SelectListItem() { Value = "-1", Text = "All" });
            }
            ViewBag.BranchList = new MultiSelectList(branchlist2, "Value", "Text");

            //Program List
            SelectList programList2 = new SelectList(programList, "Id", "Name");
            List<SelectListItem> listProgram = programList2.ToList();
            if (allProgram)
            {
                listProgram.Insert(0, new SelectListItem() {Value = "-1", Text = "All"});
            }
            ViewBag.ProgramList = new MultiSelectList(listProgram, "Value", "Text");

            #endregion







            return View();
        }

        [HttpPost]
        public ActionResult ManageUserPermissionAjax(UserpermissionFormViewModel userpermission)
        {
            try
            {
                var currentUserId = long.Parse(User.Identity.GetUserId());
                var userProfileList = _userService.LoadActive(userpermission.UserIds).ToList();
                var organizationIdList = userpermission.OrganizationId.Contains(-1)
                    ? new long[] { -1 }
                    : userpermission.OrganizationId;
                var programIdList = userpermission.ProgramId.Contains(-1)
                    ? new long[] { -1 }
                    : userpermission.ProgramId;
                var branchIdList = userpermission.BranchId.Contains(-1)
                    ? new long[] { -1 }
                    : userpermission.BranchId;
                var menuIdList = (from up in userpermission.UserPermissionListViewModel
                                  where up.CheckedValue == true
                                  select up.Id).ToList();

                //set permission
                _userMenuService.BulkUserPermissionSet(currentUserId, userProfileList, organizationIdList, programIdList, branchIdList, menuIdList);

                //force to load
                foreach (var profile in userProfileList)
                {
                    AuthorizationCache.SetUserPermissionChangeTime(profile.AspNetUser);
                }

                //TempData["PermissionSuccessMessage"] = "Permission successfully updated.";
                return Json(new Response(true, "Permission successfully updated"));
            }
            catch (Exception ex)
            {
                TempData["PermissinErrorMessage"] = WebHelper.CommonErrorMessage;
                logger.Error(ex);
                return Json(new Response(false, "Permission error"));
            }
            //return RedirectToAction("ManageUserPermission");
        }
        
        public ActionResult Index()
        {
            try
            {
                ViewBagOrganizationList();
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                //bool allBranch = false;
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(authorizeMenu, out allBranch);

                List<long> branchIdList = AuthHelper.LoadBranchIdList(authorizeMenu);


                IList<Branch> iBranchLists = _branchService.LoadAuthorizedBranch(authorizeMenu, null, null, null, false);
                //SelectList branchList = new SelectList(iBranchLists, "Id", "Name");
                SelectList branchList = new SelectList(new List<Branch>(), "Id", "Name");
                List<SelectListItem> list = branchList.ToList();

                //if (branchIdList==null)
                list.Insert(0, new SelectListItem() { Value = "-1", Text = "Select Branch" });

                ViewBag.BranchList = new SelectList(list, "Value", "Text");
                GetStatusText();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return View();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                GetStatusText();
                return View();
            }

        }
        
        public ActionResult ViewUserList()
        {
            try
            {
                ViewBagOrganizationList();
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                //bool allBranch = false;
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(authorizeMenu, out allBranch);

                List<long> branchIdList = AuthHelper.LoadBranchIdList(authorizeMenu);


                IList<Branch> iBranchLists = _branchService.LoadAuthorizedBranch(authorizeMenu, null, null, null, false);
                //SelectList branchList = new SelectList(iBranchLists, "Id", "Name");
                SelectList branchList = new SelectList(new List<Branch>(), "Id", "Name");
                List<SelectListItem> list = branchList.ToList();

                //if (branchIdList==null)
                list.Insert(0, new SelectListItem() { Value = "-1", Text = "Select Branch" });

                ViewBag.BranchList = new SelectList(list, "Value", "Text");
                GetStatusText();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return View();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                GetStatusText();
                return View();
            }

        }

        public ActionResult ObserveUserPermission()
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<Branch> branchList = _branchService.LoadAuthorizedBranch(authorizeMenu, null, null, null, false);

            #region Filter ViewBag

            //organization List
            SelectList organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
            List<SelectListItem> oList = organizationList.ToList();
            oList.Insert(0, new SelectListItem() { Value = "", Text = "Select Organization" });
            ViewBag.OrganizationList = new MultiSelectList(oList, "Value", "Text");

            //Department List
            List<Department> departmentList = _departmentService.LoadAuthorizedDepartment(authorizeMenu).ToList();
            SelectList departmentSelectList = new SelectList(departmentList, "Id", "Name");
            List<SelectListItem> departmentSelectItemList = departmentSelectList.ToList();
            departmentSelectItemList.Insert(0, new SelectListItem() { Value = "", Text = "Select Department" });
            ViewBag.DepartmentSelectItemList = new MultiSelectList(departmentSelectItemList, "Value", "Text");

            //Branch List
            SelectList branchLists = new SelectList(branchList, "Id", "Name");
            List<SelectListItem> list = branchLists.ToList();
            list.Insert(0, new SelectListItem() { Value = "", Text = "Select Branch" });
            ViewBag.BranchList2 = new MultiSelectList(list, "Value", "Text");

            //User List
            List<UserProfile> allUsers = _userService.LoadActiveAuthorizedUserByBranch(authorizeMenu).ToList();
            allUsers = allUsers.Where(x => x.AspNetUser.UserName != ApplicationUsers.SuperAdmin).OrderBy(x => x.AspNetUser.Email).ToList();
            MultiSelectList userMultiSelectList = new MultiSelectList(allUsers, "Id", "AspNetUser.Email");
            ViewBag.UserMultiSelectList = userMultiSelectList;
            #endregion

            return View();
        }

        #endregion

        #region Save Operation
        [AuthorizeAccess]
        public ActionResult Create()
        {
            try
            {
                ViewBagOrganizationList();
                ViewBagEmptyBranchList();
                //ViewBagEmptyCampusLists();
                ViewBag.campusList = new SelectList(new List<Campus>(), "Id", "Name");


            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(UserAddForm model)
        {
            try
            {
                ViewBagOrganizationList();
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                bool allBranch = false;
                List<long> userBranchIds = AuthHelper.LoadBranchIdList(authorizeMenu, null);
                if (userBranchIds == null)
                    allBranch = true;
                IList<Branch> branchList = _branchService.LoadAuthorizedBranch(authorizeMenu, _commonHelper.ConvertIdToList(model.OrganizationId), null, null, false);
                ViewBagAllActiveAutorizedBranch(branchList, allBranch, model.BranchId);

                IList<Campus> ICampusLists = _campusService.LoadCampus(null, null, null, _commonHelper.ConvertIdToList(Convert.ToInt64(model.BranchId)), null, false);
                ViewBagcampusList(ICampusLists, model.CampusId);

                var validImageTypes = new string[] { "image/gif", "image/jpeg", "image/pjpeg", "image/png" };
                AspNetUser aspNetUser = new AspNetUser();
                if (ModelState.IsValid)
                {
                    bool checkContactNo = _userService.HasDuplicateByContactNo(model.ContactNo);
                    if (!checkContactNo)
                    {
                        ViewBag.ErrorMessage = "Contact Number exist,Please enter a different contact number";
                        return View(model);
                    }
                    if (!validImageTypes.Contains(model.Image.ContentType))
                    {
                        ViewBag.ErrorMessage = "Please choose either a GIF, JPG or PNG image.";
                        return View(model);
                    }
                    var userCreate = new ApplicationUser()
                                     {
                                         UserName = model.Email,
                                         Email = model.Email,
                                         FullName = model.FullName,
                                         PhoneNumber = model.ContactNo,
                                         LockoutEndDateUtc = DateTime.Now,
                                         Status = BaseEntity<long>.EntityStatus.Active,
                                         Rank = _userService.GetMaxRank(aspNetUser) + 1,
                                         CreateBy =
                                             Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId()),
                                         ModifyBy =
                                             Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId())
                                     };
                    var result = await UserManager.CreateAsync(userCreate, model.Password);

                    if (result.Succeeded)
                    {
                        await UserManager.AddToRoleAsync(userCreate.Id, ApplicationRoles.User);
                        var imageFile = ConvertFileInByteArray(model.Image.InputStream, model.Image.ContentLength);

                        UserProfile userObj = new UserProfile();
                        userObj.Name = userCreate.UserName;
                        userObj.Branch = null;
                        userObj.Campus = null;
                        userObj.NickName = model.NickName;
                        if (model.BranchId != -1)
                        {
                            Branch branch = _branchService.GetBranch(model.BranchId);
                            if (branch != null)
                                userObj.Branch = branch;

                            if (model.CampusId != -1)
                            {
                                Campus campusInfo = _campusService.GetCampus(model.CampusId);
                                if (campusInfo != null)
                                    userObj.Campus = campusInfo;
                            }
                        }
                        userObj.AspNetUser = _userService.LoadAspNetUserById(userCreate.Id);

                        Images imgObj = new Images();
                        //imgObj.GuidId = Guid.NewGuid();
                        imgObj.Hash = ComputeHash(imageFile);
                        imgObj.UserImages = imageFile;
                        imgObj.Name = model.Image.FileName;
                        _userService.Save(userObj, imgObj);

                        ViewBag.SuccessMessage = "User Added Successfully.";
                        ModelState.Clear();
                        return View(new UserAddForm());
                    }
                    else
                    {
                        var errors = string.Join(",", result.Errors);
                        ViewBag.ErrorMessage = errors.Replace("Name", "Email");
                        //AddErrors(result);
                        return View(model);
                    }
                }
            }
            catch (NullObjectException ex)
            {
                ViewBag.ErrorMessage = ex;
                logger.Error(ex);
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex;
                logger.Error(ex);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex;
                logger.Error(ex);
            }
            return View(model);
        }
        private string ComputeHash(byte[] file)
        {
            MD5 md5 = MD5.Create();

            byte[] hashAraay = md5.ComputeHash(file);

            var builder = new StringBuilder();

            foreach (byte b in hashAraay)
            {
                builder.AppendFormat("{0:x2}", b);
            }

            return builder.ToString();
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(long id)
        {
            try
            {
                GetStatusText();
                AspNetUser basicUser = _userService.LoadbyAspNetUserId(id);
                UserProfile user = _userService.LoadByAspNetUserWithoutStatus(basicUser);

                if (basicUser == null)
                    return HttpNotFound();

                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                bool allBranch = false;
                List<long> userBranchIds = AuthHelper.LoadBranchIdList(authorizeMenu, null);
                if (userBranchIds == null)
                    allBranch = true;

                List<long> authoUserOrganizationIdList = AuthHelper.LoadOrganizationIdList(authorizeMenu, _commonHelper.ConvertIdToList(user.Branch.Organization.Id));
                IList<Branch> branchList = _branchService.LoadAuthorizedBranch(authorizeMenu, (authoUserOrganizationIdList.Any()) ? authoUserOrganizationIdList : null, null, null, false);
                ViewBagAllActiveAutorizedBranch(branchList, allBranch);

                //check user can edit this user
                bool canEditUser = false;
                if (allBranch)
                    canEditUser = true;
                else if (user.Branch != null)
                    canEditUser = userBranchIds.Contains(user.Branch.Id);

                if (!canEditUser)
                    return RedirectToAction("PermissionDenied", "Home", new { area = "" });
                //end check user can edit this user

                UserUpdateForm userUpdateForm = new UserUpdateForm();
                userUpdateForm.Id = basicUser.Id;
                userUpdateForm.FullName = basicUser.FullName;
                userUpdateForm.Email = basicUser.Email;
                userUpdateForm.ContactNo = basicUser.PhoneNumber;
                userUpdateForm.Status = basicUser.Status;
                userUpdateForm.NickName = user.NickName;
                if (user.UserImages.Count > 0)
                {
                    EmployeeMediaImage mediaEmployeeImages = _employeeImageMediaService.GetEmployeeImageMediaByRefId(
                         user.UserImages[0].Id);
                    if (mediaEmployeeImages != null)
                    {
                        byte[] bytes = mediaEmployeeImages.Images;
                        string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                        ViewBag.DisplayImage = string.Format("data:image/png;base64,{0}", base64String);
                    }

                }
                ViewBagOrganizationList();
                if (user.Branch != null)
                {
                    userUpdateForm.OrganizationId = user.Branch.Organization.Id;
                    userUpdateForm.BranchId = user.Branch.Id;
                    if (user.Campus != null)
                    {
                        userUpdateForm.CampusId = user.Campus.Id;
                        IList<Campus> ICampusLists = _campusService.LoadCampus(_commonHelper.ConvertIdToList(user.Branch.Organization.Id), null, null, _commonHelper.ConvertIdToList(user.Branch.Id), null, false);
                        ViewBagcampusList(ICampusLists, userUpdateForm.CampusId);
                    }
                    else
                    {
                        ViewBagEmptyCampusLists();
                    }
                }
                else
                {
                    ViewBagEmptyCampusLists();
                }
                return View(userUpdateForm);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(UserUpdateForm userUpdateForm)
        {
            AspNetUser basicUser = null;
            UserProfile user = null;
            //byte[] imageFile=null;
            try
            {
                GetStatusText();
                basicUser = _userService.LoadbyAspNetUserId(userUpdateForm.Id);
                user = _userService.LoadByAspNetUserWithoutStatus(basicUser);
                if (user == null)
                    return HttpNotFound();

                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                bool allBranch = false;
                List<long> userBranchIds = AuthHelper.LoadBranchIdList(authorizeMenu, null);
                if (userBranchIds == null)
                    allBranch = true;

                IList<Branch> branchList = _branchService.LoadAuthorizedBranch(authorizeMenu, null, null, null, false);
                ViewBagAllActiveAutorizedBranch(branchList, allBranch);

                ViewBagEmptyCampusLists();
                var validImageTypes = new string[] { "image/gif", "image/jpeg", "image/pjpeg", "image/png" };
                if (ModelState.IsValid)
                {
                    ApplicationUser Model = await _userStore.FindByIdAsync(userUpdateForm.Id);
                    Images imgObj = new Images();
                    if (!CheckUserEmailDuplicate(user, userUpdateForm) || !CheckUserMobileNo(user, userUpdateForm))
                    {
                        return RedirectToAction("Edit", new { id = userUpdateForm.Id });
                    }

                    Model.Email = userUpdateForm.Email;
                    Model.FullName = userUpdateForm.FullName;
                    Model.UserName = userUpdateForm.Email;
                    Model.Status = userUpdateForm.Status;
                    Model.PhoneNumber = userUpdateForm.ContactNo;
                    Model.ModifyBy = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    Model.ModificationDate = DateTime.Now;
                    if (userUpdateForm.Password != null)
                    {
                        String hashedNewPassword = UserManager.PasswordHasher.HashPassword(userUpdateForm.Password);
                        await _userStore.SetPasswordHashAsync(Model, hashedNewPassword);
                    }
                    await _userStore.UpdateAsync(Model);
                    var ctx = _userStore.Context;
                    var result = ctx.SaveChanges();

                    if (result == 0)
                    {
                        user.Branch = null;
                        user.Campus = null;
                        user.Status = userUpdateForm.Status;
                        user.NickName = userUpdateForm.NickName;
                        if (userUpdateForm.BranchId != null)
                        {
                            Branch branch = _branchService.GetBranch(userUpdateForm.BranchId);
                            if (branch != null)
                            {
                                user.Branch = branch;
                            }
                            if (userUpdateForm.CampusId != -1 && user.Branch != null)
                            {
                                Campus campusObj = _campusService.GetCampus(userUpdateForm.CampusId);
                                if (campusObj != null)
                                {
                                    user.Campus = campusObj;
                                    IList<Campus> campusLists = _campusService.LoadCampus(null, null, null, _commonHelper.ConvertIdToList(user.Branch.Id), null, false);
                                    ViewBagcampusList(campusLists);
                                }
                            }
                        }
                        if (userUpdateForm.Image != null)
                        {
                            if (!validImageTypes.Contains(userUpdateForm.Image.ContentType))
                            {
                                if (user.UserImages.Count > 0)
                                {
                                    ViewBagUserDisplayImage(user);
                                }
                                TempData["UserErrorMessage"] = "Please choose either a GIF, JPG or PNG image";
                                return RedirectToAction("Edit", new { id = userUpdateForm.Id });

                            }
                            var imageFile = ConvertFileInByteArray(userUpdateForm.Image.InputStream,
                               userUpdateForm.Image.ContentLength);
                            imgObj.UserImages = imageFile;
                            imgObj.User = user;
                        }
                        else
                        {
                            if (user.UserImages.Count <= 0)
                            {
                                TempData["UserErrorMessage"] = "Please choose either a GIF, JPG or PNG image";
                                return RedirectToAction("Edit", new { id = userUpdateForm.Id });
                            }
                            //imgObj.UserImages = user.UserImages[0].UserImages;
                            EmployeeMediaImage mediaEmployeeImages = _employeeImageMediaService.GetEmployeeImageMediaByRefId(
                                user.UserImages[0].Id);
                            imgObj.UserImages = mediaEmployeeImages.Images;
                            imgObj.User = user;
                        }
                        _userService.Update(userUpdateForm.Id, user, imgObj);

                        if (user.UserImages.Count > 0)
                        {
                            ViewBagUserDisplayImage(user);
                        }
                        TempData["UserSuccessMessage"] = "User update Successfully";
                        AuthorizationCache.SetUserForceLogoff(basicUser, (userUpdateForm.Status == -1));
                        return RedirectToAction("Edit", new { id = userUpdateForm.Id });
                    }
                    else
                    {
                        if (user.UserImages.Count > 0)
                        {
                            ViewBagUserDisplayImage(user);
                        }
                        return RedirectToAction("Edit", new { id = userUpdateForm.Id });
                    }
                }
                else
                {
                    string errorImage = "";
                    if (user.UserImages.Count <= 0)
                    {
                        if (userUpdateForm.Image != null)
                        {
                            if (!validImageTypes.Contains(userUpdateForm.Image.ContentType))
                            {
                                errorImage = " Please choose either a GIF, JPG or PNG image";
                            }
                        }
                        else
                        {
                            errorImage = " Image field required";
                        }
                    }
                    else if (userUpdateForm.Image != null)
                    {
                        if (!validImageTypes.Contains(userUpdateForm.Image.ContentType))
                        {
                            errorImage = " Please choose either a GIF, JPG or PNG image";
                        }
                    }
                    string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));

                    TempData["UserErrorMessage"] = messages + errorImage;
                    return RedirectToAction("Edit", new { id = userUpdateForm.Id });
                }
            }
            catch (Exception ex)
            {
                if (user.UserImages.Count > 0)
                {
                    ViewBagUserDisplayImage(user);
                }
                TempData["UserErrorMessage"] = WebHelper.CommonErrorMessage;
                logger.Error(ex);

            }
            return RedirectToAction("Edit", new { id = userUpdateForm.Id });
            //return View(userUpdateForm);
        }
        #endregion

        #region Details Operation
        public ActionResult Details(long id)
        {
            AspNetUser basicUser = _userService.LoadbyAspNetUserId(id);
            UserProfile user = _userService.LoadByAspNetUser(basicUser);
            if (user != null)
            {
                user.CreateByText = _userService.GetUserNameByAspNetUserId(user.CreateBy);
                user.ModifyByText = _userService.GetUserNameByAspNetUserId(user.ModifyBy);
            }
            if (user.UserImages.Count > 0)
            {

                var userImageId = user.UserImages[0].Id;
                EmployeeMediaImage mediaEmployeeImages = _employeeImageMediaService.GetEmployeeImageMediaByRefId(userImageId);
                byte[] bytes = mediaEmployeeImages.Images;
                string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                ViewBag.ProfileImage = string.Format("data:image/png;base64,{0}", base64String);
            }
            else
            {
                ViewBag.ProfileImage = null;
            }
            //var _userObj = new UserProfile();
            return View(user);
        }
        #endregion

        #region Ajax Function

        [HttpPost]
        public JsonResult doesEmailExist(string contactNo, long? id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (id != null)
                    {
                        bool checkContactNo = _userService.HasDuplicateByContactNo(contactNo, Convert.ToInt64(id));
                        return Json(checkContactNo);
                    }
                    else
                    {
                        bool checkContactNo = _userService.HasDuplicateByContactNo(contactNo);
                        return Json(checkContactNo);
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                    {
                        logger.Error("Phone number Can't be Checked  due to " + ex.InnerException);
                    }
                    else
                    {
                        logger.Error("Phone number be Checked  due to " + ex.Message);
                    }
                    return Json(false);
                }
            }
            else { return Json(false); }
        }
        
        public ActionResult GetCampusForDropDownList(int? branchId)
        {
            if (Request.IsAjaxRequest())
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                try
                {
                    if (branchId != null)
                    {
                        IList<Campus> ICampusLists = _campusService.LoadAuthorizeCampus(userMenu, null, null, _commonHelper.ConvertIdToList(Convert.ToInt64(branchId)), null, false);
                        ViewBagcampusListWithListCout(ICampusLists);
                    }
                    else
                    {
                        ViewBagEmptyCampusListsWithCount();
                    }
                    return PartialView("Partial/_CampusList");
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return HttpNotFound(); }
        }
        
        public JsonResult GetBranchForOrganization(int? organizationId)
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                //bool allBranch = false;
                //List<long> userBranchIds = AuthHelper.LoadBranchIdList(authorizeMenu, null);
                //if (userBranchIds == null)
                //    allBranch = true;
                IList<Branch> branchList = _branchService.LoadAuthorizedBranch(authorizeMenu, (organizationId == null) ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), null, null, false);
                SelectList branchLists = new SelectList(branchList, "Id", "Name");
                List<SelectListItem> list = branchLists.ToList();
                list.Insert(0, new SelectListItem() { Value = "", Text = "Select Branch" });
                //if (allBranch)
                //    list.Insert(0, new SelectListItem() { Value = "-1", Text = "Select Branch" });

                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

        }
        
        public ActionResult GetPermittedBranchByOrganization(long organizationId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;

                    if (organizationId != -1)
                    {
                        var bList = new SelectList(_branchService.LoadAuthorizedBranch(authorizeMenu, _commonHelper.ConvertIdToList(organizationId), null, null, false), "Id", "Name");
                        List<SelectListItem> branchList = bList.ToList();
                        branchList.Insert(0, new SelectListItem() { Value = "", Text = "Select Branch" });
                        return Json(new { branchList = branchList, isSuccess = true });
                    }
                    else
                    {
                        var bList = new SelectList(_branchService.LoadAuthorizedBranch(authorizeMenu, null, null, null, false), "Id", "Name");
                        List<SelectListItem> branchList = bList.ToList();
                        branchList.Insert(0, new SelectListItem() { Value = "", Text = "Select Branch" });
                        return Json(new { branchList = branchList, isSuccess = true });
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    Console.WriteLine("{0}", ex.Message);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return HttpNotFound(); }
        }
        
        public ActionResult GetprogramBranchByOrganization(List<long> organizationIds)
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {

                bool allOrganization = false;
                bool allBranch = false;
                bool allProgram = false;
                // Permission menu Id = 88
                // Permission Menu All Check

                AspNetUser aspNetUser = _userService.LoadAspNetUserById(_userService.GetCurrentUserId());
                if (aspNetUser.UserName.ToLower() == ApplicationUsers.SuperAdmin)
                {
                    allOrganization = true;
                    allBranch = true;
                    allProgram = true;
                }
                else
                {
                    long menuId = 88;
                    long userProfileId = _userService.GetByAspNetUser(_userService.GetCurrentUserId()).Id;
                    _userMenuService.GetAllOrganizationProgramBranchMenuPermission(userProfileId, menuId, out allOrganization, out allBranch, out allProgram, organizationIds);
                }

                if (organizationIds.Contains(-1))
                {
                    organizationIds = null;
                }

                var bList = new SelectList(_branchService.LoadAuthorizedBranch(authorizeMenu, organizationIds, null, null, false), "Id", "Name");
                List<SelectListItem> branchList = bList.ToList();
                if (allBranch)
                {
                    branchList.Insert(0, new SelectListItem() {Value = "-1", Text = "All"});
                }

                var pList = new SelectList(_programService.LoadAuthorizedProgram(authorizeMenu, organizationIds), "Id", "Name");
                List<SelectListItem> programList = pList.ToList();
                if (allProgram)
                {
                    programList.Insert(0, new SelectListItem() {Value = "-1", Text = "All"});
                }

                return Json(new { branchList = branchList, programList = programList, isSuccess = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Console.WriteLine("{0}", ex.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        
        public ActionResult GetPermittedUserByBranch(long branchId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;

                    List<long> branchIdList = AuthHelper.LoadBranchIdList(authorizeMenu);
                    List<long> programIdList = AuthHelper.LoadProgramIdList(authorizeMenu);

                    var allUsers = _userService.LoadActiveAuthorizedUserByBranch(programIdList, branchIdList);
                    allUsers = allUsers.Where(x => x.AspNetUser.UserName != ApplicationUsers.SuperAdmin).OrderBy(x => x.AspNetUser.Email).ToList();

                    if (branchId != -1)
                    {
                        allUsers = allUsers.Where(x => x.Branch != null && x.Branch.Id == branchId).ToList();
                    }
                    var msUser = new MultiSelectList(allUsers, "Id", "AspNetUser.Email");

                    return Json(msUser, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    Console.WriteLine("{0}", ex.Message);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return HttpNotFound(); }
        }

        public ActionResult LoadUserListByDepartmentBranch(List<long> branchIdList, List<long> departmentIdList = null, List<long> campusIdList = null)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    List<UserProfile> allUserProfiles = _userService.LoadUserProfileListByDepartmentBranch(authorizeMenu, branchIdList, departmentIdList, campusIdList);
                    allUserProfiles = allUserProfiles.Where(x => x.AspNetUser.UserName != ApplicationUsers.SuperAdmin).OrderBy(x => x.AspNetUser.Email).ToList();
                    var msUser = new MultiSelectList(allUserProfiles, "Id", "AspNetUser.Email");
                    return Json(msUser, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    Console.WriteLine("{0}", ex.Message);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return HttpNotFound(); }
        }

        [HttpPost]
        public JsonResult ForcelyPsswordChange(long id)
        {
            try
            {
                var basicUser = _userService.LoadbyAspNetUserId(id);
                var user = _userService.LoadByAspNetUser(basicUser);
                var massage = "";
                if (user != null)
                {
                    massage = user.BusinessId == null ? "Forcely Pssword Change successfull Applied" : "Remove Forcely Pssword Change Sucessfully Applied";
                    user.BusinessId = user.BusinessId != null ? null : "1";
                    _userService.Update(user.Id, user, null);
                    return Json(new Response(true, massage));
                }
                else
                {
                    return Json(new Response(false, "Action Failed !"));
                }
            }
            catch (Exception e)
            {
                logger.Error(e);
                Console.WriteLine("{0}", e.Message);
                return Json(new Response(false, "ActionFailed !"));
            }
        }
        
        [HttpPost]
        public async Task<JsonResult> LockUnlockUser(long id, bool userLock = true)
        {
            var massage = userLock ? "User lock" : "User unlock";
            try
            {
                var basicUser = _userService.LoadbyAspNetUserId(id);
                //var user = _userService.LoadByAspNetUser(basicUser);
                await UserManager.SetLockoutEnabledAsync(id, userLock);
                await UserManager.ResetAccessFailedCountAsync(id);
                AuthorizationCache.SetUserForceLogoff(basicUser, userLock);
                return Json(new Response(true, massage + "successfull !"));
            }
            catch (Exception e)
            {
                logger.Error(e);
                Console.WriteLine("{0}", e.Message);
                return Json(new Response(false, massage + "Fail !"));
            }
        }
        
        public ActionResult GetPermittedValue(long[] listUser, long[] organizationIds, long[] programId, long[] branchId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    bool isAllOrganizationSelect = Array.Exists(organizationIds, item => item == -1);
                    bool isAllprogramSelect = Array.Exists(programId, item => item == -1);
                    bool isAllBranchSelect = Array.Exists(branchId, item => item == -1);

                    if (isAllOrganizationSelect) organizationIds = null;
                    if (isAllprogramSelect) programId = null;
                    if (isAllBranchSelect) branchId = null;
                    if (listUser == null)
                        throw new Exception("Please Select User");

                    var selectedUserMenu = _userMenuService.LoadDataByUserBranchProgramOrganization(listUser, organizationIds, programId, branchId);
                    ViewBag.SelectedUserMenu = selectedUserMenu;
                    ViewBag.Program = programId;
                    ViewBag.Branch = branchId;

                    //set permision menu
                    AspNetUser aspNetUser = _userService.LoadAspNetUserById(_userService.GetCurrentUserId());
                    if (aspNetUser.UserName.ToLower() == ApplicationUsers.SuperAdmin)
                    {
                        ViewBag.UserMenuForPermission = (List<Menu>)ViewBag.AllMenu;
                    }
                    else
                    {
                        long userProfileId = _userService.GetByAspNetUser(_userService.GetCurrentUserId()).Id;
                        //List<UserMenu> userMenuForPermission = _userMenuService.LoadUserMenuForPermission(userProfileId, (organizationIds != null) ? organizationIds.ToList() : null, (programId != null) ? programId.ToList() : null, (branchId != null) ? branchId.ToList() : null);
                        
                        //List<Menu> parentMenuList = new List<Menu>();
                        //List<Menu> menuForPermission = userMenuForPermission.Select(x => x.Menu).Distinct().ToList();
                        //if (menuForPermission.Any())
                        //{
                        //    parentMenuList = menuForPermission.Where(x => x.MenuSelfMenu != null).Select(x => x.MenuSelfMenu).Distinct().ToList();
                        //}
                        //menuForPermission.AddRange(parentMenuList);
                        //List<Menu> test = (List<Menu>) ViewBag.AllMenu;
                        List<Menu> menuForPermission = _menuService.LoadMenuForUserPermission(userProfileId, (organizationIds != null) ? organizationIds.ToList() : null, (programId != null) ? programId.ToList() : null, (branchId != null) ? branchId.ToList() : null);

                        ViewBag.UserMenuForPermission = menuForPermission; //userMenuForPermission;   
                    }

                    return PartialView("Partial/_MenuPermissionList");
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    Console.WriteLine("{0}", ex.Message);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return null; }
        }
        
        [HttpPost]
        public ActionResult LoadUserMenuListToObservePermission(long userProfileId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    List<Menu> menuForObservePermission = _menuService.LoadMenuForUserObservePermission(userProfileId);
                    ViewBag.UserMenuForPermission = menuForObservePermission;
                    ViewBag.ProfileId = userProfileId;

                    return PartialView("Partial/_ObservedMenuPermissionList");
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    Console.WriteLine("{0}", ex.Message);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return null; }
        }

        [HttpPost]
        public ActionResult DetailsObservePermission(long userProfileId, long menuId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    string menuName = _menuService.MenuLoadById(menuId).Name;
                    List<UserMenu> userMenuDetailsObservePermission = _userMenuService.LoadDetailsObservePermission(userProfileId, menuId);
                    ViewBag.UserMenuDetailsObservePermission = userMenuDetailsObservePermission;
                    ViewBag.ProfileId = userProfileId;
                    ViewBag.MenuName = menuName;

                    return PartialView("Partial/_DetailsObservedMenuPermissionList");
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    Console.WriteLine("{0}", ex.Message);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return null; }
        }

        #endregion

        #region Render Data Table
        public JsonResult UserList(int draw, int start, int length, string userName, string contactNo, string branchId, string status, string organizationId)
        {
            NameValueCollection nvc = Request.Form;

            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            try
            {
                var convertedOrganizationId = String.IsNullOrEmpty(organizationId) ? (long?)null : Convert.ToInt32(organizationId);
                var convertedBranchId = String.IsNullOrEmpty(branchId) ? (long?)null : Convert.ToInt32(branchId);
                //load allProgram & all Branch Lisis based on authorization 
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                //bool allBranch = false;
                //bool allProgram = false;
                //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(authorizeMenu, out allProgram);
                //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(authorizeMenu, out allBranch);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(authorizeMenu);
                List<long> programIdList = AuthHelper.LoadProgramIdList(authorizeMenu);

                //end load allProgram & all Branch Lisis based on authorization 

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "branch.Name";
                            break;
                        case "1":
                            orderBy = "campus.Name";
                            break;
                        case "2":
                            orderBy = "aspU.FullName";
                            break;
                        case "3":
                            orderBy = "aspU.UserName";
                            break;
                        case "4":
                            orderBy = "aspU.PhoneNumber";
                            break;
                        case "5":
                            orderBy = "aspU.Status";
                            break;
                        default:
                            orderBy = "aspU.UserName";
                            break;
                    }
                }
                string rank = "";
                int recordsTotal = _userService.RowCountAuthorizedUserBybranch(orderBy, orderDir.ToUpper(), userName, contactNo, convertedBranchId, status, convertedOrganizationId, rank, programIdList, branchIdList);

                long recordsFiltered = recordsTotal;

                var userList = _userService.LoadAuthorizedUserBybranch(start, length, orderBy, orderDir.ToUpper(), userName, contactNo, convertedBranchId, status, convertedOrganizationId, rank, programIdList, branchIdList).ToList();

                foreach (var user in userList)
                {
                    //var str = new List<string>();
                    //var blockUsr = "";
                    //UserProfile userProfile = _userService.LoadByAspNetUser(user,false);
                    //if (userProfile != null)
                    //{
                    //    str.Add(userProfile.Branch != null ? userProfile.Branch.Organization.ShortName : "All");
                    //    str.Add(userProfile.Branch != null ? userProfile.Branch.Name : "All");
                    //    str.Add(userProfile.Campus != null ? userProfile.Campus.Name : "Main Campus");
                    //    blockUsr = userProfile.BusinessId != null ? "' class='blockUsr glyphicon glyphicon-ban-circle' title='non block user'" : "' class='blockUsr glyphicon glyphicon-ok-circle' title='block user'";
                    //}
                    //else
                    //{
                    //    str.Add("All");
                    //    str.Add("All");
                    //    str.Add("Main Campus");
                    //}
                    //str.Add(user.FullName);
                    //if (userProfile != null) str.Add(userProfile.NickName??"Empty");
                    //else str.Add("Empty");
                    //str.Add(user.UserName);
                    //str.Add(user.PhoneNumber);
                    //str.Add(StatusTypeText.GetStatusText(user.Status));
                    //var checkLockoutEnable = user.LockoutEnabled ? "' class='lockEnable glyphicon glyphicon-off'" : "' class=''";

                    //str.Add("<a href='" + Url.Action("Details", "User") + "?Id=" + user.Id + "' data-id='" + user.Id + "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;" +
                    //    "<a href='" + Url.Action("Edit", "User") + "?Id=" + user.Id + "' data-id='" + user.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; "+
                    //    "<a href='#' id='" + user.Id + "' data-name='" + user.UserName + "' data-id='" + user.Id + checkLockoutEnable + "></a>&nbsp;&nbsp; "+
                    //    "<a href='#' id='" + user.Id + "' data-name='" + user.UserName + "' data-id='" + user.Id + blockUsr + "></a>&nbsp;&nbsp; "
                    //        );
                    //getData.Add(str);
                    var str = new List<string>();
                    var forcelyChangePassword = "";
                    UserProfile userProfile = _userService.LoadByAspNetUser(user, false);
                    if (userProfile != null)
                    {
                        str.Add(userProfile.Branch != null ? userProfile.Branch.Organization.ShortName : "All");
                        str.Add(userProfile.Branch != null ? userProfile.Branch.Name : "All");
                        str.Add(userProfile.Campus != null ? userProfile.Campus.Name : "Main Campus");
                        //blockUsr = userProfile.BusinessId != null ? "' class='blockUsr glyphicon glyphicon-ban-circle' title='non block user'" : "' class='blockUsr glyphicon glyphicon-ok-circle' title='block user'";
                        forcelyChangePassword = userProfile.BusinessId == null ? "' class='forcelyPsswordChange fa fa-user' title='Force change Password'" : "' class='RemoveForcelyPsswordChange fa fa-user-times' title='remove Forcely password change'";
                    }
                    else
                    {
                        str.Add("All");
                        str.Add("All");
                        str.Add("Main Campus");
                    }
                    str.Add(user.FullName);
                    if (userProfile != null) str.Add(userProfile.NickName ?? "Empty");
                    else str.Add("Empty");
                    str.Add(user.UserName);
                    str.Add(user.PhoneNumber);
                    str.Add(StatusTypeText.GetStatusText(user.Status));
                    var blockUsr = (user.LockoutEnabled == true) ? "' class='unlockUser fa fa-lock glyphicon-off' title='Unlock user'" : "' class='lockUser fa fa-unlock glyphicon-off' title='lock user'";

                    str.Add("<a href='" + Url.Action("Details", "User") + "?Id=" + user.Id + "' data-id='" + user.Id + "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;" +
                        "<a href='" + Url.Action("Edit", "User") + "?Id=" + user.Id + "' data-id='" + user.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; " +
                        "<a href='#' id='" + user.Id + "' data-name='" + user.UserName + "' data-id='" + user.Id + forcelyChangePassword + "></a>&nbsp;&nbsp; " +
                        "<a href='#' id='" + user.Id + "' data-name='" + user.UserName + "' data-id='" + user.Id + blockUsr + "></a>&nbsp;&nbsp; "
                            );
                    getData.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData

                });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }
        public ActionResult ExportUserList(string username, string contactNo, long? branchId, string status, long? organizationId)
        {
            try
            {
                //load allProgram & all Branch Lisis based on authorization 
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                //var allBranch = false;
                //var allProgram = false;
                //var authorizedProgramLists = AuthHelper.GetUserProgramIdList(authorizeMenu, out allProgram);
                //var authorizedBranchLists = AuthHelper.GetUserBranchIdList(authorizeMenu, out allBranch);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(authorizeMenu);
                List<long> programIdList = AuthHelper.LoadProgramIdList(authorizeMenu);

                const string orderBy = "branch.Name";
                var recordsTotal = _userService.RowCountAuthorizedUserBybranch(orderBy, "", username, contactNo, branchId, status, organizationId, "", programIdList, branchIdList);
                var length = recordsTotal;
                const int start = 0;
                var userList = _userService.LoadAuthorizedUserBybranch(start, length, orderBy, "", username, contactNo, branchId, status, organizationId, "", programIdList, branchIdList).ToList();

                var headerList = new List<string> { ErpInfo.OrganizationNameFull };

                var footerList = new List<string> { "" };

                var columnList = new List<string>();
                columnList.Add("Organization");
                columnList.Add("Branch");
                columnList.Add("Campus");
                columnList.Add("Nick Name");
                columnList.Add("Full Name");
                columnList.Add("User Name");
                columnList.Add("Mobile Number");
                columnList.Add("Status");

                var userExcelList = new List<List<object>>();

                foreach (var user in userList)
                {
                    var xlsRow = new List<object>();

                    UserProfile userProfile = _userService.LoadByAspNetUser(user, false);
                    if (userProfile != null)
                    {
                        xlsRow.Add(userProfile.Branch != null ? userProfile.Branch.Organization.ShortName : "All");
                        xlsRow.Add(userProfile.Branch != null ? userProfile.Branch.Name : "All");
                        xlsRow.Add(userProfile.Campus != null ? userProfile.Campus.Name : "Main Campus");
                        xlsRow.Add(userProfile.NickName ?? "Empty");
                    }
                    else
                    {
                        xlsRow.Add("All");
                        xlsRow.Add("All");
                        xlsRow.Add("Main Campus");
                        xlsRow.Add("Empty");
                    }
                    xlsRow.Add(user.FullName);
                    xlsRow.Add(user.UserName);
                    xlsRow.Add(user.PhoneNumber);
                    xlsRow.Add(StatusTypeText.GetStatusText(user.Status));
                    userExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, userExcelList, footerList, "user-list-report__"
                        + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return View("AutoClose");
            }


        }
        #endregion

        #region Helper Function

        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
        }

        private void ViewBagOrganizationList()
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            //SelectList organization = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
            SelectList organization = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName"); //for authorize organization only
            List<SelectListItem> listOrganization = organization.ToList();
            listOrganization.Insert(0, new SelectListItem() { Value = "", Text = "Select Organization" });
            ViewBag.organizationList = new SelectList(listOrganization, "Value", "Text");
        }

        private void ViewBagEmptyBranchList()
        {
            SelectList branch = new SelectList(string.Empty, "Id", "Name");
            List<SelectListItem> listBranch = branch.ToList();
            listBranch.Insert(0, new SelectListItem() { Value = "", Text = "Select Branch" });
            ViewBag.BranchList = new SelectList(listBranch, "Value", "Text");
        }

        private void ViewBagAllActiveAutorizedProgram(IList<Program> IProgramLists, bool allProgram)
        {
            SelectList programList = new SelectList(IProgramLists, "Id", "Name");
            List<SelectListItem> listProgram = programList.ToList();

            if (allProgram)
                listProgram.Insert(0, new SelectListItem() { Value = "-1", Text = "All" });

            //ViewBag.ProgramList = new SelectList(listProgram, "Value", "Text");
            ViewBag.ProgramList = new MultiSelectList(listProgram, "Value", "Text");

        }

        private void ViewBagAllActiveAutorizedBranch(IList<Branch> IBranchLists, bool allBranch, long selectedBranchId = 0)
        {
            SelectList branchList = new SelectList(IBranchLists, "Id", "Name", selectedBranchId);
            List<SelectListItem> list = branchList.ToList();

            if (allBranch)
                list.Insert(0, new SelectListItem() { Value = "", Text = "Select Branch" });
            ViewBag.BranchList2 = new MultiSelectList(list, "Value", "Text");

            List<SelectListItem> list2 = branchList.ToList();
            if (allBranch)
                list2.Insert(0, new SelectListItem() { Value = "-1", Text = "All" });
            ViewBag.BranchList = new MultiSelectList(list2, "Value", "Text");

        }

        private void ViewBagUserDisplayImage(UserProfile user)
        {
            EmployeeMediaImage mediaEmployeeImages = _employeeImageMediaService.GetEmployeeImageMediaByRefId(user.UserImages[0].Id);
            if (mediaEmployeeImages != null)
            {
                byte[] bytes = mediaEmployeeImages.Images;
                string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                ViewBag.DisplayImage = string.Format("data:image/png;base64,{0}", base64String);
            }

        }

        private bool CheckUserEmailDuplicate(UserProfile user, UserUpdateForm userUpdateForm)
        {
            var checkEmail = _userService.HasDuplicateByEmail(userUpdateForm.Email, userUpdateForm.Id);
            if (checkEmail == false)
            {
                if (user.UserImages.Count > 0)
                {
                    //byte[] bytes = user.UserImages[0].UserImages;
                    //string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    //ViewBag.DisplayImage = string.Format("data:image/png;base64,{0}", base64String);
                    ViewBagUserDisplayImage(user);
                }
                TempData["UserErrorMessage"] = "Email already exists";
                if (userUpdateForm.CampusId != -1)
                {
                    IList<Campus> campusLists = _campusService.LoadCampus(null, null, null, _commonHelper.ConvertIdToList(userUpdateForm.BranchId), null, false);
                    ViewBagcampusList(campusLists, userUpdateForm.CampusId);
                }

            }
            return checkEmail;
        }

        private bool CheckUserMobileNo(UserProfile user, UserUpdateForm userUpdateForm)
        {
            bool checkContactNo = _userService.HasDuplicateByContactNo(userUpdateForm.ContactNo, userUpdateForm.Id);
            if (!checkContactNo)
            {
                if (user.UserImages.Count > 0)
                {
                    //byte[] bytes = user.UserImages[0].UserImages;
                    //string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    //ViewBag.DisplayImage = string.Format("data:image/png;base64,{0}", base64String);
                    ViewBagUserDisplayImage(user);
                }
                TempData["UserErrorMessage"] = "Contact Number exist,Please enter a different contact number";
                if (userUpdateForm.CampusId != -1)
                {
                    IList<Campus> campusLists = _campusService.LoadCampus(null, null, null, _commonHelper.ConvertIdToList(userUpdateForm.BranchId), null, false);
                    ViewBagcampusList(campusLists, userUpdateForm.CampusId);
                }
            }

            return checkContactNo;
        }


        private void ViewBagcampusList(IList<Campus> ICampusLists, long selectedCampusId = 0)
        {
            SelectList campus = new SelectList(ICampusLists, "Id", "Name", selectedCampusId);
            List<SelectListItem> _listCampus = campus.ToList();
            ViewBag.campusList = new SelectList(_listCampus, "Value", "Text");
        }

        private void ViewBagcampusListWithListCout(IList<Campus> ICampusLists, long selectedCampusId = 0)
        {
            SelectList campus = new SelectList(ICampusLists, "Id", "Name", selectedCampusId);
            List<SelectListItem> _listCampus = campus.ToList();
            _listCampus.Insert(0, new SelectListItem() { Value = "", Text = "Select Campus" });
            ViewBag.campusList = new SelectList(_listCampus, "Value", "Text");
            ViewBag.CampusListCount = _listCampus.Count;
        }

        private void ViewBagEmptyCampusLists()
        {
            SelectList campus = new SelectList(string.Empty, "Id", "Name");
            List<SelectListItem> listCampus = campus.ToList();
            listCampus.Insert(0, new SelectListItem() { Value = "", Text = "Select Campus" });
            ViewBag.campusList = new SelectList(listCampus, "Value", "Text");
        }

        private void ViewBagEmptyCampusListsWithCount()
        {
            SelectList campus = new SelectList(string.Empty, "Id", "Name");
            List<SelectListItem> listCampus = campus.ToList();
            listCampus.Insert(0, new SelectListItem() { Value = "", Text = "Select Campus" });
            ViewBag.campusList = new SelectList(listCampus, "Value", "Text");
            ViewBag.CampusListCount = listCampus.Count;
        }

        private byte[] ConvertFileInByteArray(Stream inputStream, int contentLength)
        {
            try
            {
                byte[] file = null;
                using (var binaryReader = new BinaryReader(inputStream))
                {
                    file = binaryReader.ReadBytes(contentLength);
                }
                return file;
            }
            catch (Exception e)
            {
                logger.Error(e);
                Console.Write(e.ToString());
                throw;
            }
        }
        //private void AddErrors(IdentityResult result)
        //{
        //    foreach (var error in result.Errors)
        //    {
        //        ModelState.AddModelError("", error);
        //    }
        //}

        #endregion

        #region No Need

        //private void UpdateUserPermissionList(long userId, long? orId, long? prId, long? brId, IEnumerable<UserPermissionListViewModel> userPermissionListViewModels)
        //{

        //    foreach (var value in userPermissionListViewModels)
        //    {
        //        bool checkedAccess = _userMenuService.CheckMenuByUserBranchProgram(userId, orId, brId, prId, value.Id);

        //        if (value.CheckedValue && checkedAccess == false)
        //        {
        //            Program program = new Program();
        //            Branch branch = new Branch();
        //            Organization organization = new Organization();

        //            UserProfile userProfile = _userService.LoadById(userId);
        //            if (orId != null) organization = _organizationService.LoadById((long)orId);
        //            if (brId != null) branch = _branchService.GetBranch((long)brId);
        //            if (prId != null) program = _programService.GetProgram((long)prId);

        //            Menu menu = _menuService.MenuLoadById(value.Id);
        //            UserMenu um = new UserMenu();
        //            um.User = userProfile;
        //            um.Menu = menu;
        //            um.Organization = organization;
        //            um.Branch = branch;
        //            um.Program = program;

        //            _userMenuService.Save(um);
        //            var checkUserMenu = _userMenuService.LoadUsrMenuByUsrAndMenuNullBP(userProfile, menu, um.Id);
        //            if (checkUserMenu != null && checkUserMenu.Count > 0)
        //            {
        //                if (organization == null && program == null && branch == null)
        //                {
        //                    foreach (var userMenu in checkUserMenu)
        //                    {
        //                        var usrMenu = _userMenuService.LoadById(userMenu.Id);
        //                        _userMenuService.Delete(userMenu.Id, usrMenu);
        //                    }
        //                }
        //                else if (organization == null && program == null && branch != null)
        //                {
        //                    foreach (var userMenu in checkUserMenu)
        //                    {
        //                        if (userMenu.Organization != null || userMenu.Program != null || userMenu.Branch == null)
        //                        {
        //                            var usrMenu = _userMenuService.LoadById(userMenu.Id);
        //                            _userMenuService.Delete(userMenu.Id, usrMenu);
        //                        }
        //                    }
        //                }
        //                else if (organization == null && program != null && branch != null)
        //                {
        //                    foreach (var userMenu in checkUserMenu)
        //                    {
        //                        if (userMenu.Organization != null || userMenu.Program == null || userMenu.Branch == null)
        //                        {
        //                            var usrMenu = _userMenuService.LoadById(userMenu.Id);
        //                            _userMenuService.Delete(userMenu.Id, usrMenu);
        //                        }
        //                    }
        //                }
        //                else if (organization == null && program != null && branch == null)
        //                {
        //                    foreach (var userMenu in checkUserMenu)
        //                    {
        //                        if (userMenu.Organization != null || userMenu.Program == null || userMenu.Branch != null)
        //                        {
        //                            var usrMenu = _userMenuService.LoadById(userMenu.Id);
        //                            _userMenuService.Delete(userMenu.Id, usrMenu);
        //                        }
        //                    }
        //                }
        //                else if (organization != null && program != null && branch != null)
        //                {
        //                    foreach (var userMenu in checkUserMenu)
        //                    {
        //                        if (userMenu.Organization == null || userMenu.Program == null || userMenu.Branch == null)
        //                        {
        //                            var usrMenu = _userMenuService.LoadById(userMenu.Id);
        //                            _userMenuService.Delete(userMenu.Id, usrMenu);
        //                        }
        //                    }
        //                }
        //                else if (organization != null && program == null && branch == null)
        //                {
        //                    foreach (var userMenu in checkUserMenu)
        //                    {
        //                        if (userMenu.Organization == null || userMenu.Program != null || userMenu.Branch != null)
        //                        {
        //                            var usrMenu = _userMenuService.LoadById(userMenu.Id);
        //                            _userMenuService.Delete(userMenu.Id, usrMenu);
        //                        }
        //                    }
        //                }
        //                else if (organization != null && program == null && branch != null)
        //                {
        //                    foreach (var userMenu in checkUserMenu)
        //                    {
        //                        if (userMenu.Organization == null || userMenu.Program != null || userMenu.Branch == null)
        //                        {
        //                            var usrMenu = _userMenuService.LoadById(userMenu.Id);
        //                            _userMenuService.Delete(userMenu.Id, usrMenu);
        //                        }
        //                    }
        //                }
        //                else if (organization != null && program != null && branch == null)
        //                {
        //                    foreach (var userMenu in checkUserMenu)
        //                    {
        //                        if (userMenu.Organization == null || userMenu.Program == null || userMenu.Branch != null)
        //                        {
        //                            var usrMenu = _userMenuService.LoadById(userMenu.Id);
        //                            _userMenuService.Delete(userMenu.Id, usrMenu);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        else if (checkedAccess && value.CheckedValue == false)
        //        {
        //            var userMenu = _userMenuService.LoadDataByUserBranchProgram(userId, orId, brId, prId, value.Id);
        //            _userMenuService.Delete(userMenu.Id, userMenu);
        //        }
        //    }
        //}

        #endregion


    }
}