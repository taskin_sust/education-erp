﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.DirectGoodsIssueTest
{
    public interface IGoodsIssueReportTest
    {
        void GoodIssueReport_Count_Should_Be_Greater_Than_Zero();
        void Load_Goods_Issue_Report_Should_Return_List();
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "DirectGoodsIssue")]
    public class GoodsIssueReportTest : DirectGoodsIssueBaseTest, IGoodsIssueReportTest
    {
        [Fact]
        public void GoodIssueReport_Count_Should_Be_Greater_Than_Zero()
        {
            _goodsIssueFactory.CreateMore(5).Persist();
            var goodsIssue = _goodsIssueFactory.GetLastCreatedObjectList()[0];
            var branch = goodsIssue.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var purposeList = new List<long>() {(int)goodsIssue.GoodsIssueDetails[0].Purpose};
            var itemGroupIdList = new List<long>() { goodsIssue.GoodsIssueDetails[0].Item.ItemGroupId };
            var itemIdList = new List<long>() { goodsIssue.GoodsIssueDetails[0].Item.Id };
            var program = goodsIssue.GoodsIssueDetails[0].Program;
            var programIdList = new List<long>() { program.Id };
            var sessionIdList = new List<long>() { goodsIssue.GoodsIssueDetails[0].Session.Id };

            var dateFrom = goodsIssue.CreationDate;
            var dateTo = DateTime.Now;
            //var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object,_sessionFactory.Object);
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            //IList<dynamic> list = _goodsIssueService.LoadGoodsIssueProgramWiseReport(0,10,userMenu,organizationId,new List<long>(){branchId},(int)GoodsIssueType.DirectGoodsIssue,purposeList,itemGroupIdList,itemIdList,programIdList,sessionIdList,(int)ReportType.ProgramWise,dateFrom,dateTo,programSessionList);
            //Assert.True(list.Count >= 5);

        }

        [Fact]
        public void Load_Goods_Issue_Report_Should_Return_List()
        {
            var goodsIssue = _goodsIssueFactory.GetLastCreatedObjectList()[0];
            var branch = goodsIssue.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var purposeList = new List<long>() { (int)goodsIssue.GoodsIssueDetails[0].Purpose };
            var itemGroupIdList = new List<long>() { goodsIssue.GoodsIssueDetails[0].Item.ItemGroupId };
            var itemIdList = new List<long>() { goodsIssue.GoodsIssueDetails[0].Item.Id };
            var program = goodsIssue.GoodsIssueDetails[0].Program;
            var programIdList = new List<long>() { program.Id };
            var sessionIdList = new List<long>() { goodsIssue.GoodsIssueDetails[0].Session.Id };
            var dateFrom = goodsIssue.CreationDate;
            var dateTo = DateTime.Now;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            var gsList = _goodsIssueService.LoadGoodsIssueBranchWiseReport(null, 0,10,userMenu,organizationId,new List<long>(){branchId},(int)GoodsIssueType.DirectGoodsIssue,purposeList,itemGroupIdList,itemIdList,programIdList,sessionIdList,(int)ReportType.BranchWise,dateFrom,dateTo,null);
            Assert.True(gsList.Count >= 10);


        }
    }
}
