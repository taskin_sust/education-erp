﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class AttendanceSynchronizerSettingFactory : ObjectFactoryBase<AttendanceSynchronizerSetting>
    {
        private IAttendanceSynchronizerSettingService _attendanceSynchronizerSettingService;
        public AttendanceSynchronizerSettingFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _attendanceSynchronizerSettingService = new AttendanceSynchronizerSettingService(session);
        }
        #region Create

        public AttendanceSynchronizerSettingFactory Create()
        {
            Random random = new Random();
            Object = new AttendanceSynchronizerSetting()
            {
                ReleaseDate = DateTime.Now,
                UpdateUrl = "www.randomurl.com" + "/" + random.Next(1000000),
                SynchronizerVersion = 1.0 + random.Next(1000000),
                Rank = 1,
                Name = Name,
                Description = "AttendanceSynchronizer Test"
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public AttendanceSynchronizerSettingFactory Create(long id, string name, DateTime releaseDate, string updateUrl,
            double synchronizerVersion)
        {
            Random random = new Random();
            Object = new AttendanceSynchronizerSetting
            {
                Id=id,
                Name = name,
                ReleaseDate = releaseDate,
                UpdateUrl = updateUrl,
                SynchronizerVersion = synchronizerVersion
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public AttendanceSynchronizerSettingFactory Persist()
        {

            SingleObjectList.ForEach(item =>
            {
                if (item.Id == 0)
                {
                    _attendanceSynchronizerSettingService.Save(item);
                }
            });

            ObjectList.ForEach(item =>
            {
                if (item.Id == 0)
                {
                    _attendanceSynchronizerSettingService.Save(item);
                }
            });
            return this;
        }
        #endregion

        #region Others
        #endregion

        #region CleanUp


        public void Cleanup()
        {
            DeleteAll();
        }
        #endregion


    }
}
