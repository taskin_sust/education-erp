﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.GoodsReturnTest
{
    public interface IGoodsReturnLoadActiveTest
    {
        void Load_WorkOrder_Should_Return_List();
    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReturn")]
    public class GoodsReturnLoadActiveTest : GoodsReturnBaseTest, IGoodsReturnLoadActiveTest
    {
        #region Basic Test

        [Fact]
        public void Load_WorkOrder_Should_Return_List()
        {
            _goodsReturnFactory.CreateMore(5).Persist();
            var goodsReturn = _goodsReturnFactory.GetLastCreatedObjectList()[0];
            var branch = goodsReturn.GoodsReceive.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var usermenu = BuildUserMenu(branch.Organization, goodsReturn.GoodsReceive.GoodsReceiveDetails[0].Program, branch);
            var grList = _goodsReturnService.LoadGoodsReturn(0, 10, "", "", usermenu, organizationId, branchId, "", null, null);
            Assert.True(grList.Count >= 5);
        }

        #endregion
    }
}
