﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class ItemController : Controller
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IItemGroupService _itemGroupService;
        private readonly ItemService _itemService;
        private readonly IUserService _userService;
        private readonly IOrganizationService _organizationService;
        private ItemGroup _itemGroup;
        private Item _item;
        private IList<Item> _itemList;
        private List<UserMenu> authorizeMenu;

        public ItemController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _itemGroupService = new ItemGroupService(session);
            _userService = new UserService(session);
            _itemService = new ItemService(session);
            _organizationService = new OrganizationService(session);
            _item = new Item();
            _itemList = new List<Item>();
            authorizeMenu = new List<UserMenu>();
        }

        #endregion

        #region Index/Manage Page

        public ActionResult Index()
        {
            try
            {
                InitializeView();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        #endregion

        #region Render Table

        public JsonResult ItemList(int draw, int start, int length,long? organization,int? itemType,long? itemGroup, string name, int? status)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            int recordsFiltered = 0;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Organization.Name";
                            break;
                        case "1":
                            orderBy = "ItemType";
                            break;
                        case "2":
                            orderBy = "itemGroup.Name";
                            break;
                        case "3":
                            orderBy = "Name";
                            break;
                        case "4":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion
                
                recordsTotal = _itemService.LoadItemCount(organization,itemType,itemGroup, name, status);
                recordsFiltered = recordsTotal;
                _itemList = _itemService.LoadItem(start, length, orderBy, orderDir.ToUpper(), organization, itemType, itemGroup, name, status).ToList();
                
                foreach (var item in _itemList)
                {
                    var str = new List<string>();
                    str.Add(item.Organization != null ? item.Organization.ShortName : "All");
                    str.Add(_commonHelper.GetEmumIdToValue<ItemType>(item.ItemType));
                    str.Add(item.ItemGroup.Name);
                    str.Add(item.Name);
                    str.Add(StatusTypeText.GetStatusText(item.Status));
                    str.Add(
                        "<a href='" + Url.Action("Details", "Item") + "?Id=" + item.Id + "' data-id='" + item.Id +
                        "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;&nbsp"
                        +
                        "<a href='" + Url.Action("Edit", "Item") + "?Id=" + item.Id + "' data-id='" + item.Id +
                        "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; "
                        +
                        "<a  id='" + item.Id.ToString() + "'    href='#' data-name='" + item.Name.ToString() +
                        "' class='glyphicon glyphicon-trash'></a> "
                        );
                    getData.Add(str);
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData
            });
        }

        #endregion

        #region Details Operation

        public ActionResult Details(int id)
        {
            try
            {
                _item = _itemService.LoadById(Convert.ToInt64(id));
                if (_item != null)
                {
                    _item.CreateByText = _userService.GetUserNameByAspNetUserId(_item.CreateBy);
                    _item.ModifyByText = _userService.GetUserNameByAspNetUserId(_item.ModifyBy);
                }
                else
                {
                    ViewBag.ErrorMessage = "Invalid Id";
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View(_item);
        }

        #endregion

        #region Create Operation

        public ActionResult Create(string message)
        {
            try
            {
                InitializeView();
                ViewBag.SuccessMessage = message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(Item itemObj)
        {
            string errorMessage = "";
            try
            {
                itemObj.Organization = itemObj.OrganizationId == SelectionType.SelelectAll ? null : _organizationService.LoadById(itemObj.OrganizationId);
                itemObj.ItemGroup = _itemGroupService.LoadById(itemObj.ItemGroupId);
                InitializeView();
                if (ModelState.IsValid)
                {
                    bool isSaved = _itemService.SaveOrUpdate(new List<Item>() { itemObj });
                    return isSaved ? Json(new { isSuccess = true, returnMessage = "Item saved successfully." }) : Json(new Response(false, "Item submit failed."));
                }
            }
            catch (MessageException ex)
            {
                errorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                errorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                errorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            return Json(new Response(false, errorMessage));
        }

        #endregion

        #region Update Operation

        public ActionResult Edit(int id)
        {
            _item = _itemService.LoadById(id);
            if (_item == null)
            {
                ViewBag.Errormessage = "Invalid id";
                return RedirectToAction("Index");
            }
            InitializeUpdateView(_item);
            _item.OrganizationId = _item.Organization==null?SelectionType.SelelectAll: _item.Organization.Id;
            _item.ItemGroupId = _item.ItemGroup.Id;
            return View(_item);
        }
        
        [HttpPost]
        public ActionResult Edit(int id, Item itemObj)
        {
            try
            {
                itemObj.Organization = _organizationService.LoadById(itemObj.OrganizationId);
                itemObj.ItemGroup = _itemGroupService.LoadById(itemObj.ItemGroupId);
                InitializeUpdateView(itemObj);
                _item = _itemService.LoadById(Convert.ToInt64(id));
                if (ModelState.IsValid && _item != null)
                {

                    bool isUpdated = _itemService.SaveOrUpdate(new List<Item>() { itemObj });
                    if (isUpdated)
                    {
                        ViewBag.SuccessMessage = "Item successfully updated.";
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "Problem Occurred, during item update.";
                }
            }
            catch (MessageException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View(_item);
        }

        #endregion

        #region Delete Operation

        public ActionResult Delete(long id)
        {
            try
            {
                bool isDeleted = _itemService.IsDelete(id);
                if (isDeleted)
                {
                    return Json(new Response(true, "Item sucessfully deleted."));
                }
                return Json(new Response(true, "Problem Occurred. Retry"));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
               _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Helper Function

        private void InitializeView()
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;

            var authorgList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
            List<SelectListItem> orgList = authorgList.ToList();
            orgList.Insert(0,new SelectListItem(){Value = "0",Text = "All organization"});
            ViewBag.organizationList = new SelectList(orgList, "Value", "Text");

            ViewBag.itemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
            ViewBag.itemTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<ItemType>(), "Key", "Value");
            ViewBag.itemUnitList = new SelectList(_commonHelper.LoadEmumToDictionary<ItemUnit>(), "Key", "Value");
            ViewBag.costBearerList = new SelectList(_commonHelper.LoadEmumToDictionary<CostBearer>(), "Key", "Value");
            ViewBag.StatusText = _commonHelper.GetStatus();
        }

        private void InitializeUpdateView(Item item)
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;

            var authorgList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
            List<SelectListItem> orgList = authorgList.ToList();
            orgList.Insert(0, new SelectListItem() { Value = "0", Text = "All organization" });
            ViewBag.organizationList = new SelectList(orgList, "Value", "Text", item.Organization == null ? SelectionType.NotApplicable : item.Organization.Id);

            ViewBag.itemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name",item.ItemGroup.Id);
            ViewBag.itemTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<ItemType>(), "Key", "Value",item.ItemType);
            ViewBag.itemUnitList = new SelectList(_commonHelper.LoadEmumToDictionary<ItemUnit>(), "Key", "Value",item.ItemUnit);
            ViewBag.costBearerList = new SelectList(_commonHelper.LoadEmumToDictionary<CostBearer>(), "Key", "Value",item.CostBearer);
            ViewBag.StatusText = _commonHelper.GetStatus();
        }

        #endregion

    }
}
