﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.OrderedGoodsReceive
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReceive")]
    public class OrderedGoodsReceiveLoadById : OrderedGoodsReceiveBaseTest
    {
        [Fact]
        public void Should_Return_Invalid_Good_Receive_No()
        {
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.GetGoodsReceive(""));
        }

        [Fact]
        public void Should_Return_Invalid_Length_Good_Receive_No()
        {
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.GetGoodsReceive("1"));
        }

        [Fact]
        public void Should_Return_Invalid_Good_Receive_Id()
        {
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.GetGoodsReceive(-1));
        }

        [Fact]
        public void Should_Return_Good_Receive_Object_Successfully()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);
            Assert.Equal(_goodsReceiveFactory.Object.GoodsReceivedNo, _goodsReceiveService.GetGoodsReceive(_goodsReceiveFactory.Object.GoodsReceivedNo).GoodsReceivedNo);
            Assert.Equal(_goodsReceiveFactory.Object.Id, _goodsReceiveService.GetGoodsReceive(_goodsReceiveFactory.Object.Id).Id);
        }
    }
}
