﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Hr.AttendanceSummaryServices;
using UdvashERP.Services.Teachers;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class CommonAjaxHrController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        const string DropDownOptionView = "Partial/_leaveDetails";
        private readonly ITeamMemberService _teamMemberService;
        private List<UserMenu> _userMenu;
        private readonly ICommonHelper _commonHelper;
        private ISession _session;
        private readonly IMentorHistoryService _mentorHistoryService;
        private readonly IAttendanceSummaryService _attendanceSummaryService;
        private readonly IShiftService _shiftService;
        private readonly IDesignationService _designationService;
        private readonly IDepartmentService _departmentService;

        public CommonAjaxHrController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _teamMemberService = new TeamMemberService(_session);
            _commonHelper = new CommonHelper();
            _mentorHistoryService = new MentorHistoryService(_session);
            _attendanceSummaryService = new AttendanceSummaryService(_session);
            _shiftService = new ShiftService(_session);
            _designationService = new DesignationService(_session);
            _departmentService = new DepartmentService(_session);
        }

        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetMemberSalaryInformation(int pin, bool isOrganization = false,
            bool isBranch = false, bool isCampus = false, bool isDepartment = false,
            bool isDesignation = false, bool isName = false, string searchingDate = "", string joiningDate = "")
        {
            bool successState = true;
            string organizationName = "";
            string branchName = "";
            string campusName = "";
            string memberName = "";
            string designationName = "";
            string message = "";
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (Request.IsAjaxRequest())
                {
                    DateTime dateTime = DateTime.Now;
                    if (!String.IsNullOrEmpty(searchingDate))
                    {
                        dateTime = Convert.ToDateTime(searchingDate);
                    }
                    if (pin <= 0)
                        throw new InvalidDataException("Invalid PIN");
                    TeamMember member = null;
                    List<TeamMember> authorizedTeamMemberList;

                    authorizedTeamMemberList = _teamMemberService.LoadHrAuthorizedTeamMember(_userMenu, null, null, null, null, null, null, _commonHelper.ConvertIdToList(pin)).ToList();
                    if (authorizedTeamMemberList.Any())
                    {
                        member = authorizedTeamMemberList[0];
                    }
                    if (member != null)
                    {
                        if (isName)
                        {
                            memberName = member.Name;
                        }
                        if (isOrganization)
                        {
                            Organization org =
                                _teamMemberService.GetTeamMemberSalaryOrganization(dateTime.ToString("yyyy-MM-dd"), member.Id);
                            if (org != null)
                                organizationName = org.ShortName;
                            else
                            {
                                successState = false;
                                message = "current team member don't have any organization";
                            }
                        }

                        if (isBranch)
                        {
                            Branch branch = _teamMemberService.GetTeamMemberSalaryBranch(dateTime.ToString("yyyy-MM-dd"),
                                member.Id);
                            if (branch != null)
                                branchName = branch.Name;
                            else { successState = false; message = "current team member don't have any branch"; }
                        }

                        if (isCampus)
                        {
                            Campus campus = _teamMemberService.GetTeamMemberSalaryCampus(dateTime.ToString("yyyy-MM-dd"),
                                member.Id);
                            if (campus != null)
                                campusName = campus.Name;
                            else { successState = false; message = "current team member don't have any campus"; }
                        }
                        if (isDesignation)
                        {
                            Designation designation = _teamMemberService.GetTeamMemberSalaryDesignation(dateTime.ToString("yyyy-MM-dd"), member.Id);
                            if (designation != null)
                                designationName = designation.ShortName;
                            else { successState = false; message = "current team member don't have any designation"; }
                        }

                    }
                    else
                    {
                        message = "don't have enough permission to see this member data";
                        successState = false;
                    }
                }
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return Json(new
            {
                IsSuccess = successState,
                Message = message,
                organizationName = organizationName,
                branchName = branchName,
                campusName = campusName,
                memberName = memberName,
                designationName = designationName

            });

        }

        [HttpPost]
        public JsonResult GetTeamMemberInformation(int pin, bool isHr = false, bool isMentor = false, bool isOrganization = false,
            bool isBranch = false, bool isCampus = false, bool isDepartment = false,
            bool isDesignation = false, string searchingDate = "", string joiningDate = "", bool isLeaveSummaryInfo = false, bool isInsteadDate = false)
        {
            string message = "";
            string teamMemberName = "";
            long teamMemberId = 0;
            bool successState = false;
            var leaveTypelist = "";
            SelectList insteadDaylist = new SelectList(new List<AttendanceSummary>());
            string organizationName = "";
            string branchName = "";
            string campusName = "";
            string departmentName = "";
            string designationName = "";
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                DateTime dateTime = DateTime.Now;
                if (!String.IsNullOrEmpty(searchingDate))
                {
                    dateTime = Convert.ToDateTime(searchingDate);
                }

                if (pin <= 0)
                    throw new InvalidDataException("Invalid PIN");

                TeamMember member = null;
                List<TeamMember> authorizedTeamMemberList;
                if (isHr)
                {
                    authorizedTeamMemberList = _teamMemberService.LoadHrAuthorizedTeamMember(_userMenu, null, null, null, null, null, null, _commonHelper.ConvertIdToList(pin)).ToList();
                    if (authorizedTeamMemberList.Any())
                    {
                        member = authorizedTeamMemberList[0];
                    }

                }
                else if (isMentor)
                {
                    TeamMember mentor = _teamMemberService.GetCurrentMember();
                    authorizedTeamMemberList = _mentorHistoryService.LoadMentorTeamMember(_commonHelper.ConvertIdToList(mentor.Id), dateTime, null, _commonHelper.ConvertIdToList(pin));
                    if (authorizedTeamMemberList.Any())
                    {
                        member = authorizedTeamMemberList[0];
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(joiningDate))
                    {
                        member = _teamMemberService.GetByPin(pin, Convert.ToDateTime(joiningDate).Date);
                        if (member == null)
                        {
                            message = "No Team Member is found during this time (" + Convert.ToDateTime(joiningDate).Date.ToString("dd MMM, yyyy") + ") ";
                        }
                    }
                    else
                    {
                        member = _teamMemberService.GetByPin(pin);
                    }

                }

                if (member != null)
                {
                    teamMemberName = member.Name;
                    teamMemberId = member.Id;


                    if (isLeaveSummaryInfo == true)
                    {
                        IList<LeaveTypeDtoList> leavePendingListDtos = _teamMemberService.LoadMembersPendingLeave(member.Id, isHr);
                        leaveTypelist = this.RenderRazorViewToOptions(DropDownOptionView, leavePendingListDtos, 0, "Select Leave Type");
                    }

                    if (isOrganization)
                    {
                        Organization org = _teamMemberService.GetTeamMemberOrganization(dateTime.ToString("yyyy-MM-dd"), member.Id);
                        if (org != null)
                            organizationName = org.ShortName;
                    }

                    if (isBranch)
                    {
                        Branch branch = _teamMemberService.GetTeamMemberBranch(dateTime.ToString("yyyy-MM-dd"), member.Id);
                        if (branch != null)
                            branchName = branch.Name;
                    }

                    if (isCampus)
                    {
                        Campus campus = _teamMemberService.GetTeamMemberCampus(dateTime.ToString("yyyy-MM-dd"), member.Id);
                        if (campus != null)
                            campusName = campus.Name;
                    }

                    if (isDepartment)
                    {
                        Department department = _teamMemberService.GetTeamMemberDepartment(dateTime.ToString("yyyy-MM-dd"), member.Id);
                        if (department != null)
                            departmentName = department.Name;
                    }

                    if (isDesignation)
                    {
                        Designation designation = _teamMemberService.GetTeamMemberDesignation(dateTime.ToString("yyyy-MM-dd"), member.Id);
                        if (designation != null)
                            designationName = designation.Name;
                    }

                    if (isInsteadDate)
                    {
                        IList<AttendanceSummary> list = _attendanceSummaryService.LoadTeamMemberDayOffAttendanceSummary(member);
                        insteadDaylist = new SelectList(list.ToList(), "Id", "ModifiedInsteadOf");
                    }
                    successState = true;
                }

            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new
            {
                IsSuccess = successState
                ,
                Message = message
                ,
                teamMemberName = teamMemberName
                ,
                teamMemberId = teamMemberId
                ,
                leaveTypelist = leaveTypelist
                ,
                organizationName = organizationName
                ,
                branchName = branchName
                ,
                campusName = campusName
                ,
                departmentName = departmentName
                ,
                designationName = designationName
                ,
                insteadDaylist = insteadDaylist
            });
        }

        public JsonResult LoadShift(List<long> organizationIds)
        {
            try
            {
                IList<Shift> shiftList = _shiftService.LoadShift(organizationIds);
                var shiftSelectList = new SelectList(shiftList.ToList(), "Id", "Name");
                return Json(new { returnShiftList = shiftSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        public JsonResult LoadDesignation(List<long> organizationIds = null)
        {
            try
            {
                IList<Designation> designationList = _designationService.LoadDesignation(organizationIds);
                var designationSelectList = new SelectList(designationList.ToList(), "Id", "Name");
                return Json(new { returnResult = designationSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred During Load Designation";
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public JsonResult LoadDepartment(List<long> organizationIds = null)
        {
            try
            {
                IList<Department> departments = _departmentService.LoadDepartment(organizationIds);
                var departmentsSelectList = new SelectList(departments.ToList(), "Id", "Name");
                return Json(new { returnResult = departmentsSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
    }
}