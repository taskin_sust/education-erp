﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Administration.ProgramSettingTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "ProgramSetting")]
    public class ProgramSettingLoadTest : ProgramSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Get_ProgramSetting_Should_Return_True()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);
            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, nextProgramSession).Persist();
            var programSettingObj = _programSettingService.GetProgramSetting(Convert.ToInt32(_programSettingFactory.Object.Id));
            Assert.NotNull(programSettingObj);
            Assert.Equal(programSettingObj.Id, _programSettingFactory.Object.Id);
        }
        [Fact]
        public void Load_ProgramSetting_Should_Return_True()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);
            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, nextProgramSession).Persist();
            var programSettingObj = _programSettingService
                                    .LoadProgramSetting(Convert.ToInt32(_programFactory.ObjectList[0].Id)
                                                        , Convert.ToInt32(_sessionFactory.ObjectList[0].Id));
            Assert.NotEqual(programSettingObj.Count, 0);
        }
        [Fact]
        public void Count_ProgramSetting_Should_Return_True()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);
            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, nextProgramSession).Persist();
            var programSettingObjCount = _programSettingService
                                    .GetProgramSettingCount(Convert.ToInt32(_programFactory.ObjectList[0].Id)
                                                        , Convert.ToInt32(_sessionFactory.ObjectList[0].Id));
            Assert.NotEqual(programSettingObjCount, 0);
        }
        [Fact]
        public void Load_PreviousProgramSetting_Should_Return_True()
        {
            _programFactory.CreateMore(3).Persist();
            var firstProgram = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var secoundProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);
            var thirdProgram = _programService.GetProgram(_programFactory.ObjectList[2].Id);

            _sessionFactory.CreateMore(3).Persist();
            var firstProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var secondProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);
            var thirdProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[2].Id);
            
            var programList = new List<Program>();
            var nextProgramList = new List<Program>();

            programList.Add(firstProgram);
            programList.Add(secoundProgram);
            nextProgramList.Add(secoundProgram);
            nextProgramList.Add(thirdProgram);
            
            var sessionList = new List<Session>();
            var nextSessionList = new List<Session>();

            sessionList.Add(firstProgramSession);
            sessionList.Add(secondProgramSession);
            nextSessionList.Add(secondProgramSession);
            nextSessionList.Add(thirdProgramSession);

            _programSettingFactory.CreateMore(2, programList, sessionList, nextProgramList, nextSessionList).Persist();
            var programSettingObj = _programSettingService
                                    .LoadPreviousProgramSettings(
                                            Convert.ToInt32(secoundProgram.Id),
                                            Convert.ToInt32(secondProgramSession.Id));
            Assert.Equal(programSettingObj[0].Id, _programSettingFactory.ObjectList[0].Id);
        }
        #endregion
        #region Business Logic Test

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Get_ProgramSetting_Should_Return_Invalid_ProgramSettings(int id)
        {
            Assert.Throws<UdvashERP.MessageExceptions.InvalidDataException>(() => _programSettingService.GetProgramSetting(id));
        }

        [Theory]
        [InlineData(0, 1)]
        [InlineData(-1, 1)]
        [InlineData(1, 0)]
        [InlineData(1, -1)]
        public void Load_ProgramSetting_Should_Return_Invalid_ProgramSettings(int programId, int sessionId)
        {
            Assert.Throws<UdvashERP.MessageExceptions.InvalidDataException>(() => _programSettingService.LoadProgramSetting(programId, sessionId));
        }
        [Theory]
        [InlineData(0, 1)]
        [InlineData(-1, 1)]
        [InlineData(1, 0)]
        [InlineData(1, -1)]
        public void Load_PreviousProgramSetting_Should_Return_Invalid_ProgramSettings(int programId, int sessionId)
        {
            Assert.Throws<InvalidDataException>(() => _programSettingService.LoadPreviousProgramSettings(programId, sessionId));
        }
        [Theory]
        [InlineData(0, 1)]
        [InlineData(-1, 1)]
        [InlineData(1, 0)]
        [InlineData(1, -1)]
        public void Count_ProgramSetting_Should_Return_Invalid_ProgramSettings(int programId, int sessionId)
        {
            Assert.Throws<InvalidDataException>(() => _programSettingService.GetProgramSettingCount(programId, sessionId));
        }
        #endregion
    }
}
