﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{

    public interface IAttendanceDeviceLogDataDao : IBaseDao<BusinessModel.Entity.Hr.AttendanceDeviceLogData, long>
    {

    }
    public class AttendanceDeviceLogDataDao : BaseDao<BusinessModel.Entity.Hr.AttendanceDeviceLogData, long>, IAttendanceDeviceLogDataDao
    {

    }
}
