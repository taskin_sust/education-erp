﻿using System;
using System.Collections.Generic;
using System.Globalization;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class SessionFactory : ObjectFactoryBase<Session>
    {
        #region Object Initialization

        protected readonly ISessionService _sessionService;
        //internal readonly string _code;
        public readonly List<string> assignCodeList;
        public SessionFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _sessionService = new SessionService(_session);
            //_code = GetRandomNumber();
            assignCodeList = new List<string>();
        }

        #endregion

        #region Create

        public SessionFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }
        public SessionFactory WithCode(string code)
        {
            Object.Code = code;
            return this;
        }
        public SessionFactory Create()
        {
            Object = new Session()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString(),
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Code = GetRandomNumber(),
                Status = Branch.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }


        public SessionFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _sessionService.Save(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _sessionService.Save(item);
                    }
                }
            }
            return this;
        }

        public SessionFactory CreateMore(int numberOfSession)
        {
            CreateSessionList(numberOfSession);
            return this;
        }

        private List<Session> CreateSessionList(int numberOfSession)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Session>();
            for (int i = 0; i < numberOfSession; )
            {
                i++;
                if (i > 99)
                    break;

                var codeStr = GetRandomNumber();
                var session = new Session()
                {
                    Id = 0,
                    Name = "T_Session_" + i,
                    Code = codeStr,
                    Rank = i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Session.EntityStatus.Active
                };
                list.Add(session);
                assignCodeList.Add(codeStr);
            }
            ObjectList.AddRange(list);
            return list;
        }

        #endregion

        #region Others
        public string GetRandomNumber()
        {

            int min = 1;
            int max = 99;
            var codeList = _sessionService.LoadSessionCode();
            if (assignCodeList != null && assignCodeList.Count > 0)
            {
                foreach (var assignCode in assignCodeList)
                {
                    codeList.Add(assignCode);
                }
            }
            do
            {
                Random r = new Random();
                var x = r.Next(min, max);
                string s = x.ToString("00");
                //string s = "10";
                if (codeList != null && !codeList.Contains(s))
                {
                    return s;
                }
                else if (codeList == null)
                {
                    return s;
                }
                min++;
            } while (min <= max);
            return "Error code found";
        }
        #endregion

        #region Cleanup
        public void CleanupSession(List<long> sessionIdList)
        {
            _session.CreateSQLQuery("Delete from session where id in(" + string.Join(", ", sessionIdList) + ")").ExecuteUpdate();
        }

        public void Cleanup()
        {
            DeleteAll();
        }

        #endregion

    }
}
