﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ReffererTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Refferer")]
    public class ReffererSaveTest : ReffererBaseTest
    {
        #region Basic Test

        
        [Fact]
        public void Should_Save_Successfully()
        {
            var referrerFactory =
                _referrerFactory.Create().WithNameAndRank(_name, 1).WithCode(_code)
                    .Persist();
            Referrer referrerObj = _referenceService.LoadById(referrerFactory.Object.Id);
            Assert.NotNull(referrerObj);
            Assert.True(referrerObj.Id > 0);
        }
        
        [Fact]
        public void Save_Should_Check_Duplicate_Name_Entry()
        {
            var referrerFactory =
                _referrerFactory.Create().WithNameAndRank(_name, 1).WithCode(_code)
                    .Persist();
            var name = referrerFactory.Object.Name;
            var referrerFactory2 =
                _referrerFactory.Create().WithNameAndRank(name, 1).WithCode(_code + "fgfghfh");
            var messageCode = _referenceService.Save(referrerFactory2.Object);
            Assert.NotEqual(0, messageCode);
        }
        
        #endregion

    }
}
