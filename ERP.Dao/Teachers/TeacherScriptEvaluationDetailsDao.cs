﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Teachers
{
    public interface ITeacherScriptEvaluationDetailsDao : IBaseDao<TeacherScriptEvaluationDetails, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        
        IList<TeacherScriptEvaluationDetails> TeacherScriptEvaluationsList(int start, int length, List<long> programIdlist, List<long> branchIdlist, long sessionId, string paymentStatus, string date, long? selectCourseId, string teacherName, long? selectBranchId, string trackingId, string tpin);

        #endregion

        #region Others Function

        long CountTeacherScriptEvaluation(List<long> programIdlist, List<long> branchIdlist, long sessionId, string paymentStatus, string date, long? selectCourseId, string teacherName, long? selectBranchId, string trackingId, string tpin);

        #endregion
    }

    public class TeacherScriptEvaluationDetailsDao : BaseDao<TeacherScriptEvaluationDetails, long>, ITeacherScriptEvaluationDetailsDao
    {
        #region Logger

        #endregion

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<TeacherScriptEvaluationDetails> TeacherScriptEvaluationsList(int start, int length, List<long> programIdlist, List<long> branchIdlist, long sessionId, string paymentStatus, string date, long? selectCourseId, string teacherName, long? selectBranchId, string trackingId, string tpin)
        {
            var criteria = GetTeacherScriptEvaluationCriteria(programIdlist, branchIdlist, sessionId, paymentStatus, date, selectCourseId, teacherName, selectBranchId, trackingId, tpin);
            criteria.AddOrder(Order.Desc(Projections.Id()));
            criteria.SetFirstResult(start).SetMaxResults(length);
            List<TeacherScriptEvaluationDetails> teacherScriptEvaluationsList = criteria.List<TeacherScriptEvaluationDetails>().ToList();
            return teacherScriptEvaluationsList;
        }

        #endregion

        #region Others Function

        public long CountTeacherScriptEvaluation(List<long> programIdlist, List<long> branchIdlist, long sessionId, string paymentStatus, string date,
            long? selectCourseId, string teacherName, long? selectBranchId, string trackingId, string tpin)
        {
            var criteria = GetTeacherScriptEvaluationCriteria(programIdlist, branchIdlist, sessionId, paymentStatus, date, selectCourseId, teacherName,
                selectBranchId, trackingId, tpin);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt64(criteria.UniqueResult());

        }

        private ICriteria GetTeacherScriptEvaluationCriteria(List<long> programIdlist, List<long> branchIdlist, long sessionId, string paymentStatus,
            string date, long? selectCourseId, string teacherName, long? selectBranchId, string trackingId, string tpin)
        {
            ICriteria criteria = Session.CreateCriteria<TeacherScriptEvaluationDetails>().Add(Restrictions.Eq("Status", TeacherScriptEvaluation.EntityStatus.Active));
            criteria.CreateAlias("TeacherScriptEvaluation", "tse").Add(Restrictions.Eq("Status", TeacherScriptEvaluationDetails.EntityStatus.Active));
            criteria.CreateAlias("Teacher", "t").Add(Restrictions.Eq("Status", Teacher.EntityStatus.Active));
            criteria.CreateAlias("tse.Program", "p").Add(Restrictions.Eq("Status", Program.EntityStatus.Active));

            criteria.CreateAlias("tse.Session", "s").Add(Restrictions.Eq("Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("tse.Course", "c").Add(Restrictions.Eq("Status", Course.EntityStatus.Active));
            criteria.Add(Restrictions.In("p.Id", programIdlist));
            if (!String.IsNullOrEmpty(tpin))
            {
                int teacherPin;
                if (int.TryParse(tpin, out teacherPin))
                {
                    criteria.Add(Restrictions.Eq("t.Tpin", teacherPin));
                }
            }
            if (branchIdlist.Count > 0)
            {
                Disjunction dr = new Disjunction();
                dr.Add(Restrictions.In("ForBranch.Id", branchIdlist));
                dr.Add(Restrictions.IsNull("ForBranch"));
                criteria.Add(dr);
            }
            else
                criteria.Add(Restrictions.IsNull("ForBranch"));

            criteria.Add(Restrictions.Eq("s.Id", sessionId));
            if (paymentStatus != null)
            {
                criteria.Add(Restrictions.Eq("PaymentStatus", Convert.ToInt32(paymentStatus)));
            }
            if (!String.IsNullOrEmpty(date))
            {

                DateTime dt = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                criteria.Add(Expression.Eq(Projections.SqlFunction("date", NHibernateUtil.DateTime, Projections.Property("tse.SubmissionDate")), dt));
            }
            if (!String.IsNullOrEmpty(teacherName))
            {
                criteria.Add(Restrictions.Like("t.NickName", teacherName, MatchMode.Anywhere));
            }
            if (selectCourseId != null)
            {
                criteria.Add(Restrictions.Eq("c.Id", selectCourseId));
            }
            if (selectBranchId != null && selectBranchId != -1)
            {
                criteria.Add(Restrictions.Eq("ForBranch.Id", selectBranchId));
            }
            else if (selectBranchId == -1)
            {
                criteria.Add(Restrictions.IsNull("ForBranch"));
            }
            if (!String.IsNullOrEmpty(trackingId))
            {
                long convertTrackingId;
                var result = long.TryParse(trackingId, out convertTrackingId);
                if (result)
                    criteria.Add(Restrictions.Eq("tse.Id", Convert.ToInt64(convertTrackingId)));
            }
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            return criteria;
        }

        #endregion
    }
}
