﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AcademicInfoTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AcademicInfo")]
    public class AcademicInfoDeleteTest:AcademicInfoBaseTest
    {
        [Fact]
        public void Should_Return_Successfully_Delete()
        {
            var tm = PersistTeamMember();
            var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            var board = _hrBoardService.LoadBoard().FirstOrDefault();
            _InstituteFactory.Create().WithInstituteCategory().Persist();
            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                ExamName = examLabel.Id.ToString(),
                Board = board.Id.ToString(),
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                Institute = _InstituteFactory.Object.Id.ToString()
            };

            academicInfoArray.Add(acInfo);
            _academicInfoService.UpdateAcademicInfo(tm, academicInfoArray);
            var obj = _academicInfoService.LoadMemberAccademicInfo(tm.Id).FirstOrDefault();
            Assert.True(_academicInfoService.DeleteAcademicInfo(obj.Id));
        }

        [Fact]
        public void Should_Return_InvalidException_For_AcademicInfo()
        {
            var tm = PersistTeamMember();
            var examLabel = _hrExamService.LoadExamList().FirstOrDefault();
            var board = _hrBoardService.LoadBoard().FirstOrDefault();
            _InstituteFactory.Create().WithInstituteCategory().Persist();
            var academicInfoArray = new List<TeamMemberAcademicInfoArray>();
            var acInfo = new TeamMemberAcademicInfoArray()
            {
                DegreeTitle = "Test Degree",
                ExamName = examLabel.Id.ToString(),
                Board = board.Id.ToString(),
                MajorOrGroup = "SCIENCE",
                Result = 2,
                ExamStatus = 1,
                Grade = 5,
                GradeScale = 5,
                Id = 0,
                Institute = _InstituteFactory.Object.Id.ToString()
            };

            academicInfoArray.Add(acInfo);
            _academicInfoService.UpdateAcademicInfo(tm, academicInfoArray);
            var obj = _academicInfoService.LoadMemberAccademicInfo(tm.Id).FirstOrDefault();
            Assert.Throws<InvalidDataException>(
                () => _academicInfoService.DeleteAcademicInfo(obj.Id + 1));
        }
    }
}
