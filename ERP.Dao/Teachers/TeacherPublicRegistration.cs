﻿using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Teachers
{

    public interface ITeacherPublicRegistrationDao : IBaseDao<TeacherPublicRegistration, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
        
        #endregion

        #region Others Function
        
        #endregion

    }

    public class TeacherPublicRegistrationDaoDao : BaseDao<TeacherPublicRegistration, long>, ITeacherPublicRegistrationDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function
        
        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
        
        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion


    }
}
