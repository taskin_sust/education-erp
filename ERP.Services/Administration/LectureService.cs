﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface ILectureService : IBaseService
    {
        #region Operational Function
        bool Save(IList<LectureSettings> lectureSettingList);

        void Update(LectureSettings lectureSettingsModel, int numberOfClassesInt, string classNamePrefix);
        void Delete(LectureSettings deleteLecture);

        #endregion

        #region Single Instances Loading Function
        Lecture GetLecture(long lectureId);
        #endregion

        #region List Loading Function
        IList<Lecture> LoadLecture(long[] lectureIds);
        IList<Lecture> LoadLecture(List<long> programIds, List<long> sessionIds, List<long> courseIds, List<long> subjectIds, List<Batch> batchList, bool returnDoneLectures);
        IList<Lecture> LoadLecture(long[] courseIdList, long[] subjectIdList, long?[] batchIdList = null, string dateFrom = null, string dateTo = null);
        IList<Lecture> LoadLecture(List<UserMenu> userMenus, long[] courseIdList, long[] subjectIdList,
           long?[] batchIdList, string dateFrom = null, string dateTo = null, List<long> organizationIdList = null,
           List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null,
           List<long> campusIdList = null, string[] batchDaysList = null, string[] batchTimeList = null,bool? clearClassAttendance=null);
        List<Lecture> LoadLecture(List<UserMenu> userMenu, List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, long[] courseIdList, DateTime dateFrom, DateTime dateTo);
        IList<Lecture> LoadLecture(List<long> programIdList, List<long> sessionIdList, long[] batchIdList, long[] courseIdList, DateTime dateFrom, DateTime dateTo);
        IList<Lecture> LoadLecture(long lecturSettingId);
        #endregion

        #region Others Function

        #endregion

    }

    public class LectureService : BaseService, ILectureService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization

        private readonly ILectureSettingsDao _lectureSettingsDao;
        private readonly ILectureDao _lectureDao;
        private readonly IBatchDao _batchDao;
        public LectureService(ISession session)
        {
            Session = session;
            _lectureSettingsDao = new LectureSettingsDao() { Session = Session };
            _lectureDao = new LectureDao() { Session = Session };
            _batchDao = new BatchDao() { Session = Session };

        }
        #endregion

        #region Operational Function
        public bool Save(IList<LectureSettings> lectureSettingList)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    string subjectsExist = "";
                    foreach (var ls in lectureSettingList)
                    {
                        LectureSettings oldLectureSetting = _lectureSettingsDao.LoadByCourseSubjectPrefix(ls.Course.Id, ls.ClassNamePrefix, ls.CourseSubject);
                        if (oldLectureSetting != null)
                        {
                            if (oldLectureSetting.CourseSubject != null)
                            {
                                subjectsExist += oldLectureSetting.CourseSubject.Subject.Name + ", ";
                            }
                            else
                            {
                                subjectsExist += "Combined" + ", ";
                            }
                            //throw new DuplicateEntryException("This Lecture is already exist for subject : " + oldLectureSetting.CourseSubject.Subject.Name);                                
                        }
                    }
                    if (!(string.IsNullOrEmpty(subjectsExist)))
                    {
                        subjectsExist = subjectsExist.Remove(subjectsExist.Length - 2);
                        throw new DuplicateEntryException("Lecture save fail, Cause this Lecture is already exist for subject : " + subjectsExist);
                    }
                    foreach (var ls in lectureSettingList)
                    {
                        _lectureSettingsDao.Save(ls);
                        for (int i = 1; i <= ls.NumberOfClasses; i++)
                        {
                            Lecture lecture = new Lecture();
                            lecture.ClassNameSuffix = i.ToString("D2");
                            lecture.Name = ls.ClassNamePrefix + lecture.ClassNameSuffix;
                            lecture.LectureSettings = ls;
                            _lectureDao.Save(lecture);
                        }
                    }
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public void Update(LectureSettings lectureSettingsModelOld, int numberOfClassesInt, string classNamePrefix)
        {
            try
            {
                LectureSettings oldLectureSetting = _lectureSettingsDao.LoadByCourseSubjectPrefix(lectureSettingsModelOld.Course.Id, classNamePrefix, lectureSettingsModelOld.CourseSubject);
                if (oldLectureSetting != null && oldLectureSetting.Id != lectureSettingsModelOld.Id)
                {
                    throw new DuplicateEntryException("This Lecture is already exist");
                }
                else
                {
                    if (lectureSettingsModelOld.NumberOfClasses > numberOfClassesInt)
                    {
                        int highestAttendance = 0;
                        foreach (var l in lectureSettingsModelOld.Lectures)
                        {
                            if (l.StudentClassAttendence.Any())
                            {
                                highestAttendance = Convert.ToInt32(l.ClassNameSuffix);
                            }
                        }

                        if (highestAttendance <= numberOfClassesInt)
                        {
                            using (var trans = Session.BeginTransaction())
                            {
                                try
                                {
                                    lectureSettingsModelOld.NumberOfClasses = numberOfClassesInt;
                                    lectureSettingsModelOld.ClassNamePrefix = classNamePrefix;

                                    int total = lectureSettingsModelOld.Lectures.Count;
                                    int remIndex = -1;
                                    int j = 0;
                                    foreach (var l in lectureSettingsModelOld.Lectures)
                                    {
                                        int currentLecture = Convert.ToInt32(l.ClassNameSuffix);
                                        if (currentLecture > numberOfClassesInt)
                                        {
                                            remIndex = j;
                                            break;
                                        }
                                        else
                                        {
                                            l.Name = lectureSettingsModelOld.ClassNamePrefix + l.ClassNameSuffix;
                                            _lectureDao.Update(l);
                                        }
                                        j++;
                                    }

                                    if (remIndex != -1)
                                    {
                                        for (int i = total - 1; i >= remIndex; i--)
                                        {
                                            lectureSettingsModelOld.Lectures.RemoveAt(i);
                                        }
                                    }


                                    _lectureSettingsDao.SaveOrUpdate(lectureSettingsModelOld);
                                    trans.Commit();
                                }
                                catch (Exception ex)
                                {
                                    if (trans != null && trans.IsActive)
                                        trans.Rollback();
                                    _logger.Error(ex);
                                }
                            }
                        }
                        else
                        {
                            throw new DependencyException("This Lecture has one or more dependant data. Please change your number of class gerater than : " + highestAttendance.ToString());
                        }
                    }
                    else if (lectureSettingsModelOld.NumberOfClasses < numberOfClassesInt)
                    {
                        using (var trans = Session.BeginTransaction())
                        {
                            try
                            {
                                lectureSettingsModelOld.NumberOfClasses = numberOfClassesInt;
                                lectureSettingsModelOld.ClassNamePrefix = classNamePrefix;
                                _lectureSettingsDao.Update(lectureSettingsModelOld);
                                foreach (var l in lectureSettingsModelOld.Lectures)
                                {
                                    l.Name = lectureSettingsModelOld.ClassNamePrefix + l.ClassNameSuffix;
                                    _lectureDao.Update(l);
                                }
                                for (int i = (lectureSettingsModelOld.Lectures.Count + 1); i <= numberOfClassesInt; i++)
                                {
                                    Lecture lecture = new Lecture();
                                    lecture.ClassNameSuffix = i.ToString("D2");
                                    lecture.Name = lectureSettingsModelOld.ClassNamePrefix + lecture.ClassNameSuffix;
                                    lecture.LectureSettings = lectureSettingsModelOld;
                                    _lectureDao.Save(lecture);
                                }

                                trans.Commit();
                            }
                            catch (Exception ex)
                            {
                                if (trans != null && trans.IsActive)
                                    trans.Rollback();
                                _logger.Error(ex);
                            }
                        }
                    }
                    else
                    {
                        using (var trans = Session.BeginTransaction())
                        {
                            try
                            {
                                lectureSettingsModelOld.NumberOfClasses = numberOfClassesInt;
                                lectureSettingsModelOld.ClassNamePrefix = classNamePrefix;
                                _lectureSettingsDao.Update(lectureSettingsModelOld);
                                foreach (var l in lectureSettingsModelOld.Lectures)
                                {
                                    l.Name = lectureSettingsModelOld.ClassNamePrefix + l.ClassNameSuffix;
                                    _lectureDao.Update(l);
                                }
                                trans.Commit();
                            }
                            catch (Exception ex)
                            {
                                if (trans != null && trans.IsActive)
                                    trans.Rollback();
                                _logger.Error(ex);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public void Delete(LectureSettings deleteLecture)
        {
            try
            {
                int highestAttendance = 0;
                foreach (var l in deleteLecture.Lectures)
                {
                    if (l.StudentClassAttendence.Any())
                    {
                        highestAttendance = Convert.ToInt32(l.ClassNameSuffix);
                        break;
                    }
                }


                if (highestAttendance > 0)
                {
                    throw new DependencyException("One or more Attendance information exist under this lecture settings");
                }

                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        _lectureSettingsDao.Update(deleteLecture);
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
            catch (DependencyException e)
            {
                throw;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public Lecture GetLecture(long lectureId)
        {
            try
            {
                return _lectureDao.LoadById(lectureId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        #endregion

        #region List Loading Function
        public IList<Lecture> LoadLecture(List<UserMenu> userMenus, long[] courseIdList, long[] subjectIdList, long?[] batchIdList, string dateFrom = null, string dateTo = null, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null, List<long> campusIdList = null, string[] batchDaysList = null, string[] batchTimeList = null, bool? clearClassAttendance = null)
        {
            IList<Lecture> lectures;
            try
            {
                if (batchIdList.Contains(0))
                {
                    List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(branchIdList), CommonHelper.ConvertSelectedAllIdList(programIdList));
                    List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, CommonHelper.ConvertSelectedAllIdList(organizationIdList), CommonHelper.ConvertSelectedAllIdList(programIdList), CommonHelper.ConvertSelectedAllIdList(branchIdList));
                    var batchList = _batchDao.LoadAuthorizeBatch(authProgramIdList, authBranchIdList, CommonHelper.ConvertSelectedAllIdList(sessionIdList), CommonHelper.ConvertSelectedAllIdList(campusIdList), batchDaysList, batchTimeList);
                    //calling private function
                    batchList = LoadAuthorizeBatch(batchList, batchDaysList, batchTimeList);
                    if (programIdList!=null)
                    {
                        batchList = batchList.Where(x => x.Program.Id.In(programIdList.ToArray())).ToList(); 
                    }
                    if (branchIdList!=null && !branchIdList.Contains(0))
                    {
                        batchList = batchList.Where(x => x.Branch.Id.In(branchIdList.ToArray())).ToList(); 
                    }
                    batchIdList = batchList.Select(t => t != null ? t.Id : (long?)null).ToArray(); 
                }
                lectures = _lectureDao.LoadLectureByCourseSubject(courseIdList, subjectIdList, batchIdList, dateFrom, dateTo, clearClassAttendance).OrderBy(x=>x.LectureName).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return lectures;
            //return new List<Lecture>();
        }

        public IList<Lecture> LoadLecture(long[] lectureIds)
        {
            IList<Lecture> lectures;
            try
            {
                lectures = _lectureDao.LoadByIds(lectureIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return lectures;
        }

        public IList<Lecture> LoadLecture(List<long> programIds, List<long> sessionIds, List<long> courseIds,
            List<long> subjectIds, List<Batch> batchList, bool returnDoneLectures)
        {
            return _lectureDao.LoadLecture(programIds, sessionIds, courseIds, subjectIds, batchList, returnDoneLectures);
        }
        public IList<Lecture> LoadLecture(long[] courseIdList, long[] subjectIdList, long?[] batchIdList, string dateFrom = null, string dateTo = null)
        {
            IList<Lecture> lectures;
            try
            {
                lectures = _lectureDao.LoadLectureByCourseSubject(courseIdList, subjectIdList, batchIdList, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return lectures;
            //return new List<Lecture>();
        }

        public List<Lecture> LoadLecture(List<UserMenu> userMenu, List<long> organizationIdList,
            List<long> programIdList, List<long> sessionIdList, long[] courseIdList, DateTime dateFrom, DateTime dateTo)
        {
            List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu, CommonHelper.ConvertSelectedAllIdList(organizationIdList));
            return _lectureDao.LoadLecture(authProgramIdList, programIdList,  sessionIdList,  courseIdList,  dateFrom,  dateTo);
        }

        
        public IList<Lecture> LoadLecture(List<long> programIdList, List<long> sessionIdList, long[] batchIdList,
            long[] courseIdList, DateTime dateFrom, DateTime dateTo)
        {
            return _lectureDao.LoadLecture( programIdList,
             sessionIdList, batchIdList, courseIdList, dateFrom, dateTo);
        }


        public IList<Lecture> LoadLecture(long lecturSettingId)
        {
            IList<Lecture> lectureList = _lectureDao.LoadLecture(lecturSettingId);
            return lectureList;
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Functions
        private IList<Batch> LoadAuthorizeBatch(IList<Batch> batchList, string[] batchDaysList, string[] batchTimeList = null)
        {
            try
            {
                if (batchDaysList != null)
                {
                    if (!batchDaysList.Contains(SelectionType.SelelectAll.ToString()))
                        batchList = (from x in batchList where batchDaysList.Any(y => y == x.Days) select x).ToList();
                }
                if (batchTimeList != null)
                {
                    if (!batchTimeList.Contains(SelectionType.SelelectAll.ToString()))
                        batchList = (from x in batchList where batchTimeList.Any(y => y == x.FormatedBatchTime) select x).ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return batchList;
        }

        #endregion
    }
}
