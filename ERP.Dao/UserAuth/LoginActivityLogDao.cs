﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.UserAuth
{
    public interface ILoginActivityLogDao : IBaseDao<LoginActivityLog, long>
    {
        #region Operational Function

        void SaveLog(LoginActivityLog obj);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<LoginActivityLog> LoadLoginActivityLog(int start, int length, string orderBy, string orderDir, string userName, int operationStatus, string ipAddress);

        #endregion

        #region Others Function

        int LoginActivityLogRowCount(string userName, int operationStatus, string ipAddress);

        #endregion
    }

    public class LoginActivityLogDao : BaseDao<LoginActivityLog, long>, ILoginActivityLogDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        public void SaveLog(LoginActivityLog obj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    Session.Save(obj);
                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<LoginActivityLog> LoadLoginActivityLog(int start, int length, string orderBy, string orderDir, string userName, int operationStatus, string ipAddress)
        {
            ICriteria criteria = GetLoginActivityLogQuery(userName, operationStatus, ipAddress);
            criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy) : Order.Desc(orderBy));
            return criteria.SetFirstResult(start).SetMaxResults(length).List<LoginActivityLog>();
        }
        
        #endregion

        #region Others Function

        public int LoginActivityLogRowCount(string userName, int operationStatus, string ipAddress)
        {
            ICriteria criteria = GetLoginActivityLogQuery(userName, operationStatus, ipAddress);
            criteria.SetProjection(Projections.RowCount());
            return (int)criteria.UniqueResult();
        }

        #endregion

        #region Helper Function

        private ICriteria GetLoginActivityLogQuery(string userName, int operationStatus, string ipAddress)
        {
            ICriteria criteria = Session.CreateCriteria<LoginActivityLog>();
            if (!String.IsNullOrEmpty(userName))
            {
                criteria.Add(Restrictions.Like("UserName", userName, MatchMode.Anywhere));
            }
            if (operationStatus != 0)
            {
                criteria.Add(Restrictions.Eq("OperationStatus", operationStatus));
            }
            if (!String.IsNullOrEmpty(ipAddress))
            {
                criteria.Add(Restrictions.Like("IpAdderss", ipAddress, MatchMode.Anywhere));
            }
            return criteria;
        }

        #endregion
    }
}
