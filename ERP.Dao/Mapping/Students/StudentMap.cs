using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students {

    public partial class StudentMap : BaseClassMap<Student,long>
    {
        public StudentMap() {
			Table("Student");
			LazyLoad();

            Map(x => x.NickName).Column("NickName");
			Map(x => x.Mobile).Column("Mobile");
			Map(x => x.Gender).Column("Gender");
           
			Map(x => x.Religion).Column("Religion");
			Map(x => x.FullName).Column("FullName");
			Map(x => x.FatherName).Column("FatherName");
			Map(x => x.DateOfBirth).Column("Dob");
            Map(x => x.GuardiansMobile1).Column("GuardiansMobile1");
            Map(x => x.GuardiansMobile2).Column("GuardiansMobile2");
			Map(x => x.BloodGroup).Column("BloodGroup");
            Map(x => x.RegistrationNo).Column("RegistrationNo");
			Map(x => x.Email).Column("Email");
            Map(x => x.IsSync);
            References(x => x.District).Column("DistrictId");
            Map(x => x.LastMeritPosition).Column("LastMeritPosition");
            Map(x => x.Gpa).Column("Gpa");
            Map(x => x.Exam).Column("Exam");
            Map(x => x.Section).Column("Section");

            Map(x => x.IsJsc);
            Map(x => x.IsSsc);
            Map(x => x.IsHsc);
           
            HasMany(x => x.StudentAcademicInfos).KeyColumn("StudentId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentUniversityInfos).KeyColumn("StudentId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentClassAttendences).KeyColumn("StudentId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentPrograms).KeyColumn("StudentId").Cascade.AllDeleteOrphan().Inverse();
         
        }
    }
}
