﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Common
{
    public interface IDivisionDao : IBaseDao<Division, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<Division> LoadDivision();
        #endregion

        #region Others Function

        #endregion
    }
    public class DivisionDao : BaseDao<Division, long>, IDivisionDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<Division> LoadDivision()
        {
            ICriteria criteria =Session.CreateCriteria<Division>().Add(Restrictions.Eq("Status", Division.EntityStatus.Active));
            //if (!String.IsNullOrEmpty(query))
            //{
            //    criteria.Add(Restrictions.Like("Name", query, MatchMode.Start));
            //}
            //criteria.SetFirstResult(0).SetMaxResults(10);
            criteria.AddOrder(Order.Asc("Name"));
            return criteria.List<Division>();
        }

        #endregion

        #region Others Function

        #endregion
    }
}