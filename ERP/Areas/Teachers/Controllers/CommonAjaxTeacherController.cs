﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Hr.AttendanceSummaryServices;
using UdvashERP.Services.Teachers;

namespace UdvashERP.Areas.Teachers.Controllers
{
    [UerpArea("Teachers")]
    [Authorize]
    [AuthorizeAccess]
    public class CommonAjaxTeacherController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("TeachersArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private List<UserMenu> _userMenu;
        private readonly ICommonHelper _commonHelper;
        private ISession _session;
        private readonly ITeacherService _teacherService;

        public CommonAjaxTeacherController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _teacherService = new TeacherService(_session);
            _commonHelper = new CommonHelper();
        }

        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetTeacherInformation(int tpin, bool isAuthorize = true, List<long> organizationIds = null)
        {
            string message = "";
            string teacherName = "";
            string teacherId = "";
            bool successState = false;
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                Teacher teacher = isAuthorize ? _teacherService.GetAuthorizedTeacherByTpin(_userMenu, tpin, organizationIds) : _teacherService.GetTeacherByTpin(tpin, organizationIds);
                if (teacher != null)
                {
                    //teacherName = (!String.IsNullOrEmpty(teacher.NickName))
                    //    ? teacher.NickName
                    //    : (!String.IsNullOrEmpty(teacher.FullName)) ? teacher.FullName : "-";
                    //teacherName += (teacher.HscPassingYear != null) ? " (" + teacher.HscPassingYear.ToString().Substring(2, 2) + ")" : "";
                    teacherName = teacher.DisplayTeacherName;
                    successState = true;
                    teacherId = teacher.Id.ToString();
                }
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message, teacherName = teacherName, teacherId = teacherId });
        }

    }
}