﻿using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Teachers
{
    public interface ITeacherExtraCurricularActivityDao : IBaseDao<TeacherExtraCurricularActivity, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion
    }

    public class TeacherExtraCurricularActivityDao : BaseDao<TeacherExtraCurricularActivity, long>, ITeacherExtraCurricularActivityDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion
    }
}
