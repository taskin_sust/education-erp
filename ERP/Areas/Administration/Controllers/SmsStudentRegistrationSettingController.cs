﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using FluentNHibernate.Utils;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.ViewModel.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Sms;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class SmsStudentRegistrationSettingController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly ISmsMaskService _smsMaskService;
        private readonly IOrganizationService _organizationService;
        private readonly ISmsService _smsService;
        private readonly ISmsStudentRegistrationSettingService _srsService;
        private readonly IStudentExamService _studentExamService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _authorizeMenu;

        public SmsStudentRegistrationSettingController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _courseService = new CourseService(session);
            _smsMaskService = new SmsMaskService(session);
            _organizationService = new OrganizationService(session);
            _smsService = new SmsService(session);
            _srsService = new SmsStudentRegistrationSettingService(session);
            _userService = new UserService(session);
            _studentExamService = new StudentExamService(session);
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Index/Manage Page

        [HttpGet]
        public ActionResult Index()
        {
            SelectList organizationSelectList = new SelectList(new List<Organization>(), "Id", "Name");
            SelectList programSelectList = new SelectList(new List<Program>(), "Id", "Name");
            SelectList sessionSelectList = new SelectList(new List<Session>(), "Id", "Name");
            SelectList courseSelectList = new SelectList(new List<Course>(), "Id", "Name");
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                organizationSelectList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
            ViewBag.OrganizationList = organizationSelectList;
            ViewBag.ProgramList = programSelectList;
            ViewBag.SessionList = sessionSelectList;
            ViewBag.CourseList = courseSelectList;
            ViewBag.StatusText = _commonHelper.GetStatus();
            ViewBag.PageSize = Constants.PageSize;
            return View();
        }

        #region Render Data Table

        [HttpPost]
        public ActionResult Index(int draw, int start, int length, string organizationId, string programId, string sessionId, string courseId, string smsSettingStatus)
        {
            var data = new List<object>();
            long recordsTotal = 0;
            long recordsFiltered = 0;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var convertedOrganizationIdList = String.IsNullOrEmpty(organizationId) ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId));
                    var convertedProgramIdList = String.IsNullOrEmpty(programId) ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(programId));
                    var convertedSessionIdList = String.IsNullOrEmpty(sessionId) ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(sessionId));
                    var convertedCourseIdList = String.IsNullOrEmpty(courseId) ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(courseId));
                    _authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    recordsTotal = _srsService.GetSmsStudentRegistrationSettingCount(_authorizeMenu, convertedOrganizationIdList, convertedProgramIdList, convertedSessionIdList, convertedCourseIdList, smsSettingStatus);
                    recordsFiltered = recordsTotal;
                    var smsStudentRegistrationSettingList = _srsService.LoadSmsStudentRegistrationSetting(_authorizeMenu, start, length, convertedOrganizationIdList, convertedProgramIdList, convertedSessionIdList, convertedCourseIdList).ToList();
                    int serial = start;
                    foreach (var smsStudentRegistrationSetting in smsStudentRegistrationSettingList)
                    {
                        var str = new List<string>();
                        str.Add((++serial).ToString());
                        str.Add(smsStudentRegistrationSetting.Program.Organization.ShortName);
                        str.Add(smsStudentRegistrationSetting.Program.ShortName);
                        str.Add(smsStudentRegistrationSetting.Session.Name);
                        str.Add(smsStudentRegistrationSetting.Course.Name);
                        str.Add(smsStudentRegistrationSetting.StudentExam.Name);
                        str.Add(smsStudentRegistrationSetting.StartDate.ToString("dd MMM, yyyy"));
                        str.Add(smsStudentRegistrationSetting.EndDate.ToString("dd MMM, yyyy"));
                        str.Add(StatusTypeText.GetStatusText(smsStudentRegistrationSetting.Status));
                        str.Add(LinkGenerator.GetGeneratedDetailsEditModelDeleteLink("Details", "Edit", "SmsStudentRegistrationSetting", smsStudentRegistrationSetting.Id, smsStudentRegistrationSetting.Name));
                        data.Add(str);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                }
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        #endregion

        #endregion

        #region Operational Function

        #region Save Operation

        [HttpGet]
        public ActionResult AddSmsStudentRegistrationSetting()
        {
            SelectList organizationSelectList = new SelectList(new List<Organization>(), "Id", "Name");
            SelectList programSelectList = new SelectList(new List<Program>(), "Id", "Name");
            SelectList sessionSelectList = new SelectList(new List<Session>(), "Id", "Name");
            SelectList courseSelectList = new SelectList(new List<Course>(), "Id", "Name");
            try
            {
                string[] receivers = { "Mobile Number (Personal)", "Mobile Number (Father)", "Mobile Number (Mother)" };
                var receiverDbList = _smsService.LoadSmsReceiverActive().Where(x => receivers.Contains(x.Name)).ToList();
                ViewBag.SuccessReceiverList = new SelectList(receiverDbList, "Id", "Name");
                ViewBag.DuplicateReceiverList = new SelectList(receiverDbList, "Id", "Name");
                SmsType st = _smsService.LoadSmsTypeName("SMS Registration Success");
                SelectList optionList = new SelectList(st.DynamicOptions, "Id", "Name");
                List<SelectListItem> optionSelectList = optionList.ToList();
                ViewBag.DynamicOptionsList = new SelectList(optionSelectList, "Value", "Text");
                ViewBag.SmsMask = new SelectList(new List<SmsMask>(), "Id", "Name");
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<StudentExam> studentExamList = _studentExamService.LoadBoardExam(true);
                ViewBag.StudentExams = new SelectList(studentExamList, "Id", "Name");
                organizationSelectList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.StatusTextList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return View();
            }
            ViewBag.OrganizationList = organizationSelectList;
            ViewBag.ProgramList = programSelectList;
            ViewBag.SessionList = sessionSelectList;
            ViewBag.CourseList = courseSelectList;
            ViewBag.PageSize = Constants.PageSize;
            return View();
        }

        #region save post
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSmsStudentRegistrationSetting(SmsStudentRegistrationSettingViewModel obj, string startDate, string endDate)
        {
            try
            {
                SmsStudentRegistrationSetting srs = new SmsStudentRegistrationSetting();
                srs.Program = _programService.GetProgram(obj.ProgramId);
                srs.Session = _sessionService.LoadById(obj.SessionId);
                srs.Course = _courseService.GetCourse(obj.CourseId);
                srs.SmsMask = obj.SmsMaskId != null ? _smsMaskService.LoadById((long)obj.SmsMaskId) : null;
                srs.StartDate = DateTime.ParseExact(startDate, "dd-MM-yyyy", null);
                srs.EndDate = DateTime.ParseExact(endDate, "dd-MM-yyyy", null);
                srs.SuccessMessage = obj.SuccessMessage;
                srs.DuplicateMessage = obj.DuplicateMessage;
                srs.StudentExam = _studentExamService.LoadById(obj.StudentExamId);
                srs.Status = obj.Status;
                foreach (var receiverId in obj.SuccessReceiverIds)
                {
                    SmsStudentRegistrationReceiver srrSuccess = new SmsStudentRegistrationReceiver();
                    srrSuccess.SmsReceiver = _smsService.LoadReceiverById(receiverId);
                    srrSuccess.SmsType = _smsService.LoadSmsTypeName("SMS Registration Success");
                    srrSuccess.SmsStudentRegistrationSetting = srs;
                    srs.SmsStudentRegistrationReceiverList.Add(srrSuccess);
                }
                foreach (var receiverId in obj.DuplicateReceiverIds)
                {
                    SmsStudentRegistrationReceiver srrDuplicate = new SmsStudentRegistrationReceiver();
                    srrDuplicate.SmsReceiver = _smsService.LoadReceiverById(receiverId);
                    srrDuplicate.SmsType = _smsService.LoadSmsTypeName("SMS Registration Duplicate");
                    srrDuplicate.SmsStudentRegistrationSetting = srs;
                    srs.SmsStudentRegistrationReceiverList.Add(srrDuplicate);
                }
                _srsService.SaveOrUpdate(srs);
            }
            catch (DependencyException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (ValueNotAssignedException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (InvalidDataException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { IsSuccess = false, Message = WebHelper.CommonErrorMessage });
            }
            return Json(new { IsSuccess = true, Message = "Sms Setting Successfully saved." });
        }
        
        #endregion

        #endregion

        #region Update Operation

        [HttpGet]
        public ActionResult Edit(long id)
        {
            SmsStudentRegistrationSettingViewModel obj = new SmsStudentRegistrationSettingViewModel();
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var srs = _srsService.GetSmsStudentRegistrationSetting(id);
                obj.OrganizationId = srs.Program.Organization.Id;
                obj.ProgramId = srs.Program.Id;
                obj.SessionId = srs.Session.Id;
                obj.CourseId = srs.Course.Id;
                obj.StudentExamId = srs.StudentExam.Id;
                obj.StartDate = srs.StartDate;
                obj.EndDate = srs.EndDate;
                obj.SmsMaskId = srs.SmsMask != null ? srs.SmsMask.Id : (long?)null;
                obj.Status = srs.Status;
                obj.SuccessMessage = srs.SuccessMessage;
                obj.DuplicateMessage = srs.DuplicateMessage;
                obj.SuccessReceiverIds = srs.SmsStudentRegistrationReceiverList.Where(x => x.SmsType.Name == "SMS Registration Success").Select(x => x.SmsReceiver.Id).ToArray();
                obj.DuplicateReceiverIds = srs.SmsStudentRegistrationReceiverList.Where(x => x.SmsType.Name == "SMS Registration Duplicate").Select(x => x.SmsReceiver.Id).ToArray();
                var convertedOrganizationIdList = _commonHelper.ConvertIdToList(obj.OrganizationId);
                var convertedProgramIdList = _commonHelper.ConvertIdToList(obj.ProgramId);
                string[] receivers = { "Mobile Number (Personal)", "Mobile Number (Father)", "Mobile Number (Mother)" };
                var receiverDbList = _smsService.LoadSmsReceiverActive().Where(x => receivers.Contains(x.Name)).ToList();
                var programList = _programService.LoadAuthorizedProgram(userMenu, convertedOrganizationIdList);
                var sessionList = _sessionService.LoadAuthorizedSession(userMenu, convertedOrganizationIdList, convertedProgramIdList);
                var courserList = _courseService.LoadCourse(convertedOrganizationIdList, convertedProgramIdList, _commonHelper.ConvertIdToList(obj.SessionId));
                var smsMaskNameList = _smsMaskService.LoadSmsMask(convertedOrganizationIdList);
                SmsType st = _smsService.LoadSmsTypeName("SMS Registration Success");
                SelectList optionList = new SelectList(st.DynamicOptions, "Id", "Name");
                List<SelectListItem> optionSelectList = optionList.ToList();
                IList<StudentExam> studentExamList = _studentExamService.LoadBoardExam(true);
                ViewBag.StudentExams = new SelectList(studentExamList, "Id", "Name", obj.StudentExamId);
                ViewBag.DynamicOptionsList = new SelectList(optionSelectList, "Value", "Text");
                ViewBag.SuccessReceiverList = new MultiSelectList(receiverDbList, "Id", "Name", obj.SuccessReceiverIds);
                ViewBag.DuplicateReceiverList = new MultiSelectList(receiverDbList, "Id", "Name", obj.DuplicateReceiverIds);
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(programList, "Id", "Name", obj.ProgramId);
                ViewBag.SessionList = new SelectList(sessionList, "Id", "Name", obj.SessionId);
                ViewBag.CourseList = new SelectList(courserList, "Id", "Name", obj.CourseId);
                ViewBag.SmsMask = new SelectList(smsMaskNameList, "Id", "Name", obj.SmsMaskId);
                ViewBag.StatusTextList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                ViewBag.Id = srs.Id;
                ViewBag.IsStudentRegistered = _srsService.IsStudentRegistered(srs.Program.Id, srs.Course.Id);
                ViewBag.PageSize = Constants.PageSize;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return View(obj);
            }
            return View(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SmsStudentRegistrationSettingViewModel obj, string smsSettingId, string startDate, string endDate)
        {
            try
            {
                long id = Convert.ToInt64(smsSettingId);
                var srs = new SmsStudentRegistrationSetting();
                srs.Id = id;
                srs.Program = _programService.GetProgram(obj.ProgramId);
                srs.Session = _sessionService.LoadById(obj.SessionId);
                srs.Course = _courseService.GetCourse(obj.CourseId);
                srs.StudentExam = _studentExamService.LoadById(obj.StudentExamId);
                srs.SmsMask = obj.SmsMaskId != null ? _smsMaskService.LoadById((long)obj.SmsMaskId) : null;
                srs.StartDate = DateTime.ParseExact(startDate, "dd-MM-yyyy", null);
                srs.EndDate = DateTime.ParseExact(endDate, "dd-MM-yyyy", null);
                srs.SuccessMessage = obj.SuccessMessage;
                srs.DuplicateMessage = obj.DuplicateMessage;
                srs.Status = obj.Status;
                //var receiverListBb = srs.SmsStudentRegistrationReceiverList.Select(x=>x.Id).ToList();
                srs.SmsStudentRegistrationReceiverList.Clear();
                foreach (var receiverId in obj.SuccessReceiverIds)
                {
                    SmsStudentRegistrationReceiver srrSuccess = new SmsStudentRegistrationReceiver();
                    srrSuccess.SmsReceiver = _smsService.LoadReceiverById(receiverId);
                    srrSuccess.SmsType = _smsService.LoadSmsTypeName("SMS Registration Success");
                    srrSuccess.SmsStudentRegistrationSetting = srs;
                    srs.SmsStudentRegistrationReceiverList.Add(srrSuccess);
                }
                foreach (var receiverId in obj.DuplicateReceiverIds)
                {
                    SmsStudentRegistrationReceiver srrDuplicate = new SmsStudentRegistrationReceiver();
                    srrDuplicate.SmsReceiver = _smsService.LoadReceiverById(receiverId);
                    srrDuplicate.SmsType = _smsService.LoadSmsTypeName("SMS Registration Duplicate");
                    srrDuplicate.SmsStudentRegistrationSetting = srs;
                    srs.SmsStudentRegistrationReceiverList.Add(srrDuplicate);
                }
                _srsService.SaveOrUpdate(srs);
            }
            catch (DependencyException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (NullObjectException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (ValueNotAssignedException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (InvalidDataException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (DuplicateEntryException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { IsSuccess = false, Message = WebHelper.CommonErrorMessage });
            }
            return Json(new { IsSuccess = true, Message = "Sms Setting Successfully updated." });
        }

        #endregion

        #region Delete Operation

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isSuccess = _srsService.Delete(id);
                if (isSuccess)
                {
                    ViewBag.SuccessMessage = "Deleted Successfully";
                    return Json(new Response(true, "sucessfully deleted."));
                }
                return Json(new Response(false, "delete failed"));
            }
            catch (DependencyException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (NullObjectException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (ValueNotAssignedException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Details Operation

        public ActionResult Details(long id)
        {
            SmsStudentRegistrationSettingViewModel obj = new SmsStudentRegistrationSettingViewModel();
            string[] receivers = { "Mobile Number (Personal)", "Mobile Number (Father)", "Mobile Number (Mother)" };
            var receiverDbList = _smsService.LoadSmsReceiverActive().Where(x => receivers.Contains(x.Name)).ToList();
            ViewBag.SuccessReceiverList = new SelectList(receiverDbList, "Id", "Name");
            ViewBag.DuplicateReceiverList = new SelectList(receiverDbList, "Id", "Name");
            var optionSelectList = new List<SelectListItem>
            {
                new SelectListItem{Text="Program Roll",Value="1"},
                new SelectListItem{Text="Registration Number",Value="2"},
                new SelectListItem{Text="Nick Name",Value="3"}
            };
            ViewBag.DynamicOptionsList = new SelectList(optionSelectList, "Value", "Text");
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var srs = _srsService.GetSmsStudentRegistrationSetting(id);
                obj.OrganizationId = srs.Program.Organization.Id;
                obj.ProgramId = srs.Program.Id;
                obj.SessionId = srs.Session.Id;
                obj.CourseId = srs.Course.Id;
                obj.StudentExamId = srs.StudentExam.Id;
                obj.StartDate = srs.StartDate;
                obj.EndDate = srs.EndDate;
                obj.SmsMaskId = srs.SmsMask != null ? srs.SmsMask.Id : (long?)null;
                obj.Status = srs.Status;
                obj.SuccessMessage = srs.SuccessMessage;
                obj.DuplicateMessage = srs.DuplicateMessage;
                obj.SuccessReceiverIds = srs.SmsStudentRegistrationReceiverList.Where(x => x.SmsType.Name == "SMS Registration Success").Select(x => x.SmsReceiver.Id).ToArray();
                obj.DuplicateReceiverIds = srs.SmsStudentRegistrationReceiverList.Where(x => x.SmsType.Name == "SMS Registration Duplicate").Select(x => x.SmsReceiver.Id).ToArray();
                var convertedOrganizationIdList = _commonHelper.ConvertIdToList(obj.OrganizationId);
                var convertedProgramIdList = _commonHelper.ConvertIdToList(obj.ProgramId);
                var programList = _programService.LoadAuthorizedProgram(userMenu, convertedOrganizationIdList);
                var sessionList = _sessionService.LoadAuthorizedSession(userMenu, convertedOrganizationIdList, convertedProgramIdList);
                var courserList = _courseService.LoadCourse(convertedOrganizationIdList, convertedProgramIdList, _commonHelper.ConvertIdToList(obj.SessionId));
                var smsMaskNameList = _smsMaskService.LoadSmsMask(convertedOrganizationIdList);
                IList<StudentExam> studentExamList = _studentExamService.LoadBoardExam(true);
                ViewBag.StudentExams = new SelectList(studentExamList, "Id", "Name", obj.StudentExamId);
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(programList, "Id", "Name", obj.ProgramId);
                ViewBag.SessionList = new SelectList(sessionList, "Id", "Name", obj.SessionId);
                ViewBag.CourseList = new SelectList(courserList, "Id", "Name", obj.CourseId);
                ViewBag.SmsMask = new SelectList(smsMaskNameList, "Id", "Name", obj.SmsMaskId);
                ViewBag.StatusTextList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                ViewBag.Id = srs.Id;
                ViewBag.PageSize = Constants.PageSize;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return View(obj);
            }
            return View(obj);
        }

        #endregion

        #endregion

        #region Helper Function

        public JsonResult LoadSmsMask(List<long> organizationIds = null, int? status = null)
        {
            try
            {
                IList<SmsMask> smsMaskNameList = _smsMaskService.LoadSmsMask(organizationIds, status);
                List<SelectListItem> smsMaskNameListItems = (new SelectList(smsMaskNameList.ToList(), "Id", "Name")).ToList();
                return Json(new { returnSmsMaskName = smsMaskNameListItems, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Sms Mask Naem";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion
    }
}