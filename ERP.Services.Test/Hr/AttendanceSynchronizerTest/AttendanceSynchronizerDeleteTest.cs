﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceSynchronizerTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceSynchronizer")]
    public class AttendanceSynchronizerDeleteTest : AttendanceSynchronizerBaseTest
    {
        #region Basic Test

        [Fact]
        public void Delete_Should_Return_Null_Exception() 
        {
            _campusRoomFactory.CreateMore(1, null);
            _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList());
            Assert.Throws<NullObjectException>(() => _attendanceSynchronizerService.Delete(null));
            Assert.Throws<NullObjectException>(() => _attendanceSynchronizerService.Delete(_attendanceSynchronizerFactory.Object));
        }
        
        [Fact]
        public void Should_Check_Dependency()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).Persist();

            var obj1 = _attendanceSynchronizerFactory.Create().WithCampus(_campusFactory.Object).Persist().Object;
            _attendanceDeviceFactory.CreateMore(1, _campusFactory.Object,obj1).Persist();

            _attendanceSynchronizerFactory.Evict(obj1);
            var obj2 = _attendanceSynchronizerService.GetAttendanceSynchronizer(obj1.Id);
            var ad = obj2.AttendanceDevice;
            obj2.Status = AttendanceSynchronizer.EntityStatus.Delete;

            Assert.Throws<DependencyException>(() => _attendanceSynchronizerService.Delete(obj2));
        }

        [Fact]
        public void Should_Delete_Successfully() 
        {
            var obj1 = _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.CreateMore(1, null).GetLastCreatedObjectList()).WithSynchronizerKey(_synchronizerKey).Persist().Object;
            var objUpdate = _attendanceSynchronizerService.GetAttendanceSynchronizer(obj1.Id);
            objUpdate.Status = AttendanceSynchronizer.EntityStatus.Delete;
            _attendanceSynchronizerService.Delete(objUpdate);
            var objUpdate2 = _attendanceSynchronizerService.GetAttendanceSynchronizer(obj1.Id);
            Assert.Null(objUpdate2);
        }

        #endregion
    }
}
