using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {


    public partial class CampusRoomMap : BaseClassMap<CampusRoom, long>
    {
        
        public CampusRoomMap() {
			Table("CampusRoom");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Rank);
            Map(x => x.RoomNo).Column("RoomNo");
			Map(x => x.ClassCapacity).Column("ClassCapacity");
			Map(x => x.ExamCapacity).Column("ExamCapacity");

            References(x => x.Campus).Column("CampusId");
        }
    }
}
