﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class CampusFactory : ObjectFactoryBase<Campus>
    {
        #region Object Initialization

        protected readonly ICampusService _campusService;
        private BranchFactory _branchFactory;
        private OrganizationFactory _organizationFactory;
        public CampusFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _campusService = new CampusService(session);
            _branchFactory = new BranchFactory(this, session);
            _organizationFactory = new OrganizationFactory(this, session);
        }

        #endregion

        #region Create

        public CampusFactory Create()
        {
            Object = new Campus()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Location = "39,ARA BHABAN,KAZI NAZRUL ISLAM, Dhaka-1217",
                ContactNumber = "8801812345678",
                Status = Campus.EntityStatus.Active,
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public CampusFactory CreateMore(int numberOfcampus, IList<Branch> branchList, List<CampusRoom> objList)
        {
            CreateCampusList(numberOfcampus, branchList, objList);
            return this;
        }

        private CampusFactory CreateCampusList(int numberOfcampus, IList<Branch> branchList, List<CampusRoom> objList)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Campus>();
            foreach (var br in branchList)
            {
                for (int i = 0; i < numberOfcampus; i++)
                {
                    var campus = new Campus
                    {
                        Id = 0,
                        BusinessId = GetPcUserName(),
                        Name = Guid.NewGuid().ToString().Substring(0, 15) + "_" + i,
                        Rank = i,
                        CreateBy = 1,
                        ModifyBy = 1,
                        Location = "39,ARA BHABAN,KAZI NAZRUL ISLAM, Dhaka-1217",
                        ContactNumber = "8801812345678",
                        Status = Campus.EntityStatus.Active
                    };
                    campus.Branch = br;
                    campus.CampusRooms = objList;
                    list.Add(campus);
                }
            }
            ObjectList.AddRange(list);
            return this;
        }

        public CampusFactory CreateMore(int numberOfcampus)
        {
            CreateCampusList(numberOfcampus);
            return this;
        }

        private CampusFactory CreateCampusList(int numberOfcampus)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Campus>();
            var organizationFactory =
                _organizationFactory.Create().WithRequiredProperties()
                    .Persist();
            var branchFactory =
                _branchFactory.Create()
                    .WithOrganization(organizationFactory.Object)
                    .WithNameAndRank(Guid.NewGuid() + "testBranchName", 1)
                //.WithCode("78")
                    .Persist();

            for (int i = 0; i < numberOfcampus; i++)
            {
                var campus = new Campus
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = "_" + i,
                    Rank = i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Location = "39,ARA BHABAN,KAZI NAZRUL ISLAM, Dhaka-1217",
                    ContactNumber = "8801812345678",
                    Status = Campus.EntityStatus.Active,
                    Branch = branchFactory.Object,
                    CampusRooms = new List<CampusRoom>()
                    {
                        new CampusRoom()
                        {
                            RoomNo = (100 + i).ToString(),
                            ClassCapacity = 100,
                            ExamCapacity = 100
                        }
                    }
                };
                list.Add(campus);
            }
            ObjectList.AddRange(list);
            return this;
        }

        public CampusFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }

        public CampusFactory WithBranch()
        {
            var branchCode = _branchFactory.GetRandomNumber();
            _branchFactory.Create().WithOrganization().WithCode(branchCode).Persist();
            Object.Branch = _branchFactory.Object;
            return this;
        }

        public CampusFactory WithBranch(Organization organization)
        {
            _branchFactory.Create().WithOrganization(organization).Persist();
            Object.Branch = _branchFactory.Object;
            return this;
        }

        public CampusFactory WithBranch(Branch branch)
        {
            Object.Branch = branch;
            return this;
        }

        public CampusFactory WithCampusRoomList(List<CampusRoom> objList)
        {
            Object.CampusRooms = objList;
            return this;
        }

        public CampusFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _campusService.Save(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _campusService.Save(item);
                    }
                }
            }
            return this;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void CleanUpBatch(List<long> batchIdList)
        {
            string deleteQuery = "Delete From Batch Where Id In (" + string.Join(", ", batchIdList) + ")";
            _session.CreateSQLQuery(deleteQuery).ExecuteUpdate();
        }
        public void Cleanup()
        {
            DeleteAll();
            _branchFactory.Cleanup();
            _organizationFactory.Cleanup();
        }
        #endregion

    }
}
