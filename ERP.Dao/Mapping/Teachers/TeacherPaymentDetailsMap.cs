﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public class TeacherPaymentDetailsMap : BaseClassMap<TeacherPaymentDetails, long>
    {
        public TeacherPaymentDetailsMap()
        {
            Table("TeacherPaymentDetails");
            LazyLoad();
            References(x => x.TeacherPayment).Column("TeacherPaymentId");
            References(x => x.TeacherClassEntryDetails).Column("TeacherClassEntryDetailsId");
            References(x => x.TeacherAcademicMaterialsDetails).Column("TeacherAcademicMaterialsDetailsId");
            References(x => x.TeacherScriptEvaluationDetails).Column("TeacherScriptEvaluationDetailsId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            Map(x => x.Amount).Column("Amount");
            Map(x => x.Quantity).Column("Quantity");
            Map(x => x.SubmissionHeldDate).Column("SubmissionHeldDate");
            Map(x => x.Description).Column("Description");
        }
    }
}
