﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ZoneSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ZoneSetting")]
    public class ZoneSettingUpdateTest : ZoneSettingBaseTest
    {

        #region Basic Test

        [Fact]
        public void Should_Update_Successfully()
        {
            var dFactory1 = _zoneSettingFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .WithToleranceTimeToleranceDayDeductionMultiplier(10, 2, .5m).Persist();
            var success = _zoneSettingService.Update(dFactory1.Object.Id, _name, "1", "1", "1", "1", "1");
            Assert.True(success);
        }

        #endregion

        #region Business Logic Test


        [Theory]
        //name null
        [InlineData("", "1", "-1", "1", ".5", "1")]
        //tolerance time < 0
        [InlineData("New Name", "1", "-1", "1", ".5", "1")]
        //tolerance time > 24*60
        [InlineData("New Name", "1", "1445", "1", ".5", "1")]
        //tolerance day < 0
        [InlineData("New Name", "1", "1", "-1", ".5", "1")]
        //tolerance day > 5
        [InlineData("New Name", "1", "1", "5", ".5", "1")]
        //deduction multiplier < -1
        [InlineData("New Name", "1", "1", "1", "-1", "1")]
        //deduction multiplier > 2
        [InlineData("New Name", "1", "1", "1", "3", "1")]
        public void Update_Should_Check_Property(string name, string organizationId, string toleranceTime, string toleranceday, string deductionMultiplier, string status)
        {
            var dFactory1 = _zoneSettingFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .WithToleranceTimeToleranceDayDeductionMultiplier(10, 2, .5m).Persist();
            dFactory1.Object.Organization = null;
            Assert.Throws<EmptyFieldException>(() => _zoneSettingService.Update(dFactory1.Object.Id, name, organizationId, toleranceTime, toleranceday, deductionMultiplier, status));

        }
        #endregion



    }
}
