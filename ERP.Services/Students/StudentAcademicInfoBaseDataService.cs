﻿using System;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface IStudentAcademicInfoBaseDataService : IBaseService
    {
        #region Operational Function
        bool SaveUpdate(StudentAcademicInfoBaseData academicInfoBaseData, long saInfoId);
        #endregion

        #region Single Instances Loading Function
        StudentAcademicInfoBaseData LoadByAcademicInfoId(long id);
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion
    }
    public class StudentAcademicInfoBaseDataService : BaseService, IStudentAcademicInfoBaseDataService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentAcademicInfoBaseDataDao _studentAcademicInfoBaseDataDao;
        private readonly IStudentAcademicInfoSubjectResultDao _studentAcademicInfoSubjectResultDao;
        public StudentAcademicInfoBaseDataService(ISession session)
        {
            Session = session;
            _studentAcademicInfoBaseDataDao = new StudentAcademicInfoBaseDataDao() { Session = Session };
            _studentAcademicInfoSubjectResultDao = new StudentAcademicInfoSubjectResultDao() { Session = Session };
        }

        #endregion

        #region Operational Function
        public bool SaveUpdate(StudentAcademicInfoBaseData academicInfoBaseData, long saInfoId)
        {
            try
            {
                var existData = _studentAcademicInfoBaseDataDao.LoadByAcademicInfoId(saInfoId);
                if (existData != null)
                {
                    existData.CreationDate = DateTime.Now;
                    existData.RollNo = academicInfoBaseData.RollNo;
                    existData.Name = academicInfoBaseData.Name;
                    existData.Board = academicInfoBaseData.Board;
                    existData.FathersName = academicInfoBaseData.FathersName;
                    existData.ExamGroup = academicInfoBaseData.ExamGroup;
                    existData.MothersName = academicInfoBaseData.MothersName;
                    existData.Session = academicInfoBaseData.Session;
                    existData.RegistrationNo = academicInfoBaseData.RegistrationNo;
                    existData.StudentType = academicInfoBaseData.StudentType;
                    existData.DateOfBirth = academicInfoBaseData.DateOfBirth;
                    existData.Result = academicInfoBaseData.Result;
                    existData.Institute = academicInfoBaseData.Institute;
                    existData.Gpa = academicInfoBaseData.Gpa;
                    bool isUpdated = _studentAcademicInfoBaseDataDao.UpdateInfoBaseData(existData);
                    if (isUpdated)
                        _studentAcademicInfoSubjectResultDao.DeleteSubjectResult(existData);
                }
                else
                {
                    academicInfoBaseData.CreationDate = DateTime.Now;
                    _studentAcademicInfoBaseDataDao.SaveInfoBaseData(academicInfoBaseData);
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public StudentAcademicInfoBaseData LoadByAcademicInfoId(long id)
        {
            StudentAcademicInfoBaseData studentAcademicInfoBaseData;
            try
            {
                studentAcademicInfoBaseData=_studentAcademicInfoBaseDataDao.LoadByAcademicInfoId(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return studentAcademicInfoBaseData;
        }
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
