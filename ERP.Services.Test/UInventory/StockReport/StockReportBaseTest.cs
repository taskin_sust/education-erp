﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.StockReport
{
    public class StockReportBaseTest : TestBase, IDisposable
    {
        #region Obj initialization

        internal readonly CommonHelper _commonHelper;
        //private ISession _session;

        public OrganizationFactory _organizationFactory;
        public BranchFactory _branchFactory;
        public ItemGroupFactory _itemGroupFactory;
        public ItemFactory _itemFactory;
        public ProgramFactory _programFactory;
        public SessionFactory _sessionFactory;
        public SupplierFactory _supplierFactory;
        public BankBranchFactory _bankBranchFactory;
        public GoodsReceiveFactory _goodsReceiveFactory;
        public GoodsReceiveDetailsFactory _goodsReceiveDetailsFactory;
        public SupplierBankDetailsFactory _supplierBankDetailsFactory;
        public ProgramSessionItemFactory _programSessionItemFactory;

        public IGoodsReceiveService _goodsReceiveService;
        public IGoodsReceiveDetailService _goodsReceiveDetailsService;
        public ICurrentStockSummaryService _currentStockSummaryService;
        public IBranchService _branchService;
        public IProgramBranchSessionService _programBranchSessionService;
        public IStudentExamService _studentExamService;
        public StockReportService _stockReportService;
        internal TestBaseService<ProgramBranchSession> _TestBaseService;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";

        #endregion

        public StockReportBaseTest()
        {
            _commonHelper = new CommonHelper();
            _organizationFactory = new OrganizationFactory(null, Session);
            _branchFactory = new BranchFactory(null, Session);
            _itemGroupFactory = new ItemGroupFactory(null, Session);
            _itemFactory = new ItemFactory(null, Session);
            _programFactory = new ProgramFactory(null, Session);
            _sessionFactory = new SessionFactory(null, Session);
            _supplierFactory = new SupplierFactory(null, Session);
            _bankBranchFactory = new BankBranchFactory(null, Session);
            _goodsReceiveFactory = new GoodsReceiveFactory(null, Session);
            _goodsReceiveDetailsFactory = new GoodsReceiveDetailsFactory(null, Session);
            _supplierBankDetailsFactory = new SupplierBankDetailsFactory(null, Session);
            _programSessionItemFactory = new ProgramSessionItemFactory(null, Session);

            _goodsReceiveService = new GoodsReceiveService(Session);
            _goodsReceiveDetailsService = new GoodsReceiveDetailsService(Session);
            _currentStockSummaryService = new CurrentStockSummaryService(Session);
            _branchService = new BranchService(Session);
            _programBranchSessionService = new ProgramBranchSessionService(Session);
            _studentExamService = new StudentExamService(Session);
            _stockReportService = new StockReportService(Session);
            _TestBaseService = new TestBaseService<ProgramBranchSession>(Session);
        }

        public ISession TestSession
        {
            get { return Session; }
            set { Session = value; }
        }

        public List<string> GenerateProgramSessionMix(Organization organization, Program program, Session session)
        {
            return new List<string>() { program.Id + "::" + session.Id };
        }

        public void Dispose()
        {
            CleanupStockSummary(_programFactory.SingleObjectList.Select(x => x.Id).ToList(), 
                                _sessionFactory.SingleObjectList.Select(x => x.Id).ToList(), 
                                _branchFactory.SingleObjectList.Select(x => x.Id).ToList(), 
                                _itemFactory.SingleObjectList.Select(x => x.Id).ToList()
                               );
            CleanUpProgramBranchSession();
            CleanUpProgramStudentExamSession();
            if (_goodsReceiveFactory.SingleObjectList != null)
                foreach (var goodsReceive in _goodsReceiveFactory.SingleObjectList)
                {
                    foreach (var recDetail in goodsReceive.GoodsReceiveDetails)
                    {
                        if (recDetail.GoodsReceive.Branch != null && recDetail.Item != null)
                        {
                            string swData = "DELETE FROM UINV_CurrentStockSummary WHERE BranchId = " + recDetail.GoodsReceive.Branch.Id + " and ItemId =" + recDetail.Item.Id + " ; ";
                            Session.CreateSQLQuery(swData).ExecuteUpdate();
                        }
                    }
                }

            _goodsReceiveDetailsFactory.CleanUp();
            _goodsReceiveFactory.CleanUp();
            _supplierBankDetailsFactory.CleanUp();
            _supplierFactory.CleanUp();
            _programSessionItemFactory.CleanUp();
            _itemFactory.CleanUp();
            _branchFactory.Cleanup();
            _programFactory.Cleanup();
            base.Dispose();

        }

        public void CleanupStockSummary(List<long> programIdList, List<long> sessionIdList, List<long> branchIdList, List<long> itemIdList)
        {
            string query = " BranchId IN(" + string.Join(",", branchIdList) + ") AND ItemId IN(" + string.Join(",", itemIdList) + ")";
            if (programIdList.Count > 0)
            {
                query += "AND ProgramId IN(" + string.Join(",", programIdList) + ")";
            }
            if (sessionIdList.Count > 0)
            {
                query += "AND SessionId IN(" + string.Join(",", sessionIdList) + ")";
            }
            Session.CreateSQLQuery("Delete from UINV_CurrentStockSummary where " + query).ExecuteUpdate();
        }
        public void CleanUpProgramBranchSession()
        {
            for (int i = 0; i < _programFactory.SingleObjectList.Count; i++)
            {
                long sessionId = 0;
                long branchId = 0;
                var programId = _programFactory.SingleObjectList[i].Id;
                if (_sessionFactory.SingleObjectList.Count > i)
                    sessionId = _sessionFactory.SingleObjectList[i].Id;
                if (_branchFactory.SingleObjectList.Count > i)
                    branchId = _branchFactory.SingleObjectList[i].Id;
                _TestBaseService.DeleteProgramBranchSession(programId, sessionId, new[] { branchId });
            }
        }
        public void CleanUpProgramStudentExamSession()
        {
            for (int i = 0; i < _programFactory.SingleObjectList.Count; i++)
            {
                long sessionId = 0;
                var programId = _programFactory.SingleObjectList[i].Id;
                if (_sessionFactory.SingleObjectList.Count > i)
                    sessionId = _sessionFactory.SingleObjectList[i].Id;
                _TestBaseService.DeleteProgramStudentExamSession(programId, sessionId);
            }
        }
    }
}
