﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface IDiscountService : IBaseService
    {
        #region Operational Function
        bool IsSave(Discount discount);
        bool IsUpdate(Discount oldDiscount, Discount discount);
        void Delete(Discount discount);
        #endregion

        #region Single Instances Loading Function
        Discount GetDiscount(long discountId);
        Discount GetMostApplicableDiscountObj(IList<Discount> discountApplicableListForStd);
        #endregion

        #region List Loading Function
        //IList<Discount> LoadDiscount(int start, int length, string orderBy, string orderDir, string organizationId, string programId, string sessionId, string courseId, string formatDate, string toDate, string amount, string status);
        IList<Discount> LoadDiscount(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string organizationId, string programId, string sessionId, string courseId, string formatDate, string toDate, string amount, string status);
        IList<Discount> LoadDiscount(Program program, Session session, DateTime stdAdmitDate);

        #endregion

        #region Others Function
        int GetDiscountCount(string orderBy, string orderDir, List<UserMenu> userMenu, string organizationId, string programId, string sessionId, string courseId, string formatDate, string toDate, string amount, string status);
        //int GetDiscountCount(string orderBy, string orderDir, string organizationId, string programId, string sessionId, string courseId, string formatDate, string toDate, string amount, string status);

        #endregion

        //no need
        bool Update(Discount discount);
    }
    public class DiscountService : BaseService, IDiscountService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IDiscountDao _discountDao;
        private readonly ICommonHelper _commonHelper;
        public DiscountService(ISession session)
        {
            Session = session;
            _discountDao = new DiscountDao() { Session = Session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function
        public bool IsSave(Discount discount)
        {
            ITransaction trans = null;
            try
            {
                var tSpan = new TimeSpan(0, 23, 59, 59);
                discount.EndDate = discount.EndDate + tSpan;
                var isProcess = IsProcessFurtherForSave(discount);
                if (isProcess)
                {
                    using (trans = Session.BeginTransaction())
                    {
                        _discountDao.Save(discount);
                        trans.Commit();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        private bool IsProcessFurtherForSave(Discount discount)
        {
            try
            {
                bool isDuplicate = _discountDao.IsDuplicateDiscount(discount);
                if (isDuplicate)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public bool IsUpdate(Discount oldDiscount, Discount discount)
        {
            ITransaction trans = null;
            try
            {
                if (oldDiscount != null && discount != null)
                {
                    var tSpan = new TimeSpan(0, 23, 59, 59);
                    oldDiscount.EndDate = oldDiscount.EndDate + tSpan;
                    oldDiscount.DiscountDetails = discount.DiscountDetails;
                    using (trans = Session.BeginTransaction())
                    {
                        _discountDao.Update(oldDiscount);
                        trans.Commit();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public void Delete(Discount discount)
        {
            ITransaction trans = null;
            try
            {
                discount.Status = Discount.EntityStatus.Delete;
                using (trans = Session.BeginTransaction())
                {
                    if (discount.DiscountDetails.Count > 0)
                    {
                        foreach (var d in discount.DiscountDetails)
                        {
                            d.Status = DiscountDetail.EntityStatus.Delete;
                        }
                    }
                    _discountDao.Update(discount);
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public Discount GetDiscount(long discountId)
        {
            try
            {
                return _discountDao.LoadById(discountId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public Discount GetMostApplicableDiscountObj(IList<Discount> discountApplicableListForStd)
        {
            Discount desiredDiscount;
            try
            {
                desiredDiscount = discountApplicableListForStd[0];
                for (int i = 0; i < discountApplicableListForStd.Count; i++)
                {
                    if (discountApplicableListForStd[i].Amount > desiredDiscount.Amount)
                    {
                        desiredDiscount = discountApplicableListForStd[i];
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return desiredDiscount;
        }
        #endregion

        #region List Loading Function
        //public IList<Discount> LoadDiscount(int start, int length, string orderBy, string orderDir, string organizationId, string programId, string sessionId, string courseId, string formatDate, string toDate, string amount, string status)
        public IList<Discount> LoadDiscount(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string organizationId, string programId, string sessionId, string courseId, string formatDate, string toDate, string amount, string status)
        {
            IList<Discount> discounts;
            try
            {
                long? convertedOrganizationId = String.IsNullOrEmpty(organizationId) ? (long?)null : Convert.ToInt64(organizationId);
                long? convertedProgramId = String.IsNullOrEmpty(programId) ? (long?)null : Convert.ToInt64(programId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (convertedProgramId != null) ? _commonHelper.ConvertIdToList((long)convertedProgramId) : null);

                discounts = _discountDao.LoadDiscount(start, length, orderBy, orderDir, organizationIdList, programIdList, sessionId, courseId, formatDate, toDate, amount, status);
                //discounts = _discountDao.LoadDiscount(start, length, orderBy, orderDir, organizationId, programId, sessionId, courseId, formatDate, toDate, amount, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return discounts;
        }
        public IList<Discount> LoadDiscount(Program program, Session session, DateTime stdAdmitDate)
        {
            IList<Discount> discounts;
            try
            {
                discounts = _discountDao.GetDiscountByProgramSessionAndDate(program, session, stdAdmitDate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return discounts;
        }
     
        #endregion

        #region Others Function
        //public int GetDiscountCount(string orderBy, string orderDir, string organizationId, string programId, string sessionId, string courseId, string formatDate, string toDate, string amount, string status)
        public int GetDiscountCount(string orderBy, string orderDir, List<UserMenu> userMenu, string organizationId, string programId, string sessionId, string courseId, string formatDate, string toDate, string amount, string status)
        {
            int discountCount;
            try
            {
                long? convertedOrganizationId = String.IsNullOrEmpty(organizationId) ? (long?)null : Convert.ToInt64(organizationId);
                long? convertedProgramId = String.IsNullOrEmpty(programId) ? (long?)null : Convert.ToInt64(programId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (convertedProgramId != null) ? _commonHelper.ConvertIdToList((long)convertedProgramId) : null);

                discountCount = _discountDao.GetDiscountCount(orderBy, orderDir, organizationIdList, programIdList, sessionId, courseId, formatDate, toDate, amount, status);
                //discountCount = _discountDao.GetDiscountCount(orderBy, orderDir, organizationId, programId, sessionId, courseId, formatDate, toDate, amount, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return discountCount;
        }

        #endregion

        //no need
        public bool Update(Discount discount)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    _discountDao.Update(discount);
                    trans.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
    }
}
