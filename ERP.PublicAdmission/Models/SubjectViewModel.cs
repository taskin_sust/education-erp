﻿namespace UdvashERP.PublicAdmission.Models
{
    public class SubjectViewModel
    {
        public virtual long Id { get; set; }
        public virtual string Name { get; set; }
    }
}