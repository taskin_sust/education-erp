﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{

    public class ExpandoObjectResultSetTransformer : IResultTransformer
    {
        public object TransformTuple(object[] tuple, string[] aliases)
        {
            var expando = new ExpandoObject();
            var dictionary = (IDictionary<string, object>)expando;
            for (int i = 0; i < tuple.Length; i++)
            {
                string alias = aliases[i];
                if (alias != null)
                {
                    dictionary[alias] = tuple[i];
                }
            }
            return expando;
        }
        public IList TransformList(IList collection)
        {
            return collection;
        }
    }

    public interface IStudentAdmissionTargetDao : IBaseDao<StudentAdmissionTarget, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Functions
        StudentAdmissionTarget GetstudentAdmissionTarget(long programId, long sessionId, DateTime deadLine, string deadLineTitle);
        StudentAdmissionTarget GetByProgramAndSessionId(long programId, long sessionId, DateTime? deadLineDate);
       
        #endregion

        #region List Loading Functions
        
        
        IList<StudentAdmissionTarget> LoadDeadLine(long organizatonId, long? programId, long? sessionId);
        IList<dynamic> LoadProgramwiseStudentTarget(long targetId, long programId, long sessionId, DateTime deadTime, int currentSessionrank);
        IList<object> LoadBranchwiseStudentTarget(long organizationId, long branchId, long? studentAdmissionProgramId);
        #endregion

        #region Other Functions
        DateTime LoadDeadLine(long programId, long sessionId, string deadLineTitle);
        IList<string> LoadDeadLineTitles(long programId, long sessionId);
        bool HasDuplicateByName(string deadLineTitle, long programId, long sessionId, long? id = null); 
        #endregion             

        #region Clean
        //IList<StudentAdmissionTarget> LoadOk();
        #endregion
    }
    public class StudentAdmissionTargetDao : BaseDao<StudentAdmissionTarget, long>, IStudentAdmissionTargetDao
    {
        #region Propertise & Object Initialization
          public static IResultTransformer ExpandoObject;

        public StudentAdmissionTargetDao()
        {
            ExpandoObject = new ExpandoObjectResultSetTransformer();
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Functions
        public StudentAdmissionTarget GetByProgramAndSessionId(long programId, long sessionId, DateTime? deadLineDate = null)
        {
            var studentAdmisssionTarget = Session.Query<StudentAdmissionTarget>()
                    .Where(x => x.Status == StudentAdmissionTarget.EntityStatus.Active && x.Program.Id == programId && x.Session.Id == sessionId);

            if (deadLineDate != null)
            {
                studentAdmisssionTarget =
                    //studentAdmisssionTarget.Where(x => x.DeadTime.Value.Date >= deadLineDate.Value.Date);
                    //studentAdmisssionTarget.Where(x => x.DeadTime.Date >= deadLineDate.Value.Date);
                    studentAdmisssionTarget.Where(x => x.DeadTime >= deadLineDate);
            }
            return studentAdmisssionTarget.OrderByDescending(x => x.CreationDate).Take(1).SingleOrDefault();
        }

        public StudentAdmissionTarget GetstudentAdmissionTarget(long programId, long sessionId, DateTime deadLine,
             string deadLineTitle)
        {
            return Session.Query<StudentAdmissionTarget>()
                 .Where(x => x.Status == StudentAdmissionTarget.EntityStatus.Active
                     && x.Program.Id == programId
                     && x.Session.Id == sessionId
                     && x.DeadTime.Date == deadLine.Date
                     && x.DeadLineTitle == deadLineTitle.Trim())
                 .SingleOrDefault();
        }

       
        #endregion

        #region List Loading Functions
        

        public IList<StudentAdmissionTarget> LoadDeadLine(long organizatonId, long? programId, long? sessionId)
        {
            ICriteria criteria = Session.CreateCriteria<StudentAdmissionTarget>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", StudentAdmissionTarget.EntityStatus.Delete)));
            criteria.CreateAlias("Program", "p");
            criteria.Add(Restrictions.Eq("p.Organization.Id", organizatonId));
            if (programId != null && programId > 0)
            {
                criteria.Add(Restrictions.Eq("Program.Id", programId));
            }
            if (sessionId != null && sessionId > 0)
            {
                criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            }

            return (List<StudentAdmissionTarget>)criteria.List<StudentAdmissionTarget>();


        }
        //        public IList<dynamic> LoadProgramwiseStudentTarget(long targetId, long programId, long sessionId, DateTime deadTime)
        //        {
        //            var criteria = Session.CreateCriteria<ProgramSettings>();
        //            criteria.Add(Restrictions.Eq("NextProgram.Id", programId));
        //            criteria.Add(Restrictions.Eq("NextSession.Id", sessionId));
        //            var previousPrograms = criteria.List<ProgramSettings>();
        //            string dateLine = deadTime.ToString("u");
        //            dateLine = dateLine.Remove(dateLine.Length - 1);
        //            #region Normal
        //            string queryOne = "SELECT " +
        //                                                "(SELECT br.Name FROM Branch as br WHERE br.Id = tb.BranchId) as BranchName " +
        //                                                ", tb.InternalTarget AS IT, it.IntAdmCount AS IA " +
        //                    ", CASE WHEN tb.InternalTarget >0 " +
        //                        "THEN CAST(((CAST(it.IntAdmCount AS DECIMAL(10,2))/CAST(tb.InternalTarget AS DECIMAL(10,2)))*100) as DECIMAL(10,2)) " +
        //                        "ELSE 0 " +
        //                    "END AS IAP " +
        //                    ", tb.ExternalTarget as ET, ISNULL(et.ExtAdmCount,0) AS EA " +
        //                    ", CASE WHEN tb.ExternalTarget >0 " +
        //                        "THEN CAST(((CAST(ISNULL(et.ExtAdmCount,0) AS DECIMAL(10,2))/CAST(tb.ExternalTarget AS DECIMAL(10,2)))*100) as DECIMAL(10,2)) " +
        //                        "ELSE 0 " +
        //                    "END AS EAP " +
        //                    ", tb.InternalTarget + tb.ExternalTarget AS TT, it.IntAdmCount + ISNULL(et.ExtAdmCount,0) AS TA " +
        //                    ", CASE WHEN (tb.InternalTarget + tb.ExternalTarget) >0 " +
        //                        "THEN CAST( " +
        //                            "( " +
        //                                "(CAST(it.IntAdmCount AS DECIMAL(10,2)) + CAST(ISNULL(et.ExtAdmCount,0) AS DECIMAL(10,2)))  " +
        //                                "/ (CAST(tb.InternalTarget AS DECIMAL(10,2)) + CAST(tb.ExternalTarget AS DECIMAL(10,2))) " +
        //                            ")*100 AS DECIMAL(10,2))  " +
        //                        "ELSE 0 " +
        //                    "END AS TAP " +
        //                "FROM StudentAdmissionTarget as t " +
        //                "INNER JOIN StudentAdmissionBranchTarget as tb ON tb.StudentAdmissionTargetId = t.Id " +
        //                "LEFT JOIN ( " +
        //                    "SELECT a.BranchId, Count(*) PreAdm, SUM(IntAdm) IntAdmCount " +
        //                    "FROM ( " +
        //                        "SELECT b.BranchId, s.Id stuId, sp.Id spId " +
        //                            ", ISNULL(( " +
        //                                "SELECT 1 " +
        //                                "FROM StudentProgram AS sp1, Batch as b1 " +
        //                                "WHERE sp1.BatchId = b1.Id  " +
        //                                    "AND sp1.StudentId = s.Id  " +
        //                                    "AND sp1.Status = 1 " +
        //                                    "AND sp1.ProgramId = " + programId + " " +
        //                                    "AND b1.SessionId = " + sessionId + " " +
        //                                    "AND sp1.CreationDate<= '" + dateLine + "' " +
        //                            "),0)  as IntAdm " +
        //                        "FROM Student AS s " +
        //                        "INNER JOIN StudentProgram as sp ON sp.StudentId = s.Id " +
        //                        "INNER JOIN Batch as b ON b.Id = sp.BatchId " +
        //                        "WHERE sp.Status = 1 AND ( ";

        //            string queryTwo = "";
        //            if (previousPrograms.Any())
        //            {
        //                int index = 0;
        //                foreach (var programSettingse in previousPrograms)
        //                {
        //                    if (index == 0)
        //                    {
        //                        queryTwo = queryTwo + "(sp.ProgramId = " + programSettingse.Program.Id + " AND b.SessionId = " + programSettingse.Session.Id + ") ";
        //                        index++;
        //                    }
        //                    else
        //                    {
        //                        queryTwo = queryTwo + "OR (sp.ProgramId = " + programSettingse.Program.Id + " AND b.SessionId = " + programSettingse.Session.Id + ") ";
        //                    }
        //                }
        //            }
        //            string queryThree = ") AND b.BranchId IN (SELECT BranchId FROM StudentAdmissionBranchTarget WHERE StudentAdmissionTargetId = " + targetId + " ) " +
        //    ") as a " +
        //    "GROUP BY a.BranchId " +
        //") as it ON it.BranchId = tb.BranchId " +
        //"LEFT JOIN ( " +
        //    "SELECT b.BranchId, COUNT(*) ExtAdmCount " +
        //    "FROM StudentProgram as sp " +
        //    "INNER JOIN Batch as b ON b.Id = sp.BatchId " +
        //    "WHERE sp.Status = 1 " +
        //        "AND sp.ProgramId = " + programId + " " +
        //        "AND b.SessionId = " + sessionId + " " +
        //        "AND sp.CreationDate<= '" + dateLine + "' " +
        //        "AND b.BranchId IN (SELECT BranchId FROM StudentAdmissionBranchTarget WHERE StudentAdmissionTargetId = " + targetId + " ) " +
        //        "AND sp.StudentId NOT IN ( " +
        //            "SELECT sp.StudentId FROM StudentProgram as sp " +
        //            "INNER JOIN Batch as b ON b.Id = sp.BatchId " +
        //            "WHERE sp.Status = 1 AND ( " + queryTwo + " ) " +
        //        ") " +
        //    "GROUP BY b.BranchId " +
        //") as et ON et.BranchId = tb.BranchId " +
        //"WHERE t.Id = " + targetId;
        //            #endregion
        //            var totalQuery = queryOne + queryTwo + queryThree;
        //            IQuery query = Session.CreateSQLQuery(totalQuery);
        //            IList<dynamic> result = DynamicList(query);
        //            return result;           
        //        }

        public IList<dynamic> DynamicList(IQuery query)
        {
            var result = query.SetResultTransformer(ExpandoObject)
                        .List<dynamic>();
            return result;
        }
        public IList<dynamic> LoadProgramwiseStudentTarget(long targetId, long programId, long sessionId, DateTime deadTime, int currentSessionrank)
        {
            string queryOne = "SELECT * FROM ( ";
             queryOne += "SELECT "
                              + " br.Name as BranchName "
                              + " , pro.Name as ProgramName "
                              + " , sat.DeadTime as DeadLine "
                              + " , sabt.Target as Target "
                              + " , a.studentCount as Achievement "
                              + " , ROUND( CASE WHEN sabt.Target >0 THEN CAST(((CAST(ISNULL(a.studentCount,0) AS DECIMAL(10,2))/CAST(sabt.Target AS DECIMAL(10,2)))*100) as DECIMAL(10,0)) ELSE 0 END ,0 ) as AchievementPercentage "
                            ;
            //previous session Student Count
            queryOne += " , ( "
                         + " CASE WHEN (SELECT TOP 1 ID FROM [Session] WHERE [Status] = 1 AND [rank] > " + currentSessionrank + " ORDER BY [rank] asc ) IS NOT NULL THEN "
                            + " ( "
                            +" select  CAST(count(sp.Id) as nvarchar(8)) from StudentProgram as sp "
                            +" Inner Join Batch as ba on ba.Id = sp.BatchId and ba.Status = 1 "
                            +" where ba.BranchId = a.BranchId "
                            + " and ba.SessionId = (SELECT TOP 1 ID FROM [Session] WHERE [Status] = 1 AND [rank] > " + currentSessionrank + "  ORDER BY [rank] asc) "
                            + " and sp.ProgramId =" + programId + " "
		                    +" and sp.Status = 1 "
                            +" ) "
		                +" ELSE 'N/A' "
		                +" END "
                    + " ) as PreviousSessionStudentCount ";
            //previous session Student Count End
            //previous Selected Student Count  // We need this code after 6 month today(20150916)
            //queryOne += " , ( "
            //             + " CASE WHEN (SELECT TOP 1 ID FROM [Session] WHERE [Status] = 1 AND [rank] > " + currentSessionrank + ") IS NOT NULL THEN "
            //                + " ( "
            //                + " select  CAST(count(sp.Id) as nvarchar(8)) from StudentProgram as sp "
            //                + " Inner Join Batch as ba on ba.Id = sp.BatchId and ba.Status = 1 "
            //                + " where ba.BranchId = a.BranchId "
            //                + " and ba.SessionId = (SELECT TOP 1 ID FROM [Session] WHERE [Status] = 1 AND [rank] > " + currentSessionrank + " ORDER BY [rank] asc) "
            //                + " and sp.ProgramId =" + programId + " "
            //                + " and sp.Status = 1 "
            //                + " and sp.CreationDate <=  dateadd(year, -1, sat.DeadTime) "
            //                + " ) "
            //            + " ELSE 'N/A' "
            //            + " END "
            //        + " ) as PreviousSessionDeadLineStudentCount ";
            //previous Selected Student Count  End
            queryOne += " FROM StudentAdmissionBranchTarget AS sabt "
                        + " INNER JOIN  StudentAdmissionTarget AS sat ON sabt.StudentAdmissionTargetId = sat.Id "
                        + " INNER JOIN Branch AS br ON br.Id = sabt.BranchId "
                        + " INNER JOIN Program AS pro ON pro.Id = sat.ProgramId "
                        ;

            //deadTime.AddHours(23).AddMinutes(59).AddSeconds(59);
            //string dateLine = deadTime.ToString("u");
            //dateLine = dateLine.Remove(dateLine.Length - 1);
            string dateLine = deadTime.Date.ToString("yyyy-MM-dd") + " 23:59:59.000";
            string queryTwo = " LEFT JOIN ( ";
            queryTwo += " SELECT "
                        + " ba.BranchId, COUNT(sp.Id) AS studentCount "
                        + " FROM StudentProgram AS sp "
                        + " INNER JOIN Batch AS ba ON ba.Id = sp.BatchId  and ba.status = 1"
                        + " WHERE sp.Status = 1 "
                        + " AND sp.ProgramId =  " + programId + " "
                        + " AND ba.SessionId =  " + sessionId + " "
                        + " AND sp.CreationDate <=  '" + dateLine + "' "
                        + " GROUP BY ba.BranchId";

            queryTwo += " ) AS a ON a.BranchId = sabt.BranchId ";
            string queryThree = " WHERE sat.id = " + targetId;
            queryThree += " ) as a  ";
            queryThree += " order by a.AchievementPercentage DESC ";    

            var totalQuery = queryOne + queryTwo + queryThree;
            IQuery query = Session.CreateSQLQuery(totalQuery);
            IList<dynamic> result = DynamicList(query);
            return result;
        }


        public IList<object> LoadBranchwiseStudentTarget(long organizationId, long branchId, long? studentAdmissionProgramId)
        {
            string queryOne = "SELECT * FROM ( ";
             queryOne += "SELECT  "
                               + " a.Name as ProgramName "
                               + " , a.DeadTime as DeadLine "
                               + " , a.Target as Target "
                               + " , a.total as Achievement "
                               + " , ROUND(CASE WHEN a.Target >0 THEN CAST(((CAST(ISNULL(total,0) AS DECIMAL(10,2))/CAST(a.Target AS DECIMAL(10,2)))*100) as DECIMAL(10,0)) ELSE 0 END, 0) AS AchievementPercentage "
                               + " , a.PreviousSessionStudentCount as PreviousSessionStudentCount "
                             ;

            queryOne += "FROM ( "
                        + " SELECT  a.StudentAddmissionTargetId  "
                        + " , a.ProgramId "
                        + " , a.Name "
                        + " , a.SessionId "
                        + " , a.DeadTime "
                        + " , a.Target "
                        + " , a.BranchId "
                        + " , ( "
                                + " select count(sp.Id) from StudentProgram as sp "
                                + " Inner Join Batch as b on b.Id = sp.BatchId and b.Status = 1 "
                                + " where sp.Status = 1 and b.BranchId = a.BranchId and b.SessionId = a.SessionId and sp.ProgramId = a.ProgramId "
                                + " and CONVERT(varchar, sp.CreationDate, 112) <= CONVERT(varchar, a.DeadTime, 112) "
                             + " ) as total "
                        ;


            //previous session Student Count
            queryOne += " , ( "
                         + " CASE WHEN (SELECT TOP 1 ID FROM [Session] WHERE [Status] = 1 AND [rank] > a.SessionRank) IS NOT NULL THEN "
                            + " ( "
                            + " select  CAST(count(sp.Id) as nvarchar(8)) from StudentProgram as sp "
                            + " Inner Join Batch as ba on ba.Id = sp.BatchId and ba.Status = 1 "
                            + " where ba.BranchId = a.BranchId "
                            + " and ba.SessionId = (SELECT TOP 1 ID FROM [Session] WHERE [Status] = 1 AND [rank] > a.SessionRank  ORDER BY [rank] asc) "
                            + " and sp.ProgramId = a.ProgramId "
                            + " and sp.Status = 1 "
                            + " ) "
                        + " ELSE 'N/A' "
                        + " END "
                    + " ) as PreviousSessionStudentCount ";
            //previous session Student Count End
            //previous session Student Count // We need this code after 6 month today(20150916)
            //queryOne += " , ( "
            //             + " CASE WHEN (SELECT TOP 1 ID FROM [Session] WHERE [Status] = 1 AND [rank] > a.SessionRank) IS NOT NULL THEN "
            //                + " ( "
            //                + " select  CAST(count(sp.Id) as nvarchar(8)) from StudentProgram as sp "
            //                + " Inner Join Batch as ba on ba.Id = sp.BatchId and ba.Status = 1 "
            //                + " where ba.BranchId = a.BranchId "
            //                + " and ba.SessionId = (SELECT TOP 1 ID FROM [Session] WHERE [Status] = 1 AND [rank] > a.SessionRank) "
            //                + " and sp.ProgramId = a.ProgramId "
            //                + " and sp.Status = 1 "
            //                + " and sp.CreationDate <=  dateadd(year, -1, a.DeadTime) "
            //                + " ) "
            //            + " ELSE 'N/A' "
            //            + " END "
            //        + " ) as PreviousSessionDeadLineStudentCount ";
            //previous session Student Count End
            string queryTwo = " FROM ( ";
            queryTwo += " SELECT  RANK() OVER (PARTITION BY sat.ProgramId, sat.SessionId ORDER BY sat.DeadTime DESC) as r "
                        + " , sat.Id as StudentAddmissionTargetId "
                        + " , sat.ProgramId "
                        + " , p.Name "
                        + " , sat.SessionId "
                        + ", s.[Rank] as SessionRank "
                        + " , sat.DeadTime   "
                        + "  , ISNULL(sabt.Target,0) [Target] "
                        + "  , sabt.BranchId "
                        + " FROM [dbo].[StudentAdmissionBranchTarget] sabt "
                        + " INNER JOIN [dbo].[StudentAdmissionTarget] sat ON sat.Id = sabt.StudentAdmissionTargetId AND sat.status = 1 "
                        + " INNER JOIN [dbo].[Program] as p ON p.id = sat.ProgramId "
                        + " INNER JOIN [dbo].[Session] as S ON S.Id = sat.SessionId "
                        + " WHERE sabt.status = 1 and sabt.BranchId =  " + branchId + " AND p.OrganizationId = " + organizationId + " "
                        + " ) as a "
                        + " where r=1 "
                        + " ) as a "
                        ;

            if (studentAdmissionProgramId != null && studentAdmissionProgramId > 0)
            {
                queryTwo += "   WHERE a.StudentAddmissionTargetId = " + studentAdmissionProgramId;
            }
            queryTwo += " ) as a  ";
            queryTwo += " order by a.AchievementPercentage DESC "; 
            var totalQuery = queryOne + queryTwo;
            IQuery query = Session.CreateSQLQuery(totalQuery);
            IList<dynamic> result = DynamicList(query);
            return result;
        }

       
        #endregion      

        #region Other Functions

        public IList<string> LoadDeadLineTitles(long programId, long sessionId)
        {
            return Session.Query<StudentAdmissionTarget>().Where(x => x.Program.Id == programId && x.Session.Id == sessionId && x.Status == StudentAdmissionTarget.EntityStatus.Active).OrderByDescending(x => x.DeadTime).ThenByDescending(x=>x.Id)
                    .Select(x => x.DeadLineTitle).ToList();
        } 

        public DateTime LoadDeadLine(long programId, long sessionId, string deadLineTitle)
        {
            return Session.QueryOver<StudentAdmissionTarget>().Where(x => x.Program.Id == programId && x.Session.Id == sessionId && x.DeadLineTitle == deadLineTitle.Trim() && x.Status == StudentAdmissionTarget.EntityStatus.Active).Select(x => x.DeadTime).SingleOrDefault<DateTime>();
        }
        public bool HasDuplicateByName(string deadLineTitle, long programId, long sessionId, long? id = null)
        {
            ICriteria criteria = Session.CreateCriteria<StudentAdmissionTarget>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", StudentAdmissionTarget.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("DeadLineTitle", deadLineTitle));
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            if (id != null && id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            var questionList = criteria.List<StudentAdmissionTarget>();
            if (questionList != null && questionList.Count > 0)
                return true;
            return false;
        }
        #endregion                  
        
        #region Helper Function

        #endregion

        #region Clean
        //public IList<StudentAdmissionTarget> LoadOk()
        //{
        //    return
        //        Session.QueryOver<StudentAdmissionTarget>()
        //            .Where(x => x.Status == StudentAdmissionTarget.EntityStatus.Active)
        //            .List<StudentAdmissionTarget>();
        //}
        #endregion
    }
}
