﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    class StudentExamAttendanceMap : BaseClassMap<StudentExamAttendance, long>
    {
        public StudentExamAttendanceMap()
        {
            Table("StudentExamAttendance");
            LazyLoad();
            Map(x => x.HeldDate);
            References(x => x.Exams).Column("ExamId");
            HasMany(x => x.StudentExamAttendanceDetails).KeyColumn("StudentExamAttendanceId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
