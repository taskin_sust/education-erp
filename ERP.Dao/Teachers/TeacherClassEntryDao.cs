﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.BusinessRules;

namespace UdvashERP.Dao.Teachers
{
    public interface ITeacherClassEntryDao : IBaseDao<TeacherClassEntry, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        TeacherClassEntry GetTeacherClassEntry();

        #endregion

        #region List Loading Function

        IList<TeacherCampusRoutineDto> ListTeacherCampusRoutine(int start, int length, string orderBy, string orderDir, long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string heldDate, string fullName, string nickName, string mobile, string tpin);
        List<TeacherClassPaymentReport> LoadClassPaymentReportList(List<long> authBranchIdList, List<long> authProgramIdList, List<long> sessionIdToList, List<long> campusIdList, DateTime dateFrom, DateTime dateTo, TeacherClassReportType reportType, TeacherClassReportTypeGroupBy reportTypeGroupBy, TeacherClassReportPaymentStatus paymentStatus, string tPinList, int start, string orderBy, string orderDir, int noOfData);
        List<TeacherSeniorshipCalculationReport> LoadSeniorshipCalculationReportList(List<long> authOrganizationList, List<long> authBranchIdList, DateTime tillDate, string tPinList, int start, string orderBy, string orderDir, int noOfData);

        #endregion

        #region Others Function

        int CountTeachercampusRoutine(long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string heldDate, string fullName, string nickName, string mobile, string tpin);
        int GetClassPaymentReportListCount(List<long> authBranchIdList, List<long> authProgramIdList, List<long> sessionIdToList, List<long> campusIdList, DateTime dateFrom, DateTime dateTo, TeacherClassReportType reportType, TeacherClassReportTypeGroupBy reportTypeGroupBy, TeacherClassReportPaymentStatus paymentStatus, string tPinList);
        int GetSeniorshipCalculationReportListCount(List<long> authOrganizationList, List<long> authBranchIdList, DateTime tillDate, string tpinList);
        #endregion

    }

    public class TeacherClassEntryDao : BaseDao<TeacherClassEntry, long>, ITeacherClassEntryDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public TeacherClassEntry GetTeacherClassEntry()
        {
            return Session.QueryOver<TeacherClassEntry>()
                    .Where(x => x.Status == TeacherClassEntry.EntityStatus.Active).OrderBy(tx => tx.SerialNumber).Desc.Take(1)
                    .SingleOrDefault<TeacherClassEntry>();
        }

        #endregion

        #region List Loading Function

        public IList<TeacherCampusRoutineDto> ListTeacherCampusRoutine(int start, int length, string orderBy, string orderDir, long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string heldDate, string fullName, string nickName, string mobile, string tpin)
        {
            ICriteria criteria = GetTeacherCampusRoutineICriteriaQuery(organizationId, programId, sessionId, branchIds, campusIds, heldDate, fullName, nickName, mobile, tpin);

            criteria.SetProjection(Projections.ProjectionList()
                .Add(Projections.GroupProperty("Teacher"))
                .Add(Projections.GroupProperty("tce.Branch"))
                .Add(Projections.GroupProperty("tce.Campus"))
                .Add(Projections.Alias(Projections.Sum("Quantity"), "TotalQuantity"))
                //.Add(Projections.GroupProperty("Id"),"Id")
                .Add(Projections.GroupProperty("b.Name"), "BranchName")
                .Add(Projections.GroupProperty("c.Name"), "CampusName")
                .Add(Projections.GroupProperty("t.Tpin"), "Tpin")
                .Add(Projections.GroupProperty("t.FullName"), "FullName")
                .Add(Projections.GroupProperty("t.NickName"), "NickName")
                .Add(Projections.GroupProperty("t.Institute"), "Institute")
                .Add(Projections.GroupProperty("t.Department"), "Department")
                .Add(Projections.GroupProperty("t.PersonalMobile"), "PersonalMobile")
                .Add(Projections.GroupProperty("t.AlternativeMobile"), "AlternativeMobile")
                .Add(Projections.GroupProperty("t.RoomMatesMobile"), "RoomMatesMobile")
                .Add(Projections.GroupProperty("t.FathersMobile"), "FathersMobile")
                .Add(Projections.GroupProperty("t.MothersMobile"), "MothersMobile")
                .Add(Projections.GroupProperty("t.HscPassingYear"), "TeacherBatch")
                );
            criteria.SetResultTransformer(Transformers.AliasToBean<TeacherCampusRoutineDto>());
            criteria.SetFirstResult(start).SetMaxResults(length);
            var a = criteria.List<TeacherCampusRoutineDto>();
            return a;
        }

        public List<TeacherClassPaymentReport> LoadClassPaymentReportList(List<long> authBranchIdList, List<long> authProgramIdList, List<long> sessionIdToList, List<long> campusIdList, DateTime dateFrom, DateTime dateTo, TeacherClassReportType reportType, TeacherClassReportTypeGroupBy reportTypeGroupBy, TeacherClassReportPaymentStatus paymentStatus, string tPinList, int start, string orderBy, string orderDir, int noOfData)
        {

            string query = GetLoadClassPaymentReportListQuery(authBranchIdList, authProgramIdList, sessionIdToList, campusIdList, dateFrom, dateTo, reportType, reportTypeGroupBy, paymentStatus, tPinList);

            query = query + " Order By " + orderBy + " " + orderDir + " ";

            if (noOfData > 0)
            {
                query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + noOfData + " ROWS ONLY";
            }

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeacherClassPaymentReport>());
            List<TeacherClassPaymentReport> list = iQuery.List<TeacherClassPaymentReport>().ToList();
            return list;
        }

        public List<TeacherSeniorshipCalculationReport> LoadSeniorshipCalculationReportList(List<long> authOrganizationList, List<long> authBranchIdList, DateTime tillDate, string tPinList, int start,
            string orderBy, string orderDir, int noOfData)
        {
            string query = GetSeniorshipCalculationReportListQuery(authOrganizationList, authBranchIdList, tillDate, tPinList);

            query = query + " Order By " + orderBy + " " + orderDir + " ";

            if (noOfData > 0)
            {
                query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + noOfData + " ROWS ONLY";
            }

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeacherSeniorshipCalculationReport>());
            List<TeacherSeniorshipCalculationReport> list = iQuery.List<TeacherSeniorshipCalculationReport>().ToList();
            return list;
        }

        #endregion

        #region Others Function

        public int CountTeachercampusRoutine(long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string heldDate, string fullName, string nickName, string mobile, string tpin)
        {
            ICriteria criteria = GetTeacherCampusRoutineICriteriaQuery(organizationId, programId, sessionId, branchIds, campusIds, heldDate, fullName, nickName, mobile, tpin);
            criteria.SetProjection(Projections.CountDistinct("Id"));
            return (int)criteria.UniqueResult();
        }

        public int GetClassPaymentReportListCount(List<long> authBranchIdList, List<long> authProgramIdList, List<long> sessionIdToList, List<long> campusIdList, DateTime dateFrom, DateTime dateTo, TeacherClassReportType reportType, TeacherClassReportTypeGroupBy reportTypeGroupBy, TeacherClassReportPaymentStatus paymentStatus, string tPinList)
        {
            string query = GetLoadClassPaymentReportListQuery(authBranchIdList, authProgramIdList, sessionIdToList, campusIdList, dateFrom, dateTo, reportType, reportTypeGroupBy, paymentStatus, tPinList);
            string countQuery = "Select count(*) From ( " + query + " ) as a ";
            IQuery iQuery = Session.CreateSQLQuery(countQuery);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int GetSeniorshipCalculationReportListCount(List<long> authOrganizationList, List<long> authBranchIdList, DateTime tillDate,
            string tpinList)
        {
            string query = GetSeniorshipCalculationReportListQuery(authOrganizationList, authBranchIdList, tillDate, tpinList);
            string countQuery = "Select count(*) From ( " + query + " ) as a ";
            IQuery iQuery = Session.CreateSQLQuery(countQuery);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function

        private ICriteria GetTeacherCampusRoutineICriteriaQuery(long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string heldDate, string fullName, string nickName, string mobile, string tpin)
        {
            ICriteria criteria = Session.CreateCriteria<TeacherClassEntryDetails>().Add(Restrictions.Eq("Status", TeacherClassEntryDetails.EntityStatus.Active));
            criteria.CreateAlias("TeacherClassEntry", "tce").Add(Restrictions.Eq("tce.Status", TeacherClassEntry.EntityStatus.Active));
            criteria.CreateAlias("Teacher", "t").Add(Restrictions.Eq("t.Status", Teacher.EntityStatus.Active));
            criteria.CreateAlias("tce.Organization", "o").Add(Restrictions.Eq("o.Status", Organization.EntityStatus.Active));
            criteria.CreateAlias("tce.Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("tce.Session", "s").Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.CreateAlias("tce.Branch", "b").Add(Restrictions.Eq("b.Status", Branch.EntityStatus.Active));
            criteria.CreateAlias("tce.Campus", "c").Add(Restrictions.Eq("c.Status", Campus.EntityStatus.Active));

            criteria.Add(Restrictions.Eq("o.Id", organizationId));
            criteria.Add(Restrictions.Eq("p.Id", programId));
            criteria.Add(Restrictions.Eq("s.Id", sessionId));

            if (!String.IsNullOrEmpty(heldDate))
            {
                DateTime dt = DateTime.ParseExact(heldDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                criteria.Add(Expression.Eq(Projections.SqlFunction("date", NHibernateUtil.DateTime, Projections.Property("tce.HeldDate")), dt));
            }

            if (!String.IsNullOrEmpty(tpin))
            {
                int teacherPin;
                if (int.TryParse(tpin, out teacherPin))
                {
                    criteria.Add(Restrictions.Eq("t.Tpin", teacherPin));
                }
            }
            if (!branchIds.Contains(0))
            {
                criteria.Add(Restrictions.In("b.Id", branchIds));
            }
            if (!campusIds.Contains(0))
            {
                criteria.Add(Restrictions.In("c.Id", campusIds));
            }
            if (!String.IsNullOrEmpty(fullName))
            {
                criteria.Add(Restrictions.Like("t.FullName", fullName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(nickName))
            {
                criteria.Add(Restrictions.Like("t.NickName", nickName, MatchMode.Anywhere));
            }
            if (!string.IsNullOrEmpty(mobile))
            {
                criteria.Add(Restrictions.Like("t.PersonalMobile", mobile, MatchMode.Anywhere));
            }

            return criteria;
        }

        private string GetLoadClassPaymentReportListQuery(List<long> authBranchIdList, List<long> authProgramIdList, List<long> sessionIdToList, List<long> campusIdList, DateTime dateFrom, DateTime dateTo, TeacherClassReportType reportType, TeacherClassReportTypeGroupBy reportTypeGroupBy, TeacherClassReportPaymentStatus paymentStatus, string tPinList)
        {
            string dailySummaryReportTypeGroupByString = " ";
            string dailySummaryReportTypeSelectString = "";
            string dailySummaryReportTypeMainSelectString = " ";

            string reportTypeGroupByMainSelectString = " ";
            string reportTypeGroupBySelectString = " ";
            string reportTypeGroupByString = " ";
            string reportTypeInnerJoin = "  ";

            string branchFilter = "";
            string campusFilter = "";
            string programFilter = "";
            string sessionFilter = "";
            string dateFilter = " AND tce.HeldDate >= '" + dateFrom.Date.ToString("yyyy-MM-dd") + " 00:00:00'  AND tce.HeldDate <= '" + dateTo.Date.ToString("yyyy-MM-dd") + " 23:59:59' ";
            string pinFilter = "";
            string paymentStatusFilter = "";

            //if (!String.IsNullOrEmpty(tPinList))
            //{
            //    pinFilter = " AND t.Tpin  IN (" + tPinList + ")";
            //}
            //modified only search on name and pin 
            if (!String.IsNullOrEmpty(tPinList))
            {
                pinFilter = @" AND ( CONVERT(varchar(10), t.Tpin) = '" + tPinList + "' OR t.NickName like '%" + tPinList + "%' OR t.FullName like '%" + tPinList + "%' )";
            }

            if (authBranchIdList != null && authBranchIdList.Any() && !authBranchIdList.Contains(SelectionType.SelelectAll))
            {
                branchFilter = " AND tce.HeldBranchId  IN (" + string.Join(",", authBranchIdList.ToArray()) + ")";
            }
            if (campusIdList != null && campusIdList.Any() && !campusIdList.Contains(SelectionType.SelelectAll))
            {
                campusFilter = " AND tce.HeldCampusId  IN (" + string.Join(",", campusIdList.ToArray()) + ")";
            }
            if (authProgramIdList != null && authProgramIdList.Any() && !authProgramIdList.Contains(SelectionType.SelelectAll))
            {
                programFilter = " AND tce.ProgramId  IN (" + string.Join(",", authProgramIdList.ToArray()) + ")";
            }
            if (sessionIdToList != null && sessionIdToList.Any() && !sessionIdToList.Contains(SelectionType.SelelectAll))
            {
                sessionFilter = " AND tce.SessionId  IN (" + string.Join(",", sessionIdToList.ToArray()) + ")";
            }


            if (reportType == TeacherClassReportType.DateWise)
            {
                dailySummaryReportTypeMainSelectString = ", CONVERT(varchar,a.HeldDate,106) AS Date ";
                dailySummaryReportTypeSelectString = " , tce.HeldDate AS HeldDate ";
                dailySummaryReportTypeGroupByString = " tce.HeldDate ,";
            }
            if (reportTypeGroupBy == TeacherClassReportTypeGroupBy.ProgramWise)
            {
                reportTypeInnerJoin = @" INNER JOIN Program p ON p.Id = a.ProgramId 
                                         INNER JOIN Session s ON s.Id = a.SessionId 
                                         ";
                reportTypeGroupByMainSelectString = " , p.Name+' ('+s.Name+')'  AS ProgramSession ";
                reportTypeGroupBySelectString = " , tce.ProgramId as ProgramId, tce.SessionId as SessionId ";
                reportTypeGroupByString = " , tce.ProgramId, tce.SessionId ";
            }
            else if (reportTypeGroupBy == TeacherClassReportTypeGroupBy.BranchWise)
            {
                reportTypeInnerJoin = @" INNER JOIN Branch b ON b.Id = a.BranchId 
                                         INNER JOIN Campus c ON c.Id = a.CampusId
                                             ";
                reportTypeGroupByMainSelectString = " , b.Name AS Branch, c.Name as Campus ";
                reportTypeGroupBySelectString = " , tce.HeldBranchId as BranchId, tce.HeldCampusId as CampusId";
                reportTypeGroupByString = " , tce.HeldBranchId, tce.HeldCampusId ";
            }
            else if (reportTypeGroupBy == TeacherClassReportTypeGroupBy.TeacherWise)
            {

            }

            if (paymentStatus != null && (int) paymentStatus != SelectionType.SelelectAll)
            {
                paymentStatusFilter = " AND tcd.PaymentStatus = " + (int)paymentStatus + @" ";
            }

            string query = @"Select 
                                t.Id as TeacherId
	                            , t.Tpin as Tpin
	                            " + dailySummaryReportTypeMainSelectString + reportTypeGroupByMainSelectString + @"
	                            , CASE WHEN t.FullName is null then t.NickName ELSE t.FullName + ' ('+t.NickName+')' END as TeacherName
	                            , CONVERT(varchar(10), t.HscPassingYear) as TeacherBatch
	                            , a.TotalClass as  TotalClass
	                            , a.TotalAmount as TotalAmount
                            from Teacher as t 
                            inner join (
	                            Select 
		                            t.Id
		                            , MAX(CASE WHEN t.FullName is null then t.NickName ELSE t.FullName + ' ('+t.NickName+')' END) as Name
		                            , SUM(ISNULL(tcd.Quantity,0)) as TotalClass
		                            , SUM(ISNULL(tcd.Amount,0)) as TotalAmount
		                            " + dailySummaryReportTypeSelectString + reportTypeGroupBySelectString + @"
	                            from TeacherClassEntryDetails as tcd
	                            inner join TeacherClassEntry as tce on tcd.TeacherClassEntryId = tce.Id 
	                            inner join Teacher as t on t.Id = tcd.TeacherId
                                where tcd.Status = " + TeacherClassEntryDetails.EntityStatus.Active + @"
	                           " + paymentStatusFilter + pinFilter + dateFilter + branchFilter + campusFilter + programFilter + sessionFilter + @"
	                            group by 
		                            " + dailySummaryReportTypeGroupByString + @"
		                            t.Id " + reportTypeGroupByString + @"
	                            ) as a on a.Id = t.Id
                            " + reportTypeInnerJoin + @" ";

            return query;
        }

        private string GetSeniorshipCalculationReportListQuery(List<long> authOrganizationList, List<long> authBranchIdList, DateTime tillDate, string tPinList)
        {
            string dateFilterTeacherPayment = " AND tce.PaymentDate <= '" + tillDate.Date.ToString("yyyy-MM-dd") + " 23:59:59' ";
            string openingDateFilter = " AND tcob.OpeningDate <= '" + tillDate.Date.ToString("yyyy-MM-dd") + " 23:59:59' ";
            string branchInnerjoin = "";
            string pinFilter = "";
            string openingBalanceOrganizationFilter = "";
            string organizationFilter = "";
            if (!String.IsNullOrEmpty(tPinList))
            {
                pinFilter = " AND t.Tpin  IN (" + tPinList + ")";
            }

            //if (authBranchIdList != null && authBranchIdList.Any() && !authBranchIdList.Contains(SelectionType.SelelectAll))
            //{
            //    branchFilter = " AND tce.HeldBranchId  IN (" + string.Join(",", authBranchIdList.ToArray()) + ")";
            //}
            if (authOrganizationList != null && authOrganizationList.Any() && !authOrganizationList.Contains(SelectionType.SelelectAll))
            {
                branchInnerjoin = " inner join Branch as b on b.Id = tce.PaidFromBranchId  ";
                openingBalanceOrganizationFilter = " AND tcob.OrganizationId  IN (" + string.Join(",", authOrganizationList.ToArray()) + ")";
                organizationFilter = " AND b.OrganizationId  IN (" + string.Join(",", authOrganizationList.ToArray()) + ")";
            }
//            var query2 = @" Select 
//                                t.Id as TeacherId
//	                            , t.Tpin as Tpin
//	                            , LTRIM (RTRIM (CASE WHEN t.FullName is null then t.NickName ELSE t.FullName + ' ('+t.NickName+')' END ) ) as TeacherName
//	                            , CONVERT(varchar(10), t.HscPassingYear) as TeacherBatch
//	                            , ISNULL(a.TotalClass,0) as  TotalClass
//                                ,CASE WHEN a.TotalClass>=1000 THEN 'Seniorship' ELSE 'N/A' END AS 'Status'
//                                from Teacher as t 
//                                left join (
//	                            Select 
//		                        unionQuery.Id
//		                        , MAX(CASE WHEN unionQuery.FullName is null then unionQuery.NickName ELSE unionQuery.FullName + ' ('+unionQuery.NickName+')' END) as Name
//		                        , ISNULL(SUM(unionQuery.Quantity),0) as TotalClass
//		                        from (select t.Id,t.FullName,t.NickName ,tcd.Quantity,tcd.Status from TeacherClassEntryDetails as tcd
//	                            inner join TeacherClassEntry as tce on tcd.TeacherClassEntryId = tce.Id 
//	                            inner join Teacher as t on t.Id = tcd.TeacherId where tce.Status= " + TeacherClassEntry.EntityStatus.Active
//                                + dateFilter + branchFilter +
//                                @" union all select TeacherId Id,'' as FullName,'' as NickName ,TotalClass as Quantity,Status as 'Status' 
//                                from TeacherClassOpeningBalance where Status= " + TeacherClassOpeningBalance.EntityStatus.Active
//                                + organizationFilter + openingDateFilter +
//                                @")unionQuery
//                                where unionQuery.Status = 1 "+
//                                @" group by 
//		                        unionQuery.Id
//	                            ) as a on a.Id = t.Id "+ pinFilter;
            string query = @" Select 
                     t.Id as TeacherId
                     , t.Tpin as Tpin
                     , LTRIM (RTRIM (CASE WHEN t.FullName is null then t.NickName ELSE t.FullName + ' ('+t.NickName+')' END ) ) as TeacherName
                     , CONVERT(varchar(10), t.HscPassingYear) as TeacherBatch
                     , ISNULL(aa.TotalClass,0)+ISNULL(a.TotalClass,0) as  TotalClass
                     , CASE WHEN (ISNULL(aa.TotalClass,0)+ISNULL(a.TotalClass,0)) >=1000 THEN 'Seniorship' ELSE 'N/A' END AS 'Status' /* 1000 to above = Seniorship */
                    from Teacher as t 
                    left join (
                     Select 
                      tce.TeacherId
                      , Sum(tce.Quantity) as TotalClass
                     from TeacherClassEntryDetails as tce 
                     " + branchInnerjoin + @"
                     where tce.Status = " + TeacherClassEntryDetails.EntityStatus.Active + @"
                      " + organizationFilter + dateFilterTeacherPayment + @"
                     Group By tce.TeacherId
                    ) as aa on aa.TeacherId = t.Id
                    left join (
                     select 
                      tcob.TeacherId as TeacherId
                      , Sum(tcob.TotalClass) as TotalClass
                     from TeacherClassOpeningBalance as tcob 
                     where tcob.Status = " + TeacherClassOpeningBalance.EntityStatus.Active + @"
                      " + openingBalanceOrganizationFilter+ openingDateFilter + @"
                     group by tcob.TeacherId
                    ) as a on a.TeacherId = t.Id
                    Where t.Status = " + Teacher.EntityStatus.Active + @"
                    " + pinFilter + @"
                    ";

            return query;
        }

        #endregion


    }

}
