﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.MediaDao;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IStudentIdCardService : IBaseService
    {

        #region Operational Function
        void Save(StudentIdCard sic);
        void Update(StudentIdCard studentIdCard);

        #endregion

        #region Single Instances Loading Function
        StudentIdCard getByStudentProgramId(long id);
        #endregion

        #region List Loading Function

        List<ResponseMessage> DistributeIdCard(long programId, long sessionId, List<string> programRoll, IList<Branch> branches);
        List<StudentProgram> LoadAllStudentProgramList(long[] studentProgramIdList); 
        List<StudentProgram> LoadStudentProgramForIndividualIdCard(List<UserMenu> userMenu, List<string> stdProRollStringList);


        #endregion

        #region Others Function
        //service block for list
        List<StudentIdCardListDto> LoadStudentProgram(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, long[] course, int printingStatus, int start, int length, string orderBy, string orderDir, int? distributionStatus = null, bool generateIdCard = false, List<ServiceBlock> serviceBlocks = null);
        #endregion
        
    }
    public class StudentIdCardService : BaseService, IStudentIdCardService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion
        
        #region Propertise & Object Initialization
        private readonly IStudentIdCardDao _StudentIdCardDao;
        private readonly IStudentProgramDao _StudentProgramDao;
        private IServiceBlockService _serviceBlockService;
        private IServiceBlockDao _serviceBlockDao;
        private CommonHelper _commonHelper;

        private readonly IStudentImageMediaDao _studentImageMediaDao;

        public StudentIdCardService(ISession _session)
        {
            Session = _session;
            _StudentIdCardDao = new StudentIdCardDao() { Session = _session };
            _StudentProgramDao = new StudentProgramDao() { Session = _session };
            _serviceBlockService = new ServiceBlockService(_session);
            _serviceBlockDao = new ServiceBlockDao() { Session = _session };
            _commonHelper=new CommonHelper();

            ISession sessionMedia = NHibernateMediaSessionFactory.OpenSession();
            _studentImageMediaDao = new StudentImageMediaDao(sessionMedia);

        }
        #endregion

        #region Operational Function
        public void Save(StudentIdCard sic)
        {
            try
            {
                _StudentIdCardDao.Save(sic);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public void Update(StudentIdCard studentIdCard)
        {
            using (var trans = Session.BeginTransaction())
            {
                try
                {
                    _StudentIdCardDao.Update(studentIdCard);
                    trans.Commit();
                }
                catch (Exception ex)
                {
                   
                    _logger.Error(ex);
                    trans.Rollback();
                        throw ex;
                }
            }
        }

        #endregion

        #region Single Instances Loading Function

        public StudentIdCard getByStudentProgramId(long id)
        {
            try
            {
                return _StudentIdCardDao.getByStudentProgramId(id);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;

            }
        }
        #endregion

        #region List Loading Function
        public List<ResponseMessage> DistributeIdCard(long programId, long sessionId, List<string> programRoll, IList<Branch> branches)
        {
            List<ResponseMessage> messageList = new List<ResponseMessage>();

            //do for each prn
            using (var trans = Session.BeginTransaction())
            {
                foreach (var prn in programRoll)
                {
                    try
                    {
                        var studentProgram = _StudentProgramDao.GetStudentProgram(prn);
                        if (studentProgram != null)
                        {
                            var idCard = _StudentIdCardDao.GetByPrn(programId, sessionId, prn);
                            if (idCard != null)
                            {
                                var sbc = _serviceBlockService.IsBlocked(idCard.StudentProgram.Program.Id, idCard.StudentProgram.Batch.Session.Id, (int)BlockService.IdCard, studentProgram);

                                if (!sbc)
                                {
                                    //if (null == idCard)
                                    //{
                                    //    var msg = new ResponseMessage(errorMessage: prn + " has no id card printed yet");
                                    //    messageList.Add(msg);
                                    //    continue;
                                    //}

                                    if (!branches.Contains(idCard.StudentProgram.Batch.Branch))
                                    {
                                        var msg = new ResponseMessage(errorMessage: "You are not authorized to distribute id card of " + prn);
                                        messageList.Add(msg);
                                        continue;
                                    }
                                    if (idCard.PrintCount < 1)
                                    {
                                        var msg = new ResponseMessage(errorMessage: "Id card is not printed for " + prn);
                                        messageList.Add(msg);
                                        continue;
                                    }

                                    if (idCard.IsDistributed)
                                    {
                                        var msg = new ResponseMessage(infoMessage: "Id card is already distributed for " + prn);
                                        messageList.Add(msg);
                                        continue;
                                    }

                                    idCard.IsDistributed = true;
                                    _StudentIdCardDao.Update(idCard);
                                    messageList.Add(new ResponseMessage(successMessage: "Id card is SUCCESSFULLY distributed for " + prn));
                                }
                                else
                                {
                                    messageList.Add(new ResponseMessage(errorMessage: "Distribution service is blocked for " + prn));
                                }
                            }
                            else
                            {
                                var msg = new ResponseMessage(errorMessage: prn + " has no id card printed yet");
                                messageList.Add(msg);
                                continue;
                            }   
                        }
                        else
                        {
                            messageList.Add(new ResponseMessage(errorMessage: prn + " is not valid for this program & session"));
                        }
                    }
                    catch (Exception ex)
                    {

                        _logger.Error(ex);
                        trans.Rollback();
                        throw ex;

                    }
                }
                trans.Commit();
            }

            return messageList;
        }

        public IList<StudentIdCard> loadAll()
        {
            try
            {
                return _StudentIdCardDao.LoadAll();
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;

            }
        }

        public IList<StudentProgram> getByPrnNo(string[] Rolls)
        {
            try
            {
                return _StudentProgramDao.LoadStudentProgram(Rolls);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;

            }
        }
        
        public List<StudentProgram> LoadAllStudentProgramList(long[] studentProgramIdList)
        {
            try
            {
                return _StudentProgramDao.LoadAllStudentProgramList(studentProgramIdList);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;

            }
        }

        public List<StudentProgram> LoadStudentProgramForIndividualIdCard(List<UserMenu> userMenu, List<string> stdProRollStringList)
        {
            ITransaction trans = null;
            List<StudentProgram> studentProgramList = new List<StudentProgram>();
            string errorMessage = "";
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<StudentProgram> validStudentProgramList = new List<StudentProgram>();

                if (!stdProRollStringList.Any())
                {
                    throw new InvalidDataException("Please enter valid Student Program Roll.");
                }

                if (!branchIdList.Any() || !programIdList.Any())
                {
                    throw new InvalidDataException("You have no access to this menu.");
                }

                foreach (string stPrRoll in stdProRollStringList)
                {
                    StudentProgram getStudentProgram = _StudentProgramDao.GetStudentProgram(stPrRoll);

                    if (getStudentProgram == null)
                    {
                        errorMessage += "Program roll number is not matching with any student for Program Roll: " + stPrRoll+". \r\n";
                    }
                    else
                    {
                        if (!branchIdList.Contains(getStudentProgram.Batch.Branch.Id) || !programIdList.Contains(getStudentProgram.Program.Id))
                            errorMessage += "You are not authorize person to search this student for Program Roll: " + stPrRoll + ". \n";
                        //    throw new InvalidDataException("You are not authorize person to search this student for Program Roll: " + stPrRoll);

                        #region Service Block Operation

                        IList<ServiceBlock> blockedContent =
                            _serviceBlockDao.LoadContentTypeList(getStudentProgram.Program.Id,
                                getStudentProgram.Batch.Session.Id, (int) BlockService.IdCard);

                        if (blockedContent != null && blockedContent.Count > 0)
                        {
                            foreach (var bc in blockedContent)
                            {
                                if (bc.ConditionType == (int) ConditionType.PaymentDue)
                                {
                                    if (getStudentProgram.DueAmount > 0)
                                    {
                                        errorMessage += "Please pay all your due, you have " + getStudentProgram.DueAmount + "Tk to pay! for Program Roll: " + stPrRoll + ". \n";
                                        //throw new MessageException("Please pay all your due, you have " +
                                        //                           getStudentProgram.DueAmount +
                                        //                           "Tk to pay! for Program Roll: " + stPrRoll);
                                    }
                                }
                                if (bc.ConditionType == (int) ConditionType.ImageDue)
                                {
                                    if (!getStudentProgram.IsImage)
                                    {
                                        errorMessage += "Image due blocked for this student for Program Roll: " + stPrRoll + ". \n";
                                        //throw new MessageException(
                                        //    "Image due blocked for this student for Program Roll: " +
                                        //    stPrRoll);
                                    }
                                }
                                if (bc.ConditionType == (int) ConditionType.PoliticalReference)
                                {
                                    if (getStudentProgram.IsPolitical)
                                    {
                                        errorMessage += "Political Reference blocked for this student for Program Roll: " + stPrRoll + ". \n";
                                        //throw new MessageException(
                                        //    "Political Reference blocked for this student for Program Roll: " + stPrRoll);
                                    }
                                }
                                if (bc.ConditionType == (int) ConditionType.Jsc)
                                {
                                    if (!getStudentProgram.Student.IsJsc)
                                    {
                                        errorMessage += "JSC service blocked for this student for Program Roll: " + stPrRoll + ". \n";
                                        //throw new MessageException("JSC service blocked for this student for Program Roll: " + stPrRoll);
                                    }
                                }
                                if (bc.ConditionType == (int) ConditionType.Ssc)
                                {
                                    if (!getStudentProgram.Student.IsSsc)
                                    {
                                        errorMessage += " service blocked for this student for Program Roll: " + stPrRoll + ". \n";
                                        //throw new MessageException("SSC service blocked for this student for Program Roll: " + stPrRoll);
                                    }
                                }
                                if (bc.ConditionType == (int) ConditionType.Hsc)
                                {
                                    if (!getStudentProgram.Student.IsHsc)
                                    {
                                        errorMessage += "HSC service blocked for this student for Program Roll: " + stPrRoll + ". \n";
                                        //throw new MessageException("HSC service blocked for this student for Program Roll: " + stPrRoll);
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                    validStudentProgramList.Add(getStudentProgram);
                }
                if (!String.IsNullOrEmpty(errorMessage))
                {
                    throw new MessageException(errorMessage);
                }

                using (trans = Session.BeginTransaction())
                {
                    foreach (var studentprogram in validStudentProgramList)
                    {
                        StudentIdCard studentIdCard = _StudentIdCardDao.getByStudentProgramId(studentprogram.Id);

                        if (studentIdCard == null)
                        {
                            StudentIdCard sic = new StudentIdCard
                            {
                                StudentProgram = studentprogram,
                                IsDistributed = false,
                                PrintCount = 1
                            };
                            _StudentIdCardDao.Save(sic);
                        }
                        else
                        {
                            studentIdCard.Status = studentIdCard.Status;
                            studentIdCard.StudentProgram = studentIdCard.StudentProgram;
                            studentIdCard.IsDistributed = studentIdCard.IsDistributed;
                            studentIdCard.PrintCount = studentIdCard.PrintCount + 1;
                            _StudentIdCardDao.Update(studentIdCard);
                        }
                        //newly added code
                        StudentMediaImage mediaStudentImages = new StudentMediaImage();
                        if (studentprogram.StudentImage.Count > 0)
                        {
                            mediaStudentImages =
                                _studentImageMediaDao.GetStudentImageMediaByRefId(studentprogram.StudentImage[0].Id);
                            studentprogram.MediaStudentImage = mediaStudentImages != null
                                ? mediaStudentImages
                                : new StudentMediaImage();
                        }
                        studentProgramList.Add(studentprogram);
                    }
                    trans.Commit();
                }

            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (MessageException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
            return studentProgramList;
        }

        //service block for list
        public List<StudentIdCardListDto> LoadStudentProgram(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId,
            string[] batchDays, string[] batchTime, long[] batchName, long[] course, int printingStatus, int start, int length,
            string orderBy, string orderDir, int? distributionStatus = null, bool generateIdCard = false, List<ServiceBlock> serviceBlocks = null)
        {
            try
            {
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                return _StudentProgramDao.LoadStudentProgram(programId, sessionId, branchId, programIdList, branchIdList, campusId, batchDays, batchTime, batchName, course, printingStatus, start, length, orderBy,orderDir, distributionStatus,generateIdCard,serviceBlocks);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw ex;

            }
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

        #region Clean
        //public IList<StudentProgram> LoadStudentProgramByCriteria(List<UserMenu> usermenu, long programId, long sessionId, long[] branchId,
        //      long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, long[] courseIds, int printingStatus, int start, int length, int? distributionStatus = null)
        //{
        //    List<long> branchIdList = AuthHelper.LoadBranchIdList(usermenu, null, _commonHelper.ConvertIdToList(programId));
        //    List<long> programIdList = AuthHelper.LoadProgramIdList(usermenu);

        //    IList<StudentProgram> studentProgramList = _StudentProgramDao.LoadStudentProgram(programId, sessionId, branchId, programIdList, branchIdList, campusId, batchDays, batchTime, batchName, courseIds, printingStatus, start, length, distributionStatus);
        //    return studentProgramList;
        //}
        #endregion
    }
}
