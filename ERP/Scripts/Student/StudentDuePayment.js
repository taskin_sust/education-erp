﻿

function submitFunction() {

    var stdProgramId = $('#stdProgramId').val();
    var InvalidMessage = "Invalid Program Roll Number";
    var neumeric = $.isNumeric(stdProgramId);
    console.log(neumeric);
    if (neumeric != false) {
        if (stdProgramId.length == 11) {
            $('#msgforpr').text("");
            $.ajax({
                url: "/Student/Payment/DuePayment",
                type: "GET",
                data: { stdProgramId: stdProgramId, withLayout: 'false' },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    if (data.Message == InvalidMessage) {
                        $.fn.customMessage({
                            displayMessage: InvalidMessage,
                            displayMessageType: "e"
                        });
                        //var link = "/Student/Payment/StudentDuePayment?message=" + data.Message + "&stdProgramId=" + data.AdditionalValue + "";
                        //window.location.href = link;
                    } else {
                        $('#mainBody').html(data);
                    }
                    $.unblockUI();

                },
                error: function (data) {
                    bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
                }
            });
        } else {
            $.fn.customMessage({
                displayMessage: InvalidMessage,
                displayMessageType: "e"
            });
            //$('#msgforpr').text("Roll Number must be a number");
        }
    } else {
        $.fn.customMessage({
            displayMessage: "Roll Number must be a number",
            displayMessageType: "e"
        });
    }
    
}
$(document).ready(function () {
    $(document).on('blur', '#stdProgramId', function () {
        var stdProgramId = $('#stdProgramId').val();
        var neumeric = $.isNumeric(stdProgramId);
        if (neumeric==true) {
            $('#msgforpr').text("");
        } else {
            $('#msgforpr').text("Roll Number must be a number");
        }
    });

    $("#duePaymentSearchFormBypId").submit(function () {
        return false;
    });


    $("body").keypress(function (event) {

        var keyPressed = event.which;
        if (keyPressed == 13) {

            if ($("body").find('#duePaymentSearchFormBypId').length > 0) {
                submitFunction();
            }
            
        }
    });
    $(document).on('click', '#searchByprNo', function () {
        submitFunction();
    });
});
