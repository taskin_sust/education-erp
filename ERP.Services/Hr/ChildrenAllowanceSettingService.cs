using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;

namespace UdvashERP.Services.Hr
{
    public interface IChildrenAllowanceSettingService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(List<UserMenu> authorizeMenu, ChildrenAllowanceSetting prChildrenAllowance);
        void Delete(ChildrenAllowanceSetting entity);

        #endregion

        #region Single Instances Loading Function

        ChildrenAllowanceSetting GetChildrenAllowanceSetting(long id);

        #endregion

        #region List Loading Function

        IList<ChildrenAllowanceSetting> LoadChilderenAllowanceSetting(int draw, int start, int length, List<UserMenu> authorizeMenu, long? organizationId = null, int? empStatus = 0, int? payType = 0);
        IList<ChildrenAllowanceSetting> LoadChilderenAllowanceSetting(long orgId);
        IList<ChildrenAllowanceSetting> LoadChilderenAllowanceSetting(long orgId, int employmentStatus);
        IList<ChildrenAllowanceSetting> GetTeamMemberChildrenEducationAllowance(long memberId, DateTime dateTime);

        #endregion

        #region Others Function
        int GetChildrenAllowanceSettingAllowanceCount(List<UserMenu> authorizeMenu, long? organizationId, int? empStatus, int? payType);

        #endregion

        #region Helper Function

        #endregion

    }

    public class ChildrenAllowanceSettingService : BaseService, IChildrenAllowanceSettingService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("PayrollService");

        #endregion

        #region Properties & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IChildrenAllowanceSettingDao _childrenAllowanceSettingDao;
        private readonly IEmploymentHistoryDao _employmentHistoryDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;
        private readonly IChildrenAllowanceBlockDao _childrenAllowanceBlockDao;
        private readonly IAllowanceSheetSecondDetailsDao _allowanceSheetSecondDetailsDao;

        public ChildrenAllowanceSettingService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _childrenAllowanceSettingDao = new ChildrenAllowanceSettingDao { Session = session };
            _employmentHistoryDao = new EmploymentHistoryDao() { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao() { Session = session };
            _childrenAllowanceBlockDao = new ChildrenAllowanceBlockDao() { Session = session };
            _allowanceSheetSecondDetailsDao = new AllowanceSheetSecondDetailsDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public void SaveOrUpdate(List<UserMenu> authorizeMenu, ChildrenAllowanceSetting prChildrenAllowance)
        {
            ITransaction transaction = null;
            ChildrenAllowanceSetting result = null;
            try
            {
                CheckBeforeSave(authorizeMenu, prChildrenAllowance);
                CheckValidation(prChildrenAllowance);
                if (prChildrenAllowance.Id == 0)
                {
                    EligibleForSave(prChildrenAllowance.Organization.Id, prChildrenAllowance.EffectiveDate);
                    _childrenAllowanceSettingDao.Save(prChildrenAllowance);
                    return;
                }
                result = EligibleForUpdate(prChildrenAllowance);
                using (transaction = Session.BeginTransaction())
                {
                    //ChildrenAllowanceSetting proxyObj = BuildProxyObj(prChildrenAllowance);
                    _childrenAllowanceSettingDao.Update(result);
                    transaction.Commit();
                }
            }
            catch (AuthenticationException ae)
            {
                throw ae;
            }
            catch (DuplicateEntryException dex)
            {
                throw dex;
            }
            catch (InvalidDataException ide)
            {
                throw ide;
            }
            catch (MessageException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void Delete(ChildrenAllowanceSetting entity)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeDelete(entity.Id);
                IList<AllowanceSheetSecondDetails> list = _allowanceSheetSecondDetailsDao.LoadByChildrenAllowanceId(entity.Id);
                if (list.Count > 0) throw new InvalidDataException("Can't delete this allowance setting because Allowance Sheet has already been prepared");
                using (transaction = Session.BeginTransaction())
                {
                    entity.Status = ChildrenAllowanceSetting.EntityStatus.Delete;
                    _childrenAllowanceSettingDao.Update(entity);
                    transaction.Commit();
                }
            }
            catch (AuthenticationException ae)
            {
                throw ae;
            }
            catch (DuplicateEntryException dex)
            {
                throw dex;
            }
            catch (InvalidDataException ide)
            {
                throw ide;
            }
            catch (MessageException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public ChildrenAllowanceSetting GetChildrenAllowanceSetting(long id)
        {
            try
            {
                if (id <= 0) throw new InvalidDataException("invalid id");
                return _childrenAllowanceSettingDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region List Loading Function

        public IList<ChildrenAllowanceSetting> LoadChilderenAllowanceSetting(int draw, int start, int length, List<UserMenu> authorizeMenu, long? organizationId = null, int? empStatus = 0, int? payType = 0)
        {
            try
            {
                if (authorizeMenu == null) throw new InvalidDataException("invalid user permission");
                long? convertedOrganizationId = organizationId == 0 || organizationId == null ? (long?)null : Convert.ToInt32(organizationId);
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authorizeMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                if (authOrgList.Count <= 0) throw new UnauthorizeDataException("Unauthorized data exception");
                return _childrenAllowanceSettingDao.LoadChildrenAllowanceSetting(draw, start, length, authOrgList, empStatus, payType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<ChildrenAllowanceSetting> GetTeamMemberChildrenEducationAllowance(long memberId, DateTime dateTime)
        {
            try
            {
                object responsiblePersonDes, responsiblePersonDep, responsiblePersonOrg;
                int empStatus;
                if (memberId <= 0) throw new InvalidDataException("invalid member id");
                if (dateTime <= DateTime.MinValue || dateTime > DateTime.MaxValue) throw new InvalidDataException("Invalid date-time");
                _employmentHistoryDao.GetCurrentInformations(memberId, out empStatus, out responsiblePersonDes, out responsiblePersonDep, out responsiblePersonOrg, dateTime);
                if (responsiblePersonDes == null) throw new InvalidDataException("Invalid Designation");
                if (responsiblePersonDep == null) throw new InvalidDataException("Invalid Department");
                if (responsiblePersonOrg == null) throw new InvalidDataException("Invalid Organization");
                if (empStatus == 0) throw new InvalidDataException("Invalid Emp Status");
                var org = (Organization)responsiblePersonOrg;
                return _childrenAllowanceSettingDao.GetTeamMemberChildrenEducationAllowance(org.Id, (int)empStatus, dateTime);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<ChildrenAllowanceSetting> LoadChilderenAllowanceSetting(long orgId)
        {
            try
            {
                if (orgId <= 0) throw new InvalidDataException("Invalid Organization Id");
                return _childrenAllowanceSettingDao.LoadByOrganization(orgId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<ChildrenAllowanceSetting> LoadChilderenAllowanceSetting(long orgId, int employmentStatus)
        {
            try
            {
                if (orgId <= 0) throw new InvalidDataException("Invalid Organization Id");
                if (employmentStatus <= 0) throw new InvalidDataException("Invalid employment status");
                return _childrenAllowanceSettingDao.LoadByOrgAndEmpStatus(orgId, employmentStatus);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Others Function

        public int GetChildrenAllowanceSettingAllowanceCount(List<UserMenu> authorizeMenu, long? organizationId, int? empStatus, int? payType)
        {
            try
            {
                if (authorizeMenu == null) throw new AuthenticationException("invalid user permission");
                long? convertedOrganizationId = organizationId == 0 || organizationId == null ? (long?)null : Convert.ToInt32(organizationId);
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authorizeMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                return _childrenAllowanceSettingDao.CountAllowance(authOrgList, empStatus, payType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        private void CheckBeforeDelete(long id)
        {
            if (_childrenAllowanceSettingDao.IsChildrenAllowanceSettingStillValid(id)) throw new MessageException("You Can't delete this allowance because Allowance sheet has already been generated");
        }

        private void CheckValidation(ChildrenAllowanceSetting prChildrenAllowance)
        {
            var validationResult = ValidationHelper.ValidateEntity<ChildrenAllowanceSetting, ChildrenAllowanceSetting>(prChildrenAllowance);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private void EligibleForSave(long orgId, DateTime effectiveDateTime)
        {
            var allObj = _allowanceSheetDao.GetEligibleAllowanceSheetByOrganizationId(orgId);
            if (allObj != null)
                if (allObj.DateTo >= effectiveDateTime)
                    throw new MessageException("You can't save this allowance from this effective date because allowance sheet has already been generated for this month!! try another effective date after " + allObj.DateTo.ToString("yyyy MMMM dd"));
        }


        private IList ShouldContinue(ChildrenAllowanceSetting updateObj)
        {
            Session.Clear();
            var prevObj = _childrenAllowanceSettingDao.LoadById(updateObj.Id);
            IList diffProperties = new ArrayList();
            foreach (var item in updateObj.GetType().GetProperties())
            {
                if (!Object.Equals(item.GetValue(updateObj, null),
                         prevObj.GetType().GetProperty(item.Name).GetValue(prevObj, null)))
                {
                    diffProperties.Add(item.Name);
                }
            }
            return diffProperties;
        }

        private ChildrenAllowanceSetting EligibleForUpdate(ChildrenAllowanceSetting prChildrenAllowance)
        {
            Session.Clear();
            DateTime eligibleDatetime = _childrenAllowanceSettingDao.GetEligibleClosingDateForPayrollSetting(prChildrenAllowance.Id);
            if (eligibleDatetime != DateTime.MinValue)
            {
                var diffProperties = ShouldContinue(prChildrenAllowance);
                if (diffProperties.Contains("MaxNoOfChild") || diffProperties.Contains("Amount") || diffProperties.Contains("StartingAge") || diffProperties.Contains("ClosingAge") ||
                    diffProperties.Contains("PaymentType") || diffProperties.Contains("EffectiveDate") || diffProperties.Contains("IsPermanent") ||
                    diffProperties.Contains("IsProbation") || diffProperties.Contains("IsPartTime") || diffProperties.Contains("IsContractual") || diffProperties.Contains("IsIntern"))
                    throw new InvalidDataException("you can't change anything except closing date");
                if (eligibleDatetime > prChildrenAllowance.ClosingDate) throw new InvalidDataException("closing date must be greater than " + eligibleDatetime.ToString("yyyy MMMM dd"));
            }
            var proxyObj = _childrenAllowanceSettingDao.LoadById(prChildrenAllowance.Id);
            IList<ChildrenAllowanceSetting> list = _childrenAllowanceSettingDao.LoadByOrgAndEmpStatus(prChildrenAllowance.Organization.Id, prChildrenAllowance.MemberEmploymentStatuses,
                prChildrenAllowance.PaymentType, prChildrenAllowance.EffectiveDate, null, prChildrenAllowance.Id);
            ChildrenAllowanceSetting prevObj = list.Where(x => x.ClosingDate < proxyObj.EffectiveDate).OrderByDescending(x => x.EffectiveDate).Take(1).SingleOrDefault<ChildrenAllowanceSetting>();
            ChildrenAllowanceSetting nextObj = list.Where(x => x.EffectiveDate > proxyObj.ClosingDate).OrderBy(x => x.EffectiveDate).Take(1).SingleOrDefault<ChildrenAllowanceSetting>();
            if (prevObj != null)
            {
                if (prevObj.ClosingDate == null)
                    throw new InvalidDataException("You can't update this allowance because an allowance has already been declared from " + prevObj.EffectiveDate.ToString("yyyy MMMM dd"));
                if (prevObj.ClosingDate >= prChildrenAllowance.EffectiveDate || prevObj.EffectiveDate > prChildrenAllowance.ClosingDate)
                    throw new InvalidDataException("You can't update this allowance because an allowance has already been declared from " + prevObj.EffectiveDate.ToString("yyyy MMMM dd") + " to " + prevObj.ClosingDate.Value.ToString("yyyy MMMM dd") + " Try another date which will be greater than " + prevObj.ClosingDate.Value.ToString("yyyy MMMM dd"));
            }
            if (nextObj != null)
            {
                if (nextObj.EffectiveDate <= prChildrenAllowance.ClosingDate || nextObj.EffectiveDate <= prChildrenAllowance.EffectiveDate)
                    throw new InvalidDataException("You can't update this allowance because an allowance has already been declared from " + nextObj.EffectiveDate.ToString("yyyy MMMM dd") + " to " + nextObj.ClosingDate.Value.ToString("yyyy MMMM dd") + " Try another date which will be greater than " + nextObj.ClosingDate.Value.ToString("yyyy MMMM dd"));
            }
            proxyObj.MemberEmploymentStatuses = prChildrenAllowance.MemberEmploymentStatuses;
            proxyObj.IsContractual = false;
            proxyObj.IsIntern = false;
            proxyObj.IsPartTime = false;
            proxyObj.IsPermanent = false;
            proxyObj.IsProbation = false;
            proxyObj.EffectiveDate = prChildrenAllowance.EffectiveDate;
            proxyObj.ClosingDate = prChildrenAllowance.ClosingDate;
            proxyObj.PaymentType = prChildrenAllowance.PaymentType;
            proxyObj.MaxNoOfChild = prChildrenAllowance.MaxNoOfChild;
            proxyObj.Amount = prChildrenAllowance.Amount;
            proxyObj.StartingAge = prChildrenAllowance.StartingAge;
            proxyObj.ClosingAge = prChildrenAllowance.ClosingAge;
            proxyObj.Name = prChildrenAllowance.Name;
            proxyObj = FillUpData(proxyObj);
            return proxyObj;
        }

        private ChildrenAllowanceSetting FillUpData(ChildrenAllowanceSetting prChildrenAllowance)
        {
            foreach (var empStatus in prChildrenAllowance.MemberEmploymentStatuses)
            {
                switch (empStatus)
                {
                    case MemberEmploymentStatus.Contractual:
                        prChildrenAllowance.IsContractual = true;
                        break;
                    case MemberEmploymentStatus.PartTime:
                        prChildrenAllowance.IsPartTime = true;
                        break;
                    case MemberEmploymentStatus.Probation:
                        prChildrenAllowance.IsProbation = true;
                        break;
                    case MemberEmploymentStatus.Permanent:
                        prChildrenAllowance.IsPermanent = true;
                        break;
                    case MemberEmploymentStatus.Intern:
                        prChildrenAllowance.IsIntern = true;
                        break;
                }
            }
            return prChildrenAllowance;
        }

        private void CheckDuplicateEntryAllowance(ChildrenAllowanceSetting prChildrenAllowance)
        {
            IList<ChildrenAllowanceSetting> list = _childrenAllowanceSettingDao.LoadByOrgAndEmpStatus(prChildrenAllowance.Organization.Id, prChildrenAllowance.MemberEmploymentStatuses,
                prChildrenAllowance.PaymentType, prChildrenAllowance.EffectiveDate, prChildrenAllowance.ClosingDate, prChildrenAllowance.Id);
            if (list != null && list.Any())
            {
                ChildrenAllowanceSetting supportAllowanceSetting = list.OrderByDescending(x => x.EffectiveDate).Take(1).SingleOrDefault();
                if (supportAllowanceSetting.ClosingDate == null) throw new InvalidDataException("You can't add/update this allowance because an allowance has already been declared from " + supportAllowanceSetting.EffectiveDate.ToString("yyyy MMMM dd"));
                if (supportAllowanceSetting.ClosingDate >= prChildrenAllowance.EffectiveDate) throw new InvalidDataException("You can't add/update this allowance because an allowance has already been declared from " + supportAllowanceSetting.EffectiveDate.ToString("yyyy MMMM dd") + " to " + supportAllowanceSetting.ClosingDate.Value.ToString("yyyy MMMM dd") + " Try another date which will be greater than " + supportAllowanceSetting.ClosingDate.Value.ToString("yyyy MMMM dd"));
            }
        }

        private ChildrenAllowanceSetting BuildProxyObj(ChildrenAllowanceSetting prChildrenAllowance)
        {
            var proxyObj = _childrenAllowanceSettingDao.LoadById(prChildrenAllowance.Id);
            proxyObj.Name = prChildrenAllowance.Name;
            proxyObj.Amount = prChildrenAllowance.Amount;
            proxyObj.MaxNoOfChild = prChildrenAllowance.MaxNoOfChild;
            proxyObj.Organization = prChildrenAllowance.Organization;
            proxyObj.PaymentType = prChildrenAllowance.PaymentType;
            proxyObj.StartingAge = prChildrenAllowance.StartingAge;
            proxyObj.ClosingAge = prChildrenAllowance.ClosingAge;
            proxyObj.EffectiveDate = prChildrenAllowance.EffectiveDate;
            proxyObj.ClosingDate = prChildrenAllowance.ClosingDate;
            proxyObj.IsContractual = prChildrenAllowance.IsContractual;
            proxyObj.IsPartTime = prChildrenAllowance.IsPartTime;
            proxyObj.IsProbation = prChildrenAllowance.IsProbation;
            proxyObj.IsPermanent = prChildrenAllowance.IsPermanent;
            proxyObj.IsIntern = prChildrenAllowance.IsIntern;
            return proxyObj;
        }

        private void CheckBeforeSave(List<UserMenu> authorizeMenu, ChildrenAllowanceSetting prChildrenAllowance)
        {
            if (prChildrenAllowance == null) throw new NullObjectException("Empty object found");
            if (prChildrenAllowance.Organization == null) throw new InvalidDataException("Invalid Org Exception");
            if (authorizeMenu == null) throw new AuthenticationException("unauthorized org");
            if (prChildrenAllowance.IsContractual == false && prChildrenAllowance.IsPartTime == false && prChildrenAllowance.IsPermanent == false && prChildrenAllowance.IsProbation == false && prChildrenAllowance.IsIntern == false)
                throw new InvalidDataException("No employment status is selected");
            if (prChildrenAllowance.ClosingDate.ToString().Length > 0)
                if (prChildrenAllowance.EffectiveDate > prChildrenAllowance.ClosingDate)
                    throw new InvalidDataException("Closing date must be bigger than effective date");
            if (prChildrenAllowance.StartingAge > prChildrenAllowance.ClosingAge) throw new InvalidDataException("Starting age can't be greater than closing age ");
            CheckDuplicateEntryAllowance(prChildrenAllowance);
        }

        #endregion
    }
}

