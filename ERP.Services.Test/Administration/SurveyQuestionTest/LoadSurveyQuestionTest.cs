﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Test.AdminisTration.SurveyQuestionTest;
using Xunit;

namespace UdvashERP.Services.Test.Administration.SurveyQuestionTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "SurveyQuestion")]
    public class LoadSurveyQuestionTest : SurveyQuestionBaseTest
    {
        #region Basic test

        [Fact]
        public void Shud_Return_Successfully()  
        {
            _surveyQuestionFactory.Create().WithQuestionAndRank(_question, 10);
            _surveyQuestionAnswerFactory.CreateMore(2, _surveyQuestionFactory.Object);
            _surveyQuestionFactory.WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList()).Persist();

            var objList = _surveyQuestionService.LoadSurveyQuestion();
            Assert.NotEmpty(objList);
        }

        #endregion
    }
}
