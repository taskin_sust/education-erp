﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Services;
using log4net;
using log4net.Config;
using DeviceSynchronizer.ProcessCommunication;

namespace DeviceSynchronizer
{
    public partial class MainForm : Form
    {
        #region Declaration

        private static readonly ILog Log = LogManager.GetLogger("SynchronizerLogger");
        private SynchronizerThread _deviceSynchronizerThread;
        private ServiceDelegate _serviceDelegate;
        private MessageCleared _messageCleared;
        private bool _manualExit = false;
        private bool _fromResizeByCode = true;
        private IpcServer _ipcServer = null;

        #endregion

        #region Events and delegate

        private delegate void UpdateListViewInvoker(string message, MessageType messageType);
        private delegate void ClearListViewInvoker();
        public delegate void MessageCleared();

        private void OnMessageCleared()
        {
            try
            {
                if (currentStatedataGridView.InvokeRequired)
                {
                    var me = new ClearListViewInvoker(OnMessageCleared);
                    currentStatedataGridView.BeginInvoke(me);
                }
                else
                {
                    currentStatedataGridView.Rows.Clear();
                    currentStatedataGridView.Refresh();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void OnGuiMessaging(string message, MessageType messageType)
        {
            try
            {
                if (currentStatedataGridView.InvokeRequired)
                {
                    var mi = new UpdateListViewInvoker(OnGuiMessaging);
                    currentStatedataGridView.BeginInvoke(mi, message, messageType);
                }
                else
                {
                    //set color
                    var rowColor = Color.WhiteSmoke;
                    if (messageType == MessageType.Attention)
                        rowColor = Color.LightGoldenrodYellow;
                    else if (messageType == MessageType.Success)
                        rowColor = Color.LightGreen;
                    else if (messageType == MessageType.Error)
                        rowColor = Color.LightCoral;
                    else if (messageType == MessageType.Important)
                        rowColor = Color.LightSkyBlue;

                    //add row
                    string[] rowData = new string[] { DateTime.Now.ToString(BusinessRules.DateTimeFormat), message };
                    currentStatedataGridView.Rows.Insert(0, rowData);
                    currentStatedataGridView.Rows[0].DefaultCellStyle.BackColor = rowColor;

                    //clear
                    if (currentStatedataGridView.Rows.Count > 20000)
                        _messageCleared();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void SetTitle(string syncName = null)
        {
            if (this.InvokeRequired)
            {
                var mi = new ServiceDelegate.SetAppTitleDelegate(SetTitle);
                this.BeginInvoke(mi, syncName);
            }
            else
            {
                var title = BusinessRules.AppName + " v" + BusinessRules.AppVersion;
                if (BusinessRules.DevMode)
                    title += " (DEV)";
                if (!String.IsNullOrEmpty(syncName))
                    title += " [" + syncName + "]";

                this.Text = title;
            }
        }

        #endregion

        #region Start and Stop Button

        [STAThread]
        private void startBtn_Click(object sender, EventArgs e)
        {
            try
            {
                startBtn.Enabled = false;
                stopBtn.Enabled = true;

                if (_deviceSynchronizerThread == null)
                {
                    //for first time
                    DbHelper.CreateDb();
                    _deviceSynchronizerThread = new SynchronizerThread(OnGuiMessaging, SetTitle);
                    _deviceSynchronizerThread.Start();
                }
                else if (_deviceSynchronizerThread._serviceStatus.IsRunning)
                {
                    //Stop service first
                    OnGuiMessaging("Program already running Please stop it first..", MessageType.Info);
                }
                else
                {
                    //just restart 
                    _messageCleared();
                    _deviceSynchronizerThread.Restart();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {
            try
            {
                startBtn.Enabled = true;
                stopBtn.Enabled = false;

                if (_deviceSynchronizerThread != null)
                    _deviceSynchronizerThread.Stop();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            string msg = "Are you sure you want to close \"" + BusinessRules.AppName + "\" software?";
            if (MessageBox.Show(msg, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                stopBtn.PerformClick();
                _manualExit = true;
                this.Close();
            }
        }
        #endregion

        #region Constructor & Form events

        public MainForm()
        {
            try
            {
                InitializeComponent();
                SetTitle();
                _messageCleared = OnMessageCleared;
                XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "log4net.config"));

                //notification icon
                var title = BusinessRules.AppName + " v" + BusinessRules.AppVersion;
                notifyIcon.BalloonTipTitle = title;
                notifyIcon.BalloonTipText = BusinessRules.AppName + " is minimized here.";
                notifyIcon.Text = title;
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (BusinessRules.DevMode)
            {
                this.ShowInTaskbar = true;
            }
            else
            {
                this.startBtn.PerformClick();
                this.WindowState = FormWindowState.Minimized;
            }


            try
            {
                //start IpcService
                _ipcServer = new IpcServer(this);
                _ipcServer.IpcServerStart();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon.Visible = true;
                notifyIcon.ShowBalloonTip(500);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon.Visible = false;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (_manualExit)
                {
                    //just stop IpcService
                    if (_ipcServer != null)
                        _ipcServer.IpcServerStop();
                }
                else
                {
                    e.Cancel = true;
                    this.WindowState = FormWindowState.Minimized;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //first time resize to half-window-size
            if (_fromResizeByCode)
            {
                this.Top = this.Left = 0;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width / 2;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height;
                this.ShowInTaskbar = true;
                this._fromResizeByCode = false;
            }

            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        #endregion

        #region Other Functions
        public bool IsServiceRunning()
        {
            if (_deviceSynchronizerThread == null)
                return false;
            else
                return _deviceSynchronizerThread._serviceStatus.IsRunning;
        }

        public void StopService(bool exitApplication = false)
        {
            stopBtn_Click(null, null);
            //stopBtn.PerformClick();
            if (exitApplication)
            {
                _manualExit = true;
                Close();
            }
        }

        #endregion
    }
}
