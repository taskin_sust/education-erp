﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Administration
{
    public interface IInstitutionService : IBaseService
    {
        #region Operational Function
        int Save(Institute institute);
        bool UpdateRank(Institute instituteOldObj, Institute instituteUpdateObj);
        bool Update(Institute institute, long categoryId);
        void Delete(Institute institute);
        #endregion

        #region Single Instances Loading Function
        Institute LoadById(long id);
        Institute LoadByRankNextOrPrevious(int rank, string action);
        Institute GetInstitute(string eiin);
        Institute GetInstituteByName(string institute);
        #endregion

        #region List Loading Function
        IList<Institute> GetInstituteList(int start, int length, string orderBy, string p, string name, string category, string district, string eiin, string status, string rank);
        IList<Institute> LoadInstituteByName(long id, string name, string shortName, long categoryId, string district, string eiin);
        IList<Institute> GetInstituteByNameLike(string name);
        #endregion

        #region Others Function
        int GetMaximumRank(Institute i);
        int GetMinRank(Institute i);
        int InstituteRowCount(Institute institute);
        int InstituteRowCount(string Name, string Category, string District, string Eiin, string Status, string Rank);
        #endregion


    }
    public class InstituteService : BaseService, IInstitutionService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly InstituteDao _instituteDao;
        private readonly InstituteCategoryDao _instituteCategoryDao;
        public InstituteService(ISession session)
        {
            Session = session;
            _instituteDao = new InstituteDao();
            {
                Session = Session;
                _instituteDao = new InstituteDao() { Session = Session };
                _instituteCategoryDao = new InstituteCategoryDao();
                {
                    Session = Session;
                    _instituteCategoryDao = new InstituteCategoryDao() { Session = Session };
                }
            }
        }
        #endregion

        #region Operational Function
        public int Save(Institute institute)
        {
            try
            {
                if (institute == null)
                {
                    throw new NullObjectException("institute can not be null.");
                }
                //check before save
                DoBeforeSave(institute);
                int customMessage = Constants.MessageSuccess;
                int maxRank = _instituteDao.GetMaximumRank(institute);
                institute.Rank = maxRank + 1;
                if (customMessage == Constants.MessageSuccess)
                {
                    _instituteDao.Save(institute);
                }
                return customMessage;
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool Update(Institute institute, long categoryId)
        {
            ITransaction trans = null;
            try
            {

                using (trans = Session.BeginTransaction())
                {
                    DoBeforeUpdate(institute, categoryId);
                    Institute updateInstitute = _instituteDao.LoadById(institute.Id);
                    updateInstitute.Name = institute.Name;
                    updateInstitute.Eiin = institute.Eiin;
                    updateInstitute.Status = institute.Status;
                    //updateInstitute.Thana = institute.Thana;
                    updateInstitute.District = institute.District;
                    updateInstitute.Website = institute.Website;
                    updateInstitute.Website = institute.Website;
                    updateInstitute.ShortName = institute.ShortName;
                    InstituteCategory instituteCategory =
                        _instituteCategoryDao.LoadInstituteCategoryById(categoryId);
                    updateInstitute.InstituteCategory = instituteCategory;
                    //updateInstitute.ModifyBy = 0;
                    _instituteDao.Update(updateInstitute);
                    trans.Commit();
                    return true;


                }

            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
        }

        public void Delete(Institute institute)
        {
            try
            {
                if (institute.StudentUniversityInfos.Count > 0)
                {
                    throw new DependencyException("This institute has one or more referrences in Student University Infos.");
                }
                else
                {
                    using (var trans = Session.BeginTransaction())
                    {
                        try
                        {
                            institute.Status = Institute.EntityStatus.Delete;
                            //institute.ModifyBy = 0;
                            _instituteDao.Update(institute);
                            trans.Commit();
                        }
                        catch (Exception e)
                        {
                            trans.Rollback();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public bool UpdateRank(Institute instituteOldObj, Institute instituteUpdateObj)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        _instituteDao.Update(instituteOldObj);
                        _instituteDao.Update(instituteUpdateObj);
                        trans.Commit();
                        return true;
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }

        }
        #endregion

        #region Single Instances Loading Function
        public Institute LoadById(long id)
        {
            try
            {
                return _instituteDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public Institute LoadByRankNextOrPrevious(int rank, string action)
        {
            try
            {
                return _instituteDao.LoadByRankDirection(rank, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public Institute GetInstitute(string eiin)
        {
            try
            {
                return _instituteDao.GetInstitute(eiin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public Institute GetInstituteByName(string institute)
        {
            try
            {
                return _instituteDao.GetInstituteByName(institute);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region List Loading Function
        public IList<Institute> GetInstituteList(int start, int length, string orderBy, string direction, string name, string category
            , string district, string eiin, string status, string rank)
        {
            try
            {
                return _instituteDao.GetInstituteList(start, length, orderBy, direction, name, category, district, eiin, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Institute> LoadInstituteByName(long id, string name, string shortName, long categoryId, string district, string eiin)
        {
            try
            {
                return _instituteDao.LoadInstituteByName(id, name, shortName, categoryId, district, eiin);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public IList<Institute> GetInstituteByNameLike(string name)
        {
            try
            {
                return _instituteDao.GetInstituteByNameLike(name);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        private void DoBeforeSave(Institute institute)
        {
            if (string.IsNullOrEmpty(institute.Name))
            {
                throw new EmptyFieldException("Institute name can not be empty.");
            }
            if (institute.InstituteCategory == null)
            {
                throw new EmptyFieldException("Institute category should be selected.");
            }
            IList<Institute> instituteByName = _instituteDao.LoadInstituteByName(institute.Id,
                institute.Name, institute.ShortName, institute.InstituteCategory.Id, institute.District, institute.Eiin);
            if (instituteByName.Count > 0)
            {
                throw new DuplicateEntryException("Institute name can not be duplicate.");
            }
        }


        private void DoBeforeUpdate(Institute institute, long categoryId)
        {

            if (string.IsNullOrEmpty(institute.Name))
            {
                throw new EmptyFieldException("Institute name can not be empty.");
            }
            if (categoryId < 1)
            {
                throw new EmptyFieldException("Institute category should be selected.");
            }
            IList<Institute> instituteByName = _instituteDao.LoadInstituteByName(institute.Id,
                institute.Name, institute.ShortName, categoryId, institute.District, institute.Eiin);
            if (instituteByName.Count > 0)
            {
                throw new DuplicateEntryException("Institute name can not be duplicate.");
            }
        }
        public int GetMaximumRank(Institute i)
        {
            try
            {
                return _instituteDao.GetMaximumRank(i);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public int GetMinRank(Institute i)
        {
            try
            {
                return _instituteDao.GetMinimumRank(i);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public int InstituteRowCount(Institute institute)
        {
            try
            {
                return _instituteDao.RowCount(institute);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public int InstituteRowCount(string Name, string Category, string District, string Eiin, string Status, string Rank)
        {
            try
            {
                return _instituteDao.InstituteRowCount(Name, Category, District, Eiin, Status, Rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }



        #endregion
    }
}


