﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ShiftTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Shift")]
    public class ShiftUpdateTest : ShiftBaseTest
    {

        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _shiftService.Update(null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var dFactory1 = _shiftFactory.Create()
                    .WithOrganization()
                    .WithStartTimeEndTime(_startTime,_endTime)
                    .WithNameAndRank(_name, 1)
                    .Persist();
            var shift = dFactory1.Object;
            shift.Organization = _organizationService.LoadById(1);
            shift.Name = shift.Name + "  DEF  GHI";
            shift.Status = -1;
            var success = _shiftService.Update(shift);
            Assert.True(success);
        }

        #endregion

        #region Business Logic Test
        
        [Fact]
        public void Should_Not_Update_Empty_Organization()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithOrganization()
                                        .WithNameAndRank(_name, 1)
                                        .WithStartTimeEndTime(_startTime,_endTime)
                                        .Persist();
            shiftFactory.Object.Organization = null;
            Assert.Throws<EmptyFieldException>(() => _shiftService.Update(shiftFactory.Object));
        }

        [Fact]
        public void Should_Not_Update_Empty_Name()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithOrganization()
                                        .WithNameAndRank(_name, 1)
                                        .WithStartTimeEndTime(_startTime, _endTime)
                                        .Persist();
            shiftFactory.Object.Name = "";
            Assert.Throws<EmptyFieldException>(() => _shiftService.Update(shiftFactory.Object));
        }
        
        [Fact]
        public void Should_Not_Update_StartTime_Null()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithOrganization()
                                        .WithNameAndRank(_name, 1)
                                        .WithStartTimeEndTime(_startTime, _endTime)
                                        .Persist();
            shiftFactory.Object.StartTime = null;
            Assert.Throws<EmptyFieldException>(() => _shiftService.Update(shiftFactory.Object));
        }
        
        [Fact]
        public void Should_Not_Update_EndTime_Null()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithOrganization()
                                        .WithNameAndRank(_name, 1)
                                        .WithStartTimeEndTime(_startTime, _endTime)
                                        .Persist();
            shiftFactory.Object.EndTime = null;
            Assert.Throws<EmptyFieldException>(() => _shiftService.Update(shiftFactory.Object));
        }
        [Fact]
        public void Should_Not_Update_StartTime_EndTime_Diff_Invalid()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithOrganization()
                                        .WithNameAndRank(_name, 1)
                                        .WithStartTimeEndTime(_startTime, _endTime)
                                        .Persist();
            shiftFactory.Object.StartTime = Convert.ToDateTime("2001-01-01 09:00:00");
            shiftFactory.Object.EndTime = Convert.ToDateTime("2001-01-01 09:00:00");
            Assert.Throws<InvalidDataException>(() => _shiftService.Update(shiftFactory.Object));
        }
        [Fact]
        public void Should_Not_Update_StartTime_EndTime_Order_Invalid()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithOrganization()
                                        .WithNameAndRank(_name, 1)
                                        .WithStartTimeEndTime(_startTime, _endTime)
                                        .Persist();
            shiftFactory.Object.EndTime = Convert.ToDateTime("2001-01-01 09:00:00").AddHours(0).AddMinutes(0);
            Assert.Throws<EmptyFieldException>(() => _shiftService.Update(shiftFactory.Object));
        }
        
        #endregion

    }
}
