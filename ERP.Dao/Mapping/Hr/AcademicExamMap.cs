using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class AcademicExamMap : BaseClassMap<BusinessModel.Entity.Hr.AcademicExam,long> {

        public AcademicExamMap()
        {
            Table("HR_AcademicExam");
			LazyLoad();
			Map(x => x.Name).Column("Name");
			HasMany(x => x.AcademicInfo).KeyColumn("ExamId");
        }
    }
}
