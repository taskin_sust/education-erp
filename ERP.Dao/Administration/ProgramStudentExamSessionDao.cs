﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IProgramStudentExamSessionDao : IBaseDao<ProgramStudentExamSession, long> 
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<ProgramStudentExamSession> LoadProgramStudentExamSession(long programId, long sessionId); 
        #endregion

        #region Others Function
        
        #endregion

       
    }
    public class ProgramStudentExamSessionDao : BaseDao<ProgramStudentExamSession, long>, IProgramStudentExamSessionDao
    {
        #region Propertise & Object Initialization

        #endregion
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<ProgramStudentExamSession> LoadProgramStudentExamSession(long programId, long sessionId)
        {

           // ICriteria criteria = Session.CreateCriteria<ProgramBoardSession>();
            var returnProgramBoardSessions = Session.QueryOver<ProgramStudentExamSession>()
                .Where(x => x.Program.Id == programId && x.Session.Id == sessionId)
                .List<ProgramStudentExamSession>();
            return returnProgramBoardSessions;
        }

        #endregion

        #region Others Function

        #endregion

        
    }
}
