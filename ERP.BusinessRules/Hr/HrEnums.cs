﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.BusinessRules.Hr
{
    public enum SalaryPurpose
    {
        [Description("Administrative Purpose")]
        AdministrativePurpose = 1,
        [Description("Marketing Purpose")]
        MarketingPurpose = 2,
        [Description("Branding Purpose")]
        BrandingPurpose = 3,
        [Description("CSR Purpose")]
        CsrPurpose = 4
    }

    public enum OrganizationType
    {
        [Description("Job Organization")]
        JobOrganization = 1,
        [Description("Salary Organization")]
        SalaryOrganization = 2
    }

    public enum PaymentType
    {
        [Description("Once")]
        Once = 1,
        [Description("Daily")]
        Daily = 2,
        [Description("Monthly")]
        Monthly = 3,
        [Description("Yearly")]
        Yearly = 4
       
    }

    public enum FundedBy
    {
        [Description("Mahmudul Hasan Sohag")]
        Chairman = 1,
        [Description("Muhammad Abul Hasan Liton")]
        ManagingDirector = 2
    }

    public enum LoanStatus
    {
        [Description("Approved")]
        Approved = 1,
        [Description("Pending")]
        Pending = 2,
        [Description("Cancelled")]
        Cancelled = 3
    }
    public enum AllowanceSettingsEditType
    {
        Uneditable = 0,
        MinimumEditable = 1,
        MaximumEditable = 2
    }

    public enum CalculationOn
    {
        [Description("Basic")]
        Basic = 1,
        [Description("Gross")]
        Gross = 2
    }

    public struct MultiplyingFactor
    {
        [Description("1")]
        public const double One = 1;
        [Description("1.5")]
        public const double OneAndHalf = 1.5;
        [Description("2")]
        public const double Two = 2;
        [Description("2.5")]
        public const double TwoAndHalf = 2.5;
        [Description("3")]
        public const double Three = 3;
    }

    public enum MonthsOfYear
    {
        [Description("January")]
        January = 1,
        [Description("February")]
        February = 2,
        [Description("March")]
        March = 3,
        [Description("April")]
        April = 4,
        [Description("May")]
        May = 5,
        [Description("June")]
        June = 6,
        [Description("July")]
        July = 7,
        [Description("August")]
        August = 8,
        [Description("September")]
        September = 9,
        [Description("October")]
        October = 10,
        [Description("November")]
        November = 11,
        [Description("December")]
        December = 12
    }
    
    public enum MemberAchievementTaskType
    {
        [Description("Complete Task")]
        Complete = 1,
        [Description("Running Task")]
        Running = 2,
        [Description("Failed Task")]
        Failed = 3,
        [Description("Future Task")]
        Future = 4,
        [Description("Learned Task")]
        Learned = 5,
        [Description("Teach Task")]
        Teach = 6
    }
}


