﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UdvashERP.PublicAdmission.Startup))]
namespace UdvashERP.PublicAdmission
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
