using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {


    public partial class CourseSubjectMap : BaseClassMap<CourseSubject, long>
    {
        
        public CourseSubjectMap() {
			Table("CourseSubject");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");
			
            Map(x => x.Payment).Column("Payment");
            Map(x => x.GuideFee).Column("GuideFee");
            Map(x => x.TutionFee).Column("TutionFee");

            References(x => x.Course).Column("CourseId");
            References(x => x.Subject).Column("SubjectId");
            HasMany(x => x.LectureSettingses).KeyColumn("CourseSubjectId").Cascade.AllDeleteOrphan().Inverse(); 
            HasMany(x => x.StudentCourseDetails).KeyColumn("CourseSubjectId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.StudentClassAttendences).KeyColumn("CourseSubjectId").Cascade.AllDeleteOrphan().Inverse();
            HasManyToMany(x => x.StudentPaymentList).Table("StudentPaymentCourseSubject");
        }
    }
}
