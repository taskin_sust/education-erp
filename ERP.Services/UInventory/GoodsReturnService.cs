using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.UInventory;
using UdvashERP.MessageExceptions;

namespace UdvashERP.Services.UInventory
{
    public interface IGoodsReturnService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(GoodsReturn goodsReturn);

        #endregion

        #region Single Instances Loading Function

        GoodsReturn GetGoodsReturn(long id);


        #endregion

        #region List Loading Function

        List<GoodsReturnDto> LoadGoodsReturn(int start, int length, string orderBy, string toUpper, List<UserMenu> userMenu, long? organizationId, long? branchId, string item, DateTime? goodsReturnDateValue, int? status);
        IList<dynamic> LoadGoodsReturnReport(int start, int length, List<UserMenu> userMenus, long organizationId,
            List<long> branchIdList, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList,
            List<long> supplierList, DateTime dateFrom, DateTime dateTo, List<string> columnList, int reportType, List<string> programsession);

        //authorized branch names from goods return table
        IList<string> LoadGoodsReturnBranchNameList(List<UserMenu> userMenu, long organizationId, List<long> branchIdList);

        IList<string> LoadGoodsReturnProgramSessionNameList(List<UserMenu> userMenu, long organizationId, List<long> programIdList, List<long> branchIdList, List<long> sessionIdList);

        #endregion

        #region Others Function

        long GetGoodsReturnReportCount(List<UserMenu> userMenu, long organizationId, List<long> branchIdList, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, DateTime fromDate, DateTime toDate, List<string> columnList, int reportType, List<string> programsession);
        long GetTotalReturnAmountAgainstWorkorderNo(string workOrderNo);
        long GetTotalReturnAmountAgainstReceiveId(long receiveId);
        #endregion

        #region Helper Function

        int GetGoodsReturnCount(List<UserMenu> userMenu, long? organizationId, long? branchId, string item, DateTime? goodsReturnDateValue, int? returnStatus);


        #endregion

    }
    public class GoodsReturnService : BaseService, IGoodsReturnService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("InventoryService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IGoodsReturnDao _goodsReturnDao;
        private readonly IBranchDao _branchDao;
        private readonly ICurrentStockSummaryDao _currentStockSummaryDao;
        public GoodsReturnService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _goodsReturnDao = new GoodsReturnDao { Session = session };
            _branchDao = new BranchDao { Session = session };
            _currentStockSummaryDao = new CurrentStockSummaryDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(GoodsReturn goodsReturn)
        {
            ITransaction transaction = null;
            try
            {
                if (goodsReturn == null || goodsReturn.GoodsReturnDetails == null || goodsReturn.GoodsReturnDetails.Count == 0)
                {
                    throw new InvalidDataException("goods return can not be null.");
                }
                using (transaction = Session.BeginTransaction())
                {
                    var goodRecieve = goodsReturn.GoodsReceive;
                    int totalReceived = 0;
                    totalReceived = goodRecieve.GoodsReceiveDetails.Sum(x => x.ReceivedQuantity);
                    var program = goodsReturn.GoodsReceive.GoodsReceiveDetails[0].Program;
                    var session = goodsReturn.GoodsReceive.GoodsReceiveDetails[0].Session;
                    long? programId = null;
                    long? sessionId = null;
                    if (program != null && session != null)
                    {
                        programId = program.Id;
                        sessionId = session.Id;
                    }

                    var currentStockSummary = _currentStockSummaryDao.GetCurrentStockSummary(goodsReturn.GoodsReturnDetails[0].Item.Id, goodsReturn.GoodsReceive.Branch.Id, programId, sessionId);
                    foreach (var goodsReturnDetailse in goodsReturn.GoodsReturnDetails)
                    {
                        var detailse = goodsReturnDetailse;
                        //var receivedQuantity =
                        //    goodsReturn.GoodsReceive.GoodsReceiveDetails.Where(x => x.Item.Id == detailse.Item.Id)
                        //        .Sum(x => x.ReceivedQuantity);
                        //var previouslyReturnedQuantity=goodsReturn.GoodsReturnDetails.Where(x => x.Item.Id == detailse.Item.Id).Sum(x => x.ReturnedQuantity);
                        var previouslyReturnedQuantity = goodsReturn.GoodsReceive.GoodsReturnList.Sum(x => x.GoodsReturnDetails[0].ReturnedQuantity);
                        if (goodsReturnDetailse.ReturnedQuantity + previouslyReturnedQuantity > totalReceived)
                        {
                            throw new OverflowException("Goods return can not be greater than goods receive");
                        }
                        if (currentStockSummary != null)
                        {
                            if (goodsReturnDetailse.ReturnedQuantity > Convert.ToInt32(currentStockSummary.StockQuantity))
                                throw new OverflowException("Current Stock Quantity is: " + currentStockSummary.StockQuantity, new Exception(goodsReturn.GoodsReceive.GoodsReceivedNo));
                        }
                        int currentlyReturned = 0;
                        currentlyReturned = goodsReturn.GoodsReturnDetails.Sum(x => x.ReturnedQuantity);

                        if (totalReceived == currentlyReturned)
                            goodsReturnDetailse.ReturnStatus = (int)GoodsReturnStatus.Returned;
                        else goodsReturnDetailse.ReturnStatus = (int)GoodsReturnStatus.PartiallyReturned;
                        goodsReturnDetailse.Status = GoodsReturn.EntityStatus.Active;
                        if (HttpContext.Current != null)
                        {
                            goodsReturnDetailse.CreateBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                            goodsReturnDetailse.ModifyBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                        }
                    }
                    goodsReturn.Status = GoodsReturn.EntityStatus.Active;
                    ModelValidationCheck(goodsReturn);
                    var branch = goodsReturn.GoodsReceive.Branch;
                    string goodsReturnNo = GenerateSeqeuntialNo(branch.Id);
                    goodsReturn.GoodsReturnNo = branch.Organization.ShortName.Substring(0, 3) +
                                           branch.Code + "GRT" + goodsReturnNo;
                    _goodsReturnDao.Save(goodsReturn);
                    if (currentStockSummary != null)
                    {
                        currentStockSummary.StockQuantity -= goodsReturn.GoodsReturnDetails[0].ReturnedQuantity;
                        _currentStockSummaryDao.Update(currentStockSummary);
                    }
                    transaction.Commit();
                    return true;
                }
            }
            catch (OverflowException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (transaction != null) transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public void ModelValidationCheck(GoodsReturn goodsReturn)
        {
            var validationResult = ValidationHelper.ValidateEntity<GoodsReturn, GoodsReturn>(goodsReturn);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
            foreach (var goodsReturnDetailse in goodsReturn.GoodsReturnDetails)
            {
                validationResult = ValidationHelper.ValidateEntity<GoodsReturnDetails, GoodsReturnDetails>(goodsReturnDetailse);
                if (validationResult.HasError)
                {
                    string errorMessage = "";
                    validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                    throw new InvalidDataException(errorMessage);
                }
            }

        }

        private string GenerateSeqeuntialNo(long branchId)
        {
            try
            {
                string goodsReturnNo = _goodsReturnDao.GenerateSeqeuntialNo(branchId);
                int zeroCount = 6 - goodsReturnNo.Length;
                var zeroString = "";
                for (int i = 0; i < zeroCount; i++)
                {
                    zeroString += "0";
                }
                goodsReturnNo = zeroString + goodsReturnNo;
                return goodsReturnNo;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public GoodsReturn GetGoodsReturn(long id)
        {
            try
            {
                var goodsReturn = _goodsReturnDao.LoadById(id);
                return goodsReturn;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        #endregion

        #region List Loading Function

        public List<GoodsReturnDto> LoadGoodsReturn(int start, int length, string orderBy, string toUpper,
            List<UserMenu> userMenu, long? organizationId, long? branchId, string item, DateTime? goodsReturnDateValue,
            int? returnStatus)
        {
            try
            {
                var organizationIdList = organizationId != null
                    ? _commonHelper.ConvertIdToList(organizationId.Value)
                    : null;
                var branchIdList = branchId != null ? _commonHelper.ConvertIdToList(branchId.Value) : null;
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null,
                    branchIdList);
                List<GoodsReturnDto> goodsReturnDtos = _goodsReturnDao.LoadGoodsReturn(start, length, orderBy, toUpper,
                    authBranchIdList, item.Trim(), goodsReturnDateValue, returnStatus);
                return goodsReturnDtos;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<dynamic> LoadGoodsReturnReport(int start, int length, List<UserMenu> userMenus, long organizationId,
            List<long> branchIdList, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList,
            List<long> supplierIdList, DateTime dateFrom, DateTime dateTo, List<string> columnList, int reportType, List<string> programsession)
        {
            try
            {
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, null, branchIdList);
                var goodsReturnList = _goodsReturnDao.LoadGoodsReturnReport(start, length, authBranchIdList.Distinct().ToList(), purposeList.Distinct().ToList(), itemGroupIdList.Distinct().ToList(),
                    itemIdList.Distinct().ToList(), programIdList, sessionIdList, supplierIdList.Distinct().ToList(), dateFrom, dateTo, columnList, reportType, programsession);
                return goodsReturnList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<string> LoadGoodsReturnBranchNameList(List<UserMenu> userMenu, long organizationId, List<long> branchIdList)
        {
            try
            {
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                if (userMenu != null)
                {
                    branchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchIdList);
                }
                return _goodsReturnDao.LoadGoodsReturnBranchNameList(branchIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<string> LoadGoodsReturnProgramSessionNameList(List<UserMenu> userMenu, long organizationId, List<long> branchIdList, List<long> programIdList, List<long> sessionIdList)
        {
            try
            {
                List<long> programIds = new List<long>();
                List<long> sessionIds = new List<long>();
                List<long> branchIds = new List<long>();
                programIds.AddRange(programIdList);
                sessionIds.AddRange(sessionIdList);
                long[] elems = { 0, -1 };
                programIds.RemoveAll(x => elems.Contains(x));
                sessionIds.RemoveAll(x => elems.Contains(x));
                if (programIds.Count == 0)
                {
                    programIds = null;
                }
                if (sessionIds.Count > 0)
                {
                    sessionIds = null;
                }
                if (branchIdList.Count == 0)
                {
                    branchIds = null;

                }
                else
                {
                    branchIds.AddRange(branchIdList);
                }
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                if (userMenu != null)
                {
                    programIds = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, branchIds, programIds);
                }
                return _goodsReturnDao.LoadGoodsReturnProgramSessionNameList(programIds, sessionIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public long GetTotalReturnAmountAgainstWorkorderNo(string workOrderNo)
        {
            try
            {
                if (workOrderNo.Length < 13) throw new InvalidDataException("invalid workorder number");
                return _goodsReturnDao.GetTotalReturnAmountAgainstWorkorderNo(workOrderNo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public long GetTotalReturnAmountAgainstReceiveId(long receiveId)
        {
            try
            {
                if (receiveId <= 0) throw new InvalidDataException("invalid receive number ");
                return _goodsReturnDao.GetTotalReturnAmountAgainstReceiveId(receiveId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetGoodsReturnCount(List<UserMenu> userMenu, long? organizationId, long? branchId, string item,
            DateTime? goodsReturnDateValue, int? returnStatus)
        {
            try
            {
                var organizationIdList = organizationId != null
                    ? _commonHelper.ConvertIdToList(organizationId.Value)
                    : null;
                var branchIdList = branchId != null ? _commonHelper.ConvertIdToList(branchId.Value) : null;
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null,
                    branchIdList);
                int goodsReturnCount = _goodsReturnDao.GetGoodsReturnCount(authBranchIdList, item.Trim(),
                    goodsReturnDateValue, returnStatus);
                return goodsReturnCount;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public long GetGoodsReturnReportCount(List<UserMenu> userMenu, long organizationId, List<long> branchIdList,
            List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList,
            List<long> sessionIdList, List<long> supplierIdList, DateTime fromDate, DateTime toDate, List<string> columnList, int reportType, List<string> programsession)
        {
            try
            {
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchIdList);
                var count = _goodsReturnDao.GetGoodsReturnReportCount(authBranchIdList.Distinct().ToList(), purposeList.Distinct().ToList(), itemGroupIdList.Distinct().ToList(),
                    itemIdList.Distinct().ToList(), programIdList, sessionIdList, supplierIdList.Distinct().ToList(), fromDate, toDate, columnList, reportType, programsession);
                return count;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                throw;
            }
        }


        #endregion

        #region Helper function



        #endregion

    }
}

