﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface IPreviousStudentAdmissionService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        
        #endregion

        #region Others Function

        #endregion
    }
    public class PreviousStudentAdmissionService : BaseService, IPreviousStudentAdmissionService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion
       
        #region Properties and Object Initialization
        private readonly IStudentProgramDao _studentProgramDao;

        public PreviousStudentAdmissionService(ISession session)
        {
            Session = session;
            _studentProgramDao = new StudentProgramDao() { Session = Session };
        } 
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
}
