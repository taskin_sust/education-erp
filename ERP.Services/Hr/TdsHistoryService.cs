﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface ITdsHistoryService : IBaseService
    {
        #region Operational Function
        
        bool Save(TdsHistory hrTdsHistory);
        bool AddTdsHistory(TeamMember member, string tdsAmount, string effectiveDate);
        bool UpdateTdsHistory(TdsHistory tdsHistory, string tdsAmount, string effectiveDate);
        bool DeleteTdsHistory(TdsHistory tdsHistory);

        #endregion

        #region Single Instances Loading Function
        
        TdsHistory GetTdsHistory(long id);

        #endregion

        #region List Loading Function
        
        List<TdsHistory> LoadTdsHistories(TeamMember member, DateTime? searchingDate = null);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

        
    }
    public class TdsHistoryService : BaseService, ITdsHistoryService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        
        private readonly CommonHelper _commonHelper;
        private readonly ITdsHistoryDao _hrTdsHistoryDao;
        private readonly ISalarySheetDao _salarySheetDao;

        public TdsHistoryService(ISession session)
        {
            Session = session;
            _hrTdsHistoryDao = new TdsHistoryDao() { Session = session };
            _salarySheetDao = new SalarySheetDao { Session = session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Functions

        public bool DeleteTdsHistory(TdsHistory tdsHistory)
        {
            ITransaction trans = null;
            try
            {
                CheckBeforeDelete(tdsHistory);
                using (trans = Session.BeginTransaction())
                {
                    tdsHistory.Status = -404;
                    _hrTdsHistoryDao.SaveOrUpdate(tdsHistory);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool Save(TdsHistory hrTdsHistory)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {

                    _hrTdsHistoryDao.Save(hrTdsHistory);
                    var hrTdsHistoryLog = new TdsHistoryLog();
                    hrTdsHistoryLog.EffectiveDate = hrTdsHistory.EffectiveDate;
                    hrTdsHistoryLog.TdsAmount = hrTdsHistory.TdsAmount;
                    hrTdsHistoryLog.TeamMember = hrTdsHistory.TeamMember;
                    hrTdsHistoryLog.TdsHistory = hrTdsHistory;
                    //save hrmember history log
                    hrTdsHistoryLog.CreationDate = DateTime.Now;
                    hrTdsHistoryLog.CreateBy = hrTdsHistoryLog.CreateBy != 0 ? hrTdsHistoryLog.CreateBy : GetCurrentUserId();
                    Session.Save(hrTdsHistoryLog);
                    //save hrmember history log
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool AddTdsHistory(TeamMember member, string tdsAmount, string effectiveDate)
        {
            ITransaction trans = null;
            if (member == null)
            {
                throw new NullObjectException("MemberEntityNotFound");
            }
            if (tdsAmount == null && effectiveDate == null)
            {
                throw new NullObjectException("NullObject");
            }
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    CheckBeforeAdd(member, tdsAmount, effectiveDate);

                    TdsHistory tdsHistory = new TdsHistory();
                    tdsHistory.TeamMember = member;
                    tdsHistory.TdsAmount = Convert.ToDecimal(tdsAmount);
                    tdsHistory.Status = TdsHistory.EntityStatus.Active;
                    tdsHistory.EffectiveDate = Convert.ToDateTime(effectiveDate);
                    _hrTdsHistoryDao.SaveOrUpdate(tdsHistory);
                    SaveTdsHistoryLog(tdsHistory);
                    trans.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool UpdateTdsHistory(TdsHistory tdsHistory, string tdsAmount, string effectiveDate)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    bool canAddLog = false;
                    TeamMember member = tdsHistory.TeamMember;
                    Session.Evict(tdsHistory);
                    CheckBeforeAdd(member, tdsAmount, effectiveDate, tdsHistory.Id);
                    if (tdsHistory.TdsAmount != Convert.ToDecimal(tdsAmount) || tdsHistory.EffectiveDate.Date != Convert.ToDateTime(effectiveDate).Date)
                    {
                        canAddLog = true;
                    }

                    tdsHistory.TdsAmount = Convert.ToDecimal(tdsAmount);
                    tdsHistory.EffectiveDate = Convert.ToDateTime(effectiveDate);
                    _hrTdsHistoryDao.SaveOrUpdate(tdsHistory);
                    if(canAddLog)
                        SaveTdsHistoryLog(tdsHistory);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public TdsHistory GetTdsHistory(long id)
        {
            try
            {
                return Session.QueryOver<TdsHistory>().Where(x => x.Id == id && x.Status == TdsHistory.EntityStatus.Active).List<TdsHistory>().FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            
        }

        #endregion

        #region List Loading Function

        public List<TdsHistory> LoadTdsHistories(TeamMember member, DateTime? searchingDate = null)
        {
            try
            {
                return _hrTdsHistoryDao.LoadTdsHistories(member, searchingDate).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Others Function


        #endregion

        #region Helper function

        private void SaveTdsHistoryLog(TdsHistory tdsHistory)
        {
            try
            {
                var hrTdsHistoryLog = new TdsHistoryLog();
                hrTdsHistoryLog.TdsHistory = tdsHistory;
                hrTdsHistoryLog.TeamMember = tdsHistory.TeamMember;
                hrTdsHistoryLog.EffectiveDate = tdsHistory.EffectiveDate;
                hrTdsHistoryLog.TdsAmount = tdsHistory.TdsAmount;
                hrTdsHistoryLog.CreationDate = DateTime.Now;
                //hrTdsHistoryLog.CreateBy = tdsHistory.ModifyBy != 0 ? tdsHistory.ModifyBy : Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                hrTdsHistoryLog.CreateBy = GetCurrentUserId();
                Session.Save(hrTdsHistoryLog);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        private void CheckBeforeAdd(TeamMember member, string tdsAmounts, string effectiveDate, long id = 0)
        {
            decimal number;
            DateTime dateCheck;
            if (member == null)
                throw new InvalidDataException("Invalid Team Member");
            if (!Decimal.TryParse(tdsAmounts, out number))
                throw new InvalidDataException("Invalid TDS Amount input!");
            if (!DateTime.TryParse(effectiveDate, out dateCheck))
                throw new InvalidDataException("Invalid Effective date input!");

            SalarySheet salarySheet = _salarySheetDao.GetTeamMemberLastSalarySheet(member.Id, member.Pin);
            if (salarySheet != null)
            {
                if (id > 0)
                {
                    TdsHistory originalTdsHistory = _hrTdsHistoryDao.LoadById(id);
                    if (originalTdsHistory == null) 
                        throw new InvalidDataException("Team Member's TDS history is invalid.");
                    if (salarySheet.EndDate.Date >= dateCheck.Date || salarySheet.EndDate.Date >= originalTdsHistory.EffectiveDate.Date)
                    {
                        throw new InvalidDataException("Team Member's TDS history can only eligible after date (" + salarySheet.EndDate.Date.ToString("d MMM, yyyy") + ").");
                    }
                }
                else
                {
                    if (salarySheet.EndDate.Date >= dateCheck.Date)
                    {
                        throw new InvalidDataException("Team Member's TDS history can only eligible after date (" + salarySheet.EndDate.Date.ToString("d MMM, yyyy") + ").");
                    }
                }
            }
            

        }

        private void CheckBeforeDelete(TdsHistory tdsHistory)
        {
            TeamMember teamMember = tdsHistory.TeamMember;
            SalarySheet salarySheet = _salarySheetDao.GetTeamMemberLastSalarySheet(teamMember.Id, teamMember.Pin);
            if (salarySheet != null && salarySheet.EndDate.Date >= tdsHistory.EffectiveDate.Date)
            {
                throw new InvalidDataException("Team Member's TDS history can delete after date (" + salarySheet.EndDate.Date.ToString("d MMM, yyyy") + ").");
            }
            int noOfTdsHistory = teamMember.TdsHistory.Where(x => x.Status != TdsHistory.EntityStatus.Delete).ToList().Count;
            if (noOfTdsHistory <= 1)
            {
                throw new DependencyException("Team member must have atleast one Tds history!!");
            }
        }

        #endregion
    }
}
