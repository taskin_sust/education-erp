using System;
using System.Linq;
using NHibernate;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class ExtradayAllowanceSettingFactory : ObjectFactoryBase<ExtradayAllowanceSetting>
    {
        private OrganizationFactory _organizationFactory;
        private readonly IExtradayAllowanceSettingService _prExtradayAllowanceService;

        private readonly string _settingsName = "SettingName_" + DateTime.Now.Second + "_" + DateTime.Now.Millisecond;
        
        public ExtradayAllowanceSettingFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _prExtradayAllowanceService = new ExtradayAllowanceSettingService(session);
            _organizationFactory = new OrganizationFactory(this, session);
        }

        #region Create

        public ExtradayAllowanceSettingFactory Create(Organization organization = null)
        {
            var entity = new ExtradayAllowanceSetting();
            if (organization == null)
            {
                _organizationFactory.Create().Persist();
                entity.Organization = _organizationFactory.Object;
            }
            else
            {
                entity.Organization = organization;
            }
            entity.Name = _settingsName;
            entity.MultiplyingFactor = 1.0M;
            entity.CalculationOn = 1;
            entity.ClosingDate = DateTime.Now.AddYears(1);
            entity.BusinessId = GetPcUserName();
            entity.VersionNumber = 500;
            entity.CreationDate = DateTime.Now;
            entity.ModificationDate = DateTime.Now;
            entity.ModifyBy = 0;
            entity.CreateBy = 0;
            entity.EffectiveDate = DateTime.Now.AddDays(1);
            entity.IsContractual = true;
            entity.IsPartTime = true;
            entity.IsPermanent = true;
            entity.IsProbation = true;
            entity.IsIntern = true;
            entity.IsGazetted = true;
            entity.IsManagement = true;
            entity.IsWeekend = true;
            Object = entity;
            SingleObjectList.Add(Object);
            return this;
        }

        public ExtradayAllowanceSettingFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public ExtradayAllowanceSettingFactory Persist(List<UserMenu> userMenus)
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _prExtradayAllowanceService.SaveOrUpdate(x, userMenus); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_prExtradayAllowanceService.SaveOrUpdate(x, userMenus); });

            }
            return this;
        }

        public ExtradayAllowanceSettingFactory CreateMore(int num, Organization organization = null)
        {
            for (int i = 0; i < num; i++)
            {
                var obj = new ExtradayAllowanceSetting();

                if (organization == null)
                {
                    _organizationFactory.Create().Persist();
                    obj.Organization = _organizationFactory.Object;
                }
                else
                {
                    obj.Organization = organization;
                }
                obj.Name = "SettingName_" + i + "_" + DateTime.Now.Ticks;
                obj.MultiplyingFactor = 1.0M;
                obj.CalculationOn = 1;
                obj.BusinessId = GetPcUserName();
                obj.VersionNumber = 500;
                obj.CreationDate = DateTime.Now;
                obj.ModificationDate = DateTime.Now;
                obj.ModifyBy = 0;
                obj.CreateBy = 0;
                obj.IsContractual = true;
                obj.IsPartTime = true;
                obj.IsPermanent = true;
                obj.IsProbation = true;
                obj.IsIntern = true;
                obj.IsGazetted = true;
                obj.IsManagement = true;
                obj.IsWeekend = true;
                GetDateRange(obj);
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other

        private void GetDateRange(ExtradayAllowanceSetting obj)
        {
            var effectiveDate = DateTime.Now.AddDays(1);
            var closingDate = effectiveDate.AddDays(10);

            if (ObjectList.Count > 0)
            {

                var easObj = ObjectList.LastOrDefault();
                if (easObj != null)
                {
                    effectiveDate = easObj.EffectiveDate.AddDays(1);
                    if (easObj.ClosingDate != null) effectiveDate = easObj.ClosingDate.Value.AddDays(1);
                    var day = DateTime.DaysInMonth(effectiveDate.Year, effectiveDate.Month);
                    closingDate = new DateTime(effectiveDate.Year, effectiveDate.Month, day);
                }
            }
            else if (SingleObjectList.Count > 0)
            {
                var easObj = SingleObjectList.LastOrDefault();
                if (easObj != null)
                {
                    effectiveDate = easObj.EffectiveDate.AddDays(1);

                    if (easObj.ClosingDate != null) effectiveDate = easObj.ClosingDate.Value.AddDays(1);
                    var day = DateTime.DaysInMonth(effectiveDate.Year, effectiveDate.Month);
                    closingDate = new DateTime(effectiveDate.Year, effectiveDate.Month, day);
                }
            }
            else
            {
                var day = DateTime.DaysInMonth(effectiveDate.Year, effectiveDate.Month);
                closingDate = new DateTime(effectiveDate.Year, effectiveDate.Month, day);
            }
            obj.EffectiveDate = effectiveDate;
            obj.ClosingDate = closingDate;
        }

        #endregion

        #region CleanUp

        public ExtradayAllowanceSettingFactory CleanUp()
        {

            DeleteAll();
            _organizationFactory.Cleanup();
            return this;
        }

        #endregion

    }
}
