﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.UserAuth;
using InvalidDataException = System.IO.InvalidDataException;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class SpecificationTemplatesController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ISpecificationTemplateService _specificationTemplateService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _userMenu;
        private readonly IOrganizationService _organizationService;
        private readonly IItemGroupService _itemGroupService;
        private readonly IItemService _itemService;
        public SpecificationTemplatesController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _specificationTemplateService = new SpecificationTemplateService(session);
            _userService=new UserService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _itemGroupService = new ItemGroupService(session);
            _itemService = new ItemService(session);

        }
        #endregion

        #region Create Template
        public ActionResult Create()
        {
            ViewBag.FieldType = new SelectList(_commonHelper.LoadEmumToDictionary<FieldType>(), "Key", "Value");
            return View();
        }

        [HttpPost]
        public ActionResult AjaxRequestForSubmitTemplate(SpecificationTemplateViewModel data)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    
                    throw new InvalidDataException("Validation Error.");
                }
                //SpecificationCriteria
                var specificationTemplate = new SpecificationTemplate();
                var specificationCriterias = new List<SpecificationCriteria>();

                specificationTemplate.Name = data.Name;

                foreach (var spCriteria in data.SpecificationCriteriaList)
                {
                    var spc = new SpecificationCriteria();
                    spc.Id = spCriteria.SpecificationCriteriaId;
                    spc.Name = spCriteria.Name;
                    spc.Controltype = spCriteria.Controltype;
                    var specificationCriteriaOptions = new List<SpecificationCriteriaOption>();
                    if (spCriteria.Controltype == FieldType.Dropdown)
                    {
                        foreach (var spOptions in spCriteria.SpecificationCriteriaOptions)
                        {
                            var spco = new SpecificationCriteriaOption { Id = spOptions.SpecificationCriteriaOptionId, Name = spOptions.Name };
                            spco.SpecificationCriteria = spc;
                            specificationCriteriaOptions.Add(spco);
                        }

                    }
                    spc.SpecificationCriteriaOptions = specificationCriteriaOptions;
                    spc.SpecificationTemplate = specificationTemplate;
                    specificationCriterias.Add(spc);
                }

                specificationTemplate.SpecificationCriteriaList = specificationCriterias;
                var successmessage = "";
                if (data.SpecificTemplateId > 0)
                {
                    specificationTemplate.Id = Convert.ToInt64(data.SpecificTemplateId);
                    successmessage = "Specification Template Updated Successfully";
                }
                else
                {
                    successmessage = "Specification Template Saved Successfully !";
                }
                _specificationTemplateService.SaveOrUpdate(specificationTemplate);
                return Json(new Response(true, successmessage));
                //return RedirectToAction("Index",new {message=successmessage});
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (NullObjectException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (DuplicateEntryException ex)
            {

                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

        }

        #endregion

        #region Edit
        public ActionResult Edit(long id)
        {
            ViewBag.FieldTypeSelectList = new SelectList(_commonHelper.LoadEmumToDictionary<FieldType>(), "Key", "Value");
            ViewBag.FieldType = _commonHelper.LoadEmumToDictionary<FieldType>();
            ViewBag.SpecificationTemplateId = id;
            var specificationTemplate = _specificationTemplateService.GetSpecificationTemplate(id);
            return View(specificationTemplate);
        } 
        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isSuccess = _specificationTemplateService.Delete(id);
                return Json(isSuccess ? new Response(true, "Specification template sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Specification template Delete Fail !"));
            }
        }
        #endregion

        #region Details Operation

        public ActionResult Details(int id)
        {
            try
            {
                var specificationTemplateObj = _specificationTemplateService.GetSpecificationTemplate(id);
                if (specificationTemplateObj != null)
                {
                    specificationTemplateObj.CreateByText = _userService.GetUserNameByAspNetUserId(specificationTemplateObj.CreateBy);
                    specificationTemplateObj.ModifyByText = _userService.GetUserNameByAspNetUserId(specificationTemplateObj.ModifyBy);
                    ViewBag.AssignedItems = string.Join(", ",specificationTemplateObj.Items);
                }
                string specificationCriteriaString = "";

                if (specificationTemplateObj != null)
                {
                    foreach (var specificationCriteria in specificationTemplateObj.SpecificationCriteriaList)
                    {
                        //specificationCriteriaString+=
                        if (specificationCriteria.Controltype ==  FieldType.Dropdown)
                        {
                            specificationCriteriaString += specificationCriteria.Name + "(";
                            string options = string.Join(", ",
                            specificationCriteria.SpecificationCriteriaOptions.Select(x => x.Name).ToList());
                            specificationCriteriaString += options + "), ";
                        }
                        else if (specificationCriteria.Controltype == FieldType.Text)
                        {
                            specificationCriteriaString += specificationCriteria.Name + ", ";
                        }
                    }
                    specificationCriteriaString = specificationCriteriaString.Trim(' ', ',');
                }
                ViewBag.specificationCriteriaString = specificationCriteriaString;
                return View(specificationTemplateObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Manage Specification
        public ActionResult Index(string message="")
        {
            if (!string.IsNullOrEmpty(message))
            {
                ViewBag.SuccessMessage = message;
            }
            return View();
        }
        
        #region Render Data Table
        public JsonResult SpecificationTemplateList(int draw, int start, int length, string searchString)
        {
            var getData = new List<object>();
            try
            {
                int recordsTotal = _specificationTemplateService.SpecificationTemplateRowCount(searchString);
                long recordsFiltered = recordsTotal;
                List<SpecificationTemplateDto> specificationTemplateList = _specificationTemplateService.LoadSpecificationTemplateList(start, length, searchString).ToList();
                var obj = new SpecificationTemplateDto();
                foreach (var specificationTemplate in specificationTemplateList)
                {
                    var str = new List<string> { specificationTemplate.SpecificationName, specificationTemplate.SpecificationCriteriaName, specificationTemplate.AssignedItemName };
                    str.Add(LinkGenerator.GetGeneratedLink("Details", "Edit", "Delete", "SpecificationTemplates", specificationTemplate.SpecificationId, specificationTemplate.SpecificationName));
                    getData.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData,
                    isSuccess = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }
        #endregion
        #endregion


        #region Assign Specification 

        public ActionResult AssignSpecification() 
        {
            try
            {
                _userMenu = (List<UserMenu>) ViewBag.UserMenu;

                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu),
                    "Id", "ShortName");
                ViewBag.ItemGroupList = new SelectList(_itemGroupService.LoadItemGroup(),
                    "Id", "Name");
                ViewBag.SpecificationTemplateList = new SelectList(_specificationTemplateService.LoadISpecificationTemplateList(SpecificationTemplate.EntityStatus.Active),
                    "Id", "Name");
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                TempData["CommonErrorMessage"] = ex.Message;
                return RedirectToAction("Error", "Home", new { area = "" });
            }
        }
        [HttpPost]
        public ActionResult AssignSpecification(AssignSpecificationViewModel assignSpecificationViewModel)
        {
            try
            {
                IList<Item> itemList = _itemService.LoadItem(assignSpecificationViewModel.OrganizationId, assignSpecificationViewModel.ItemGroupId,null,null,true);
                if (!itemList.Any())
                {
                    throw new MessageException("No Item found");
                }
                var sptemp = _specificationTemplateService.GetSpecificationTemplate(
                    assignSpecificationViewModel.SpecificationTemplateId);

                if (assignSpecificationViewModel.SelectedItem ==null || !assignSpecificationViewModel.SelectedItem.Any())
                {
                    throw new MessageException("Please select at least one item");
                }

                foreach (var item in itemList)
                {
                    item.SpecificationTemplate = assignSpecificationViewModel.SelectedItem.Contains(item.Id) ? sptemp : item.SpecificationTemplate == sptemp ? null : item.SpecificationTemplate;
                    //_itemService.SaveOrUpdate(item);

                }
                 _itemService.SaveOrUpdate(itemList);
               
                ViewBag.SuccessMessage = "Assign Successfully";
            }
            catch (MessageException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            finally
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu),
                    "Id", "ShortName");
                ViewBag.ItemGroupList = new SelectList(_itemGroupService.LoadItemGroup(),
                    "Id", "Name");
                ViewBag.SpecificationTemplateList = new SelectList(_specificationTemplateService.LoadISpecificationTemplateList(SpecificationTemplate.EntityStatus.Active),
                    "Id", "Name");
            }
            return View(assignSpecificationViewModel);
        }
        public ActionResult AjaxRequestForLoadItems(long specificationTemplateId, long organizationId, long itemGroupId)
        {
            try
            {
                IList<Item> itemList = _itemService.LoadItem(organizationId, itemGroupId,null,null,true);
                if (!itemList.Any())
                {
                    return Json(new Response(false, "No Item Found"));
                }
                var itemCheckViewModels = new List<CheckViewModel>();
                foreach (var item in itemList)
                {
                    var itemchViewModel = new CheckViewModel
                    {
                        Name = item.Name,
                        Id = item.Id,
                    };
                    if (item.SpecificationTemplate != null && item.SpecificationTemplate.Id == specificationTemplateId)
                    {
                        itemchViewModel.Checked = true;
                    }
                    else
                    {
                        itemchViewModel.Checked = false;
                    }
                    itemCheckViewModels.Add(itemchViewModel);
                }

                 return Json(new { returnList = itemCheckViewModels, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion



    }
}