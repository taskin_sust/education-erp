﻿using System.Security.Cryptography.X509Certificates;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.OrganizationTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Organization")]
    public class OrganizationCountTest : OrganizationBaseTest
    {
        [Fact]
        public void Organization_Count_Greater_Than_Zero_Check()
        {
            _organizationFactory.CreateMore(100).Persist();
            var organizationCount = _organizationService.GetOrganizationCount("", "", "", Subject.EntityStatus.Active.ToString());
            Assert.True(organizationCount > 0);
        }
        
    }
}
