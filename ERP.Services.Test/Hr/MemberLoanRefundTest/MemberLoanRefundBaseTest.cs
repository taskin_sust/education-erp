﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.MemberLoanRefundTest
{
    public class MemberLoanRefundBaseTest : TestBase, IDisposable
    {
        #region Propertise & Object Initialization

        private ICommonHelper _commonHelper;
        protected readonly MemberLoanRefundFactory _memberLoanRefundFactory;
        protected readonly IMemberLoanRefundService _memberLoanRefundService;
        protected readonly OrganizationFactory _organizationFactory;
        protected readonly BranchFactory _branchFactory;
        protected readonly CampusFactory _campusFactory;
        protected readonly CampusRoomFactory _campusRoomFactory;
        protected readonly DepartmentFactory _departmentFactory;
        protected readonly DesignationFactory _designationFactory;
        protected readonly TeamMemberFactory _teamMemberFactory;
        protected readonly ITeamMemberService _teamMemberService;
        protected readonly SalaryHistoryFactory _salaryHistoryFactory;
        protected readonly MemberLoanFactory _memberLoanFactory;
        protected readonly MemberLoanApplicationFactory _memberLoanApplicationFactory;
        protected readonly IMemberLoanService _memberLoanService;
        protected readonly IOrganizationService _organizationService;
        protected readonly IBranchService _branchService;
        protected readonly ICampusService _campusService;

        public MemberLoanRefundBaseTest()
        {
            _commonHelper = new CommonHelper();
            _memberLoanRefundFactory = new MemberLoanRefundFactory(null, Session);
            _memberLoanRefundService = new MemberLoanRefundService(Session);
            _organizationFactory=new OrganizationFactory(null,Session);
            _branchFactory=new BranchFactory(null,Session);
            _campusFactory=new CampusFactory(null,Session);
            _campusRoomFactory=new CampusRoomFactory(null,Session);
            _departmentFactory=new DepartmentFactory(null,Session);
            _designationFactory=new DesignationFactory(null,Session);
            _teamMemberFactory=new TeamMemberFactory(null,Session);
            _teamMemberService=new TeamMemberService(Session);
            _salaryHistoryFactory=new SalaryHistoryFactory(null,Session);
            _memberLoanFactory=new MemberLoanFactory(null,Session);
            _memberLoanApplicationFactory=new MemberLoanApplicationFactory(null,Session);
            _memberLoanService=new MemberLoanService(Session);
            _organizationService=new OrganizationService(Session);
            _branchService=new BranchService(Session);
            _campusService=new CampusService(Session);
        }

        #endregion

        #region Disposal

        public void Dispose()
        {
            _memberLoanRefundFactory.CleanUp();
            _memberLoanFactory.CleanUp();
            _memberLoanApplicationFactory.CleanUp();
            _salaryHistoryFactory.CleanUp();
            _campusFactory.Cleanup();
            _branchFactory.Cleanup();
            _organizationFactory.Cleanup();
            base.Dispose();
        }

        #endregion
    }
}
