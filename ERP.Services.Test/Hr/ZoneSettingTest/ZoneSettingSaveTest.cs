﻿using System;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ZoneSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ZoneSetting")]
    public class ZoneSettingSaveTest : ZoneSettingBaseTest
    {
        #region Basic Test
        
        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _zoneSettingService.Save(null));
        }
        
        [Fact]
        public void Should_Save_Successfully()
        {
            var zoneFactory =
                _zoneSettingFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .WithToleranceTimeToleranceDayDeductionMultiplier(10, 2, .5m)
                    .Persist();
            ZoneSetting zoneSetting = _zoneSettingService.LoadById(zoneFactory.Object.Id);
            Assert.NotNull(zoneSetting);
            Assert.True(zoneSetting.Id > 0);
        } 

        #endregion
       
        #region Business Logic Test
        
        [Fact]
        public void Save_Should_Check_Property()
        {
            ZoneSettingFactory zoneFactory;
            zoneFactory = _zoneSettingFactory.Create().WithOrganization().WithNameAndRank("", 1);
            Assert.Throws<EmptyFieldException>(() => zoneFactory.Persist());
        }
        
        [Fact]
        public void Save_Should_Check_Duplicate_Name()
        {
            var zoneFactory1 =
                _zoneSettingFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .WithToleranceTimeToleranceDayDeductionMultiplier(10, 2, .5m).Persist();


            var organization = _organizationService.LoadById(zoneFactory1.Object.Organization.Id);
            var zoneFactory2 =
                _zoneSettingFactory.Create()
                    .WithOrganization(organization)
                    .WithNameAndRank(_name, 1)
                    .WithToleranceTimeToleranceDayDeductionMultiplier(11, 2, .5m);
            Assert.Throws<DuplicateEntryException>(() => _zoneSettingService.Save(zoneFactory2.Object));

        }
        
        [Fact]
        public void Save_Should_Check_Duplicate_ToleranceTime()
        {
            var zoneFactory1 =
                _zoneSettingFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .WithToleranceTimeToleranceDayDeductionMultiplier(10, 2, .5m).Persist();
            var organization = _organizationService.LoadById(zoneFactory1.Object.Organization.Id);
            var zoneFactory2 =
                _zoneSettingFactory.Create()
                    .WithOrganization(organization)
                    .WithNameAndRank(_name + "  DEF  GHI", 1)
                    .WithToleranceTimeToleranceDayDeductionMultiplier(10, 2, .5m);
            Assert.Throws<DuplicateEntryException>(() => _zoneSettingService.Save(zoneFactory2.Object));
        } 

        #endregion
        
    }
}
