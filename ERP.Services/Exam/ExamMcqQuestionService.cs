﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.Exam;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Exam;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Exam
{
    public interface IExamMcqQuestionService : IBaseService
    {
        #region Operational Function
        void SaveOrUpdate(List<ExamMcqQuestion> examMcqQuestions);

        bool Delete(long id);

        #endregion

        #region Single Instances Loading Function

        ExamMcqQuestion GetExamMcqQuestion(long id);

        List<ExamMcqQuestion> GetExamMcqQuestion(long examId, int uniqueSet, long subjectId, int questionSerial);

        #endregion

        #region List Loading Function

        IList<ExamMcqQuestion> GetExamMcqQuestionList(int start, int length, List<UserMenu> authorizeMenu, List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, int? examVersion, string keyword, string uniqueSetNo);
        
        IList<ExamMcqQuestion> LoadExamMcqQuestion(List<UserMenu> authorizeMenu, long examId, int uniqueSet, int version, int? questionSerial = null);
        
        #endregion

        #region Others Function
        int GetLastQuestionSerial(List<UserMenu> authorizeMenu, long examId, int uniqueSet);

        int GetExamMcqQuestionCount(List<UserMenu> authorizeMenu, List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, int? examVersion, string keyword, string uniqueSetNo);


        #endregion

    }
    public class ExamMcqQuestionService : BaseService, IExamMcqQuestionService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("ExamService");
        #endregion

        #region Propertise & Object Initialization
        private List<long> _authorizeOrganizationIdList, _authorizeProgramIdList, _authSessionIdList, _authCourseIdList;
        private readonly IExamMcqQuestionDao _examMcqQuestionDao;
        private readonly IExamMcqQuestionSetDao _examMcqQuestionSetDao;
        private readonly IExamsStudentMarksDao _examsStudentMarksDao;
        private readonly ISessionDao _sessionDao;
        private readonly ICourseDao _courseDao;
        public ExamMcqQuestionService(ISession session)
        {
            Session = session;
            _examMcqQuestionDao = new ExamMcqQuestionDao() { Session = Session };
            _examMcqQuestionSetDao = new ExamMcqQuestionSetDao() { Session = Session };
            _sessionDao = new SessionDao { Session = Session };
            _courseDao = new CourseDao { Session = Session };
            _examsStudentMarksDao = new ExamsStudentMarksDao { Session = Session };
        }

        #endregion

        #region Operational Function

        public void SaveOrUpdate(List<ExamMcqQuestion> examMcqQuestions)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeSaveOrUpdate(examMcqQuestions);

                using (transaction = Session.BeginTransaction())
                {
                    foreach (var examMcqQuestion in examMcqQuestions)
                    {
                        ExamMcqQuestion obj = examMcqQuestion;

                        if (examMcqQuestion.Id == 0)
                        {
                            obj.Status = ExamMcqQuestion.EntityStatus.Active;
                            _examMcqQuestionDao.Save(obj);

                        }
                        else
                        {
                            var questionDb = _examMcqQuestionDao.LoadById(examMcqQuestion.Id);
                            if (questionDb.CorrectAnswer != obj.CorrectAnswer)
                            {
                                UpdateExamMcqQuestionSet(questionDb.Exams.Id, questionDb.UniqueSet, questionDb.QuestionSerial, obj.CorrectAnswer);
                            }
                            //CheckBeforeUpdateOrDelete(questionDb);
                            questionDb.QuestionText = obj.QuestionText;
                            questionDb.OptionA = obj.OptionA;
                            questionDb.OptionB = obj.OptionB;
                            questionDb.OptionC = obj.OptionC;
                            questionDb.OptionD = obj.OptionD;
                            questionDb.OptionE = obj.OptionE;
                            questionDb.CorrectAnswer = obj.CorrectAnswer;
                            questionDb.IsShuffle = obj.IsShuffle;
                            _examMcqQuestionDao.Update(questionDb);
                        }
                    }
                    transaction.Commit();
                }
            }
            catch (MessageException)
            {
                throw;
            }
            catch (EmptyFieldException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (DuplicateEntryException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public bool Delete(long id)
        {
            ITransaction transaction = null;
            try
            {
                var questionObj = _examMcqQuestionDao.LoadById(id);
                CheckBeforeUpdateOrDelete(questionObj);
                var examQuestionList = _examMcqQuestionDao.GetExamMcqQuestion(questionObj.Exams.Id,
                    questionObj.UniqueSet,
                    questionObj.Subject.Id, questionObj.QuestionSerial);
                using (transaction = Session.BeginTransaction())
                {
                    foreach (var question in examQuestionList)
                    {
                        question.Status = ExamMcqQuestion.EntityStatus.Delete;
                        _examMcqQuestionDao.Update(question);
                    }
                    transaction.Commit();
                    return true;
                }
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public ExamMcqQuestion GetExamMcqQuestion(long id)
        {
            try
            {
                return _examMcqQuestionDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<ExamMcqQuestion> GetExamMcqQuestion(long examId, int uniqueSet, long subjectId, int questionSerial)
        {
            try
            {
                return _examMcqQuestionDao.GetExamMcqQuestion(examId, uniqueSet, subjectId, questionSerial);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        //public List<ExamMcqQuestion> LoadExamMcqQuestionList(long examId, List<int> uniqueSetList)
        //{
        //    try
        //    {
        //        return _examMcqQuestionDao.LoadExamMcqQuestionList(examId, uniqueSetList);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}

        public IList<ExamMcqQuestion> GetExamMcqQuestionList(int start, int length, List<UserMenu> authorizeMenu, List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, int? examVersion, string keyword, string uniqueSetNo)
        {
            try
            {
                LoadAuthorizedList(authorizeMenu, organizationIdList, programIdList, sessionIdList, courseIdList);
                return _examMcqQuestionDao.GetExamMcqQuestionList(start, length, _authorizeProgramIdList, _authSessionIdList, _authCourseIdList, examVersion, keyword, uniqueSetNo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ExamMcqQuestion> LoadExamMcqQuestion(List<UserMenu> authorizeMenu, long examId, int uniqueSet, int version, int? questionSerial = null)
        {
            try
            {
              //  if (authorizeMenu == null) throw new InvalidDataException("invalid user permission");
                //List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(authorizeMenu);
                //List<long> programIdList = AuthHelper.LoadProgramIdList(authorizeMenu, organizationIdList, null, null);
                //List<long> branchIdList = AuthHelper.LoadBranchIdList(authorizeMenu, organizationIdList, null);

                return _examMcqQuestionDao.LoadExamMcqQuestion(examId, uniqueSet, version, questionSerial);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        
        #endregion

        #region Others Function

        private void LoadAuthorizedList(List<UserMenu> authorizeMenu, List<long> organizationIdList, List<long> programIdList,
           List<long> sessionIdList, List<long> courseIdList)
        {
            _authorizeOrganizationIdList = AuthHelper.LoadOrganizationIdList(authorizeMenu, organizationIdList);
            _authorizeProgramIdList = AuthHelper.LoadProgramIdList(authorizeMenu, _authorizeOrganizationIdList, null, programIdList);
            //load session
            _authSessionIdList = new List<long>();
            IList<Session> authSessionList = _sessionDao.LoadAuthorizedSession(_authorizeProgramIdList, null, null, null);
            if (sessionIdList != null && sessionIdList.Count > 0)
            {
                authSessionList =
                    authSessionList.Where(x => sessionIdList.Contains(x.Id)).ToList();
            }
            _authSessionIdList = authSessionList.Select(x => x.Id).ToList();
            //load course
            IList<Course> courseList = new List<Course>();
            courseList = _courseDao.LoadCourse(organizationIdList, programIdList, sessionIdList);
            _authCourseIdList = new List<long>();
            if (courseIdList != null && courseIdList.Count > 0)
            {
                courseList =
                    courseList.Where(x => courseIdList.Contains(x.Id)).ToList();
            }
            _authCourseIdList =
                    courseList.Select(x => x.Id).ToList();
        }

        public int GetExamMcqQuestionCount(List<UserMenu> authorizeMenu, List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, List<long> courseIdList, int? examVersion, string keyword, string uniqueSetNo)
        {
            try
            {
                LoadAuthorizedList(authorizeMenu, organizationIdList, programIdList, sessionIdList, courseIdList);
                return _examMcqQuestionDao.GetExamMcqQuestionCount(_authorizeProgramIdList, _authSessionIdList, _authCourseIdList, examVersion, keyword, uniqueSetNo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetLastQuestionSerial(List<UserMenu> authorizeMenu, long examId, int uniqueSet)
        {
            try
            {
                if (authorizeMenu == null) throw new InvalidDataException("invalid user permission");

                return _examMcqQuestionDao.GetLastQuestionSerial(examId, uniqueSet);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        private void UpdateExamMcqQuestionSet(long examId, int uniqueSet, int questionSerial, string correctAnswer)
        {
            List<ExamMcqQuestionSet> examMcqQuestionSetList = _examMcqQuestionSetDao.LoadExamMcqQuestionSet(examId, new List<int> {uniqueSet}, questionSerial).ToList();
            if (examMcqQuestionSetList.Any())
            {
                foreach (ExamMcqQuestionSet examMcqQuestionSet in examMcqQuestionSetList)
                {

                    bool hasMarks = _examsStudentMarksDao.ExamHasAlreayMarks(examId, examMcqQuestionSet.SetId);
                    if(hasMarks)
                        throw new InvalidDataException("Set Code (" + examMcqQuestionSet.SetId + ") has one or more marks. Please clear marks.");

                    string setCorrectAnswer = "";
                    if (!String.IsNullOrEmpty(examMcqQuestionSet.OptionA) && correctAnswer.ToUpper().Contains(examMcqQuestionSet.OptionA.ToUpper()))
                    {
                        setCorrectAnswer += "A";
                    }
                    if (!String.IsNullOrEmpty(examMcqQuestionSet.OptionB) && correctAnswer.ToUpper().Contains(examMcqQuestionSet.OptionB.ToUpper()))
                    {
                        setCorrectAnswer += "B";
                    }
                    if (!String.IsNullOrEmpty(examMcqQuestionSet.OptionC) && correctAnswer.ToUpper().Contains(examMcqQuestionSet.OptionC.ToUpper()))
                    {
                        setCorrectAnswer += "C";
                    }
                    if (!String.IsNullOrEmpty(examMcqQuestionSet.OptionD) && correctAnswer.ToUpper().Contains(examMcqQuestionSet.OptionD.ToUpper()))
                    {
                        setCorrectAnswer += "D";
                    }
                    if (!String.IsNullOrEmpty(examMcqQuestionSet.OptionE) && correctAnswer.ToUpper().Contains(examMcqQuestionSet.OptionE.ToUpper()))
                    {
                        setCorrectAnswer += "E";
                    }
                    examMcqQuestionSet.CorrectAnswer = setCorrectAnswer;
                    _examMcqQuestionSetDao.SaveOrUpdate(examMcqQuestionSet);
                }
            }
        }

        #endregion

        #region Helper Function

        private void CheckBeforeSaveOrUpdate(List<ExamMcqQuestion> examMcqQuestions)
        {
            if (examMcqQuestions == null)
                throw new NullObjectException("Question List is empty");
            if (!examMcqQuestions.Any())
                throw new NullObjectException("Question List is empty");


            Exams exam = examMcqQuestions.FirstOrDefault().Exams;

            if (exam == null)
            {
                throw new InvalidDataException("Invalid Exam");
            }

            //check eaxm already generate Set or Not
            foreach (ExamMcqQuestion examMcqQuestion in examMcqQuestions)
            {

                if (examMcqQuestion.Id <= 0)
                {
                    bool hasGenerateSet = _examMcqQuestionSetDao.HasExamGenerateSet(examMcqQuestion.Exams.Id, examMcqQuestion.UniqueSet);
                    if (hasGenerateSet)
                        throw new InvalidDataException("This exam's unique set (" + examMcqQuestion.UniqueSet.ToString() + ") already generated.");
                }
            }

            if (!exam.IsMcq)
                throw new InvalidDataException("Exam Type must be MCQ");

            if (exam.IsEnglishversion && exam.IsBanglaVersion)
            {
                if (examMcqQuestions.Count != 2)
                    throw new InvalidDataException("Exam And Question Version does not match.");
                if (!examMcqQuestions.Where(x => x.Version == (int)ExamVersion.Bangla).ToList().Any() || !examMcqQuestions.Where(x => x.Version == (int)ExamVersion.English).ToList().Any())
                    throw new InvalidDataException("Exam And Question Version does not match.");
                var optionCCount = examMcqQuestions.Where(x => string.IsNullOrEmpty(x.OptionC)).ToList().Count;
                if (optionCCount > 0 && optionCCount < examMcqQuestions.Count)
                    throw new InvalidDataException("Both Option C must be required.");

                var optionDCount = examMcqQuestions.Where(x => string.IsNullOrEmpty(x.OptionD)).ToList().Count;
                if (optionDCount > 0 && optionDCount < examMcqQuestions.Count)
                    throw new InvalidDataException("Both Option D must be required.");

            }
            else if (!exam.IsEnglishversion && exam.IsBanglaVersion)
            {
                if (examMcqQuestions.Count != 1)
                    throw new InvalidDataException("Exam And Question Version does not match.");
                if (!examMcqQuestions.Where(x => x.Version == (int)ExamVersion.Bangla).ToList().Any())
                    throw new InvalidDataException("Exam And Question Version does not match.");
            }
            else if (exam.IsEnglishversion && !exam.IsBanglaVersion)
            {
                if (examMcqQuestions.Count != 1)
                    throw new InvalidDataException("Exam And Question Version does not match.");
                if (!examMcqQuestions.Where(x => x.Version == (int)ExamVersion.English).ToList().Any())
                    throw new InvalidDataException("Exam And Question Version does not match.");
            }

            foreach (var emq in examMcqQuestions)
            {
                CheckValidation(emq);
            }
        }

        private void CheckValidation(ExamMcqQuestion examMcqQuestion)
        {
            var validationResult = ValidationHelper.ValidateEntity<ExamMcqQuestion, ExamMcqQuestion>(examMcqQuestion);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private void CheckBeforeUpdateOrDelete(ExamMcqQuestion questionObj)
        {
            try
            {
                //var examQuestionSetList = _examMcqQuestionSetDao.LoadExamMcqQuestionSet(questionObj.Exams.Id, questionObj.UniqueSet, questionObj.QuestionSerial);
                //if (examQuestionSetList != null && examQuestionSetList.Any())
                bool hasGenerateSet = _examMcqQuestionSetDao.HasExamGenerateSet(questionObj.Exams.Id, questionObj.UniqueSet);
                if (hasGenerateSet)
                    throw new DependencyException("This exam's unique set (" + questionObj.UniqueSet.ToString() + ") already generated.");
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion


    }

}
