﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IAttendanceSynchronizerSettingDao : IBaseDao<AttendanceSynchronizerSetting, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        AttendanceSynchronizerSetting GetSynchronizerSetting(string url, long id);
        #endregion

        #region List Loading Function

        List<AttendanceSynchronizerSetting> LoadSynchronizerSetting(int start, int length, string orderBy,
            string orderDir);

        #endregion

        #region Others Function
        bool SynchronizerVersionCheck(double syncVersion, long id);
        #endregion

        #region Helper Function

        #endregion

    }
    public class AttendanceSynchronizerSettingDao : BaseDao<AttendanceSynchronizerSetting, long>, IAttendanceSynchronizerSettingDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public AttendanceSynchronizerSetting GetSynchronizerSetting(string url, long id)
        {
            if (id > 0)
            {
                return Session.QueryOver<AttendanceSynchronizerSetting>().Where(x => x.Status !=
                    AttendanceSynchronizerSetting.EntityStatus.Delete && x.UpdateUrl == url && x.Id != id).SingleOrDefault<AttendanceSynchronizerSetting>();
            }
            return Session.QueryOver<AttendanceSynchronizerSetting>().Where(x => x.Status != AttendanceSynchronizerSetting.EntityStatus.Delete && x.UpdateUrl == url).SingleOrDefault<AttendanceSynchronizerSetting>();
        }

        #endregion

        #region List Loading Function
        public List<AttendanceSynchronizerSetting> LoadSynchronizerSetting(int start, int length, string orderBy, string orderDir)
        {
            var queryOver =
                Session.QueryOver<AttendanceSynchronizerSetting>()
                    .Where(x => x.Id != AttendanceSynchronizerSetting.EntityStatus.Delete);

            //ICriteria criteria = GetSynchronizerSettingCriteria();
            if (orderDir == "ASC")
            {
                //queryOver.UnderlyingCriteria.AddOrder(new Order(orderBy, true));
                queryOver.UnderlyingCriteria.AddOrder(Order.Asc(orderBy));
                //   criteria.AddOrder(Order.Asc(orderBy));
            }
            else
            {
                queryOver.UnderlyingCriteria.AddOrder(Order.Desc(orderBy));
            }
            return queryOver.List<AttendanceSynchronizerSetting>().Skip(0).Take(10).ToList();
            //var list = criteria.List<AttendanceSynchronizerSetting>();
            //return (List<AttendanceSynchronizerSetting>)criteria.SetFirstResult(start).SetMaxResults(length).List<AttendanceSynchronizerSetting>();
        }

        #endregion

        #region Others Function

        public bool SynchronizerVersionCheck(double syncVersion, long id)
        {
            if (id <= 0)
            {
                int count = Session.QueryOver<AttendanceSynchronizerSetting>().
                    Where(x => x.Status != AttendanceSynchronizerSetting.EntityStatus.Delete && x.SynchronizerVersion >= syncVersion).RowCount();
                if (count > 0)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        #endregion

        #region Helper Function
        private ICriteria GetSynchronizerSettingCriteria()
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceSynchronizerSetting>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", AttendanceSynchronizerSetting.EntityStatus.Delete)));
            return criteria;
        }
        #endregion
        
        
    }

}
