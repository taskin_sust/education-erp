﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.FestivalBonusSheetTest
{
    public class FestivalBonusSheetUpdateTest:FestivalBonusSheetBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Successfully_Update()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.Remarks = "Update";

            _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj });
            Assert.NotEqual(_festivalBonusSheetFactory.Object.Remarks, _festivalBonusSheetService.LoadById(obj.Id).Remarks);
        }

        [Fact]
        public void Should_Return_InvalidException_For_Null_object()
        {
            Assert.Throws<InvalidDataException>(
                () => _festivalBonusSheetService.SaveOrUpdate(null));
        }
        
        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_MessageException_For_Null_TeamMember()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.TeamMember = null;

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_JobBranch()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.JobBranch = null;

            Assert.Throws<MessageException>(() => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_JobCampus()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.JobCampus = null;

            Assert.Throws<MessageException>(() => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_JobDepartment()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.JobDepartment = null;

            Assert.Throws<MessageException>(
                 () => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_JobDesignation()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.JobDesignation = null;

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_JobOrganization()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.JobOrganization = null;

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_salaryBranch()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.SalaryBranch = null;

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_salaryCampus()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.SalaryCampus = null;

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_SalaryDepartment()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.SalaryDepartment = null;

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_SalaryDesignation()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.SalaryDesignation = null;

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_SalaryOrganization()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.SalaryOrganization = null;

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_FestivalBonusSetting()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Session.Evict(_festivalBonusSheetFactory.Object);
            var obj = _festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.Object.Id);
            obj.FestivalBonusSetting = null;

            Assert.Throws<MessageException>(() => _festivalBonusSheetService.SaveOrUpdate(new List<FestivalBonusSheet>() { obj }));
        }

        #endregion
        
    }
}
