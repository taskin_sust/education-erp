﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.DirectGoodsReceive
{
    public interface IDirectGoodsReceiveSaveOrUpdateTest
    {
        #region Basic Test

        void Should_Return_Branch_Null_Exception();
        void Should_Return_Supplier_Null_Exception();
        void Should_Return_Invalid_Data_Exception_For_TotalCost();
        void Should_Return_Item_Null_Exception();
        void Should_Return_Receive_Quantity_Exception();
        void Should_Return_Receive_Rate_Exception();
        void Should_Return_Save_Successfully();
        void Should_Return_Branch_Null_Exception_For_GetCurrentGoodsReceivedNoByBranchAndOrg();

        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReceive")]
    public class DirectGoodsReceiveSaveOrUpdateTest : DirectGoodsReceiveBaseTest, IDirectGoodsReceiveSaveOrUpdateTest
    {

        #region Basic Test

        [Fact]
        public void Should_Return_Branch_Null_Exception()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(_organizationFactory.Object, _commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), _branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(_branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();
            //receive
            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Object.Branch = null;
//            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            Assert.Throws<NullObjectException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        [Fact]
        public void Should_Return_Supplier_Null_Exception()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(_organizationFactory.Object, _commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), _branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(_branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();
            //receive
            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Object.Supplier = null;

            Assert.Throws<NullObjectException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        [Fact]
        public void Should_Return_Invalid_Data_Exception_For_TotalCost()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(_organizationFactory.Object, _commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), _branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(_branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();
            //receive
            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Object.TotalCost = -5;

            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        [Fact]
        public void Should_Return_Item_Null_Exception()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object).Object);
            Assert.Throws<NullObjectException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        [Fact]
        public void Should_Return_Receive_Quantity_Exception()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveDetailsFactory.Object.ReceivedQuantity = 0;
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        [Fact]
        public void Should_Return_Receive_Rate_Exception()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveDetailsFactory.Object.Rate = 0;
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        [Fact]
        public void Should_Return_Save_Successfully()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);
             Assert.Equal(_goodsReceiveFactory.Object.Id, _goodsReceiveService.GetGoodsReceive(_goodsReceiveFactory.Object.Id).Id);
        }

        [Fact]
        public void Should_Return_Branch_Null_Exception_For_GetCurrentGoodsReceivedNoByBranchAndOrg()
        {
            Assert.Throws<NullObjectException>(() => _goodsReceiveService.GetCurrentGoodsReceivedNoByBranchAndOrg(null));
        }

        #endregion
    }
}
