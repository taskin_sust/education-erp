﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DeviceSynchronizer.ApiReferences;
using DeviceSynchronizer.Dao;
using DeviceSynchronizer.DeviceHelper;
using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model;
using DeviceSynchronizer.Model.ApiModel;
using DeviceSynchronizer.Model.BusinessModel;
using log4net;
using Newtonsoft.Json;

namespace DeviceSynchronizer.Services
{
    public class DeviceManagement
    {
        #region Declaration

        private static readonly ILog Log = LogManager.GetLogger("ServiceLogger");
        private readonly AttendanceDeviceModel _attendanceDevice;
        private readonly IAttendanceDeviceDriver _attendanceDeviceDriver;

        #endregion

        #region Constructor

        public DeviceManagement(AttendanceDeviceModel attendanceDevice, IAttendanceDeviceDriver attendanceDeviceDriver)
        {
            this._attendanceDevice = attendanceDevice;
            _attendanceDeviceDriver = attendanceDeviceDriver;

        }

        #endregion

        #region Helper

        public DateTime? GetCurrentTime()
        {
            var apiReq = new SyncTeamMemberInfoApiRequest();
            return apiReq.GetCurrentDateTime();
        }

        #endregion

        #region Operation

        //Prepare device before operation
        // clean Device hardware
        internal bool ReadyDevice()
        {
            try
            {
                _attendanceDeviceDriver.SetTime(GetCurrentTime);
                if (_attendanceDevice.IsReset)
                {
                    _attendanceDeviceDriver.Clean();
                    _attendanceDeviceDriver.RefreshData();
                }
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return false;
            }
        }

        #endregion
    }
}
