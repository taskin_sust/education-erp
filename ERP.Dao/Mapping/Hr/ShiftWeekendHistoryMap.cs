using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class ShiftWeekendHistoryMap : BaseClassMap<ShiftWeekendHistory,long> {
        
        public ShiftWeekendHistoryMap() {
			Table("HR_ShiftWeekendHistory");
			LazyLoad();
			//References(x => x.Organization).Cascade.All().Column("OrganizationId");old
            References(x => x.Organization).Column("OrganizationId");//new
			References(x => x.Shift).Column("ShiftId");
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.Weekend).Column("Weekend");
            Map(x => x.EffectiveDate).Column("EffectiveDate");
			//HasMany(x => x.ShiftWeekendHistoryLog).KeyColumn("ShiftWeekendHistoryId");old
        }
    }
}
