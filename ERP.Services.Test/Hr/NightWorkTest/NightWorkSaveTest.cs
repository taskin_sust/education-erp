﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Testing.Values;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.NightWorkTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "NightWorkService")]
    public class NightWorkSaveTest : NightWorkBaseTest
    {
        #region Basic Test

        [Fact]
        public void NightWork_Should_Save_Successful()
        {
            var teamMember = PersistTeamMember(MentorId);
            _nightWorkFactory.CreateWith(new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day),1 ).WithTeamMember(teamMember).Persist();
            var list = _nightWorkService.LoadNightWork(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),teamMember.Id);
            Assert.NotEmpty(list);  
        }

        [Fact]
        public void NightWork_Should_Save_By_NightWork_FullValues()
        {
            var teamMember = PersistTeamMember(MentorId);
            var mentorTeamMemberIdList = _mentorHistoryService.LoadMentorTeamMemberId(new List<long> { MentorId }, DateTime.Now, null);
            var fullValues = "";
            var halfValues = "2";
            var noValues = "";
            var inValues = DateTime.Now;
            var outValues = DateTime.Now.AddHours(3);
            _nightWorkFactory.Create().WithTeamMember(teamMember);
            _nightWorkService.Save(_nightWorkFactory.Object.NightWorkDate.ToString(), _nightWorkFactory.Object.TeamMember.Pin.ToString(), fullValues, halfValues, noValues, inValues.ToString(), outValues.ToString());
            var list = _nightWorkService.LoadNightWork(DateTime.Now, DateTime.Now.AddHours(2), mentorTeamMemberIdList,
               _nightWorkFactory.Object.TeamMember.Pin.ToString(), null, null, null, null, null, 0, 0, null, null);
            Assert.NotNull(list); 
        } 

        #endregion

        #region Business Logic

        [Fact]
        public void NightWork_Should_Save_By_NightWork_HalfValues()
        {
            var teamMember = PersistTeamMember(MentorId);
            var mentorTeamMemberIdList = _mentorHistoryService.LoadMentorTeamMemberId(new List<long> { MentorId }, DateTime.Now, null);
            var fullValues = "";
            var halfValues = "2";
            var noValues = "";
            var inValues = DateTime.Now;
            var outValues = DateTime.Now.AddHours(3);
            _nightWorkFactory.Create().WithTeamMember(teamMember);
            _nightWorkService.Save(_nightWorkFactory.Object.NightWorkDate.ToString(), _nightWorkFactory.Object.TeamMember.Pin.ToString(), fullValues, halfValues, noValues, inValues.ToString(), outValues.ToString());
            var list = _nightWorkService.LoadNightWork(DateTime.Now, DateTime.Now.AddHours(2), mentorTeamMemberIdList,
               _nightWorkFactory.Object.TeamMember.Pin.ToString(), null, null, null, null, null, 0, 0, null, null);
            Assert.NotNull(list); 
        }
        
        [Fact]
        public void InvalidDataException_For_NightWork_For_TeamMember()
        {
            var teamMember = PersistTeamMember(MentorId);
            var fullValues = "1";
            var halfValues = "";
            var noValues = "";
            var inValues = DateTime.Now;
            var outValues = DateTime.Now.AddHours(3);
            _nightWorkFactory.Create().WithTeamMember(teamMember);
            Assert.Throws<NullObjectException>(() => _nightWorkService.Save(_nightWorkFactory.Object.NightWorkDate.ToString(), null, fullValues, halfValues, noValues, inValues.ToString(), outValues.ToString()));
        }

        [Fact]
        public void InvalidDataException_For_NightWork_For_FullValues()
        {
            var teamMember = PersistTeamMember(MentorId);
            var fullValues = "1";
            var halfValues = "";
            var noValues = "";
            var inValues = DateTime.Now;
            var outValues = DateTime.Now.AddHours(3);
            _nightWorkFactory.Create().WithTeamMember(teamMember);
            Assert.Throws<NullReferenceException>(() => _nightWorkService.Save(_nightWorkFactory.Object.NightWorkDate.ToString(), _nightWorkFactory.Object.TeamMember.Pin.ToString(), null, halfValues, noValues, inValues.ToString(), outValues.ToString()));
        }

        [Fact]
        public void InvalidDataException_For_NightWork_For_HalfValues()
        {
            var teamMember = PersistTeamMember(MentorId);
            var fullValues = "1";
            var halfValues = "";
            var noValues = "";
            var inValues = DateTime.Now;
            var outValues = DateTime.Now.AddHours(3);
            _nightWorkFactory.Create().WithTeamMember(teamMember);
            Assert.Throws<NullReferenceException>(() => _nightWorkService.Save(_nightWorkFactory.Object.NightWorkDate.ToString(), _nightWorkFactory.Object.TeamMember.Pin.ToString(), fullValues, null, noValues, inValues.ToString(), outValues.ToString()));
        }

        [Fact]
        public void InvalidDataException_For_NightWork_For_NoValues()
        {
            var teamMember = PersistTeamMember(MentorId);
            var fullValues = "1";
            var halfValues = "";
            var noValues = "";
            var inValues = DateTime.Now;
            var outValues = DateTime.Now.AddHours(3);
            _nightWorkFactory.Create().WithTeamMember(teamMember);
            Assert.Throws<NullReferenceException>(() => _nightWorkService.Save(_nightWorkFactory.Object.NightWorkDate.ToString(), _nightWorkFactory.Object.TeamMember.Pin.ToString(), fullValues, halfValues, null, inValues.ToString(), outValues.ToString()));
        }


        #endregion
    }
}
