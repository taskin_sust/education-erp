﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class NomineeInfoFactory : ObjectFactoryBase<NomineeInfo>
    {
        private readonly INomineeInfoService _nomineeInfoService;
        private TeamMemberFactory _teamMemberFactory;
        public NomineeInfoFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _nomineeInfoService = new NomineeInfoService(session);
            _teamMemberFactory = new TeamMemberFactory(this, session);
        }
       
        #region Create

        public NomineeInfoFactory Create()
        {
            Object = new NomineeInfo()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                MobileNo = "8801811419555",
                Relation = "NIL"
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public NomineeInfoFactory CreateWith(string mobileNo, string relation)
        {
            Object = new NomineeInfo()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                MobileNo = mobileNo,
                Relation = relation
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public NomineeInfoFactory WithTeamMember()
        {
            _teamMemberFactory.Create().Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }
        public NomineeInfoFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        #endregion

        #region Others

        #endregion

        #region CleanUp

        public NomineeInfoFactory CleanUp()
        {
            DeleteAll();
            _teamMemberFactory.Cleanup();
            return this;
        }
        #endregion

    }

}
