﻿
/*Written BY Taskin*/

$(document).ready(function () {
    var PAYMENT_ERROR_MESSAGE = '<p class="errorMsg" style="color: #F00"> Field Value Invalid </p>';
    var SUCCESS_MESS = "Program Assign Successfully";
    var ERROR_MESS = "Program Assign Failed";
    var UPDATE_MESS = "Program Assign Update Successfully";
    var UPDATE_ERROR_MESS = "Program Assign Update Failed";
    var COMMON_ERROR = "Failed";
    var AJAX_ERROR = "Slow Internet Connection";

    /* CHECKBOX SELECTION */
    $(document).on('click', '#allchecked', function () {

        if ($('#allchecked').is(':checked')) {
            $('#checkbox .checkbox-inline').each(function (index, value) {
                $(this).prop('checked', true);
            });
        } else {
            $('#checkbox .checkbox-inline').each(function (index, value) {
                $(this).prop('checked', false);
            });
        }
    });

    //$(document).on('change', '#Branch', function () {
    //    var branchId = $('#Branch').val();
    //    $('.removeThisClass').empty();
    //    $.ajax({
    //        url: "/Administration/Program/GetSession",
    //        type: "POST",
    //        data: { branchId: branchId },
    //        beforeSend: function () {
    //            $.blockUI({
    //                timeout: 0,
    //                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //            });
    //        },
    //        success: function (data) {
    //            $('.sessionListGenerate').html(data);
    //            $.unblockUI();
    //        },
    //        complete: function () {
    //            $.unblockUI();
    //        },
    //        error: function (data) {
    //            $.fn.customMessage({
    //                displayMessage: AJAX_ERROR,
    //                displayMessageType: "error",
    //                displayTime: 5000
    //            });
    //        }
    //    });
    //});

    /* IF YOU CHANGE BRANCH OR SESSION VALUE FROM DROPDOWN THEN FIND THE ASSOCIATED PROGRAM BY THIS PIECE OF CODE*/
    function setProgram() {
        var branchId = $('#Branch').val();
        console.log(branchId);
        //var branchId = [];
        //$('#Branch :selected').each(function (i, selected) {
        //    branchId[i] = $(selected).text();
        //});

        var sessionId = $('#Session').val();
        if (branchId.length <= 0 || sessionId.length <= 0) { $('.removeThisClass').empty(); }
        if (branchId.length > 0 && sessionId.length > 0) {
            $.ajax({
                url: "/Administration/Program/GetProgramBySessionAndBranch",
                type: "POST",
                data: { branchId: branchId, sessionId: sessionId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    //alert(data);
                    $('#allProgramList').html(data);
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: AJAX_ERROR,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
    }
    $(document).on('change', '#Branch, #Session', function () {
        //alert();
        setProgram();
    });

    /* NEW PROGRAM WILL BE ASSIGNED FROM THIS PIECE OF CODE */
    $(document).on('click', '#programAssign', function () {
        var ii = 0;
        var programList = "";
        $('#checkbox .checkbox-inline').each(function (index, value) {
            if ($(this).is(':checked')) {
                programList += $(this).attr('Id') + ",";
            }
        });
        var branchId = $('#Branch').val();
        var sessionId = $('#Session').val();
        var program = JSON.stringify(programList);
        $.ajax({
            url: "/Administration/Program/AssignProgramPost",
            type: "POST",
            data: { programList: program, branchId: branchId, sessionId: sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                if (data.ErrorMessage != null) {
                    $.fn.customMessage({
                        displayMessage: data.ErrorMessage,
                        displayMessageType: "e",
                    });
                }  
                if (data.WarningMessage != null) {
                    $.fn.customMessage({
                        displayMessage: data.WarningMessage,
                        displayMessageType: "w",
                    });
                } 
                if (data.SuccessMessage != null) {
                    $.fn.customMessage({
                        displayMessage: data.SuccessMessage,
                        displayMessageType: "s",
                    });
                }
                setProgram();
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                $.fn.customMessage({
                    displayMessage: AJAX_ERROR,
                    displayMessageType: "error",
                    displayTime: 5000
                });
            }
        });
    });
});