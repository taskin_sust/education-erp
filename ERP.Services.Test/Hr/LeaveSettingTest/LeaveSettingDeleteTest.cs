﻿using System;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.LeaveSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "LeaveSettings")]
    public class LeaveSettingDeleteTest : LeaveSettingBaseTest
    {
        [Fact]
        public void Delete_Should_Return_ValueNotAssignedException_Exception()
        {
            var invalidId = -1;
            Assert.Throws<ValueNotAssignedException>(() => _leaveService.Delete(invalidId));
        }
        

        [Fact]
        public void Should_Delete_Successfully()
        {
            //GenerateLeave();
            leaveFactory.Create().WithOrganization().Persist();
            leaveFactory.Object.EffectiveDate = DateTime.Now.AddDays(1);
            Assert.True(_leaveService.Delete(leaveFactory.Object.Id));
        }

    }
}
