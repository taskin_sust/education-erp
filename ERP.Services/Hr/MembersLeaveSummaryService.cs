﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{

    public interface IMembersLeaveSummaryService : IBaseService
    {
        #region Operational Function

        List<String> SaveAndUpdateIndividualLeave(List<MemberLeaveSummary> addList, List<MemberLeaveSummary> updateList);
        void AssignMemberYearlyLeaveSummary(List<UserMenu> userMenu, MemberYearlyLeaveSummaryAssignFormViewModel memberYearlyLeaveSummaryFormViewModel);

        #endregion

        #region Single Instances Loading Function

        MemberLeaveSummary GetLeaveSummary(long orgId, long memId, long leaveId);
        IList<MemberLeaveSummary> GetLeaveSummary(long orgId, long memId, int year);
        MemberLeaveSummary GetLeaveSummary(long orgIdp, long memId, long leaveId, int year);

        #endregion

        #region List Loading Function

        IList<MemberLeaveSummary> LoadByMemberAndYear(long memId, int year);
        IList<TeamMemberTransferLeaveDetailsViewModel> LoadMemberLeaveSumarryForTransfer(long memId, int year);
        List<MemberLeaveSummary> LoadMemberLeaveSummaryByOrgLeaveMemberYear(int year, long organizationId, List<long> teamMemberIdList, List<long> leaveIdList);

        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion
        
    }
    public class MembersLeaveSummaryService : BaseService, IMembersLeaveSummaryService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        
        #region Propertise & Object Initialization

        private readonly IMembersLeaveSummaryDao _hrMembersLeaveSummaryDao;
        private readonly ICommonHelper _commonHelper;
        private readonly ITeamMemberDao _teamMemberDao;
        private readonly ILeaveDao _hrLeaveDao;
        private readonly IEmploymentHistoryDao _employmentHistoryDao;
        private readonly IMaritalInfoDao _maritalInfoDao;
        private readonly ILeaveApplicationDao _leaveApplicationDao;

        public MembersLeaveSummaryService(ISession session)
        {
            Session = session;
            _hrMembersLeaveSummaryDao = new MembersLeaveSummaryDao() { Session = session };
            _teamMemberDao = new TeamMemberDao { Session = session };
            _hrLeaveDao = new LeaveDao { Session = session };
            _employmentHistoryDao = new EmploymentHistoryDao { Session = session };
            _maritalInfoDao = new MaritalInfoDao { Session = session };
            _leaveApplicationDao = new LeaveApplicationDao { Session = session };
            _commonHelper = new CommonHelper();

        }

        #endregion

        #region Operational Functions
        
        public List<String> SaveAndUpdateIndividualLeave(List<MemberLeaveSummary> addList, List<MemberLeaveSummary> updateList)
        {
            ITransaction trans;
            List<String> errorList = new List<string>();

            using (trans = Session.BeginTransaction())
            {
                try
                {
                    if (addList != null && EnumerableExtensions.Any(addList))
                    {
                        foreach (var hrMemberLeaveSummary in addList)
                        {
                            int? maxLimitt;
                            var totalLeaveBalance = (hrMemberLeaveSummary.CurrentYearLeave +
                                                     hrMemberLeaveSummary.CarryForwardLeave) -
                                                    hrMemberLeaveSummary.EncashLeave;
                            if (hrMemberLeaveSummary.Leave == null)
                                throw new InvalidDataException("Invalid Leave");

                            if (hrMemberLeaveSummary.TeamMember == null)
                                throw new InvalidDataException("Invalid TeamMember");

                            if (hrMemberLeaveSummary.Leave.IsPublic == false ||
                                hrMemberLeaveSummary.Leave.PayType == (int) PayType.WithoutPay)
                                continue;
                            if (hrMemberLeaveSummary.Leave.IsCarryforward == true)
                            {
                                maxLimitt = hrMemberLeaveSummary.Leave.MaxCarryDays +
                                            hrMemberLeaveSummary.Leave.NoOfDays;
                            }
                            else
                            {
                                maxLimitt = hrMemberLeaveSummary.Leave.NoOfDays;
                            }

                            if (maxLimitt < hrMemberLeaveSummary.AvailableBalance)
                            {
                                errorList.Add(" Maximum no. Of Days for Leave " + hrMemberLeaveSummary.Leave.Name +
                                              " is" + maxLimitt);
                            }
                            else if (totalLeaveBalance <
                                     (hrMemberLeaveSummary.ApprovedLeave + hrMemberLeaveSummary.PendingLeave))
                                errorList.Add(
                                    "Total balance must be grater then or equal to the sum of taken and pending leave" +
                                    " for leave " + hrMemberLeaveSummary.Leave.Name);

                            else if (hrMemberLeaveSummary.Leave != null &&
                                     hrMemberLeaveSummary.Leave.IsEncash == false &&
                                     hrMemberLeaveSummary.EncashLeave > 0)
                                errorList.Add("Encash is not possible for leave " +
                                              hrMemberLeaveSummary.Leave.Name);

                                //else if (hrMemberLeaveSummary.Leave.IsEncash == true && hrMemberLeaveSummary.EncashLeave > 0 &&
                                //         (hrMemberLeaveSummary.CarryForwardLeave - hrMemberLeaveSummary.Leave.MinEncashReserveDays) <
                                //         (hrMemberLeaveSummary.EncashLeave + hrMemberLeaveSummary.VoidLeave))
                                //{
                                //    errorList.Add("Encash or void calculation is not correct for leave " + hrMemberLeaveSummary.Leave.Name);
                                //}

                            else if (hrMemberLeaveSummary.Leave.IsEncash == true &&
                                     hrMemberLeaveSummary.EncashLeave > 0 &&
                                     hrMemberLeaveSummary.CarryForwardLeave <
                                     hrMemberLeaveSummary.Leave.MinEncashReserveDays)
                            {
                                errorList.Add("Carryforoward must be greater then or equal to " +
                                              hrMemberLeaveSummary.Leave.MinEncashReserveDays +
                                              " for leave " + hrMemberLeaveSummary.Leave.Name);
                            }
                            else if (hrMemberLeaveSummary.Leave.IsCarryforward == false &&
                                     hrMemberLeaveSummary.CarryForwardLeave > 0)
                                errorList.Add("Carry forward is not available for leave " +
                                              hrMemberLeaveSummary.Leave.Name);
                            else
                            {
                                _hrMembersLeaveSummaryDao.Save(hrMemberLeaveSummary);
                            }
                        }
                    }
                    if (updateList != null && EnumerableExtensions.Any(updateList))
                    {
                        foreach (var hrMemberLeaveSummary in updateList)
                        {
                            int? maxLimitt;
                            var totalLeaveBalance = (hrMemberLeaveSummary.CurrentYearLeave +
                                                     hrMemberLeaveSummary.CarryForwardLeave) -
                                                    hrMemberLeaveSummary.EncashLeave;
                            if (hrMemberLeaveSummary.Leave == null)
                                throw new InvalidDataException("Invalid Leave");

                            if (hrMemberLeaveSummary.TeamMember == null)
                                throw new InvalidDataException("Invalid TeamMember");

                            if (hrMemberLeaveSummary.Leave.IsPublic == false ||
                                hrMemberLeaveSummary.Leave.PayType == (int) PayType.WithoutPay)
                                continue;

                            if (hrMemberLeaveSummary.Leave.IsCarryforward == true)
                            {
                                maxLimitt = hrMemberLeaveSummary.Leave.MaxCarryDays +
                                            hrMemberLeaveSummary.Leave.NoOfDays;
                            }
                            else
                            {
                                maxLimitt = hrMemberLeaveSummary.Leave.NoOfDays;
                            }

                            if (maxLimitt < hrMemberLeaveSummary.AvailableBalance)
                            {
                                errorList.Add(" Maximum no. Of Days for Leave " + hrMemberLeaveSummary.Leave.Name +
                                              " is" + maxLimitt);
                            }
                            else if (totalLeaveBalance <
                                     (hrMemberLeaveSummary.ApprovedLeave + hrMemberLeaveSummary.PendingLeave))
                                errorList.Add(
                                    "Total balance must be grater then or equal to the sum of taken and pending leave" +
                                    " for leave " + hrMemberLeaveSummary.Leave.Name);

                            else if (hrMemberLeaveSummary.Leave != null &&
                                     hrMemberLeaveSummary.Leave.IsEncash == false &&
                                     hrMemberLeaveSummary.EncashLeave > 0)
                                errorList.Add("Encash is not possible for leave " +
                                              hrMemberLeaveSummary.Leave.Name);

                            //else if (hrMemberLeaveSummary.Leave.IsEncash == true &&
                            //         hrMemberLeaveSummary.EncashLeave > 0 &&
                            //         (hrMemberLeaveSummary.CarryForwardLeave -
                            //          hrMemberLeaveSummary.Leave.MinEncashReserveDays) <
                            //         (hrMemberLeaveSummary.EncashLeave + hrMemberLeaveSummary.VoidLeave))
                            //{
                            //    errorList.Add("Encash or void calculation is not correct for leave " +
                            //                  hrMemberLeaveSummary.Leave.Name);
                            //}

                            else if (hrMemberLeaveSummary.Leave.IsEncash == true &&
                                     hrMemberLeaveSummary.EncashLeave > 0 &&
                                     hrMemberLeaveSummary.CarryForwardLeave <
                                     hrMemberLeaveSummary.Leave.MinEncashReserveDays)
                            {
                                errorList.Add("Carryforoward must be greater then or equal to " +
                                              hrMemberLeaveSummary.Leave.MinEncashReserveDays +
                                              " for leave " + hrMemberLeaveSummary.Leave.Name);
                            }
                            else if (hrMemberLeaveSummary.Leave.IsCarryforward == false &&
                                     hrMemberLeaveSummary.CarryForwardLeave > 0)
                                errorList.Add("Carry forward is not available for leave " +
                                              hrMemberLeaveSummary.Leave.Name);

                            else
                            {
                                _hrMembersLeaveSummaryDao.Update(hrMemberLeaveSummary);
                            }
                        }
                    }
                    trans.Commit();

                }
                catch (InvalidDataException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.Error(ex);
                    // throw;
                }
            }
            return errorList;
        }

        public void AssignMemberYearlyLeaveSummary(List<UserMenu> userMenu, MemberYearlyLeaveSummaryAssignFormViewModel memberYearlyLeaveSummaryFormViewModel)
        {
            ITransaction trans = null;
            try
            {
                List<MemberLeaveSummary> memberLeaveSummaryList = new List<MemberLeaveSummary>();
                List<int> pinList = null;

                string pinListString = (!String.IsNullOrEmpty(memberYearlyLeaveSummaryFormViewModel.PinList))
                    ? memberYearlyLeaveSummaryFormViewModel.PinList.Trim()
                    : "";
                Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                pinListString = regex.Replace(pinListString, @",");
                if (!String.IsNullOrEmpty(pinListString))
                {
                    pinList = pinListString.Split(',').Select(Int32.Parse).ToList();
                }

                List<long> authOrgIdList = AuthHelper.LoadOrganizationIdList(userMenu, (memberYearlyLeaveSummaryFormViewModel.OrganizationId != SelectionType.SelelectAll)
                        ? _commonHelper.ConvertIdToList(memberYearlyLeaveSummaryFormViewModel.OrganizationId)
                        : null);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrgIdList, null,
                    (memberYearlyLeaveSummaryFormViewModel.BranchId != SelectionType.SelelectAll)
                        ? _commonHelper.ConvertIdToList(memberYearlyLeaveSummaryFormViewModel.BranchId)
                        : null);
                DateTime searchingDate = new DateTime(memberYearlyLeaveSummaryFormViewModel.ClosingYear + 1, 01, 1, 23,
                    59, 59);

                List<TeamMember> teamMemberList =
                    _teamMemberDao.LoadHrAuthorizedTeamMember(searchingDate, authBranchIdList, null, null, null, pinList,
                        false).ToList();

                if (!teamMemberList.Any())
                {
                    throw new InvalidDataException("No TeamMember Found To Operate This Operation");
                }
                foreach (TeamMember teamMember in teamMemberList)
                {
                    bool isPendingLeave = _leaveApplicationDao.HasPendingLeaveApplication(searchingDate.Year - 1,
                        teamMember.Id);
                    if (isPendingLeave)
                        throw new InvalidDataException("TeamMember has one or more pending leave application for Year " + (searchingDate.Year - 1).ToString() + " for PIN (" + teamMember.Pin + ").");
                    bool hasLeaveSummary = _hrMembersLeaveSummaryDao.HasTeamMeberHasAnyLeave(teamMember.Id,
                        searchingDate.Year);
                    // already has a leve Summary then skip
                    if (!hasLeaveSummary)
                    {
                        Organization currentOrganization = _teamMemberDao.GetTeamMemberOrganization(searchingDate, teamMember.Id, teamMember.Pin);
                        EmploymentHistory currentEmploymentHistory = _employmentHistoryDao.GetTeamMemberEmploymentHistory(searchingDate, teamMember.Id, teamMember.Pin);
                        int currentMaritalStatus = _maritalInfoDao.GetCurrentMaritalStatusForLeave(teamMember.Id);
                        IList<Leave> leaveTypesList = _hrLeaveDao.LoadLeave(currentOrganization.Id, teamMember.Gender, currentEmploymentHistory.EmploymentStatus, currentMaritalStatus);
                        List<MemberLeaveSummary> lastYearMemberLeaveSummaryList = _hrMembersLeaveSummaryDao.LoadByMemberAndYear(teamMember.Id, searchingDate.Year - 1).ToList();
                        DateTime joiningDate = _employmentHistoryDao.GetTeamMemberJoiningEmploymentHistory(teamMember.Id).EffectiveDate;
                        EmploymentHistory parmanentEmploymentHistory = _employmentHistoryDao.GetTeamMemberPermanentEmploymentHistory(teamMember.Id);

                        if (leaveTypesList.Any())
                        {

                            foreach (Leave leave in leaveTypesList)
                            {
                                int carryForwardLeave = 0;
                                int encashLeave = 0;
                                int voidLeave = 0;
                                bool applyLeaveToProfile = true;
                                if (leave.StartFrom == (int)StartingPoint.Joining)
                                {

                                }
                                else if (leave.StartFrom == (int)StartingPoint.Permanent)
                                {
                                    if (parmanentEmploymentHistory == null)
                                    {
                                        applyLeaveToProfile = false;
                                    }
                                    else if (parmanentEmploymentHistory.EffectiveDate.Date > searchingDate.Date)
                                    {
                                        applyLeaveToProfile = false;
                                    }
                                }
                                else if (leave.StartFrom == (int)StartingPoint.One_Year_After_Permanent)
                                {
                                    if (parmanentEmploymentHistory == null)
                                    {
                                        applyLeaveToProfile = false;
                                    }
                                    else if (parmanentEmploymentHistory.EffectiveDate.AddYears(1).Date > searchingDate.Date)
                                    {
                                        applyLeaveToProfile = false;
                                    }
                                }

                                if (leave.RepeatType == (int)RepeatType.MaxTwo && applyLeaveToProfile == true)
                                {
                                    int leaveCount = _leaveApplicationDao.GetTotalLeaveApplicationCountForRepeatTypeLeave(leave.Id, teamMember.Id);
                                    if (leaveCount >= 2)
                                        applyLeaveToProfile = false;
                                }
                                if (applyLeaveToProfile)
                                {

                                    #region LastyearLeaveCalculation

                                    if (lastYearMemberLeaveSummaryList.Any() && leave.PayType == (int)PayType.WithPay &&
                                        leave.IsPublic == true)
                                    {
                                        MemberLeaveSummary lastYearLeaveSummary =
                                            lastYearMemberLeaveSummaryList.Where(
                                                x =>
                                                    x.Leave.Id == leave.Id &&
                                                    x.Organization.Id == currentOrganization.Id)
                                                .Take(1)
                                                .SingleOrDefault();
                                        if (lastYearLeaveSummary != null)
                                        {
                                            #region Carry Forward Leave

                                            if (leave.IsCarryforward == true)
                                            {
                                                if (leave.MaxCarryDays != null && leave.MaxCarryDays < (lastYearLeaveSummary.AvailableBalance + leave.NoOfDays.Value))
                                                {
                                                    voidLeave = (lastYearLeaveSummary.AvailableBalance + leave.NoOfDays.Value) - leave.MaxCarryDays.Value;
                                                    carryForwardLeave = lastYearLeaveSummary.AvailableBalance - voidLeave;
                                                }
                                                else
                                                {
                                                    carryForwardLeave = lastYearLeaveSummary.AvailableBalance;
                                                }
                                            }

                                            #endregion

                                            #region Void Leave

                                            if (leave.IsCarryforward == null || leave.IsCarryforward == false)
                                            {
                                                // Onece and Two Time Leave should Not Be Void
                                                if (leave.RepeatType == (int)RepeatType.Once ||
                                                    leave.RepeatType == (int)RepeatType.MaxTwo)
                                                    voidLeave = 0;
                                                else
                                                    voidLeave = lastYearLeaveSummary.AvailableBalance;
                                            }

                                            #endregion

                                            #region Encash

                                            //Encash Leave Has no Void 
                                            if (leave.IsEncash == true)
                                            {
                                                if (leave.MinEncashReserveDays != null &&
                                                    leave.MinEncashReserveDays < lastYearLeaveSummary.AvailableBalance)
                                                {
                                                    carryForwardLeave = leave.MinEncashReserveDays.Value;
                                                    encashLeave = lastYearLeaveSummary.AvailableBalance -
                                                                  leave.MinEncashReserveDays.Value;
                                                }
                                                voidLeave = 0;
                                            }

                                            #endregion
                                        }
                                    }

                                    #endregion

                                    #region leaveSummary Assign
                                    MemberLeaveSummary newSummary = new MemberLeaveSummary
                                    {
                                        //Name = hrLeav.Name,
                                        Leave = leave,
                                        TeamMember = teamMember,
                                        Organization = currentOrganization,
                                        CarryForwardLeave = carryForwardLeave,
                                        CurrentYearLeave = leave.NoOfDays.Value,
                                        EncashLeave = encashLeave,
                                        VoidLeave = voidLeave,
                                        TotalLeaveBalance = leave.NoOfDays.Value + carryForwardLeave,
                                        ApprovedLeave = 0,
                                        PendingLeave = 0,
                                        AvailableBalance = leave.NoOfDays.Value + carryForwardLeave,
                                        Year = searchingDate.Year
                                    };
                                    memberLeaveSummaryList.Add(newSummary);
                                    #endregion

                                }
                            }
                        }
                    }
                }

                if (!memberLeaveSummaryList.Any())
                    throw new InvalidDataException("No TeamMember has any Leave Summary to assign for Year " + searchingDate.Year + ".");


                using (trans = Session.BeginTransaction())
                {
                    foreach (MemberLeaveSummary memberLeaveSummary in memberLeaveSummaryList)
                    {
                        _hrMembersLeaveSummaryDao.Save(memberLeaveSummary);
                    }
                    trans.Commit();
                }

            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if(trans!=null && trans.IsActive)
                    trans.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public MemberLeaveSummary GetLeaveSummary(long orgId, long memId, long leaveId)
        {
            try
            {
                return _hrMembersLeaveSummaryDao.GetLeaveSummary(orgId, memId, leaveId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<MemberLeaveSummary> GetLeaveSummary(long orgId, long memId, int year)
        {
            try
            {
                return _hrMembersLeaveSummaryDao.GetLeaveSummary(orgId, memId, year);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public MemberLeaveSummary GetLeaveSummary(long orgIdp, long memId, long leaveId, int year)
        {
            try
            {
                return _hrMembersLeaveSummaryDao.GetLeaveSummary(orgIdp, memId, leaveId, year);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }

        #endregion

        #region List Loading Function

        public IList<MemberLeaveSummary> LoadByMemberAndYear(long memId, int year)
        {
            try
            {
                return _hrMembersLeaveSummaryDao.LoadByMemberAndYear(memId, year);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<TeamMemberTransferLeaveDetailsViewModel> LoadMemberLeaveSumarryForTransfer(long memId, int year)
        {
            try
            {
                return _hrMembersLeaveSummaryDao.LoadMemberLeaveSumarryForTransfer(memId, year);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<MemberLeaveSummary> LoadMemberLeaveSummaryByOrgLeaveMemberYear(int year, long organizationId, List<long> teamMemberIdList, List<long> leaveIdList)
        {
            try
            {
                return _hrMembersLeaveSummaryDao.LoadMemberLeaveSummaryByOrgLeaveMemberYear(year, organizationId, teamMemberIdList,leaveIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        
        #endregion

        #region Helper function
        #endregion
    }
}
