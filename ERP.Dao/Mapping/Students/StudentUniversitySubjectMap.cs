﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentUniversitySubjectMap : BaseClassMap<StudentUniversitySubject,long>
    {
        public StudentUniversitySubjectMap()
        {
            Table("StudentUniversitySubject");
            LazyLoad();
            Map(x => x.Name);

        }
    }
}
