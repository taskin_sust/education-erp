﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.Administration.CourseTest
{
    public class CourseBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        private ISession _session;
        internal CourseService _courseService;
        internal ProgramService _programService;
        internal SessionService _sessionService;
        internal OrganizationService _organizationService;
        internal SubjectService _subjectService; 
        internal ProgramSessionSubjectService _programSessionSubjectService; 
        internal TestBaseService<ProgramSessionSubject> _TestBaseService; 

        internal CourseFactory _courseFactory;
        internal CourseSubjectFactory _courseSubjectFactory;
        internal ProgramFactory _programFactory;
        internal SessionFactory _sessionFactory;
        internal SubjectFactory _subjectFactory; 

        internal readonly string _name = "Course_" + Guid.NewGuid().ToString().Substring(0, 6);

        public CourseBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _courseService = new CourseService(_session);
            _courseFactory = new CourseFactory(null,_session);
            _programService = new ProgramService(_session);
            _sessionService = new SessionService(_session);
            _organizationService = new OrganizationService(_session);
            _subjectService = new SubjectService(_session);
            _programSessionSubjectService = new ProgramSessionSubjectService(_session);
            _TestBaseService = new TestBaseService<ProgramSessionSubject>(_session);

            _courseSubjectFactory = new CourseSubjectFactory(null,_session);
            _programFactory = new ProgramFactory(null, _session);
            _sessionFactory = new SessionFactory(null, _session);
            _subjectFactory = new SubjectFactory(null,_session);
        }

        #endregion

        public void CleanUpProgramSessionSubject(long programId,long sessionId)
        {
            _TestBaseService.DeleteProgramSessionSubject(programId, sessionId);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _courseSubjectFactory.Cleanup();
            _courseFactory.Cleanup();
            _subjectFactory.Cleanup();
            _programFactory.Cleanup();
            _sessionFactory.Cleanup();
            base.Dispose();
        }
    }
}
