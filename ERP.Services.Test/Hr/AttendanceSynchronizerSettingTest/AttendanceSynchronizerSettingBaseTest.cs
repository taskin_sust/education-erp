﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.AttendanceSynchronizerSettingTest
{
    public class AttendanceSynchronizerSettingBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper CommonHelper;
        internal readonly IAttendanceSynchronizerSettingService _attendanceSynchronizerSettingService;
        internal AttendanceSynchronizerService _attendanceSynchronizerService;
        internal CampusService _campusService;
        internal List<long> IdList = new List<long>();

        private ISession _session;

        internal AttendanceSynchronizerSettingFactory _attendanceSynchronizerSettingFactory;
        internal AttendanceSynchronizerFactory _attendanceSynchronizerFactory;
        internal CampusRoomFactory _campusRoomFactory;
        internal CampusFactory _campusFactory;
        internal AttendanceDeviceFactory _attendanceDeviceFactory;
        internal OrganizationFactory _organizationFactory;
        internal BranchFactory _branchFactory;


        internal readonly string _name = Guid.NewGuid().ToString().Substring(0, 5) + DateTime.Now.ToString("yyyyMMddHHmmss") + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal readonly string _synchronizerKey = Guid.NewGuid().ToString().Substring(0, 5) + DateTime.Now.ToString("yyyyMMddHHmmss") + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";

        #endregion

        public AttendanceSynchronizerSettingBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _attendanceSynchronizerSettingService = new AttendanceSynchronizerSettingService(_session);
            _attendanceSynchronizerService = new AttendanceSynchronizerService(_session);
            _attendanceSynchronizerSettingFactory = new AttendanceSynchronizerSettingFactory(null, _session);
        }

        public void Dispose()
        {
            _attendanceSynchronizerSettingFactory.DeleteAll();
            base.Dispose();
        }
    }
}
