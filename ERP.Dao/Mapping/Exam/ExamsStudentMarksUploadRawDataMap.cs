﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Exam;

namespace UdvashERP.Dao.Mapping.Exam
{
    public partial class ExamsStudentMarksUploadRawDataMap : ClassMap<ExamsStudentMarksUploadRawData>
    {
        public ExamsStudentMarksUploadRawDataMap()
        {
            Table("ExamsStudentMarksUploadRawData");
			LazyLoad();

            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);

            Map(x => x.Roll).Column("Roll"); 
            Map(x => x.RegNo).Column("RegNo"); 
            Map(x => x.Board).Column("Board"); 
            Map(x => x.BarCode).Column("BarCode"); 
            Map(x => x.QrCode).Column("QrCode"); 
           
            Map(x => x.Gender).Column("Gender"); 
            Map(x => x.Version).Column("Version"); 
            Map(x => x.Image).Column("Image"); 
            Map(x => x.Answer).Column("Answer"); 
            Map(x => x.ObtainMark).Column("ObtainMark"); 
            Map(x => x.Message).Column("Message");
            Map(x => x.Eiin).Column("Eiin");
            Map(x => x.PrnNo).Column("PrnNo");

        }
    }
}
