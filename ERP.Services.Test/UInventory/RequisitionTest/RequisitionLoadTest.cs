﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Testing.Values;
using NHibernate.Mapping;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.RequisitionTest
{

    [Trait("Area", "UInventory")]
    [Trait("Service", "Requisition")]
    public class RequisitionLoadTest : RequisitionBaseTest
    {
        #region Basic Test

        [Fact]
        public void Load_RequisitionSlip()
        {
            const int count = 5;
            var orgFactory = organizationFactory.Create().Persist();
            var branchFactory = _branchFactory.CreateMore(count, orgFactory.Object).Persist();
            var reqfactory = _requisitionFactory.CreateMore(orgFactory.Object, branchFactory.ObjectList, count);
            
            var listFactory = _requisitionDetailsFactory.Create(2, orgFactory.Object, reqfactory.ObjectList[0]);
            var listFactory1 = _requisitionDetailsFactory.Create(2, orgFactory.Object, reqfactory.ObjectList[1]);
            var listFactory2 = _requisitionDetailsFactory.Create(2, orgFactory.Object, reqfactory.ObjectList[2]);
            var listFactory3 = _requisitionDetailsFactory.Create(2, orgFactory.Object, reqfactory.ObjectList[3]);
            var listFactory4 = _requisitionDetailsFactory.Create(2, orgFactory.Object, reqfactory.ObjectList[4]);

            var userMenu = BuildUserMenu(new List<Organization>() { orgFactory.Object }
                                        , listFactory.ObjectList.Select(x=>x.Program).ToList()
                                        , branchFactory.ObjectList);

            reqfactory.ObjectList[0].RequisitionDetails = new List<RequisitionDetails>()
            {
                listFactory.ObjectList[0],
                listFactory.ObjectList[1]
            };
            reqfactory.ObjectList[1].RequisitionDetails = new List<RequisitionDetails>()
            {
                listFactory1.ObjectList[2],
                listFactory1.ObjectList[3]
            };
            reqfactory.ObjectList[2].RequisitionDetails = new List<RequisitionDetails>()
            {
                listFactory2.ObjectList[4],
                listFactory2.ObjectList[5]
            };
            reqfactory.ObjectList[3].RequisitionDetails = new List<RequisitionDetails>()
            {
                listFactory3.ObjectList[6],
                listFactory3.ObjectList[7]
            };
            reqfactory.ObjectList[4].RequisitionDetails = new List<RequisitionDetails>()
            {
                listFactory4.ObjectList[8],
                listFactory4.ObjectList[9]
            };
            foreach (var requisition in reqfactory.ObjectList)
            {
                _requisitionService.SaveRequisition(requisition, userMenu);
            }
            var entityList = _requisitionService.LoadRequisitionSlipList(0, 10, "ItemName", "desc", userMenu, 0, 0, string.Empty, string.Empty, 0, false);
            Assert.Equal(entityList.Count, count);
        }

        #endregion

        #region business logic test
        
        [Fact]
        public void Requisition_List_Report_Return_True_With_Item()
        {
            var orgFactory = organizationFactory.Create().Persist();
            var reqfactory = _requisitionFactory.Create(orgFactory.Object);
            var rdFactory = _requisitionDetailsFactory.Create(5, orgFactory.Object, reqfactory.Object);
            var userMenu = BuildUserMenu(orgFactory.ObjectList[0], rdFactory.ObjectList.Select(x => x.Program).FirstOrDefault(), reqfactory.Object.Branch);

            foreach (var rdObj in rdFactory.ObjectList)
            {
                reqfactory.Object.RequisitionDetails.Add(rdObj);
            }
            _requisitionService.SaveRequisition(reqfactory.Object, userMenu);

            var bIds = new[] { reqfactory.Object.Branch.Id };
            var organizationId = orgFactory.Object.Id;

            var programSession = new List<string>();
            var purposelist = new List<int>();
            var itemIds = new[] { rdFactory.ObjectList[0].Item.Id };
            var itemGroupIds = new[] { rdFactory.ObjectList[0].Item.ItemGroup.Id };
            var statusIds = new[]
            {
                (int)RequisitionStatus.Pending,
                (int)RequisitionStatus.Partially,
                (int)RequisitionStatus.Completed
            };
            var dateFrom = reqfactory.Object.CreationDate.AddDays(-5);
            var dateTo = reqfactory.Object.CreationDate.AddDays(4);

            int i = 0 ;
            foreach (var obj in rdFactory.ObjectList)
            {
                if (obj == null) continue;
                if (obj.Purpose != null)
                {
                    purposelist.Add(obj.Purpose.Value);
                }
                if (obj.Program != null && obj.Session != null)
                {
                    programSession.Add(obj.Program.Id.ToString() + ":" + obj.Session.Id.ToString() + ": 0");
                }
            }
            purposelist = purposelist.Distinct().ToList();
            int recordsTotal = _requisitionService.GetRequisitionReportByListCount(userMenu, organizationId,
                                    bIds, itemGroupIds, itemIds, programSession.ToArray(), purposelist.ToArray(), statusIds, dateFrom, dateTo);
            IList<RequisitionListReportDto> list = _requisitionService.RequisitionReportByList(0, 10, "CreationDate", "desc", userMenu, organizationId,
                                    bIds, itemGroupIds, itemIds, programSession.ToArray(), purposelist.ToArray(), statusIds, dateFrom, dateTo).ToList();
            Assert.Equal(list.Count, recordsTotal);
            Assert.Equal(bIds.Count(), recordsTotal);
        }
        
        [Fact]
        public void Requisition_Branch_Wise_Report_Return_True()
        {
            var orgFactory = organizationFactory.Create().Persist();
            
            var reqfactory = _requisitionFactory.Create(orgFactory.Object);
            
            var rdFactory = _requisitionDetailsFactory.Create(5, orgFactory.Object, reqfactory.Object);
            var userMenu = BuildUserMenu(orgFactory.ObjectList[0], rdFactory.ObjectList.Select(x => x.Program).FirstOrDefault(), reqfactory.Object.Branch);

            foreach (var rdObj in rdFactory.ObjectList)
            {
                reqfactory.Object.RequisitionDetails.Add(rdObj);
            }
            _requisitionService.SaveRequisition(reqfactory.Object, userMenu);

            var bIds = new[] { reqfactory.Object.Branch.Id };

            var organizationId = orgFactory.Object.Id;

            var purposelist = new List<int>();
            var programSession = new List<string>(); 
            var itemIds = new[] { rdFactory.ObjectList[0].Item.Id };
            var itemGroupIds = new[] { rdFactory.ObjectList[0].Item.ItemGroup.Id };
            var statusIds = new[]
            {
                (int)RequisitionStatus.Pending,
                (int)RequisitionStatus.Partially,
                (int)RequisitionStatus.Completed
            };
            var dateFrom = reqfactory.Object.CreationDate.AddDays(-5);
            var dateTo = reqfactory.Object.CreationDate.AddDays(4);

            foreach (var obj in rdFactory.ObjectList)
            {
                if (obj == null) continue;
                if (obj.Purpose != null)
                {
                    purposelist.Add(obj.Purpose.Value);
                }
                programSession.Add(obj.Program.Id.ToString() + ":" + obj.Session.Id.ToString() + ": 0");
            }
            purposelist = purposelist.Distinct().ToList();

            var count = _requisitionService.RequisitionReportByBranchCount(userMenu, organizationId, bIds, purposelist.ToArray(), programSession.ToArray(),
                                                                            itemGroupIds, itemIds, statusIds, dateFrom, dateTo);
            var list = _requisitionService.RequisitionReportByBranch(0, count, "ItemGroup", "DESC", userMenu, organizationId, bIds, purposelist.ToArray()
                                                                        , programSession.ToArray(),itemGroupIds, itemIds, statusIds, dateFrom, dateTo);

            Assert.Equal(list.Count, count);
            Assert.Equal(itemIds.Count(), count);
        }
        
        [Fact]
        public void Requisition_ProgramSession_Wise_Report_Return_True()
        {
            var orgFactory = organizationFactory.Create().Persist();
            var reqfactory = _requisitionFactory.Create(orgFactory.Object);
            
            var rdFactory = _requisitionDetailsFactory.Create(5, orgFactory.Object, reqfactory.Object);
            var userMenu = BuildUserMenu(orgFactory.ObjectList[0], rdFactory.ObjectList.Select(x => x.Program).FirstOrDefault(), reqfactory.Object.Branch);
            foreach (var rdObj in rdFactory.ObjectList)
            {
                reqfactory.Object.RequisitionDetails.Add(rdObj);
            }
            reqfactory.Persist();

            var bIds = new[] { reqfactory.Object.Branch.Id };

            var organizationId = orgFactory.Object.Id;

            var purposelist = new List<int>();
            var programSession = new List<string>();
            var itemIds = new[] { rdFactory.ObjectList[0].Item.Id };
            var itemGroupIds = new[] { rdFactory.ObjectList[0].Item.ItemGroup.Id };
            var statusIds = new[]
            {
                (int)RequisitionStatus.Pending,
                (int)RequisitionStatus.Partially,
                (int)RequisitionStatus.Completed
            };
            var dateFrom = reqfactory.Object.CreationDate.AddDays(-5);
            var dateTo = reqfactory.Object.CreationDate.AddDays(4);

            foreach (var obj in rdFactory.ObjectList)
            {
                if (obj == null) continue;
                if (obj.Purpose != null)
                {
                    purposelist.Add(obj.Purpose.Value);
                }
                programSession.Add(obj.Program.Id.ToString() + ":" + obj.Session.Id.ToString() + ": 0");
            }
            purposelist = purposelist.Distinct().ToList();
            programSession = programSession.Distinct().ToList();

            var count = _requisitionService.RequisitionReportByProgramSessionCount(userMenu, organizationId, bIds, purposelist.ToArray(), programSession.ToArray()
                                                                                    , itemGroupIds, itemIds, statusIds, dateFrom, dateTo);
            var list = _requisitionService.RequisitionReportByProgramSession(0, count, "ItemGroup", "DESC", userMenu, organizationId, bIds, purposelist.ToArray(), itemIds
                                                                                    , itemGroupIds, programSession.ToArray(), statusIds, dateFrom, dateTo);

            Assert.Equal(list.Count, count);
            Assert.Equal(itemIds.Count(), count);
        }
        #endregion
    }
}
