﻿using log4net;
using NHibernate;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{

    public interface ILeaveApplicationLogService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class LeaveApplicationLogService : BaseService, ILeaveApplicationLogService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        public LeaveApplicationLogService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Functions

        public bool Save()
        {

            return true;
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}
