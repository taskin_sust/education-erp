﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.DailySupportAllowanceBlockTest
{
    public class DailySupportAllowanceBlockBaseTest : TestBase, IDisposable
    {
        public const int MentorId = 2;
        protected readonly DailySupportAllowanceBlockFactory _dailySupportAllowanceBlockFactory;
        protected readonly IDailyAllowanceBlockService _dailyAllowanceBlockService;
        protected readonly IAttendanceAdjustmentService _attendanceAdjustmentService;

        public DailySupportAllowanceBlockBaseTest()
        {
            _dailySupportAllowanceBlockFactory = new DailySupportAllowanceBlockFactory(null, Session);
            _dailyAllowanceBlockService = new DailySupportAllowanceBlockService(Session);
            _attendanceAdjustmentService = new AttendanceAdjustmentService(Session);
        }

        public void Dispose()
        {
            _dailySupportAllowanceBlockFactory.CleanUp();
            branchFactory.Cleanup();
            base.Dispose();
        }
    }
}
