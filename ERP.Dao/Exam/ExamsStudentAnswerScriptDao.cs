﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.SqlCommand;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Exam;


namespace UdvashERP.Dao.Exam
{
    public interface IExamsStudentAnswerScriptDao : IBaseDao<ExamsStudentAnswerScript, long>
    {
        #region Operational Function
        
        #endregion

        #region Single Instances Loading Function

        
        #endregion

        #region List Loading Function
        IList<ExamsStudentAnswerScript> GetExamStudentAnsScriptByExamId(Exams exams, int setCode);
        IList<ExamsStudentAnswerScript> LoadExamsStudentAnswerScript(long examsDetailsId, int setCode, int questionNo);
        #endregion

        #region Others Function
        bool CheckIsDuplicateAnswer(long programId, long sessionId, long courseId, long examId, int setCode); 
        #endregion

        
    }
    public class ExamsStudentAnswerScriptDao : BaseDao<ExamsStudentAnswerScript, long>, IExamsStudentAnswerScriptDao
    {
        /*SOME*/
        #region Properties & Object & Initialization
        #endregion
       
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        
        #endregion

        #region List Loading Function
        public IList<ExamsStudentAnswerScript> GetExamStudentAnsScriptByExamId(Exams exam, int setCode)
        {
            var examsStudentAnswers = new List<ExamsStudentAnswerScript>();
            foreach (var examDetail in exam.ExamDetails)
            {
                if (examDetail.ExamType == ExamType.Mcq)
                {
                    if (examDetail.ExamsStudentAnswerScripts != null && examDetail.ExamsStudentAnswerScripts.Count>0)
                        examsStudentAnswers.AddRange(examDetail.ExamsStudentAnswerScripts.Where(x => x.SetCode == setCode));
                }
            }
            var ss = examsStudentAnswers;
            return ss;
       }
        public IList<ExamsStudentAnswerScript> LoadExamsStudentAnswerScript(long examsDetailsId, int setCode, int questionNo)
        {
            var examsStudentAnswerScript = Session.Query<ExamsStudentAnswerScript>()
                .Where(x => x.ExamsDetails.Id == examsDetailsId && x.SetCode == setCode && x.QuestionNo == questionNo)
                .ToList();
            return examsStudentAnswerScript;
        }
       #endregion

        #region Others Function
        public bool CheckIsDuplicateAnswer(long programId, long sessionId, long courseId, long examId, int setCode)
        {

            var criteria = Session.CreateCriteria<Exams>();
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            criteria.Add(Restrictions.Eq("Course.Id", courseId));
            criteria.Add(Restrictions.Eq("Id", examId));

            criteria.CreateAlias("ExamDetails", "ed", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("ed.Exams.Id", examId));

            criteria.CreateAlias("ed.ExamsStudentAnswerScripts", "esas", JoinType.InnerJoin);
            criteria.Add(Restrictions.EqProperty("esas.ExamsDetails.Id", "ed.Id"));
            criteria.Add(Restrictions.Eq("esas.SetCode", setCode));
            var ansSheet = criteria.List<Exams>();
            if (ansSheet.Any())
            {
                return true;
            }
            return false;
        } 
        #endregion
        
        #region Helper Function

        #endregion
    }
}
