﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.ObjectFactory.UserAuth
{
    public class AspNetUserFactory:ObjectFactoryBase<AspNetUser>
    {
        #region Object Initialization

        private IUserService _userService;
        public AspNetUserFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _userService = new UserService(session);
        }

        #endregion

        #region Create
        #endregion

        #region Others

        public AspNetUserFactory LoadAspNetUser() //TODO: inappropriate name
        {
            const long branchId = 107;
            const long organizationId = 3;
            var userList = _userService.LoadAuthorizedUserBybranch(0, 0, "", "", "", "", branchId, "", organizationId, "", null, null);
            for (var i = 0; i < userList.Count; i++)
            {
                ObjectList.Add(userList[i]);
            }
            return this;
        }

        public AspNetUserFactory GetAspNetUser()
        {
            const long branchId = 107;
            const long organizationId = 3;
            var userList = _userService.LoadAuthorizedUserBybranch(0, 0, "", "", "", "", branchId, "", organizationId, "", null, null);
            Object = userList.OrderByDescending(x => x.Rank).FirstOrDefault();
            return this;
        }

        #endregion

        #region Cleanup
        #endregion

    }
}
