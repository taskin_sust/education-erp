﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.BatchTest
{
    public class BatchBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal const long OrgId = 1;
        internal DateTime _startTime = DateTime.Now.AddDays(1).AddHours(1);
        internal DateTime _endTime = DateTime.Now.AddDays(1).AddHours(10);
        internal string _days = "Sat, Mon, Wed, Fri";

        internal IBatchService _batchService;
        internal IProgramService _programService;
        internal ISessionService _sessionService;
        internal IBranchService _branchService;
        internal ICampusService _campusService;

        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal BatchFactory _batchFactory;

        #endregion

        public BatchBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();

            _batchService = new BatchService(_session);
            _programService = new ProgramService(_session);
            _sessionService = new SessionService(_session);
            _branchService = new BranchService(_session);
            _campusService = new CampusService(_session);

            CommonHelper = new CommonHelper();
            _batchFactory = new BatchFactory(null, _session);
        }

        public void Dispose()
        {
            _batchFactory.Cleanup();
            base.Dispose();
        }
    }
}
