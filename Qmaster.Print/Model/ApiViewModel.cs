﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qmaster.Print.Model
{


    public class JsonResponse
    {
        public virtual bool IsSuccess { get; set; }
        public virtual dynamic Data { get; set; }
    }

    public class JsonResponseOrganizationViewModel
    {
        public virtual bool IsSuccess { get; set; }
        public virtual String Message { get; set; }
        public virtual List<Organization> Data { get; set; }
    }

    public class JsonResponseProgramViewModel
    {
        public virtual bool IsSuccess { get; set; }
        public virtual String Message { get; set; }
        public virtual List<Model.Program> Data { get; set; }
    }

    public class JsonResponseSessionViewModel
    {
        public virtual bool IsSuccess { get; set; }
        public virtual String Message { get; set; }
        public virtual List<Session> Data { get; set; }
    }

    public class JsonResponseCourseViewModel
    {
        public virtual bool IsSuccess { get; set; }
        public virtual String Message { get; set; }
        public virtual List<Course> Data { get; set; }
    }

    public class JsonResponseExamViewModel
    {
        public virtual bool IsSuccess { get; set; }
        public virtual String Message { get; set; }
        public virtual List<Exam> Data { get; set; }
    }

    public class JsonResponseQuestionViewModel
    {
        public virtual bool IsSuccess { get; set; }
        public virtual String Message { get; set; }
        public virtual ExamQuestionSetPrintApiDto Data { get; set; }
    }
}
