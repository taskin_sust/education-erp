﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Engine;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class BranchFactory : ObjectFactoryBase<Branch>
    {
        #region Object Initialization

        protected readonly IBranchService _branchService;
        private OrganizationFactory _organizationFactory;
        //internal readonly string _code;
        public readonly List<string> assignCodeList;

        public BranchFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _branchService = new BranchService(_session);
            _organizationFactory = new OrganizationFactory(this, _session);
            assignCodeList = new List<string>();
        }

        #endregion

        #region Create

        public BranchFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }
        public BranchFactory WithCode(string code)
        {
            Object.Code = code;
            return this;
        }
        public BranchFactory Create()
        {
            string cd = GetRandomNumber();
            Object = new Branch()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString(),
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Code = cd,
                Status = Branch.EntityStatus.Active,
            };
            assignCodeList.Add(cd);
            SingleObjectList.Add(Object);
            return this;
        }


        public BranchFactory IsCorporate(bool isCorporate)
        {
            Object.IsCorporate = isCorporate;
            return this;
        }
        public BranchFactory WithOrganization()
        {
            _organizationFactory.Create().Persist();
            Object.Organization = _organizationFactory.Object;
            return this;
        }

        public BranchFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public BranchFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _branchService.IsSave(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _branchService.IsSave(item);
                    }
                }
            }
            return this;
        }
        public BranchFactory CreateMore(int numberOfBranch, IList<Organization> organizationList)
        {
            CreateBranchList(numberOfBranch, organizationList);
            return this;
        }

        private List<Branch> CreateBranchList(int numberOfBranch, IList<Organization> organizationList)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Branch>();
            foreach (var org in organizationList)
            {
                for (int i = 0; i < numberOfBranch; i++)
                {
                    string cd = GetRandomNumber();
                    var branch = new Branch
                    {
                        Id = 0,
                        BusinessId = GetPcUserName(),
                        Name = Guid.NewGuid().ToString().Substring(0, 6) + "_" + i,
                        Code = cd,
                        Rank = i,
                        CreateBy = 1,
                        ModifyBy = 1,
                        Status = Branch.EntityStatus.Active
                    };
                    branch.Organization = org;
                    list.Add(branch);
                    assignCodeList.Add(cd);
                }
            }
            ObjectList.AddRange(list);
            return list;
        }

        public BranchFactory CreateMore(int numberOfBranch)
        {
            CreateBranchList(numberOfBranch);
            return this;
        }


        public BranchFactory CreateMore(int numberOfBranch, Organization organization)
        {
            var orgList = new List<Organization>();
            orgList.Add(organization);

            CreateBranchList(numberOfBranch, orgList);
            return this;
        }

        public BranchFactory AssignOrganization(Organization organization)
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    item.Organization = organization;
                }
            }
            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    item.Organization = organization;
                }
            }
            return this;
        }
        private List<Branch> CreateBranchList(int numberOfBranch)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Branch>();
            var orgNamePrefix = Guid.NewGuid().ToString();
            _organizationFactory.Create()
                .WithNameAndRank(orgNamePrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"), 1)
                .Persist();

            for (int i = 0; i < numberOfBranch; i++)
            {
                var cd = GetRandomNumber();
                var branch = new Branch
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = "_" + i,
                    Code = cd,
                    Rank = i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Branch.EntityStatus.Active
                };
                branch.Organization = _organizationFactory.Object;
                list.Add(branch);
                assignCodeList.Add(cd);
            }
            ObjectList.AddRange(list);
            return list;
        }

        public BranchFactory WithOrganization(string orgName, int rank, string shortName, string contact, decimal basicSalary, decimal houseRent, decimal medicalAllowance, decimal convenyance, int monthlyWorkingDay, decimal deductionPerAbsent)
        {
            _organizationFactory.CreateWith(orgName, rank, shortName, contact, basicSalary, houseRent, medicalAllowance, convenyance, monthlyWorkingDay, deductionPerAbsent);
            Object.Organization = _organizationFactory.Object;
            return this;
        }

        #endregion

        #region Others

        public string GetRandomNumber()
        {
            int min = 10;
            int max = 99;
            int i = 0;
            var codeList = _branchService.LoadAllCode();
            if (assignCodeList != null && assignCodeList.Count > 0)
                codeList.AddRange(assignCodeList);
            do
            {
                Random r = new Random();
                var x = r.Next(min, max);
                string s = x.ToString("00");
                if (codeList != null && !codeList.Contains(s))
                {
                    return s;
                }
                else if (codeList == null)
                {
                    return s;
                }
                i++;
            } while (true);
            return "Error code found";
        }

        #endregion

        #region Cleanup

        public void Cleanup()
        {
            DeleteAll();
            _organizationFactory.Cleanup();
        }

        public void CleanupWithoutOrganization()
        {
            DeleteAll();
        }

        #endregion

    }
}
