﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentAdmissionTargetMap : BaseClassMap<StudentAdmissionTarget,long>
    {
        public StudentAdmissionTargetMap()
        {
            Table("StudentAdmissionTarget");
            LazyLoad();

            Map(x => x.DeadTime);
            Map(x => x.DeadLineTitle);
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId"); ;
            HasMany(x => x.StudentBranchTargets).KeyColumn("StudentAdmissionTargetId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
