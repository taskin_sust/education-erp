﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    class TeacherClassTypeMap : BaseClassMap<TeacherClassType, long>
    {
        public TeacherClassTypeMap()
        {
            Table("TeacherClassType");
            LazyLoad();
            Map(x => x.Name).Column("Name");
            HasMany(x => x.TeacherClassEntries).KeyColumn("TeacherClassTypeId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
