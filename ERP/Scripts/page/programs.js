$(document).ready(function () {
    

    /* Dispaly Programs Session Wise*/
    $("#BranchId").change(function () {
        var branch = $("#BranchId").val();
        var session = $("#SessionId").val();
      
        if (branch != "" && session != "") {
            var data = { "BranchId": branch, "SessionId": session };
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Program/getBranchSessionProgram",
                contenttype: "application/json; charset=utf-8",
                data: data,
                success: function (result) {
                    $('.programCheckbox').prop('checked', false);
                    if (!($.isEmptyObject(result))) {
                        $("#submit").val("Update"); 
                        $.each(result, function (index, program) {
                            if (program.isChecked == 0) {
                                $('#program' + program.value).prop('checked', true);
                            }
                        });
                    }
                    else { $("#submit").val("Create"); }

                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });          
        }
        else {
            
        }
    });
    $("#SessionId").change(function () {
        var branch = $("#BranchId").val();
        var session = $("#SessionId").val();

        if (branch != "" && session != "") {
            var data = { "BranchId": branch, "SessionId": session };
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Program/getBranchSessionProgram",
                contenttype: "application/json; charset=utf-8",
                data: data,
                success: function (result) {
                    $('.programCheckbox').prop('checked', false);
                    if (!($.isEmptyObject(result))) {
                        $("#submit").val("Update");                      
                        $.each(result, function (index, program) {
                            if (program.isChecked == 0) {
                                $('#program' + program.value).prop('checked', true);
                            }
                        });
                    }
                    else { $("#submit").val("Create"); }

                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
        else {

        }
    });

    /* Select/Deselect All Programs*/
    $("#allPrograms").click(function () {
        if ($(this).is(":checked")) {
            var c = this.checked;
            $('.programCheckbox').prop('checked', c);
        }
        else {
            var c = this.checked;
            $('.programCheckbox').prop('checked', c);
        }
    });
    
});


    
