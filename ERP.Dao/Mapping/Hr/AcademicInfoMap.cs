using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class AcademicInfoMap : BaseClassMap<AcademicInfo,long> {
        
        public AcademicInfoMap() {
			Table("HR_AcademicInfo");
			LazyLoad();
			
			References(x => x.Exam).Column("ExamId");
            References(x => x.Board).Column("BoardId");
			References(x => x.Institute).Column("InstituteId");
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.DegreeTitle).Column("DegreeTitle");
			Map(x => x.Major).Column("Major");
            Map(x => x.ResultType).Column("ResultType");
            Map(x => x.GradeScale).Column("GradeScale");
            Map(x => x.Grade).Column("Grade");
			Map(x => x.Result).Column("Result");
			Map(x => x.Year).Column("Year");
			Map(x => x.CurrentAcademicStatus).Column("CurrentAcademicStatus");
			HasMany(x => x.Certificate).KeyColumn("AcademicInfoId");
        }
    }
}
