using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class ManagementTimeMap : BaseClassMap<ManagementTime,long> {

        public ManagementTimeMap()
        {
            Table("TM_ManagementTime");
			LazyLoad();
            References(x => x.TeamMember).Column("TeamMemberId");
            Map(x => x.WorkDate).Column("WorkDate");
            Map(x => x.Time).Column("Time");
            Map(x => x.Remarks).Column("Remarks");
        }
    }
}
