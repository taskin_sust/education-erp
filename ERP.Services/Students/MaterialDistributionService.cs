﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IMaterialDistributionService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        
       
        #endregion

        #region Others Function
        
        #endregion


        //with Pair Auth
        //IList<string> LoadStudentProgramByCriteria(List<UserMenu> userMenu, int start, int length, long organizationId, long programId, long[] branchId, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus);
        //IList<StudentProgram> LoadStudentProgramByCriteriaForExport(List<UserMenu> userMenu, long organizationId, long programId, long[] branchId, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus);
        //int GetStudentProgramCount(List<UserMenu> userMenu, long organizationId, long programId, long[] branchId, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus);

        //without Pair

        IList<string> LoadStudentProgramByCriteria(int start, int length, List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus, string[] informationViewList);
        IList<StudentProgram> LoadStudentProgramByCriteriaForExport(int start, int length, List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus, string[] informationViewList);
        int GetStudentProgramCount(int start, int length, List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus, string[] informationViewList);
        
    }

    public class MaterialDistributionService : BaseService, IMaterialDistributionService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IMaterialDistributionDao _materialDistributionDao;
        private readonly ICommonHelper _commonHelper;
        public MaterialDistributionService(ISession session)
        {
            Session = session;
            _materialDistributionDao = new MaterialDistributionDao() { Session = Session };
            _commonHelper=new CommonHelper();
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
      
       
        #endregion

        #region Others Function

       
        #endregion

        #region Helper Function

        #endregion

        #region With Pair
        //public IList<string> LoadStudentProgramByCriteria(List<UserMenu> userMenu, int start, int length, long organizationId, long programId, long[] branchId, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus)
        //{
        //    IList<string> studentPrograms;
        //    try
        //    {
        //        List<long> notNullBranchList = (branchId != null) ? CommonHelper.ConvertSelectedAllIdList(branchId.ToList()) : null;
        //        List<ProgramBranchPair> programBranchPairList = AuthHelper.LoadProgramBranchPairList(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), notNullBranchList);
        //        studentPrograms = _materialDistributionDao.LoadStudentProgramByCriteria(start, length, organizationId, programBranchPairList, sessionId, campusId, batchDays, batchTime, batch, materialType, materialId, materialStatus, paymentStatus);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    return studentPrograms;

        //}


        //public IList<StudentProgram> LoadStudentProgramByCriteriaForExport(List<UserMenu> userMenu, long organizationId, long programId, long[] branchId, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus)
        //{
        //    IList<StudentProgram> studentPrograms;
        //    try
        //    {
        //        List<long> notNullBranchList = (branchId != null) ? CommonHelper.ConvertSelectedAllIdList(branchId.ToList()) : null;
        //        List<ProgramBranchPair> programBranchPairList = AuthHelper.LoadProgramBranchPairList(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), notNullBranchList);
        //        studentPrograms = _materialDistributionDao.LoadStudentProgramByCriteriaForExport(organizationId, programBranchPairList, sessionId, campusId, batchDays, batchTime, batch, materialType, materialId, materialStatus, paymentStatus);
        //        //List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
        //        //List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
        //        //studentPrograms = _materialDistributionDao.LoadStudentProgramByCriteriaForExport(start, length, programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batch, materialType, materialId, materialStatus, paymentStatus, informationViewList);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    return studentPrograms;
        //}
        //public int GetStudentProgramCount(List<UserMenu> userMenu, long organizationId, long programId, long[] branchId, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus)
        //{
        //    int count = 0;
        //    try
        //    {
        //        List<long> notNullBranchList = (branchId != null) ? CommonHelper.ConvertSelectedAllIdList(branchId.ToList()) : null;
        //        List<ProgramBranchPair> programBranchPairList = AuthHelper.LoadProgramBranchPairList(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), notNullBranchList);
        //        count = _materialDistributionDao.GetStudentProgramCount(organizationId, programBranchPairList, sessionId, campusId, batchDays, batchTime, batch, materialType, materialId, materialStatus, paymentStatus);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    return count;
        //}
        #endregion

        #region Without Pair
        public IList<string> LoadStudentProgramByCriteria(int start, int length, List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus, string[] informationViewList)
        {
            IList<string> studentPrograms;
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                studentPrograms = _materialDistributionDao.LoadStudentProgramByCriteria(start, length, programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batch, materialType, materialId, materialStatus, paymentStatus, informationViewList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return studentPrograms;

        }
        public int GetStudentProgramCount(int start, int length, List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus, string[] informationViewList)
        {
            int count = 0;
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                count = _materialDistributionDao.GetStudentProgramCount(start, length, programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batch, materialType, materialId, materialStatus, paymentStatus, informationViewList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return count;
        }
        public IList<StudentProgram> LoadStudentProgramByCriteriaForExport(int start, int length, List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, long[] materialType, long[] materialId, long materialStatus, long paymentStatus, string[] informationViewList)
        {
            IList<StudentProgram> studentPrograms;
            try
            {
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                studentPrograms = _materialDistributionDao.LoadStudentProgramByCriteriaForExport(start, length, programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batch, materialType, materialId, materialStatus, paymentStatus, informationViewList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return studentPrograms;
        }
        #endregion

    }
}
