﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class BatchFactory : ObjectFactoryBase<Batch>
    {
        #region Object Initialization

        private ProgramFactory _programFactory;
        private SessionFactory _sessionFactory;
        private CampusFactory _campusFactory;
        private BranchFactory _branchFactory;

        protected readonly IBatchService _batchService;
        private TestBaseService<Batch> _batchTestBaseService;

        public BatchFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _programFactory = new ProgramFactory(this, session);
            _sessionFactory = new SessionFactory(this, session);
            _campusFactory = new CampusFactory(this, session);
            _branchFactory = new BranchFactory(this, session);

            _batchService = new BatchService(session);
            _batchTestBaseService = new TestBaseService<Batch>(session);
        }

        #endregion

        #region Create

        public BatchFactory Create()
        {
            Object = new Batch()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Batch.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public BatchFactory WithProperties(Organization organization, Program program, Session session, Branch branch, Campus campus,
            string batchDays, DateTime? startTime = null, DateTime? endTime = null, int? capacity = null, int? versionOfStudy = null, int? gender = null)
        {
            if (organization != null)
                Object.Organization = organization;
            if (program != null)
                Object.Program = program;
            if (session != null)
                Object.Session = session;
            if (branch != null)
                Object.Branch = branch;
            if (campus != null)
                Object.Campus = campus;
            Object.Days = batchDays;
            Object.StartTime = startTime;
            Object.EndTime = endTime;
            Object.Capacity = capacity;
            if (gender != null)
            {
                Object.Gender = gender.Value;
            }
            if (versionOfStudy != null)
            {
                Object.VersionOfStudy = versionOfStudy.Value;
            }
            return this;
        }

        public BatchFactory WithProperties(bool withOrganization, bool withProgram, bool withSession, bool withBranch, bool withCampus,
            string batchDays, DateTime? startTime = null, DateTime? endTime = null, int? capacity = null, int? versionOfStudy = null, int? gender = null)
        {
            var branchFactory = _branchFactory.Create().WithOrganization().Persist();
            var programFactory = _programFactory.Create().WithOrganization().Persist();
            var campusFactory = _campusFactory.Create().WithBranch(branchFactory.Object)
                    .WithCampusRoomList(new List<CampusRoom>()
                    {
                        new CampusRoom()
                        {
                            RoomNo = "101",
                            ClassCapacity = 100,
                            ExamCapacity = 100
                        }
                    })
                    .Persist();

            var sessionFactory = _sessionFactory.Create().Persist();
            if (withOrganization)
                Object.Organization = programFactory.Object.Organization;
            if (withProgram)
                Object.Program = programFactory.Object;
            if (withSession)
                Object.Session = sessionFactory.Object;
            if (withBranch)
                Object.Branch = branchFactory.Object;
            if (withCampus)
                Object.Campus = campusFactory.Object;
            Object.Days = batchDays;
            Object.StartTime = startTime;
            Object.EndTime = endTime;
            Object.Capacity = capacity;
            if (gender != null)
            {
                Object.Gender = gender.Value;
            }
            if (versionOfStudy != null)
            {
                Object.VersionOfStudy = versionOfStudy.Value;
            }
            return this;
        }

        public BatchFactory CreateMore(int numberOfcampus, IList<Branch> branchList, List<CampusRoom> objList)
        {
            CreateBatchList(numberOfcampus, branchList);
            return this;
        }

        private List<Batch> CreateBatchList(int numberOfcampus, IList<Branch> branchList)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Batch>();
            foreach (var br in branchList)
            {
                for (int i = 0; i < numberOfcampus; i++)
                {
                    var batch = new Batch()
                    {
                        Id = 0,
                        BusinessId = GetPcUserName(),
                        Name = "_" + i,
                        Rank = i,
                        CreateBy = 1,
                        ModifyBy = 1,
                        Status = Batch.EntityStatus.Active
                    };
                    list.Add(batch);
                }
            }
            ObjectList.AddRange(list);
            return list;
        }

        public BatchFactory CreateMore(int numberOfBatch)
        {
            CreateBatchList(numberOfBatch);
            return this;
        }

        private List<Batch> CreateBatchList(int numberOfBatch)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Batch>();
            var branchFactory = _branchFactory.Create().WithOrganization().Persist();
            var programFactory = _programFactory.Create().WithOrganization().Persist();
            var campusFactory = _campusFactory.Create().WithBranch(branchFactory.Object).WithCampusRoomList(new List<CampusRoom>()
                    {
                        new CampusRoom()
                        {
                            RoomNo = "101",
                            ClassCapacity = 100,
                            ExamCapacity = 100
                        }
                    }).Persist();
            var sessionFactory = _sessionFactory.Create().Persist();

            for (int i = 0; i < numberOfBatch; i++)
            {
                var batch = new Batch()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    Name = "_" + i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Batch.EntityStatus.Active,
                    Branch = branchFactory.Object,
                    Campus = campusFactory.Object,
                    Organization = programFactory.Object.Organization,
                    Program = programFactory.Object,
                    Session = sessionFactory.Object,
                    Days = Guid.NewGuid() + "batchDays",
                    StartTime = DateTime.Now,
                    EndTime = DateTime.Now.AddHours(3),
                    Capacity = 100,
                    Gender = 1,
                    VersionOfStudy = 1,
                };
                list.Add(batch);
            }
            ObjectList.AddRange(list);
            return list;
        }

        public BatchFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }

        public BatchFactory WithBranch(Branch branch)
        {
            Object.Branch = branch;
            return this;
        }

        public BatchFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _batchService.Save(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _batchService.Save(item);
                    }
                }
            }
            return this;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void Cleanup()
        {
            DeleteAll();
            _programFactory.Cleanup();
            _sessionFactory.Cleanup();
            if (_campusFactory.Object != null && _campusFactory.Object.CampusRooms != null && _campusFactory.Object.CampusRooms.Count > 0)
                _batchTestBaseService.DeleteCampusRoom(_campusFactory.Object.CampusRooms.Select(x => x.Id).ToList());
            _campusFactory.Cleanup();
            _branchFactory.Cleanup();

        }
        #endregion

    }
}
