﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UdvashERP.Api.Models
{
    public class TeamMemberModel
    {
        public virtual string Name { get; set; }
        public virtual string CardNumber { get; set; }
        public virtual int Pin { get; set; }
        public virtual int Status { get; set; }
        public virtual bool SaveToDeviceSuccessed { get; set; }
        public virtual int EnrollNo { get; set; }
        public virtual bool IsAdmin { get; set; }
        public virtual string FingerIndex0 { get; set; }
        public virtual string FingerIndex1 { get; set; }
        public virtual string FingerIndex2 { get; set; }
        public virtual string FingerIndex3 { get; set; }
        public virtual string FingerIndex4 { get; set; }
        public virtual string FingerIndex5 { get; set; }
        public virtual string FingerIndex6 { get; set; }
        public virtual string FingerIndex7 { get; set; }
        public virtual string FingerIndex8 { get; set; }
        public virtual string FingerIndex9 { get; set; }

    }
}