﻿/*Written By Habibul Morsalin */
$(document).ready(function () {

    $(document).on('click', '#checkAll', function () {
        if (this.checked) { // check select status
            $('#DataGrid tbody .innerchk').each(function () { //loop through each checkbox
                this.checked = true;                
            });
        } else {
            $('#DataGrid tbody .innerchk').each(function () { //loop through each checkbox
                this.checked = false;                    
            });
        }
    });
    $(document).on('click', '#printIdCard', function () {
        $("#studentProgramIdListSelected").empty();
        //$('#printIdCardForm').submit(false);
        var $isSelectAnyStudent = false;
        $('#DataGrid tbody .innerchk').each(function () { //loop through each checkbox
            if (this.checked) {
                var vals = $(this).attr("id");
                var $hiddenInput = $('<input/>', { type: 'hidden', name: 'studentProgramIdList', id: vals, value: vals });
                $("#studentProgramIdListSelected").append($hiddenInput);
                $isSelectAnyStudent = true;
            }
        });
        
        if ($isSelectAnyStudent) {          
            $('#printIdCardForm').submit();
        }
    });
});