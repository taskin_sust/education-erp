﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Org.BouncyCastle.Bcpg.OpenPgp;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.BusinessRules.Teachers;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Students;
using UdvashERP.Services.Teachers;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Areas.Teachers.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Teachers")]
    public class TeacherController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeachersArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly ITeacherActivityService _teacherActivityService;
        private readonly ISubjectService _subjectService;
        private readonly ITeacherService _teacherService;
        private readonly IExtraCurricularActivityService _extraCarricularActivitiesService;
        private readonly ITeacherImageMediaService _teacherImageMediaService;
        public TeacherController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _organizationService = new OrganizationService(session);
                _teacherActivityService = new TeacherActivityService(session);
                _subjectService = new SubjectService(session);
                _teacherService = new TeacherService(session);
                _extraCarricularActivitiesService = new ExtraCurricularActivityService(session);
                _teacherImageMediaService = new TeacherImageMediaService();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Save

        public ActionResult Create()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            TeacherViewModel teacherViewModel = new TeacherViewModel();
            ViewBag.OrganizationList = new List<Organization>();
            ViewBag.SubjectList = new SelectList(new List<Subject>(), "Id", "Name");
            ViewBag.ActivityList = new SelectList(new List<TeacherActivity>(), "Id", "Name");
            ViewBag.VersionOfStudyList = new SelectList(new Dictionary<int, string>(), "Key", "Value");
            ViewBag.GenderList = new SelectList(new Dictionary<int, string>(), "Key", "Value");
            ViewBag.ReligionList = new SelectList(new Dictionary<int, string>(), "Key", "Value");
            ViewBag.BloodGroupList = new SelectList(new Dictionary<int, string>(), "Key", "Value");
            ViewBag.PreviousExtraCuricularList = new List<TeacherExtraCurricularActivity>();
            ViewBag.WishExtraCuricularList = new List<TeacherExtraCurricularActivity>();
            try
            {
                ViewBag.OrganizationList = _organizationService.LoadAuthorizedOrganization(userMenu);
                ViewBag.SubjectList = new SelectList(_subjectService.LoadSubject(), "Id", "Name");
                ViewBag.ActivityList = new SelectList(_teacherActivityService.LoadTeacherActivity(), "Id", "Name");
                ViewBag.VersionOfStudyList = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(new List<int> { (int)VersionOfStudy.Combined }), "Key", "Value");
                ViewBag.GenderList = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(new List<int> { (int)Gender.Combined }), "Key", "Value");
                ViewBag.ReligionList = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(), "Key", "Value");
                ViewBag.BloodGroupList = new SelectList(_commonHelper.LoadEmumToDictionary<BloodGroup>(), "Key", "Value");

                // Previous extra curicular activity are those which are organization Id null
                // Previous Extra curicular
                ViewBag.PreviousExtraCuricularList = _extraCarricularActivitiesService.LoadExtraCurricularActivity();

                teacherViewModel.TeacherAdmission = new List<TeacherAdmissionViewModel>()
                {
                    new TeacherAdmissionViewModel()
                    {
                        InstituteName = "BUET",
                        AtPosition = "",
                        AtSession = "",
                        GotSubject = ""
                    },
                    new TeacherAdmissionViewModel()
                    {
                        InstituteName = "MEDICAL",
                        AtPosition = "",
                        AtSession = "",
                        GotSubject = ""
                    },
                    new TeacherAdmissionViewModel()
                    {
                        InstituteName = "DU",
                        AtPosition = "",
                        AtSession = "",
                        GotSubject = ""
                    },
                };

            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return View(teacherViewModel);
        }

        #endregion

        #region Edit

        [HttpGet]
        public ActionResult Edit(string tpin = "")
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.Tpin = tpin;
            ViewBag.isPost = false;
            ViewBag.teacherViewModel = null;
            try
            {
                if (!String.IsNullOrEmpty(tpin) && Convert.ToInt32(tpin) > 0)
                {
                    ViewBag.isPost = true;
                    Teacher teacher = _teacherService.GetTeacherByTpin(Convert.ToInt32(tpin));
                    if (teacher != null)
                    {
                        ViewBag.OrganizationList = _organizationService.LoadAuthorizedOrganization(userMenu);
                        ViewBag.SubjectList = _subjectService.LoadSubject();
                        ViewBag.ActivityList = _teacherActivityService.LoadTeacherActivity();
                        ViewBag.VersionOfStudyList = _commonHelper.LoadEmumToDictionary<VersionOfStudy>(new List<int> { (int)VersionOfStudy.Combined });//_commonHelper.GetVersionOfStudies();
                        ViewBag.GenderList = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(new List<int> { (int)Gender.Combined }), "Key", "Value", teacher.Gender);
                        ViewBag.ReligionList = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(), "Key", "Value", teacher.Religion);
                        ViewBag.BloodGroupList = new SelectList(_commonHelper.LoadEmumToDictionary<BloodGroup>(), "Key", "Value", teacher.BloodGroup);
                        ViewBag.PreviousExtraCuricularList = _extraCarricularActivitiesService.LoadExtraCurricularActivity();
                        ViewBag.WishExtraCuricularList = new List<TeacherExtraCurricularActivity>();
                        if (teacher.Organizations.Any())
                        {
                            List<long> orgArray = teacher.Organizations.Select(x => x.Id).ToList();
                            List<string> orgArrayString = orgArray.Select(org => org.ToString()).ToList();
                            ViewBag.WishExtraCuricularList = _extraCarricularActivitiesService.LoadExtraCurricularActivity(orgArrayString.ToArray());
                        }
                        TeacherViewModel teacherViewModel = new TeacherViewModel();
                        teacherViewModel.Id = teacher.Id;
                        teacherViewModel.Tpin = teacher.Tpin;

                        #region Quick Information

                        teacherViewModel.NickName = teacher.NickName;
                        teacherViewModel.MobileNumber1 = teacher.PersonalMobile;
                        teacherViewModel.MobileNumber2 = teacher.AlternativeMobile;
                        teacherViewModel.Institute = teacher.Institute;
                        teacherViewModel.Department = teacher.Department;
                        teacherViewModel.HscPassingYear = (int)teacher.HscPassingYear;
                        teacherViewModel.Religion = (int)teacher.Religion;
                        teacherViewModel.Gender = (int)teacher.Gender;

                        #region organization List

                        List<TeacherOrganizationViewModel> teacherOrganizationViewModelList = new List<TeacherOrganizationViewModel>();
                        if (teacher.Organizations.Any())
                        {
                            foreach (Organization organization in teacher.Organizations)
                            {
                                TeacherOrganizationViewModel teacherOrganizationViewModel = new TeacherOrganizationViewModel();
                                teacherOrganizationViewModel.TeacherOrganizationId = 0;
                                teacherOrganizationViewModel.OrganizationId = organization.Id;
                                teacherOrganizationViewModel.OrganizationName = organization.ShortName;
                                teacherOrganizationViewModel.TeacherId = teacher.Id;
                                teacherOrganizationViewModel.TeacherName = teacher.Name;
                                teacherOrganizationViewModelList.Add(teacherOrganizationViewModel);
                            }
                        }
                        teacherViewModel.TeacherOrganization = teacherOrganizationViewModelList;

                        #endregion

                        #region Activity Priority List

                        List<TeacherActivityViewModel> teacherActivityViewModelList = new List<TeacherActivityViewModel>();
                        if (teacher.TeacherActivityPriorities.Any())
                        {
                            foreach (TeacherActivityPriority teacherActivityPriority in teacher.TeacherActivityPriorities)
                            {
                                TeacherActivityViewModel teacherActivityViewModel = new TeacherActivityViewModel();
                                teacherActivityViewModel.TeacherActivityId = teacherActivityPriority.Id;
                                teacherActivityViewModel.ActivityId = teacherActivityPriority.TeacherActivity.Id;
                                teacherActivityViewModel.ActivityName = teacherActivityPriority.TeacherActivity.Name;
                                teacherActivityViewModel.Priority = teacherActivityPriority.Priority;
                                teacherActivityViewModel.TeacherId = teacher.Id;
                                teacherActivityViewModel.TeacherName = teacher.Name;
                                teacherActivityViewModelList.Add(teacherActivityViewModel);
                            }
                        }
                        teacherViewModel.TeacherActivity = teacherActivityViewModelList;

                        #endregion

                        #region Subject Priority List

                        List<TeacherSubjectPriorityViewModel> teacherSubjectPriorityViewModelLIst = new List<TeacherSubjectPriorityViewModel>();
                        if (teacher.TeacherSubjectPriorities.Any())
                        {
                            foreach (TeacherSubjectPriority teacherSubjectPriority in teacher.TeacherSubjectPriorities)
                            {
                                TeacherSubjectPriorityViewModel teacherSubjectPriorityView = new TeacherSubjectPriorityViewModel();
                                teacherSubjectPriorityView.TeacherSubjectPriorityId = teacherSubjectPriority.Id;
                                teacherSubjectPriorityView.SubjectId = teacherSubjectPriority.Subject.Id;
                                teacherSubjectPriorityView.SubjectName = teacherSubjectPriority.Subject.Name;
                                teacherSubjectPriorityView.TeacherId = teacher.Id;
                                teacherSubjectPriorityView.TeacherName = teacher.Name;
                                teacherSubjectPriorityViewModelLIst.Add(teacherSubjectPriorityView);
                            }
                        }
                        teacherViewModel.TeacherSubjectPriority = teacherSubjectPriorityViewModelLIst;

                        #endregion

                        #region Version Priority List

                        List<TeacherVersionOfStudyPriorityViewModel> teacherVersionOfStudyPriorityViewModelList = new List<TeacherVersionOfStudyPriorityViewModel>();
                        if (teacher.TeacherVersionOfStudyPriorities.Any())
                        {
                            foreach (TeacherVersionOfStudyPriority teacherVersionOfStudyPriority in teacher.TeacherVersionOfStudyPriorities)
                            {
                                TeacherVersionOfStudyPriorityViewModel teacherVersionOfStudyPriorityViewModel = new TeacherVersionOfStudyPriorityViewModel();
                                teacherVersionOfStudyPriorityViewModel.TeacherVersionOfStudyPriorityId = teacherVersionOfStudyPriority.Id;
                                teacherVersionOfStudyPriorityViewModel.VersionOfStudyId = teacherVersionOfStudyPriority.VersionOfStudy;
                                teacherVersionOfStudyPriorityViewModel.Priority = teacherVersionOfStudyPriority.Priority;
                                teacherVersionOfStudyPriorityViewModel.TeacherId = teacher.Id;
                                teacherVersionOfStudyPriorityViewModel.TeacherName = teacher.Name;
                                teacherVersionOfStudyPriorityViewModelList.Add(teacherVersionOfStudyPriorityViewModel);
                            }
                        }
                        teacherViewModel.TeacherVersionOfStudyPriority = teacherVersionOfStudyPriorityViewModelList;

                        #endregion

                        #endregion

                        #region Details Information

                        teacherViewModel.FullName = teacher.FullName;
                        teacherViewModel.FatherName = teacher.FathersMobile;
                        teacherViewModel.DateOfBirth = (teacher.DateOfBirth != null) ? teacher.DateOfBirth.Value.Day.ToString("00") + "/" + teacher.DateOfBirth.Value.Month.ToString("00") + "/" + teacher.DateOfBirth.Value.Year.ToString("0000") : "";
                        teacherViewModel.MobileNumberRoommate = teacher.RoomMatesMobile;
                        teacherViewModel.MobileNumberFather = teacher.FathersMobile;
                        teacherViewModel.MobileNumberMother = teacher.MothersMobile;
                        teacherViewModel.FacebookId = teacher.FbId;
                        teacherViewModel.PresentAddress = teacher.PresentAddress;
                        teacherViewModel.Area = teacher.Area;
                        teacherViewModel.PermanentAddress = teacher.PermanentAddress;

                        #region admission info

                        List<TeacherAdmissionViewModel> teacherAdmissionTestList = new List<TeacherAdmissionViewModel>()
                                    {
                                        new TeacherAdmissionViewModel()
                                        {
                                            TeacherAdmissionId = 0,
                                            InstituteName = "BUET",
                                            AtPosition = "",
                                            AtSession = "",
                                            GotSubject = ""
                                        },
                                        new TeacherAdmissionViewModel()
                                        {
                                            TeacherAdmissionId = 0,
                                            InstituteName = "MEDICAL",
                                            AtPosition = "",
                                            AtSession = "",
                                            GotSubject = ""
                                        },
                                        new TeacherAdmissionViewModel()
                                        {
                                            TeacherAdmissionId = 0,
                                            InstituteName = "DU",
                                            AtPosition = "",
                                            AtSession = "",
                                            GotSubject = ""
                                        },
                                    };
                        if (teacher.TeacherAdmissionInfos.Any())
                        {
                            foreach (TeacherAdmissionInfo teacheradminiAdmissionInfo in teacher.TeacherAdmissionInfos)
                            {
                                switch (teacheradminiAdmissionInfo.UniversityName)
                                {
                                    case (int)University.BUET:
                                        TeacherAdmissionViewModel teacherAdmissionTestViewModelBuet = teacherAdmissionTestList.SingleOrDefault(x => x.InstituteName == "BUET");
                                        if (teacherAdmissionTestViewModelBuet != null)
                                        {
                                            teacherAdmissionTestViewModelBuet.AtPosition = teacheradminiAdmissionInfo.AtPosition.ToString();
                                            teacherAdmissionTestViewModelBuet.AtSession = teacheradminiAdmissionInfo.AtSession.ToString();
                                            teacherAdmissionTestViewModelBuet.GotSubject = teacheradminiAdmissionInfo.GotSubject;
                                            teacherAdmissionTestViewModelBuet.TeacherAdmissionId = teacheradminiAdmissionInfo.Id;
                                        }
                                        break;
                                    case (int)University.MEDICAL:
                                        TeacherAdmissionViewModel teacherAdmissionTestViewModelMedical = teacherAdmissionTestList.SingleOrDefault(x => x.InstituteName == "MEDICAL");
                                        if (teacherAdmissionTestViewModelMedical != null)
                                        {
                                            teacherAdmissionTestViewModelMedical.AtPosition = teacheradminiAdmissionInfo.AtPosition.ToString();
                                            teacherAdmissionTestViewModelMedical.AtSession = teacheradminiAdmissionInfo.AtSession.ToString();
                                            teacherAdmissionTestViewModelMedical.GotSubject = teacheradminiAdmissionInfo.GotSubject;
                                        }
                                        break;
                                    case (int)University.DU:
                                        TeacherAdmissionViewModel teacherAdmissionTestViewModelDu = teacherAdmissionTestList.SingleOrDefault(x => x.InstituteName == "DU");
                                        if (teacherAdmissionTestViewModelDu != null)
                                        {
                                            teacherAdmissionTestViewModelDu.AtPosition = teacheradminiAdmissionInfo.AtPosition.ToString();
                                            teacherAdmissionTestViewModelDu.AtSession = teacheradminiAdmissionInfo.AtSession.ToString();
                                            teacherAdmissionTestViewModelDu.GotSubject = teacheradminiAdmissionInfo.GotSubject;
                                        }
                                        break;
                                }
                            }
                        }
                        teacherViewModel.TeacherAdmission = teacherAdmissionTestList;

                        #endregion

                        #region Previous Extra Curricular Activity

                        List<TeacherExtraCurricularActivity> teacherPreviousExtraCurricularActivitityList = teacher.TeacherExtraCurricularActivities.Where(x => x.ExtraCurricularActivity.Organization == null).ToList();
                        List<TeacherPreviousExtraCuricularActivityViewModel> teacherExtraCurricularActivityViewModelList = new List<TeacherPreviousExtraCuricularActivityViewModel>();
                        if (teacherPreviousExtraCurricularActivitityList.Any())
                        {
                            foreach (TeacherExtraCurricularActivity teacherExtraCurricularActivity in teacherPreviousExtraCurricularActivitityList)
                            {
                                TeacherPreviousExtraCuricularActivityViewModel teacherPreviousExtraCuricularActivityViewModel = new TeacherPreviousExtraCuricularActivityViewModel();
                                teacherPreviousExtraCuricularActivityViewModel.ExtraCuricularActivityId = teacherExtraCurricularActivity.ExtraCurricularActivity.Id;
                                teacherPreviousExtraCuricularActivityViewModel.ExtraCuricularActivityName = teacherExtraCurricularActivity.ExtraCurricularActivity.Name;
                                teacherPreviousExtraCuricularActivityViewModel.TeacherExtraCuricularActivityId = teacherExtraCurricularActivity.Id;
                                teacherExtraCurricularActivityViewModelList.Add(teacherPreviousExtraCuricularActivityViewModel);
                            }
                        }
                        teacherViewModel.TeacherPreviousExtraCuricularActivity = teacherExtraCurricularActivityViewModelList;

                        #endregion

                        #region Wish Extra Curricular Activity

                        List<TeacherExtraCurricularActivity> teacherWishExtraCurricularActivitityList = teacher.TeacherExtraCurricularActivities.Where(x => x.ExtraCurricularActivity.Organization != null).ToList();
                        List<TeacherWishExtraCuricularActivityViewModel> teacherWishExtraCuricularActivityViewModelList = new List<TeacherWishExtraCuricularActivityViewModel>();
                        if (teacherWishExtraCurricularActivitityList.Any())
                        {
                            foreach (TeacherExtraCurricularActivity teacherExtraCurricularActivity in teacherWishExtraCurricularActivitityList)
                            {
                                TeacherWishExtraCuricularActivityViewModel teacherWishExtraCuricularActivityViewModel = new TeacherWishExtraCuricularActivityViewModel();
                                teacherWishExtraCuricularActivityViewModel.ExtraCuricularActivityId = teacherExtraCurricularActivity.ExtraCurricularActivity.Id;
                                teacherWishExtraCuricularActivityViewModel.ExtraCuricularActivityName = teacherExtraCurricularActivity.ExtraCurricularActivity.Name;
                                teacherWishExtraCuricularActivityViewModel.TeacherExtraCuricularActivityId = teacherExtraCurricularActivity.Id;
                                teacherWishExtraCuricularActivityViewModelList.Add(teacherWishExtraCuricularActivityViewModel);
                            }
                        }
                        teacherViewModel.TeacherWishExtraCuricularActivity = teacherWishExtraCuricularActivityViewModelList;

                        #endregion

                        #region Image

                        teacherViewModel.Images = null;
                        TeacherImages teacherImage = teacher.TeacherImageses.Where(x => x.Status == TeacherImages.EntityStatus.Active).Take(1).SingleOrDefault();
                        if (teacherImage != null)
                        {
                            TeacherMediaImage mediaImage = _teacherImageMediaService.GetByRefId(teacherImage.Id);
                            if (mediaImage != null)
                            {
                                teacherViewModel.Images = mediaImage.Images;
                            }
                        }

                        #endregion

                        #endregion

                        ViewBag.teacherViewModel = teacherViewModel;
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.isPost = false;
                _logger.Error(ex);
            }
            return View();
        }

        #endregion

        #region Common Method for Both Save and Update teacher

        [HttpPost]
        public JsonResult SaveTeacher(TeacherViewModel formData)
        {
            bool isNew = true;
            int tpin = 0;
            bool successState = false;
            string message = "";
            string imageSrc = "";
            try
            {
                DateTime date = DateTime.Now;
                long createdBy = Convert.ToInt64(HttpContext.User.Identity.GetUserId());
                long modifyBy = createdBy;
                Teacher teacher = new Teacher();

                //quick information here 
                #region quick information

                teacher.Id = formData.Id;
                teacher.Tpin = formData.Tpin;
                teacher.NickName = formData.NickName;
                teacher.PersonalMobile = formData.MobileNumber1;
                teacher.AlternativeMobile = formData.MobileNumber2;
                teacher.Institute = formData.Institute;
                teacher.Department = formData.Department;
                teacher.HscPassingYear = formData.HscPassingYear;
                teacher.Religion = formData.Religion;
                teacher.Gender = formData.Gender;

                #region Teacher Organization

                if (!String.IsNullOrEmpty(formData.TeacherOrganizationString))
                {
                    formData.TeacherOrganization = new JavaScriptSerializer().Deserialize<List<TeacherOrganizationViewModel>>(formData.TeacherOrganizationString);
                    if (formData.TeacherOrganization.Any())
                    {
                        List<Organization> organizationList = new List<Organization>();
                        foreach (TeacherOrganizationViewModel teacherOrganization in formData.TeacherOrganization)
                        {
                            Organization organization = _organizationService.LoadById(teacherOrganization.OrganizationId);
                            organizationList.Add(organization);
                        }
                        teacher.Organizations = organizationList;
                    }
                }

                #endregion

                #region Teacher Activity

                if (!String.IsNullOrEmpty(formData.TeacherActivityString))
                {
                    formData.TeacherActivity = new JavaScriptSerializer().Deserialize<List<TeacherActivityViewModel>>(formData.TeacherActivityString);
                    if (formData.TeacherActivity.Any())
                    {
                        List<TeacherActivityPriority> teacherActivityPriorityList = new List<TeacherActivityPriority>();
                        foreach (TeacherActivityViewModel teacherActivityViewModel in formData.TeacherActivity)
                        {
                            TeacherActivityPriority teacherActivity = new TeacherActivityPriority
                            {
                                Teacher = teacher,
                                TeacherActivity = _teacherActivityService.GetTeacherActivity(teacherActivityViewModel.ActivityId),
                                Priority = teacherActivityViewModel.Priority,
                                CreationDate = date,
                                ModificationDate = date,
                                CreateBy = createdBy,
                                ModifyBy = modifyBy,
                                Status = TeacherActivityPriority.EntityStatus.Active
                            };
                            teacherActivityPriorityList.Add(teacherActivity);
                        }
                        teacher.TeacherActivityPriorities = teacherActivityPriorityList;
                    }
                }

                #endregion

                #region Teacher Subject priority

                if (!String.IsNullOrEmpty(formData.TeacherSubjectPriorityString))
                {
                    formData.TeacherSubjectPriority = new JavaScriptSerializer().Deserialize<List<TeacherSubjectPriorityViewModel>>(formData.TeacherSubjectPriorityString);
                    if (formData.TeacherSubjectPriority.Any())
                    {
                        List<TeacherSubjectPriority> teacherSubjectPriorityList = new List<TeacherSubjectPriority>();
                        foreach (TeacherSubjectPriorityViewModel teacherSubjectPriorityViewModel in formData.TeacherSubjectPriority)
                        {
                            TeacherSubjectPriority teacherSubjectPriority = new TeacherSubjectPriority
                            {
                                Teacher = teacher,
                                Subject = _subjectService.GetSubject(teacherSubjectPriorityViewModel.SubjectId),
                                Priority = teacherSubjectPriorityViewModel.Priority,
                                CreationDate = date,
                                ModificationDate = date,
                                CreateBy = createdBy,
                                ModifyBy = modifyBy,
                                Status = TeacherSubjectPriority.EntityStatus.Active
                            };
                            teacherSubjectPriorityList.Add(teacherSubjectPriority);
                        }
                        teacher.TeacherSubjectPriorities = teacherSubjectPriorityList;
                    }
                }

                #endregion

                #region Teacher Version priority

                if (!String.IsNullOrEmpty(formData.TeacherVersionOfStudyPriorityString))
                {
                    formData.TeacherVersionOfStudyPriority = new JavaScriptSerializer().Deserialize<List<TeacherVersionOfStudyPriorityViewModel>>(formData.TeacherVersionOfStudyPriorityString);
                    if (formData.TeacherVersionOfStudyPriority.Any())
                    {
                        List<TeacherVersionOfStudyPriority> teacherVersionOfStudyPriorityList = new List<TeacherVersionOfStudyPriority>();
                        foreach (TeacherVersionOfStudyPriorityViewModel teacherVersionOfStudyPriorityViewModel in formData.TeacherVersionOfStudyPriority)
                        {
                            TeacherVersionOfStudyPriority teacherVersionOfStudyPriority = new TeacherVersionOfStudyPriority
                            {
                                Teacher = teacher,
                                VersionOfStudy = teacherVersionOfStudyPriorityViewModel.VersionOfStudyId,
                                Priority = teacherVersionOfStudyPriorityViewModel.Priority,
                                CreationDate = date,
                                ModificationDate = date,
                                CreateBy = createdBy,
                                ModifyBy = modifyBy,
                                Status = TeacherVersionOfStudyPriority.EntityStatus.Active
                            };
                            teacherVersionOfStudyPriorityList.Add(teacherVersionOfStudyPriority);
                        }
                        teacher.TeacherVersionOfStudyPriorities = teacherVersionOfStudyPriorityList;
                    }
                }

                #endregion

                #endregion

                //details Infirmation here 

                #region details Information

                teacher.FullName = formData.FullName;
                teacher.FatherName = formData.FatherName;
                if (!String.IsNullOrEmpty(formData.DateOfBirth))
                {
                    string[] splitDate = formData.DateOfBirth.Split('/');
                    if (splitDate.Length == 3 && Convert.ToInt16(splitDate[2]) > 1980 && Convert.ToInt16(splitDate[1]) > 0 && Convert.ToInt16(splitDate[2]) > 0)
                    {
                        DateTime birthDay = new DateTime(Convert.ToInt16(splitDate[2]), Convert.ToInt16(splitDate[1]),Convert.ToInt16(splitDate[0]));
                        teacher.DateOfBirth = birthDay;
                    }
                    ////string dateTimeForBathDay = Convert.ToDateTime(formData.DateOfBirth).Date.ToString("yyyy-MM-dd");
                    //teacher.DateOfBirth = Convert.ToDateTime(formData.DateOfBirth, CultureInfo.CurrentCulture);
                }
                if (formData.BloodGroup > 0)
                {
                    teacher.BloodGroup = formData.BloodGroup;
                }
                teacher.RoomMatesMobile = formData.MobileNumberRoommate;
                teacher.FathersMobile = formData.MobileNumberFather;
                teacher.MothersMobile = formData.MobileNumberMother;
                teacher.Email = formData.Email;
                teacher.FbId = formData.FacebookId;
                teacher.PresentAddress = formData.PresentAddress;
                teacher.Area = formData.Area;
                teacher.PermanentAddress = formData.PermanentAddress;

                #region Teacher Admission Info

                if (!String.IsNullOrEmpty(formData.TeacherAdmissionString))
                {
                    formData.TeacherAdmission = new JavaScriptSerializer().Deserialize<List<TeacherAdmissionViewModel>>(formData.TeacherAdmissionString);
                    if (formData.TeacherAdmission.Any())
                    {
                        List<TeacherAdmissionInfo> teacherAdmissionInfoList = new List<TeacherAdmissionInfo>();
                        foreach (TeacherAdmissionViewModel teacherAdmissionViewModel in formData.TeacherAdmission)
                        {
                            TeacherAdmissionInfo teacherAdmissionInfo = new TeacherAdmissionInfo();
                            switch (teacherAdmissionViewModel.InstituteName)
                            {
                                case "BUET":
                                    teacherAdmissionInfo.UniversityName = UniversityText.BUET;
                                    break;
                                case "MEDICAL":
                                    teacherAdmissionInfo.UniversityName = UniversityText.MEDICAL;
                                    break;
                                case "DU":
                                    teacherAdmissionInfo.UniversityName = UniversityText.DU;
                                    break;
                                default:
                                    teacherAdmissionInfo.UniversityName = UniversityText.SUST;
                                    break;
                            }
                            teacherAdmissionInfo.Teacher = teacher;
                            teacherAdmissionInfo.AtPosition = Convert.ToInt32(teacherAdmissionViewModel.AtPosition);
                            teacherAdmissionInfo.AtSession = Convert.ToInt32(teacherAdmissionViewModel.AtSession);
                            teacherAdmissionInfo.GotSubject = teacherAdmissionViewModel.GotSubject;
                            teacherAdmissionInfo.CreationDate = date;
                            teacherAdmissionInfo.ModificationDate = date;
                            teacherAdmissionInfo.CreateBy = createdBy;
                            teacherAdmissionInfo.ModifyBy = modifyBy;
                            teacherAdmissionInfo.Status = TeacherAdmissionInfo.EntityStatus.Active;

                            teacherAdmissionInfoList.Add(teacherAdmissionInfo);
                        }
                        teacher.TeacherAdmissionInfos = teacherAdmissionInfoList;
                    }
                }

                #endregion

                #region Teacher Previous Extra Curricular Activities

                List<TeacherExtraCurricularActivity> teacherExtraCurricularActivityList = new List<TeacherExtraCurricularActivity>();
                if (!String.IsNullOrEmpty(formData.TeacherPreviousExtraCuricularActivityString))
                {
                    formData.TeacherPreviousExtraCuricularActivity = new JavaScriptSerializer().Deserialize<List<TeacherPreviousExtraCuricularActivityViewModel>>(formData.TeacherPreviousExtraCuricularActivityString);
                    if (formData.TeacherPreviousExtraCuricularActivity.Any())
                    {
                        foreach (TeacherPreviousExtraCuricularActivityViewModel teacherPreviousExtraCuricularActivityViewModel in formData.TeacherPreviousExtraCuricularActivity)
                        {
                            TeacherExtraCurricularActivity teacherExtraCurricularActivity = new TeacherExtraCurricularActivity
                            {
                                Teacher = teacher,
                                ExtraCurricularActivity = _extraCarricularActivitiesService.GetExtraCurricularActivity(teacherPreviousExtraCuricularActivityViewModel.ExtraCuricularActivityId),
                                CreationDate = date,
                                ModificationDate = date,
                                CreateBy = createdBy,
                                ModifyBy = modifyBy,
                                Status = TeacherExtraCurricularActivity.EntityStatus.Active
                            };
                            teacherExtraCurricularActivityList.Add(teacherExtraCurricularActivity);
                        }
                    }
                }

                #endregion

                #region Teacher Wish Extra Curricular Activities

                if (!String.IsNullOrEmpty(formData.TeacherWishExtraCuricularActivityString))
                {
                    formData.TeacherWishExtraCuricularActivity = new JavaScriptSerializer().Deserialize<List<TeacherWishExtraCuricularActivityViewModel>>(formData.TeacherWishExtraCuricularActivityString);
                    if (formData.TeacherWishExtraCuricularActivity.Any())
                    {
                        foreach (TeacherWishExtraCuricularActivityViewModel teacherWishExtraCuricularActivityViewModel in formData.TeacherWishExtraCuricularActivity)
                        {
                            TeacherExtraCurricularActivity teacherExtraCurricularActivity = new TeacherExtraCurricularActivity
                            {
                                Teacher = teacher,
                                ExtraCurricularActivity = _extraCarricularActivitiesService.GetExtraCurricularActivity(teacherWishExtraCuricularActivityViewModel.ExtraCuricularActivityId),
                                CreationDate = date,
                                ModificationDate = date,
                                CreateBy = createdBy,
                                ModifyBy = modifyBy,
                                Status = TeacherExtraCurricularActivity.EntityStatus.Active
                            };
                            teacherExtraCurricularActivityList.Add(teacherExtraCurricularActivity);
                        }
                    }
                }

                #endregion

                if (teacherExtraCurricularActivityList.Any())
                {
                    teacher.TeacherExtraCurricularActivities = teacherExtraCurricularActivityList;
                }

                #endregion

                if (teacher.Id > 0)
                {
                    //for updated teacher
                    _teacherService.UpdateTeacher(teacher, formData.ImageUpload);

                    //after update teacher re render image if is updated
                    if (formData.ImageUpload != null)
                    {
                        TeacherImages teacherImages = _teacherService.GetTeacherImageByTeacherId(teacher.Id);
                        if (teacherImages != null)
                        {
                            TeacherMediaImage teacherMediaImages = _teacherImageMediaService.GetByRefId(teacherImages.Id);
                            string imageBase64 = Convert.ToBase64String(teacherMediaImages.Images);
                            imageSrc = string.Format("data:image/gif;base64,{0}", imageBase64);
                        }
                    }
                    message = "Teacher Successfully Updated";
                    isNew = false;
                }
                else
                {
                    //for add new teacher
                    _teacherService.SaveTeacher(teacher, formData.ImageUpload);
                    message = "Teacher Created Successfully. Teacher Nick Name: " + teacher.NickName + " & TPIN: " + teacher.Tpin;
                }
                successState = true;
                tpin = teacher.Tpin;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }

            return Json(new { IsSuccess = successState, Message = message, New = isNew, tpin = tpin, imageSrc = imageSrc });
        }

        #endregion

        #region TeacherList

        [HttpGet]
        public ActionResult TeacherList()
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<SelectListItem> orgList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName").ToList();
                List<SelectListItem> teacherActivity = new SelectList(_teacherActivityService.LoadTeacherActivity(), "Id", "Name").ToList();
                List<SelectListItem> teachersByHscYear = new SelectList(_teacherService.LoadTeacher(), "Id", "HscPassingYear").ToList();
                List<SelectListItem> subjects = new SelectList(_subjectService.LoadSubject(), "Id", "Name").ToList();

                orgList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Organization" });
                teacherActivity.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Teacher Type" });
                teachersByHscYear.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All HSC Passing YEar" });
                subjects.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Subjects" });

                ViewBag.OrgList = orgList;
                ViewBag.TeacherActivity = teacherActivity;
                ViewBag.HscPassingYear = teachersByHscYear;
                ViewBag.Subjects = subjects;

                List<SelectListItem> verPrioritySelectList = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(new List<int>((int)VersionOfStudy.Combined)), "Key", "Value").ToList();
                verPrioritySelectList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Version Priority" });

                List<SelectListItem> genderselectList = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(new List<int>((int)Gender.Combined)), "Key", "Value").ToList();
                genderselectList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Gender" });

                List<SelectListItem> religionSelectList = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(), "Key", "Value").ToList();
                religionSelectList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Religion" });

                ViewBag.ReligionList = religionSelectList;
                ViewBag.GenderList = genderselectList;
                ViewBag.VersionOfPriorityList = verPrioritySelectList;
            }
            catch (Exception)
            {
                throw;
            }
            return View();
        }
        [HttpPost]
        public ActionResult TeacherList(TeacherViewList teacherViewLists)
        {
            try
            {
                if (teacherViewLists.InformationViewList == null)
                {
                    teacherViewLists.InformationViewList = new[]
                        {
                            "TPIN",
                            "Organization",
                            "Full Name",
                            "Nick Name",
                            "Father Name",
                            "Institute",
                            "Mobile Number 1",
                            "Mobile Number 2",
                            "Mobile Number(Room Mate)",
                            "Mobile Number(Father)",
                            "Mobile Number(Mother)",
                            "Religion",
                            "Gender",
                            "Date Of Birth",
                            "Blood Group",
                            "Department",
                            "Hsc Passing Year",
                            "Email",
                            "Activity Priority",
                            "Subject Priority",
                            "Version Priority"

                        };
                }
                ViewBag.informationViewList = teacherViewLists.InformationViewList;
                var json = new JavaScriptSerializer().Serialize(teacherViewLists);
                ViewData["teacherListViewModelData"] = json;
                return View("TeacherReportList");
                //ViewBag.informationViewList = teacherViewLists.InformationViewList;
                //var json = new JavaScriptSerializer().Serialize(teacherViewLists);
                //ViewData["teacherListViewModelData"] = json;
                //return View("TeacherReportList");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public JsonResult TeacherListCount(long[] organizations, long[] teacherActivities, long[] subjects, int[] versionOfPriority, int[] religion, int[] gender)
        {
            try
            {
                int teacherCount = _teacherService.GetTeacherCount(organizations, teacherActivities, subjects, religion, versionOfPriority, gender);
                return Json(new { teacherCount = teacherCount, IsSuccess = true });
            }
            catch (Exception ex)
            {

                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult GenerateTeacherList(int draw, int start, int length, string teacherListObj, string nickName = "", string instutute = "", string personalM = "", string fatherM = "", string tpin = "")
        {
            try
            {
                var teacherObj = new JavaScriptSerializer().Deserialize<TeacherViewList>(teacherListObj);
                NameValueCollection nvc = Request.Form;
                var orderBy = "";
                var orderDir = "";
                #region OrderBy and Direction
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "1":
                            orderBy = "Tpin";
                            break;
                        case "2":
                            orderBy = "o.Name";
                            break;
                        case "3":
                            orderBy = "FullName";
                            break;
                        case "4":
                            orderBy = "NickName";
                            break;
                        case "5":
                            orderBy = "FatherName";
                            break;
                        case "6":
                            orderBy = "Institute";
                            break;
                        default:
                            orderBy = "Tpin";
                            break;
                    }
                }
                #endregion
                int rowCount = _teacherService.GetTeacherCount(teacherObj.Organizations, teacherObj.TeacherActivities, teacherObj.Subjects, teacherObj.Religion, teacherObj.VersionOfPriority, teacherObj.Gender, nickName, instutute, personalM, fatherM, tpin);
                IList<Teacher> teachers = _teacherService.LoadTeacher(teacherObj.Organizations, teacherObj.TeacherActivities, teacherObj.Subjects, teacherObj.Religion, teacherObj.VersionOfPriority, teacherObj.Gender, nickName, instutute, personalM, fatherM, tpin, start, length, orderBy, orderDir.ToUpper());
                int recordsTotal = rowCount;
                long recordsFiltered = recordsTotal;
                var data = new List<object>();
                int sl = start;
                foreach (var individualTeacheher in teachers)
                {
                    sl++;
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    if (teacherObj.InformationViewList != null && teacherObj.InformationViewList.Count() > 0)
                    {
                        foreach (string informationView in teacherObj.InformationViewList)
                        {
                            switch (informationView)
                            {
                                case "TPIN":
                                    str.Add(individualTeacheher.Tpin.ToString());
                                    break;
                                case "Organization":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Organizations.ToString()))
                                    {
                                        var orgShortName = "";
                                        foreach (var org in individualTeacheher.Organizations)
                                        {
                                            orgShortName += org.ShortName + ","
                                                ;
                                        }
                                        str.Add(orgShortName.Remove(orgShortName.Length - 1));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Full Name":
                                    if (!string.IsNullOrEmpty(individualTeacheher.FullName))
                                    {
                                        str.Add(individualTeacheher.FullName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Nick Name":
                                    if (!string.IsNullOrEmpty(individualTeacheher.NickName))
                                    {
                                        str.Add(individualTeacheher.NickName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Father Name":
                                    if (!string.IsNullOrEmpty(individualTeacheher.FatherName))
                                    {
                                        str.Add(individualTeacheher.FatherName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Institute":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Institute))
                                    {
                                        str.Add(individualTeacheher.Institute);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number 1":
                                    if (!string.IsNullOrEmpty(individualTeacheher.PersonalMobile))
                                    {
                                        str.Add(individualTeacheher.PersonalMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number 2":
                                    if (!string.IsNullOrEmpty(individualTeacheher.AlternativeMobile))
                                    {
                                        str.Add(individualTeacheher.AlternativeMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number(Room Mate)":
                                    if (!string.IsNullOrEmpty(individualTeacheher.RoomMatesMobile))
                                    {
                                        str.Add(individualTeacheher.RoomMatesMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number(Father)":
                                    if (!string.IsNullOrEmpty(individualTeacheher.FathersMobile))
                                    {
                                        str.Add(individualTeacheher.FathersMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number(Mother)":
                                    if (!string.IsNullOrEmpty(individualTeacheher.MothersMobile))
                                    {
                                        str.Add(individualTeacheher.MothersMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;

                                case "Religion":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Religion.ToString()))
                                    {
                                        if (individualTeacheher.Religion == (int)Religion.Islam)
                                        {
                                            str.Add("Islam");
                                        }
                                        else if (individualTeacheher.Religion == (int)Religion.Hinduism)
                                        {
                                            str.Add("Hinduism");
                                        }
                                        else if (individualTeacheher.Religion == (int)Religion.Buddhism)
                                        {
                                            str.Add("Buddhism");
                                        }
                                        else if (individualTeacheher.Religion == (int)Religion.Christianity)
                                        {
                                            str.Add("Christianity");
                                        }
                                        else if (individualTeacheher.Religion == (int)Religion.Other)
                                        {
                                            str.Add("Other");
                                        }
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Gender":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Gender.ToString()))
                                    {
                                        if (individualTeacheher.Gender == (int)Gender.Male)
                                        {
                                            str.Add("Male");
                                        }
                                        else if (individualTeacheher.Gender == (int)Gender.Female)
                                        {
                                            str.Add("Female");
                                        }
                                        else if (individualTeacheher.Religion == (int)Gender.Combined)
                                        {
                                            str.Add("Combined");
                                        }
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Date Of Birth":

                                    if (!string.IsNullOrEmpty(individualTeacheher.DateOfBirth.ToString()))
                                    {
                                        str.Add(individualTeacheher.DateOfBirth.Value.ToString("dd-MMM-yyyy"));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Blood Group":
                                    if (!String.IsNullOrEmpty(individualTeacheher.BloodGroup.ToString()))
                                    {
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.ABneg)
                                        {
                                            str.Add("AB-");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.ABpos)
                                        {
                                            str.Add("AB+");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Aneg)
                                        {
                                            str.Add("A-");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Apos)
                                        {
                                            str.Add("A+");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Bneg)
                                        {
                                            str.Add("B-");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Bpos)
                                        {
                                            str.Add("B+");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Oneg)
                                        {
                                            str.Add("O-");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Opos)
                                        {
                                            str.Add("O+");
                                        }
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Department":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Department.ToString()))
                                    {
                                        str.Add(individualTeacheher.Department);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Hsc Passing Year":
                                    if (!string.IsNullOrEmpty(individualTeacheher.HscPassingYear.ToString()))
                                    {
                                        str.Add(individualTeacheher.HscPassingYear.ToString());
                                        break;
                                    }
                                    str.Add("");
                                    break;


                                case "Email":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Email))
                                    {
                                        str.Add(individualTeacheher.Email);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Activity Priority":
                                    if (individualTeacheher.TeacherActivityPriorities.Count > 0)
                                    {
                                        var activityPriorityList = new List<string>();
                                        foreach (var activityPriority in individualTeacheher.TeacherActivityPriorities)
                                        {
                                            activityPriorityList.Add(activityPriority.TeacherActivity.Name);

                                        }
                                        str.Add(string.Join(", ", activityPriorityList));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Subject Priority":
                                    if (individualTeacheher.TeacherSubjectPriorities.Count > 0)
                                    {
                                        var subjectPriorityList = new List<string>();
                                        foreach (var subjectPriority in individualTeacheher.TeacherSubjectPriorities)
                                        {
                                            subjectPriorityList.Add(subjectPriority.Subject.Name);

                                        }
                                        str.Add(string.Join(", ", subjectPriorityList));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Version Priority":
                                    if (individualTeacheher.TeacherVersionOfStudyPriorities.Count > 0)
                                    {
                                        var versionPriorityList = new List<string>();
                                        foreach (
                                            var versionPriority in individualTeacheher.TeacherVersionOfStudyPriorities)
                                        {

                                            var versionOfStudy = (VersionOfStudy)versionPriority.VersionOfStudy;
                                            string versionOfStudyString = versionOfStudy.ToString();
                                            versionPriorityList.Add(versionOfStudyString);
                                        }
                                        str.Add(string.Join(", ", versionPriorityList));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    else
                    {
                        str.Add(individualTeacheher.Tpin.ToString());
                        str.Add(individualTeacheher.NickName);
                        str.Add(individualTeacheher.Institute);
                        str.Add(individualTeacheher.PersonalMobile.ToString());

                        if (!String.IsNullOrEmpty(individualTeacheher.FathersMobile))
                        {
                            str.Add(individualTeacheher.FathersMobile.ToString());
                        }
                        else
                        {
                            str.Add("");
                        }
                        string subject = "";
                        foreach (var teacherSub in individualTeacheher.TeacherSubjectPriorities)
                        {
                            subject += _subjectService.GetSubject(teacherSub.Subject.Id).ShortName.Trim() + ", ";

                        }
                        str.Add(subject.Remove(subject.Length - 2, 1));
                    }
                    data.Add(str);
                }
                var json = new JavaScriptSerializer().Serialize(teacherObj);
                ViewData["teacherListViewModelData"] = json;

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        #endregion

        #region Teacher Search Form

        [HttpGet]
        public ActionResult TeacherSearch()
        {
            try
            {
                List<UserMenu> userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<SelectListItem> organizationSelectList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName").ToList();
                organizationSelectList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All" });
                List<SelectListItem> informationViewSelectList = new SelectList(TeacherSearchConstants.GetSearchTeacherInformationStringList().Select(x => new SelectListItem() { Text = x, Value = x }), "Text", "Value").ToList();

                ViewBag.OrganizationSelectList = organizationSelectList;
                ViewBag.InformationViewSelectList = informationViewSelectList;
            }
            catch (Exception)
            {
                throw;
            }
            return View();
        }

        [HttpPost]
        public ActionResult TeacherSearch(TeacherSearchList teacherSearchList)
        {
            try
            {
                if (teacherSearchList.InformationViewList == null)
                {
                    teacherSearchList.InformationViewList = new List<string>() { "TPIN", "Nick Name" };
                }
                ViewBag.InformationViewList = teacherSearchList.InformationViewList;
                var json = new JavaScriptSerializer().Serialize(teacherSearchList);
                ViewData["TeacherSearchListViewModel"] = json;
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Problem occurred during Teacher search";
            }
            return View("TeacherSearchReport");
        }

        [HttpPost]
        public JsonResult TeacherSearchReportDataTable(int draw, int start, int length, string teacherSearchListViewModel = "", string tpin = "", string nickName = "", string mobileNumber1 = "", string subjectPrority = "")
        {
            List<UserMenu> userMenu = (List<UserMenu>)ViewBag.UserMenu;
            int recordsTotal = 0;
            int recordsFiltered = 0;
            var data = new List<object>();
            List<string> informationViewList = new List<string>() { "TPIN", "Nick Name" };
            try
            {
                TeacherSearchList teacherSearchList =
                    new JavaScriptSerializer().Deserialize<TeacherSearchList>(teacherSearchListViewModel);
                informationViewList = teacherSearchList.InformationViewList;

                #region datatable

                NameValueCollection nvc = Request.Form;
                var orderBy = "";
                var orderDir = "";

                #region OrderBy and Direction

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        default:
                            orderBy = "Tpin";
                            break;
                    }
                }

                #endregion

                IList<Teacher> teachers = _teacherService.LoadTeacherByKeyWordSearch(userMenu, start, length, orderBy, orderDir.ToUpper(), teacherSearchList.OrganizationIds, teacherSearchList.Keyword, teacherSearchList.InformationViewList, tpin, nickName, mobileNumber1, subjectPrority);

                int rowCount = _teacherService.GetTeacherByKeyWordSearchCount(userMenu, teacherSearchList.OrganizationIds, teacherSearchList.Keyword, teacherSearchList.InformationViewList, tpin, nickName, mobileNumber1, subjectPrority);
                recordsTotal = rowCount;
                recordsFiltered = recordsTotal;

                int sl = start;
                foreach (var individualTeacheher in teachers)
                {
                    sl++;
                    List<string> str = new List<string> { sl.ToString() };

                    #region datatable
                    if (informationViewList != null && informationViewList.Any())
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case TeacherSearchConstants.Tpin:
                                    str.Add(individualTeacheher.Tpin.ToString());
                                    break;
                                case TeacherSearchConstants.NickName:
                                    if (!string.IsNullOrEmpty(individualTeacheher.NickName))
                                    {
                                        str.Add(individualTeacheher.NickName);
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherSearchConstants.MobileNumber1:
                                    if (!string.IsNullOrEmpty(individualTeacheher.PersonalMobile))
                                    {
                                        str.Add(individualTeacheher.PersonalMobile);
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherSearchConstants.Organization:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Organizations.ToString()))
                                    {
                                        var orgShortName = "";
                                        foreach (var org in individualTeacheher.Organizations)
                                        {
                                            orgShortName += org.ShortName + ",";
                                        }
                                        str.Add(orgShortName.Remove(orgShortName.Length - 1));
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherSearchConstants.FullName:
                                    if (!string.IsNullOrEmpty(individualTeacheher.FullName))
                                    {
                                        str.Add(individualTeacheher.FullName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.FatherName:
                                    if (!string.IsNullOrEmpty(individualTeacheher.FatherName))
                                    {
                                        str.Add(individualTeacheher.FatherName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.Institute:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Institute))
                                    {
                                        str.Add(individualTeacheher.Institute);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.MobileNumber2:
                                    if (!string.IsNullOrEmpty(individualTeacheher.AlternativeMobile))
                                    {
                                        str.Add(individualTeacheher.AlternativeMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.RoomMateMobileNumber:
                                    if (!string.IsNullOrEmpty(individualTeacheher.RoomMatesMobile))
                                    {
                                        str.Add(individualTeacheher.RoomMatesMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.FatherMobileNumber:
                                    if (!string.IsNullOrEmpty(individualTeacheher.FathersMobile))
                                    {
                                        str.Add(individualTeacheher.FathersMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.MotherMobileNumber:
                                    if (!string.IsNullOrEmpty(individualTeacheher.MothersMobile))
                                    {
                                        str.Add(individualTeacheher.MothersMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;

                                case TeacherSearchConstants.Religion:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Religion.ToString()))
                                    {
                                        str.Add(_commonHelper.GetEmumIdToValue<Religion>((int)individualTeacheher.Religion));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.Gender:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Gender.ToString()))
                                    {
                                        str.Add(_commonHelper.GetEmumIdToValue<Gender>((int)individualTeacheher.Gender));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.DateOfBirth:

                                    if (!string.IsNullOrEmpty(individualTeacheher.DateOfBirth.ToString()))
                                    {
                                        str.Add(individualTeacheher.DateOfBirth.Value.ToString("dd-MMM-yyyy"));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.BloodGroup:
                                    if (!String.IsNullOrEmpty(individualTeacheher.BloodGroup.ToString()))
                                    {
                                        str.Add(_commonHelper.GetEmumIdToValue<BloodGroup>((int)individualTeacheher.BloodGroup));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.Department:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Department.ToString()))
                                    {
                                        str.Add(individualTeacheher.Department);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.HscPassingYear:
                                    if (!string.IsNullOrEmpty(individualTeacheher.HscPassingYear.ToString()))
                                    {
                                        str.Add(individualTeacheher.HscPassingYear.ToString());
                                        break;
                                    }
                                    str.Add("");
                                    break;


                                case TeacherSearchConstants.Email:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Email))
                                    {
                                        str.Add(individualTeacheher.Email);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.ActivityPriority:
                                    if (individualTeacheher.TeacherActivityPriorities.Count > 0)
                                    {
                                        var activityPriorityList = individualTeacheher.TeacherActivityPriorities.Select(activityPriority => activityPriority.TeacherActivity.Name).ToList();
                                        str.Add(string.Join(", ", activityPriorityList));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.SubjectPriority:
                                    if (individualTeacheher.TeacherSubjectPriorities.Count > 0)
                                    {
                                        var subjectPriorityList = individualTeacheher.TeacherSubjectPriorities.Select(subjectPriority => subjectPriority.Subject.Name).ToList();
                                        str.Add(string.Join(", ", subjectPriorityList));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case TeacherSearchConstants.VersionPriority:
                                    if (individualTeacheher.TeacherVersionOfStudyPriorities.Count > 0)
                                    {
                                        var versionPriorityList = individualTeacheher.TeacherVersionOfStudyPriorities.Select(versionPriority => (VersionOfStudy)versionPriority.VersionOfStudy).Select(versionOfStudy => versionOfStudy.ToString()).ToList();
                                        str.Add(string.Join(", ", versionPriorityList));
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                default:
                                    break;
                            }

                        }
                    }
                    #endregion

                    data.Add(str);
                }

                #endregion
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Problem occurred during Teacher search";
            }

            return Json(new { draw = draw, recordsTotal = recordsTotal, recordsFiltered = recordsFiltered, start = start, length = length, data = data });
        }
        #endregion

        #region ExcelExport

        public ActionResult ExportTeacherList(string teacherListObj, string nickName = "", string instutute = "", string personalM = "", string fatherM = "", string tpin = "")
        {
            try
            {
                List<string> headerList = new List<string>();
                headerList.Add("Teacher List");
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                var teacherObj = new JavaScriptSerializer().Deserialize<TeacherViewList>(teacherListObj);
                if (teacherObj.InformationViewList != null)
                {

                    foreach (var information in teacherObj.InformationViewList)
                    {
                        columnList.Add(information);
                    }

                }
                IList<Teacher> teachers = _teacherService.LoadTeacher(teacherObj.Organizations, teacherObj.TeacherActivities, teacherObj.Subjects, teacherObj.Religion, teacherObj.VersionOfPriority, teacherObj.Gender, nickName, instutute, personalM, fatherM, "", null, null, "Tpin", "ASC");
                var teacherXlsList = new List<List<object>>();
                foreach (var individualTeacheher in teachers)
                {
                    var xlsRow = new List<object>();

                    #region All Row
                    if (teacherObj.InformationViewList != null && teacherObj.InformationViewList.Any())
                    {
                        foreach (string informationView in teacherObj.InformationViewList)
                        {
                            switch (informationView)
                            {
                                case "TPIN":
                                    xlsRow.Add(individualTeacheher.Tpin.ToString());
                                    break;
                                case "Organization":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Organizations.ToString()))
                                    {
                                        var orgShortName = "";
                                        foreach (var org in individualTeacheher.Organizations)
                                        {
                                            orgShortName += org.ShortName + ","
                                                ;
                                        }
                                        xlsRow.Add(orgShortName.Remove(orgShortName.Length - 1));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;

                                case "Full Name":
                                    if (!string.IsNullOrEmpty(individualTeacheher.FullName))
                                    {
                                        xlsRow.Add(individualTeacheher.FullName);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Nick Name":
                                    if (!string.IsNullOrEmpty(individualTeacheher.NickName))
                                    {
                                        xlsRow.Add(individualTeacheher.NickName);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Father Name":
                                    if (!string.IsNullOrEmpty(individualTeacheher.FatherName))
                                    {
                                        xlsRow.Add(individualTeacheher.FatherName);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Institute":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Institute))
                                    {
                                        xlsRow.Add(individualTeacheher.Institute);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Mobile Number 1":
                                    if (!string.IsNullOrEmpty(individualTeacheher.PersonalMobile))
                                    {
                                        xlsRow.Add(individualTeacheher.PersonalMobile);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Mobile Number 2":
                                    if (!string.IsNullOrEmpty(individualTeacheher.AlternativeMobile))
                                    {
                                        xlsRow.Add(individualTeacheher.AlternativeMobile);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Mobile Number(Room Mate)":
                                    if (!string.IsNullOrEmpty(individualTeacheher.RoomMatesMobile))
                                    {
                                        xlsRow.Add(individualTeacheher.RoomMatesMobile);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Mobile Number(Father)":
                                    if (!string.IsNullOrEmpty(individualTeacheher.FathersMobile))
                                    {
                                        xlsRow.Add(individualTeacheher.FathersMobile);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Mobile Number(Mother)":
                                    if (!string.IsNullOrEmpty(individualTeacheher.MothersMobile))
                                    {
                                        xlsRow.Add(individualTeacheher.MothersMobile);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;

                                case "Religion":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Religion.ToString()))
                                    {
                                        if (individualTeacheher.Religion == (int)Religion.Islam)
                                        {
                                            xlsRow.Add("Islam");
                                        }
                                        else if (individualTeacheher.Religion == (int)Religion.Hinduism)
                                        {
                                            xlsRow.Add("Hinduism");
                                        }
                                        else if (individualTeacheher.Religion == (int)Religion.Buddhism)
                                        {
                                            xlsRow.Add("Buddhism");
                                        }
                                        else if (individualTeacheher.Religion == (int)Religion.Christianity)
                                        {
                                            xlsRow.Add("Christianity");
                                        }
                                        else if (individualTeacheher.Religion == (int)Religion.Other)
                                        {
                                            xlsRow.Add("Other");
                                        }
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Gender":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Gender.ToString()))
                                    {
                                        if (individualTeacheher.Gender == (int)Gender.Male)
                                        {
                                            xlsRow.Add("Male");
                                        }
                                        else if (individualTeacheher.Gender == (int)Gender.Female)
                                        {
                                            xlsRow.Add("Female");
                                        }
                                        else if (individualTeacheher.Religion == (int)Gender.Combined)
                                        {
                                            xlsRow.Add("Combined");
                                        }
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Date Of Birth":

                                    if (!string.IsNullOrEmpty(individualTeacheher.DateOfBirth.ToString()))
                                    {
                                        xlsRow.Add(individualTeacheher.DateOfBirth.Value.ToString("dd-MMM-yyyy"));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Blood Group":
                                    if (!String.IsNullOrEmpty(individualTeacheher.BloodGroup.ToString()))
                                    {
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.ABneg)
                                        {
                                            xlsRow.Add("AB-");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.ABpos)
                                        {
                                            xlsRow.Add("AB+");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Aneg)
                                        {
                                            xlsRow.Add("A-");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Apos)
                                        {
                                            xlsRow.Add("A+");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Bneg)
                                        {
                                            xlsRow.Add("B-");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Bpos)
                                        {
                                            xlsRow.Add("B+");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Oneg)
                                        {
                                            xlsRow.Add("O-");
                                        }
                                        if (individualTeacheher.BloodGroup == (int)BloodGroup.Opos)
                                        {
                                            xlsRow.Add("O+");
                                        }
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Department":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Department.ToString()))
                                    {
                                        xlsRow.Add(individualTeacheher.Department);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Hsc Passing Year":
                                    if (!string.IsNullOrEmpty(individualTeacheher.HscPassingYear.ToString()))
                                    {
                                        xlsRow.Add(individualTeacheher.HscPassingYear.ToString());
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;


                                case "Email":
                                    if (!string.IsNullOrEmpty(individualTeacheher.Email))
                                    {
                                        xlsRow.Add(individualTeacheher.Email);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Activity Priority":
                                    if (individualTeacheher.TeacherActivityPriorities.Count > 0)
                                    {
                                        var activityPriorityList = new List<string>();
                                        foreach (var activityPriority in individualTeacheher.TeacherActivityPriorities)
                                        {
                                            activityPriorityList.Add(activityPriority.TeacherActivity.Name);

                                        }
                                        xlsRow.Add(string.Join(", ", activityPriorityList));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Subject Priority":
                                    if (individualTeacheher.TeacherSubjectPriorities.Count > 0)
                                    {
                                        var subjectPriorityList = new List<string>();
                                        foreach (var subjectPriority in individualTeacheher.TeacherSubjectPriorities)
                                        {
                                            subjectPriorityList.Add(subjectPriority.Subject.Name);

                                        }
                                        xlsRow.Add(string.Join(", ", subjectPriorityList));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Version Priority":
                                    if (individualTeacheher.TeacherVersionOfStudyPriorities.Count > 0)
                                    {
                                        var versionPriorityList = new List<string>();
                                        foreach (
                                            var versionPriority in individualTeacheher.TeacherVersionOfStudyPriorities)
                                        {

                                            var versionOfStudy = (VersionOfStudy)versionPriority.VersionOfStudy;
                                            string versionOfStudyString = versionOfStudy.ToString();
                                            versionPriorityList.Add(versionOfStudyString);
                                        }
                                        xlsRow.Add(string.Join(", ", versionPriorityList));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    else
                    {
                        xlsRow.Add(individualTeacheher.NickName);
                        xlsRow.Add(individualTeacheher.Institute);
                        xlsRow.Add(individualTeacheher.PersonalMobile.ToString());

                        if (!String.IsNullOrEmpty(individualTeacheher.FathersMobile))
                        {
                            xlsRow.Add(individualTeacheher.FathersMobile.ToString());
                        }
                        else
                        {
                            xlsRow.Add("");
                        }
                        string subject = "";
                        foreach (var teacherSub in individualTeacheher.TeacherSubjectPriorities)
                        {
                            subject += _subjectService.GetSubject(teacherSub.Subject.Id).ShortName.Trim() + ", ";

                        }
                        xlsRow.Add(subject.Remove(subject.Length - 2, 1));
                    }
                    #endregion
                    teacherXlsList.Add(xlsRow);
                }

                ExcelGenerator.GenerateExcel(headerList, columnList, teacherXlsList, footerList, "TeacherListReport__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("Autoclose");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return View("Autoclose");
            }
        }

        public ActionResult ExportTeacherSearchReport(string teacherSearchListViewModel, string tpin = "", string nickName = "", string mobileNumber1 = "", string subjectPrority = "")
        {
            try
            {
                List<UserMenu> userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<string> headerList = new List<string>();
                headerList.Add("Teacher List");
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                var teacherObj = new JavaScriptSerializer().Deserialize<TeacherSearchList>(teacherSearchListViewModel);
                if (teacherObj.InformationViewList != null)
                {
                    columnList.AddRange(teacherObj.InformationViewList);
                }
                IList<Teacher> teachers = _teacherService.LoadTeacherByKeyWordSearch(userMenu, 0, int.MaxValue, "Tpin", "ASC", teacherObj.OrganizationIds, teacherObj.Keyword, teacherObj.InformationViewList, tpin, nickName, mobileNumber1, subjectPrority);
                var teacherXlsList = new List<List<object>>();
                foreach (var individualTeacheher in teachers)
                {
                    var xlsRow = new List<object>();
                    if (teacherObj.InformationViewList != null && teacherObj.InformationViewList.Any())
                    {
                        foreach (string informationView in teacherObj.InformationViewList)
                        {
                            switch (informationView)
                            {
                                case TeacherSearchConstants.Tpin:
                                    xlsRow.Add(individualTeacheher.Tpin.ToString());
                                    break;
                                case TeacherSearchConstants.NickName:
                                    if (!string.IsNullOrEmpty(individualTeacheher.NickName))
                                    {
                                        xlsRow.Add(individualTeacheher.NickName);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.MobileNumber1:
                                    if (!string.IsNullOrEmpty(individualTeacheher.PersonalMobile))
                                    {
                                        xlsRow.Add(individualTeacheher.PersonalMobile);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.Organization:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Organizations.ToString()))
                                    {
                                        var orgShortName = individualTeacheher.Organizations.Aggregate("", (current, org) => current + (org.ShortName + ","));
                                        xlsRow.Add(orgShortName.Remove(orgShortName.Length - 1));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.FullName:
                                    if (!string.IsNullOrEmpty(individualTeacheher.FullName))
                                    {
                                        xlsRow.Add(individualTeacheher.FullName);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.FatherName:
                                    if (!string.IsNullOrEmpty(individualTeacheher.FatherName))
                                    {
                                        xlsRow.Add(individualTeacheher.FatherName);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.Institute:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Institute))
                                    {
                                        xlsRow.Add(individualTeacheher.Institute);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.MobileNumber2:
                                    if (!string.IsNullOrEmpty(individualTeacheher.AlternativeMobile))
                                    {
                                        xlsRow.Add(individualTeacheher.AlternativeMobile);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.RoomMateMobileNumber:
                                    if (!string.IsNullOrEmpty(individualTeacheher.RoomMatesMobile))
                                    {
                                        xlsRow.Add(individualTeacheher.RoomMatesMobile);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.FatherMobileNumber:
                                    if (!string.IsNullOrEmpty(individualTeacheher.FathersMobile))
                                    {
                                        xlsRow.Add(individualTeacheher.FathersMobile);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.MotherMobileNumber:
                                    if (!string.IsNullOrEmpty(individualTeacheher.MothersMobile))
                                    {
                                        xlsRow.Add(individualTeacheher.MothersMobile);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.Religion:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Religion.ToString()))
                                    {
                                        xlsRow.Add(_commonHelper.GetEmumIdToValue<Religion>((int)individualTeacheher.Religion));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.Gender:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Gender.ToString()))
                                    {
                                        xlsRow.Add(_commonHelper.GetEmumIdToValue<Gender>((int)individualTeacheher.Gender));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.DateOfBirth:
                                    if (!string.IsNullOrEmpty(individualTeacheher.DateOfBirth.ToString()))
                                    {
                                        xlsRow.Add(individualTeacheher.DateOfBirth.Value.ToString("dd-MMM-yyyy"));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.BloodGroup:
                                    if (!String.IsNullOrEmpty(individualTeacheher.BloodGroup.ToString()))
                                    {
                                        xlsRow.Add(_commonHelper.GetEmumIdToValue<BloodGroup>((int)individualTeacheher.BloodGroup));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.Department:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Department.ToString()))
                                    {
                                        xlsRow.Add(individualTeacheher.Department);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.HscPassingYear:
                                    if (!string.IsNullOrEmpty(individualTeacheher.HscPassingYear.ToString()))
                                    {
                                        xlsRow.Add(individualTeacheher.HscPassingYear.ToString());
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.Email:
                                    if (!string.IsNullOrEmpty(individualTeacheher.Email))
                                    {
                                        xlsRow.Add(individualTeacheher.Email);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.ActivityPriority:
                                    if (individualTeacheher.TeacherActivityPriorities.Count > 0)
                                    {
                                        var activityPriorityList = individualTeacheher.TeacherActivityPriorities.Select(activityPriority => activityPriority.TeacherActivity.Name).ToList();
                                        xlsRow.Add(string.Join(", ", activityPriorityList));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.SubjectPriority:
                                    if (individualTeacheher.TeacherSubjectPriorities.Count > 0)
                                    {
                                        var subjectPriorityList = individualTeacheher.TeacherSubjectPriorities.Select(subjectPriority => subjectPriority.Subject.Name).ToList();
                                        xlsRow.Add(string.Join(", ", subjectPriorityList));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case TeacherSearchConstants.VersionPriority:
                                    if (individualTeacheher.TeacherVersionOfStudyPriorities.Count > 0)
                                    {
                                        var versionPriorityList = individualTeacheher.TeacherVersionOfStudyPriorities.Select(versionPriority => (VersionOfStudy) versionPriority.VersionOfStudy).Select(versionOfStudy => versionOfStudy.ToString()).ToList();
                                        xlsRow.Add(string.Join(", ", versionPriorityList));
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    teacherXlsList.Add(xlsRow);
                }

                ExcelGenerator.GenerateExcel(headerList, columnList, teacherXlsList, footerList, "TeacherSearchListReport__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("Autoclose");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return View("Autoclose");
            }
        }

        #endregion

        #region Helper Function

        public ActionResult CheckExistingMobile(string personalMobile)
        {
            try
            {
                bool checkMobileNo = personalMobile != null && _teacherService.IsDuplicateMobileNumber(personalMobile);
                return Json(new { checkMobileNo = checkMobileNo });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(false);
            }
        }

        [HttpPost]
        public ActionResult GetActivities(string idValue)
        {
            try
            {
                if (String.IsNullOrEmpty(idValue))
                {
                    return PartialView("Partial/_WishActivities", new List<ExtraCurricularActivity>());
                }
                idValue = idValue.Trim().Remove(idValue.Length - 1, 1);

                var orgArray = idValue.Split(',');
                IList<ExtraCurricularActivity> orgaExtraCurricularActivities = _extraCarricularActivitiesService.LoadExtraCurricularActivity(orgArray);

                return PartialView("Partial/_WishActivities", orgaExtraCurricularActivities);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion
    }
}