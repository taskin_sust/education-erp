﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Administration
{
    public interface ISessionDao : IBaseDao<Session, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function



        #endregion

        #region List Loading Function
        IList<Session> LoadSession(List<long> organizationIdList, List<long> programIdList);
        IList<Session> LoadSession(DateTime date, long teacherId, List<long> programIds);
        IList<Session> LoadSessionForList(int start, int length, string orderBy, string orderDir, string name, string code, string rank, string status);
        //IList<Session> LoadAuthorizedOfficePublicSession(List<long> programIdList, List<long> branchIdList, long? programId, bool isOffice = false, bool isPublic = false);
        IList<Session> LoadAuthorizedSession(List<long> authProgramIdList, List<long> authBranchIdList, bool? isOffice, bool? isPublic);
        #endregion

        #region Public API
        IList<Session> LoadPublicSessionByProgram(string orgBusinessId, long programId, long? branchId=null);
        #endregion

        #region Others Function
        bool CheckDuplicateSession(out string fieldName, string name = null, string code = null, long id = 0);
        int GetSessionCount(string name, string code, string rank, string status);
        IList<string> LoadSessionCode();
        #endregion

    }
    public class SessionDao : BaseDao<Session, long>, ISessionDao
    {
        #region Properties & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<Session> LoadSession(List<long> organizationIdList, List<long> programIdList)
        {
            ICriteria criteria = Session.CreateCriteria<Session>().Add(Restrictions.Eq("Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            if (organizationIdList != null || programIdList != null)
            {
                DetachedCriteria programSessionBranch = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Session.Id")));
                if (organizationIdList != null)
                {
                    programSessionBranch.CreateAlias("pr.Organization", "Organization").Add(Restrictions.Eq("Organization.Status", Organization.EntityStatus.Active));
                    if (!organizationIdList.Contains(SelectionType.SelelectAll))
                        programSessionBranch.Add(Restrictions.In("Organization.Id", organizationIdList));
                }
                if (programIdList != null)
                {
                    programSessionBranch.CreateAlias("Program", "pr").Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
                    if (!programIdList.Contains(SelectionType.SelelectAll))
                        programSessionBranch.Add(Restrictions.In("pr.Id", programIdList));
                }
                criteria.Add(Subqueries.PropertyIn("Id", programSessionBranch));
            }
            return criteria.List<Session>().OrderBy(x => x.Rank).ToList();
        }

        public IList<Session> LoadSession(DateTime date, long teacherId, List<long> programIds)
        {
            var sessionList = Session.Query<TeacherClassEntryDetails>().Where(x => x.Status == Teacher.EntityStatus.Active && x.TeacherClassEntry.Status == Teacher.EntityStatus.Active && x.TeacherClassEntry.HeldDate == date.Date && x.Teacher.Id == teacherId && x.TeacherClassEntry.Program.Id == programIds[0]).Select(x => x.TeacherClassEntry.Session);
            return sessionList.Where(x => x.Id != null).Distinct().ToList();
        }
        //public IList<Session> LoadAuthorizedOfficePublicSession(List<long> programIdList, List<long> branchIdList, long? programId, bool isOffice = false, bool isPublic = false)
        //{
        //    ICriteria criteria = Session.CreateCriteria<Session>().Add(Restrictions.Eq("Status", Model.Entity.Administration.Session.EntityStatus.Active));
        //    DetachedCriteria programSessionBranch = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Session.Id")));
        //    programSessionBranch.CreateAlias("Session", "s");
        //    programSessionBranch.CreateAlias("Program", "pr").Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
        //    programSessionBranch.CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));

        //    if (programId != null)
        //    {
        //        if (programId != SelectionType.SelelectAll)
        //            programSessionBranch.Add(Restrictions.Eq("pr.Id", programId));
        //    }
        //    if (branchIdList != null)
        //    {
        //        programSessionBranch.Add(Restrictions.In("br.Id", branchIdList));
        //    }
        //    if (programIdList != null)
        //    {
        //        programSessionBranch.Add(Restrictions.In("pr.Id", programIdList));
        //    }
        //    if (isOffice)
        //    {
        //        programSessionBranch.Add(Restrictions.Eq("IsOffice", isOffice));        
        //    }
        //    if (isPublic)
        //    {
        //        programSessionBranch.Add(Restrictions.Eq("IsPublic", isPublic));
        //    }

        //    criteria.Add(Subqueries.PropertyIn("Id", programSessionBranch));
        //    return criteria.List<Session>().OrderBy(x => x.Rank).ToList();
        //}

        //public IList<Session> LoadAuthorizedSession(List<long> authBranchIdList, List<long> authProgramIdList, List<long> organizationIds, List<long> programIds)
        //{
        //    ICriteria criteria = Session.CreateCriteria<Session>().Add(Restrictions.Eq("Status", Model.Entity.Administration.Session.EntityStatus.Active));           

        //    DetachedCriteria programSessionBranch = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Session.Id")));
        //   // programSessionBranch.CreateAlias("Session", "s");
        //    programSessionBranch.CreateAlias("Program", "pr").Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
        //    programSessionBranch.CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
        //    programSessionBranch.CreateAlias("pr.Organization", "or").Add(Restrictions.Eq("or.Status", Organization.EntityStatus.Active));

        //    if (authProgramIdList != null)
        //    {
        //        programSessionBranch.Add(Restrictions.In("pr.Id", authProgramIdList));
        //    }

        //    if (authBranchIdList != null)
        //    {
        //        programSessionBranch.Add(Restrictions.In("br.Id", authBranchIdList));
        //    }


        //    if (programIds != null && programIds.Count > 0)
        //    {
        //        if (!programIds.Contains(0))
        //            programSessionBranch.Add(Restrictions.In("pr.Id", programIds));
        //    }


        //    if (organizationIds != null)
        //    {
        //        if (!organizationIds.Contains(0))
        //            programSessionBranch.Add(Restrictions.In("or.Id", organizationIds));
        //    }
        //    criteria.Add(Subqueries.PropertyIn("Id", programSessionBranch));
        //    return criteria.List<Session>().OrderBy(x => x.Rank).ToList();
        //}
        public IList<Session> LoadAuthorizedSession(List<long> authProgramIdList, List<long> authBranchIdList, bool? isOffice, bool? isPublic)
        {
            ICriteria criteria = Session.CreateCriteria<Session>().Add(Restrictions.Eq("Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));

            DetachedCriteria programSessionBranch = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Session.Id")));
            // programSessionBranch.CreateAlias("Session", "s");
            programSessionBranch.CreateAlias("Program", "pr").Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
            programSessionBranch.CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            programSessionBranch.CreateAlias("pr.Organization", "or").Add(Restrictions.Eq("or.Status", Organization.EntityStatus.Active));

            if (authProgramIdList != null)
            {
                programSessionBranch.Add(Restrictions.In("pr.Id", authProgramIdList));
            }
            if (authBranchIdList != null)
            {
                programSessionBranch.Add(Restrictions.In("br.Id", authBranchIdList));
            }
            if (isOffice != null)
            {
                programSessionBranch.Add(Restrictions.Eq("IsOffice", isOffice));
            }
            if (isPublic != null)
            {
                programSessionBranch.Add(Restrictions.Eq("IsPublic", isPublic));
            }

            criteria.Add(Subqueries.PropertyIn("Id", programSessionBranch));
            return criteria.List<Session>().OrderBy(x => x.Rank).ToList();
        }


        public IList<Session> LoadSessionForList(int start, int length, string orderBy, string orderDir, string name, string code, string rank, string status)
        {
            ICriteria criteria = GetSessionListCriteria(name, code, rank, status);
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy) : Order.Desc(orderBy));
            }
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Session>();
        }

        private ICriteria GetSessionListCriteria(string name, string code, string rank, string status)
        {
            ICriteria criteria = Session.CreateCriteria<Session>().Add(Restrictions.Not(Restrictions.Eq("Status", BusinessModel.Entity.Administration.Session.EntityStatus.Delete)));
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(code))
            {
                criteria.Add(Restrictions.Like("Code", code, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            return criteria;
        }

        #endregion

        #region Public API
        public IList<Session> LoadPublicSessionByProgram(string orgBusinessId, long programId, long? branchId = null)
        {
            ICriteria criteria = Session.CreateCriteria<Session>()
                .Add(Restrictions.Eq("Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active))
                .CreateAlias("ProgramBranchSessions", "pbs")
                .Add(Restrictions.Eq("pbs.IsPublic", true))
                //.Add(Restrictions.Eq("pbs.Program.Id", programId))
                .CreateAlias("pbs.Program", "p").CreateAlias("pbs.Branch", "br")
                .Add(Restrictions.Eq("p.Id", programId))
                .Add(Restrictions.Eq("p.Status", BusinessModel.Entity.Administration.Program.EntityStatus.Active));

                if (branchId!=null)
                {
                    criteria.Add(Restrictions.Eq("br.Id", branchId));
                }
                
                criteria.Add(Restrictions.Eq("br.Status", BusinessModel.Entity.Administration.Branch.EntityStatus.Active))
                .CreateAlias("p.Organization", "o")
                .Add(Restrictions.Eq("o.BusinessId", orgBusinessId))
                .Add(Restrictions.Eq("o.Status", BusinessModel.Entity.Administration.Organization.EntityStatus.Active));

            return criteria.List<Session>().OrderBy(x => x.Rank).Distinct().ToList();
        }
        #endregion

        #region Others Function
        public bool CheckDuplicateSession(out string fieldName, string name = null, string code = null, long id = 0)
        {
            fieldName = "";

            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<Session>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", BusinessModel.Entity.Administration.Session.EntityStatus.Delete)));
            if (id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (name != null)
            {
                criteria.Add(Restrictions.Eq("Name", name));
                fieldName = "Name";
            }
            else if (code != null)
            {
                criteria.Add(Restrictions.Eq("Code", code));
                fieldName = "Code";
            }
            var sessionList = criteria.List<Session>();
            if (sessionList != null && sessionList.Count > 0)
            {

                checkValue = true;
            }
            return checkValue;
        }
        public int GetSessionCount(string name, string code, string rank, string status)
        {
            ICriteria criteria = GetSessionListCriteria(name, code, rank, status);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public IList<string> LoadSessionCode()
        {
            var codeList = Session.QueryOver<Session>().Select(x => x.Code).List<string>();
            return codeList;
        }
        #endregion
    }
}