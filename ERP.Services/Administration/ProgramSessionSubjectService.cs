﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Administration
{
    public interface IProgramSessionSubjectService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        ProgramSessionSubject LoadById(long id);
        #endregion

        #region List Loading Function
        IList<ProgramSessionSubject> LoadProgramSessionSubject(long programId, long sessionId);
        #endregion

        #region Others Function

        #endregion

        //need to transfer
        string SaveUpdateSubject(SubjectAssign subjectAssign, long programId, long sessionId);
    }
    public class ProgramSessionSubjectService : BaseService, IProgramSessionSubjectService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ProgramSessionSubjectDao _programSessionSubjectDao;
        private readonly IStudentCourseDetailDao _studentCourseDetailDao;
        private readonly ISubjectDao _subjectDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        public ProgramSessionSubjectService(ISession session)
        {
            Session = session;
            _programSessionSubjectDao = new ProgramSessionSubjectDao { Session = Session };
            _studentCourseDetailDao = new StudentCourseDetailDao() { Session = Session };
            _subjectDao = new SubjectDao() { Session = Session };
            _programDao = new ProgramDao() { Session = Session };
            _sessionDao = new SessionDao() { Session = Session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public ProgramSessionSubject LoadById(long id)
        {
           return _programSessionSubjectDao.LoadById(id);
        }
        #endregion

        #region List Loading Function
        public IList<ProgramSessionSubject> LoadProgramSessionSubject(long programId, long sessionId)
        {
            try
            {
                return _programSessionSubjectDao.LoadProgramSessionSubject(programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        #endregion

        #region Others Function

        #endregion

        //need to transfer
        public string SaveUpdateSubject(SubjectAssign subjectAssign, long programId, long sessionId)
        {
            ITransaction transaction = null;
            try
            {

                using (transaction = Session.BeginTransaction())
                {
                    //try
                    //{
                    #region Checking All Old Assign List Using This Program & Session

                    IList<ProgramSessionSubject> existingSubjectList = _programSessionSubjectDao.LoadProgramSessionSubject(programId, sessionId);

                    IList<long> nonDeleteAbleSubjectList = new List<long>();
                    string returnMessage = "";

                    foreach (var exisSub in existingSubjectList)
                    {
                        bool isStudentExitWithThisSub = _studentCourseDetailDao.IsExistCheckStudentByProgramSessionAndSubject(programId, sessionId, exisSub.Subject.Id);

                        if (isStudentExitWithThisSub)
                        {
                            nonDeleteAbleSubjectList.Add(exisSub.Subject.Id);
                            returnMessage += exisSub.Subject.Name + " ,";
                        }
                        else
                        {
                            _programSessionSubjectDao.Delete(exisSub);
                        }

                    }

                    #endregion

                    #region Assign Subject

                    returnMessage += "||";
                    foreach (var subId in subjectAssign.SelectedSubjects)
                    {
                        bool isExistInNonDeleteableList = nonDeleteAbleSubjectList.Contains(subId);

                        if (!isExistInNonDeleteableList)
                        {
                            var subjectObj = _subjectDao.LoadById(subId);
                            var programObj = _programDao.LoadById(programId);
                            var sessionObj = _sessionDao.LoadById(sessionId);
                            
                            var programSessionSubject = new ProgramSessionSubject();
                            returnMessage += subjectObj.Name + " ,";
                            returnMessage = returnMessage.Remove(returnMessage.Length - 1);
                            programSessionSubject.Subject = subjectObj;
                            programSessionSubject.Program = programObj;
                            programSessionSubject.Session = sessionObj;
                            _programSessionSubjectDao.Save(programSessionSubject);
                        }
                    }
                    #endregion

                    transaction.Commit();

                    if (returnMessage != "")
                        return returnMessage;
                    else
                    {
                        return "";
                    }

                    //}
                    //catch (Exception ex)
                    //{
                    //    if (transaction != null && transaction.IsActive)
                    //        transaction.Rollback();
                    //    return "";
                    //}

                }


            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                return "";
            }


        }
    }
}
