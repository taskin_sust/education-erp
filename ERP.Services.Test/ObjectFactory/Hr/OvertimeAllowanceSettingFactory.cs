using System;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class OvertimeAllowanceSettingFactory : ObjectFactoryBase<OvertimeAllowanceSetting>
    {
        private readonly OrganizationFactory _organizationFactory;
        private readonly IOvertimeAllowanceSettingService _prOvertimeAllowanceService;

        public OvertimeAllowanceSettingFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _prOvertimeAllowanceService = new OvertimeAllowanceSettingService(session);
            _organizationFactory = new OrganizationFactory(this, session);

        }

        #region Create

        //public OvertimeAllowanceSettingFactory Create(Organization or)
        //{
        //    var entity = new OvertimeAllowanceSetting();
        //    entity.Name = "SettingName_" + DateTime.Now.Second + "_" + DateTime.Now.Millisecond;
        //    entity.MultiplyingFactor = 1.0M;
        //    entity.CalculationOn = 1;
        //    entity.ClosingDate = DateTime.Now.AddYears(5);
        //    entity.BusinessId = GetPcUserName();
        //    entity.VersionNumber = 404;
        //    entity.CreationDate = DateTime.Now;
        //    entity.ModificationDate = DateTime.Now;
        //    entity.ModifyBy = 0;
        //    entity.CreateBy = 0;
        //    entity.EffectiveDate = DateTime.Now.AddDays(1);
        //    entity.IsContractual = true;
        //    entity.IsPartTime = true;
        //    entity.IsPermanent = true;
        //    entity.IsProbation = true;
        //    entity.IsIntern = true;
        //    entity.IsGazzeted = true;
        //    entity.IsManagementWeekend = true;
        //    entity.IsWorkingDay = true;
        //    Object = entity;
        //    SingleObjectList.Add(Object);
        //    return this;
        //}

        public OvertimeAllowanceSettingFactory Create()
        {
            var entity = new OvertimeAllowanceSetting();
            entity.Name = "SettingName_" + DateTime.Now.Second + "_" + DateTime.Now.Millisecond;
            entity.MultiplyingFactor = 1.0M;
            entity.CalculationOn = 1;
            entity.ClosingDate = DateTime.Now.AddYears(5);
            entity.BusinessId = GetPcUserName();
            entity.VersionNumber = 404;
            entity.CreationDate = DateTime.Now;
            entity.ModificationDate = DateTime.Now;
            entity.ModifyBy = 0;
            entity.CreateBy = 0;
            entity.EffectiveDate = DateTime.Now.AddDays(1);
            entity.IsContractual = true;
            entity.IsPartTime = true;
            entity.IsPermanent = true;
            entity.IsProbation = true;
            entity.IsIntern = true;
            entity.IsGazzeted = true;
            entity.IsManagementWeekend = true;
            entity.IsWorkingDay = true;
            Object = entity;
            SingleObjectList.Add(Object);
            return this;
        }

        //this funcion will be remove
        public OvertimeAllowanceSettingFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        internal OvertimeAllowanceSettingFactory WithoutEmploymentStatus()
        {
            Object.IsContractual = false;
            Object.IsPartTime = false;
            Object.IsPermanent = false;
            Object.IsProbation = false;
            Object.IsIntern = false;
            return this;
        }

        internal OvertimeAllowanceSettingFactory WithoutOvertimeType()
        {
            Object.IsWorkingDay = false;
            Object.IsManagementWeekend = false;
            Object.IsGazzeted = false;      
            return this;
        }

        public OvertimeAllowanceSettingFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public OvertimeAllowanceSettingFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

              //  SingleObjectList.ForEach(x => { if (x.Id == 0) _prOvertimeAllowanceService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                //ObjectList.ForEach(x => { if (x.Id == 0)_prOvertimeAllowanceService.SaveOrUpdate(x); });

            }
            return this;
        }

        public OvertimeAllowanceSettingFactory CreateMore(int num, Organization organization = null)
        {
            for (int i = 0; i < num; i++)
            {
                var obj = new OvertimeAllowanceSetting();

                if (organization == null)
                {
                    _organizationFactory.Create().Persist();
                    obj.Organization = _organizationFactory.Object;
                }
                else
                {
                    obj.Organization = organization;
                }
                obj.Name = "SettingName_" + i + " " + DateTime.Now.Second + "_" + DateTime.Now.Millisecond;
                obj.MultiplyingFactor = 1.0M;
                obj.CalculationOn = 1;
                obj.BusinessId = GetPcUserName();
                obj.VersionNumber = 404;
                obj.CreationDate = DateTime.Now;
                obj.ModificationDate = DateTime.Now;
                obj.ModifyBy = 0;
                obj.CreateBy = 0;
                obj.EffectiveDate = DateTime.Now.AddDays(i + 1);
                //obj.ClosingDate = DateTime.Now.AddYears(i + 1);
                obj.IsContractual = true;
                obj.IsPartTime = true;
                obj.IsPermanent = true;
                obj.IsProbation = true;
                obj.IsIntern = true;
                obj.IsGazzeted = true;
                obj.IsManagementWeekend = true;
                obj.IsWorkingDay = true;
                ObjectList.Add(obj);
            }
            return this;
        }


        #endregion

        #region other
        #endregion

        #region CleanUp

        public OvertimeAllowanceSettingFactory CleanUp()
        {

            DeleteAll();
            _organizationFactory.Cleanup();
            return this;
        }

        #endregion

    }
}
