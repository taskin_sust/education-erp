﻿using System;
using System.Collections.Generic;
using NHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.SessionTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Session")]
    public class SessionDeleteTest : SessionBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            SessionFactory sessionFactory =
                    _sessionFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithCode(_sessionFactory.GetRandomNumber())
                    .Persist();
            Session sessionObj = _sessionService.LoadById(sessionFactory.Object.Id);
            var success = _sessionService.Delete(sessionObj.Id);
            Assert.True(success);
            sessionObj = _sessionService.LoadById(sessionFactory.Object.Id);
            Assert.Null(sessionObj);

        }
        #endregion

        #region Businuss Logic Test
        [Fact]
        public void Delete_Should_Throw_Dependency_Exception()
        {
            SessionFactory objFactory =
                 _sessionFactory.Create()
                     .WithNameAndRank(_name, 1).Persist();
            objFactory.Object.Batches = new List<Batch>()
            {
                new Batch()
            };
            Assert.Throws<DependencyException>(() => _sessionService.Delete(objFactory.Object.Id));
        }
        #endregion

    }
}
