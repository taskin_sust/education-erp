using System;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class MemberLoanRefundFactory : ObjectFactoryBase<MemberLoanRefund>
    {

        public readonly IMemberLoanRefundService _memberLoanRefundService;

        public MemberLoanRefundFactory(IObjectFactoryBase caller, ISession session): base(caller, session)
        {
            _memberLoanRefundService = new MemberLoanRefundService(session);
        }

        #region Create

        public MemberLoanRefundFactory Create()
        {
            Object=new MemberLoanRefund()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = MemberLoanRefund.EntityStatus.Active,
                RefundDate = DateTime.Now

            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanRefundFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanRefundFactory WithReceivedBy(long receivedBy)
        {
            Object.ReceivedById = receivedBy;
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanRefundFactory WithSalaryOrganization(Organization organization)
        {
            Object.SalaryOrganization = organization;
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanRefundFactory WithSalaryBranch(Branch branch)
        {
            Object.SalaryBranch = branch;
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanRefundFactory WithSalaryCampus(Campus campus)
        {
            Object.SalaryCampus = campus;
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanRefundFactory WithRefundAmount(decimal refundAmount)
        {
            Object.RefundAmount = refundAmount;
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanRefundFactory WithRemark(string remark)
        {
            Object.Remarks = remark;
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanRefundFactory CreateWith(long id,DateTime refundDate)
        {
            Object = new MemberLoanRefund()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = MemberLoanRefund.EntityStatus.Active,
                RefundDate = refundDate
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanRefundFactory Persist()
        {

            //if (SingleObjectList != null && SingleObjectList.Count > 0){

            //    SingleObjectList.ForEach(x =>{if (x.Id == 0) _prMemberLoanRefundService.SaveOrUpdate(x);});
            //}

            //if (ObjectList != null && ObjectList.Count > 0){

            //    ObjectList.ForEach(x =>{if (x.Id == 0)_prMemberLoanRefundService.SaveOrUpdate(x);});

            //}
            return this;
        }

        public MemberLoanRefundFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new MemberLoanRefund() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public MemberLoanRefundFactory CleanUp()
        {
            DeleteAll();
            return this;
        }

        #endregion

    }
}
