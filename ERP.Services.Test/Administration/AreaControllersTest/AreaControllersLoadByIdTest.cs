﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.AreaControllersTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "AreaControllers")]
    public class AreaControllersLoadByIdTest : AreaControllersBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _areaControllersService.LoadById(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            AreaControllersFactory sessionFactory =
                _areaControllersFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithArea(_areaName)
                    .Persist();
            var obj = _areaControllersService.LoadById(sessionFactory.Object.Id);
            Assert.NotNull(obj);
            
        }

    }
}
