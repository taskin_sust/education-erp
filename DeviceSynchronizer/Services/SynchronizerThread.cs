﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DeviceSynchronizer.ApiReferences;
using DeviceSynchronizer.Dao;
using DeviceSynchronizer.DeviceHelper;
using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model;
using DeviceSynchronizer.Model.ApiModel;
using log4net;
using Newtonsoft.Json;
using System.Security.Permissions;

namespace DeviceSynchronizer.Services
{
    public class SynchronizerThread
    {
        #region Declaration

        private static readonly ILog Log = LogManager.GetLogger("ServiceLogger");
        private Thread _deviceSynchronizerThread;
        private readonly SyncTeamMemberInfoApiRequest _syncTeamMemberInfoApiRequest;
        private readonly SyncAttendanceReceiveApiRequest _syncAttendanceReceiveApiRequest;

        public volatile ServiceStatus _serviceStatus;
        public volatile bool InternetAvailable = false;
        private const int ThreadJoiningTime = 10 * 1000;
        private int _serverCallingTimeInterval = 30;
        private const int MinuteToSec = 60;
        private const int TimeInterval = 10;
        private readonly ServiceDelegate.DeviceSynchronizedEventHandler _throwMessage;
        private readonly ServiceDelegate.SetAppTitleDelegate _softwareTitle;

        #endregion

        #region Constructor

        public SynchronizerThread(ServiceDelegate.DeviceSynchronizedEventHandler throwMessage, ServiceDelegate.SetAppTitleDelegate softwareTitle)
        {
            _syncTeamMemberInfoApiRequest = new SyncTeamMemberInfoApiRequest();
            _syncAttendanceReceiveApiRequest = new SyncAttendanceReceiveApiRequest();

            _serviceStatus = new ServiceStatus();
            _throwMessage = throwMessage;
            _softwareTitle = softwareTitle;
        }

        #endregion

        #region Delegate Function

        protected virtual void OnThrowingMessage(string message, MessageType messageType)
        {
            if (_throwMessage != null)
            {
                _throwMessage(message, messageType);
            }
        }

        protected virtual void OnSoftwareVersion(string name)
        {
            if (_softwareTitle != null)
            {
                _softwareTitle(name);
            }
        }

        #endregion

        #region Thread Start and Stop Point

        public void Start()
        {
            try
            {
                if (_deviceSynchronizerThread != null && _deviceSynchronizerThread.IsAlive)
                {
                    OnThrowingMessage("Program alreday running Please stop it first", MessageType.Info);
                }
                else
                {
                    _serviceStatus.IsRunning = true;
                    _deviceSynchronizerThread = new System.Threading.Thread(DeviceSync);
                    _deviceSynchronizerThread.SetApartmentState(ApartmentState.STA);
                    _deviceSynchronizerThread.Start();
                }
            }
            catch (FileNotFoundException exception)
            {
                Log.Error("Could not start ", exception);
                OnThrowingMessage("Service fails to start due to " + exception.Message, MessageType.Error);
                SafeSleep(TimeInterval);
            }
            catch (Exception ex)
            {
                Log.Error("Could not start ", ex);
                OnThrowingMessage("Service fails to start due to " + ex.Message, MessageType.Error);
            }
        }

        public void Stop()
        {
            try
            {
                _serviceStatus.IsRunning = false;
                _deviceSynchronizerThread.Join();
            }
            catch (ThreadStartException ese)
            {
                throw ese;
            }
            catch (ThreadInterruptedException tie)
            {
                throw tie;
            }
            catch (Exception ex)
            {
                Log.Error("stop failed due to " + ex.ToString());
                //OnThrowingMessage("Service fails to stop due to " + ex.Message, MessageType.Error);
            }
        }

        public void Restart()
        {
            try
            {
                _serviceStatus.IsRunning = false;
                _deviceSynchronizerThread.Join(ThreadJoiningTime);
                Start();
            }
            catch (Exception ex)
            {
                Log.Error("Restart failed due to " + ex.Message);
                OnThrowingMessage("Service fails to restart due to " + ex.Message, MessageType.Error);
            }
        }

        private void SafeSleep(int second)
        {
            int currentSec = 0;
            while (_serviceStatus.IsRunning && currentSec < second)
            {
                currentSec++;
                Thread.Sleep(1000);
            }
        }

        #endregion

        #region Operation

        [STAThread]
        private void DeviceSync()
        {
            //starting thread
            string info = "Service Started at: " + DateTime.Now.ToString(BusinessRules.DateTimeFormat);
            Log.Info(info);
            OnThrowingMessage(info, MessageType.Important);
            var hddSerials = Helper.Helper.GetHddSerials();
            while (_serviceStatus.IsRunning)
            {
                try
                {
                    //get data from online
                    OnThrowingMessage("Try to get data from online", MessageType.Info);
                    var attendanceSynchronizer = _syncTeamMemberInfoApiRequest.GetAttendanceSynchronizer(hddSerials);
                    if (attendanceSynchronizer == null)
                    {
                        OnThrowingMessage("Failed to get data from online", MessageType.Error);
                    }
                    else
                    {
                        //update ui & do operation for each device
                        OnSoftwareVersion(attendanceSynchronizer.Name);
                        _serverCallingTimeInterval = MinuteToSec * attendanceSynchronizer.DataCallingInterval;
                        var devices = attendanceSynchronizer.AttendanceDevices;
                        foreach (var attendanceDevice in devices)
                        {
                            if (!_serviceStatus.IsRunning) break;
                            SyncOperation(attendanceDevice);
                        }
                    }

                    OnThrowingMessage("Next Sync Time: " + DateTime.Now.AddSeconds(_serverCallingTimeInterval).ToString(BusinessRules.DateTimeFormat), MessageType.Important);
                    SafeSleep(_serverCallingTimeInterval);
                }
                catch (WebException webException)
                {
                    Log.Error(webException);
                    OnThrowingMessage("Internet unavailable", MessageType.Error);
                    SafeSleep(TimeInterval);
                }
                catch (Exception exception)
                {
                    Log.Error(exception);
                    SafeSleep(TimeInterval);
                }
            }

            //stoping thread
            info = "Service Stopped at: " + DateTime.Now.ToString(BusinessRules.DateTimeFormat);
            Console.WriteLine("before execution !");
            Log.Info(info);
            OnThrowingMessage(info, MessageType.Important);Console.WriteLine("after execution !");
        }

        //use as a container 
        // create multiple object and share these objects 
        private void SyncOperation(AttendanceDeviceModel attendanceDevice)
        {
            SQLiteConnection sqLiteConnection = null;
            IAttendanceDeviceDriver attendanceDeviceDriver = null;
            try
            {
                //required object create
                sqLiteConnection = DbHelper.OpenDbConnection();

                //device driver
                attendanceDeviceDriver = AttendanceDeviceDriverFactory(attendanceDevice);
                if (attendanceDeviceDriver == null || attendanceDeviceDriver.OpenConnection() == false)
                    return;

                #region Device Management

                var deviceManagement = new DeviceManagement(attendanceDevice, attendanceDeviceDriver);
                bool isSuccess = deviceManagement.ReadyDevice();
                if (!isSuccess) return;

                #endregion

                #region UserManagement

                var userManagement = new UserManagement(attendanceDevice, attendanceDeviceDriver, _serviceStatus, OnThrowingMessage);
                bool isSuc = userManagement.RegisterUser();
                if (!isSuc) return;

                #endregion

                #region Attendance Synchronization

                var attendanceManagement = new AttendanceManagement(attendanceDevice, attendanceDeviceDriver, sqLiteConnection, OnThrowingMessage);
                attendanceManagement.UpdateAttendance();

                #endregion

                #region Update Time

                _syncAttendanceReceiveApiRequest.UpdateTimeOfAttendanceDevice(attendanceDevice.Id, attendanceDevice.LastUpdateTime, attendanceDevice.TeamMembers.Where(x=>x.SaveToDeviceSuccessed).Select(x => x.EnrollNo).ToList());
                OnThrowingMessage("Sync completed on device " + attendanceDevice.IPAddress + " until " + attendanceDevice.LastUpdateTime, MessageType.Info);

                #endregion

            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                OnThrowingMessage(e.Message, MessageType.Error);
            }
            finally
            {
                if (sqLiteConnection != null)
                    sqLiteConnection.Close();
                if (attendanceDeviceDriver != null)
                    attendanceDeviceDriver.CloseConnection();
            }
        }

        public IAttendanceDeviceDriver AttendanceDeviceDriverFactory(AttendanceDeviceModel attendanceDevice)
        {
            if (attendanceDevice.DeviceTypeCode.Equals(DeviceType.BlackAndWhiteDevice, StringComparison.CurrentCultureIgnoreCase))
            {
                return new AxAttendanceDeviceDriverBlackWhite(attendanceDevice, OnThrowingMessage);
            }
            else if (attendanceDevice.DeviceTypeCode.Equals(DeviceType.ColoredDevice, StringComparison.CurrentCultureIgnoreCase))
            {
                return new AxAttendanceDeviceDriverColor(attendanceDevice, OnThrowingMessage);
            }
            else if (attendanceDevice.DeviceTypeCode.Equals(DeviceType.ColoredDeviceWithFingerTemplate, StringComparison.CurrentCultureIgnoreCase))
            {
                return new AxAttendanceDeviceDriverColorWithFinger(attendanceDevice, OnThrowingMessage);
            }
            else
            {
                OnThrowingMessage("Attendance device driver not found for model: " + attendanceDevice.DeviceModelNo, MessageType.Error);
                return null;
            }

        }

        #endregion

    }



    //Required for pass as paramiter 
    public class ServiceStatus
    {
        public volatile bool IsRunning = false;
    }
}
