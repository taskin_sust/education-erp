﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface ISessionService : IBaseService
    {
        #region Operational Function
        bool Save(Session erpSession);
        bool Update(long id, Session erpSession);
        bool Delete(long id);
        bool UpdateRank(long id, int current, string action);
        #endregion

        #region Single Instances Loading Function
        Session LoadById(long id);
        #endregion

        #region List Loading Function
        IList<Session> LoadSession(List<long> organizationIdList = null, List<long> programIdList = null);
        IList<Session> LoadSession(int start, int length, string orderBy, string orderDir, string name, string code, string status, string rank);
        IList<Session> LoadAuthorizedSession(List<UserMenu> userMenu, List<long> organizationIds = null, List<long> programIds = null, bool? isOffice = null, bool? isPublic = null);
        #endregion

        #region Others Function
        int GetMinimumRank(Session sessionObj);
        int GetMaximumRank(Session sessionObj);
        int SessionRowCount(string name, string code, string rank, string status);
        IList<string> LoadSessionCode();
        #endregion

        #region Public API
        IList<Session> LoadPublicSessionByProgram(string orgBusinessId, long programId);
        //  IList<Session> LoadAuthorizedOfficePublicSession(List<UserMenu> userMenu, long? programId = null, bool isOffice = false, bool isPublic = false);
        #endregion

    }
    public class SessionService : BaseService, ISessionService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ISessionDao _sessionDao;
        private readonly IProgramBranchSessionDao _programBranchSessionDao;
        private readonly ICommonHelper _commonHelper;
        public SessionService(ISession session)
        {
            Session = session;
            _sessionDao = new SessionDao() { Session = Session };
            _programBranchSessionDao = new ProgramBranchSessionDao() { Session = Session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function
        private void CheckEmptyField(Session erpSession)
        {
            if (string.IsNullOrEmpty(erpSession.Name))
                throw new EmptyFieldException("Name is empty");
            if (string.IsNullOrEmpty(erpSession.Code))
                throw new EmptyFieldException("Code is empty");
        }
        private void CheckBeforeSave(Session sessionObj)
        {
            string fieldName = "";
            var checkDuplicateName = _sessionDao.CheckDuplicateSession(out fieldName, sessionObj.Name, null, sessionObj.Id);
            if (checkDuplicateName)
            {
                if (fieldName == "Name")
                    throw new DuplicateEntryException("Duplicate session found");
            }

            if (!sessionObj.Code.All(Char.IsDigit) || sessionObj.Code.Length != 2 || sessionObj.Code.Equals("00"))
            {
                throw new InvalidDataException("Code string is in invalid format");
            }
            else
            {
                var checkDuplicateCode = _sessionDao.CheckDuplicateSession(out fieldName, null, sessionObj.Code, sessionObj.Id);
                if (checkDuplicateCode)
                {
                    if (fieldName == "Code")
                        throw new DuplicateEntryException("Duplicate code found");
                }
            }
        }

        public bool Save(Session erpSession)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    if (erpSession == null)
                    {
                        throw new NullObjectException("session can not be null");
                    }
                    CheckEmptyField(erpSession);
                    //check before save
                    CheckBeforeSave(erpSession);
                    erpSession.Rank = _sessionDao.GetMaximumRank(erpSession) + 1;
                    _sessionDao.Save(erpSession);
                    transaction.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        private void CheckBeforeUpdate(Session sessionObj)
        {
            string fieldName = "";
            var checkDuplicateName = _sessionDao.CheckDuplicateSession(out fieldName, sessionObj.Name, null, sessionObj.Id);
            if (checkDuplicateName)
            {
                if (fieldName == "Name")
                    throw new DuplicateEntryException("Duplicate session found");
            }
            if (!sessionObj.Code.All(Char.IsDigit) || sessionObj.Code.Length != 2 || sessionObj.Code.Equals("00"))
            {
                throw new InvalidDataException("Code string is in invalid format");
            }
            else
            {
                var checkDuplicateCode = _sessionDao.CheckDuplicateSession(out fieldName, null, sessionObj.Code, sessionObj.Id);
                if (checkDuplicateCode)
                {
                    if (fieldName == "Code")
                        throw new DuplicateEntryException("Duplicate code found");
                }
            }
        }
        public bool Update(long id, Session erpSession)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    if (erpSession == null)
                    {
                        throw new NullObjectException("session can not be null");
                    }
                    var sessionObj = _sessionDao.LoadById(id);
                    CheckEmptyField(sessionObj);
                    //check before update
                    CheckBeforeUpdate(erpSession);
                    sessionObj.Name = erpSession.Name;
                    sessionObj.Code = erpSession.Code;
                    sessionObj.Status = erpSession.Status;
                    _sessionDao.Update(sessionObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        private void CheckBeforeDelete(Session erpSession)
        {
            if (erpSession.Batches != null && erpSession.Batches.Count > 0)
                throw new DependencyException("You can't delete this session, session is already assigned to branch(s).");
            if (erpSession.ProgramBranchSessions != null && erpSession.ProgramBranchSessions.Count > 0)
                throw new DependencyException("You can't delete this session, session is already assigned to program(s).");
        }
        public bool Delete(long id)
        {
            ITransaction transaction = null;
            try
            {
                var tempSessionObj = _sessionDao.LoadById(id);
                CheckBeforeDelete(tempSessionObj);
                using (transaction = Session.BeginTransaction())
                {
                    tempSessionObj.Status = BusinessModel.Entity.Administration.Session.EntityStatus.Delete;
                    _sessionDao.Update(tempSessionObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool UpdateRank(long id, int current, string action)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    Session sessionUpdateObj = _sessionDao.LoadById(Convert.ToInt64(id));
                    if (sessionUpdateObj != null)
                    {
                        int newRank;
                        if (action == RankOperation.Up)
                            newRank = sessionUpdateObj.Rank - 1;
                        else
                            newRank = sessionUpdateObj.Rank + 1;
                        var sessionOldObj = _sessionDao.LoadByRankDirection(newRank, action);//LoadByRank(newRank);
                        newRank = sessionOldObj.Rank;
                        sessionOldObj.Rank = sessionUpdateObj.Rank;
                        sessionUpdateObj.Rank = newRank;

                        _sessionDao.Update(sessionOldObj);
                        _sessionDao.Update(sessionUpdateObj);
                        transaction.Commit();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public Session LoadById(long id)
        {
            Session session;
            try
            {
                session = _sessionDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return session;
        }
        #endregion

        #region List Loading Function
        //public IList<Session> LoadAuthorizedOfficePublicSession(List<UserMenu> userMenu, long? programId = null, bool isOffice = false, bool isPublic = false)
        //{
        //    IList<Session> sessions;
        //    try
        //    {
        //        //var programIds = new List<long>();
        //        //if (programId != null)
        //        //    programIds = _commonHelper.ConvertIdToList(programId.Value);
        //        var notNullableProgramId = (programId == null) ? 0 : (long)programId;
        //        List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(notNullableProgramId).ToList());
        //        List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
        //        sessions = _sessionDao.LoadAuthorizedOfficePublicSession(programIdList, branchIdList, programId, isOffice, isPublic);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    return sessions;
        //}

        public IList<Session> LoadAuthorizedSession(List<UserMenu> userMenu, List<long> organizationIds = null, List<long> programIds = null, bool? isOffice = null, bool? isPublic = null)
        {
            try
            {
                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu, CommonHelper.ConvertSelectedAllIdList(organizationIds), null, CommonHelper.ConvertSelectedAllIdList(programIds));
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, CommonHelper.ConvertSelectedAllIdList(organizationIds), CommonHelper.ConvertSelectedAllIdList(programIds));
                return _sessionDao.LoadAuthorizedSession(authProgramIdList, authBranchIdList, isOffice, isPublic);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Session> LoadSession(List<long> organizationIdList = null, List<long> programIdList = null)
        {
            IList<Session> sessions;
            try
            {
                sessions = _sessionDao.LoadSession(organizationIdList, programIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return sessions;
        }

        public IList<Session> LoadSession(int start, int length, string orderBy, string orderDir, string name, string code, string rank, string status)
        {
            IList<Session> sessions;
            try
            {
                sessions = _sessionDao.LoadSessionForList(start, length, orderBy, orderDir, name, code, rank, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return sessions;
        }
        #endregion

        #region Others Function

        public int GetMinimumRank(Session sessionObj)
        {
            int minimumRank;
            try
            {
                minimumRank = _sessionDao.GetMinimumRank(sessionObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return minimumRank;
        }

        public int GetMaximumRank(Session sessionObj)
        {
            int maximumRank;
            try
            {
                maximumRank = _sessionDao.GetMaximumRank(sessionObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return maximumRank;
        }

        public int SessionRowCount(string name, string code, string rank, string status)
        {
            int sessionCount;
            try
            {
                sessionCount = _sessionDao.GetSessionCount(name, code, rank, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return sessionCount;
        }

        public IList<string> LoadSessionCode()
        {
            return _sessionDao.LoadSessionCode();
        }
        #endregion

        #region Public API
        public IList<Session> LoadPublicSessionByProgram(string orgBusinessId, long programId)
        {
            IList<Session> sessions;
            try
            {
                sessions = _sessionDao.LoadPublicSessionByProgram(orgBusinessId, programId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return sessions;
        }
        #endregion
    }
}
