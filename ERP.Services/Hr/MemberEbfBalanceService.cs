using System;
using System.Collections;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
using UdvashERP.MessageExceptions;

namespace UdvashERP.Services.Hr
{
    public interface IMemberEbfBalanceService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(List<UserMenu> userMenu, EmployeeBenefitFundSettingOpeningView empBenefitFundSettingOpening);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class MemberEbfBalanceService : BaseService, IMemberEbfBalanceService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IMemberEbfBalanceDao _memberEbfBalanceDao;
        private IEmployeeBenefitsFundSettingDao _employeeBenefitsFundSettingDao;
        private ITeamMemberDao _teamMemberDao;
        IList<MemberEbfBalance> memberEbfBalancList;
        public MemberEbfBalanceService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _memberEbfBalanceDao = new MemberEbfBalanceDao { Session = session };
            _employeeBenefitsFundSettingDao = new EmployeeBenefitsFundSettingDao { Session = session };
            _teamMemberDao = new TeamMemberDao { Session = session };
            memberEbfBalancList = new List<MemberEbfBalance>();
        }
        #endregion

        #region Operational Functions

        private void DoBeforeSave(List<UserMenu> userMenu, EmployeeBenefitFundSettingOpeningView empBenefitFundSettingOpening)
        {
            if (userMenu == null)
            {
                throw new NullObjectException("Invalid user menu");
            }

            if (empBenefitFundSettingOpening == null)
            {
                throw new NullObjectException("Empty employee benefit fund settings opening");
            }

            if (empBenefitFundSettingOpening.Organization <= 0)
            {
                throw new NullObjectException("Null organization found");
            }

            List<long> authOrganizationIds = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(empBenefitFundSettingOpening.Organization));
            List<long> authBranchIds = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIds, null, empBenefitFundSettingOpening.Branch);
            IList<EmployeeBenefitFundBalanceView> empBenefitFundBalanceViews = new List<EmployeeBenefitFundBalanceView>();
            empBenefitFundBalanceViews = _employeeBenefitsFundSettingDao.LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(authOrganizationIds, authBranchIds, empBenefitFundSettingOpening.Campus, empBenefitFundSettingOpening.Department, empBenefitFundSettingOpening.Designation, empBenefitFundSettingOpening.EmploymentStatus, empBenefitFundSettingOpening.DepositDate);

            IList<MemberEbfBalance> memberEbfBalancList = new List<MemberEbfBalance>();

            foreach (var empFund in empBenefitFundBalanceViews)
            {
                foreach (var getTheBalance in empBenefitFundSettingOpening.EmployeeBenefitFundBalanceViewList)
                {
                    MemberEbfBalance memberEbfBalance = new MemberEbfBalance();
                    if (empFund.TeamMemberId == getTheBalance.TeamMemberId)
                    {
                        memberEbfBalance.CreateBy = GetCurrentUserId();
                        memberEbfBalance.ModifyBy = GetCurrentUserId();
                        memberEbfBalance.EmployeeAmount = getTheBalance.EmployeeContribution;
                        memberEbfBalance.EmployerAmount = getTheBalance.EmployerContribution;
                        memberEbfBalance.DepositDate = empBenefitFundSettingOpening.DepositDate;
                        memberEbfBalance.TeamMember = _teamMemberDao.LoadById(getTheBalance.TeamMemberId);
                        memberEbfBalancList.Add(memberEbfBalance);
                    }

                }
            }
        }


        public bool SaveOrUpdate(List<UserMenu> userMenu, EmployeeBenefitFundSettingOpeningView empBenefitFundSettingOpening)
        {
            DoBeforeSave(userMenu, empBenefitFundSettingOpening);

            ITransaction tran = null;
            bool returnVal = false;
            try
            {
                using (tran = Session.BeginTransaction())
                {
                    foreach (var memberEbf in memberEbfBalancList)
                    {
                        _memberEbfBalanceDao.Save(memberEbf);
                    }
                    tran.Commit();
                    returnVal = true;
                }
            }
            catch (Exception)
            {
                if (tran != null && tran.IsActive)
                    tran.Rollback();
                throw;
            }
            return returnVal;
        }

     

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}

