﻿using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Exam;


namespace UdvashERP.Dao.Exam
{
    public interface IExamsDetailsDao : IBaseDao<ExamsDetails, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion
    }
    public class ExamsDetailsDao : BaseDao<ExamsDetails, long>, IExamsDetailsDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion
    }
}
