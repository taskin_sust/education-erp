﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.AssignItemTest
{
    public class ProgramSessionItemBaseTest:TestBase,IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal ProgramSessionItemService _ProgramSessionItemService;
        internal ProgramSessionItemFactory _ProgramSessionItemFactory;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        #endregion
        public ProgramSessionItemBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _ProgramSessionItemFactory = new ProgramSessionItemFactory(null,Session);
            _ProgramSessionItemService = new ProgramSessionItemService(Session);
        }
        
        public void Dispose()
        {
            _ProgramSessionItemFactory.CleanUp();
            base.Dispose();
        }
    }
}
