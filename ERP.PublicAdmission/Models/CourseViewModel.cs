﻿using System.Collections.Generic;

namespace UdvashERP.PublicAdmission.Models
{
    public class CourseViewModel
    {
        public CourseViewModel()
        {
            SubjectViewModels= new List<SubjectViewModel>();
        }
        public virtual long Id { get; set; }
        public virtual string Name { get; set; }
        public IList<SubjectViewModel> SubjectViewModels { get; set; } 
    }
   

}