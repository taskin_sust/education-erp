﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    class TeacherScriptEvaluationMap : BaseClassMap<TeacherScriptEvaluation, long>
    {
        public TeacherScriptEvaluationMap()
        {
            Table("TeacherScEvaluation");
            LazyLoad();
            References(x => x.Organization).Column("OrganizationId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Course).Column("CourseId");
            References(x => x.Session).Column("SessionId");
            Map(x => x.SerialNumber).Column("SerialNumber");
            Map(x => x.SubmissionDate).Column("SubmissionDate");
            HasMany(x => x.TeacherScriptEvaluationDetails).KeyColumn("TeacherScEvaluationId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
