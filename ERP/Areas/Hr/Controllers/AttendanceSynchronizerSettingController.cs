﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    
    public class AttendanceSynchronizerSettingController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ICampusService _campusService;
        private readonly IBranchService _branchService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IAttendanceDeviceService _hrAttendanceDeviceService;
        private readonly IAttendanceSynchronizerService _hrAttendanceSynchronizerService;
        private readonly IAttendanceSynchronizerSettingService _hrAttendanceSynchronizerSettingService;

        public AttendanceSynchronizerSettingController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _campusService = new CampusService(session);
            _branchService = new BranchService(session);
            _userService = new UserService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _hrAttendanceDeviceService = new AttendanceDeviceService(session);
            _hrAttendanceSynchronizerService = new AttendanceSynchronizerService(session);
            _hrAttendanceSynchronizerSettingService=new AttendanceSynchronizerSettingService(session);
        }
        #endregion

        #region Operational Function
        
        #region Save Operation

        public ActionResult Create()
        {
            return View();
        }

        public string CheckFileSettingsForTypeAndSize(string name, string contentType, int size)
        {
            try
            {
                int fileSize = Convert.ToInt32(ConfigurationManager.AppSettings["DeviceSyncUpateMaxSize"]);
                if (name == null || contentType == null || size < 1)
                    return "This is not valid file. Check the file extension.";

                if (contentType.ToLower() == "zip" || contentType.ToLower() == "application/zip" || contentType.ToLower() == "application/octet-stream")
                {
                    string message = "";
                    var fileSizeKB = TeamMemberImageSize.CalculateSize(Convert.ToDouble(size));

                    if (fileSizeKB < 0)
                        return "The image size is too small.";

                    if (fileSizeKB > fileSize)
                        return "The file size is too large. Max valid size is up to " + fileSize + " KB";

                    return message;
                }

                return "This file type is not valid. Only valid file type is zip with *.zip extension";
            }
            catch (Exception ex)
            {
                return "This file type is not valid. Only valid file type is zip with *.zip extension";
            }
        }

        public void DeleteFile(string fileNameWithFullPath)
        {
            try
            {
                if (System.IO.File.Exists(fileNameWithFullPath))
                {
                    System.IO.File.Delete(fileNameWithFullPath);
                }
            }
            catch (Exception ex)
            {
                throw new InvalidDataException("faild to delete file");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AttendanceSynchronizerSetting hrAttendanceSynchronizerSettingObj, HttpPostedFileBase file = null)
        {

            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                string filename = "";
                if (ModelState.IsValid)
                {
                    string uploadFilePath = ConfigurationManager.AppSettings["DeviceSyncUpatePath"];
                    string baseUrl = (Request != null) ? Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') : "";
                    string tempFileName = "";

                    if (String.IsNullOrEmpty(baseUrl))
                    {
                        throw new InvalidDataException("Base url not found");
                    }

                    #region file Save

                    if (file != null)
                    {
                        string name = file.FileName;
                        string contentType = file.ContentType;
                        int size = file.ContentLength;
                        string errorMessage = CheckFileSettingsForTypeAndSize(name, contentType, size);
                        if (String.IsNullOrEmpty(errorMessage))
                        {
                            string filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + uploadFilePath);
                            if (String.IsNullOrEmpty(filePath))
                            {
                                throw new InvalidDataException(uploadFilePath + " Folder not found");
                            }
                            filename = DateTime.Now.ToString("yyyyMMdd_HHmmss") + "__" + name;
                            tempFileName = filename;
                            baseUrl += "/" + uploadFilePath + filename;
                            filename = Path.Combine(filePath, filename);
                            if (System.IO.File.Exists(filename))
                            {
                                throw new InvalidDataException(filename + " file allready exists. Give another.");
                            }
                            else
                            {
                                file.SaveAs(filename);
                            }
                        }
                        else
                        {
                            throw new InvalidDataException(errorMessage);
                        }
                    }
                    else
                    {
                        throw new InvalidDataException("No file found!");
                    }
                    hrAttendanceSynchronizerSettingObj.Name = tempFileName;
                    hrAttendanceSynchronizerSettingObj.UpdateUrl = baseUrl;
                    #endregion
                    
                    var isSuccess = _hrAttendanceSynchronizerSettingService.Save(hrAttendanceSynchronizerSettingObj);
                
                    if (isSuccess)
                    {
                        ViewBag.SuccessMessage = "Synchronizer Setting Add successfull";
                        ModelState.Clear();
                    }
                    else
                    {
                        if (System.IO.File.Exists(filename))
                        {
                            DeleteFile(filename);
                        }
                        ViewBag.ErrorMessage = "Synchronizer Setting Add Faild";
                        //return RedirectToAction("Index");
                    }
                }
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = "Duplicate Synchronizer found";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        #endregion

        #region Delete Operation

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                AttendanceSynchronizerSetting hRAttendanceSynchronizerSetting = _hrAttendanceSynchronizerSettingService.GetAttendanceSynchronizerSetting(id);
                _hrAttendanceSynchronizerSettingService.Delete(hRAttendanceSynchronizerSetting);
                return Json(new Response(true, "Attendance Synchronizer Setting Delete Successful !"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Attendance Synchronizer Setting Delete Fail !"));
            }
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(long id)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                AttendanceSynchronizerSetting hRAttendanceSynchronizerSetting = _hrAttendanceSynchronizerSettingService.GetAttendanceSynchronizerSetting(id);
                if (hRAttendanceSynchronizerSetting == null)
                {
                    return HttpNotFound();
                }

                return View(hRAttendanceSynchronizerSetting);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return View();
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AttendanceSynchronizerSetting hrAttendanceSynchronizerSettingObj, long id, HttpPostedFileBase file = null)
        {
            string filename = "";
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                if (ModelState.IsValid)
                {

                    #region file opreation 
                    
                    string uploadFilePath = ConfigurationManager.AppSettings["DeviceSyncUpatePath"];
                    string baseUrl = (Request != null) ? Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') : "";
                    string tempFileName = "";

                    if (String.IsNullOrEmpty(baseUrl))
                    {
                        throw new InvalidDataException("Base url not found");
                    }

                    #region file Save

                    if (file != null)
                    {
                        string name = file.FileName;
                        string contentType = file.ContentType;
                        int size = file.ContentLength;
                        string errorMessage = CheckFileSettingsForTypeAndSize(name, contentType, size);
                        if (String.IsNullOrEmpty(errorMessage))
                        {
                            string filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + uploadFilePath);
                            if (String.IsNullOrEmpty(filePath))
                            {
                                throw new InvalidDataException(uploadFilePath + " Folder not found");
                            }
                            filename = DateTime.Now.ToString("yyyyMMdd_HHmmss") + "__" + name;
                            tempFileName = filename;
                            baseUrl += "/" + uploadFilePath + filename;
                            filename = Path.Combine(filePath, filename);
                            if (System.IO.File.Exists(filename))
                            {
                                throw new InvalidDataException(filename + " file allready exists. Give another.");
                            }
                            else
                            {
                                file.SaveAs(filename);
                                AttendanceSynchronizerSetting tempObject = _hrAttendanceSynchronizerSettingService.GetAttendanceSynchronizerSetting(id);
                                if (tempObject != null)
                                {
                                    string tempDeletePath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + uploadFilePath) + tempObject.Name;
                                    DeleteFile(tempDeletePath);
                                }
                                hrAttendanceSynchronizerSettingObj.Name = tempFileName;
                                hrAttendanceSynchronizerSettingObj.UpdateUrl = baseUrl;
                            }
                        }
                        else
                        {
                            throw new InvalidDataException(errorMessage);
                        }
                    }
                    #endregion
                    
                    #endregion

                    _hrAttendanceSynchronizerSettingService.Update(hrAttendanceSynchronizerSettingObj, id);
                    ViewBag.SuccessMessage = "Synchronizer Setting Update successfull";
                    return View(_hrAttendanceSynchronizerSettingService.GetAttendanceSynchronizerSetting(id));
                }
            }
            catch (DuplicateEntryException ex)
            {
                if (!String.IsNullOrEmpty(filename))
                {
                    DeleteFile(filename);
                }
                ViewBag.ErrorMessage = "Duplicate Synchronizer Setting found";
            }
            catch (DependencyException ex)
            {
                if (!String.IsNullOrEmpty(filename))
                {
                    DeleteFile(filename);
                }
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                if (!String.IsNullOrEmpty(filename))
                {
                    DeleteFile(filename);
                }
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View(hrAttendanceSynchronizerSettingObj);
        }

        #endregion 
        
        #endregion

        #region Index/Manage Page
        public ActionResult Index()
        {
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
            return View();

        }
        [HttpPost]
        public ActionResult Index(int draw, int start, int length, string organizationId, string branchId, string campusId, string iPAddress)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        // orderBy = nvc["order[0][column]"];
                        orderBy = "Name";
                        orderDir = nvc["order[0][dir]"];
                    }
                    #endregion

                    int recordsTotal = _hrAttendanceSynchronizerSettingService.SynchronizerSettingRowCount();
                    long recordsFiltered = recordsTotal;
                    List<AttendanceSynchronizerSetting> dataList = _hrAttendanceSynchronizerSettingService.LoadSynchronizerSetting(start, length, orderBy, orderDir.ToUpper());
                    var data = new List<object>();
                    foreach (var c in dataList)
                    {
                        List<string> str = new List<string>();
                        str.Add(c.Name);
                        str.Add(c.SynchronizerVersion.ToString(CultureInfo.InvariantCulture));
                        str.Add(c.UpdateUrl);
                        str.Add(c.ReleaseDate.ToString("yyyy-MM-dd"));
                        str.Add(c.Description);
                        string edit = string.Format("<a href='{0}?id={1}' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp;<a id='{1}' href='#' data-name='{2}' class='glyphicon glyphicon-trash'> </a>",
                            Url.Action("Edit", "AttendanceSynchronizerSetting"),
                                c.Id.ToString(CultureInfo.InvariantCulture),
                                (c.Name != null) ? c.Name.ToString(CultureInfo.InvariantCulture) : "-"
                                );
                        str.Add(edit);
                        data.Add(str);
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return HttpNotFound();
        }
        
        #endregion
    }
}