using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IDailySupportAllowanceSettingDao : IBaseDao<DailySupportAllowanceSetting, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        DailySupportAllowanceSetting GetDailySupportAllowanceSetting(long orgId, long designationId, System.DateTime dateTime);

        #endregion

        #region List Loading Function

        IList<DailySupportAllowanceSetting> LoadDailySupportAllowanceSetting(int draw, int start, int length, List<long> authOrgList);
        IList<DailySupportAllowanceSetting> LoadDailySupportAllowanceSetting(long orgId, long designationId, long dailySupportAllowanceId);

        #endregion

        #region Others Function

        int GetDailySupportAllowanceSettingCount(List<long> authOrgList);
        DateTime GetEligibleClosingDateForPayrollSetting(long id);

        #endregion

    }

    public class DailySupportAllowanceSettingDao : BaseDao<DailySupportAllowanceSetting, long>, IDailySupportAllowanceSettingDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public DailySupportAllowanceSetting GetDailySupportAllowanceSetting(long organizationId, long designationId, DateTime dateTime)
        {
            var query = Session.QueryOver<DailySupportAllowanceSetting>().Where(x => x.Status == DailySupportAllowanceSetting.EntityStatus.Active);
            query.Where(x => x.Organization.Id == organizationId);
            query.Where(x => x.Designation.Id == designationId);
            query = query.Where(x => x.EffectiveDate <= dateTime);
            query = query.Where(x => x.ClosingDate == null || x.ClosingDate >= dateTime);
            return query.SingleOrDefault<DailySupportAllowanceSetting>();
        }

        #endregion

        #region List Loading Function

        public IList<DailySupportAllowanceSetting> LoadDailySupportAllowanceSetting(int draw, int start, int length, List<long> authOrgList)
        {
            var criteria = Session.CreateCriteria<DailySupportAllowanceSetting>();
            criteria.Add(Restrictions.Eq("Status", DailySupportAllowanceSetting.EntityStatus.Active));
            if (authOrgList.Count > 0)
            {
                criteria.CreateAlias("Organization", "org");
                criteria.Add(Restrictions.In("org.Id", authOrgList));
            }
            return criteria.SetFirstResult(start).SetMaxResults(length).List<DailySupportAllowanceSetting>();
        }

        public IList<DailySupportAllowanceSetting> LoadDailySupportAllowanceSetting(long orgId, long designationId, long dailySupportAllowanceId = 0)
        {
            var query = Session.QueryOver<DailySupportAllowanceSetting>().Where(x => x.Organization.Id == orgId && x.Designation.Id == designationId && x.Status == DailySupportAllowance.EntityStatus.Active);
            if (dailySupportAllowanceId > 0)
                query.Where(x => x.Id != dailySupportAllowanceId);
            return query.List<DailySupportAllowanceSetting>();
        }

        #endregion

        #region Others Function

        public int GetDailySupportAllowanceSettingCount(List<long> authOrgList)
        {
            var criteria = Session.CreateCriteria<DailySupportAllowanceSetting>();
            criteria.Add(Restrictions.Eq("Status", DailySupportAllowanceSetting.EntityStatus.Active));
            if (authOrgList.Count > 0)
                criteria.Add(Restrictions.In("Organization", authOrgList));
            return criteria.List<DailySupportAllowanceSetting>().Count;

        }

        public DateTime GetEligibleClosingDateForPayrollSetting(long id)
        {
            var query = @"select top(1) aSheet.DateTo from(
                    select * from [HR_AllowanceSheetFirstDetails] where DailySupportAllowanceSettingId=" + id;
            query += @") as aSheetSecDetail
                    left join HR_AllowanceSheet as aSheet on aSheet.Id= aSheetSecDetail.AllowanceSheetId and aSheet.IsFinalSubmit='true'
                    order by aSheet.DateTo desc ";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            return iQuery.UniqueResult<DateTime>();
        }

        #endregion

        #region Helper Function

        #endregion

    }
}
