﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.FestivalBonusSheetTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "FestivalBonusSheet")]
    public class FestivalBonusSheetSaveTest:FestivalBonusSheetBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Successfully_Save()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList);
            Assert.NotNull(_festivalBonusSheetService.LoadById(_festivalBonusSheetFactory.SingleObjectList.FirstOrDefault().Id));
        }

        [Fact]
        public void Should_Return_InvalidException_For_Null_object()
        {
            Assert.Throws<InvalidDataException>(
                () => _festivalBonusSheetService.SaveOrUpdate(null));
        }

        #endregion

        #region Business logic Test

        [Fact]
        public void Should_Return_MessageException_For_Null_TeamMember()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                // .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_JobBranch()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                //.WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_JobCampus()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                //.WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            Assert.Throws<MessageException>(
                 () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_JobDepartment()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                // .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            Assert.Throws<MessageException>(
                 () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_JobDesignation()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                //.WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_JobOrganization()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                //.WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_SalaryBranch()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                //.WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_SalaryCampus()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                //.WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_SalaryDepartment()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                //.WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_SalaryDesignation()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                // .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_SalaryOrganization()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                .WithFestivalSetting(_festivalBonusSettingFactory.Object)
                //.WithSalaryOrganization(org)
                ;

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        [Fact]
        public void Should_Return_MessageException_For_Null_FestivalBonusSetting()
        {
            var teamMember = PersistTeamMember();
            var employmentHistory = teamMember.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var campus = employmentHistory.Campus;
            var org = employmentHistory.Campus.Branch.Organization;
            var designation = employmentHistory.Designation;
            salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.SaveOrUpdateSalaryHistory(salaryHistoryFactory.Object);

            int year = employmentHistory.EffectiveDate.Year;
            int month = employmentHistory.EffectiveDate.Month;

            _festivalBonusSettingFactory.Create()
                .WithOrganization(org)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();


            _festivalBonusSheetFactory.CreateWith(0, (int)salaryHistoryFactory.Object.BankAmount,
                (int)salaryHistoryFactory.Object.CashAmount, false, month, year)
                .WithJobBranch(campus.Branch)
                .WithJobCampus(campus)
                .WithJobDepartment(department)
                .WithJobDesignation(designation)
                .WithJobOrganization(org)
                .WithSalaryBranch(campus.Branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(department)
                .WithSalaryDesignation(designation)
                .WithTeamMember(teamMember)
                //.WithFestivalSetting(_festivalBonusSettingFactory.Object)
                .WithSalaryOrganization(org);

            Assert.Throws<MessageException>(
                () => _festivalBonusSheetService.SaveOrUpdate(_festivalBonusSheetFactory.SingleObjectList));
        }

        #endregion
        
    }
}