using System;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class RequisitionDetailsFactory : ObjectFactoryBase<RequisitionDetails>
    {
        internal int RequiredQuantity = 100;
        internal readonly ISessionService _sessionService;
        internal readonly IProgramService _programService;
        internal readonly IItemGroupService _itemGroupService;
        internal readonly IItemService _itemService;
        internal readonly IProgramSessionItemService _itemProgramSessionService;
        private readonly IRequisitionDetailService _requisitionDetailService;
        internal readonly CommonHelper CommonHelper;


        internal ItemGroupFactory _itemGroupFactory;
        internal ProgramFactory _programFactory;
        internal SessionFactory _sessionFactory;
        internal ItemFactory _itemFactory;
        internal ProgramSessionItemFactory _programSessionItemFactory;

        public RequisitionDetailsFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _requisitionDetailService = new RequisitionDetailsService(session);
            _itemGroupService = new ItemGroupService(session);
            _itemService = new ItemService(session);
            _itemProgramSessionService = new ProgramSessionItemService(session);
            CommonHelper = new CommonHelper();

            _itemGroupFactory = new ItemGroupFactory(this, _session);
            _itemFactory = new ItemFactory(this, _session);
            _programFactory = new ProgramFactory(this, _session);
            _sessionFactory = new SessionFactory(this, _session);
            _programSessionItemFactory = new ProgramSessionItemFactory(this, _session);

        }

        #region Create

        public RequisitionDetailsFactory Create()
        {
            Object = new RequisitionDetails()
            {
                Status = RequisitionDetails.EntityStatus.Active,
                Purpose = (int)Purpose.ProgramPurpose,
                RequiredQuantity = RequiredQuantity,
                Program = null,
                Session = null,
                RequisitionStatus = (int)RequisitionStatus.Pending,
            };
            SingleObjectList.Add(Object);
            return this;
        }


        public RequisitionDetailsFactory Create(Organization organization, Requisition requisition)
        {

            _programFactory.Create().WithOrganization(organization).Persist();
            _sessionFactory.Create().Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.Create().WithItemGroup(_itemGroupFactory.Object).WithOrganization(organization).Persist();

            Object = new RequisitionDetails()
            {
                Status = RequisitionDetails.EntityStatus.Active,
                Purpose = (int)Purpose.ProgramPurpose,
                PurposeProgram = _programFactory.Object,
                PurposeSession = _sessionFactory.Object,
                Item = _itemFactory.Object,
                RequiredQuantity = RequiredQuantity,
                Program = _programFactory.Object,
                Session = _sessionFactory.Object,
                RequisitionStatus = (int)RequisitionStatus.Pending,
                Requisition = requisition
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public RequisitionDetailsFactory Create(int count, Organization organization, Requisition requisition)
        {
            _programFactory.Create().WithOrganization(organization).Persist();
            _sessionFactory.Create().Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.Create().WithItemGroup(_itemGroupFactory.Object).WithOrganization(organization).Persist();
            var purposeList = CommonHelper.LoadEmumToDictionary<Purpose>().Keys.ToList();
            int pI = 0;

            for (int i = 0; i < count; i++)
            {
                if (purposeList.Count == pI)
                {
                    pI = 0;
                }
                var obj = new RequisitionDetails()
                {
                    Status = RequisitionDetails.EntityStatus.Active,
                    Purpose = purposeList[pI],
                    PurposeProgram = _programFactory.Object,
                    PurposeSession = _sessionFactory.Object,
                    Item = _itemFactory.Object,
                    RequiredQuantity = RequiredQuantity,
                    Program = _programFactory.Object,
                    Session = _sessionFactory.Object,
                    RequisitionStatus = (int)RequisitionStatus.Pending,
                    Requisition = requisition
                };
                ObjectList.Add(obj);
                pI++;
            }
            return this;
        }

        public RequisitionDetailsFactory WithRequisition(Requisition requisition)
        {
            Object.Requisition = requisition;
            return this;
        }
        public RequisitionDetailsFactory WithItem(Item item)
        {
            Object.Item = item;
            return this;
        }
        public RequisitionDetailsFactory WithProgram(Program program)
        {
            Object.Program = program;
            return this;
        }
        public RequisitionDetailsFactory WithSession(Session session)
        {
            Object.Session = session;
            return this;
        }
        

        public RequisitionDetailsFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new RequisitionDetails() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public RequisitionDetailsFactory CleanUp()
        {

            DeleteAll();
            _itemFactory.CleanUp();
            _itemGroupFactory.CleanUp();
            _programFactory.Cleanup();
            _sessionFactory.Cleanup();
            return this;
        }

        #endregion

    }
}
