﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.ShiftTest
{
    public class ShiftBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal ShiftService _shiftService;
        internal OrganizationService _organizationService;
        internal List<long> IdList = new List<long>();
        
        private ISession _session;
        internal ShiftFactory _shiftFactory;

        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal const long OrgId = 1;
        internal DateTime _startTime = DateTime.Now.AddDays(1).AddHours(1);
        internal DateTime _endTime = DateTime.Now.AddDays(1).AddHours(10);
        #endregion

        public ShiftBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _shiftFactory = new ShiftFactory(null, _session);
            _shiftService = new ShiftService(_session);
            _organizationService = new OrganizationService(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _shiftFactory.Cleanup();
            base.Dispose();
        }
    }   
}
