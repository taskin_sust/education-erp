﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Exam;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.Services.Base;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface IAllowanceLogService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class AllowanceLogService : BaseService, IAllowanceLogService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        public AllowanceLogService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Functions

        public bool Save()
        {
            
            return true;
        }

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
       
        #endregion

        #region Others Function
       
       
        #endregion

        #region Helper function

       

        #endregion
    }
}
