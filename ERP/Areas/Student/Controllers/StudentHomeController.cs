﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class StudentHomeController : Controller
    {
        [AuthorizeRequired]
        public ActionResult Index()
        {
            return View();
        }
    }
}