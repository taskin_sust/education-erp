﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.ItemTest
{
    public class ItemBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal ItemService _itemService;
        internal ItemFactory _itemFactory;
        internal ItemGroupFactory _itemGroupFactory;
        internal ProgramSessionItemFactory _programSessionItemFactory;

        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";

        #endregion

        public ItemBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _itemFactory = new ItemFactory(null, _session);
            _programSessionItemFactory = new ProgramSessionItemFactory(null, _session);
            _itemService = new ItemService(_session);
        }

        public void Dispose()
        {
            _programSessionItemFactory.CleanUp();
            _itemFactory.CleanUp();
            base.Dispose();
        }
    }
}
