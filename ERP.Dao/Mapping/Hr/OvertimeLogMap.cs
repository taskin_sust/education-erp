using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class OvertimeLogMap : ClassMap<OvertimeLog>
    {
        public OvertimeLogMap() {
			Table("HR_OvertimeLog");
			LazyLoad();
			References(x => x.Overtime).Column("OvertimeId");
			Map(x => x.OverTimeDate).Column("OverTimeDate");
			Map(x => x.ApprovedTime).Column("ApprovedTime");

            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
        }
    }
}
