﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.AccessControl;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services.Description;
using FluentNHibernate.Conventions;
using FluentNHibernate.Testing.Values;
using FluentNHibernate.Utils;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using log4net;
using Microsoft.Ajax.Utilities;
using NHibernate.Criterion;
using NHibernate.Hql.Ast.ANTLR;
using NHibernate.Mapping;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.Areas.Student.Models;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Sms;
using UdvashERP.Services.Students;
using Font = iTextSharp.text.Font;

namespace UdvashERP.Areas.Exam.Controllers
{
    [UerpArea("Exam")]
    [Authorize]
    [AuthorizeAccess]

    public class MeritListGenerationController : Controller
    {
        #region Logger
        private readonly ILog logger = LogManager.GetLogger("ExamArea");
        #endregion

        #region Global variable

        public string _ProgramSession = "";
        public string _BranchName = "";
        public string _BatchName = "";
        public string _CampusName = "";
        public string _BatchDays = "";
        public List<string> _ColumnList;
        public Document document;
        public PdfWriter pdfWriter;
        public int _TopMargin = 0;
        public int _EventCalling = 0;
        public int _TemplateStartingPoint = 0;


        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly IProgramService _programService;
        private readonly IBranchService _branchService;
        private readonly IBatchService _batchService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly ISubjectService _subjectService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IStudentClassAttendanceService _classAttendanceService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly ICampusService _campusService;
        private readonly IExamsService _examsService;
        private readonly IExamsStudentMarksService _examsStudentMarksService;
        private readonly IExamsDetailsService _examsDetailsService;
        private readonly IStudentCourseDetailsService _studentCourseDetailsService;
        private List<UserMenu> _userMenu;
        private readonly IOrganizationService _organizationService;
        private readonly CommonHelper _commonHelper;
        private readonly ISmsService _smsService;
        private readonly IServiceBlockService _serviceBlockService;
        private TextEvents _textEvents;
        #endregion

        #region Report
        public MeritListGenerationController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _batchService = new BatchService(session);
                _sessionService = new SessionService(session);
                _classAttendanceService = new StudentClassAttendanceService(session);
                _courseService = new CourseService(session);
                _subjectService = new SubjectService(session);
                _studentProgramService = new StudentProgramService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
                _campusService = new CampusService(session);
                _examsService = new ExamsService(session);
                _examsStudentMarksService = new ExamsStudentMarksService(session);
                _examsDetailsService = new ExamsDetailsService(session);
                _organizationService = new OrganizationService(session);
                _studentCourseDetailsService = new StudentCourseDetailsService(session);
                _commonHelper = new CommonHelper();
                _smsService = new SmsService(session);
                _serviceBlockService = new ServiceBlockService(session);
                _textEvents = new TextEvents(this);

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                if (ex.InnerException != null)
                {
                    logger.Error(ex.InnerException);
                }
                else
                {
                    logger.Error(ex.Message);
                }
                Console.WriteLine("{0}", ex.Message);
            }
        }

        [HttpPost]
        public ActionResult GetExam(long programId, long sessionId, long[] courseId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Exams> examList =
                    _examsService.LoadExamByProgramAndSessionAndBranchsAndCampusesAndBatches(_userMenu, programId,
                        sessionId, courseId,
                        branchId, campusId, batchId, null, null, null,null);
                //IList<Exams> examList = _examsService.GetExamByProgramAndSessionAndBranchsAndCampusesAndBatches(programId, sessionId, courseId, branchId, campusId, batchId);
                List<SelectListItem> selectexamList = (new SelectList(examList, "Id", "Name")).ToList();
                if (examList.Any())
                {
                    selectexamList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Exams" });
                    return Json(new { examList = selectexamList, IsSuccess = true });
                }
                return Json(new { examList = new List<Exams>(), IsSuccess = true });
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return Json(new { examList = new List<Exams>(), IsSuccess = false });

            }
            return View();
        }

        [HttpGet]
        public ActionResult GenerateMeritList()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                MeritList meritListInfo = new MeritList();
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                //ViewBag.Program = new SelectList(_programService.LoadAuthorizedProgram(_userMenu), "Id", "Name");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedExam = new SelectList(new List<Exams>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedGenerateBy = new SelectList(new List<SelectListItem>()
                                                               {
                                                                   new SelectListItem() {Value = "0", Text = "Generate By"},
                                                                   new SelectListItem() {Value = SelectionType.TotalMarks.ToString(), Text = "Total Marks"},
                                                                   new SelectListItem() {Value = SelectionType.SubjectWiseMarks.ToString(), Text = "Subject wise Marks"}
                                                               }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
                ViewBag.SelectedGender = new SelectList(new List<SelectListItem>()
                                                               {
                                                                   new SelectListItem() {Value = "0", Text = "Select Gender"},
                                                                   new SelectListItem() {Value = SelectionType.All.ToString(), Text = "All"},
                                                                   new SelectListItem() {Value = SelectionType.Male.ToString(), Text = "Male"},
                                                                   new SelectListItem() {Value = SelectionType.Female.ToString(), Text = "Female"}
                                                                  // new SelectListItem() {Value = SelectionType.Other.ToString(), Text = "Other"}
                                                               }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());

                ViewBag.SelectedOrderBy = new SelectList(new List<SelectListItem>()
                                                               {
                                                                   new SelectListItem() {Value = "0", Text = "Order By"},
                                                                   new SelectListItem() {Value = SelectionType.MeritPosition.ToString(), Text = "Merit Position"},
                                                                   new SelectListItem() {Value = SelectionType.RollNumber.ToString(), Text = "Roll Number"}
                                                                   
                                                               }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
                ViewBag.SelectedMeritBy = new SelectList(new List<SelectListItem>()
                                                               {
                                                                  // new SelectListItem() {Value = "", Text = "Merit By"},
                                                                   new SelectListItem() {Value = SelectionType.TotalMarks.ToString(), Text = "Total Marks"},
                                                                   new SelectListItem() {Value = SelectionType.AverageMarks.ToString(), Text = "Percent Marks"}
                                                               }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());

                ViewBag.ExampTypes = new SelectList(new List<SelectListItem>()
                                                    {
                                                       new SelectListItem(){Value = "",Text = "Select exam type"},
                                                       new SelectListItem(){Value = "-1",Text = "All"},
                                                       new SelectListItem(){Value = Convert.ToString(ExamType.Mcq),Text = "MCQ"},
                                                       new SelectListItem(){Value = Convert.ToString(ExamType.Written),Text = "Written"}
                                                    }.ToList(), "value", "text");
                //ViewBag.AttendanceTypes = new SelectList(new List<SelectListItem>()
                //                                    {
                //                                       new SelectListItem(){Value = "",Text = "Select Attendance type"},
                //                                       new SelectListItem(){Value = "0",Text = "All"},
                //                                       //new SelectListItem(){Value = "1",Text = "Attended"},
                //                                       new SelectListItem(){Value = "1",Text = "Present"},
                //                                       new SelectListItem(){Value = "2",Text = "Absent"}
                //                                    }.ToList(), "value", "text");

                ViewBag.AttendanceTypes = "1";
                return View(meritListInfo);

            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }
            return View(new MeritList());
            //return View();
        }

        [HttpPost]
        public ActionResult GenerateMeritList(MeritList meritList, string selectedGenerateBy, string selectedGender, string selectedOrderBy, string selectedExampTypes, string selectedAttendanceTypes, string selectedMeritBy)
        {
            try
            {
                selectedAttendanceTypes = "1";
                var program = _programService.GetProgram(meritList.ProgramId);
                var session = _sessionService.LoadById(meritList.SessionId);
                var batchName = "";
                var branchName = "";
                var batchDays = "";
                var campusName = "";
                batchDays = meritList.SelectedBatchDays[0] == "0" ? "ALL" : string.Join(" ; ", meritList.SelectedBatchDays.ToArray());
                if (meritList.SelectedBatch[0] == 0)
                {
                    batchName = "ALL";
                }
                else
                {
                    IList<Batch> batches = _batchService.LoadBatch(null, null, null, null, null, meritList.SelectedBatch.ToList());
                    var batch = batches.GroupBy(x => x.Name);
                    batchName = batch.Aggregate(batchName, (current, batcNam) => current + (batcNam.Key + ", "));
                    batchName = batchName.Remove(batchName.Length - 2);
                }

                if (meritList.SelectedBranch[0] == 0)
                {
                    branchName = "ALL";
                }
                else
                {
                    IList<Branch> branches = _branchService.LoadBranch(null, null, null, meritList.SelectedBranch.ToList());
                    var batc = branches.GroupBy(x => x.Name);
                    branchName = batc.Aggregate(branchName, (current, brName) => current + (brName.Key + ", "));
                    branchName = branchName.Remove(branchName.Length - 2);
                }

                if (meritList.SelectedCampus[0] == 0)
                {
                    campusName = "ALL";
                }
                else
                {
                    IList<Campus> campuses = _campusService.LoadCampus(null, null, null, null, meritList.SelectedCampus.ToList());
                    var batc = campuses.GroupBy(x => x.Name);
                    campusName = batc.Aggregate(campusName, (current, caName) => current + (caName.Key + ", "));
                    campusName = campusName.Remove(campusName.Length - 2);
                }

                var json = new JavaScriptSerializer().Serialize(meritList);
                ViewData["MeritListObj"] = json;
                ViewData["GenerateBy"] = selectedGenerateBy;
                ViewData["Gender"] = selectedGender;
                ViewData["OrderBy"] = selectedOrderBy;
                ViewData["selectedExampTypes"] = selectedExampTypes;
                ViewData["selectedAttendanceTypes"] = selectedAttendanceTypes;
                ViewData["selectedMeritBy"] = selectedMeritBy;

                ViewBag.OrganizationName = program.Organization.Name;
                ViewBag.ProgramSession = program.Name + "-" + session.Name;
                ViewBag.Branch = branchName;
                ViewBag.BatchName = batchName;
                ViewBag.BatchDays = batchDays;
                ViewBag.CampusName = campusName;
                ViewBag.informationViewList = meritList.InformationViewList;
                return View("MeritList");
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return View();
                // throw;
            }

        }

        [HttpPost]
        public ActionResult GenerateMeritListPost(int draw, int start, int length, string MeritListObj = null, string selectedGenerateBy = null, string selectedGender = null, string selectedOrderBy = null, string finalMeritListFromView = null, string selectedExamType = null, string selectedAttendanceType = null, string selectedMeritBy = null, string prn = "")
        {
            IList<ExamMeritListDto> finaList = new List<ExamMeritListDto>();
            var meritList = new JavaScriptSerializer().Deserialize<MeritList>(MeritListObj);
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;

            // List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(meritList.OrganizationId), _commonHelper.ConvertIdToList(meritList.ProgramId), meritList.SelectedBranch.ToList(), _commonHelper.ConvertIdToList(meritList.SessionId), meritList.SelectedCampus.ToList(), meritList.SelectedBatchDays, meritList.SelectedBatchTime, null, selectedGender == "4" ? null : _commonHelper.ConvertIdToList(Convert.ToInt32(selectedGender))).ToList();

            List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(meritList.OrganizationId), _commonHelper.ConvertIdToList(meritList.ProgramId), meritList.SelectedBranch.ToList(), _commonHelper.ConvertIdToList(meritList.SessionId), meritList.SelectedCampus.ToList(), meritList.SelectedBatchDays, meritList.SelectedBatchTime, null, null).ToList();
            if (meritList.SelectedBatch[0] != 0)
            {
                batchList = batchList.Where(b => b.Id.In(meritList.SelectedBatch)).ToList();
            }

            var serviceBlocks = _serviceBlockService.LoadContentTypeList(meritList.ProgramId, meritList.SessionId, (int)BlockService.MeritList);

            //int TotalRow = _examsStudentMarksService.GetExamResultListCount(_userMenu, meritList.SelectedExam, selectedGenerateBy, selectedGender, selectedOrderBy, meritList.ProgramId, meritList.SessionId, meritList.SelectedCourse, meritList.SelectedBranch, meritList.SelectedCampus, selectedExamType, selectedAttendanceType, selectedMeritBy, batchList, serviceBlocks.ToList(), prn);
            int TotalRow = 0;
            finaList = _examsStudentMarksService.GetExamResultList(_userMenu, meritList.SelectedExam, start, length, selectedGenerateBy, selectedGender, selectedOrderBy, meritList.ProgramId, meritList.SessionId, meritList.SelectedCourse, meritList.SelectedBranch, meritList.SelectedCampus, selectedExamType, selectedAttendanceType, selectedMeritBy, batchList, serviceBlocks.ToList(), prn, TotalRow);

            var data = new List<object>();
            foreach (var datareportMeritList in finaList)
            {
                if (TotalRow == 0) { TotalRow = datareportMeritList.TotalRow; }
                var str = new List<string>();
                #region InfomationViewList
                if (meritList.InformationViewList != null)
                {
                    foreach (string informationView in meritList.InformationViewList)
                    {
                        switch (informationView)
                        {
                            case "Program Roll":
                                if (!string.IsNullOrEmpty(datareportMeritList.PrnNo))
                                {
                                    str.Add(datareportMeritList.PrnNo);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Full Name":
                                if (!string.IsNullOrEmpty(datareportMeritList.FullName))
                                {
                                    str.Add(datareportMeritList.FullName);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Nick Name":
                                if (!string.IsNullOrEmpty(datareportMeritList.NickName))
                                {
                                    str.Add(datareportMeritList.NickName);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Father Name":
                                if (!string.IsNullOrEmpty(datareportMeritList.FatherName))
                                {
                                    str.Add(datareportMeritList.FatherName);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Mobile Number(Father)":
                                if (!string.IsNullOrEmpty(datareportMeritList.GuardiansMobile1))
                                {
                                    str.Add(datareportMeritList.GuardiansMobile1);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Mobile Number(Mother)":
                                if (!string.IsNullOrEmpty(datareportMeritList.GuardiansMobile2))
                                {
                                    str.Add(datareportMeritList.GuardiansMobile2);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Mobile Number(Personal)":
                                if (!string.IsNullOrEmpty(datareportMeritList.Mobile))
                                {
                                    str.Add(datareportMeritList.Mobile);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Branch":
                                if (!string.IsNullOrEmpty(datareportMeritList.BranchName))
                                {
                                    str.Add(datareportMeritList.BranchName);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Batch":
                                if (!string.IsNullOrEmpty(datareportMeritList.BatchName))
                                {
                                    str.Add(datareportMeritList.BatchName);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Campus":
                                if (!string.IsNullOrEmpty(datareportMeritList.CampusName))
                                {
                                    str.Add(datareportMeritList.CampusName);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Institute":
                                if (!string.IsNullOrEmpty(datareportMeritList.Institute))
                                {
                                    str.Add(datareportMeritList.Institute);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Exam Name":
                                if (!string.IsNullOrEmpty(datareportMeritList.ExamName))
                                {
                                    str.Add(datareportMeritList.ExamName);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "MCQ Marks":
                                if (!string.IsNullOrEmpty(datareportMeritList.MCQMarks))
                                {
                                    str.Add(datareportMeritList.MCQMarks);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Written Marks":
                                if (!string.IsNullOrEmpty(datareportMeritList.WrittenMarks))
                                {
                                    str.Add(datareportMeritList.WrittenMarks);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Total Marks":
                                if (!string.IsNullOrEmpty(datareportMeritList.TotalMarks))
                                {
                                    str.Add(datareportMeritList.TotalMarks);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Highest Marks":
                                if (!string.IsNullOrEmpty(datareportMeritList.HighestMarks))
                                {
                                    str.Add(datareportMeritList.HighestMarks);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Full Marks":
                                if (!string.IsNullOrEmpty(datareportMeritList.FullMarks))
                                {
                                    str.Add(datareportMeritList.FullMarks);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Percent Marks":
                                if (!string.IsNullOrEmpty(datareportMeritList.PercentMarks))
                                {
                                    str.Add(datareportMeritList.PercentMarks);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Average Marks":
                                if (!string.IsNullOrEmpty(datareportMeritList.AverageMarks))
                                {
                                    str.Add(datareportMeritList.AverageMarks);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Branch Merit":
                                if (!string.IsNullOrEmpty(datareportMeritList.BMP))
                                {
                                    str.Add(datareportMeritList.BMP);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Central Merit":
                                if (!string.IsNullOrEmpty(datareportMeritList.CMP))
                                {
                                    str.Add(datareportMeritList.CMP);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Total Participant":
                                if (!string.IsNullOrEmpty(datareportMeritList.TotalParticipant))
                                {
                                    str.Add(datareportMeritList.TotalParticipant);
                                    break;
                                }
                                str.Add("");
                                break;
                            case "Total Student":
                                if (!string.IsNullOrEmpty(datareportMeritList.TotalStudent))
                                {
                                    str.Add(datareportMeritList.TotalStudent);
                                    break;
                                }
                                str.Add("");
                                break;

                            default:
                                break;
                        }
                    }
                }
                else
                {
                    str.Add(datareportMeritList.PrnNo.ToString());
                    str.Add(datareportMeritList.NickName.ToString());
                }
                #endregion
                str.Add("<a href='" + Url.Action("IndividualPerformanceReportView", "IndividualPerformance") + "?programRoll=" + datareportMeritList.PrnNo + "' data-id='" + datareportMeritList.PrnNo + "' class='glyphicon glyphicon-th-list'></a> ");

                data.Add(str);
            }

            ViewData["MeritListObj"] = meritList;
            ViewData["GenerateBy"] = selectedGenerateBy;
            ViewData["Gender"] = selectedGender;
            ViewData["OrderBy"] = selectedOrderBy;

            return Json(new
            {
                draw = draw,
                recordsTotal = TotalRow,
                recordsFiltered = TotalRow,
                start = start,
                length = length,
                data = data
            });
        }

        #endregion

        #region Send SMS
        public ActionResult MeritListSendSms()
        {
            try
            {
                if (TempData["SuccessMessages"] != null)
                {
                    ViewBag.SuccessMessage = TempData["SuccessMessages"];
                }
                if (TempData["ErrorMessages"] != null)
                {
                    ViewBag.ErrorMessage = TempData["ErrorMessages"];
                }
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedExam = new SelectList(new List<Exams>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.SelectedGender = new SelectList(new List<SelectListItem>()
                                                               {
                                                                   new SelectListItem() {Value = "0", Text = "Select Gender"},
                                                                   new SelectListItem() {Value = SelectionType.All.ToString(), Text = "All"},
                                                                   new SelectListItem() {Value = SelectionType.Male.ToString(), Text = "Male"},
                                                                   new SelectListItem() {Value = SelectionType.Female.ToString(), Text = "Female"},
                                                                   //new SelectListItem() {Value = SelectionType.Other.ToString(), Text = "Other"}
                                                               }.ToList(), "value", "text", SelectionType.SelelectAll.ToString());
                ViewBag.SelectedAttendanceType = new SelectList(new List<SelectListItem>()
                                                    {
                                                       new SelectListItem(){Value = "0",Text = "All"},
                                                       new SelectListItem(){Value = "1",Text = "Present"},
                                                       new SelectListItem(){Value = "2",Text = "Absent"}
                                                    }.ToList(), "value", "text");
                List<SelectListItem> examTypes = new List<SelectListItem>();
                examTypes.Add(new SelectListItem() { Text = "--Select Exam Type--", Value = "" });
                ViewBag.ExamTypeList = examTypes;
                var receiverNumberId = new long[3];
                receiverNumberId[0] = 1;
                receiverNumberId[1] = 2;
                receiverNumberId[2] = 3;
                var notDisplay = new long[2];
                notDisplay[0] = 30;
                notDisplay[1] = 31;
                var dynamicOpt = _smsService.LoadSmsTypeById(10).DynamicOptions.Where(x => !x.Id.In(31, 32)).ToList();
                ViewBag.SmsReceiverList = new SelectList(_smsService.LoadSmsReceiverByIds(receiverNumberId), "Id", "Name");
                ViewBag.SmsMaskName = new SelectList(new List<SmsMask>(), "Name", "Name");
                ViewBag.DynamicOptionsList = new SelectList(dynamicOpt, "Id", "Name");

                return View();

            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return View(new MeritList());
            }

        }

        [HttpPost]
        public ActionResult MeritListSendSms(MeritListSendMessageViewModel MeritListSendMessageViewModel)
        {
            IList<ExamMeritListDto> finaList = new List<ExamMeritListDto>();

            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(MeritListSendMessageViewModel.OrganizationId),
                _commonHelper.ConvertIdToList(MeritListSendMessageViewModel.ProgramId), MeritListSendMessageViewModel.SelectedBranch.ToList(),
                _commonHelper.ConvertIdToList(MeritListSendMessageViewModel.SessionId), MeritListSendMessageViewModel.SelectedCampus.ToList(),
                MeritListSendMessageViewModel.SelectedBatchDays, MeritListSendMessageViewModel.SelectedBatchTime, null, Convert.ToString(
                MeritListSendMessageViewModel.SelectedGender) == "4" ? null : _commonHelper.ConvertIdToList(Convert.ToInt32(Convert.ToString(
                MeritListSendMessageViewModel.SelectedGender)))).ToList();
            if (MeritListSendMessageViewModel.SelectedBatch[0] != 0)
            {
                batchList = batchList.Where(b => b.Id.In(MeritListSendMessageViewModel.SelectedBatch)).ToList();
            }
            var serviceBlocks = _serviceBlockService.LoadContentTypeList(MeritListSendMessageViewModel.ProgramId, MeritListSendMessageViewModel.SessionId, (int)BlockService.SendResult);
            //int TotalRow = _examsStudentMarksService.GetExamResultListCount(_userMenu, _commonHelper.ConvertIdToList(MeritListSendMessageViewModel.ExamId).ToArray(), "1", Convert.ToString(MeritListSendMessageViewModel.SelectedGender), "1", MeritListSendMessageViewModel.ProgramId, MeritListSendMessageViewModel.SessionId, MeritListSendMessageViewModel.SelectedCourse, MeritListSendMessageViewModel.SelectedBranch, MeritListSendMessageViewModel.SelectedCampus, Convert.ToString(MeritListSendMessageViewModel.SelectedExamType), "1", "1", batchList, serviceBlocks.ToList());

            int start = 0;
            int length = 0;// TotalRow;
            finaList = _examsStudentMarksService.GetExamResultList(_userMenu, _commonHelper.ConvertIdToList(MeritListSendMessageViewModel.ExamId).ToArray(), start, length,
                "1", Convert.ToString(MeritListSendMessageViewModel.SelectedGender), "1", MeritListSendMessageViewModel.ProgramId,
                MeritListSendMessageViewModel.SessionId, MeritListSendMessageViewModel.SelectedCourse, MeritListSendMessageViewModel.SelectedBranch,
                MeritListSendMessageViewModel.SelectedCampus, Convert.ToString(MeritListSendMessageViewModel.SelectedExamType), "1", "1", batchList, serviceBlocks.ToList());

            //int totalCandidates = finaList.Count;
            string campaignName = "MeritList " + DateTime.Now;
            int totalSendSmsNumber = 0;
            //int msgCount = 0;
            SmsType smsType = _smsService.LoadSmsTypeName("Manual Result List");//LoadSmsTypeById(10);

            foreach (var reportMeritList in finaList)
            {
                
                string message = MeritListSendMessageViewModel.InformationMessage;
                Program program = _studentProgramService.GetStudentProgram(reportMeritList.PrnNo).Program;
                BusinessModel.Entity.Students.Student student = _studentProgramService.GetStudentProgram(reportMeritList.PrnNo).Student;
                var mask = MeritListSendMessageViewModel.MaskName;
                var options = smsType.DynamicOptions;
                var smsToBeSend = new List<SmsViewModel>();
                #region Selected Option
                if (options.Any())
                {
                    foreach (var o in options)
                    {
                        string optionField = "[[{{" + o.Name + "}}]]";
                        string optionValue = "";
                        switch (o.Name)
                        {
                            case "Nick Name":
                                optionValue = reportMeritList.NickName;
                                break;
                            case "Program Roll":
                                optionValue = reportMeritList.PrnNo;
                                break;
                            case "Registration Number":
                                optionValue = reportMeritList.RegistrationNo;
                                break;
                            case "Exam Name":
                                optionValue = MeritListSendMessageViewModel.SelectedExam;
                                break;
                            case "MCQ Marks":
                                optionValue = reportMeritList.MCQMarks;
                                break;
                            case "Written Marks":
                                optionValue = reportMeritList.WrittenMarks;
                                break;
                            case "Total Marks":
                                optionValue = reportMeritList.TotalMarks;
                                break;
                            case "Highest Marks":
                                optionValue = reportMeritList.HighestMarks;
                                break;
                            case "Full Marks":
                                optionValue = reportMeritList.FullMarks;
                                break;
                            case "Average Marks":
                                optionValue = reportMeritList.AverageMarks;
                                break;
                            case "BMP":
                                optionValue = reportMeritList.BMP;
                                break;
                            case "CMP":
                                optionValue = reportMeritList.CMP;
                                break;
                            case "Total Participant":
                                optionValue = reportMeritList.TotalParticipant;//totalCandidates.ToString();
                                break;
                            case "Total Student":
                                optionValue = reportMeritList.TotalStudent;
                                break;
                            case "MCQ Full Marks":
                                optionValue = reportMeritList.MCQFullMarks;
                                break;
                            case "Written Full Marks":
                                optionValue = reportMeritList.WrittenFullMarks;
                                break;
                            case "Program":
                                optionValue = reportMeritList.ProgramShortName;
                                break;
                            case "Session":
                                optionValue = reportMeritList.SessionName;
                                break;
                            default:
                                optionValue = "";
                                break;
                        }
                        message = message.Replace(optionField, optionValue);
                    }
                }
                #endregion
                foreach (var srId in MeritListSendMessageViewModel.InformationReceiverList)
                {
                    var sr = _smsService.LoadReceiverById(Convert.ToInt64(srId));
                    if (sr != null)
                    {
                        var svm = new SmsViewModel();
                        switch (sr.Name)
                        {
                            case "Mobile Number (Personal)":
                                string studentMobile = SendSmsApi.CheckMobileNumber(reportMeritList.Mobile);
                                if (studentMobile != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = "88" + studentMobile;
                                }
                                break;
                            case "Mobile Number (Father)":
                                string guardiansMobile1 = SendSmsApi.CheckMobileNumber(reportMeritList.GuardiansMobile1);
                                if (guardiansMobile1 != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = "88" + guardiansMobile1;
                                }
                                break;
                            case "Mobile Number (Mother)":
                                string guardiansMobile2 = SendSmsApi.CheckMobileNumber(reportMeritList.GuardiansMobile2);
                                if (guardiansMobile2 != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = "88" + guardiansMobile2;
                                }
                                break;
                            default:
                                break;
                        }
                        if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                        {
                            var alreadyExist = smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                            if (!alreadyExist.Any())
                            {
                                smsToBeSend.Add(svm);
                            }
                        }
                    }
                }
                if (smsToBeSend.Any())
                {
                    if (String.IsNullOrEmpty(mask))
                    {
                        mask = "";
                    }
                    try
                    {
                        foreach (var sts in smsToBeSend)
                        {
                            var smsHistory = new SmsHistory();
                            smsHistory.ReceiverNumber = sts.ReceiverNumber;
                            smsHistory.SmsReceiver = _smsService.LoadReceiverById(sts.SmsReceiverId);
                            smsHistory.Sms = message;
                            smsHistory.Program = program;
                            smsHistory.Organization = program.Organization;
                            smsHistory.SmsSettings = null;
                            smsHistory.Type = Convert.ToInt32(smsType.Id);
                            smsHistory.Student = student;
                            smsHistory.CampaignName = campaignName;
                            smsHistory.Mask = mask;
                            smsHistory.Priority = 5;
                            smsHistory.Status = SmsHistory.EntityStatus.Pending;
                            _smsService.SaveSmsHistory(smsHistory);
                            totalSendSmsNumber++;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        //return Json(new { responseMsg = "SuccessFull", Issuccess = false }); 
                        //throw;
                    }
                    //return totalSendSmsNumber;
                }
            }
            ModelState.Clear();
            TempData["SuccessMessages"] = "Successfully saved data Total( " + totalSendSmsNumber + " )";

            return Json(new { responseMsg = "SuccessFull", Issuccess = true });
        }

        public JsonResult MeritListSendSmsPreview(MeritListSendMessageViewModel MeritListSendMessageViewModel)
        {
            try
            {
                IList<ExamMeritListDto> finaList = new List<ExamMeritListDto>();

                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(MeritListSendMessageViewModel.OrganizationId),
                    _commonHelper.ConvertIdToList(MeritListSendMessageViewModel.ProgramId), MeritListSendMessageViewModel.SelectedBranch.ToList(),
                    _commonHelper.ConvertIdToList(MeritListSendMessageViewModel.SessionId), MeritListSendMessageViewModel.SelectedCampus.ToList(),
                    MeritListSendMessageViewModel.SelectedBatchDays, MeritListSendMessageViewModel.SelectedBatchTime, null, Convert.ToString(
                    MeritListSendMessageViewModel.SelectedGender) == "4" ? null : _commonHelper.ConvertIdToList(Convert.ToInt32(Convert.ToString(
                    MeritListSendMessageViewModel.SelectedGender)))).ToList();
                if (MeritListSendMessageViewModel.SelectedBatch[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(MeritListSendMessageViewModel.SelectedBatch)).ToList();
                }
                var serviceBlocks = _serviceBlockService.LoadContentTypeList(MeritListSendMessageViewModel.ProgramId, MeritListSendMessageViewModel.SessionId, (int)BlockService.SendResult);
                //int TotalRow = _examsStudentMarksService.GetExamResultListCount(_userMenu, _commonHelper.ConvertIdToList(MeritListSendMessageViewModel.ExamId).ToArray(), "1", Convert.ToString(MeritListSendMessageViewModel.SelectedGender), "1", MeritListSendMessageViewModel.ProgramId, MeritListSendMessageViewModel.SessionId, MeritListSendMessageViewModel.SelectedCourse, MeritListSendMessageViewModel.SelectedBranch, MeritListSendMessageViewModel.SelectedCampus, Convert.ToString(MeritListSendMessageViewModel.SelectedExamType), "1", "1", batchList, serviceBlocks.ToList());
                int start = 0;
                int length = 1;
                finaList = _examsStudentMarksService.GetExamResultList(_userMenu, _commonHelper.ConvertIdToList(MeritListSendMessageViewModel.ExamId).ToArray(), start, length,
                    "1", Convert.ToString(MeritListSendMessageViewModel.SelectedGender), "1", MeritListSendMessageViewModel.ProgramId,
                    MeritListSendMessageViewModel.SessionId, MeritListSendMessageViewModel.SelectedCourse, MeritListSendMessageViewModel.SelectedBranch,
                    MeritListSendMessageViewModel.SelectedCampus, Convert.ToString(MeritListSendMessageViewModel.SelectedExamType), "1", "1", batchList, serviceBlocks.ToList());
                //finaList.OrderBy(x => x.BMP).Take(1);
                //int totalCandidates = TotalRow;
                string message = MeritListSendMessageViewModel.InformationMessage;
                foreach (var reportMeritList in finaList)
                {
                    SmsType smsType = _smsService.LoadSmsTypeById(10);
                    var options = smsType.DynamicOptions;
                    // var smsToBeSend = new List<SmsViewModel>();
                    #region Selected Option
                    if (options.Any())
                    {
                        foreach (var o in options)
                        {
                            string optionField = "[[{{" + o.Name + "}}]]";
                            string optionValue = "";
                            switch (o.Name)
                            {
                                case "Nick Name":
                                    optionValue = reportMeritList.NickName;
                                    break;
                                case "Program Roll":
                                    optionValue = reportMeritList.PrnNo;
                                    break;
                                case "Registration Number":
                                    optionValue = reportMeritList.RegistrationNo;
                                    break;
                                case "Exam Name":
                                    optionValue = MeritListSendMessageViewModel.SelectedExam;
                                    break;
                                case "MCQ Marks":
                                    optionValue = reportMeritList.MCQMarks;
                                    break;
                                case "Written Marks":
                                    optionValue = reportMeritList.WrittenMarks;
                                    break;
                                case "Total Marks":
                                    optionValue = reportMeritList.TotalMarks;
                                    break;
                                case "Highest Marks":
                                    optionValue = reportMeritList.HighestMarks;
                                    break;
                                case "Full Marks":
                                    optionValue = reportMeritList.FullMarks;
                                    break;
                                case "Average Marks":
                                    optionValue = reportMeritList.AverageMarks;
                                    break;
                                case "BMP":
                                    optionValue = reportMeritList.BMP;
                                    break;
                                case "CMP":
                                    optionValue = reportMeritList.CMP;
                                    break;
                                case "Total Participant":
                                    optionValue = reportMeritList.TotalParticipant;//totalCandidates.ToString();
                                    break;
                                case "Total Student":
                                    optionValue = reportMeritList.TotalStudent;
                                    break;
                                case "MCQ Full Marks":
                                    optionValue = reportMeritList.MCQFullMarks;
                                    break;
                                case "Written Full Marks":
                                    optionValue = reportMeritList.WrittenFullMarks;
                                    break;
                                case "Program":
                                    optionValue = reportMeritList.ProgramShortName;
                                    break;
                                case "Session":
                                    optionValue = reportMeritList.SessionName;
                                    break;
                                default:
                                    optionValue = "";
                                    break;
                            }
                            message = message.Replace(optionField, optionValue);
                        }
                    }
                    #endregion
                }

                return Json(new { returnPreviewSms = message, IsSuccess = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public JsonResult CountStudentForMeritListSendSms(long organizationId, long programId, long sessionId, long[] courseIds, long[] branchIds,
            long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long examId, int examType, int gender, int[] smsReciever)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(organizationId),
                _commonHelper.ConvertIdToList(programId), branchIds.ToList(),
                _commonHelper.ConvertIdToList(sessionId), campusIds.ToList(),
                batchDays, batchTimes, null, Convert.ToString(gender) == "4" ? null : _commonHelper.ConvertIdToList(Convert.ToInt32(Convert.ToString(gender)))).ToList();
                if (batchIds[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(batchIds)).ToList();
                }
                var serviceBlocks = _serviceBlockService.LoadContentTypeList(programId, sessionId, (int)BlockService.SendResult);
                //   var totalStudent = _examsStudentMarksService.CountStudentForMeritListSendSms(_userMenu, organizationId, programId, sessionId, courseIds, branchIds,
                //campusIds, batchDays, batchTimes, batchIds, examId, examType, gender);
                
                long[] c = new long[1]{0};
                
                int totalStudent = _examsStudentMarksService.GetExamResultListCount(_userMenu, _commonHelper.ConvertIdToList(examId).ToArray(), "1", Convert.ToString(gender), "1", programId, sessionId, c, branchIds, campusIds, Convert.ToString(examType), "1", "1", batchList, serviceBlocks.ToList());
                int totalMobileCount = _examsStudentMarksService.GetMobileCount(_userMenu, _commonHelper.ConvertIdToList(examId).ToArray(), "1", Convert.ToString(gender), "1", programId, sessionId, courseIds, branchIds, campusIds, Convert.ToString(examType), "1", "1", batchList, smsReciever, serviceBlocks.ToList());
                return Json(new { returnTotalCount = totalStudent, returnTotalMobileCount = totalMobileCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #region Edit
        public ActionResult EditMeritList(string programRoll)
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {

                logger.Error(e.Message);
                return View();
            }
        }
        #endregion

        #region Delete
        [HttpPost]
        public ActionResult DeleteIndividualMeritList(string MeritListObj, string selectedGenerateBy, string selectedGender,
            string selectedOrderBy, string programRoll)
        {
            try
            {
                var meritList = new JavaScriptSerializer().Deserialize<MeritList>(MeritListObj);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                bool isSuccess = _examsStudentMarksService.IsMeritListDeleteSuccess(_userMenu, meritList.SelectedExam, programRoll);
                //bool isSuccess = true;
                if (isSuccess)
                {
                    return Json(new Response(true, "Successfully Deleted"));
                }
                return Json(new Response(false, "Not Deleted"));
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return Json(new Response(false, "Something occurs wrong"));
            }
        }
        [HttpPost]
        public ActionResult Delete(string MeritListObj, string selectedGenerateBy, string selectedGender,
            string selectedOrderBy)
        {
            try
            {
                var meritList = new JavaScriptSerializer().Deserialize<MeritList>(MeritListObj);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                bool isSuccess = _examsStudentMarksService.IsMeritListDeleteSuccess(_userMenu, meritList.SelectedExam);
                //bool isSuccess = true;
                if (isSuccess)
                {
                    return Json(new Response(true, "Successfully Deleted"));
                }
                return Json(new Response(false, "Not Deleted"));
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return Json(new Response(false, "Something occurs wrong"));
            }
        }
        #endregion

        #region Excel Generation
        [HttpGet]
        public ActionResult MeritListExcelGeneration(string programSessionHeader, string branchNameHeader, string campusNameHeader, string batchDaysHeader, string batchNameHeader, string MeritListObj, string selectedGenerateBy, string selectedGender, string selectedOrderBy, string selectedExamType, string selectedAttendanceType, string selectedMeritBy)
        {
            try
            {
                List<string> headerList = new List<string>();
                headerList.Add(programSessionHeader);
                headerList.Add("Merit List");
                headerList.Add("Branch: " + branchNameHeader);
                headerList.Add("Campus: " + campusNameHeader);
                headerList.Add("Batch Days: " + batchDaysHeader);
                headerList.Add("Batch Name: " + batchNameHeader);
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                var meritList = new JavaScriptSerializer().Deserialize<MeritList>(MeritListObj);
                if (meritList.InformationViewList != null)
                {
                    foreach (var information in meritList.InformationViewList)
                    {
                        columnList.Add(information);
                    }
                }
                else
                {
                    columnList.Add("Prn Number");
                    columnList.Add("Nick Name");
                }
                var transactionXlsList = new List<List<object>>();

                IList<ExamMeritListDto> finaList = new List<ExamMeritListDto>();
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(meritList.OrganizationId), _commonHelper.ConvertIdToList(meritList.ProgramId), meritList.SelectedBranch.ToList(), _commonHelper.ConvertIdToList(meritList.SessionId), meritList.SelectedCampus.ToList(), meritList.SelectedBatchDays, meritList.SelectedBatchTime, null, selectedGender == "4" ? null : _commonHelper.ConvertIdToList(Convert.ToInt32(selectedGender))).ToList();
                if (meritList.SelectedBatch[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(meritList.SelectedBatch)).ToList();
                }
                var serviceBlocks = _serviceBlockService.LoadContentTypeList(meritList.ProgramId, meritList.SessionId, (int)BlockService.MeritList);
                //int TotalRow = _examsStudentMarksService.GetExamResultListCount(_userMenu, meritList.SelectedExam, selectedGenerateBy, selectedGender, selectedOrderBy, meritList.ProgramId, meritList.SessionId, meritList.SelectedCourse, meritList.SelectedBranch, meritList.SelectedCampus, selectedExamType, selectedAttendanceType, selectedMeritBy, batchList, serviceBlocks.ToList());
                int TotalRow = 0;
                finaList = _examsStudentMarksService.GetExamResultList(_userMenu, meritList.SelectedExam, 0, 0, selectedGenerateBy, selectedGender, selectedOrderBy, meritList.ProgramId, meritList.SessionId, meritList.SelectedCourse, meritList.SelectedBranch, meritList.SelectedCampus, selectedExamType, selectedAttendanceType, selectedMeritBy, batchList, serviceBlocks.ToList());

                //var data = new List<object>();
                foreach (var datareportMeritList in finaList)
                {
                    if (TotalRow == 0) { TotalRow = datareportMeritList.TotalRow; }
                    var str = new List<object>();

                    #region InfomationViewList

                    if (meritList.InformationViewList != null)
                    {
                        foreach (string informationView in meritList.InformationViewList)
                        {
                            switch (informationView)
                            {
                                case "Program Roll":
                                    if (!string.IsNullOrEmpty(datareportMeritList.PrnNo))
                                    {
                                        str.Add(datareportMeritList.PrnNo);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Full Name":
                                    if (!string.IsNullOrEmpty(datareportMeritList.FullName))
                                    {
                                        str.Add(datareportMeritList.FullName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Nick Name":
                                    if (!string.IsNullOrEmpty(datareportMeritList.NickName))
                                    {
                                        str.Add(datareportMeritList.NickName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Father Name":
                                    if (!string.IsNullOrEmpty(datareportMeritList.FatherName))
                                    {
                                        str.Add(datareportMeritList.FatherName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number(Father)":
                                    if (!string.IsNullOrEmpty(datareportMeritList.GuardiansMobile1))
                                    {
                                        str.Add(datareportMeritList.GuardiansMobile1);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number(Mother)":
                                    if (!string.IsNullOrEmpty(datareportMeritList.GuardiansMobile2))
                                    {
                                        str.Add(datareportMeritList.GuardiansMobile2);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number(Personal)":
                                    if (!string.IsNullOrEmpty(datareportMeritList.Mobile))
                                    {
                                        str.Add(datareportMeritList.Mobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Branch":
                                    if (!string.IsNullOrEmpty(datareportMeritList.BranchName))
                                    {
                                        str.Add(datareportMeritList.BranchName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Batch":
                                    if (!string.IsNullOrEmpty(datareportMeritList.BatchName))
                                    {
                                        str.Add(datareportMeritList.BatchName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Campus":
                                    if (!string.IsNullOrEmpty(datareportMeritList.CampusName))
                                    {
                                        str.Add(datareportMeritList.CampusName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Institute":
                                    if (!string.IsNullOrEmpty(datareportMeritList.Institute))
                                    {
                                        str.Add(datareportMeritList.Institute);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Exam Name":
                                    if (!string.IsNullOrEmpty(datareportMeritList.ExamName))
                                    {
                                        str.Add(datareportMeritList.ExamName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "MCQ Marks":
                                    if (!string.IsNullOrEmpty(datareportMeritList.MCQMarks))
                                    {
                                        str.Add(datareportMeritList.MCQMarks);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Written Marks":
                                    if (!string.IsNullOrEmpty(datareportMeritList.WrittenMarks))
                                    {
                                        str.Add(datareportMeritList.WrittenMarks);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Total Marks":
                                    if (!string.IsNullOrEmpty(datareportMeritList.TotalMarks))
                                    {
                                        str.Add(datareportMeritList.TotalMarks);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Highest Marks":
                                    if (!string.IsNullOrEmpty(datareportMeritList.HighestMarks))
                                    {
                                        str.Add(datareportMeritList.HighestMarks);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Full Marks":
                                    if (!string.IsNullOrEmpty(datareportMeritList.FullMarks))
                                    {
                                        str.Add(datareportMeritList.FullMarks);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Percent Marks":
                                    if (!string.IsNullOrEmpty(datareportMeritList.PercentMarks))
                                    {
                                        str.Add(datareportMeritList.PercentMarks);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Average Marks":
                                    if (!string.IsNullOrEmpty(datareportMeritList.AverageMarks))
                                    {
                                        str.Add(datareportMeritList.AverageMarks);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Branch Merit":
                                    if (!string.IsNullOrEmpty(datareportMeritList.BMP))
                                    {
                                        str.Add(datareportMeritList.BMP);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Central Merit":
                                    if (!string.IsNullOrEmpty(datareportMeritList.CMP))
                                    {
                                        str.Add(datareportMeritList.CMP);
                                        break;
                                    }
                                    str.Add("");
                                    break;

                                case "Total Participant":
                                    if (!string.IsNullOrEmpty(datareportMeritList.TotalParticipant))
                                    {
                                        str.Add(datareportMeritList.TotalParticipant);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Total Student":
                                    if (!string.IsNullOrEmpty(datareportMeritList.TotalStudent))
                                    {
                                        str.Add(datareportMeritList.TotalStudent);
                                        break;
                                    }
                                    str.Add("");
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                    else
                    {
                        str.Add(datareportMeritList.PrnNo.ToString());
                        str.Add(datareportMeritList.NickName.ToString());
                    }
                    #endregion
                    //str.Add("<a href='" + Url.Action("IndividualPerformanceReportView", "IndividualPerformance") + "?programRoll=" + datareportMeritList.PrnNo + "' data-id='" + datareportMeritList.PrnNo + "' class='glyphicon glyphicon-th-list'></a> ");

                    transactionXlsList.Add(str);
                }

                ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "MeritListReport__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("Autoclose");
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return View("Autoclose");
            }
        }
        #endregion

        #region Pdf Generation
        [HttpPost]
        public void GeneratePdf(string MeritListObj, string selectedGenerateBy, string selectedGender, string selectedOrderBy, string selectedExamType, string selectedAttendanceType, string selectedMeritBy)
        {
            try
            {
                IList<ExamMeritListDto> finaList = new List<ExamMeritListDto>();
                var meritList = new JavaScriptSerializer().Deserialize<MeritList>(MeritListObj);
                Program program = _programService.GetProgram(meritList.ProgramId);
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                /*Total column Calculation*/
                List<string> columnList = new List<string>();
                if (meritList.InformationViewList != null)
                {
                    foreach (var information in meritList.InformationViewList)
                    {
                        columnList.Add(information);
                    }
                }
                else
                {
                    columnList.Add("Prn Number");
                    columnList.Add("Nick Name");
                }
                columnList.Insert(0, "SL.");
                _ColumnList = columnList;
                /*Batch FIlter*/
                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(meritList.OrganizationId), _commonHelper.ConvertIdToList(meritList.ProgramId), meritList.SelectedBranch.ToList(), _commonHelper.ConvertIdToList(meritList.SessionId), meritList.SelectedCampus.ToList(), meritList.SelectedBatchDays, meritList.SelectedBatchTime, null, selectedGender == "4" ? null : _commonHelper.ConvertIdToList(Convert.ToInt32(selectedGender))).ToList();
                if (meritList.SelectedBatch[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(meritList.SelectedBatch)).ToList();
                }
                /*Get Final MeritList */
                var serviceBlocks = _serviceBlockService.LoadContentTypeList(meritList.ProgramId, meritList.SessionId, (int)BlockService.MeritList);
                finaList = _examsStudentMarksService.GetExamResultList(_userMenu, meritList.SelectedExam, 0, 0, selectedGenerateBy, selectedGender,
                        selectedOrderBy, meritList.ProgramId, meritList.SessionId, meritList.SelectedCourse, meritList.SelectedBranch, meritList.SelectedCampus,
                        selectedExamType, selectedAttendanceType, selectedMeritBy, batchList, serviceBlocks.ToList());
                //int topmarg = topmargincalculator(_branchname, _campusname);
                //_topmargin = topmarg;

                #region ItextSharp Implementation

                document = new Document(PageSize.LEGAL);
                var wid = PageSize.LEGAL.Width;
                document.SetMargins(-50, -50, 30, 5);
                //var widt = PageSize.A4.Width;
                using (var memeoryStream = new MemoryStream())
                {
                    pdfWriter = PdfWriter.GetInstance(document, memeoryStream);
                    pdfWriter.PageEvent = new TextEvents(this);
                    document.Open();

                    #region table 1

                    PdfPTable table1 = new PdfPTable(1);
                    PdfPCell tempCell1 = new PdfPCell();
                    tempCell1.Border = 0;
                    tempCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                    tempCell1.VerticalAlignment = Element.ALIGN_TOP;
                    tempCell1.FixedHeight = 22;
                    tempCell1.Image =
                        iTextSharp.text.Image.GetInstance(
                            Server.MapPath(Url.Content("~") + program.Organization.ReportImageUrl));
                    //tempCell1.Image.ScaleToFit(80,40);
                    table1.AddCell(tempCell1);

                    #region Font Declaration

                    var blackListTextFontBig = FontFactory.GetFont("Calibri", 12);
                    var blackListTextFont = FontFactory.GetFont("Calibri", 10);
                    var blackListSmallTextFont = FontFactory.GetFont("Calibri", 8);
                    var headerFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8);

                    #endregion


                    PdfPCell table1Cell2 = new PdfPCell(new Phrase(new Chunk(_ProgramSession, blackListTextFontBig)));
                    //table1Cell2.MinimumHeight = 10f;
                    table1Cell2.Colspan = 1;
                    table1Cell2.PaddingTop = -2;
                    //programHeaderCell.HorizontalAlignment = 1;
                    table1Cell2.Border = 0;
                    table1Cell2.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1Cell2.VerticalAlignment = Element.ALIGN_TOP;
                    table1Cell2.FixedHeight = 20;
                    table1.AddCell(table1Cell2);

                    //MeritList Table
                    //PdfPCell table1Cell2 = new PdfPCell(new Phrase("MeritList"));
                    //table1Cell2.MinimumHeight = 10f;
                    //table1Cell2.Colspan = 1;
                    //table1Cell2.Border = 0;
                    //table1Cell2.HorizontalAlignment = Element.ALIGN_CENTER;
                    //table1.AddCell(table1Cell2);

                    #endregion

                    #region table 2

                    PdfPTable table2 = new PdfPTable(4);
                    PdfPCell table2Cell1 = new PdfPCell();

                    //table2Cell1.MinimumHeight = 20f;
                    //cell.FixedHeight = 100f;
                    table2Cell1.Colspan = 4;
                    table2Cell1.Border = 0;
                    table2Cell1.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2Cell1.VerticalAlignment = Element.ALIGN_TOP;
                    table2Cell1.PaddingBottom = 5;

                    table2.AddCell(table2Cell1);
                    table2.AddCell(new Phrase(new Chunk("Branch :", blackListTextFont)));
                    table2.AddCell(new Phrase(new Chunk(_BranchName, blackListTextFont)));
                    table2.AddCell(new Phrase(new Chunk("Campus :", blackListTextFont)));
                    table2.AddCell(new Phrase(new Chunk(_CampusName, blackListTextFont)));
                    table2.AddCell(new Phrase(new Chunk("Batch Days :", blackListTextFont)));
                    table2.AddCell(new Phrase(new Chunk(_BatchDays, blackListTextFont)));
                    table2.AddCell(new Phrase(new Chunk("Batch Name :", blackListTextFont)));
                    table2.AddCell(new Phrase(new Chunk(_BatchName, blackListTextFont)));

                    #endregion

                    #region Table 3

                    PdfPTable table3 = new PdfPTable(_ColumnList.Count); //Table creation with n column
                    float[] table3Widths = new float[_ColumnList.Count];
                    var index = 0;
                    //var len = PageSize.A4.Width;
                    PdfPCell pdfPCell = new PdfPCell();
                    pdfPCell.PaddingTop = 0;
                    pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    foreach (var column in columnList)
                    {
                        if (index == 0)
                        {
                            table3Widths[index] = 50;
                        }
                        else if (index == 1)
                        {
                            table3Widths[index] = 70;
                        }
                        else
                        {
                            if (table3Widths.Length > 0)

                                table3Widths[index] =
                                    (int)
                                        ((int)((PageSize.LEGAL.Width + 100) - ((table3Widths[0] + table3Widths[1]))) /
                                         (ColumnList.Count - 2));
                        }
                        pdfPCell.Phrase = new Phrase(column, headerFont);
                        table3.AddCell(pdfPCell); // column add within table
                        index++;
                    }
                    table3.SetWidths(table3Widths);

                    #endregion

                    #region Table 4

                    PdfPTable table4 = new PdfPTable(columnList.Count); //Table creation with n column
                    PdfPCell table4Cell1 = new PdfPCell();
                    table4Cell1.PaddingBottom = 4;

                    #endregion

                    #region spacing

                    table1.SpacingAfter = 10;
                    table2.SpacingAfter = 25;
                    table3.SpacingAfter = 0;

                    #endregion

                    //_TemplateStartingPoint = (int)((int)table2.SpacingAfter + table2.HeaderHeight + table2.GetRowHeight(0) +
                    //                                 table2.GetRowHeight(1));

                    #region Data filtering according to user selection

                    var dataList = new List<List<Object>>();
                    int slNo = 0;
                    foreach (var datareportMeritList in finaList)
                    {
                        slNo++;
                        var str = new List<object>();

                        #region InfomationViewList

                        if (meritList.InformationViewList != null)
                        {

                            foreach (string informationView in columnList)
                            {

                                switch (informationView)
                                {
                                    case "SL.":

                                        str.Add(slNo.ToString());

                                        break;

                                    case "Program Roll":
                                        if (!string.IsNullOrEmpty(datareportMeritList.PrnNo))
                                        {
                                            str.Add(datareportMeritList.PrnNo);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Full Name":
                                        if (!string.IsNullOrEmpty(datareportMeritList.FullName))
                                        {
                                            str.Add(datareportMeritList.FullName);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Nick Name":
                                        if (!string.IsNullOrEmpty(datareportMeritList.NickName))
                                        {
                                            str.Add(datareportMeritList.NickName);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Father Name":
                                        if (!string.IsNullOrEmpty(datareportMeritList.FatherName))
                                        {
                                            str.Add(datareportMeritList.FatherName);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Mobile Number(Father)":
                                        if (!string.IsNullOrEmpty(datareportMeritList.GuardiansMobile1))
                                        {
                                            str.Add(datareportMeritList.GuardiansMobile1);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Mobile Number(Mother)":
                                        if (!string.IsNullOrEmpty(datareportMeritList.GuardiansMobile2))
                                        {
                                            str.Add(datareportMeritList.GuardiansMobile2);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Mobile Number(Personal)":
                                        if (!string.IsNullOrEmpty(datareportMeritList.Mobile))
                                        {
                                            str.Add(datareportMeritList.Mobile);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Branch":
                                        if (!string.IsNullOrEmpty(datareportMeritList.BranchName))
                                        {
                                            str.Add(datareportMeritList.BranchName);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Batch":
                                        if (!string.IsNullOrEmpty(datareportMeritList.BatchName))
                                        {
                                            str.Add(datareportMeritList.BatchName);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Campus":
                                        if (!string.IsNullOrEmpty(datareportMeritList.CampusName))
                                        {
                                            str.Add(datareportMeritList.CampusName);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Institute":
                                        if (!string.IsNullOrEmpty(datareportMeritList.Institute))
                                        {
                                            str.Add(datareportMeritList.Institute);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Exam Name":
                                        if (!string.IsNullOrEmpty(datareportMeritList.ExamName))
                                        {
                                            str.Add(datareportMeritList.ExamName);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "MCQ Marks":
                                        if (!string.IsNullOrEmpty(datareportMeritList.MCQMarks))
                                        {
                                            str.Add(datareportMeritList.MCQMarks);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Written Marks":
                                        if (!string.IsNullOrEmpty(datareportMeritList.WrittenMarks))
                                        {
                                            str.Add(datareportMeritList.WrittenMarks);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Total Marks":
                                        if (!string.IsNullOrEmpty(datareportMeritList.TotalMarks))
                                        {
                                            str.Add(datareportMeritList.TotalMarks);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Highest Marks":
                                        if (!string.IsNullOrEmpty(datareportMeritList.HighestMarks))
                                        {
                                            str.Add(datareportMeritList.HighestMarks);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Full Marks":
                                        if (!string.IsNullOrEmpty(datareportMeritList.FullMarks))
                                        {
                                            str.Add(datareportMeritList.FullMarks);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Percent Marks":
                                        if (!string.IsNullOrEmpty(datareportMeritList.PercentMarks))
                                        {
                                            str.Add(datareportMeritList.PercentMarks);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Average Marks":
                                        if (!string.IsNullOrEmpty(datareportMeritList.AverageMarks))
                                        {
                                            str.Add(datareportMeritList.AverageMarks);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Branch Merit":
                                        if (!string.IsNullOrEmpty(datareportMeritList.BMP))
                                        {
                                            str.Add(datareportMeritList.BMP);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Central Merit":
                                        if (!string.IsNullOrEmpty(datareportMeritList.CMP))
                                        {
                                            str.Add(datareportMeritList.CMP);
                                            break;
                                        }
                                        str.Add("");
                                        break;

                                    case "Total Participant":
                                        if (!string.IsNullOrEmpty(datareportMeritList.TotalParticipant))
                                        {
                                            str.Add(datareportMeritList.TotalParticipant);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Total Student":
                                        if (!string.IsNullOrEmpty(datareportMeritList.TotalStudent))
                                        {
                                            str.Add(datareportMeritList.TotalStudent);
                                            break;
                                        }
                                        str.Add("");
                                        break;

                                    default:
                                        break;
                                }
                            }
                        }
                        else
                        {
                            str.Add(datareportMeritList.PrnNo.ToString());
                            str.Add(datareportMeritList.NickName.ToString());
                        }

                        #endregion

                        dataList.Add(str);
                    }

                    #endregion

                    var bodyTextFont = FontFactory.GetFont("calibri", 8);
                    float[] table4Widths = new float[_ColumnList.Count];
                    table4Cell1.HorizontalAlignment = Element.ALIGN_CENTER;
                    foreach (var singledataObj in dataList) // Meritlist data for each and every student 
                    {
                        var indexCounter = 0;
                        foreach (var singleColumnValue in singledataObj)
                        // Meritlist column value for each and every student 
                        {
                            if (indexCounter == 0)
                            {
                                table4Widths[indexCounter] = 50;
                            }
                            else if (indexCounter == 1)
                            {
                                table4Widths[indexCounter] = 70;
                            }
                            else
                            {
                                if (table4Widths.Length > 0)

                                    table4Widths[indexCounter] =
                                        (int)
                                            ((int)
                                                ((PageSize.LEGAL.Width + 100) - ((table4Widths[0] + table4Widths[1]))) /
                                             (ColumnList.Count - 2));
                            }
                            indexCounter++;
                            //PdfPCell pdfPCellfData = new PdfPCell();
                            table4Cell1.Phrase = new Phrase(new Chunk(singleColumnValue.ToString(), bodyTextFont));
                            table4.AddCell(table4Cell1);
                        }
                    }
                    table4.SetWidths(table4Widths);
                    document.Add(table1);
                    document.Add(table2);
                    document.Add(table3);
                    document.Add(table4); // add table to document

                    document.Close();
                    Response.BinaryWrite(memeoryStream.GetBuffer());
                }
                /*Sample Pdf generation */
                //Response.ClearContent();
                //Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + "MeritList_" + _ProgramSession + ".pdf");
                //Response.AddHeader
                //("Content-Disposition", "attachment; filename=" + strS);
                //Response.TransmitFile(System.Web.HttpContext.Current.Server.MapPath("~/Uploads/demopdf.pdf"));
                Response.End();

                //Response.Flush();
                //Response.Clear();
                #endregion
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private int TopMarginCalculator(string branchName, string campusName)
        {
            int branchLengh = branchName.Length;
            int campusLength = campusName.Length;
            const int defaultVal = 150;
            int result = 1;
            if (branchLengh > 15 || campusLength > 15)
            {
                if (branchLengh >= campusLength)
                {
                    branchLengh = branchLengh / 15;
                    result = result * branchLengh;
                    result = defaultVal + result * 5;
                }
                else
                {
                    campusLength = campusLength / 15;
                    result = result * campusLength;
                    result = defaultVal + result * 5;
                }


                return result;
            }
            return defaultVal;

        }

        [HttpGet]
        public void MeritListPdfGeneration(string programSessionHeader, string branchNameHeader, string campusNameHeader, string batchDaysHeader,
            string batchNameHeader, string MeritListObj, string selectedGenerateBy, string selectedGender, string selectedOrderBy, string selectedExamType,
            string selectedAttendanceType, string selectedMeritBy)
        {
            try
            {
                _ProgramSession = programSessionHeader;
                _BranchName = branchNameHeader;
                _CampusName = campusNameHeader;
                _BatchDays = batchDaysHeader;
                _BatchName = batchNameHeader;
                GeneratePdf(MeritListObj, selectedGenerateBy, selectedGender, selectedOrderBy, selectedExamType, selectedAttendanceType, selectedMeritBy)
                ;
                #region Old Code

                //var meritList = new JavaScriptSerializer().Deserialize<MeritList>(MeritListObj);
                //List<string> columnList = new List<string>();
                //if (meritList.InformationViewList != null)
                //{
                //    foreach (var information in meritList.InformationViewList)
                //    {
                //        columnList.Add(information);
                //    }
                //}
                //else
                //{
                //    columnList.Add("Prn Number");
                //    columnList.Add("Nick Name");
                //}

                //var json = new JavaScriptSerializer().Serialize(meritList);
                //ViewData["MeritListObj"] = json;
                //ViewData["GenerateBy"] = selectedGenerateBy;
                //ViewData["Gender"] = selectedGender;
                //ViewData["OrderBy"] = selectedOrderBy;
                //ViewData["selectedExampTypes"] = selectedExamType;
                //ViewData["selectedAttendanceTypes"] = selectedAttendanceType;
                //ViewData["selectedMeritBy"] = selectedMeritBy;
                //ViewBag.ProgramSession = programSessionHeader;
                //ViewBag.Branch = branchNameHeader;
                //ViewBag.BatchName = batchNameHeader;
                //ViewBag.BatchDays = batchDaysHeader;
                //ViewBag.CampusName = campusNameHeader;
                //ViewBag.informationViewList = meritList.InformationViewList;
                //ViewBag.Columns = columnList;
                //return View();
                //return new Rotativa.ViewAsPdf()
                #endregion

            }
            catch (Exception e)
            {
                throw;
            }
        }

        #region Operation Function Json(Ajax Call)

        [HttpPost]
        public ActionResult AjaxRequestForExamCoad(string query, string programId, string sessionId, long[] courseId)
        {
            try
            {
                SelectList examsList = new SelectList(_examsService.GetExamCodeByAutoComplete(query, Convert.ToInt64(programId), Convert.ToInt64(sessionId), courseId), "Id", "Code");
                List<SelectListItem> selectExamList = examsList.ToList();
                return Json(new { returnList = selectExamList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        //[HttpPost]
        //public JsonResult GetProgramByOrganization(int? organizatonId)
        //{
        //    try
        //    {
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var programList = _programService.LoadAuthorizedProgram(_userMenu, (organizatonId == null) ? null : _commonHelper.ConvertIdToList(Convert.ToInt64(organizatonId)));
        //        var programSelectList = new SelectList(programList, "Id", "Name");
        //        return Json(new { programList = programSelectList, IsSuccess = true });
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error(e.Message);
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}

        #endregion

        public ActionResult Index()
        {
            return View();
        }

        #region Properties Goes here

        public int TemplateStartingPoint { get { return _TemplateStartingPoint; } }
        public int EventCall
        {
            get { return _EventCalling; }
        }
        public List<string> ColumnList
        {
            get { return _ColumnList; }
        }
        public int TopMargin
        {
            get { return _TopMargin; }

        }
        public string ProgramSessionName
        {
            get { return _ProgramSession; }

        }

        public string BranchName
        {
            get { return _BranchName; }

        }

        public string CampusName
        {
            get { return _CampusName; }

        }

        public string BatchName
        {
            get { return _BatchName; }

        }

        public string BatchDays
        {
            get { return _BatchDays; }

        }
        #endregion
        #endregion
    }

    #region Pdf Events
    public class TextEvents : PdfPageEventHelper
    {
        private MeritListGenerationController _meritListGenerationController;
        public TextEvents(MeritListGenerationController meritListGenerationControllerRef)
        {
            _meritListGenerationController = meritListGenerationControllerRef;

        }

        // This is the contentbyte object of the writer
        PdfContentByte cb;
        // we will put the final number of pages in a template
        PdfTemplate headerTemplate, footerTemplate, infoTemplate;
        // this is the BaseFont we are going to use for the header / footer
        BaseFont bf = null;
        // This keeps track of the creation time
        DateTime PrintTime = DateTime.Now;

        #region Fields
        private string _header;
        #endregion

        #region Properties

        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }
        #endregion
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                PrintTime = DateTime.Now;
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                headerTemplate = cb.CreateTemplate(100, 10);
                infoTemplate = cb.CreateTemplate(100, 100);
                footerTemplate = cb.CreateTemplate(50, 50);
            }
            catch (DocumentException de)
            {
                throw de;

            }
            catch (System.IO.IOException ioe)
            {
                throw ioe;
            }
        }
        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            const int labelHeight = 12;
            const int labelHeightlt = 14;
            base.OnEndPage(writer, document);
            //document.SetMargins(5, 5, 80, 5);
            iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            _meritListGenerationController._EventCalling += 1;
            var blackListTextFont = FontFactory.GetFont("Calibri", 8);
            var headerFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 8);
            #region
            PdfPTable infoTable = new PdfPTable(_meritListGenerationController.ColumnList.Count); //Table creation with n column
            float[] table5Widths = new float[_meritListGenerationController.ColumnList.Count];
            var index = 0;
            PdfPCell pdfPCell = new PdfPCell();
            //pdfPCell.PaddingBottom = 4;
            pdfPCell.PaddingTop = 0;
            pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfPCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            foreach (var column in _meritListGenerationController.ColumnList)
            {
                if (index == 0)
                {
                    table5Widths[index] = 50;
                }
                else if (index == 1) { table5Widths[index] = 70; }
                else
                {
                    if (table5Widths.Length > 0)

                        table5Widths[index] = (int)((int)((PageSize.LEGAL.Width + 100) - ((table5Widths[0] + table5Widths[1]))) / (_meritListGenerationController.ColumnList.Count - 2));
                }
                pdfPCell.Phrase = new Phrase(column, headerFont);
                infoTable.AddCell(pdfPCell); // column add within table
                index++;
            }
            infoTable.SetWidths(table5Widths);
            #endregion
            //First row print for all page goes here 
            // Phrase 
            #region Header Info Design Here
            String text = "Page " + writer.PageNumber + " of ";
            #region Templating code for header and Infobar

            //Add paging to header
            //{
            //    cb.BeginText();
            //    cb.SetFontAndSize(bf, 8);
            //    cb.SetTextMatrix(document.PageSize.GetRight(200), document.PageSize.GetTop(20));
            //    cb.ShowText(text);
            //    cb.EndText();
            //    float len = bf.GetWidthPoint(text, 8);
            //    //Adds "12" in Page 1 of 12
            //    cb.AddTemplate(headerTemplate, document.PageSize.GetRight(200) + len, document.PageSize.GetTop(20));
            //}
            //Add Firstrow for every page
            {
                //cb.BeginText();
                var ct = new ColumnText(cb);
                ct.Alignment = Element.ALIGN_CENTER;
                if (_meritListGenerationController.EventCall == 1)
                {
                    //document.SetMargins(2, 2, 100, 5);
                    ct.SetSimpleColumn(5, 30, PageSize.LEGAL.Width - 5, (PageSize.LEGAL.Height - 142));
                }
                else if (_meritListGenerationController.EventCall > 1)
                {
                    if (_meritListGenerationController.ColumnList.Count == 12)
                    {
                        ct.SetSimpleColumn(-50, 0, PageSize.LEGAL.Width + 50, (PageSize.LEGAL.Height - labelHeight));
                    }
                    else if (_meritListGenerationController.ColumnList.Count > 8)
                    {
                        ct.SetSimpleColumn(-50, 0, PageSize.LEGAL.Width + 50, (PageSize.LEGAL.Height - labelHeight));
                    }
                    else if (_meritListGenerationController.ColumnList.Count <= 8)
                    {
                        ct.SetSimpleColumn(-50, 0, PageSize.LEGAL.Width + 50, (PageSize.LEGAL.Height - labelHeightlt));
                    }
                }

                if (_meritListGenerationController.EventCall == 1) { ct.AddElement(new PdfPTable(1)); }
                else
                {
                    ct.AddElement(infoTable);
                    int status = ct.Go();
                }

            }
            //Add paging to footer
            //{
            //    cb.BeginText();
            //    cb.SetFontAndSize(bf, 12);
            //    cb.SetTextMatrix(document.PageSize.GetRight(180), document.PageSize.GetBottom(30));
            //    cb.ShowText(text);
            //    cb.EndText();
            //    float len = bf.GetWidthPoint(text, 12);
            //    cb.AddTemplate(footerTemplate, document.PageSize.GetRight(180) + len, document.PageSize.GetBottom(30));
            //}
            #endregion

            #region alignment
            // set the alignment of all three cells and set border to 0
            //pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;

            //pdfCell1.VerticalAlignment = Element.ALIGN_BOTTOM;

            //pdfCell1.Border = 0;

            //add all three cells into PdfTable
            //pdfTab.AddCell(pdfCell1);
            //pdfTab.TotalWidth = document.PageSize.Width - 80f;
            //pdfTab.WidthPercentage = 70;
            #endregion
            //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
            //first param is start row. -1 indicates there is no end row and all the rows to be included to write
            //Third and fourth param is x and y position to start writing
            //pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);
            //headerInfoTable.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);
            //set pdfContent value

            //Move the pointer and draw line to separate header section from rest of page
            //cb.MoveTo(40, document.PageSize.Height - 100);
            //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
            cb.Stroke();
            #endregion


            //Move the pointer and draw line to separate footer section from rest of page
            //cb.MoveTo(40, document.PageSize.GetBottom(50));
            //cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
            //cb.Stroke();
        }
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            headerTemplate.BeginText();
            headerTemplate.SetFontAndSize(bf, 12);
            headerTemplate.SetTextMatrix(0, 0);
            headerTemplate.ShowText((writer.PageNumber - 1).ToString());
            headerTemplate.EndText();

            infoTemplate.BeginText();
            infoTemplate.SetFontAndSize(bf, 12);
            infoTemplate.SetTextMatrix(0, 0);
            //infoTemplate.ShowText((writer.PageNumber - 1).ToString());
            infoTemplate.EndText();

            footerTemplate.BeginText();
            footerTemplate.SetFontAndSize(bf, 12);
            footerTemplate.SetTextMatrix(0, 0);
            footerTemplate.ShowText((writer.PageNumber - 1).ToString());
            footerTemplate.EndText();


        }
    }
    #endregion
    public interface ITableHeader
    {
        string ProgramSessionName { get; set; }
        string BranchName { get; set; }
        string CampusName { get; set; }

        string BatchName { get; set; }
        string BatchDays { get; set; }

    }

}
