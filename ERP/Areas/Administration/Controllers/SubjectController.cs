﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.Services.Administration;
using UdvashERP.App_Start;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class SubjectController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private Subject _subjectObj;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ISubjectService _subjectService;
        private readonly IProgramSessionSubjectService _programSessionSubjectService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private IList<Subject> _subjectList;
        private List<UserMenu> _userMenus;
        public SubjectController()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            _subjectObj = new Subject();
            _organizationService = new OrganizationService(nHsession);
            _programService = new ProgramService(nHsession);
            _sessionService = new SessionService(nHsession);
            _subjectService = new SubjectService(nHsession);
            _userService = new UserService(nHsession);
            _programSessionSubjectService = new ProgramSessionSubjectService(nHsession);
            _commonHelper = new CommonHelper();
            _subjectList = new List<Subject>();
        }

        #endregion

        #region Index/Manage Session

        #region Manage Subject
        public ActionResult Index()
        {
            try
            {
                GetStatusText();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                GetStatusText();
            }
            return View();
        }

        #endregion

        #region Render Data Table
        public JsonResult SubjectList(int draw, int start, int length, string name, string shortName, string rank, string status)
        {
            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "ShortName";
                            break;
                        case "2":
                            orderBy = "Rank";
                            break;
                        case "3":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                #endregion

                int recordsTotal = _subjectService.GetSubjectCount(name, shortName, rank, status);
                long recordsFiltered = recordsTotal;

                _subjectList = _subjectService.LoadSubject(start, length, orderBy, orderDir.ToUpper(), name, shortName, rank, status).ToList();
                Subject subjectObj = new Subject();
                int[] maxMinRank = new int[2];
                maxMinRank[0] = _subjectService.GetMinimumRank(subjectObj);// 1;
                maxMinRank[1] = _subjectService.GetMaximumRank(subjectObj);

                foreach (var subject in _subjectList)
                {
                    var str = new List<string>();
                    str.Add(subject.Name);
                    str.Add(subject.ShortName);
                    string rankbtn = subject.Rank.ToString();
                    if (subject.Rank > maxMinRank[0])
                    {
                        rankbtn += "<a href='#'  id='" + subject.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                    }
                    if (subject.Rank < maxMinRank[1])
                    {
                        rankbtn += "<a href='#'  id='" + subject.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                    }
                    str.Add(rankbtn);
                    str.Add(StatusTypeText.GetStatusText(subject.Status));
                    str.Add(
                        "<a href='" + Url.Action("Details", "Subject") + "?Id=" + subject.Id + "' data-id='" + subject.Id + "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;&nbsp"
                        +
                        "<a href='" + Url.Action("Edit", "Subject") + "?Id=" + subject.Id + "' data-id='" + subject.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; "
                        +
                        "<a  id='" + subject.Id.ToString() + "'    href='#' data-name='" + subject.Name.ToString() + "' class='glyphicon glyphicon-trash'></a> "
                        );
                    getData.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }
        #endregion

        #region Rank Operation
        public JsonResult RankChange(long id, int current, string action)
        {
            try
            {
                Subject subjectUpdateObj = _subjectService.GetSubject(Convert.ToInt64(id));
                if (subjectUpdateObj == null)
                {
                    return null;
                }
                int newRank;
                if (action == "up")
                    newRank = subjectUpdateObj.Rank - 1;
                else
                    newRank = subjectUpdateObj.Rank + 1;
                var subjectOldObj = _subjectService.GetSubjectByRankNextOrPrevious(newRank, action); //LoadByRank(newRank);
                newRank = subjectOldObj.Rank;
                subjectOldObj.Rank = subjectUpdateObj.Rank;
                subjectUpdateObj.Rank = newRank;
                bool isSuccess = _subjectService.UpdateRank(subjectOldObj, subjectUpdateObj);
                if (isSuccess)
                {
                    return Json("Rank Changes");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json("Something Wrong Occured!!");
        }

        #endregion

        #endregion

        #region Operational Function

        #region Save Operation
        public ActionResult Create(string message)
        {
            ViewBag.SuccessMessage = message;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Subject subjectObj)
        {
            try
            {
                bool isSucess = _subjectService.SubjectSaveOrUpdate(subjectObj);
                if (isSucess)
                {
                    return RedirectToAction("Create", new { message = "Subject successfully saved." });
                }
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
            }
            return View();
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(long id)
        {
            try
            {
                _subjectObj = _subjectService.GetSubject(id);
                GetStatusText();
                return View(_subjectObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                GetStatusText();
                TempData["CommonErrorMessage"] = ex.Message;
                return RedirectToAction("Error", "Home", new { area = "" });
            }
        }

        [HttpPost]
        public ActionResult Edit(long id, Subject subjectObj)
        {
            try
            {
                bool isSuccess = _subjectService.SubjectSaveOrUpdate(subjectObj);
                if (isSuccess)
                    ViewBag.SuccessMessage = "Subject successfully updated.";
                else
                    ViewBag.ErrorMessage = "Problem Occurred, during subject update.";
                InitializeEditView(id);
                return View(_subjectObj);
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
                InitializeEditView(id);
                return View(_subjectObj);
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
                InitializeEditView(id);
                return View(_subjectObj);
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
                InitializeEditView(id);
                return View(_subjectObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                GetStatusText();
                TempData["CommonErrorMessage"] = ex.Message;
                return RedirectToAction("Error", "Home", new { area = "" });
            }
        }

        #region Initialize Edit View
        private void InitializeEditView(long id)
        {
            try
            {
                GetStatusText();
                _subjectObj = _subjectService.GetSubject(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isSuccess = _subjectService.Delete(id);
                if (isSuccess)
                {
                    return Json(new Response(true, "Subject sucessfully deleted."));
                }
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
            }
            return Json(new Response(false, "Problem Occurred. Please Retry"));
        }
        #endregion

        #region Details Operation
        public ActionResult Details(long id)
        {
            try
            {
                _subjectObj = _subjectService.GetSubject(id);
                if (_subjectObj != null)
                {
                    _subjectObj.CreateByText = _userService.GetUserNameByAspNetUserId(_subjectObj.CreateBy);
                    _subjectObj.ModifyByText = _userService.GetUserNameByAspNetUserId(_subjectObj.ModifyBy);
                }
                return View(_subjectObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                TempData["CommonErrorMessage"] = ex.Message;
                return RedirectToAction("Error", "Home", new { area = "" });
            }
        }
        #endregion

        #endregion

        #region Subject Assign

        [HttpGet]
        public ActionResult SubjectAssign(string message)
        {
            try
            {
                _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.ErrorMessage = message;
                var subjectList = _subjectService.LoadSubject();
                var assigedtSubjectListFromProgramSession = new List<Subject>();
                var viewModel = new SubjectAssign
                {
                    AllSubject = subjectList,
                    Subjects = assigedtSubjectListFromProgramSession
                };
                //ViewBag.OrganizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenus), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionList = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.Subjects = subjectList;
                return View(viewModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                TempData["CommonErrorMessage"] = ex.Message;
                return RedirectToAction("Error", "Home", new { area = "" });
            }
        }

        [HttpPost]
        public ActionResult SubjectAssign(SubjectAssign subjectAssign, long organizationId, long programId, long sessionId)
        {
            _userMenus = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                if (programId > 0 && sessionId > 0)
                {
                    if (subjectAssign.SelectedSubjects != null)
                    {
                        var message = _programSessionSubjectService.SaveUpdateSubject(subjectAssign, programId, sessionId);
                        if (message != "")
                        {
                            var confirmationMessage = message.Split(new string[] { "||" }, StringSplitOptions.None);
                            if (!String.IsNullOrEmpty(confirmationMessage[0]))
                            {
                                ViewBag.WarningMessage = "Can't remove " + confirmationMessage[0].Remove(confirmationMessage[0].Length - 1);
                            }
                            if (!String.IsNullOrEmpty(confirmationMessage[1]))
                            {
                                ViewBag.SuccessMessage = "These subjects have already been assigned. " + confirmationMessage[1];
                            }
                        }
                        else
                            ViewBag.SuccessMessage = "Subject successfully assigned";
                    }
                    else
                        return RedirectToAction("SubjectAssign", new { message = "You need to select subject for subject assign" });
                }
                else
                    return RedirectToAction("SubjectAssign", new { message = "You need to select program or subject." });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
            }

            #region Initialization
            //ViewBag.OrganizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
            //ViewBag.ProgramList = new SelectList(_programService.LoadProgram(_commonHelper.ConvertIdToList(Convert.ToInt64(organizationId))), "Id", "Name");
            //ViewBag.SessionList = new SelectList(_sessionService.LoadSession(_commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), _commonHelper.ConvertIdToList(Convert.ToInt64(programId))), "Id", "Name");
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenus), "Id", "ShortName");
            ViewBag.ProgramList = new SelectList(_programService.LoadAuthorizedProgram(_userMenus, _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId))), "Id", "Name");
            ViewBag.SessionList = new SelectList(_sessionService.LoadAuthorizedSession(_userMenus, _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), _commonHelper.ConvertIdToList(Convert.ToInt64(programId))), "Id", "Name");
            var subjectList = _subjectService.LoadSubject();
            ViewBag.Subjects = subjectList;

            #region Get Selected Subject
            var assigedtSubjectListFromProgramSession = new List<Subject>();
            var alreadyAssignedSubjectList = _programSessionSubjectService.LoadProgramSessionSubject(programId, sessionId);
            if (alreadyAssignedSubjectList != null && alreadyAssignedSubjectList.Count > 0)
            {
                foreach (var pss in alreadyAssignedSubjectList)
                {
                    assigedtSubjectListFromProgramSession.Add(pss.Subject);
                }
            }
            var viewModel = new SubjectAssign
            {
                AllSubject = subjectList,
                Subjects = assigedtSubjectListFromProgramSession
            };
            #endregion

            #endregion

            return View(viewModel);
        }
        #endregion

        #region Ajax Request Function

        //get the selected subject
        [HttpPost]
        public ActionResult GetSubject(long sessionId, long programId)
        {
            try
            {
                var assigedtSubjectListFromProgramSession = new List<Subject>();
                //get the already assigned subject from program_session_subject table
                var alreadyAssignedSubjectList = _programSessionSubjectService.LoadProgramSessionSubject(programId, sessionId);
                //initialize subject
                var subjectList = _subjectService.LoadSubject();
                ViewBag.Subjects = subjectList;
                if (alreadyAssignedSubjectList != null && alreadyAssignedSubjectList.Count > 0)
                {
                    //here pss program_session_subject
                    foreach (var pss in alreadyAssignedSubjectList)
                    {
                        assigedtSubjectListFromProgramSession.Add(pss.Subject);
                    }
                }
                var viewModel = new SubjectAssign();
                viewModel.AllSubject = subjectList;
                viewModel.Subjects = assigedtSubjectListFromProgramSession;
                return View("SubjectList", viewModel);
            }
            catch (Exception ex)
            {
                TempData["CommonErrorMessage"] = ex.Message;
                _logger.Error(ex);
                return RedirectToAction("Error", "Home", new { area = "" });
            }
            //return View();
        }
        #endregion

        #region Helper Function
        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
        }

        #endregion
    }
}
