﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BranchTest
{
    interface IBranchLoadActiveTest
    {
        void LoadActive_should_return_list();
        void LoadActive_should_Not_return_list();
        void Load_Branch_Successfully_With_BranchIdList();
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Branch")]
    public class BranchLoadActiveTest : BranchBaseTest, IBranchLoadActiveTest
    {
        #region Basic Test
        
        [Fact]
        public void LoadActive_should_return_list()
        {
            organizationFactory.Create().Persist();
            _branchFactory.CreateMore(10, organizationFactory.SingleObjectList).Persist();
            IList<Branch> branchList = _branchService.LoadBranch(0, 10, "Name", "Asc", organizationFactory.Object.Id.ToString(), "", "", "1");
            Assert.Equal(10, branchList.Count);

            IList<Branch> branchLists = _branchService.LoadBranch(0, 5, "", "", "", "", "", "");
            Assert.Equal(5, branchLists.Count);

            IList<Branch> brLists = _branchService.LoadBranch(0, 5, "", "", "", "", "", "1");
            Assert.Equal(5, brLists.Count);

            IList<Branch> bLists = _branchService.LoadBranch(0, 0, "", "", "", "", "", "1");
            Assert.Equal(0, bLists.Count);
        }

        [Fact]
        public void LoadActive_should_Not_return_list()
        {
            IList<Branch> branchList = _branchService.LoadBranch(0, 10, "Name", "Asc", "-1", "", "", "1");
            Assert.Equal(0, branchList.Count);
        }

        public void Load_Branch_Successfully_With_BranchIdList()
        {
            organizationFactory.Create().Persist();
            _branchFactory.CreateMore(10, organizationFactory.SingleObjectList).Persist();
            IList<Branch> bList = _branchService.LoadBranch(_branchFactory.ObjectList.Select(x=>x.Id).ToArray());
            Assert.Equal(10,bList.Count);
        }
        #endregion
    }
}
