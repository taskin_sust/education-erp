﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Sms;
using UdvashERP.Dao.Teachers;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherPublicRegistrationService : IBaseService
    {
        #region Operational Function

        void SaveTeacherRegistration(TeacherPublicRegistration teacherPublicRegistration);
        void DeletePublicTeacher(long id);

        #endregion

        #region Single Instances Loading Function

        TeacherPublicRegistration GetById(long Id);
        #endregion

        #region List Loading Function

        IList<TeacherPublicRegistration> LoadRegisterTeacherList(List<UserMenu> userMenu, int start, int length, string orderBy = "NickName", string orderDirection = "ASC", List<long> organizationIdList = null, List<long> teacherActivityIdList = null, List<long> subjectIdList = null, List<long> religionIdList = null, List<long> versionOfStudies = null, List<long> genderIdList = null, int? academicStudentStatus = null, int? teacherApplicationStatus = null, string nickName = "", string institute = "", string mobileNumber = "");

        #endregion

        #region Others Function
        
        int GetRegisterTeacherCount(List<UserMenu> userMenu, List<long> organizationIdList = null, List<long> teacherActivityIdList = null, List<long> subjectIdList = null, List<long> religionIdList = null, List<long> versionOfStudies = null, List<long> genderIdList = null, int? academicStudentStatus = null, int? teacherApplicationStatus = null, string nickName = "", string institute = "", string mobileNumber = "");

        #endregion

    }

    public class TeacherPublicRegistrationService : BaseService, ITeacherPublicRegistrationService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization

        private readonly ITeacherPublicRegistrationDao _teacherPublicRegistrationDao;
        private readonly ITeacherDao _teacherDao;
        private readonly ICommonHelper _commonHelper;
        private readonly ISmsHistoryDao _hrHistoryDao;
        private readonly ISmsSettingsDao _smsSettingsDao;
        public TeacherPublicRegistrationService(ISession session)
        {
            Session = session;
            _teacherPublicRegistrationDao = new TeacherPublicRegistrationDao { Session = session };
            _teacherDao = new TeacherDao { Session = session };
            _hrHistoryDao = new SmsHistoryDao { Session = session };
            _smsSettingsDao = new SmsSettingsDao { Session = session };
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Function

        public void SaveTeacherRegistration(TeacherPublicRegistration teacherPublicRegistration)
        {
            ITransaction transaction = null;
            try
            {
                #region check 

                Regex mobileRgx = new Regex(@"^(?:\+?88)?0?1[15-9]\d{8}$");
                Regex HscRgx = new Regex(@"^(\d{4})$");
                Regex emailRgx =
                    new Regex(
                        @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");

                if (String.IsNullOrEmpty(teacherPublicRegistration.FullName))
                {
                    throw new InvalidDataException("Please enter your Full Name.");
                }
                if (String.IsNullOrEmpty(teacherPublicRegistration.NickName))
                {
                    throw new InvalidDataException("Please enter your Nick Name.");
                }
                if (String.IsNullOrEmpty(teacherPublicRegistration.MobileNumber1))
                {
                    throw new InvalidDataException("Please enter your Mobile Number1.");
                }

                if (!String.IsNullOrEmpty(teacherPublicRegistration.MobileNumber1))
                {
                    if (!mobileRgx.IsMatch(teacherPublicRegistration.MobileNumber1))
                    {
                        throw new InvalidDataException("Please enter valid Mobile Number 1.");
                    }
                    else
                    {
                        if (teacherPublicRegistration.MobileNumber1.Length == 11)
                            teacherPublicRegistration.MobileNumber1 = "88" + teacherPublicRegistration.MobileNumber1;
                        else if (teacherPublicRegistration.MobileNumber1.Length == 10)
                            teacherPublicRegistration.MobileNumber1 = "880" + teacherPublicRegistration.MobileNumber1;
                    }
                }
                if (!String.IsNullOrEmpty(teacherPublicRegistration.MobileNumber2))
                {
                    if (!mobileRgx.IsMatch(teacherPublicRegistration.MobileNumber2))
                    {
                        throw new InvalidDataException("Please enter valid Mobile Number 2.");
                    }
                    else
                    {
                        if (teacherPublicRegistration.MobileNumber2.Length == 11)
                            teacherPublicRegistration.MobileNumber2 = "88" + teacherPublicRegistration.MobileNumber2;
                        else if (teacherPublicRegistration.MobileNumber1.Length == 10)
                            teacherPublicRegistration.MobileNumber2 = "880" + teacherPublicRegistration.MobileNumber2;
                    }
                }
                if (teacherPublicRegistration.MobileNumber1 == teacherPublicRegistration.MobileNumber2)
                {
                    throw new InvalidDataException("Mobile Number 1 and Moblie Number 2 can't be same, please use different number.");
                }

                if (CheckDublicateMobileNumber(teacherPublicRegistration.MobileNumber1) == true ||
                    _teacherDao.IsDuplicateMobileNumber(teacherPublicRegistration.MobileNumber1) == true)
                {
                    throw new InvalidDataException("This mobile number 1 is already taken. Please give other mobile number for Mobile number 1.");
                }


                if (String.IsNullOrEmpty(teacherPublicRegistration.Email) ||
                    !emailRgx.IsMatch(teacherPublicRegistration.Email))
                {
                    throw new InvalidDataException("Please enter valid Email Address.");
                }

                if (String.IsNullOrEmpty(teacherPublicRegistration.Institute))
                {
                    throw new InvalidDataException("Please enter Institute name.");
                }

                if (String.IsNullOrEmpty(teacherPublicRegistration.Department))
                {
                    throw new InvalidDataException("Please enter Department name.");
                }

                if (teacherPublicRegistration.HscPassingYear == null || teacherPublicRegistration.HscPassingYear <= 0)
                {
                    throw new InvalidDataException("Please enter your HSC passing year.");
                }
                else
                {
                    if (!HscRgx.IsMatch(teacherPublicRegistration.HscPassingYear.ToString()) ||
                        teacherPublicRegistration.HscPassingYear > DateTime.Now.Year ||
                        teacherPublicRegistration.HscPassingYear < DateTime.Now.Year - 100)
                    {
                        throw new InvalidDataException("Please enter valid HSC passing year.");
                    }
                }
                if (teacherPublicRegistration.Religion == null || teacherPublicRegistration.Religion <= 0)
                {
                    throw new InvalidDataException("Please enter your Religion.");
                }
                else
                {
                    List<int> religionInList =
                        _commonHelper.LoadEmumToDictionary<Religion>().Select(x => x.Key).ToList();
                    if (!religionInList.Contains(teacherPublicRegistration.Religion))
                    {
                        throw new InvalidDataException("Please enter valid Religion.");
                    }
                }

                if (teacherPublicRegistration.Gender == null || teacherPublicRegistration.Gender <= 0)
                {
                    throw new InvalidDataException("Please enter your Gender.");
                }
                else
                {
                    List<int> genderIdList =
                        _commonHelper.LoadEmumToDictionary<Gender>(new List<int> {(int) Gender.Combined})
                            .Select(x => x.Key)
                            .ToList();
                    if (!genderIdList.Contains(teacherPublicRegistration.Gender))
                    {
                        throw new InvalidDataException("Please enter valid Gender.");
                    }
                }

                #region version
                List<int> vsesionIdList = _commonHelper.LoadEmumToDictionary<VersionOfStudy>(new List<int> {(int) VersionOfStudy.Combined})
                        .Select(x => x.Key)
                        .ToList();
                if (teacherPublicRegistration.VersionOfStudy1 == null || teacherPublicRegistration.VersionOfStudy1 <= 0)
                {
                    throw new InvalidDataException("Please enter Vserion Priority choice-01.");
                }
                else
                {
                    if (!vsesionIdList.Contains(teacherPublicRegistration.VersionOfStudy1))
                    {
                        throw new InvalidDataException("Please enter valid Vserion Priority choice-01.");
                    }
                }

                if (teacherPublicRegistration.VersionOfStudy2 != null)
                {
                    if (!vsesionIdList.Contains((int) teacherPublicRegistration.VersionOfStudy2))
                    {
                        throw new InvalidDataException("Please enter valid Vserion Priority choice-02.");
                    }
                }
                List<int> versionPriorityIdList = new List<int>();
                if (teacherPublicRegistration.VersionOfStudy1 != null)
                {
                    versionPriorityIdList.Add(teacherPublicRegistration.VersionOfStudy1);
                }
                if (teacherPublicRegistration.VersionOfStudy2 != null)
                {
                    versionPriorityIdList.Add((int)teacherPublicRegistration.VersionOfStudy2);
                }

                //similarilty check
                int countDuplicateVersion = versionPriorityIdList.GroupBy(x => x).Select(x => new
                {
                    Id = x,
                    Count = x.Count()
                }).Where(x => x.Count > 1).ToList().Count;
                if (countDuplicateVersion > 0)
                {
                    throw new InvalidDataException("Version Priority selection duplicate found.");
                }
                #endregion


                #region TA
                List<long> teacherActivityIdList = new List<long>();
                if (teacherPublicRegistration.TeacherActivity1 == null)
                {
                    throw new InvalidDataException("Please enter teacher Activity choice-01.");
                }
                else
                {
                    teacherActivityIdList.Add(teacherPublicRegistration.TeacherActivity1.Id);
                }

                if (teacherPublicRegistration.TeacherActivity2 == null && teacherPublicRegistration.TeacherActivity3 != null)
                {
                   
                    throw new InvalidDataException("Please enter teacher Activity choice-02 first.");
                }
                if (teacherPublicRegistration.TeacherActivity2 != null)
                {
                    teacherActivityIdList.Add(teacherPublicRegistration.TeacherActivity2.Id);
                }
                if (teacherPublicRegistration.TeacherActivity3 != null)
                {
                    teacherActivityIdList.Add(teacherPublicRegistration.TeacherActivity3.Id);
                }

                //similarilty check
                int countDuplicateActivity = teacherActivityIdList.GroupBy(x => x).Select(x => new
                {
                    Id = x,
                    Count = x.Count()
                }).Where(x => x.Count > 1).ToList().Count;
                if (countDuplicateActivity > 0)
                {
                    throw new InvalidDataException("Teacher Activity selection duplicate found.");
                }
                #endregion

                #region Subject Priority
                List<long> subjectIdList = new List<long>();
                if (teacherPublicRegistration.Subject1 == null)
                {
                    throw new InvalidDataException("Please enter Subject Priority choice-01.");
                }
                else
                {
                    subjectIdList.Add(teacherPublicRegistration.Subject1.Id);
                }

                if (teacherPublicRegistration.Subject2 == null && teacherPublicRegistration.Subject3 != null)
                {
                    throw new InvalidDataException("Please enter teacher Activity choice-02 first.");
                }
                if (teacherPublicRegistration.TeacherActivity2 != null)
                {
                    subjectIdList.Add(teacherPublicRegistration.TeacherActivity2.Id);
                }
                if (teacherPublicRegistration.Subject3 != null)
                {
                    subjectIdList.Add(teacherPublicRegistration.Subject3.Id);
                }

                //similarilty check
                int countDuplicateSubject = subjectIdList.GroupBy(x => x).Select(x => new
                {
                    Id = x,
                    Count = x.Count()
                }).Where(x => x.Count > 1).ToList().Count;
                if (countDuplicateSubject > 0)
                {
                    throw new InvalidDataException("Subject Priority selection duplicate found.");
                }
                #endregion

                if (String.IsNullOrEmpty(teacherPublicRegistration.AppointmentRemarks))
                {
                    throw new InvalidDataException("Please enter 'Why Udvash should appoint you as a teacher?'.");
                }

                #endregion

                using (transaction = Session.BeginTransaction())
                {
                    _teacherPublicRegistrationDao.SaveOrUpdate(teacherPublicRegistration);

                    SaveSmsHistory(teacherPublicRegistration, new List<string>() { "Teacher Online Registration" });
                    transaction.Commit();
                }

            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                {
                    transaction.Rollback();
                }
            }
        }

        public void DeletePublicTeacher(long id)
        {
            ITransaction transaction = null;
            try
            {
                TeacherPublicRegistration publicRegistration = _teacherPublicRegistrationDao.LoadById(id);
                if(publicRegistration == null)
                    throw new MessageExceptions.InvalidDataException("Invalid Data found.");
                if (publicRegistration.Status == TeacherPublicRegistration.EntityStatus.Delete)
                    throw new MessageExceptions.InvalidDataException("This record already removed from the list.");
                if (publicRegistration.Tpin > 0)
                    throw new MessageExceptions.InvalidDataException("It is already assigned.");
                using (transaction = Session.BeginTransaction())
                {
                    publicRegistration.Status = TeacherPublicRegistration.EntityStatus.Delete;
                    _teacherPublicRegistrationDao.Update(publicRegistration);
                    transaction.Commit();
                }
            }
            catch (MessageExceptions.InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                {
                    transaction.Rollback();
                }
            }
        }

        #endregion

        #region Single Instances Loading Function

        public TeacherPublicRegistration GetById(long Id)
        {
            try
            {
                return _teacherPublicRegistrationDao.LoadById(Id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function

        public IList<TeacherPublicRegistration> LoadRegisterTeacherList(List<UserMenu> userMenu, int start, int length, string orderBy = "NickName", string orderDirection = "ASC", List<long> organizationIdList = null, List<long> teacherActivityIdList = null, List<long> subjectIdList = null, List<long> religionIdList = null, List<long> versionOfStudies = null, List<long> genderIdList = null, int? academicStudentStatus = null, int? teacherApplicationStatus = null, string nickName = "", string institute = "", string mobileNumber = "")
        {
            try
            {
                List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organizationIdList);
                IList<TeacherPublicRegistration> teacherPublicRegistration = _teacherPublicRegistrationDao.LoadRegisterTeacherList(start, length, orderBy, orderDirection,  authoOrganizationIdList, teacherActivityIdList, subjectIdList, religionIdList, versionOfStudies, genderIdList, academicStudentStatus, teacherApplicationStatus, nickName, institute, mobileNumber);
                return teacherPublicRegistration;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public bool CheckDublicateMobileNumber(string mobileNumber)
        {
            try
            {
                return _teacherPublicRegistrationDao.CheckDublicateMobileNumber(mobileNumber);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetRegisterTeacherCount(List<UserMenu> userMenu, List<long> organizationIdList = null, List<long> teacherActivityIdList = null, List<long> subjectIdList = null, List<long> religionIdList = null, List<long> versionOfStudies = null, List<long> genderIdList = null, int? academicStudentStatus = null, int? teacherApplicationStatus = null, string nickName = "", string institute = "", string mobileNumber = "")
        {
            try
            {
                List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organizationIdList);
                int countRegisterTeacherCount = _teacherPublicRegistrationDao.GetRegisterTeacherCount(authoOrganizationIdList, teacherActivityIdList, subjectIdList, religionIdList, versionOfStudies, genderIdList, academicStudentStatus, teacherApplicationStatus, nickName, institute, mobileNumber);
                return countRegisterTeacherCount;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper Function
        private void SaveSmsHistory(TeacherPublicRegistration teacherPublicRegistration, List<string> smsTypes)
        {
            var smsSettingses = _smsSettingsDao.SmsSettingsList(smsTypes);
            if (smsSettingses.Any())
            {
                long smsUserId = 1;
                var smsSettings = smsSettingses.FirstOrDefault();
                if (smsSettings!=null)
                {
                    var smsHistoryList = new List<SmsHistory>();
                    var options = smsSettings.SmsType.DynamicOptions;
                    var message = smsSettings.Template;
                    var mask = smsSettings.MaskName;
                    var teacherMobile = teacherPublicRegistration.MobileNumber1;

                    #region Generate SMS from Template
                    if (options.Any())
                    {
                        foreach (var o in options)
                        {
                            string optionField = "[[{{" + o.Name + "}}]]";
                            string optionValue = "";

                            switch (o.Name)
                            {
                                case "Teacher Full Name":
                                    optionValue = teacherPublicRegistration.FullName;
                                    break;
                                case "Teacher Nick Name":
                                    optionValue = teacherPublicRegistration.NickName;
                                    break;
                                case "Teacher Pin":
                                    optionValue = teacherPublicRegistration.Tpin.ToString();
                                    break;
                                
                                default:
                                    optionValue = "";
                                    break;
                            }
                            message = message.Replace(optionField, optionValue);
                        }
                    }

                    #endregion

                    #region Listing Receivers

                    var smsToBeSend = new List<SendTeacherSmsViewModel>();
                    foreach (var sr in smsSettings.SmsReceivers)
                    {
                        var svm = new SendTeacherSmsViewModel();
                        switch (sr.Name)
                        {
                            case "Mobile Number (Teacher)":
                                teacherMobile = _commonHelper.CheckMobileNumber(teacherMobile);
                               // teacherMobile = CheckMobileNumber(teacherMobile);
                                if (teacherMobile != null && teacherMobile != null && teacherMobile != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = "88" + teacherMobile;
                                    svm.TeacherId = teacherPublicRegistration.Id;
                                    svm.OrganizationId = teacherPublicRegistration.Organization.Id;
                                    svm.Organization = teacherPublicRegistration.Organization;
                                }
                                break;

                            default:
                                break;
                        }

                        if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                        {
                            var alreadyExist = smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                            if (!alreadyExist.Any())
                            {
                                smsToBeSend.Add(svm);
                            }
                        }
                    }
                    #endregion

                    #region Generate Sms History List
                    if (smsToBeSend.Any())
                    {
                        if (String.IsNullOrEmpty(mask))
                        {
                            mask = "";
                        }
                        foreach (var stbs in smsToBeSend)
                        {

                            var smsHistory = new SmsHistory();
                            var currentNumber = stbs;
                            smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                            smsHistory.SmsReceiver = smsSettings.SmsReceivers.FirstOrDefault(x => x.Id == currentNumber.SmsReceiverId);
                            smsHistory.Sms = message;
                            smsHistory.Organization = stbs.Organization;
                            smsHistory.SmsSettings = smsSettings;
                            smsHistory.Mask = mask;
                            smsHistory.Type = Convert.ToInt32(smsSettings.SmsType.Id);
                            smsHistory.Status = SmsHistory.EntityStatus.Pending;
                            smsHistory.CreateBy = smsUserId;
                            smsHistory.ModifyBy = smsUserId;
                            smsHistory.Priority = 4;
                            smsHistoryList.Add(smsHistory);
                        }
                    }

                    #endregion

                    if (smsHistoryList.Any())
                    {
                        foreach (var smsHistory in smsHistoryList)
                        {
                            _hrHistoryDao.Save(smsHistory);
                        }
                    }
                }                
            }          
            //var smsHistoryList = GenerateSmsHistoryTemplate(teacherPublicRegistration, smsSettingses);
        }
        #endregion
    }
}
