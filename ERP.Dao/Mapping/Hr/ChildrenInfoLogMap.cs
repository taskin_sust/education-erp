using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class ChildrenInfoLogMap : ClassMap<ChildrenInfoLog> {

        public ChildrenInfoLogMap()
        {
			Table("HR_ChildrenInfoLog");
			LazyLoad();
            //References(x => x.TeamMember).Column("TeamMemberId");
            //References(x => x.ChildrenInfo).Column("ChildrenInfoId");
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
            Map(x => x.TeamMemberId);
            Map(x => x.ChildrenInfoId);
            Map(x => x.Status).Column("Status");
			Map(x => x.Name).Column("Name");
			Map(x => x.Dob).Column("Dob");
			Map(x => x.Gender).Column("Gender");
			Map(x => x.IsStudying).Column("IsStudying");
			Map(x => x.Remarks).Column("Remarks");
        }
    }
}
