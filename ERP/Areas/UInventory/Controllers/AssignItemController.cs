﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.UserAuth;
using Array = NHibernate.Mapping.Array;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class AssignItemController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IItemGroupService _itemGroupService;
        private readonly ItemService _itemService;
        private readonly IUserService _userService;
        private readonly IOrganizationService _organizationService;
        private List<UserMenu> authorizeMenu;
        private readonly IProgramService _programService;
        private readonly IProgramSessionItemService _programSessionItemService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly ISessionService _sessionService;

        public AssignItemController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _itemGroupService = new ItemGroupService(session);
            _userService = new UserService(session);
            _itemService = new ItemService(session);
            _organizationService = new OrganizationService(session);
            _programService = new ProgramService(session);
            _programSessionItemService = new ProgramSessionItemService(session);
            _sessionService = new SessionService(session);
            _programBranchSessionService = new ProgramBranchSessionService(session);
            authorizeMenu = new List<UserMenu>();
        }

        #endregion

        #region Create Operation

        public ActionResult Create()
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.programSessionList = new MultiSelectList(string.Empty, "Value", "Text");
                
                var loadAuthorizeOrgList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
                List<SelectListItem> listOrganization = loadAuthorizeOrgList.ToList();
                listOrganization.Insert(0,new SelectListItem(){Value = "0",Text = "All Organization"});
                ViewBag.organizationList = new SelectList(listOrganization, "Value", "Text");

                ViewBag.itemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
                ViewBag.itemTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<ItemType>(), "Key", "Value");
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(AssignItemViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (viewModel.ItemListViewModel!=null && viewModel.ItemListViewModel.Count > 0)
                    {
                        var programSessionItems = new List<ProgramSessionItem>();
                        if (viewModel.ProgramSessionId!=null && viewModel.ProgramSessionId.Any())
                        {
                            foreach (var programSessionId in viewModel.ProgramSessionId)
                            {
                                var spltValue = programSessionId.Split(new string[] { "::" },StringSplitOptions.RemoveEmptyEntries);

                                foreach (var itemViewModel in viewModel.ItemListViewModel)
                                {
                                    if (itemViewModel.CheckedValue)
                                    {
                                        var obj = new ProgramSessionItem()
                                        {
                                            Item = _itemService.LoadById(itemViewModel.ItemId),
                                            Program = _programService.GetProgram(Convert.ToInt64(spltValue[0])),
                                            Session = _sessionService.LoadById(Convert.ToInt64(spltValue[1])),
                                            Status = ProgramSessionItem.EntityStatus.Active
                                        };
                                        programSessionItems.Add(obj);
                                    }
                                    else
                                    {
                                        var checkItem = _programSessionItemService.CheckItem(itemViewModel.ItemId,Convert.ToInt64(spltValue[0]),Convert.ToInt64(spltValue[1]));
                                        if (checkItem != null)
                                        {
                                            checkItem.Status = ProgramSessionItem.EntityStatus.Delete;
                                            programSessionItems.Add(checkItem);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (var itemViewModel in viewModel.ItemListViewModel)
                            {
                                if (itemViewModel.CheckedValue)
                                {
                                    var obj = new ProgramSessionItem()
                                    {
                                        Item = _itemService.LoadById(itemViewModel.ItemId),
                                        Program = null,
                                        Session = null,
                                        Status = ProgramSessionItem.EntityStatus.Active
                                    };
                                    programSessionItems.Add(obj);
                                }
                                else
                                {
                                    var checkItem = _programSessionItemService.CheckNullableProgramSessionItem(itemViewModel.ItemId, viewModel.ItemGroupId,viewModel.ItemType);
                                    if (checkItem != null)
                                    {
                                        checkItem.Status = ProgramSessionItem.EntityStatus.Delete;
                                        programSessionItems.Add(checkItem);
                                    }
                                }
                            }
                        }
                        
                        var isAssignItem = _programSessionItemService.SaveOrUpdate(programSessionItems);
                        if (isAssignItem)
                            return Json(new { isSuccess = true, returnAssignItem = "Assign item successfully updated" });
                    }
                }
                return Json(new Response(false, "Please select item !!"));
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion
        
        #region Ajax Operation

        public ActionResult GetItemList(long organizationId, int itemType, long itemGroupId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    ViewBag.AssignItemList = _itemService.LoadAssignItemList(new string[0], itemType, itemGroupId,organizationId);
                    ViewBag.ItemList = _itemService.LoadItem(organizationId, itemGroupId, itemType, null);
                   
                    return PartialView("Partial/_ItemList");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                }
            }
            else { return Json(new Response(false, WebHelper.CommonErrorMessage)); }
        }
        
        public ActionResult GetAssignItemList(string[] programSessionIdList, long organizationId, int itemType, long itemGroupId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (programSessionIdList.Any())
                    {
                        ViewBag.AssignItemList = _itemService.LoadAssignItemList(programSessionIdList, itemType,
                            itemGroupId, organizationId);
                    }
                    ViewBag.ItemList = _itemService.LoadItem(organizationId, itemGroupId, itemType, null);

                    return PartialView("Partial/_ItemList");
                }
                catch (NullObjectException ex)
                {
                    return Json(new Response(false, ex.Message));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                }
            }
            else { return Json(new Response(false, WebHelper.CommonErrorMessage)); }
        }

        #endregion

    }
}