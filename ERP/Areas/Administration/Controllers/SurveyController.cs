﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class SurveyController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly ICommonHelper _commonHelper;
        private readonly ISurveyQuestionService _surveyQuestionService;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IUserService _userService;
        private IProgramSessionSurveyQuestionService _programSessionSurveyQuestionService;
        private List<UserMenu> _userMenu;
        public SurveyController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _surveyQuestionService = new SurveyQuestionService(session);
                _organizationService = new OrganizationService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _userService = new UserService(session);
                _programSessionSurveyQuestionService=new ProgramSessionSurveyQuestionService(session);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }
        #endregion

        #region Index/Manage Page
        public ActionResult Index()
        {
            try
            {
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Survey Question Loading Fail";
            }
            ViewBag.PageSize = Constants.PageSize;
            return View();
        }

        public JsonResult SurveyQuestionList(int draw, int start, int length, string question, string status)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                            case "1":
                                orderBy = "Question";
                                break;
                            case "2":
                                orderBy = "Rank";
                                break;
                            case "3":
                                orderBy = "Status";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }

                    int recordsTotal = _surveyQuestionService.SurveyQuestionRowCount(question, status);
                    long recordsFiltered = recordsTotal;
                    List<SurveyQuestion> surveyQuestionList = _surveyQuestionService.LoadSurveyQuestion(start, length, orderBy, orderDir.ToUpper(), question, status).ToList();

                    SurveyQuestion m = null;
                    int[] MaxMinRank = new int[2];
                    MaxMinRank[0] = 1;
                    MaxMinRank[1] = _surveyQuestionService.GetMaximumRank(m);

                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var c in surveyQuestionList)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        str.Add(c.Question);

                        string rankbtn = c.Rank.ToString();
                        if (c.Rank > MaxMinRank[0])
                        {
                            rankbtn += "<a href='#'  id='" + c.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                        }
                        if (c.Rank < MaxMinRank[1])
                        {
                            rankbtn += "<a href='#'  id='" + c.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                        }

                        str.Add(rankbtn);
                        str.Add(StatusTypeText.GetStatusText(c.Status));
                        str.Add("<a href='" + Url.Action("Details", "Survey") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp;<a href='" + Url.Action("Edit", "Survey") + "?id=" + c.Id.ToString() + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp;<a id='" + c.Id.ToString() + "' href='#' data-name='" + c.Question.ToString() + "' class='glyphicon glyphicon-trash'> </a>");
                        data.Add(str);
                        sl++;
                    }


                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return Json(HttpNotFound());
        }
        #endregion

        #region Save Survey Question and Answer Operation
        public ActionResult AddSurveyQuestion()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SaveSurveyQuestion(SurveyQuestion data)
        {
            try
            {
                if (data == null)
                    return Json(new Response(false, "Question is required"));
                if (string.IsNullOrEmpty(data.Question))
                    return Json(new Response(false, "Question is required"));
                if (!data.SurveyQuestionAnswers.Any())
                    return Json(new Response(false, "You must give at least one answer"));

                foreach (var sqa in data.SurveyQuestionAnswers)
                {
                    sqa.Status = SurveyQuestionAnswer.EntityStatus.Active;
                    sqa.SurveyQuestion = data;
                }
                _surveyQuestionService.Save(data);
                return Json(new Response(true, "Survey Save Successful"));
            }
            catch (EmptyFieldException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Survey Question Save fail"));
            }
        }
        #endregion

        #region Update Survey Question and Answer Operation
        public ActionResult Edit(long id)
        {
            try
            {
                SurveyQuestion surveyQuestiondetails = _surveyQuestionService.GetSurveyQuestion(id);
                if (surveyQuestiondetails == null)
                {
                    return HttpNotFound();
                }

                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                return View(surveyQuestiondetails);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
        }
        [HttpPost]
        public ActionResult UpdateSurveyQuestion(SurveyQuestion data)
        {
            try
            {
                if (data == null)
                    return Json(new Response(false, "Question is required"));
                if (string.IsNullOrEmpty(data.Question))
                    return Json(new Response(false, "Question is required"));
                if (!data.SurveyQuestionAnswers.Any())
                    return Json(new Response(false, "You must give at least one answer"));


                _surveyQuestionService.Update(data);
                return Json(new Response(true, "Survey Update Successful"));
            }
            catch (EmptyFieldException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (MessageException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Survey Question Save fail"));
            }
        }
        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                SurveyQuestion deleteSurveyQuestion = _surveyQuestionService.GetSurveyQuestion(id);
                deleteSurveyQuestion.Status = MenuGroup.EntityStatus.Delete;
                _surveyQuestionService.Delete(deleteSurveyQuestion);
                return Json(new Response(true, "Survey Question Delete Successful !"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Survey Question Delete Fail !"));
            }
        }
        #endregion

        #region Details Operation
        public ActionResult Details(long id)
        {
            try
            {
                SurveyQuestion surveyQuestiondetails = _surveyQuestionService.GetSurveyQuestion(id);
                if (surveyQuestiondetails == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    surveyQuestiondetails.CreateByText = _userService.GetUserNameByAspNetUserId(surveyQuestiondetails.CreateBy);
                    surveyQuestiondetails.ModifyByText = _userService.GetUserNameByAspNetUserId(surveyQuestiondetails.ModifyBy);
                }
                return View(surveyQuestiondetails);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Survey Question Loading Fail";
                return View();
            }
        }
        #endregion

        #region Rank Operation
        public ActionResult RankChange(int id, int current, string action)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    SurveyQuestion surveyQuestionUpdateObj = _surveyQuestionService.GetSurveyQuestion(Convert.ToInt64(id));
                    if (surveyQuestionUpdateObj == null)
                    {
                        return HttpNotFound();
                    }
                    int newRank;
                    if (action == "up")
                        newRank = surveyQuestionUpdateObj.Rank - 1;
                    else
                        newRank = surveyQuestionUpdateObj.Rank + 1;
                    SurveyQuestion surveyQuestionOldObj = _surveyQuestionService.LoadByRank(newRank);
                    surveyQuestionOldObj.Rank = surveyQuestionUpdateObj.Rank;
                    surveyQuestionUpdateObj.Rank = newRank;
                    _surveyQuestionService.UpdateRank(surveyQuestionOldObj, surveyQuestionUpdateObj);

                    return Json(new Response(true, "Rank Successfuly Changed"));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            return Json(new Response(false, WebHelper.CommonErrorMessage));
        }
        #endregion

        #region Survey Question Assign

        [HttpGet]
        public ActionResult SurveyQuestionAssign(string message)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.ErrorMessage = message;
                var surveyQuestionList = _surveyQuestionService.LoadSurveyQuestion();
                surveyQuestionList = surveyQuestionList.OrderBy(x => x.Rank).ToList();
                var assigedtSurveyQuestionListFromProgramSession = new List<SurveyQuestion>();
                var viewModel = new SurveyQuestionAssign
                {
                    AllSurveyQuestion = surveyQuestionList,
                    SurveyQuestions = assigedtSurveyQuestionListFromProgramSession
                };
                //ViewBag.OrganizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionList = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SurveyQuestions = surveyQuestionList;


                List<SelectListItem> surveyTypeList = new List<SelectListItem>();

                foreach (SurveyType surveyType in Enum.GetValues(typeof(SurveyType)))
                {
                    var x = (int)surveyType;
                    surveyTypeList.Add(new SelectListItem() { Text = surveyType.GetDescription(), Value = x.ToString() });

                }

                ViewBag.SurveyType = new SelectList(surveyTypeList, "Value", "Text");

                return View(viewModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                TempData["CommonErrorMessage"] = ex.Message;
                return RedirectToAction("Error", "Home", new { area = "" });
            }
        }

        [HttpPost]
        public ActionResult SurveyQuestionAssign(SurveyQuestionAssign surveyQuestionAssign, long organizationId, long programId, long sessionId)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                if (programId > 0 && sessionId > 0)
                {
                    //if (surveyQuestionAssign.SelectedSurveyQuestions != null)
                    //{
                        var message = _surveyQuestionService.SaveUpdateAssignSurveyQuestion(surveyQuestionAssign, programId, sessionId);
                        if (message != "")
                        {
                            ViewBag.InfoMessage = message;
                        }
                        ViewBag.SuccessMessage = "Survey Question successfully assigned";
                    //}
                    //else
                    //    return RedirectToAction("SurveyQuestionAssign", new { message = "You need to select Question for Question assign" });
                }
                else
                    return RedirectToAction("SurveyQuestionAssign", new { message = "You need to select program or session." });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
            }

            #region Initialization
            //ViewBag.OrganizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
            //ViewBag.ProgramList = new SelectList(_programService.LoadProgram(_commonHelper.ConvertIdToList(Convert.ToInt64(organizationId))), "Id", "Name");
            //ViewBag.SessionList = new SelectList(_sessionService.LoadSession(_commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), _commonHelper.ConvertIdToList(programId)), "Id", "Name");
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.ProgramList = new SelectList(_programService.LoadAuthorizedProgram(_userMenu,_commonHelper.ConvertIdToList(Convert.ToInt64(organizationId))), "Id", "Name");
            ViewBag.SessionList = new SelectList(_sessionService.LoadAuthorizedSession(_userMenu,_commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), _commonHelper.ConvertIdToList(programId)), "Id", "Name");
            List<SelectListItem> surveyTypeList = new List<SelectListItem>();

            foreach (SurveyType surveyType in Enum.GetValues(typeof(SurveyType)))
            {
                var x = (int)surveyType;
                surveyTypeList.Add(new SelectListItem() { Text = surveyType.GetDescription(), Value = x.ToString() });
            }

            ViewBag.SurveyType = new SelectList(surveyTypeList, "Value", "Text");
            var surveyQuestionList = _surveyQuestionService.LoadSurveyQuestion();
            ViewBag.SurveyQuestions = surveyQuestionList;
            #region Get Selected Subject
            var assigedtSurveyQuestionListFromProgramSession = new List<SurveyQuestion>();
            var alreadyAssignedSurveyQuestionList = _programSessionSurveyQuestionService.LoadProgramSessionSurveyQuestion(programId, sessionId, surveyQuestionAssign.SurveyType);
            if (alreadyAssignedSurveyQuestionList != null && alreadyAssignedSurveyQuestionList.Count > 0)
            {
                foreach (var pss in alreadyAssignedSurveyQuestionList)
                {
                    assigedtSurveyQuestionListFromProgramSession.Add(pss.SurveyQuestion);
                }
            }
            var viewModel = new SurveyQuestionAssign
            {
                AllSurveyQuestion = surveyQuestionList,
                SurveyQuestions = assigedtSurveyQuestionListFromProgramSession
            };
            #endregion

            #endregion
            return View(viewModel);
        }
        #endregion    

        #region Helper Function
        [HttpPost]
        public ActionResult AnswerList(long id)
        {
            if (id == 0)
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

            try
            {
                SurveyQuestion surveyQuestiondetails = _surveyQuestionService.GetSurveyQuestion(id);
                if (surveyQuestiondetails == null)
                {
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
                return View(surveyQuestiondetails);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public ActionResult GetSurveyQuestion(long sessionId, long programId, int surveyType)
        {
            try
            {
                var assigedtSurveyQuestionListFromProgramSession = new List<SurveyQuestion>();
                var alreadyAssignedSurveyQuestionList = _programSessionSurveyQuestionService.LoadProgramSessionSurveyQuestion(programId, sessionId, surveyType);
                var surveyQuestionList = _surveyQuestionService.LoadSurveyQuestion();
                ViewBag.Subjects = surveyQuestionList;
                if (alreadyAssignedSurveyQuestionList != null && alreadyAssignedSurveyQuestionList.Count > 0)
                {
                    foreach (var pss in alreadyAssignedSurveyQuestionList)
                    {
                        assigedtSurveyQuestionListFromProgramSession.Add(pss.SurveyQuestion);
                    }
                }
                var viewModel = new SurveyQuestionAssign();
                viewModel.AllSurveyQuestion = surveyQuestionList;
                viewModel.SurveyQuestions = assigedtSurveyQuestionListFromProgramSession;
                return View("SurveyQuestionList", viewModel);
            }
            catch (Exception ex)
            {
                TempData["CommonErrorMessage"] = ex.Message;
                _logger.Error(ex);
                return RedirectToAction("Error", "Home", new { area = "" });
            }
            //return View();
        }
        #endregion
    }
}