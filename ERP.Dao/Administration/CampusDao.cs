﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface ICampusDao : IBaseDao<Campus, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        Campus GetCampusByRankNextOrPrevious(int rank, string action);
        #endregion

        #region List Loading Function
        //IList<Campus> LoadAuthorizeCampus(List<long> authOrganizationIdList, List<long> authProgramIdList, List<long> authBranchIdList, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null);
        IList<Campus> LoadAuthorizeCampus(List<long> authProgramIdList, List<long> authBranchIdList, List<long> sessionIdList = null, bool isProgramBranSession = true);
        IList<Campus> LoadCampus(List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, List<long> branchIdList, List<long> campusIdList, bool isProgramBranSession = true);
        IList<Campus> LoadCampus(List<long> programIdList, List<long> branchIdList, int start, int length, string orderBy, string direction, string orgId, string bId, string cName, string contactNumber, string cRank, string cStatus);
        IList<Campus> LoadCampusByBranchIdsProgramSession(long[] branchId, long programId, long sessionId);
        IList<Campus> LoadCampus(long[] campusId); 
        #endregion

        #region Public API
        IList<Campus> LoadPublicCampusByProgramSessionBranch(string orgBusinessId, long programId, long sessionId, long branchId);
        #endregion

        #region Others Function
        bool HasDupilcate(Campus campusObj, bool isUpdate);

        bool HasDependency(Campus campusObj);

        int GetCampusCount(List<long> programIdList, List<long> branchIdList, string orgId, string bId, string cName, string contactNumber, string cRank, string cStatus);

        long HasManyCampusObject(long branchId);

        int GetActiveBranchCampuCount(Branch branchObj);

        #endregion
    }
    public class CampusDao : BaseDao<Campus, long>, ICampusDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public Campus GetCampusByRankNextOrPrevious(int rank, string action)
        {
            if (action == "down")
                return Session.QueryOver<Campus>().Where(x => x.Rank >= rank && x.Status != Campus.EntityStatus.Delete).OrderBy(x => x.Rank).Asc.Take(1).SingleOrDefault<Campus>();
            if (action == "up")
                return Session.QueryOver<Campus>().Where(x => x.Rank <= rank && x.Status != Campus.EntityStatus.Delete).OrderBy(x => x.Rank).Desc.Take(1).SingleOrDefault<Campus>();
            return null;
        }
        #endregion

        #region List Loading Function

        public IList<Campus> LoadCampus(List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, List<long> branchIdList, List<long> campusIdList, bool isProgramBranSession = true)
        {
            ICriteria criteria = Session.CreateCriteria<Campus>().Add(Restrictions.Eq("Status", Campus.EntityStatus.Active));



            if (branchIdList != null)
            {
                if (!branchIdList.Contains(SelectionType.SelelectAll))
                {
                    criteria.Add(Restrictions.In("Branch.Id", branchIdList));
                }
            }
            if (campusIdList != null)
            {
                if (!campusIdList.Contains(SelectionType.SelelectAll))
                {
                    criteria.Add(Restrictions.In("Id", campusIdList));
                }
            }



            if (isProgramBranSession)
            {
                DetachedCriteria programSessionBran = DetachedCriteria.For<ProgramBranchSession>()
                    .SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
                    .CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));

                if (organizationIdList != null)
                {
                    if (!organizationIdList.Contains(SelectionType.SelelectAll))
                        programSessionBran.Add(Restrictions.In("br.Organization.Id", organizationIdList));
                }

                if (programIdList != null)
                {
                    if (!programIdList.Contains(SelectionType.SelelectAll))
                    {
                        programSessionBran.CreateAlias("Program", "pr")
                            .Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
                        programSessionBran.Add(Restrictions.In("pr.Id", programIdList));
                    }
                }

                if (sessionIdList != null)
                {
                    if (!sessionIdList.Contains(SelectionType.SelelectAll))
                    {
                        programSessionBran.CreateAlias("Session", "s")
                            .Add(Restrictions.Eq("s.Status",
                                BusinessModel.Entity.Administration.Session.EntityStatus.Active));
                        programSessionBran.Add(Restrictions.In("Session.Id", sessionIdList));
                    }
                }
                criteria.Add(Subqueries.PropertyIn("Branch.Id", programSessionBran));
            }
            else
            {
                if (organizationIdList != null)
                {
                    if (!organizationIdList.Contains(SelectionType.SelelectAll))
                    {
                        criteria.CreateAlias("Branch", "branch").Add(Restrictions.Eq("branch.Status", Branch.EntityStatus.Active));
                        criteria.Add(Restrictions.In("branch.Organization.Id", organizationIdList));
                    }
                }
            }
           return criteria.List<Campus>().OrderBy(x => x.Rank).ToList();
        }

        public IList<Campus> LoadAuthorizeCampus(List<long> authProgramIdList, List<long> authBranchIdList, List<long> sessionIdList = null, bool isProgramBranSession = true)
        {
            ICriteria criteria = Session.CreateCriteria<Campus>().Add(Restrictions.Eq("Status", Campus.EntityStatus.Active));

            if (authBranchIdList != null)
            {
                criteria.Add(Restrictions.In("Branch.Id", authBranchIdList));
            }

            if (isProgramBranSession)
            {
                DetachedCriteria programSessionBran = DetachedCriteria.For<ProgramBranchSession>()
                    .SetProjection(Projections.Distinct(Projections.Property("Branch.Id")));

                programSessionBran.CreateAlias("Branch", "br")
                    .Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
                programSessionBran.CreateAlias("Program", "pr")
                    .Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
                programSessionBran.CreateAlias("Session", "s")
                    .Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));

                if (authProgramIdList != null)
                {
                    programSessionBran.Add(Restrictions.In("pr.Id", authProgramIdList));
                }
                if (sessionIdList != null)
                {
                    programSessionBran.Add(Restrictions.In("Session.Id", sessionIdList));
                }

                criteria.Add(Subqueries.PropertyIn("Branch.Id", programSessionBran));
            }

            return criteria.List<Campus>().OrderBy(x => x.Rank).ToList();
        }
        //public IList<Campus> LoadAuthorizeCampus(List<long> authOrganizationIdList, List<long> authProgramIdList, List<long> authBranchIdList, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null)
        //{
        //    ICriteria criteria = Session.CreateCriteria<Campus>().Add(Restrictions.Eq("Status", Campus.EntityStatus.Active));

        //    DetachedCriteria programSessionBran = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
        //        .CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));

        //    programSessionBran.CreateAlias("Program", "pr").Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));

        //    programSessionBran.CreateAlias("Session", "s").Add(Restrictions.Eq("s.Status", Model.Entity.Administration.Session.EntityStatus.Active));


        //    if (authOrganizationIdList != null)
        //    {
        //        programSessionBran.Add(Restrictions.In("pr.Organization.Id", authOrganizationIdList));
        //    }
        //    if (authProgramIdList != null)
        //    {
        //        programSessionBran.Add(Restrictions.In("pr.Id", authProgramIdList));
        //    }
        //    if (authBranchIdList != null)
        //    {
        //        criteria.Add(Restrictions.In("Branch.Id", authBranchIdList));
        //    }


        //    if (programIdList != null)
        //    {
        //        if (!programIdList.Contains(SelectionType.SelelectAll))
        //            programSessionBran.Add(Restrictions.In("Program.Id", programIdList));
        //    }
        //    if (branchIdList != null)
        //    {
        //        if (!branchIdList.Contains(SelectionType.SelelectAll))
        //            criteria.Add(Restrictions.In("Branch.Id", branchIdList));
        //    }
        //    if (sessionIdList != null)
        //    {
        //        if (!sessionIdList.Contains(SelectionType.SelelectAll))
        //            programSessionBran.Add(Restrictions.In("Session.Id", sessionIdList));
        //    }
        //    //criteriaForBranch.Add(Subqueries.PropertyIn("Program.Id", criteriaForProgram)); 
        //    criteria.Add(Subqueries.PropertyIn("Branch.Id", programSessionBran));

        //    return criteria.List<Campus>().OrderBy(x => x.Rank).ToList();
        //}

        public IList<Campus> LoadCampusByBranchIdsProgramSession(long[] branchId, long programId, long sessionId)
        {
            ICriteria criteria =
                    Session.CreateCriteria<Campus>().Add(Restrictions.Eq("Status", Campus.EntityStatus.Active));
            if (branchId.Contains(BusinessRules.SelectionType.SelelectAll))
            {
                DetachedCriteria programSessionBranch = DetachedCriteria.For<ProgramBranchSession>()
                .SetProjection(Projections.Distinct(Projections.Property("Branch.Id"))).CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active))
                .Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));

                criteria.Add(Subqueries.PropertyIn("Branch.Id", programSessionBranch));
            }
            else
            {
                criteria.Add(Restrictions.In("Branch.Id", branchId)).CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            }
            return criteria.List<Campus>();
        }

        

        public IList<Campus> LoadCampus(List<long> programIdList, List<long> branchIdList, int start, int length, string orderBy, string direction, string orgId, string bId, string cName, string contactNumber, string cRank, string cStatus)
        {
            ICriteria criteria = Session.CreateCriteria<Campus>().Add(Restrictions.Not(Restrictions.Eq("Status", Campus.EntityStatus.Delete)));
            criteria.CreateAlias("Branch", "br");
            
            if (branchIdList != null && branchIdList.Count>0)
            {
                criteria.Add(Restrictions.In("br.Id", branchIdList));
            }

            if (!string.IsNullOrEmpty(orgId))
            {
                criteria.Add(Restrictions.Eq("br.Organization.Id", Convert.ToInt64(orgId)));
            }
            if (!String.IsNullOrEmpty(bId))
            {
                criteria.Add(Restrictions.Eq("br.Id", Convert.ToInt64(bId.Trim())));
            }
            if (!String.IsNullOrEmpty(cName))
            {
                criteria.Add(Restrictions.InsensitiveLike("Name", cName.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(contactNumber))
            {
                criteria.Add(Restrictions.InsensitiveLike("ContactNumber", contactNumber.Trim(), MatchMode.Anywhere));
            }

            if (!String.IsNullOrEmpty(cRank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(cRank)));
            }
            if (!String.IsNullOrEmpty(cStatus))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(cStatus)));
            }
            if (!string.IsNullOrEmpty(orderBy.Trim()))
            {
                criteria.AddOrder(direction == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim())); 
            }
            return (List<Campus>)criteria.SetFirstResult(start).SetMaxResults(length).List<Campus>();
        }

        public IList<Campus> LoadCampus(long[] campusId)
        {
            ICriteria criteria = Session.CreateCriteria<Campus>().Add(Restrictions.Eq("Status", Campus.EntityStatus.Active));
            if (campusId.Any())
            {
                criteria.Add(Restrictions.In("Id", campusId));
            }
            return criteria.List<Campus>();
        }

        #endregion

        #region Public API
        public IList<Campus> LoadPublicCampusByProgramSessionBranch(string orgBusinessId, long programId, long sessionId, long branchId)
        {
            ICriteria criteria = Session.CreateCriteria<Campus>()
                .Add(Restrictions.Eq("Status", BusinessModel.Entity.Administration.Branch.EntityStatus.Active))
                .CreateAlias("Branch", "b")
                .Add(Restrictions.Eq("b.Id", branchId))
                .Add(Restrictions.Eq("b.Status", BusinessModel.Entity.Administration.Branch.EntityStatus.Active))
                .CreateAlias("b.ProgramBranchSessions", "pbs")
                .Add(Restrictions.Eq("pbs.IsPublic", true))
                .Add(Restrictions.Eq("pbs.Status", BusinessModel.Entity.Administration.ProgramBranchSession.EntityStatus.Active))
                .CreateAlias("pbs.Program", "p")
                .Add(Restrictions.Eq("p.Id", programId))
                .Add(Restrictions.Eq("p.Status", BusinessModel.Entity.Administration.Program.EntityStatus.Active))
                .CreateAlias("p.Organization", "o")
                .Add(Restrictions.Eq("o.BusinessId", orgBusinessId))
                .Add(Restrictions.Eq("o.Status", BusinessModel.Entity.Administration.Organization.EntityStatus.Active))
                .CreateAlias("pbs.Session", "s")
                .Add(Restrictions.Eq("s.Id", sessionId))
                .Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));

            return criteria.List<Campus>().OrderBy(x => x.Rank).Distinct().ToList();
        }
        #endregion

        #region Others Function
        public int GetCampusCount(List<long> programIdList, List<long> branchIdList, string orgId, string bId, string cName, string contactNumber, string cRank, string cStatus)
        {

            ICriteria criteria = Session.CreateCriteria<Campus>().Add(Restrictions.Not(Restrictions.Eq("Status", Campus.EntityStatus.Delete)));
            criteria.CreateAlias("Branch", "br");
            if (branchIdList != null && branchIdList.Count>0)
            {
                criteria.Add(Restrictions.In("br.Id", branchIdList));
            }
            if (!string.IsNullOrEmpty(orgId))
            {
                criteria.Add(Restrictions.Eq("br.Organization.Id", Convert.ToInt64(orgId)));
            }
            if (!String.IsNullOrEmpty(bId))
            {
                criteria.Add(Restrictions.Eq("br.Id", Convert.ToInt64(bId.Trim())));
            }
            if (!String.IsNullOrEmpty(cName))
            {
                criteria.Add(Restrictions.InsensitiveLike("Name", cName.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(contactNumber))
            {
                criteria.Add(Restrictions.InsensitiveLike("ContactNumber", contactNumber.Trim(), MatchMode.Anywhere));
            }

            if (!String.IsNullOrEmpty(cRank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(cRank)));
            }
            if (!String.IsNullOrEmpty(cStatus))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(cStatus)));
            }
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());

            //var query = Session.QueryOver<Campus>();

            //if (!allBranch)
            //{
            //    query.Where(x => x.Branch.Id.IsIn(branchIdList));
            //}
            //if (!string.IsNullOrEmpty(bId))
            //{
            //    query.Where(x => x.Branch.Id == Convert.ToInt64(bId.Trim()));
            //}
            //if (!string.IsNullOrEmpty(cName))
            //{
            //    query.Where(new LikeExpression("Name", cName, MatchMode.Anywhere));
            //}
            //if (!string.IsNullOrEmpty(cRank))
            //{
            //    query.Where(x => x.Rank == Convert.ToInt32(cRank.Trim()));
            //}
            //if (!string.IsNullOrEmpty(cStatus))
            //{
            //    query.Where(x => x.Status == Convert.ToInt32(cStatus.Trim()));
            //}
            //return query.Where(x => x.Status != Campus.EntityStatus.Delete).RowCount();
        }

        public bool HasDupilcate(Campus campusObj, bool isUpdate)
        {
            if (isUpdate)
            {
                var result = Session.QueryOver<Campus>().Where(
                    x => x.Branch == campusObj.Branch
                         && x.Name == campusObj.Name && x.Status != Campus.EntityStatus.Delete
                         && x.Id != campusObj.Id
                    ).SingleOrDefault<Campus>();

                if (result == null || result.Id < 1)
                    return false;
                return true;
            }
            var result2 = Session.QueryOver<Campus>().Where(
                x => x.Branch == campusObj.Branch
                     && x.Name == campusObj.Name && x.Status != Campus.EntityStatus.Delete
                ).SingleOrDefault<Campus>();

            if (result2 == null || result2.Id < 1)
                return false;
            return true;
        }

        public bool HasDependency(Campus campusObj)
        {
            int dependBatchCount = Session.QueryOver<Batch>().Where(
                x => x.Campus.Id == campusObj.Id
                     && x.Status != Campus.EntityStatus.Delete
                ).RowCount();
            if (dependBatchCount > 0)
                return true;
            return false;
        }

        public long HasManyCampusObject(long branchId)
        {
            var foundedObj = Session.QueryOver<Campus>().Where(x => x.Branch.Id == branchId && x.Status != Campus.EntityStatus.Delete).List<Campus>();
            return foundedObj.Count;
        }

        public int GetActiveBranchCampuCount(Branch branchObj)
        {
            ICriteria criteria = Session.CreateCriteria<Campus>().Add(Restrictions.Eq("Status", Campus.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Branch.Id", branchObj.Id));
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        #endregion

        #region Helper Function

        #endregion
    }
}
