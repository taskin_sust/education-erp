﻿$(document).ready(function () {

    function dataTableRender() {
        var pageSize = 10;
        if ($('#PageSize').val() != "" && /^\d+$/.test($('#PageSize').val())) {
            pageSize = parseInt($('#PageSize').val());
        } else {
            $('#PageSize').val(pageSize);
        }
        $('#DataGrid').dataTable({
            destroy: true,
            "processing": true,
            searching: false,
            serverSide: true,
            "scrollX": true,
            "bLengthChange": false,
            "iDisplayLength": pageSize,
            //stateSave: false,
            order: [[3, "asc"]],
            ajax: {
                url: '/Exam/MeritListGeneration/GenerateMeritListPost',
                type: 'POST',
                data: function (d) {
                    d.MeritListObj = $('#MeritListObj').val();
                    d.selectedGenerateBy = $('#GenerateBy').val();
                    d.selectedGender = $('#Gender').val();
                    d.selectedOrderBy = $('#OrderBy').val();
                    //d.finalMeritListFromView = $("#hiddenFinalListConvertToJson").val();
                }
            }
        });
    }

    $(document).on('change', '#SelectedBatch', function () {

        $("#SelectedExam option").remove();
        //remove all dependant dropdownlist & selection option  
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var courseId = $("#SelectedCourse").val();
        var branchId = $("#SelectedBranch").val();
        var campusId = $("#SelectedCampus").val();
        var batchDays = $("#SelectedBatchDays").val();
        var batchTime = $("#SelectedBatchTime").val();
        var batchId = $("#SelectedBatch").val();
        if (programId.length > 0 && sessionId.length > 0 && branchId.length > 0 && campusId.length > 0 && batchId.length > 0) {
            $.ajax({
                type: "post",
                url: "/Exam/MeritListGeneration/GetExam",
                cache: false,
                async: true,
                data: { programId: programId, sessionId: sessionId, courseId: courseId, branchId: branchId, campusId: campusId, batchDays: batchDays, batchTime: batchTime, batchId: batchId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.each(result.examList, function (i, v) {
                            $('#SelectedExam').append($('<option>').text(v.Text).attr('value', v.Value));
                        });

                    } else {
                        $.each(result.examList, function (i, v) {
                            $('#SelectedExam').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        // console.log("Error -----");

                    }

                }, complete: function () {
                    $.unblockUI();
                    //console.log("Complete -----");

                },
                error: function (result) {
                    $.unblockUI();
                }
            });
        }
    });
    $(document).on('click', '#deleteBtn', function () {

        bootbox.deleteConfirm("<h3>Are you sure you want to Delete <span class='confirm-message'>" + name + "</span> Merit List</h3>", function (result) {
            if (result) {
                $.ajax({
                    type: "post",
                    url: "/Exam/MeritListGeneration/Delete",
                    cache: false,
                    async: true,
                    data: { MeritListObj: $('#MeritListObj').val(), selectedGenerateBy: $('#GenerateBy').val(), selectedGender: $('#Gender').val(), selectedOrderBy: $('#OrderBy').val() },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (result) {
                        $.unblockUI();
                        if (result.IsSuccess) {
                            dataTableRender();
                        } else {
                            //$.each(result.examList, function (i, v) {
                            //    $('#SelectedExam').append($('<option>').text(v.Text).attr('value', v.Value));
                            //});
                        }
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (result) {
                        $.unblockUI();
                    }
                });
            }
        }).css({ 'margin-top': (($(window).height() / 4)) });

    });
    
    $(document).on('click', '#meritListGenerationBtn', function (event) {
        console.log("===>> Entered");
        var isSuccess = true;
        $('.selectedGeneratedBy').each(function (index, value) {
            console.log("--------------->");
            var taValue = $(this).val();
            var id = $(this).attr('id');
            validateDropDownField(id, id, "Please Select Generate.");
            showErrorMessageBelowCtrl(id, "", false);
            if (taValue=="0") {
                showErrorMessageBelowCtrl(id, "Please Select Generate.", true);
                console.log("Sub--->>>" + id);
                isSuccess = false;
                event.preventDefault();
                return false;
            }
        });
        $('.selectedGenderdBy').each(function (index, value) {
            console.log("--------------->");
            var taValue = $(this).val();
            var id = $(this).attr('id');
            validateDropDownField(id, id, "Please Select Gender.");
            showErrorMessageBelowCtrl(id, "", false);
            if (taValue=="0") {
                showErrorMessageBelowCtrl(id, "Please Select Gender.", true);
                console.log("Sub--->>>" + id);
                isSuccess = false;
                event.preventDefault();
                return false;
            }
        });
        $('.selectedorderBy').each(function (index, value) {
            console.log("--------------->");
            var taValue = $(this).val();
            var id = $(this).attr('id');
            validateDropDownField(id, id, "Please Select Order.");
            showErrorMessageBelowCtrl(id, "", false);
            if (taValue=="0") {
                showErrorMessageBelowCtrl(id, "Please Select Order.", true);
                console.log("Sub--->>>" + id);
                isSuccess = false;
                event.preventDefault();
                return false;
            }
        });
        $('.SelectedExampTypes').each(function (index, value) {
            var taValue = $(this).val();
            var id = $(this).attr('id');
            validateDropDownField(id, id, "Please Select examType.");
            showErrorMessageBelowCtrl(id, "", false);
            if (taValue == "") {
                showErrorMessageBelowCtrl(id, "Please Select exampType.", true);
                isSuccess = false;
                event.preventDefault();
                return false;
            }
        });
        //$('.SelectedAttendanceTypes').each(function (index, value) {
        //    var taValue = $(this).val();
        //    var id = $(this).attr('id');
        //    validateDropDownField(id, id, "Please Select Attendance Type.");
        //    showErrorMessageBelowCtrl(id, "", false);
        //    if (taValue == "") {
        //        showErrorMessageBelowCtrl(id, "Please Select Attendance Type.", true);
        //        isSuccess = false;
        //        event.preventDefault();
        //        return false;
        //    }
        //});
        if (isSuccess == true) {
            $("#meritListGenerationForm").submit();
            return true;
        } else {
            event.preventDefault();
            return false;
        }


    });

    $(document).on('click', '.individualMeritListDeleteBtn', function () {

        var programRoll = $(this).attr('data-id');
        bootbox.deleteConfirm("<h3>Are you sure you want to Delete <span class='confirm-message'>" + name + "</span> Merit List</h3>", function (result) {
            if (result) {
                $.ajax({
                    type: "post",
                    url: "/Exam/MeritListGeneration/DeleteIndividualMeritList",
                    cache: false,
                    async: true,
                    data: { MeritListObj: $('#MeritListObj').val(), selectedGenerateBy: $('#GenerateBy').val(), selectedGender: $('#Gender').val(), selectedOrderBy: $('#OrderBy').val(), programRoll: programRoll },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (result) {
                        $.unblockUI();
                        if (result.IsSuccess) {
                            dataTableRender();
                        } else {
                            
                        }
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (result) {
                        $.unblockUI();
                    }
                });
            }
        }).css({ 'margin-top': (($(window).height() / 4)) });

    });





    //$(document).on('click', '#excelBtn', function () {

    //    $.ajax({
    //        type: "post",
    //        url: "/Exam/MeritListGeneration/MeritListExcelGeneration",
    //        cache: false,
    //        async: true,
    //        data: { MeritListObj: $('#MeritListObj').val(), selectedGenerateBy: $('#GenerateBy').val(), selectedGender: $('#Gender').val(), selectedOrderBy: $('#OrderBy').val() },
    //        beforeSend: function () {
    //            $.blockUI({
    //                timeout: 0,
    //                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
    //            });
    //        },
    //        success: function (result) {
    //            $.unblockUI();
    //            if (result.IsSuccess) {
    //                dataTableRender();
    //            } else {
    //                //$.each(result.examList, function (i, v) {
    //                //    $('#SelectedExam').append($('<option>').text(v.Text).attr('value', v.Value));
    //                //});
    //            }
    //        }, complete: function () {
    //            $.unblockUI();
    //        },
    //        error: function (result) {
    //            $.unblockUI();
    //        }
    //    });
    //});
});