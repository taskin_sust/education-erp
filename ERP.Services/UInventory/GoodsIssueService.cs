using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Engine;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface IGoodsIssueService : IBaseService
    {

        #region Operational Function

        bool SaveOrUpdate(GoodsIssue goodsIssue);
        bool SaveOrUpdateRequisitionGoodsIssue(GoodsIssue entityModel);
        bool AuthorizeSaveOrUpdateRequisitionGoodsIssue(List<UserMenu> list, GoodsIssue entityModel);

        #endregion

        #region Single Instances Loading Function

        GoodsIssue GetGoodsIssue(long id);
        GoodsIssue LoadById(long id);
        GoodsIssue GetGoodsIssue(string goodsIssueNo);


        #endregion

        #region List Loading Function

        IList<GoodsIssueDto> LoadGoodsIssuedList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, string item = "", DateTime? issuedDate = null, string goodsIssueNo = "", int? status = null);
        IList<dynamic> LoadGoodsIssueBranchWiseReport(AuthorizeBranchNameDelegate branchNameDelegate, int start, int length, List<UserMenu> userMenus, long organizationId, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, List<string> programsession, bool isOnlyNa = false);
        IList<dynamic> LoadGoodsIssueProgramWiseReport(int start, int length, List<UserMenu> userMenus, long organizationId, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, List<string> programsession, bool isOnlyNa = false);

        #endregion

        #region Others Function

        GoodsIssue GetGoodsIssueByRequisitionSlipId(long requisitionId);
        string GetGoodsIssueNo(long branchId);
        int LoadCurrentStockQuantity(long branchId, long itemId, int? status = null);
        int GetGoodsIssueCount(List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, string item = "", DateTime? issuedDate = null, string goodsIssueNo = "", int? status = null);
        int LoadGoodsIssueBranchWiseReportCount(AuthorizeBranchNameDelegate branchNameDelegate, List<UserMenu> userMenus, long organizationId, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, List<string> programsession, bool isOnlyNa = false);
        int LoadGoodsIssueProgramWiseReportCount(List<UserMenu> userMenus, long organizationId, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, List<string> programsession, bool isOnlyNa = false);

        #endregion

        #region Helper Function

        string GetGoodsIssueNo(Branch branch);

        #endregion

    }
    public class GoodsIssueService : BaseService, IGoodsIssueService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("InventoryService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IGoodsIssueDao _goodsIssueDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        private readonly IBranchDao _branchDao;
        private readonly IItemDao _itemDao;
        private readonly ICurrentStockSummaryDao _currentStockSummaryDao;
        private readonly IRequisitionDao _requisitionDao;

        public GoodsIssueService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _goodsIssueDao = new GoodsIssueDao { Session = session };
            _programDao = new ProgramDao { Session = session };
            _sessionDao = new SessionDao { Session = session };
            _branchDao = new BranchDao { Session = session };
            _itemDao = new ItemDao { Session = session };
            _currentStockSummaryDao = new CurrentStockSummaryDao { Session = session };
            _requisitionDao = new RequisitionDao { Session = session };
        }

        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(GoodsIssue goodsIssue)
        {
            bool returnText = false;
            ITransaction trans = null;
            try
            {
                if (goodsIssue == null)
                {
                    throw new NullObjectException("Goods Issued not submitted in correct format!");
                }
                if (goodsIssue.Branch == null) throw new NullObjectException("Branch can't be null !");
                IList<GoodsIssueDetails> issueDetailse = new List<GoodsIssueDetails>();
                IList<CurrentStockSummary> summaries = new List<CurrentStockSummary>();
                foreach (var goodsIssueDetailse in goodsIssue.GoodsIssueDetails)
                {
                    goodsIssueDetailse.GoodsIssue = goodsIssue;
                    if (HttpContext.Current != null)
                    {
                        goodsIssueDetailse.CreateBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                        goodsIssueDetailse.ModifyBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                    }
                    goodsIssueDetailse.PurposeProgramId = goodsIssueDetailse.Program;
                    goodsIssueDetailse.PurposeSessionId = goodsIssueDetailse.Session;

                    //Issue details
                    goodsIssueDetailse.Status = GoodsIssue.EntityStatus.Active;
                    if (goodsIssueDetailse.Purpose == null)
                        throw new NullObjectException("Purpose can't be null!");
                    if (goodsIssueDetailse.Item == null)
                        throw new NullObjectException("Item can't be null!");
                    var currentStockCheck = LoadCurrentStockQuantity(goodsIssue.Branch.Id, goodsIssueDetailse.Item.Id);
                    if (currentStockCheck < 0 || currentStockCheck == null)
                        throw new InvalidDataException("Current Item Stock can not less then zero (0) or empty.");
                    issueDetailse.Add(goodsIssueDetailse);

                    //stock summary update 
                    var currentStockSummary = new CurrentStockSummary()
                    {
                        Item = goodsIssueDetailse.Item,
                        Program = goodsIssueDetailse.Program,
                        Session = goodsIssueDetailse.Session,
                        Branch = goodsIssue.Branch,
                        StockQuantity = goodsIssueDetailse.IssuedQuantity
                    };
                    if (goodsIssueDetailse.Item.ItemType == (int)ItemType.CommonItem)
                    {
                        currentStockSummary.Program = null;
                        currentStockSummary.Session = null;
                    }
                    summaries.Add(currentStockSummary);

                }
                using (trans = Session.BeginTransaction())
                {
                    CheckValidation(goodsIssue);
                    goodsIssue.GoodsIssueDetails = issueDetailse;
                    _goodsIssueDao.Save(goodsIssue);
                    foreach (var currentStockSummary in summaries)
                    {
                        var program = currentStockSummary.Program;
                        var session = currentStockSummary.Session;
                        long? programId = null;
                        long? sessionId = null;
                        if (program != null && session != null)
                        {
                            programId = program.Id;
                            sessionId = session.Id;
                        }
                        CurrentStockSummary prevStockSummary = _currentStockSummaryDao.GetCurrentStockSummary(currentStockSummary.Item.Id, currentStockSummary.Branch.Id,
                           programId, sessionId);
                        if (prevStockSummary != null)
                        {
                            prevStockSummary.StockQuantity -= currentStockSummary.StockQuantity;
                            _currentStockSummaryDao.Update(prevStockSummary);
                        }
                        //else
                        //    _currentStockSummaryDao.Save(currentStockSummary);
                    }

                    trans.Commit();
                    returnText = true;
                }

            }

            catch (NullObjectException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (DuplicateEntryException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                {
                    trans.Rollback();
                }
            }
            return returnText;
        }
        public bool SaveOrUpdateRequisitionGoodsIssue(GoodsIssue entityModel)
        {
            bool returnText = false;
            ITransaction trans = null;
            try
            {
                if (entityModel == null)
                {
                    throw new NullObjectException("GoodsIssue is not submitted properly. Please try again!");
                }
                if (entityModel.GoodsIssueDetails == null || !entityModel.GoodsIssueDetails.Any())
                {
                    throw new NullObjectException("GoodsIssue is not submitted properly. Please try again!");
                }
                Branch corporateBranch = entityModel.Branch.Organization.Branches.Where(x => x.Status == Branch.EntityStatus.Active).Single(x => x.IsCorporate);
                if (corporateBranch == null)
                {
                    throw new InvalidDataException("Your Organization has no Corporate Branch. Requisition Goods Issue !");
                }

                CheckValidation(entityModel);
                CheckValidationForStockAndIssuedQuantityMismatch(entityModel);
                Branch requisitionBranch = entityModel.Branch;
                entityModel.Branch = corporateBranch;
                using (trans = Session.BeginTransaction())
                {
                    IList<int> quantityList = new List<int>();
                    IList<Item> items = new List<Item>();
                    IList<Program> programs = new List<Program>();
                    IList<Session> sessions = new List<Session>();
                    var rowList = new List<RowForCurrentStockSummary>();
                    int index = 0;

                    foreach (var row in entityModel.GoodsIssueDetails.Where(row => row.IssuedQuantity > 0))
                    {
                        if (rowList.Any(x => x.Item == row.Item && x.Program == row.Program && x.Session == row.Session))
                        {
                            var rf = rowList.SingleOrDefault(x => x.Item == row.Item && x.Program == row.Program && x.Session == row.Session);
                            if (rf == null)
                            {
                                throw new InvalidDataException("One or more requisition item has null program or session!");
                            }
                            quantityList[rf.Index] = quantityList[rf.Index] + row.IssuedQuantity;
                        }
                        else
                        {
                            var r = new RowForCurrentStockSummary()
                            {
                                Index = index,
                                Item = row.Item,
                                Program = row.Program,
                                Session = row.Session
                            };
                            rowList.Add(r);
                            quantityList.Add(row.IssuedQuantity);
                            index++;
                        }
                    }
                    foreach (var rr in rowList)
                    {
                        programs.Add(rr.Program);
                        sessions.Add(rr.Session);
                        items.Add(rr.Item);
                    }
                    _goodsIssueDao.SaveOrUpdate(entityModel);
                    UpdateItemCurrentStock(programs, sessions, corporateBranch, requisitionBranch, items, quantityList);
                    _requisitionDao.SaveOrUpdate(entityModel.Requisition);
                    trans.Commit();
                    returnText = true;
                }
            }
            catch (NullObjectException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (DuplicateEntryException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                {
                    trans.Rollback();
                }
            }
            return returnText;
        }
        public bool AuthorizeSaveOrUpdateRequisitionGoodsIssue(List<UserMenu> userMenu, GoodsIssue model)
        {
            if (model == null)
            {
                throw new NullObjectException("Model not found!");
            }
            if (userMenu == null)
            {
                throw new InvalidDataException("User Menu form not found!");
            }
            bool result;
            var authBranchIds = AuthHelper.LoadBranchIdList(userMenu, null, null, _commonHelper.ConvertIdToList(model.Branch.Id));

            var programIds = model.GoodsIssueDetails.Where(x => x.Program != null).Select(x => x.Program.Id).ToList();
            var authProgramList = AuthHelper.LoadProgramIdList(userMenu, null, null, programIds);


            if (authBranchIds.Count > 0 && authBranchIds[0] == model.Branch.Id)
            {
                if (authProgramList != null && authProgramList.Count > 0)
                {
                    var inListButNotInProgramIds = authProgramList.Except(programIds).ToList();
                    if (programIds.Count > 0 && inListButNotInProgramIds.Count > 0)
                    {
                        throw new InvalidDataException("Unauthorize program selected!");
                    }
                    result = SaveOrUpdateRequisitionGoodsIssue(model);
                }
                else
                {
                    throw new InvalidDataException("Unauthorize branch selected!");
                }
            }
            else
            {
                throw new InvalidDataException("Unauthorize branch selected!");
            }
            return result;
        }
        private void CheckValidationForStockAndIssuedQuantityMismatch(GoodsIssue entityModel)
        {
            if (entityModel.GoodsIssueDetails.Where(row => row.IssuedQuantity <= 0).ToList<GoodsIssueDetails>().Any())
            {
                throw new InvalidDataException("GoodsIssue is not submitted properly. Please try again!");
            }

            var stockValueList = new List<int>();
            IList<int> quantitys = new List<int>();
            IList<Item> items = new List<Item>();

            foreach (var row in entityModel.GoodsIssueDetails.Where(row => row.IssuedQuantity > 0))
            {
                if (items.Contains(row.Item))
                {
                    var i = items.IndexOf(row.Item);
                    var q = quantitys[i] + row.IssuedQuantity;
                    quantitys[i] = q;
                }
                else
                {
                    quantitys.Add(row.IssuedQuantity);
                    items.Add(row.Item);

                    var corporateBranch = entityModel.Branch.Organization.Branches.Where(x=>x.Status == Branch.EntityStatus.Active).Single(x => x.IsCorporate);

                    int stockCount = _currentStockSummaryDao.GetCurrentStockQuantity(row.Item.Id, corporateBranch.Id, row.Program != null ? row.Program.Id : (long?)null, row.Session != null ? row.Session.Id : (long?)null);
                    stockValueList.Add(stockCount);
                }
            }
            if (quantitys.Count != items.Count)
            {
                const string errorText = "form contains invalid input!";
                throw new InvalidDataException(errorText);
            }
            for (var i = 0; i < quantitys.Count; i++)
            {
                int rq = quantitys[i];
                int sq = stockValueList[i];
                if (rq > sq)
                {
                    const string errorText = "One or more item(s) issued quantity value is higher then stock quantity! Please try with valid input!";
                    throw new InvalidDataException(errorText);
                }
            }
        }

        #endregion

        #region Single Instances Loading Function

        public GoodsIssue GetGoodsIssue(long id)
        {
            try
            {
                if (id <= 0) throw new System.IO.InvalidDataException("Invalid Goods Issue Id");
                return _goodsIssueDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public GoodsIssue LoadById(long id)
        {
            try
            {
                return _goodsIssueDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public GoodsIssue GetGoodsIssue(string goodsIssueNo)
        {
            try
            {
                if(String.IsNullOrEmpty(goodsIssueNo)) throw new System.IO.InvalidDataException("Invalid Direct Goods Issue No.");
                if(goodsIssueNo.Length > 13 || goodsIssueNo.Length < 13) throw  new System.IO.InvalidDataException("Invalid length of Direct Goods No.");
                var goodsIssueObj = _goodsIssueDao.GetGoodsIssue(goodsIssueNo.Trim());
                return goodsIssueObj;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public string GetGoodsIssueNo(long branchId)
        {
            try
            {
                return _goodsIssueDao.GetGoodsIssueNo(branchId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<dynamic> LoadGoodsIssueProgramWiseReport(int start, int length, List<UserMenu> userMenus, long organizationId, List<long> branchIdList,
            int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList,
            int reportType, DateTime dateFrom, DateTime dateTo, List<string> programsession, bool isOnlyNa = false)
        {
            try
            {
                List<long> brIds = branchIdList.ToList();
                if (brIds.Contains(SelectionType.SelelectAll))
                    brIds = null;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, programIdList, brIds);
                List<long> authProgramList = AuthHelper.LoadProgramIdList(userMenus, organizationIdList, authBranchIdList, programIdList);
                string programSessionNameStr = GetProgramSessionByAuthorized(programsession);
                var goodsIssueList = _goodsIssueDao.LoadGoodsIssueProgramWiseReport(start, length, authBranchIdList, goodsIssueType, purposeList, itemGroupIdList,
                    itemIdList, authProgramList, sessionIdList, reportType, dateFrom, dateTo, programSessionNameStr, programsession,isOnlyNa);
                return goodsIssueList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public GoodsIssue GetGoodsIssueByRequisitionSlipId(long requisitionId)
        {
            try
            {
                var entity = _goodsIssueDao.GetGoodsIssueByRequisitionSlipId(requisitionId);
                return entity ?? null;
            }
            catch (Exception)
            {

                return null;
            }
        }

        #endregion

        #region List Loading Function

        public IList<dynamic> LoadGoodsIssueBranchWiseReport(AuthorizeBranchNameDelegate branchNameDelegate, int start, int length, List<UserMenu> userMenus, long organizationId, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, List<string> programsession, bool isOnlyNa = false)
        {
            try
            {
                if (organizationId == null || organizationId <= 0)
                    throw new InvalidDataException("Invalid Organization");
                if (!branchIdList.Any())
                    throw new InvalidDataException("Invalid Branch");
                if (userMenus == null || !userMenus.Any())
                    throw new InvalidDataException("Invalid User menu");

                List<long> brIds = branchIdList.ToList();
                if (brIds.Contains(SelectionType.SelelectAll))
                    brIds = null;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, programIdList,
                    brIds);
                List<long> authProgramList = AuthHelper.LoadProgramIdList(userMenus, organizationIdList,
                    authBranchIdList, programIdList);
                string branchNameStr = GetBranchNameByAuthorizedBranch(branchNameDelegate, userMenus, organizationIdList,
                    authBranchIdList);
                var goodsIssueList = _goodsIssueDao.LoadGoodsIssueBranchWiseReport(start, length, organizationIdList, authBranchIdList, goodsIssueType, purposeList, itemGroupIdList,
                    itemIdList, authProgramList, sessionIdList, reportType, dateFrom, dateTo, branchNameStr, programsession,isOnlyNa);
                return goodsIssueList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<GoodsIssueDto> LoadGoodsIssuedList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, string item = "", DateTime? issuedDate = null, string goodsIssueNo = "", int? status = null)
        {
            try
            {
                var organizationIdList = organizationId != null ? _commonHelper.ConvertIdToList(organizationId.Value) : null;
                var branchIdList = branchId != null ? _commonHelper.ConvertIdToList(branchId.Value) : null;
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchIdList);
                var goodIssuedList = _goodsIssueDao.LoadGoodsIssue(start, length, orderBy, orderDir, authBranchIdList, item, issuedDate, goodsIssueNo, status);
                return goodIssuedList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public int LoadGoodsIssueBranchWiseReportCount(AuthorizeBranchNameDelegate branchNameDelegate, List<UserMenu> userMenus, long organizationId, List<long> branchIdList, int goodsIssueType, List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType, DateTime dateFrom, DateTime dateTo, List<string> programsession, bool isOnlyNa = false)
        {
            try
            {
                if (organizationId == null || organizationId <= 0)
                    throw new InvalidDataException("Invalid Organization");
                if (!branchIdList.Any())
                    throw new InvalidDataException("Invalid Branch");
                if (userMenus == null || !userMenus.Any())
                    throw new InvalidDataException("Invalid User menu");

                List<long> brIds = branchIdList.ToList();
                if (brIds.Contains(SelectionType.SelelectAll))
                    brIds = null;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, programIdList, brIds);
                string branchNameStr = GetBranchNameByAuthorizedBranch(branchNameDelegate, userMenus, organizationIdList, authBranchIdList);
                var count = _goodsIssueDao.LoadGoodsIssueBranchWiseReportCount(organizationIdList, authBranchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, dateFrom, dateTo, branchNameStr, programsession, isOnlyNa);
                return count;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public int LoadGoodsIssueProgramWiseReportCount(List<UserMenu> userMenus, long organizationId, List<long> branchIdList, int goodsIssueType,
            List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, int reportType,
            DateTime dateFrom, DateTime dateTo, List<string> programsession, bool isOnlyNa = false)
        {
            try
            {
                if (organizationId == null || organizationId <= 0)
                    throw new InvalidDataException("Invalid Organization");
                if (!branchIdList.Any())
                    throw new InvalidDataException("Invalid Branch");
                if (userMenus == null || !userMenus.Any())
                    throw new InvalidDataException("Invalid User menu");

                List<long> brIds = branchIdList.ToList();
                if (brIds.Contains(SelectionType.SelelectAll))
                    brIds = null;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, programIdList, brIds);
                string programSessionNameStr = GetProgramSessionByAuthorized(programsession);
                var count = _goodsIssueDao.LoadGoodsIssueProgramWiseReportCount(organizationIdList, authBranchIdList, goodsIssueType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, reportType, dateFrom, dateTo, programSessionNameStr, programsession,isOnlyNa);
                return count;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        private string GetBranchNameByAuthorizedBranch(AuthorizeBranchNameDelegate branchNameDelegate, List<UserMenu> userMenus, List<long> organizationIdList, List<long> authBranchIdList)
        {
            IList<string> branchNameList = branchNameDelegate(userMenus, organizationIdList, authBranchIdList);
            string branchNameStr = "";
            for (int i = 0; i < branchNameList.Count; i++)
            {
                branchNameStr += "[" + branchNameList[i].Trim() + "]";
                if (i < branchNameList.Count - 1)
                    branchNameStr += ",";
            }
            return branchNameStr;
        }
        private string GetProgramSessionByAuthorized(List<string> authProgramSessionList)
        {
            string programSession = "";
            int count = 0;
            if (authProgramSessionList.Contains("-101"))
            {
                programSession = "[N/A],";
                authProgramSessionList = authProgramSessionList.Skip(1).Take(authProgramSessionList.Count - 1).ToList();
                if (authProgramSessionList.Count == 0) programSession = "[N/A]";
            }
            foreach (var programsession in authProgramSessionList)
            {
                string[] programSessionArray = programsession.Split(new string[] { "::" }, StringSplitOptions.None);
                Program program = _programDao.LoadById(Convert.ToInt64(programSessionArray[0]));
                Session session = _sessionDao.LoadById(Convert.ToInt64(programSessionArray[1]));

                string programName = program.ShortName;
                string sessionName = session.Name;
                string programSessionName = "[" + programName + " & " + sessionName + "]";
                if (count < authProgramSessionList.Distinct().ToList().Count - 1)
                    programSessionName += ",";
                programSession += programSessionName;
                count++;
            }
            return programSession;
        }

        #endregion

        #region Others Function

        public int LoadCurrentStockQuantity(long branchId, long itemId, int? stockQuantityStaus = null)
        {
            try
            {
                int count = _goodsIssueDao.GetCurrentStockQuantity(branchId, itemId, stockQuantityStaus);
                return count;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public int GetGoodsIssueCount(List<UserMenu> userMenu, long? organizationId = null, long? branchId = null, string item = "", DateTime? issuedDate = null, string goodsIssueNo = "", int? status = null)
        {
            try
            {
                if (userMenu == null) throw new InvalidDataException("invalid user permission");
                var organizationIdList = organizationId != null ? _commonHelper.ConvertIdToList(organizationId.Value) : null;
                var branchIdList = branchId != null ? _commonHelper.ConvertIdToList(branchId.Value) : null;
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchIdList);
                int count = _goodsIssueDao.GetGoodsIssueCount(authBranchIdList, item, issuedDate, goodsIssueNo, status);
                return count;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        private static void CheckValidation(GoodsIssue goodsIssue)
        {
            var validationResult = ValidationHelper.ValidateEntity<GoodsIssue, GoodsIssue>(goodsIssue);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }
        void UpdateItemCurrentStock(IList<Program> programs, IList<Session> sessions, Branch corporateBranch, Branch tobranch, IList<Item> items, IList<int> quantityList)
        {
            for (int i = 0; i < items.Count; i++)
            {
                var fromEntity = Session.QueryOver<CurrentStockSummary>()
                                    .Where(x => x.Branch.Id == corporateBranch.Id)
                                    .Where(x => x.Item == items[i]).Where(x => x.Program == programs[i])
                                    .Where(x => x.Session == sessions[i]).List<CurrentStockSummary>();
                fromEntity[0].StockQuantity = fromEntity[0].StockQuantity - quantityList[i];
                _currentStockSummaryDao.Update(fromEntity[0]);

                var toEntity = Session.QueryOver<CurrentStockSummary>()
                                    .Where(x => x.Branch.Id == tobranch.Id)
                                    .Where(x => x.Item == items[i])
                                    .Where(x => x.Program == programs[i])
                                    .Where(x => x.Session == sessions[i])
                                    .SingleOrDefault<CurrentStockSummary>();
                if (toEntity == null)
                {
                    var newEntity = new CurrentStockSummary()
                    {
                        Session = sessions[i] ?? null,
                        Program = programs[i] ?? null,
                        Branch = tobranch,
                        StockQuantity = quantityList[i],
                        Item = items[i],
                        Status = CurrentStockSummary.EntityStatus.Active,
                        ModifyBy = 0,
                        CreateBy = 0,
                    };
                    _currentStockSummaryDao.SaveOrUpdate(newEntity);
                }
                else
                {
                    toEntity.StockQuantity = toEntity.StockQuantity + quantityList[i];
                    _currentStockSummaryDao.SaveOrUpdate(toEntity);
                }
            }
        }
        public string GetGoodsIssueNo(Branch branch)
        {
            string name = "";
            try
            {
                var organizationName = branch.Organization.ShortName.Substring(0, 3);
                var branchCode = branch.Code.ToString(CultureInfo.CurrentCulture);

                var lastGoodsIssueNo = GetGoodsIssueNo(branch.Id);
                string goodsIssueNo = "";

                if (!string.IsNullOrWhiteSpace(lastGoodsIssueNo))
                {
                    string newSl = (Convert.ToInt32(lastGoodsIssueNo.Substring(lastGoodsIssueNo.Length - 6, 6)) + 1).ToString(CultureInfo.CurrentCulture);
                    int lengthDiff = 6 - newSl.Length;
                    for (int i = 0; i < lengthDiff; i++)
                    {
                        newSl = "0" + newSl;
                    }
                    goodsIssueNo = newSl;
                }
                else
                {
                    goodsIssueNo = "000001";
                }
                name = organizationName + branchCode + "GI" + goodsIssueNo;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return name;
        }

        #endregion

    }
}

