﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.HolidayWorkTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "HolidayWork")]
    public class HolidayWorkSaveTest : HolidayWorkBaseTest
    {
        #region Basic Test

        [Fact]
        public void HolidayWork_Should_Save_Sussccessful_By_Mentor()
        {
            GetTeamMember();
            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);
            
            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int) DateTime.Now.DayOfWeek - 1);
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            
            Assert.True(_holidayWorkService.SaveHolidayWorkApproval(new List<HolidayWork>() { _holidayWorkFactory.Object }, mentor, true, false, userMenu));
        }

        [Fact]
        public void HolidayWork_Should_Save_Sussccessful_By_Hr()
        {
            GetTeamMember();
            var employment = teamMemberFactory.Object.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { organizationFactory.Object }, null, new List<Branch>() { employment.Campus.Branch });

            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);
            
            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(new List<HolidayWork>() {_holidayWorkFactory.Object}, mentor,
                false, true, userMenu);
            Assert.True(true);
        }

        #endregion

        #region Business logic test

        [Fact]
        public void InvalidDataException_For_HolidayWork_Object_Null()
        {
            Assert.Throws<InvalidDataException>(() => _holidayWorkService.SaveHolidayWorkApproval(null, null, false, true));
        }
        [Fact]
        public void InvalidDataException_For_HolidayWork_Object_List_Empty()
        {
            Assert.Throws<InvalidDataException>(() => _holidayWorkService.SaveHolidayWorkApproval(new List<HolidayWork>(), null, false, true));
        }
        
        [Fact]
        public void InvalidDataException_For_HolidayWork_UserMenu_Empty()
        {
            GetTeamMember();
            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            Assert.Throws<InvalidDataException>(() => _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, null, false, true));
        }
        
        [Fact]
        public void InvalidDataException_For_HolidayWork_Unauthorized_TeamMember()
        {
            GetTeamMember();
            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().TeamMember;
            Assert.Throws<InvalidDataException>(() => _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu));
        }
        
        [Fact]
        public void InvalidDataException_For_HolidayWork_Reason_Empty()
        {
            GetTeamMember();
            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _holidayWorkFactory.Object.Reason = string.Empty;
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            Assert.Throws<InvalidDataException>(() => _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu));
        }

        
        //[Fact(Skip = "Service Not Updated")]
        //public void InvalidDataException_For_HolidayWork_WeekDay_Invalid()
        //{
        //    GetTeamMember();
        //    _holidayWorkFactory.Create().WithTeamMember(_teamMemberFactory.Object);
        //    _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 0);
        //    var userMenu = BuildUserMenu(_organizationFactory.SingleObjectList);
        //    var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
        //    Assert.Throws<InvalidDataException>(() => _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu));
        //}

        #endregion
    }
}
