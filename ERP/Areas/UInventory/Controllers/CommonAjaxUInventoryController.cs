﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class CommonAjaxUInventoryController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly IProgramSessionItemService _itemProgramSessionService;
        private readonly IBranchService _branchService;
        private readonly IItemService _itemService;
        private List<UserMenu> authorizeMenu;
        private readonly IOrganizationService _organizationService;
        private readonly ICurrentStockSummaryService _currentStockSummaryService;
        private IProgramService _programService;

        public CommonAjaxUInventoryController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _programBranchSessionService = new ProgramBranchSessionService(session);
            _itemProgramSessionService = new ProgramSessionItemService(session);
            _branchService = new BranchService(session);
            _itemService = new ItemService(session);
            authorizeMenu = new List<UserMenu>();
            _organizationService = new OrganizationService(session);
            _currentStockSummaryService = new CurrentStockSummaryService(session);
        }

        #endregion

        public ActionResult GetProgramAndSession(long organizationId, List<long> branchIdList, List<long> itemIdList = null, 
            int purposeId = 0, bool isReport = true, bool? withOutCurrentProgramSession = false)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    List<ProgramSessionDto> programSessionList = new List<ProgramSessionDto>();
                    authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    if (!isReport)
                    {
                        if (itemIdList != null)
                        {
                            var itemType = _itemService.LoadById(itemIdList.FirstOrDefault()).ItemType;
                            if (itemType == (int)ItemType.CommonItem && purposeId == SelectionType.NotApplicable)
                                return Json(new { programSessionList = (MultiSelectList)null, isSuccess = true });

                            if ((itemType == (int)ItemType.CommonItem && (purposeId > 0 && purposeId <= 3)) || itemType == (int)ItemType.ProgramItem)
                            {
                                programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, itemIdList).ToList();
                            }
                            else if (itemType == (int) ItemType.CommonItem && (purposeId > 3 && purposeId <= 5))
                            {
                                programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, itemIdList).ToList();
                                var lists = new SelectList(programSessionList, "ProgramAndSessionId", "ProgramAndSessionName").ToList();
                                lists.Insert(0, new SelectListItem() { Value = SelectionType.NotApplicable.ToString(), Text = "N/A" });
                                return Json(new { programSessionList = lists, isSuccess = true });
                            }
                            //else
                            //{
                            //    programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, itemIdList).ToList();
                            //}
                        }
                        else
                        {
                            programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, null, withOutCurrentProgramSession).ToList();
                        }

                    }
                    else
                    {
                        programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, itemIdList).ToList();
                    }

                    var list = new MultiSelectList(programSessionList, "ProgramAndSessionId", "ProgramAndSessionName");
                    return Json(new { programSessionList = list, isSuccess = true });



                    #region old code
                    //List<ProgramSessionDto> programSessionList = new List<ProgramSessionDto>();
                    //if (!isReport)
                    //{
                    //    if (itemIdList != null)
                    //    {
                    //        var itemType = _itemService.LoadById(itemIdList.FirstOrDefault()).ItemType;
                    //        if (itemType == (int)ItemType.CommonItem)
                    //            return Json(new { programSessionList = (MultiSelectList)null, isSuccess = true });

                    //        authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    //        programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, itemIdList).ToList();
                    //    }
                    //    else
                    //    {
                    //        authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    //        programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, null, withOutCurrentProgramSession).ToList();
                    //    }

                    //}
                    //else
                    //{
                    //    authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    //    programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIdList, itemIdList).ToList();
                    //}

                    //var list = new MultiSelectList(programSessionList, "ProgramAndSessionId", "ProgramAndSessionName");
                    //return Json(new { programSessionList = list, isSuccess = true });
                    #endregion


                }
                catch (InvalidDataException ex)
                {
                    return Json(new Response(false, ex.Message));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        public ActionResult GetProgramAndSessionWithBranch(long organizationId, List<long> branchIds, List<long> itemIdList = null)
        {
            if (Request.IsAjaxRequest())
            {
                var selectListItems = new List<SelectListItem>();
                try
                {
                    authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var programSessionList = GenerateAppropriateProgramSession(organizationId, branchIds, itemIdList);
                    return Json(new { programSessionList = programSessionList, isSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                }
            }
            return HttpNotFound();
        }

        private List<SelectListItem> GenerateAppropriateProgramSession(long organizationId, List<long> branchIds, List<long> itemIdList)
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            var selectListItems = new List<SelectListItem>();
            var programSessionList = _programBranchSessionService.LoadProgramSession(authorizeMenu, organizationId, branchIds, itemIdList).ToList();
            IList<Item> items = _itemService.LoadItemList(itemIdList.ToArray());
            //selectionItemype=1 programtype
            //selectionItemype=2 commontype
            //selectionItemype =3 both

            int selectionItemype = 0;
            for (int index = 0; index < items.Count; index++)
            {
                var item = items[index];
                if (item.ItemType == (int)ItemType.CommonItem)
                {
                    if (selectionItemype == 1)
                    {
                        selectionItemype = 3;
                        break;
                    }
                    selectionItemype = 2;
                }
                else if (item.ItemType == (int)ItemType.ProgramItem)
                {
                    if (selectionItemype == 2)
                    {
                        selectionItemype = 3;
                        break;
                    }
                    selectionItemype = 1;
                }
            }
            if (selectionItemype == 1)
                selectListItems = programSessionList.Select(programBranchSession => new SelectListItem()
                {
                    Text = programBranchSession.ProgramName + " " + programBranchSession.SessionName,
                    Value = programBranchSession.ProgramId + "::" + programBranchSession.SessionId
                }).ToList();
            //0 indicates all
            //-101 indicate only N/A
            else if (selectionItemype == 2)
                selectListItems.Add(new SelectListItem() { Text = "N/A", Value = "-101" });
            else
            {
                selectListItems = programSessionList.Select(programBranchSession => new SelectListItem()
                {
                    Text = programBranchSession.ProgramName + " " + programBranchSession.SessionName,
                    Value = programBranchSession.ProgramId + "::" + programBranchSession.SessionId
                }).ToList();
                selectListItems.Insert(0, new SelectListItem() { Text = "N/A", Value = "-101" });
            }
            return selectListItems;
        }

        [HttpPost]
        public ActionResult GetItemProgramAndSession(long itemId, long? branchId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var item = _itemService.LoadById(itemId);
                    if (item != null)
                    {
                        var itemProgramSessionList = _itemProgramSessionService.LoadProgramSessionItemDtoByItemId(new long[] { item.Id });
                        var list = new MultiSelectList(itemProgramSessionList, "ProgramSessionAndItemId", "ProgramAndSessionName");
                        if (item.ItemType == (int)ItemType.CommonItem)
                        {
                            int stockQ = _currentStockSummaryService.GetCurrentStockQuantity(userMenu, itemId, (long)branchId, null, null);
                            return Json(new { programSessionList = list, stockQuantity = stockQ, isSuccess = true });
                        }

                        return Json(new { programSessionList = list, isSuccess = true });
                    }
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                }
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult GetItemListProgramAndSession(long organizationId, long[] itemIdList)
        {
            if (!Request.IsAjaxRequest())
                return Json(new Response(false, "Invalid Ajax Request !"));
            if (Request.IsAjaxRequest())
            {
                var selectListItems = new List<SelectListItem>();
                try
                {
                    authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var programSessionList = GenerateAppropriateProgramSession(organizationId, itemIdList);
                    return Json(new { programSessionList = programSessionList, isSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                }
            }
            return HttpNotFound();
        }

        private List<SelectListItem> GenerateAppropriateProgramSession(long organizationId, long[] itemIdList)
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            var selectListItems = new List<SelectListItem>();
            var programSessionList = _programBranchSessionService.LoadProgramSessionForReport(authorizeMenu, organizationId, null, itemIdList.ToList()).ToList();
            IList<Item> items = _itemService.LoadItemList(itemIdList.ToArray());

            int selectionItemype = 0;
            for (int index = 0; index < items.Count; index++)
            {
                var item = items[index];
                if (item.ItemType == (int)ItemType.CommonItem)
                {
                    if (selectionItemype == 1)
                    {
                        selectionItemype = 3;
                        break;
                    }
                    selectionItemype = 2;
                }
                else if (item.ItemType == (int)ItemType.ProgramItem)
                {
                    if (selectionItemype == 2)
                    {
                        selectionItemype = 3;
                        break;
                    }
                    selectionItemype = 1;
                }
            }
            if (selectionItemype == 1)
                selectListItems = programSessionList.Select(programBranchSession => new SelectListItem()
                {
                    Text = programBranchSession.ProgramName + " " + programBranchSession.SessionName,
                    Value = programBranchSession.ProgramId + "::" + programBranchSession.SessionId
                }).ToList();

            else if (selectionItemype == 2)
                selectListItems.Add(new SelectListItem() { Text = "N/A", Value = "-101" });
            else
            {
                selectListItems = programSessionList.Select(programBranchSession => new SelectListItem()
                {
                    Text = programBranchSession.ProgramName + " " + programBranchSession.SessionName,
                    Value = programBranchSession.ProgramId + "::" + programBranchSession.SessionId
                }).ToList();
                selectListItems.Insert(0, new SelectListItem() { Text = "N/A", Value = "-101" });
            }
            return selectListItems;
        }

        //[HttpPost]
        //public ActionResult LoadItems(long itemGroupId, long? organizationId)
        //{
        //    try
        //    {
        //        if (!Request.IsAjaxRequest())
        //            return Json(new Response(false, "Invalid Ajax Request !"));
        //        if (itemGroupId <= 0)
        //            return Json(new Response(false, "Invalid ItemGroup !"));
        //        var itemSelectList = new SelectList(_itemService.LoadItem(organizationId, itemGroupId), "Id", "Name");
        //        return Json(new { returnResult = itemSelectList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        return Json(new { returnResult = new List<SelectListItem>(), IsSuccess = false });
        //    }
        //}

        [HttpPost]
        public ActionResult LoadItem(long? organizationId, long[] itemGroupIds = null)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (!Request.IsAjaxRequest())
                    return Json(new Response(false, "Invalid Ajax Request !"));
                if (itemGroupIds == null)
                    return Json(new Response(false, "Invalid ItemGroup !"));
                if (itemGroupIds.Contains(0)) itemGroupIds = null;
                var itemList = _itemService.LoadItemForOrganizationOrGroup(userMenu, organizationId, itemGroupIds);
                var itemSelectList = new SelectList(itemList.ToList(), "Id", "Name");
                return Json(new { returnResult = itemSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { returnResult = new List<SelectListItem>(), IsSuccess = false });
            }
        }

        //[HttpPost]
        //public JsonResult LoadItemForOrganizationOrGroup(long? organizationId, long[] itemGroupIds = null)
        //{
        //    try
        //    {
        //        if (itemGroupIds == null)
        //            return Json(new Response(false, "Invalid ItemGroup !"));
        //        if (itemGroupIds.Contains(0)) itemGroupIds = null;
        //        var itemList = _itemService.LoadItemForOrganizationOrGroup(organizationId, itemGroupIds);
        //        var itemSelectList = new SelectList(itemList.ToList(), "Id", "Name");
        //        return Json(new { returnItemList = itemSelectList, IsSuccess = true });
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}

        [HttpPost]
        public ActionResult LoadItemList(long[] itemGroupIds, long? organizationId)
        {
            try
            {
                if (!Request.IsAjaxRequest())
                    return Json(new Response(false, "Invalid Ajax Request !"));

                var itemList = _itemService.LoadItem(itemGroupIds, organizationId);
                var itemSelectList = new SelectList(itemList, "Id", "Name");
                return Json(new { returnResult = itemSelectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { returnResult = new List<SelectListItem>(), IsSuccess = false });
            }
        }

        [HttpPost]
        public JsonResult CurrentStockQuantityCheck(long branchId, long itemId, string programSession)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                long? programId;
                long? sessionId;

                if (String.IsNullOrEmpty(programSession) || programSession.Equals("0"))
                {
                    programId = null;
                    sessionId = null;
                }
                else
                {
                    var spltValue = programSession.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                    programId = Convert.ToInt64(spltValue[0]);
                    sessionId = Convert.ToInt64(spltValue[1]);
                }
                var currentStockAmount = _currentStockSummaryService.GetCurrentStockQuantity(userMenu, itemId, branchId, programId, sessionId);
                return Json(new { stockAmount = currentStockAmount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }


        }

    }
}