﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentBoardMap:BaseClassMap<StudentBoard,long>
    {
        public StudentBoardMap()
        {
            Table("StudentBoard");
            LazyLoad();
            Map(x => x.Name);

        }
    }
}
