using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Mapping.UserAuth {


    public partial class MenuMap : BaseClassMap<Menu, long>
    {
        
        public MenuMap() {
			Table("Menu");
			LazyLoad();
            //Not.LazyLoad();
            Map(x => x.Rank);
            Map(x => x.Name);
            Map(x => x.VariableNames);

            References(x => x.MenuSelfMenu).Column("ParentId").LazyLoad();
            References(x => x.MenuGroup).Column("MenuGroupId").LazyLoad();
            References(x => x.DefaultAction).Column("ActionId").LazyLoad();

            HasMany(x => x.Menus).KeyColumn("ParentId").Cascade.AllDeleteOrphan().Inverse().Where(ss => ss.Status != Menu.EntityStatus.Delete).OrderBy("Rank").LazyLoad();
            HasMany(x => x.UserMenus).KeyColumn("MenuId").Cascade.AllDeleteOrphan().Inverse().Where(ss => ss.Status != Menu.EntityStatus.Delete).LazyLoad();
            HasMany(x => x.MenuAccess).KeyColumn("MenuId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
        }
    }
}
