﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.Administration.BankBranchTest
{
    public interface IBankBranchLoadByIdTest
    {
        [Fact]
        void LoadById_Null_Check();
        [Fact]
        void LoadById_NotNull_Check();
    }
    [Trait("Area", "Administration")]
    [Trait("Service", "BankBranch")]
    public class BankBranchLoadByIdTest : BankBranchBaseTest, IBankBranchLoadByIdTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _bankBranchService.GetBankBranch(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            BankBranchFactory bankBranchFactory =
                _bankBranchFactory.Create().WithBank().Persist();
            var obj = _bankBranchService.GetBankBranch(bankBranchFactory.Object.Id);
            Assert.NotNull(obj);

        }
    }
}
