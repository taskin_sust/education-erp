using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public partial class StudentProgramImageMap : BaseClassMap<StudentProgramImage, long>
    {

        public StudentProgramImageMap()
        {
            Table("StudentProgramImage");
            LazyLoad();
            //Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Hash); 
            //Map(x => x.ImageIGuid);
            //Map(x => x.Images).Column("Images").CustomSqlType("VARBINARY (MAX) FILESTREAM").Length(2147483647).LazyLoad();
           // References(x => x.Student).Column("StudentId");
            References(x => x.StudentProgram).Column("StudentProgramId");

            //HasMany(x => x.StudentPrograms).KeyColumn("ImageId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
