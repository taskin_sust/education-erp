﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Exceptions;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface IHolidayWorkService : IBaseService
    {
        #region Operational Function
        
        bool SaveHolidayWorkApproval(List<HolidayWork> holidayWork, TeamMember currentTeamMember, bool isMentor = false, bool isHr = false, List<UserMenu> userMenus = null);

        #endregion

        #region Single Instances Loading Function

        HolidayWork GetById(long id);

        #endregion

        #region List Loading Function

        IList<HolidayApprovalDto> LoadMentorHolidayWorkApproval(TeamMember mentor, DateTime fromDate, DateTime toDate, string pinList = "");
        IList<HolidayApprovalDto> LoadHrHolidayWorkApproval(List<UserMenu> userMenu, DateTime fromDate, DateTime toDate, string pinList = "", List<long> organizationIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null);
        IList<HolidayApprovalDto> LoadHrHolidayWorkApprovalHistory(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, DateTime searchingDateTimeFrom, DateTime searchingDateTimeTo, string pin, long? organizationId, long? branchId, long? campusId, long? departmentId);

        #endregion

        #region Others Function

        int GetHrHolidayWorkApprovalHistoryRowCount(List<UserMenu> userMenu, DateTime searchingDateTimeFrom, DateTime searchingDateTimeTo, string pin, long? organizationId, long? branchId, long? campusId, long? departmentId);
        DateTime? GetTeamMemberHoliDayWorkIsPostDateTime(long? teamMemberId = null, int? pin = null); 

        #endregion

        #region Helper Function
        #endregion

    }
    public class HolidayWorkService : BaseService, IHolidayWorkService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IHolidayWorkDao _holidayWorkDao;
        private readonly ITeamMemberDao _teamMemberDao;
        private readonly IMentorHistoryDao _mentorHistoryDao;
        private readonly IDayOffAdjustmentDao _dayOffAdjustmentDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;

        public HolidayWorkService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _holidayWorkDao = new HolidayWorkDao { Session = session };
            _teamMemberDao = new TeamMemberDao { Session = session };
            _mentorHistoryDao = new MentorHistoryDao { Session = session };
            _dayOffAdjustmentDao = new DayOffAdjustmentDao { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao(){Session = session};
        }

        #endregion

        #region Operational Functions

        public bool SaveHolidayWorkApproval(List<HolidayWork> holidayWorkList, TeamMember currentTeamMember, bool isMentor = false, bool isHr = false, List<UserMenu> userMenus = null)
        {
            ITransaction trans = null;
            try
            {
                if (holidayWorkList == null || !holidayWorkList.Any())
                    throw new InvalidDataException("There is no Holiday Work to Approved!");

                var dublicate = holidayWorkList.GroupBy(x => new { x.TeamMember.Pin, x.HolidayWorkDate.Value.Date })
                       .Select(x => new { teammemberPin = x.Key.Pin, workDate = x.Key.Date, count = x.Count() })
                       .Where(x => x.count > 1)
                       .ToList();
                if (dublicate.Any())
                    throw new InvalidDataException("Duplicate entry for TeamMember with pin (" + dublicate[0].teammemberPin + ")  And Holiday Work Date (" + dublicate[0].workDate.ToString("yyyy-MM-dd") + ").");
                
                List<long> authorizedMemberIdList = new List<long>();
                if (isHr)
                {
                    if (userMenus == null || !userMenus.Any())
                        //throw new InvalidDataException("There is no User Menu for Hr Work to Approved!");
                        throw new InvalidDataException("Your are not authorized to access this Menu!");

                    List<long> authorizedOrganizationList = AuthHelper.LoadOrganizationIdList(userMenus);
                    List<long> authorizedBranchList = AuthHelper.LoadBranchIdList(userMenus, authorizedOrganizationList);
                    authorizedMemberIdList = _teamMemberDao.LoadHrAuthorizedTeamMemberId(null, authorizedBranchList);
                }

                if (isMentor)
                {
                    authorizedMemberIdList = _mentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(currentTeamMember.Id),DateTime.Today.Date);
                }

                List<HolidayWork> validHolidayWorkList = new List<HolidayWork>();
                foreach (HolidayWork holidayWork in holidayWorkList)
                {
                    validHolidayWorkList.Add(CheckValidHolidayWork(holidayWork, authorizedMemberIdList));
                }

                using (trans = Session.BeginTransaction())
                {
                    foreach (var holidayWork in validHolidayWorkList)
                    {
                        _holidayWorkDao.SaveOrUpdate(holidayWork);
                        SaveHolidayWorkApprovalLog(holidayWork);
                    }
                    trans.Commit();
                    return true;
                }

            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }
        
        #endregion

        #region Single Instances Loading Function

        public HolidayWork GetById(long id)
        {
            try
            {
                return _holidayWorkDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region List Loading Function

        public IList<HolidayApprovalDto> LoadMentorHolidayWorkApproval(TeamMember mentor, DateTime holidayWorkFromDate, DateTime holidayWorkToDate, string pinList = "")
        {
            try
            {
                

                List<int> memberPinList = new List<int>();
                if (!String.IsNullOrEmpty(pinList))
                {
                    string[] pinArray = pinList.Split(',');
                    memberPinList.AddRange(pinArray.Select(pin => Convert.ToInt32(pin)));
                }
                else
                {
                    memberPinList.Add(Convert.ToInt32(SelectionType.SelelectAll));
                }
                
                var mentorTeamMemberIdList = _mentorHistoryDao.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now.Date, null, memberPinList);
                if (!mentorTeamMemberIdList.Any())
                {
                    throw new InvalidDataException("You have no Team Member to Mentor.");
                    //mentorTeamMemberIdList.Add(0);
                }
                if (holidayWorkToDate < holidayWorkFromDate)
                    throw new InvalidDataException("Invalid Date Range.");
                return _holidayWorkDao.LoadHolidayWorkApproval(holidayWorkFromDate, holidayWorkToDate, mentorTeamMemberIdList, null, null, null, null, null, true);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<HolidayApprovalDto> LoadHrHolidayWorkApproval(List<UserMenu> userMenu, DateTime holidayWorkFromDate, DateTime holidayWorkToDate, string pinList, List<long> organizationIdList = null, List<long> branchIdList = null, List<long> campusIdList = null, List<long> departmentIdList = null, List<long> designationIdList = null)
        {
            try
            {
                List<int> memberPinList = new List<int>();
                if (!String.IsNullOrEmpty(pinList))
                {
                    string[] pinArray = pinList.Split(',');
                    memberPinList.AddRange(pinArray.Select(pin => Convert.ToInt32(pin)));
                }
                else
                {
                    memberPinList.Add(Convert.ToInt32(SelectionType.SelelectAll));
                }

                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organizationIdList);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIdList, null, (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll) ? branchIdList : null));

                if (!authBranchIdList.Any())
                {
                    throw new InvalidDataException("You have no authorized Branch.");
                }
                if (holidayWorkToDate < holidayWorkFromDate)
                    throw new InvalidDataException("Invalid Date Range.");
                return _holidayWorkDao.LoadHolidayWorkApproval(holidayWorkFromDate, holidayWorkToDate, null, memberPinList, authBranchIdList, campusIdList, departmentIdList, designationIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<HolidayApprovalDto> LoadHrHolidayWorkApprovalHistory(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, DateTime holidayWorkDateFrom, DateTime holidayWorkDateTo, string pin, long? organizationId, long? branchId, long? campusId, long? departmentId)
        {
            try
            {
                var organizationIdList = (organizationId != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)) : null;
                var branchIdIdList = (branchId != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(branchId)) : null;
                var departmentIdList = (departmentId != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(departmentId)) : null;
                var campusIdList = (campusId != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(campusId)) : null;
                var convertedPin = (pin != "") ? Convert.ToInt32(pin) : 0;
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchIdIdList);
                if (!authBranchIdList.Any())
                {
                    throw new InvalidDataException("You have no authorized Branch.");
                }
                if(holidayWorkDateFrom > holidayWorkDateTo)
                    throw new InvalidDataException("Invalid Date Range.");
                return _holidayWorkDao.LoadHrHolidayWorkApprovalHistory(start, length, orderBy, orderDir, holidayWorkDateFrom, holidayWorkDateTo, null, (convertedPin != 0) ? _commonHelper.ConvertIdToList(convertedPin) : null, authBranchIdList, campusIdList, departmentIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        
        #endregion

        #region Others Function

        public int GetHrHolidayWorkApprovalHistoryRowCount(List<UserMenu> userMenu, DateTime dateTimeFrom, DateTime dateTimeTo, string pin, long? organizationId, long? branchId, long? campusId, long? departmentId)
        {
            try
            {
                var organizationIdList = (organizationId != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)) : null;
                var branchIdIdList = (branchId != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(branchId)) : null;
                var departmentIdList = (departmentId != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(departmentId)) : null;
                var campusIdList = (campusId != null) ? _commonHelper.ConvertIdToList(Convert.ToInt64(campusId)) : null;
                var convertedPin = (pin != "") ? Convert.ToInt32(pin) : 0;
                var authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, organizationIdList, null, branchIdIdList);
                if (!authBranchIdList.Any())
                {
                    throw new InvalidDataException("You have no authorized Branch.");
                }

                return _holidayWorkDao.GetHrHolidayWorkApprovalHistoryRowCount(dateTimeFrom, dateTimeTo, null, (convertedPin != 0) ? _commonHelper.ConvertIdToList(convertedPin) : null, authBranchIdList, campusIdList, departmentIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public DateTime? GetTeamMemberHoliDayWorkIsPostDateTime(long? teamMemberId = null, int? pin = null) 
        {
            DateTime? postDateTime = null;
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            AllowanceSheet allowanceSheet = _allowanceSheetDao.GetTeamMemberLastIsSubmittedAllowanceSheet(teamMemberId, pin);
            HolidayWork holidayWork = _holidayWorkDao.GetTeamMemberLastIsPostHoliDayWork(teamMemberId, pin);

            if (allowanceSheet != null && holidayWork != null)
            {
                postDateTime = allowanceSheet.DateTo;
                if (holidayWork.HolidayWorkDate != null && holidayWork.HolidayWorkDate > postDateTime)
                    postDateTime = holidayWork.HolidayWorkDate;
            }
            else if (allowanceSheet != null)
            {
                postDateTime = allowanceSheet.DateTo;
            }
            else if (holidayWork != null && holidayWork.HolidayWorkDate != null)
            {
                postDateTime = holidayWork.HolidayWorkDate;
            }
            return postDateTime;
        }

        #endregion

        #region Helper function

        private HolidayWork CheckValidHolidayWork(HolidayWork holidayWork, List<long> authorizedMemberIdList)
        {
            HolidayWork previousHolidayWork = new HolidayWork();
            //if (holidayWork.Id > 0)
            //{
            //    previousHolidayWork = _holidayWorkDao.GetTeamMemberHolidayWorkOnDate(holidayWork.TeamMember.Id, holidayWork.HolidayWorkDate.Value.Date);//_holidayWorkDao.LoadById(holidayWork.Id);
            //    //holidayWork.TeamMember = previousHolidayWork.TeamMember;
            //    previousHolidayWork.ApprovalType = holidayWork.ApprovalType;
            //    previousHolidayWork.Reason = holidayWork.Reason;
            //    holidayWork = previousHolidayWork;
            //}

            previousHolidayWork = _holidayWorkDao.GetTeamMemberHolidayWorkOnDate(holidayWork.TeamMember.Id, holidayWork.HolidayWorkDate.Value.Date.Date, false);//_holidayWorkDao.LoadById(holidayWork.Id);
            if (previousHolidayWork != null)
            {
                previousHolidayWork.ApprovalType = holidayWork.ApprovalType;
                previousHolidayWork.Reason = holidayWork.Reason;
                holidayWork = previousHolidayWork;
            }

            //Authorization Check
            if (!authorizedMemberIdList.Contains(holidayWork.TeamMember.Id))
                throw new InvalidDataException("This TeamMember is not authorized for you!( Holiday Work Day " + holidayWork.HolidayWorkDate.Value.ToString("dd-MMM,yyyy") + ") with Pin " + holidayWork.TeamMember.Pin);
          
            //Reason Check: if new Entry then reason only apply on full and half approved type otherwize reason required
            if (String.IsNullOrEmpty(holidayWork.Reason))
                throw new InvalidDataException("please mention Reason!( Holiday Work Day " + holidayWork.HolidayWorkDate.Value.ToString("dd-MMM,yyyy") + ") with Pin " + holidayWork.TeamMember.Pin);


            //Checking this day has already an active day off taken or not
            DayOffAdjustment dayOff = _dayOffAdjustmentDao.GetDayOffAdjustmentByInsteadOfDateAndMemberId(holidayWork.TeamMember.Pin, holidayWork.HolidayWorkDate.Value.Date);
            if (dayOff != null)
            {
                throw new InvalidDataException("This Holiday already taken as dayOff!( Holiday Work Day " + holidayWork.HolidayWorkDate.Value.ToString("dd-MMM,yyyy") + ") with Pin " + holidayWork.TeamMember.Pin);
            }

            #region Is Post Check

            DateTime? postDateTime = GetTeamMemberHoliDayWorkIsPostDateTime(holidayWork.TeamMember.Id);
            if (postDateTime != null && postDateTime.Value.Date >= holidayWork.HolidayWorkDate.Value.Date)
                throw new InvalidDataException("TeamMember can only applicable for holiday work after Date (" + postDateTime.Value.ToString("d MMM, yyyy") + ") for Pin (" + holidayWork.TeamMember.Pin + ") .");

            #endregion
            
            return holidayWork;
        }
        
        private void SaveHolidayWorkApprovalLog(HolidayWork holidayApproval)
        {
            try
            {
                var holidayApprovalLog = new HolidayWorkLog
                {
                    BusinessId = "1",
                    CreationDate = DateTime.Now,
                    CreateBy = (holidayApproval.ModifyBy != 0) ? holidayApproval.ModifyBy : GetCurrentUserId(),
                    HolidayWorkId = holidayApproval.Id,
                    HolidayWorkDate = holidayApproval.HolidayWorkDate,
                    ApprovalType = holidayApproval.ApprovalType,
                    Reason = holidayApproval.Reason
                };
                Session.Save(holidayApprovalLog);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

    }
}
