﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class AttendanceSynchronizerFactory : ObjectFactoryBase<AttendanceSynchronizer>
    {
        //create instance of campusfactory 
        private CampusFactory _campusFactory;
       
       
        private readonly IAttendanceSynchronizerService _attendanceSynchronizerService;
        public AttendanceSynchronizerFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _attendanceSynchronizerService = new AttendanceSynchronizerService(session);
            _campusFactory = new CampusFactory(this, session);          
        }

        #region Create
        public AttendanceSynchronizerFactory Create()
        {
            Object = new AttendanceSynchronizer()
            {
                Id = 0,
                Name = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString(),
                SynchronizerKey = "*567*8#9" + Guid.NewGuid().ToString().Substring(0, 5),
                DataCallingInterval = 5,
                OperatorContact = "8801710352226",
                LastMemberInfoUpdateTime = DateTime.Now
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public AttendanceSynchronizerFactory Create(long id,string name, string synchronizerKey, int dataCallingInterval,
            string operatorContact, DateTime? lastMemberInfoUpdateTime)
        {
            Object = new AttendanceSynchronizer()
            {
                Id = id,
                Name = name,
                SynchronizerKey = synchronizerKey,
                DataCallingInterval = dataCallingInterval,
                OperatorContact = operatorContact,
                LastMemberInfoUpdateTime = lastMemberInfoUpdateTime
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public AttendanceSynchronizerFactory WithSynchronizerKey(string synchronizerKey) 
        {
            Object.SynchronizerKey = synchronizerKey;
            return this;
        }
        public AttendanceSynchronizerFactory WithCampus(List<CampusRoom> campusRoomList)
        {
            _campusFactory.Create().WithCampusRoomList(campusRoomList).WithBranch().Persist();
            Object.Campus = _campusFactory.Object;
            return this;
        } 
        public AttendanceSynchronizerFactory WithCampus() 
        {
            _campusFactory.Create().WithBranch().Persist(); 
            Object.Campus = _campusFactory.Object;
            return this;
        }

        public AttendanceSynchronizerFactory WithCampus(Campus campus)
        {
            Object.Campus = campus;
            return this;
        }

        public AttendanceSynchronizerFactory WithAttendanceDeviceList(List<AttendanceDevice> objList) 
        {
            Object.AttendanceDevice = objList;
            return this;
        }
        public AttendanceSynchronizerFactory CreateMore(int numberOfAttdSync, IList<Campus> campusList )
        {
            ListStartIndex = ObjectList.Count;
            foreach (var campus in campusList) 
            {
                for (int i = 0; i < numberOfAttdSync; i++)
                {
                    var obj = new AttendanceSynchronizer()
                    {
                        Id = 0,
                        SynchronizerKey = "*567*8#9" + Guid.NewGuid().ToString().Substring(0, 5),
                        DataCallingInterval = 5,
                        OperatorContact = "8801710352226",
                        LastMemberInfoUpdateTime = DateTime.Now,
                        Name = "Name_" + i + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString(),
                    };
                    obj.Campus = campus;
                    ObjectList.Add(obj);
                }
            }
            return this;
        }

        public AttendanceSynchronizerFactory CreateMore(int numberOfAttdSync)
        {
            ListStartIndex = ObjectList.Count;
            _campusFactory.Create().Persist();
            for (int i = 0; i < numberOfAttdSync; i++)
            {
                var obj = new AttendanceSynchronizer()
                {
                    Id = 0,
                    SynchronizerKey = "*567*8#9" + Guid.NewGuid().ToString().Substring(0, 5),
                    DataCallingInterval = 5,
                    OperatorContact = "8801710352226",
                    LastMemberInfoUpdateTime = DateTime.Now,
                    Name = "Name_"+i+DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString(),
                };
                obj.Campus = _campusFactory.Object;
                ObjectList.Add(obj);
            }
            return this;
        }

        public AttendanceSynchronizerFactory CreateMore(int numberOfAttdSync, Campus campus)
        {
            ListStartIndex = ObjectList.Count;
            for (int i = 0; i < numberOfAttdSync; i++)
            {
                var obj = new AttendanceSynchronizer()
                {
                    Id = 0,
                    SynchronizerKey = "*567*8#9" + Guid.NewGuid().ToString().Substring(0, 5),
                    DataCallingInterval = 5,
                    OperatorContact = "8801710352226",
                    LastMemberInfoUpdateTime = DateTime.Now,
                    Name = "Name_" + i
                };
                obj.Campus = campus;
                ObjectList.Add(obj);
            }
            return this;
        }
        public AttendanceSynchronizerFactory Persist()
        {

            SingleObjectList.ForEach(item =>
            {
                if (item.Id == 0)
                {
                    _attendanceSynchronizerService.Save(item);
                }
            });
            
            ObjectList.ForEach(item =>
            {
                if (item.Id == 0)
                {
                    _attendanceSynchronizerService.Save(item);
                }
            });
            return this;
        }
        #endregion

        #region Others
        #endregion

        #region CleanUp


        public void Cleanup()
        {
            DeleteAll();
            _campusFactory.Cleanup();
        }
        #endregion

        internal void Evict(AttendanceSynchronizer attendanceSynchronizer)
        {
            _attendanceSynchronizerService.Session.Evict(attendanceSynchronizer);
        }
      
    }
}
