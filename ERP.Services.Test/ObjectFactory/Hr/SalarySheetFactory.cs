﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class SalarySheetFactory:ObjectFactoryBase<SalarySheet>
    {
        protected readonly SalarySheetService SalarySheetService;
        public SalarySheetFactory(IObjectFactoryBase caller,ISession session):base(caller,session)
        {
            SalarySheetService = new SalarySheetService(session);
        }

        public SalarySheetFactory Create()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            Object = new SalarySheet()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = MemberEbfHistory.EntityStatus.Active,
                BasicSalaryAmount = 72,
                HouseRentAmount = 36,
                ConveyanceAmount = Convert.ToDecimal(3.60),
                MedicalAmount = Convert.ToDecimal(8.40),
                GrossSalaryAmount = 120,
                IncreamentAmount = 0,
                ArrearAmount = 0,
                AutoZoneAmount = 0,
                FinalZoneAmount = 0,
                AutoAbsentAmount = Convert.ToDecimal(5.40),
                FinalAbsentAmount = Convert.ToDecimal(5.40),
                LeaveWithoutPayAmount = Convert.ToDecimal(0.00),
                AutoLoanRefund = Convert.ToDecimal(0.00),
                FinalLoanRefund = Convert.ToDecimal(1000.00),
                TdsAmount = Convert.ToDecimal(5.00),
                BankAmount = Convert.ToDecimal(50),
                CashAmount = Convert.ToDecimal(100),
                TotalSalaryAmount = Convert.ToDecimal(1088.60),
                TotalAmount = Convert.ToDecimal(150),
                EbfAmount = Convert.ToDecimal(0.00),
                EbfForEmplyer = Convert.ToDecimal(0.00),
                IsSubmit = true,
                StartDate = startDate,
                EndDate = endDate,
                Year = 2016,
                Month = 12
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public SalarySheetFactory CreateWith(long id, decimal basicSalaryAmount, decimal houseRentAmount, decimal conveyanceAmount, decimal medicalAmount,
            decimal grossSalaryAmount, decimal increamentAmount, decimal arrearAmount, decimal autoZoneAmount, decimal finalZoneAmount, decimal autoAbsentAmount,
            decimal finalAbsentAmount, decimal leaveWithoutPayAmount, decimal autoLoanRefund, decimal finalLoanRefund, decimal tdsAmount, decimal bankAmount,
            decimal cashAmount, decimal totalSalaryAmount, decimal totalAmount, decimal ebfAmount, decimal ebfForEmplyer,bool isSubmit,
            DateTime startDate, DateTime endDate, int year, int month)
        {
            Object = new SalarySheet()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = MemberEbfHistory.EntityStatus.Active,
                BasicSalaryAmount = basicSalaryAmount,
                HouseRentAmount = houseRentAmount,
                ConveyanceAmount = conveyanceAmount,
                MedicalAmount = medicalAmount,
                GrossSalaryAmount = grossSalaryAmount,
                IncreamentAmount = increamentAmount,
                ArrearAmount = arrearAmount,
                AutoZoneAmount = autoZoneAmount,
                FinalZoneAmount = finalZoneAmount,
                AutoAbsentAmount = autoAbsentAmount,
                FinalAbsentAmount = finalAbsentAmount,
                LeaveWithoutPayAmount = leaveWithoutPayAmount,
                AutoLoanRefund = autoLoanRefund,
                FinalLoanRefund = finalLoanRefund,
                TdsAmount = tdsAmount,
                BankAmount = bankAmount,
                CashAmount = cashAmount,
                TotalSalaryAmount = totalSalaryAmount,
                TotalAmount = totalAmount,
                EbfAmount = ebfAmount,
                EbfForEmplyer = ebfForEmplyer,
                IsSubmit = isSubmit,
                StartDate = startDate,
                EndDate = endDate,
                Year = year,
                Month = month
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public SalarySheetFactory WithIsSubmit(bool isSubmit)
        {
            Object.IsSubmit = isSubmit;
            return this;
        }

        public SalarySheetFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public SalarySheetFactory WithSalaryHistory(SalaryHistory salaryHistory)
        {
            Object.SalaryHistory = salaryHistory;
            return this;
        }

        public SalarySheetFactory WithSalaryOrganization(Organization salaryOrganization)
        {
            Object.SalaryOrganization = salaryOrganization;
            return this;
        }

        public SalarySheetFactory WithSalaryBranch(Branch salaryBranch)
        {
            Object.SalaryBranch = salaryBranch;
            return this;
        }

        public SalarySheetFactory WithSalaryCampus(Campus salaryCampus)
        {
            Object.SalaryCampus = salaryCampus;
            return this;
        }

        public SalarySheetFactory WithSalaryDepartment(Department salaryDepartment)
        {
            Object.SalaryDepartment = salaryDepartment;
            return this;
        }
        public SalarySheetFactory WithSalaryDesignation(Designation salaryDesignation)
        {
            Object.SalaryDesignation = salaryDesignation;
            return this;
        }
        public SalarySheetFactory WithJobOrganization(Organization jobOrganization)
        {
            Object.JobOrganization = jobOrganization;
            return this;
        }
        public SalarySheetFactory WithJobBranch(Branch jobBranch)
        {
            Object.JobBranch = jobBranch;
            return this;
        }
        public SalarySheetFactory WithJobCampus(Campus jobCampus)
        {
            Object.JobCampus = jobCampus;
            return this;
        }
        public SalarySheetFactory WithJobDepartment(Department jobDepartment)
        {
            Object.JobDepartment = jobDepartment;
            return this;
        }
        public SalarySheetFactory WithJobDesignation(Designation jobDesignation)
        {
            Object.JobDesignation = jobDesignation;
            return this;
        }

        public SalarySheetFactory WithSalarySheetDetails(List<SalarySheetDetails> salarySheetDetails)
        {
            Object.SalarySheetDetailses = salarySheetDetails ;
            return this;
        }

        public SalarySheetFactory WithMemberEbfHistory(List<MemberEbfHistory> memberEbfHistorys)
        {
            Object.MemberEbfHistories = memberEbfHistorys;
            return this;
        }

        public SalarySheetFactory Persist()
        {
            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    SalarySheetService.SaveOrUpdate(new List<SalarySheet>(){x});
                }
            });

            ObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    SalarySheetService.SaveOrUpdate(new List<SalarySheet>() { x });
                }
            });
            
            return this;
        }

        public SalarySheetFactory CleanUp()
        {
            DeleteAll();
            return this;
        }
    }
}