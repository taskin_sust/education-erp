﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceAdjustmentTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceAdjustment")]
    public class AttendanceAdjustmentUpdateTest : AttendanceAdjustmentBaseTest
    {
        #region basic Test

        [Fact]
        public void Should_Return_AttendanceAdjustment_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016,12,5),new DateTime(2016, 12, 5, 10, 10, 10),new DateTime(2016, 12, 5, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm);
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>()
            {
                _attendanceAdjustmentFactory.Object
            });

            AttendanceAdjustment attendanceAdjustment = _attendanceAdjustmentService.LoadById(_attendanceAdjustmentFactory.Object.Id);

            attendanceAdjustment.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Rejected;
            attendanceAdjustment.Reason = "Due to update";

            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>()
            {
                attendanceAdjustment
            });

            AttendanceAdjustment attendanceAdjustmentupdated = _attendanceAdjustmentService.LoadById(_attendanceAdjustmentFactory.Object.Id);

            Assert.Equal(attendanceAdjustmentupdated.Reason, "Due to update");
            Assert.Equal(attendanceAdjustmentupdated.AdjustmentStatus, (int)AttendanceAdjustmentStatus.Rejected);
        }

        [Fact]
        public void Should_Return_AttendanceAdjustment_InvalidDataException_For_Reason()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016,12,5),new DateTime(2016, 12, 5, 10, 10, 10),new DateTime(2016, 12, 5, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm);
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>()
            {
                _attendanceAdjustmentFactory.Object
            });

            AttendanceAdjustment attendanceAdjustment = _attendanceAdjustmentService.LoadById(_attendanceAdjustmentFactory.Object.Id);

            attendanceAdjustment.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Rejected;
            attendanceAdjustment.Reason = "";

            Assert.Throws<InvalidDataException>(() => _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>()
            {
                attendanceAdjustment
            }));
        }

        [Fact]
        public void Should_Return_AttendanceAdjustment_InvalidException_For_Empty_Adjustment()
        {
            Assert.Throws<InvalidDataException>(
                () =>
                    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { }));

        }
        
        #endregion
        
        #region Business Logic Test
        
        [Fact]
        public void Should_Return_AttendanceAdjustment_InvalidException_For_StartTimeIsGreaterThanEndTime()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016,12,5),new DateTime(2016, 12, 5, 10, 10, 10),new DateTime(2016, 12, 5, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm);
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>()
            {
                _attendanceAdjustmentFactory.Object
            });

            AttendanceAdjustment attendanceAdjustment = _attendanceAdjustmentService.LoadById(_attendanceAdjustmentFactory.Object.Id);

            attendanceAdjustment.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Rejected;
            attendanceAdjustment.Reason = "Due to update";

            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>()
            {
                attendanceAdjustment
            });

            AttendanceAdjustment attendanceAdjustmentupdated = _attendanceAdjustmentService.LoadById(_attendanceAdjustmentFactory.Object.Id);
            attendanceAdjustmentupdated.StartTime = new DateTime(2016,12,5);
            attendanceAdjustmentupdated.EndTime = new DateTime(2016,12,2);

            Assert.Throws<InvalidDataException>(
                () =>
                    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>(){attendanceAdjustmentupdated}));
        }

        [Fact]
        public void Should_Return_AttendanceAdjustment_Duplicate_Exception()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016,12,5), new DateTime(2016, 12, 5, 10, 10, 10),new DateTime(2016, 5, 5, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm);
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>()
            {
                _attendanceAdjustmentFactory.Object
            });

            AttendanceAdjustment attendanceAdjustment = _attendanceAdjustmentService.LoadById(_attendanceAdjustmentFactory.Object.Id);

            attendanceAdjustment.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Rejected;
            attendanceAdjustment.Reason = "Due to update";

            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>()
            {
                attendanceAdjustment
            });
            
            var att = _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016, 12, 5), 
                new DateTime(2016, 12, 5, 10, 10, 10),new DateTime(2016, 5, 5, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm).Object;

            Assert.Throws<DuplicateEntryException>(() => _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { att }));
        }

        [Fact(Skip = "For Decesion")]
        public void Should_Return_AttendanceAdjustment_IsPost_SalarySheet()
        {

            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;
            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();

            _attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();

            salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation)
                ;

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);





        }
        
        #region RejectedAttendanceAdjustment

        [Fact]
        public void Should_Return_RejectedAttendanceAdjustment_InvalidException_FOr_AttendanceAdjustment()
        {
            Assert.Throws<InvalidDataException>(() => _attendanceAdjustmentService.RejectedAttendanceAdjustment(null));
        }

        [Fact]
        public void Should_Return_RejectedAttendanceAdjustment_InvalidException_FOr_TeamMember()
        {
            var a = new AttendanceAdjustment()
            {
                Reason = "Hudai",
                TeamMember = null,
                Date = new DateTime(2016,12,1),
                AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved,
                IsPost = false
            };
            Assert.Throws<InvalidDataException>(() => _attendanceAdjustmentService.RejectedAttendanceAdjustment(a));
        }

        [Fact]
        public void Should_Return_RejectedAttendanceAdjustment_InvalidException_FOr_Date()
        {
             var joiningDate = new DateTime(2016, 6, 1);
             var dateTo = new DateTime(2016, 6, 1);
             var tm = PersistTeamMember(joiningDate, dateTo);
            var a = new AttendanceAdjustment()
            {
                Reason = "Hudai",
                TeamMember = tm,
                Date = null,
                AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved,
                IsPost = false

            };
            Assert.Throws<InvalidDataException>(() => _attendanceAdjustmentService.RejectedAttendanceAdjustment(a));
        }

        [Fact]
        public void Should_Return_RejectedAttendanceAdjustment_InvalidException_FOr_HolidayWork()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;
            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();
            _attendanceAdjustmentFactory.CreateMore(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);
            
            var a = _attendanceAdjustmentFactory.ObjectList.FirstOrDefault(x => x.Date.Value.Date == holyDayDate.Date);

            Assert.Throws<DependencyException>(() => _attendanceAdjustmentService.RejectedAttendanceAdjustment(a));
        }

        [Fact]
        public void Should_Return_RejectedAttendanceAdjustment_InvalidException_FOr_NightWork()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            _attendanceAdjustmentFactory.CreateMore(startDate, tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            nightWorkFactory.CreateWith(new DateTime(2016, 12, 18), 1).WithTeamMember(tm).Persist();
            DateTime nightWorkDate = new DateTime(2016, 12, 18);
            var a = _attendanceAdjustmentFactory.ObjectList.FirstOrDefault(x => x.Date.Value.Date == nightWorkDate.Date);
            Assert.Throws<DependencyException>(() => _attendanceAdjustmentService.RejectedAttendanceAdjustment(a));
        }

        [Fact]
        public void Should_Return_RejectedAttendanceAdjustment_InvalidException_FOr_DayOffAdjustment()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            _attendanceAdjustmentFactory.CreateMore(startDate, tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);

            DateTime dayOffDay = new DateTime(2016, 12, 18);
            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016,1,15), dayOffDay,
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() {_dayOffAdjustmentFactory.Object});
            var a = _attendanceAdjustmentFactory.ObjectList.FirstOrDefault(x => x.Date.Value.Date == dayOffDay.Date);
            Assert.Throws<DependencyException>(() => _attendanceAdjustmentService.RejectedAttendanceAdjustment(a));
        }

        #endregion

        #region AttendanceAdjustmentFinalize

        [Fact]
        public void Should_Return_AttendanceAdjustmentFinalize_DataNotFoundException_For_TeamMember()
        {
            Assert.Throws<NullObjectException>(
                () => _attendanceAdjustmentService.AttendanceAdjustmentFinalize(null, new DateTime(), new DateTime()));
        }

        [Fact]
        public void Should_Return_AttendanceAdjustmentFinalize_DataNotFoundException_For_AttendanceAdjustment()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);
            Assert.Throws<NullObjectException>(
                () => _attendanceAdjustmentService.AttendanceAdjustmentFinalize(new List<long>() { tm.Pin }, new DateTime(2016,12,1), new DateTime(2016,12,1)));
        }

        #endregion

        #endregion

    }
}
