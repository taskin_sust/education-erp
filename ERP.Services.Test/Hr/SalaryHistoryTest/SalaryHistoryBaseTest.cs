﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.SalaryHistoryTest
{

    public class SalaryHistoryBaseTest : TestBase, IDisposable
    {
        #region Propertise & Object Initilization
        public const int MentorId = 2;
        private ISession _session;
        protected readonly ISalaryHistoryService _salaryHistoryService;
        protected readonly ITeamMemberService _teamMemberService;
        protected readonly SalaryHistoryFactory _salaryHistoryFactory;
        protected readonly TeamMemberFactory _teamMemberFactory;
        protected readonly EmploymentHistoryFactory _employmentHistoryFactory;
        protected readonly OrganizationFactory _organizationFactory;
        protected readonly BranchFactory _branchFactory;
        protected readonly DepartmentFactory _departmentFactory;
        protected readonly CampusFactory _campusFactory;
        protected readonly CampusRoomFactory _campusRoomFactory;
        protected readonly DesignationFactory _designationFactory;


        #endregion

        public SalaryHistoryBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _salaryHistoryService = new SalaryHistoryService(_session);
            _teamMemberService = new TeamMemberService(_session);
            _salaryHistoryFactory = new SalaryHistoryFactory(null, _session);
            _teamMemberFactory = new TeamMemberFactory(null, _session);
            _employmentHistoryFactory = new EmploymentHistoryFactory(null, _session);
            _organizationFactory = new OrganizationFactory(null, _session);
            _departmentFactory = new DepartmentFactory(null, _session);
            _branchFactory = new BranchFactory(null, _session);
            _campusFactory = new CampusFactory(null, _session);
            _campusRoomFactory = new CampusRoomFactory(null, _session);
            _designationFactory = new DesignationFactory(null, _session);

        }

        #region Disposal

        public void Dispose()
        {
            //_salaryHistoryFactory.LogCleanUp(_teamMemberFactory.Object);
            _salaryHistoryFactory.LogCleanUp(_salaryHistoryFactory);
            _salaryHistoryFactory.CleanUp();
            base.Dispose();
        }
        
        #endregion

    }
}
