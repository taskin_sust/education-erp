﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class ComplementaryCourseController : Controller
    {
        #region Logger
        private ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ICommonHelper _commonHelper;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly IComplementaryCourseService _complementaryCourseService;
        private readonly IOrganizationService _organizationService;
        public ComplementaryCourseController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _organizationService = new OrganizationService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _courseService = new CourseService(session);
                _complementaryCourseService = new ComplementaryCourseService(session);

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }

        }
        #endregion


        #region Operational Function
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.OrganizationIdList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.ProgramIdList = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionIdList = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.CourseIdList = new SelectList(new List<Course>(), "Id", "Name");
            ViewBag.ComplementaryCourseIdList = new SelectList(new List<Course>(), "Id", "Name");
            try
            {
                ViewBag.OrganizationIdList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return View();


        }
        #endregion Operational Function

        #region Helper Function
        [HttpPost]
        public ActionResult LoadComplementaryCourse(List<long> organizationIds, List<long> programIds, List<long> sessionIds, long courseId)
        {
            ViewBag.ComplementaryCourse = new List<Course>();
            ViewBag.Program = new Program();
            ViewBag.Session = new Session();
            ViewBag.MainCourse = new Course();
            try
            {
                ViewBag.ComplementaryCourse = _courseService.LoadComplementaryCourse(organizationIds, programIds, sessionIds, courseId);
                ViewBag.Program = programIds[0];
                ViewBag.Session = sessionIds[0];
                ViewBag.MainCourse = _courseService.GetCourse(courseId);
                return PartialView("Partial/_ComplementaryCourse");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Program Occurred During Load Course";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]
        public JsonResult AddComplementaryCourse(long programId, long sessionId, long courseId, List<long> complementaryCourseIds)
        {
            try
            {
                _complementaryCourseService.Save(programId, sessionId, courseId, complementaryCourseIds);
                return Json(new { Message = "Complementary Course add successfully!", IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Program Occurred During Add Complementary Course";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion Helper Function
        

        


    }
}