﻿using System;
using System.Collections.Generic;
using System.Web.Util;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface IProgramService : IBaseService
    {
        #region Operational Function

        void ProgramSaveOrUpdate(Program program);
        //void IsSave(Program program);
        //bool UpdateProgram(Program oldProgram, Program newProgram, int programType, int status);
        void Delete(Program program);
        // bool UpdateRank(Program programGroupOldObj, Program programUpdateObj);
        bool UpdateRank(Program program, string action);
        #endregion

        #region Single Instances Loading Function
        Program GetProgram(long programId);
        
        Program GetProgram(string shortName);
        //Program GetProgramByRankNextOrPrevious(int rank, string action);
        #endregion

        #region List Loading Function
        IList<Program> LoadProgram(List<long> organizationIds = null, List<long> sessionIds = null);
        IList<Program> LoadProgram(int start, int length, string orderBy, string direction, string organization, string programName, string code, string shortName, string type, string status, string rank);
        IList<Program> LoadProgram(DateTime date, long teacherId);
        IList<Program> LoadAuthorizedProgram(List<UserMenu> userMenu, List<long> organizationIdList = null, List<long> sessionIdList = null, bool? isOffice = null, bool? isPublic = null, List<long> branchIdList = null);
        IList<string> LoadProgramCode();

        #endregion

        #region Others Function
        int GetProgramCount(string orderBy, string direction, string organization, string programName, string code, string shortName, string type, string status, string rank);
        int GetMaximumRank(Program program);
        int GetMinimumRank(Program program);

        #endregion

        #region Helper Function
        Dictionary<string, int> GetProgramType();
        #endregion

        #region Public API
        IList<Program> LoadPublicProgram(string orgBusinessId);
        #endregion

    }
    public class ProgramService : BaseService, IProgramService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IProgramDao _programDao;
        private readonly IOrganizationDao _organizationDao;

        private readonly ICommonHelper _commonHelper;
        public ProgramService(ISession session)
        {
            Session = session;
            _programDao = new ProgramDao { Session = Session };
            _organizationDao = new OrganizationDao { Session = Session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function
        public void ProgramSaveOrUpdate(Program program)
        {
            ITransaction trans = null;
            try
            {
                CheckBeforeSaveOrUpdate(program);

                using (trans = Session.BeginTransaction())
                {
                    if (program.Id > 0)
                    {
                        var existingProgram = _programDao.LoadById(program.Id);
                        existingProgram.BusinessId = program.BusinessId;
                        existingProgram.Organization = program.Organization;
                        existingProgram.Name = program.Name;
                        existingProgram.Code = program.Code;
                        existingProgram.ShortName = program.ShortName;
                        existingProgram.Status = program.Status;
                        existingProgram.Type = program.Type;
                        _programDao.Update(existingProgram);
                    }
                    else
                    {
                        program.Rank = _programDao.GetMaximumRank(program) + 1;
                        program.Status = Program.EntityStatus.Active;
                        _programDao.Save(program);
                    }
                    trans.Commit();
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }

            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #region OLD CODE
        //public void IsSave(Program program)
        //{
        //    ITransaction trans = null;
        //    try
        //    {
        //        if (program.Organization.OrganizationReference < 1 && program.Organization != null)
        //        {
        //            program.Organization.OrganizationReference = program.Organization.Id;
        //        }
        //        //do before save purpose
        //        CheckBeforeSaveOrUpdate(program);
        //        //responsible for save program
        //        using (trans = Session.BeginTransaction())
        //        {
        //            program.Organization = _organizationDao.LoadById(program.Organization.OrganizationReference);
        //            program.Rank = _programDao.GetMaximumRank(program) + 1;
        //            _programDao.Save(program);
        //            trans.Commit();
        //        }
        //    }
        //    catch (NullObjectException ex)
        //    {
        //        throw;
        //    }
        //    catch (InvalidDataException ex)
        //    {
        //        throw;
        //    }

        //    catch (DuplicateEntryException ex)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }

        //    finally
        //    {
        //        if (trans != null && trans.IsActive)
        //            trans.Rollback();

        //    }
        //}

        //public bool UpdateProgram(Program oldProgram, Program newProgram, int programType, int status)
        //{
        //    ITransaction trans = null;
        //    try
        //    {
        //        if (newProgram == null)
        //        {
        //            throw new NullObjectException("program can not be null");
        //        }
        //        newProgram.Type = programType;
        //        CheckBeforeSaveOrUpdate(newProgram);

        //        using (trans = Session.BeginTransaction())
        //        {
        //            oldProgram.Name = newProgram.Name;
        //            oldProgram.Code = newProgram.Code;
        //            oldProgram.ShortName = newProgram.ShortName;
        //            oldProgram.Status = status;
        //            oldProgram.Organization = _organizationDao.LoadById(newProgram.Organization.OrganizationReference);
        //            oldProgram.Type = programType;
        //            _programDao.Update(oldProgram);
        //            trans.Commit();
        //            return true;
        //        }


        //    }
        //    catch (NullObjectException ex)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (trans != null && trans.IsActive)
        //            trans.Rollback();
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}

        //public bool UpdateRank(Program programGroupOldObj, Program programUpdateObj)
        //{
        //    ITransaction trans = null;
        //    try
        //    {
        //        using (trans = Session.BeginTransaction())
        //        {
        //            _programDao.Update(programGroupOldObj);
        //            _programDao.Update(programUpdateObj);
        //            trans.Commit();
        //            return true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        if (trans != null && trans.IsActive)
        //            trans.Rollback();
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}
        #endregion


        public void Delete(Program program)
        {
            ITransaction trans = null;
            try
            {
                if (program == null)
                {
                    throw new NullObjectException("object references not found");
                }
                if (program.ProgramBranchSessions != null && program.ProgramBranchSessions.Count > 0)
                {
                    throw new DependencyException("Program Can't be deleted due to one or more dependency!");
                }
                using (trans = Session.BeginTransaction())
                {
                    program.Status = Program.EntityStatus.Delete;
                    _programDao.Update(program);
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }


        public bool UpdateRank(Program program, string action)
        {
            ITransaction trans = null;
            try
            {
                if (program == null)
                {
                    throw new NullObjectException("Program can not be empty");
                }
                int newRank;
                if (action == "up")
                    newRank = program.Rank - 1;
                else
                    newRank = program.Rank + 1;

                var programOldObj = GetProgramByRankNextOrPrevious(newRank, action);
                //if (programOldObj == null)
                //{
                //    throw new NullObjectException("object references not found");
                //}
                //newRank = programOldObj.Rank;
                //programOldObj.Rank = program.Rank;
                //program.Rank = newRank;
                if (programOldObj != null)
                {
                    newRank = programOldObj.Rank;
                    programOldObj.Rank = program.Rank;
                    program.Rank = newRank;
                }
                using (trans = Session.BeginTransaction())
                {
                    if (programOldObj != null)
                        _programDao.Update(programOldObj);
                    _programDao.Update(program);
                    trans.Commit();
                    return true;
                }

            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public Program GetProgram(long programId)
        {
            Program program;
            try
            {
                program = _programDao.LoadById(programId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return program;
        }

        public Program GetProgram(string shortName)
        {
            Program program;
            try
            {
                program = _programDao.GetProgram(shortName.Trim());
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return program;
        }

        private Program GetProgramByRankNextOrPrevious(int rank, string action)
        {
            Program program;
            try
            {
                program = _programDao.LoadByRankDirection(rank, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return program;
        }
        #endregion

        #region List Loading Function
        public IList<Program> LoadProgram(List<long> organizationIds = null, List<long> sessionIds = null)
        {
            try
            {
                return _programDao.LoaProgram(organizationIds, sessionIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Program> LoadAuthorizedProgram(List<UserMenu> userMenu, List<long> organizationIdList = null, List<long> sessionIdList = null, bool? isOffice = null, bool? isPublic = null, List<long> branchIdList = null)
        {
            try
            {
                List<long> authorizeOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, CommonHelper.ConvertSelectedAllIdList(organizationIdList));
                List<long> authorizeProgramIdList = AuthHelper.LoadProgramIdList(userMenu, authorizeOrganizationIdList);
                List<long> authorizeBranchIdList = null;
                if (branchIdList != null)
                    authorizeBranchIdList = AuthHelper.LoadBranchIdList(userMenu, CommonHelper.ConvertSelectedAllIdList(organizationIdList), authorizeProgramIdList, (!branchIdList.Contains(SelectionType.SelelectAll)) ? branchIdList : null);

                var programList = _programDao.LoadAuthorizedProgram(authorizeProgramIdList, CommonHelper.ConvertSelectedAllIdList(sessionIdList), isOffice, isPublic, authorizeBranchIdList);
                return programList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Program> LoadProgram(int start, int length, string orderBy, string direction, string organization, string programName, string code, string shortName, string type, string status, string rank)
        {
            try
            {
                return _programDao.LoadProgram(start, length, orderBy, direction, organization, programName, code, shortName, type, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public IList<Program> LoadProgram(DateTime date, long teacherId)
        {
            return _programDao.LoadProgram(date, teacherId);
        }

        public IList<string> LoadProgramCode()
        {
            return _programDao.LoadProgramCode();
        }


        #endregion

        #region Others Function
        public int GetMaximumRank(Program program)
        {
            int maximumRank;
            try
            {
                maximumRank = _programDao.GetMaximumRank(program);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return maximumRank;
        }
        public int GetMinimumRank(Program program)
        {
            int minimumRank;
            try
            {
                minimumRank = _programDao.GetMinimumRank(program);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return minimumRank;
        }
        public int GetProgramCount(string orderBy, string direction, string organization, string programName, string code, string shortName, string type, string status, string rank)
        {
            try
            {
                return _programDao.GetProgramCount(orderBy, direction, organization, programName, code, shortName, type, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Helper Function
        public Dictionary<string, int> GetProgramType()
        {
            try
            {
                var programType = new Dictionary<string, int>
                {
                    {"Paid", Program.ProgramTypeStatus.Paid},
                    {"Unpaid", Program.ProgramTypeStatus.UnPaid},
                    {"Csr", Program.ProgramTypeStatus.Csr}
                };
                return programType;
            }
            catch (Exception e)
            {
                return new Dictionary<string, int>();
            }
        }
        private void CheckBeforeSaveOrUpdate(Program program)
        {
            if (program == null)
            {
                throw new NullObjectException("program can not be null");
            }

            if (program.Organization == null || program.Organization.Id <= 0)
            {
                throw new InvalidDataException("organization is invalid");
            }

            //validation check with meta data
            var validationResult = ValidationHelper.ValidateEntity<Program, Program.ProgramMetaData>(program);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
            //var isDuplicate = _programDao.IsDuplicateProgram(program.Name, program.Code, program.ShortName, program.Type, program.Id);
            var isDuplicate = _programDao.HasDuplicateProgram(program);

            if (!String.IsNullOrWhiteSpace(isDuplicate))
            {
                throw new DuplicateEntryException(isDuplicate + " duplicate entry found.");
            }

            if (program.Id > 0)
            {
                Session.Evict(program);
                var alreadyPersistedProgram = _programDao.LoadById(program.Id);
                if (alreadyPersistedProgram.Status != program.Status)
                {
                    if (alreadyPersistedProgram.ProgramBranchSessions != null && alreadyPersistedProgram.ProgramBranchSessions.Count > 0)
                    {
                        throw new DependencyException("You can't inactive the program due to one or more dependency!");
                    }
                }
            }
        }

        #endregion

        #region Public API
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orgBusinessId"></param>
        /// <returns></returns>
        public IList<Program> LoadPublicProgram(string orgBusinessId)
        {
            IList<Program> programs;
            try
            {
                programs = _programDao.LoadPublicProgram(orgBusinessId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

            return programs;
        }
        #endregion
    }
}
