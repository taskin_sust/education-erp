﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UdvashERP.FingerPrint.BusinessModel
{
    public class Device
    {
        public Device()
        {
            //IpAddress = "192.168.1.201";
            //CommunicationKey = "419555";
            //MachineNo = 10;
            //Port = 4370;
        }
        [XmlAttribute("Name")]
        public string Name { get; set; }
        [XmlAttribute("IpAddress")]
        public string IpAddress { get; set; }
        [XmlAttribute("CommunicationKey")]
        public string CommunicationKey { get; set; }
        [XmlAttribute("MachineNo")]
        public int MachineNo { get; set; }
        [XmlAttribute("Port")]
        public int Port { get; set; }
    }

    
}
