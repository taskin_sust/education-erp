﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Dto.Students;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students 
{
    public interface IStudentSurveyService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Functions

        StudentSurveyAnswer LoadStudentAnswerByQuestionIdStudentProgramId(long questionId, long studentProgramId); 

        #endregion
        
        #region List Loading Functions

        IList<StudentMakeSurveyDto> LoadAuthorizedStudentsMakeSurveyDto(List<UserMenu> userMenu, int start, int length, string orderBy, string orderDir, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchName, long[] courseId, int surveyStatus, string programRoll, string name);
        IList<StudentWiseSurveyReportDto> GetAuthorizedStudentWiseSurveyReportDto(List<UserMenu> userMenu, int start, int length, string orderBy, string orderDir, long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long[] questionIds, string[] answerNames, string programRoll, string name, string mobile);
        IList<List<string>> LoadQuestionWiseSurvey(List<UserMenu> userMenu, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, long[] questionId, string[] answerName);
        IList<StudentWiseSurveyQuestionAnswerDto> LoadStudentWiseSurveyQuestionAnswerDto(List<long> studentProgramIdList, List<long> surveyQuestionIdList);
        
        #endregion

        #region Other Functions

        int GetAuthorizedStudentsCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int surveyStatus, string programRoll, string name);
        void SaveSurvey(IList<StudentSurveyAnswer> studentSurveyAnswerList);
        int GetAuthorizedStudentWiseSurveyReportCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, long[] questionId, string[] answerName, string programRoll, string nickName, string mobile);
        int QuestionWiseSurveyCount(int start, int length, long[] questions);

        #endregion
        
    }
     
    public class StudentSurveyService : BaseService, IStudentSurveyService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        
        #endregion
       
        #region Propertise & Object Initialization 
        
        private readonly ISurveyQuestionDao _surveyQuestionDao;
        private readonly ISurveyQuestionAnswerDao _surveyQuestionAnswerDao;
        private readonly IProgramSessionSurveyQuestionDao _programSessionSurveyQuestionDao;
        private readonly IStudentSurveyAnswerDao _studentSurveyAnswerDao; 
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        private readonly IStudentProgramDao _StudentProgramDao;
        private readonly CommonHelper _commonHelper;
        
        public StudentSurveyService(ISession session) 
        {
            Session = session;
            _surveyQuestionDao = new SurveyQuestionDao() { Session = Session };
            _surveyQuestionAnswerDao = new SurveyQuestionAnswerDao() {Session = Session};
            _programSessionSurveyQuestionDao = new ProgramSessionSurveyQuestionDao() { Session = Session };
            _studentSurveyAnswerDao = new StudentSurveyAnswerDao() { Session = Session };
            _programDao = new ProgramDao() { Session = Session };
            _sessionDao = new SessionDao() { Session = Session };
            _StudentProgramDao = new StudentProgramDao() { Session = Session };
            _commonHelper = new CommonHelper();
        }
        
        #endregion

        #region Operational Function

        public void SaveSurvey(IList<StudentSurveyAnswer> studentSurveyAnswerList)
        {
            try
            {
                ITransaction transaction = null;
                using (transaction = Session.BeginTransaction())
                {
                    try
                    {
                        foreach (var ssa in studentSurveyAnswerList)
                        {
                            // StudentSurveyAnswer isExistAnswer = _studentSurveyAnswerDao.LoadStudentAnswerByAnswerId(ssa.SurveyQuestionAnswer.Id);
                            if (ssa.Id > 0)
                            {
                                _studentSurveyAnswerDao.Update(ssa);
                            }
                            else
                            {
                                _studentSurveyAnswerDao.Save(ssa);
                            }
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        if (transaction != null && transaction.IsActive)
                            transaction.Rollback();
                        _logger.Error(ex);
                        throw;
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }  

        #endregion

        #region Single Instances Loading Functions

        public StudentSurveyAnswer LoadStudentAnswerByQuestionIdStudentProgramId(long questionId, long studentProgramId)
        {
            try
            {
                return _studentSurveyAnswerDao.LoadStudentAnswerByQuestionIdStudentProgramId(questionId, studentProgramId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        
        #endregion
        
        #region List Loading Functions

        public IList<StudentMakeSurveyDto> LoadAuthorizedStudentsMakeSurveyDto(List<UserMenu> userMenu, int start, int length, string orderBy, string orderDir, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int surveyStatus, string programRoll, string name)
        {
            try
            {
                List<long> givenBranchIdList = null;
                if (branchId.Any() && !branchId.ToList().Contains(SelectionType.SelelectAll))
                {
                    givenBranchIdList = branchId.ToList();
                }
                List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu);
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu, authoOrganizationIdList, null, _commonHelper.ConvertIdToList(programId));
                List<long> authorizedBranchLists = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationIdList, authorizedProgramLists, givenBranchIdList);

                return _studentSurveyAnswerDao.LoadAuthorizedStudentsMakeSurveyDto(start, length, orderBy, orderDir, authorizedProgramLists, authorizedBranchLists, sessionId, campusId, batchDays, batchTime, batchId, courseId, surveyStatus, programRoll, name);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<StudentWiseSurveyReportDto> GetAuthorizedStudentWiseSurveyReportDto(List<UserMenu> userMenu, int start, int length, string orderBy, string orderDir, long organizationId, long programId, long sessionId, long[] branchIds, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, long[] courseIds, long[] questionIds, string[] answerNames, string programRoll, string name, string mobile)
        {
            try
            {
                List<long> givenBranchIdList = null;
                if (branchIds.Any() && !branchIds.ToList().Contains(SelectionType.SelelectAll))
                {
                    givenBranchIdList = branchIds.ToList();
                }
                List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu, authoOrganizationIdList, null, _commonHelper.ConvertIdToList(programId));
                List<long> authorizedBranchLists = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationIdList, authorizedProgramLists, givenBranchIdList);

                return _studentSurveyAnswerDao.LoadAuthorizedStudentWiseSurveyReportDto(start, length, orderBy, orderDir, authorizedProgramLists, authorizedBranchLists, sessionId, campusIds, batchDays, batchTimes, batchIds, courseIds, questionIds, answerNames, programRoll, name, mobile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<List<string>> LoadQuestionWiseSurvey(List<UserMenu> _userMenu, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, long[] questionId, string[] answerName)
        {
            try
            {
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(_userMenu, null, null, _commonHelper.ConvertIdToList(programId));//by menu
                var branchIds = new List<long>();
                branchIds.AddRange(branchId.ToList());
                if (branchIds.Contains(0))
                {
                    branchIds = null;
                }
                List<long> authorizedBranchLists = AuthHelper.LoadBranchIdList(_userMenu, null, _commonHelper.ConvertIdToList(programId), branchIds);//by menu 
                var questionList = _surveyQuestionDao.LoadQuestionWiseSurvey(start, length, questionId);
                var questionWiseSurveyList = new List<List<string>>();
                foreach (var question in questionList)
                {
                    string questionLine = question.Question;
                    string answers = "";
                    string ans = "";
                    long ansCount = 0;
                    if (question.StudentSurveyAnswers != null && question.StudentSurveyAnswers.Count > 0)
                    {
                        var surveyAnswer = _studentSurveyAnswerDao.LoadStudentSurveyAnswersByCriteria(sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, answerName, question, authorizedProgramLists, authorizedBranchLists).ToList();
                        if (surveyAnswer.Count > 0)
                        {
                            ans = "";
                            ansCount = 0;
                            foreach (var an in question.SurveyQuestionAnswers)
                            {
                                if (answerName.Contains("0") || answerName.Contains(an.Answer))
                                {
                                    ans = an.Answer;
                                    ansCount = surveyAnswer.Where(x => x.SurveyQuestionAnswer.Answer == ans).LongCount();
                                    answers += ans + ": " + ansCount.ToString() + "<br/>";
                                }
                            }
                        }
                        var questionAnswer = new List<string>() { questionLine, answers };
                        questionWiseSurveyList.Add(questionAnswer);
                    }

                }

                return questionWiseSurveyList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<StudentWiseSurveyQuestionAnswerDto> LoadStudentWiseSurveyQuestionAnswerDto(List<long> studentProgramIdList, List<long> surveyQuestionIdList)
        {
            try
            {
                return _studentSurveyAnswerDao.LoadStudentWiseSurveyQuestionAnswerDto(studentProgramIdList, surveyQuestionIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Other Functions

        public int GetAuthorizedStudentsCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int surveyStatus, string programRoll, string name)
        {
            try
            {
                List<long> givenBranchIdList = null;
                if (branchId.Any() && !branchId.ToList().Contains(SelectionType.SelelectAll))
                {
                    givenBranchIdList = branchId.ToList();
                }
                List<long> authoOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu);
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu, authoOrganizationIdList, null, _commonHelper.ConvertIdToList(programId));
                List<long> authorizedBranchLists = AuthHelper.LoadBranchIdList(userMenu, authoOrganizationIdList, authorizedProgramLists, givenBranchIdList);

                return _studentSurveyAnswerDao.GetAuthorizedStudentsCount(authorizedProgramLists, authorizedBranchLists, sessionId, campusId, batchDays, batchTime, batchId, courseId, surveyStatus, programRoll, name);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetAuthorizedStudentWiseSurveyReportCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, long[] questionId, string[] answerName, string programRoll, string nickName, string mobile)
        {
            try
            {
                List<long> givenBranchIdList = null;
                if (branchId.Any() && !branchId.ToList().Contains(SelectionType.SelelectAll))
                {
                    givenBranchIdList = branchId.ToList();
                }
                List<long> authorizedOrganizationIdLists = AuthHelper.LoadOrganizationIdList(userMenu);
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu, authorizedOrganizationIdLists, null, _commonHelper.ConvertIdToList(programId));
                List<long> authorizedBranchLists = AuthHelper.LoadBranchIdList(userMenu, authorizedOrganizationIdLists,authorizedProgramLists, givenBranchIdList);

                return _studentSurveyAnswerDao.GetAuthorizedStudentWiseSurveyReportCount(authorizedProgramLists, authorizedBranchLists, sessionId, campusId, batchDays, batchTime, batchId, courseId, questionId, answerName, programRoll, nickName, mobile);

                //return _studentSurveyAnswerDao.GetAuthorizedStudentWiseSurveyReportCount(authorizedProgramLists, authorizedBranchLists, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, questionId, answerName, programRoll, nickName, mobile);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int QuestionWiseSurveyCount(int start, int length, long[] questionIds)
        {
            try
            {
                return _surveyQuestionDao.GetQuestionWiseSurveyCount(start, length, questionIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        

        #endregion

        #region Helper Function

        #endregion
    }
}
