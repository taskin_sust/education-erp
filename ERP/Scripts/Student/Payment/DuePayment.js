﻿function StudentPaymentModel() {
    this.OfferedDiscount = 0;
    this.NetReceivable = 0;
    this.CashBackAmount = 0;
    this.ConsiderationAmount = 0;
    this.CourseFees = 0;
    this.DiscountAmount = 0;
    this.DueAmount = 0;
    this.NextReceivedDate = "";
    this.PayableAmount = 0;
    this.PaymentMethod = 0;
    this.PaymentType = 0;
    this.ReceiptNo = "";
    this.ReceivedAmount = 0;
    this.ReceivableAmount = 0;
    this.ReceivedDate = "";
    this.ReferrerId = 0;
    this.SpDiscountAmount = 0;
    this.Remarks = "";
    this.SpReferenceNote = "";
    this.StudentPaymentModel = function () { };
}

var MESSAGE_SHOW_INTERVAL = 10000;
function showErrorMessage(msg) {
    $("#messageContainer").append('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a><strong>Error!</strong> ' + msg + '.</div>');
    scrollToElement("#messageContainer");
    setTimeout(function () {
        $("#messageContainer").html("");
    }, MESSAGE_SHOW_INTERVAL);
}

$(document).ready(function() {
    
    validateTextField("referrerenceNote", "Please Enter Referance Note.");
    validateDropDownField("RefererList", "Please Select Referer.");

    function validateNewAdmissionPaymentForm() {

        var receivedAmountS = $('#receivedAmount').val();
        var receivedAmount = parseInt(receivedAmountS);
        var netReceivable = $("#netReceivable").val();

        if (isNaN(receivedAmount) || receivedAmount < 0) {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
            showErrorMessageBelowCtrl("receivedAmount", "Please enter valid amount", true);
            return false;
        }
        else if (receivedAmount > parseInt("0" + netReceivable)) {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
            showErrorMessageBelowCtrl("receivedAmount", "Received amount cannot be grater than Net Receiveable amount", true);
            return false;
        }
        else {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
        }

        var nextRecDate = $('#nextRecDate').val();
        var dueAmount = $('#dueAmount').val();

        if (nextRecDate.length <= 0 && parseInt("0" + dueAmount) > 0) {
            showErrorMessageBelowCtrl("nextRecDate", "", false);
            showErrorMessageBelowCtrl("nextRecDate", "Please select next payment date", true);
            return false;
        } else {
            showErrorMessageBelowCtrl("nextRecDate", "", false);
        }

        var spDiscount = $("#spDiscountAmount").val();
        var spDiscountI = parseInt("0" + spDiscount);

        if (spDiscountI > 0) {

            var referer = $("#RefererList").val();
            var refNote = $("#referrerenceNote").val();

            if (referer.length == 0) {
                showErrorMessageBelowCtrl("RefererList", "", false);
                showErrorMessageBelowCtrl("RefererList", "Please select referer", true);
                return false;
            } else {
                showErrorMessageBelowCtrl("RefererList", "", false);
            }

            if (refNote.length == 0) {
                showErrorMessageBelowCtrl("referrerenceNote", "", false);
                showErrorMessageBelowCtrl("referrerenceNote", "Please enter referer note", true);
                return false;
            } else {
                showErrorMessageBelowCtrl("referrerenceNote", "", false);
            }
        }

        return true;
    }

    function getAmount(receivable, spDiscount) {
        var val = parseFloat("0" + receivable) - parseFloat("0" + spDiscount);
        return val;
    }

    function getStudentPaymentJson() {

        var studentPayment = new StudentPaymentModel();

        //studentPayment.CourseFee = parseInt("0" + $("#totalCourseFee").val());
        studentPayment.OfferedDiscount = parseInt("0" + $("#offeredDiscount").val());
        studentPayment.ReceivableAmount = parseInt("0" + $("#receivableAmount").val());
        studentPayment.SpDiscountAmount = parseInt("0" + $("#spDiscountAmount").val());
        studentPayment.NetReceivable = parseInt("0" + $("#netReceivable").val());

        studentPayment.ReceivedAmount = parseInt("0" + $("#receivedAmount").val());
        studentPayment.DueAmount = parseInt("0" + $("#dueAmount").val());

        studentPayment.ReferrerId = parseInt("0" + $("#RefererList").val());
        studentPayment.SpReferenceNote = $("#referrerenceNote").val();
        studentPayment.Remarks = $("#referrerenceNote").val();
        studentPayment.PaymentMethod = parseInt("0" + $("#PaymentMethods").val());
        studentPayment.NextReceivedDate = $("#nextRecDate").val();

        console.log(studentPayment);
        var studentPaymentObj = JSON.stringify(studentPayment);

        return studentPaymentObj;
    }

    $(document).on("change", "#nextRecDate", function (event) {
        var val = $("#nextRecDate").val();

        console.log(val);
        if (val.length > 0) {
            showErrorMessageBelowCtrl("nextRecDate", "", false);
        }
    });

    $(document).on("input propertychange", "#receivedAmount", function (event) {

        var receivableAmount = $('#receivableAmount').val();
        var netReceivable = $('#netReceivable').val();
        var receivedAmount = $('#receivedAmount').val();
        var received = $("#receivedAmount").val();
        var receivedI = parseInt("" + received);
        var netReceivableI = parseInt("" + netReceivable);

        if (receivedI >= 0) {
            $("#duePaymentSubmitBtn").removeAttr("disabled");
        } else {
            $("#duePaymentSubmitBtn").attr("disabled", "disabled");
        }

        if (receivedI >= netReceivable) {
            $('#spDiscountAmount').attr("disabled", "disabled");
        } else {
            $('#spDiscountAmount').removeAttr("disabled");
        }

        if (receivedI > netReceivableI) {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
            showErrorMessageBelowCtrl("receivedAmount", "Received amount can not be grater than Net Receiveable amount", true);
            return false;
        } else {
            showErrorMessageBelowCtrl("receivedAmount", "", false);
        }

        if (receivableAmount.length > 0 && netReceivable.length > 0) {

            if (receivedAmount.length > 0) {
                var dAmount = getAmount(netReceivable, receivedAmount);
                $('#dueAmount').val(dAmount);
            } else {
                $('#dueAmount').val(netReceivable);
            }

            console.log("netReceivable: " + netReceivable);
        }
    });

    $(document).on("input propertychange", "#spDiscountAmount", function (event) {

        var receivableAmount = $('#receivableAmount').val();
        var netReceivable = $('#netReceivable').val();
        var receivedAmount = $('#receivedAmount').val().trim();
        var spDiscountAmount = $('#spDiscountAmount').val();
        var dAmount = 0;

        if (receivableAmount.length > 0 && netReceivable.length > 0) {

            var sDis = parseFloat("0" + spDiscountAmount);
            var receiveableAmountF = parseFloat("0" + receivableAmount);
            var receivedAmountI = parseInt("0" + receivedAmount);
            var netReceivableI = parseInt("0" + netReceivable);

            if (sDis > 0) {

                $('#RefererList').removeAttr('disabled');
                $('#referrerenceNote').removeAttr('disabled');

                showErrorMessageBelowCtrl('spDiscountAmount', '', false);

                if (sDis >= 0 && sDis <= receiveableAmountF) {
                    netReceivable = getAmount(receivableAmount, spDiscountAmount);
                    $('#netReceivable').val(netReceivable);
                    dAmount = getAmount(netReceivable, receivedAmount);
                    if (dAmount < 0) {
                        showErrorMessageBelowCtrl("dueAmount", "", false);
                        showErrorMessageBelowCtrl("dueAmount", "Due amount can not be negative.", true);
                    } else {
                        showErrorMessageBelowCtrl("dueAmount", "", false);
                    }
                    $('#dueAmount').val(dAmount);
                } else {
                    showErrorMessageBelowCtrl('spDiscountAmount', 'Special discount can not be negative or higher than receiveable amount.', true);
                }

                if (sDis == receiveableAmountF) {
                    $("#duePaymentSubmitBtn").removeAttr("disabled");
                }

            } else {
                showErrorMessageBelowCtrl('spDiscountAmount', '', false);
                showErrorMessageBelowCtrl("dueAmount", "", false);
                $('#netReceivable').val(receivableAmount);
                receivedAmount = $('#receivedAmount').val();
                netReceivable = $('#netReceivable').val();
                var due = getAmount(netReceivable, receivedAmount);
                $('#dueAmount').val(due);

                $('#RefererList').prop('disabled', true);
                $('#referrerenceNote').prop('disabled', true);
            }
        }
    });

    $(document).on('click', '#duePaymentSubmitBtn', function () {

        if (validateNewAdmissionPaymentForm()) {

            console.log("Submited");
            $("#duePaymentSubmitBtn").attr("disabled", "disabled");

            var studentPayment = getStudentPaymentJson();
            var studentProgramId = $("#stdPId").val();
            console.log(studentPayment);

            $.ajax({
                url: "/Student/Payment/PaymentPaidAndMoneyReceiptGeneration",
                type: "POST",
                data: { studentPaymentJson: studentPayment, stdPId:studentProgramId},
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    console.log(data);
                    $.unblockUI();

                    if (data.IsSuccess) {
                        window.location = "/Student/Payment/GenerateMoneyReciept?id=" + data.AdditionalValue;
                    } else {
                        showErrorMessage(data.Message);
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.unblockUI();
                    //bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
                    showErrorMessage(data.Message);
                }
            });
        }
    });
    
});