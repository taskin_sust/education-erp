﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ShiftTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Shift")]
    public class ShiftSaveTest : ShiftBaseTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _shiftService.Save(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var zoneFactory =
                _shiftFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .WithStartTimeEndTime(_startTime, _endTime)
                    .Persist();
            Shift shift = _shiftService.LoadById(zoneFactory.Object.Id);
            Assert.NotNull(shift);
            Assert.True(shift.Id > 0);
        }
        
        [Fact]
        public void Save_Should_Check_Duplicate_Entry()
        {
            var factoryObj1 =
                _shiftFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .WithStartTimeEndTime(_startTime, _endTime)
                    .Persist();
            var organization = factoryObj1.Object.Organization;
            var factoryObj2 =
                _shiftFactory.Create()
                    .WithOrganization(organization)
                    .WithNameAndRank(_name, 1)
                    .WithStartTimeEndTime(_startTime, _endTime);
            Assert.Throws<DuplicateEntryException>(() => _shiftService.Save(factoryObj2.Object));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Not_Save_Empty_Organization()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithNameAndRank(_name, 1);
            Assert.Throws<EmptyFieldException>(() => _shiftService.Save(shiftFactory.Object));
        }

        [Fact]
        public void Should_Not_Save_Empty_Name()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithOrganization().WithNameAndRank("", 1);
            Assert.Throws<EmptyFieldException>(() => _shiftService.Save(shiftFactory.Object));
        }


        [Fact]
        public void Should_Not_Save_StartTime_Null()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithOrganization()
                                        .WithNameAndRank(_name, 1)
                                        .WithStartTimeEndTime(_startTime, _endTime);
            shiftFactory.Object.StartTime = null;
            Assert.Throws<EmptyFieldException>(() => _shiftService.Save(shiftFactory.Object));
        }

        [Fact]
        public void Should_Not_Save_EndTime_Null()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithOrganization()
                                        .WithNameAndRank(_name, 1)
                                        .WithStartTimeEndTime(_startTime, _endTime);
            shiftFactory.Object.EndTime = null;
            Assert.Throws<EmptyFieldException>(() => _shiftService.Save(shiftFactory.Object));
        }
        [Fact]
        public void Should_Not_Save_StartTime_EndTime_Diff_Invalid()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithOrganization()
                                        .WithNameAndRank(_name, 1);
            shiftFactory.Object.StartTime = Convert.ToDateTime("2001-01-01 09:00:00");
            shiftFactory.Object.EndTime = Convert.ToDateTime("2001-01-01 09:00:00");
            Assert.Throws<InvalidDataException>(() => _shiftService.Save(shiftFactory.Object));
        }
        [Fact]
        public void Should_Not_Save_StartTime_EndTime_Order_Invalid()
        {
            ShiftFactory shiftFactory = _shiftFactory.Create().WithOrganization()
                                        .WithNameAndRank(_name, 1)
                                        .WithStartTimeEndTime(_startTime, _endTime);
            shiftFactory.Object.EndTime = Convert.ToDateTime("2001-01-01 09:00:00").AddHours(0).AddMinutes(0);
            Assert.Throws<EmptyFieldException>(() => _shiftService.Save(shiftFactory.Object));
        }
        

        #endregion
    }
}
