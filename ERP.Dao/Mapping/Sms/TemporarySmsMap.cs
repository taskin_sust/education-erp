﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using NHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Sms;

namespace UdvashERP.Dao.Mapping.Sms
{
    public class TemporarySmsMap : BaseClassMap<TemporarySms, long>
        //: ClassMap<TemporarySms> 
    {
        public TemporarySmsMap()
        {
            //Id(x => x.Id).GeneratedBy.Identity().Column("Id"); 
            Table("TemporarySms");
            LazyLoad();

            //Map(x => x.Status);
            Map(x => x.ExamType);
            References(x => x.Student).Column("StudentId");
            References(x => x.StudentProgram).Column("StudentProgramId");;
            References(x => x.Exams).Column("ExamId"); 
            
        }
    }
}
