﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Teachers;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Teachers.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Teachers")]
    public class TeacherClassOpeningBalanceController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("TeachersArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly ITeacherService _teacherService;
        private readonly IUserService _userService;
        private readonly ITeacherClassOpeningBalanceService _teacherClassOpeningBalanceService;
        private List<UserMenu> _userMenu;

        public TeacherClassOpeningBalanceController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _organizationService = new OrganizationService(session);
                _teacherService = new TeacherService(session);
                _userService = new UserService(session);
                _teacherClassOpeningBalanceService = new TeacherClassOpeningBalanceService(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        #endregion

        #region Index/Manage

        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region Opening Balance

        public ActionResult OpeningBalance()
        {
            ViewBag.OrganizationList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.IsPost = false;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult OpeningBalance(TeacherClassOpeningBalanceFormViewModel openingBalanceForm)
        {
            ViewBag.IsPost = true;
            ViewBag.OrganizationList = new SelectList(string.Empty, "Value", "Text");
            List<TeacherClassOpeningBalanceDto> teacherClassOpeningBalanceDtoList = new List<TeacherClassOpeningBalanceDto>();
            ViewBag.TeacherClassOpeningBalanceDtoList = teacherClassOpeningBalanceDtoList;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).ToList(), "Id", "ShortName", openingBalanceForm.OrganizationId);
                if (ModelState.IsValid)
                {
                    string pinList = (!String.IsNullOrEmpty(openingBalanceForm.TpinList)) ? openingBalanceForm.TpinList.Trim() : "";
                    Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    pinList = regex.Replace(pinList, @",");
                    teacherClassOpeningBalanceDtoList = _teacherClassOpeningBalanceService.LoadTeacherClassOpeningBalance(_userMenu, _commonHelper.ConvertIdToList(openingBalanceForm.OrganizationId), pinList).ToList();
                    ViewBag.TeacherClassOpeningBalanceDtoList = teacherClassOpeningBalanceDtoList;
                }
                else
                {
                    ViewBag.ErrorMessage = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult SaveOpeningBalance(List<TeacherClassOpeningBalanceViewModel> teacherClassOpeningBalanceViewModelList)
        {
            string message = "";
            bool successState = false;
            int count = 0;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<TeacherClassOpeningBalance> teacherClassOpeningBalanceList = new List<TeacherClassOpeningBalance>();
                if (teacherClassOpeningBalanceViewModelList.Any())
                {
                    foreach (TeacherClassOpeningBalanceViewModel openingBalanceView in teacherClassOpeningBalanceViewModelList)
                    {
                        TeacherClassOpeningBalance teacherClassOpeningBalance = new TeacherClassOpeningBalance();
                        if (openingBalanceView.TeacherClassOpeningBalanceId != 0)
                        {
                            teacherClassOpeningBalance = _teacherClassOpeningBalanceService.GetTeacherClassOpeningBalance(openingBalanceView.TeacherClassOpeningBalanceId);
                            if (teacherClassOpeningBalance != null)
                            {
                                openingBalanceView.GetModelFromViewModel(teacherClassOpeningBalance);
                                teacherClassOpeningBalanceList.Add(teacherClassOpeningBalance);
                                count++;
                            }
                        }
                        else
                        {
                            openingBalanceView.GetModelFromViewModel(teacherClassOpeningBalance);
                            teacherClassOpeningBalance.Teacher = _teacherService.GetTeacher(openingBalanceView.TeacherId);
                            teacherClassOpeningBalance.Organization = _organizationService.LoadById(openingBalanceView.OrganizationId);
                            teacherClassOpeningBalance.Status = TeacherClassOpeningBalance.EntityStatus.Active;
                            teacherClassOpeningBalanceList.Add(teacherClassOpeningBalance);
                                count++;
                        }
                    }
                    _teacherClassOpeningBalanceService.SaveOrUpdateTeacherClassOpeningBalance(teacherClassOpeningBalanceList);
                    message = "Total " + count + " Teacher(s) Class Opening Balance Save Successfully.";
                    successState = true;
                }
                else
                {
                    throw new InvalidDataException("No Teacher Class Opening Balance Data found to save.");
                }

            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new { IsSuccess = successState, Message = message });

        }

        #endregion

        #region Details Operation
        #endregion

        #region Delete Operation
        #endregion

        #region Update Operation
        #endregion

    }
}