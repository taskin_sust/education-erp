﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using InvalidDataException = System.IO.InvalidDataException;

namespace UdvashERP.Services.Hr
{
    public interface ISalaryHistoryService : IBaseService
    {
        #region Operational Function

        bool Save(SalaryHistory hrSalaryHistory);
        void SaveOrUpdateSalaryHistory(SalaryHistory salaryHistory);
        void SaveBatchSalaryHistory(List<SalaryHistory> teamMemberSalaryHistoryList);
        void DeleteSalaryHistory(SalaryHistory salaryHistory);

        #endregion

        #region Single Instances Loading Function

        SalaryHistory GetSalaryHistory(long? id);

        #endregion

        #region List Loading Function

        List<SalaryHistory> LoadSalaryHistories(TeamMember member, DateTime? searchingDate = null);

        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion

    }
    public class SalaryHistoryService : BaseService, ISalaryHistoryService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Properties & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly ISalaryHistoryDao _hrSalaryHistoryDao;
        private readonly ICampusDao _campusDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly IEmploymentHistoryDao _hrEmploymentHistoryDao;
        private readonly ISalarySheetDao _salarySheetDao;

        public SalaryHistoryService(ISession session)
        {
            Session = session;
            _hrSalaryHistoryDao = new SalaryHistoryDao { Session = session };
            _commonHelper = new CommonHelper();
            _campusDao = new CampusDao { Session = session };
            _organizationDao = new OrganizationDao { Session = session };
            _hrEmploymentHistoryDao = new EmploymentHistoryDao { Session = session };
            _salarySheetDao = new SalarySheetDao { Session = session };
        }
        #endregion

        #region Operational Functions

        public void DeleteSalaryHistory(SalaryHistory salaryHistory)
        {
            ITransaction trans = null;
            try
            {
                if (salaryHistory == null)
                    throw new MessageExceptions.InvalidDataException("Invalid salary history");

                CheckBeforeDelete(salaryHistory);
                using (trans = Session.BeginTransaction())
                {
                    salaryHistory.Status = -404;
                    _hrSalaryHistoryDao.SaveOrUpdate(salaryHistory);
                    SaveSalaryHistoryLog(salaryHistory);
                    trans.Commit();
                }
            }
            catch (MessageExceptions.InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        public bool Save(SalaryHistory hrSalaryHistory)
        {
            ITransaction trans = null;
            try
            {
                if (hrSalaryHistory == null)
                    throw new NullObjectException("Invalid salary history");

                if (hrSalaryHistory.TeamMember == null)
                    throw new NullObjectException("Invalid teamMember");

                if (hrSalaryHistory.SalaryCampus == null)
                    throw new NullObjectException("Invalid salary campus");

                if (hrSalaryHistory.SalaryOrganization == null)
                    throw new NullObjectException("Invalid salary organization");

                if (hrSalaryHistory.SalaryDepartment == null)
                    throw new NullObjectException("Invalid salary department");
                if (hrSalaryHistory.SalaryDesignation == null)
                    throw new NullObjectException("Invalid salary designation");

                using (trans = Session.BeginTransaction())
                {

                    _hrSalaryHistoryDao.Save(hrSalaryHistory);
                    var hrSalaryHistoryLog = new SalaryHistoryLog();
                    hrSalaryHistoryLog.EffectiveDate = hrSalaryHistory.EffectiveDate;
                    //hrEmploymentHistoryLog.LeaveDate = Convert.ToDateTime(dateTo);
                    hrSalaryHistoryLog.CampusId = hrSalaryHistory.SalaryCampus.Id;
                    hrSalaryHistoryLog.Salary = hrSalaryHistory.Salary;
                    hrSalaryHistoryLog.BankAmount = hrSalaryHistory.BankAmount;
                    hrSalaryHistoryLog.CashAmount = hrSalaryHistory.CashAmount;
                    hrSalaryHistoryLog.TeamMemberId = hrSalaryHistory.TeamMember.Id;
                    hrSalaryHistoryLog.SalaryHistoryId = hrSalaryHistory.Id;
                    hrSalaryHistoryLog.OrganizationId = hrSalaryHistory.SalaryOrganization.Id;
                    hrSalaryHistoryLog.DepartmentId = hrSalaryHistory.SalaryDepartment.Id;
                    hrSalaryHistoryLog.DesignationId = hrSalaryHistory.SalaryDesignation.Id;
                    hrSalaryHistoryLog.CreationDate = DateTime.Now;
                    hrSalaryHistoryLog.CreateBy = GetCurrentUserId();
                    Session.Save(hrSalaryHistoryLog);
                    //save hrmember history log
                    trans.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (NullReferenceException ex)
            {
                _logger.Error(ex);
                throw ex;
            }

            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw ex;
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public void SaveOrUpdateSalaryHistory(SalaryHistory salaryHistory)
        {
            ITransaction trans = null;
            try
            {
              //  Session.Evict(salaryHistory);
                CheckBeforeAdd(salaryHistory);

                #region Assign chnages into old obj

                  SalaryHistory oldSalaryHistory = new SalaryHistory();
                if (salaryHistory.Id > 0)
                {
                    oldSalaryHistory = _hrSalaryHistoryDao.LoadById(salaryHistory.Id);
                    oldSalaryHistory.SalaryOrganization = salaryHistory.SalaryOrganization;
                    oldSalaryHistory.SalaryDepartment = salaryHistory.SalaryDepartment;
                    oldSalaryHistory.SalaryDesignation = salaryHistory.SalaryDesignation;
                    oldSalaryHistory.SalaryCampus = salaryHistory.SalaryCampus;
                    oldSalaryHistory.SalaryPurpose = salaryHistory.SalaryPurpose;
                    oldSalaryHistory.Salary = salaryHistory.Salary;
                    oldSalaryHistory.BankAmount = salaryHistory.BankAmount;
                    oldSalaryHistory.CashAmount = salaryHistory.CashAmount;
                    oldSalaryHistory.EffectiveDate = salaryHistory.EffectiveDate;
                }
                else
                {
                    oldSalaryHistory = salaryHistory;
                }

                #endregion

                using (trans = Session.BeginTransaction())
                {
                    _hrSalaryHistoryDao.SaveOrUpdate(oldSalaryHistory);
                    SaveSalaryHistoryLog(oldSalaryHistory);
                    trans.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        public void SaveBatchSalaryHistory(List<SalaryHistory> teamMemberSalaryHistoryList)
        {
            ITransaction trans = null;
            try
            {
                List<SalaryHistory> validSalaryHistoryList = new List<SalaryHistory>();
                foreach (SalaryHistory salaryHistory in teamMemberSalaryHistoryList)
                {
                    Session.Evict(salaryHistory);
                    
                    CheckBeforeAdd(salaryHistory, true);
                    validSalaryHistoryList.Add(salaryHistory);
                }
                if (!validSalaryHistoryList.Any())
                {
                    throw new InvalidDataException("No Valid TeamMember's Salary History Found!");
                }
                using (trans = Session.BeginTransaction())
                {
                    foreach (SalaryHistory salaryHistory in validSalaryHistoryList)
                    {
                        _hrSalaryHistoryDao.SaveOrUpdate(salaryHistory);
                        SaveSalaryHistoryLog(salaryHistory);
                    }
                    trans.Commit();
                }
            }
            catch (MessageExceptions.InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public SalaryHistory GetSalaryHistory(long? id)
        {
            try
            {
                return Session.QueryOver<SalaryHistory>().Where(x => x.Id == id).SingleOrDefault<SalaryHistory>();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public List<SalaryHistory> LoadSalaryHistories(TeamMember member, DateTime? searchingDate = null)
        {
            try
            {
                if (member == null)
                    throw new MessageExceptions.InvalidDataException("member can't empty");
                return _hrSalaryHistoryDao.LoadSalaryHistories(member, searchingDate).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Others Function
        #endregion

        #region Helper function

        private void SaveSalaryHistoryLog(SalaryHistory salaryHistory)
        {
            try
            {
                var hrSalaryHistoryLog = new SalaryHistoryLog();
                hrSalaryHistoryLog.SalaryHistoryId = salaryHistory.Id;
                hrSalaryHistoryLog.TeamMemberId = salaryHistory.TeamMember.Id;
                hrSalaryHistoryLog.EffectiveDate = salaryHistory.EffectiveDate;
                hrSalaryHistoryLog.CampusId = salaryHistory.SalaryCampus.Id;
                hrSalaryHistoryLog.OrganizationId = salaryHistory.SalaryOrganization.Id;
                hrSalaryHistoryLog.DepartmentId = salaryHistory.SalaryDepartment.Id;
                hrSalaryHistoryLog.DesignationId = salaryHistory.SalaryDesignation.Id;
                hrSalaryHistoryLog.SalaryPurpose = salaryHistory.SalaryPurpose != null ? (int)salaryHistory.SalaryPurpose : (int?)null;
                hrSalaryHistoryLog.Salary = salaryHistory.Salary;
                hrSalaryHistoryLog.BankAmount = salaryHistory.BankAmount;
                hrSalaryHistoryLog.CashAmount = salaryHistory.CashAmount;
                hrSalaryHistoryLog.CreationDate = DateTime.Now;
                hrSalaryHistoryLog.Status = salaryHistory.Status;
                hrSalaryHistoryLog.CreateBy = GetCurrentUserId();
                Session.Save(hrSalaryHistoryLog);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CheckBeforeAdd(SalaryHistory salaryHistory, bool isPinWithMessage = false)
        {
            string concatingErrorMessage = ".";
            if (isPinWithMessage)
                concatingErrorMessage = " for PIN " + salaryHistory.TeamMember.Pin + ".";
            decimal number;
            DateTime dateCheck;
            if (salaryHistory == null)
                throw new MessageExceptions.InvalidDataException("Invalid Salary History" + concatingErrorMessage);
            if (salaryHistory.TeamMember == null)
                throw new MessageExceptions.InvalidDataException("Invalid Team Member" + concatingErrorMessage);
            if (salaryHistory.SalaryOrganization == null)
                throw new MessageExceptions.InvalidDataException("Invalid Organization" + concatingErrorMessage);
            if (salaryHistory.SalaryCampus == null)
                throw new MessageExceptions.InvalidDataException("Invalid Campus" + concatingErrorMessage);
            if (salaryHistory.SalaryDepartment == null || (salaryHistory.SalaryDepartment != null && salaryHistory.SalaryDepartment.Organization.Id != salaryHistory.SalaryOrganization.Id))
                throw new MessageExceptions.InvalidDataException("Invalid Department" + concatingErrorMessage);

            if (salaryHistory.SalaryDesignation == null || (salaryHistory.SalaryDesignation != null && salaryHistory.SalaryDesignation.Organization.Id != salaryHistory.SalaryOrganization.Id))
                throw new MessageExceptions.InvalidDataException("Invalid Designation" + concatingErrorMessage);

            if (salaryHistory.Salary == null || (salaryHistory.Salary != null && !Decimal.TryParse(salaryHistory.Salary.ToString(), out number)))
                throw new MessageExceptions.InvalidDataException("Invalid Salary input" + concatingErrorMessage);
            if (salaryHistory.CashAmount == null || (salaryHistory.CashAmount != null && !Decimal.TryParse(salaryHistory.CashAmount.ToString(), out number)))
                throw new MessageExceptions.InvalidDataException("Invalid Cash Amount input" + concatingErrorMessage);
            if (salaryHistory.BankAmount == null || (salaryHistory.BankAmount != null && !Decimal.TryParse(salaryHistory.BankAmount.ToString(), out number)))
                throw new MessageExceptions.InvalidDataException("Invalid Bank Amount input" + concatingErrorMessage);
            if (salaryHistory.EffectiveDate == null || (salaryHistory.EffectiveDate != null && !DateTime.TryParse(salaryHistory.EffectiveDate.ToString(), out dateCheck)))
                throw new MessageExceptions.InvalidDataException("Invalid Effective date input" + concatingErrorMessage);
            if (salaryHistory.Salary != salaryHistory.CashAmount + salaryHistory.BankAmount)
                throw new MessageExceptions.InvalidDataException("Salary amount must be equal to cash and bank salary amount" + concatingErrorMessage);
            List<int> salaryPurposeIdList = _commonHelper.LoadEmumToDictionary<SalaryPurpose>().Select(x => x.Key).ToList();
            if (salaryHistory.SalaryPurpose != null && !salaryPurposeIdList.Contains((int)salaryHistory.SalaryPurpose))
                throw new MessageExceptions.InvalidDataException("Invalid Salary Purpose selected" + concatingErrorMessage);

            EmploymentHistory teamMemberJoiningEmploymentHistory = _hrEmploymentHistoryDao.GetTeamMemberJoiningEmploymentHistory(salaryHistory.TeamMember.Id);
            if (teamMemberJoiningEmploymentHistory == null)
                throw new MessageExceptions.InvalidDataException("This TeamMember has no Employement History" + concatingErrorMessage);
            if (teamMemberJoiningEmploymentHistory.EffectiveDate.Date > salaryHistory.EffectiveDate.Value.Date)
                throw new MessageExceptions.InvalidDataException("This TeamMember has not joined yet" + concatingErrorMessage);

            SalarySheet salarySheet = _salarySheetDao.GetTeamMemberLastSalarySheet(salaryHistory.TeamMember.Id, null);
            // for edit check
            if (salaryHistory.Id > 0 && salarySheet != null)
            {
                SalaryHistory originalSalaryHistory = _hrSalaryHistoryDao.LoadById(salaryHistory.Id);
                    //check with original object
                if (originalSalaryHistory == null)
                    throw new MessageExceptions.InvalidDataException("Invalid Salary History found" + concatingErrorMessage);
                if (originalSalaryHistory.EffectiveDate.Value.Date <= salarySheet.EndDate.Date || salarySheet.EndDate.Date >= salaryHistory.EffectiveDate.Value.Date)
                    throw new MessageExceptions.InvalidDataException("Team Member's Salary history can edit only after Date(" + salarySheet.EndDate.Date.ToString("d MMM, yyyy") + ")" + concatingErrorMessage);
            }
            else if(salarySheet!=null && salaryHistory.Id == 0)
            {
                if (salarySheet.EndDate.Date >= salaryHistory.EffectiveDate.Value.Date)
                    throw new MessageExceptions.InvalidDataException("Team Member's Salary history can add only after Date(" + salarySheet.EndDate.Date.ToString("d MMM, yyyy") + ")" + concatingErrorMessage);
            }

            SalaryHistory teamMemberLastSalaryHistory = _hrSalaryHistoryDao.GetTeamMemberLastSalaryHistory(salaryHistory.TeamMember.Id);
            if (teamMemberLastSalaryHistory != null && teamMemberLastSalaryHistory.EffectiveDate.Value.Date >= salaryHistory.EffectiveDate.Value.Date)
                throw new MessageExceptions.InvalidDataException("This TeamMember has already a Salary History for this Date (" + teamMemberLastSalaryHistory.EffectiveDate.Value.Date.ToString("d MMM, yyyy") + ") Range" + concatingErrorMessage);

            //SalaryHistory teamMemberInitialSalaryHistory = _hrSalaryHistoryDao.GetTeamMemberJoiningSalaryHistory(salaryHistory.TeamMember.Id);
            //if (teamMemberInitialSalaryHistory != null && teamMemberInitialSalaryHistory.EffectiveDate.Value.Date >= salaryHistory.EffectiveDate.Value.Date)
            //    throw new MessageExceptions.InvalidDataException("This TeamMember has already a Salary History for this Date Range" + concatingErrorMessage);
        }

        private void CheckBeforeDelete(SalaryHistory salaryHistory)
        {
            TeamMember teamMember = salaryHistory.TeamMember;
            if (teamMember == null)
                throw new MessageExceptions.InvalidDataException("Invalid team member.");

            if (teamMember.SalaryHistory == null)
                throw new MessageExceptions.InvalidDataException("Invalid salary history of team member.");

            SalarySheet salarySheet = _salarySheetDao.GetTeamMemberLastSalarySheet(salaryHistory.TeamMember.Id, null);
            if (salarySheet != null && salarySheet.EndDate >= salaryHistory.EffectiveDate.Value.Date)
                    throw new MessageExceptions.InvalidDataException("Team Member's Salary history can add only be deleted after Date(" + salarySheet.EndDate.Date.ToString("d MMM, yyyy") + ")");

            int noOfSalaryHistory = teamMember.SalaryHistory.Where(x => x.Status != SalaryHistory.EntityStatus.Delete).ToList().Count;
            if (noOfSalaryHistory <= 1)
            {
                throw new DependencyException("Team member must have atleast one Salary history!!");
            }
        }

        #endregion
    }
}
