﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.AdminisTration.MenuGroupTest
{
    public class MenuGroupBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal IMenuService _menuService;
        internal MenuGroupFactory _menugroupFactory;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal readonly string _displaytext = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN)";
        #endregion

        public MenuGroupBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _menugroupFactory = new MenuGroupFactory(null, _session);
            _menuService = new MenuService(_session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _menugroupFactory.Cleanup();
            base.Dispose();
        }
    }
}
