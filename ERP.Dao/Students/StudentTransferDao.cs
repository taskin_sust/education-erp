﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Dto.Students;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentTransferDao : IBaseDao<StudentTransfer, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<StudentTransfer> LoadStudentTransferByCriteria(int start, int length, long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string prnNo = "", string name = "", string mobile = "", DateTime? dateFrom = null, DateTime? dateTo = null);
        IList<StudentTransfer> LoadStudentTransferByCriteriaCount(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string prnNo = "", string name = "", string mobile = "", DateTime? dateFrom = null, DateTime? dateTo = null);
        IList<StudentTransfer> LoadStudentTransferSettleByCriteria
            (int start, int length, long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string dateFrom, string dateTo, string prnNo, string name, string mobile);
        IList<StudentTransfer> LoadStudentTransferSettleByCriteriaCount(long programId, long sessionId,
            long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string dateFrom, string dateTo, string prnNo = "", string name = "", string mobile = "");
        #endregion

        #region Others Function

        #endregion



        decimal GetTotalPayable(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo);

        decimal GetTotalReceivable(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo);

        StudentTransfer GetPreviousTransferInfo(long studentProgramId);

        IList<StudentTransferReportDto> LoadStudentTransferReport(int start, int length, long programId, long sessionId, long[] fromBranchIds, long[] selectedFromCampus, string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo);

        int LoadStudentTransferReportCount(long programId, long sessionId, long[] fromBranchIds, long[] campusIds, string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo);

        void GetOvetAllPayableReceivable(long programId, long sessionId, long[] fromBranchIds, long[] campusIds, string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo, out decimal overallPayable, out decimal overallReceivable);
    }

    public class StudentTransferDao : BaseDao<StudentTransfer, long>, IStudentTransferDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<StudentTransfer> LoadStudentTransferByCriteria(int start, int length, long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string prnNo = "", string name = "", string mobile = "", DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            ICriteria criteria = Session.CreateCriteria<StudentTransfer>();

            criteria.CreateCriteria("ToBatch", "tb");
            criteria.CreateCriteria("FromBatch", "fb");
            if (programId != SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("tb.Program.Id", programId));
                criteria.Add(Restrictions.Eq("fb.Program.Id", programId));
            }
            if (sessionId != SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("tb.Session.Id", sessionId));
                criteria.Add(Restrictions.Eq("fb.Session.Id", sessionId));
            }
            criteria.CreateCriteria("StudentProgram", "sp");
            criteria.CreateAlias("sp.Student", "st");
            if (dateFrom != null)
            {
                criteria.Add(Restrictions.Ge("TransferDate", dateFrom.Value.Date));
            }
            if (dateTo != null)
            {
                criteria.Add(Restrictions.Le("TransferDate", dateTo.Value.AddDays(1).Date));
            }


            if (selectedToBranch == null && selectedToCampus == null)
            {
                if (!(Array.Exists(selectedFromBranch, item => item == SelectionType.SelelectAll)))
                {

                    criteria.CreateAlias("fb.Branch", "fbr");
                    criteria.CreateAlias("tb.Branch", "tbr");
                    criteria.Add(Restrictions.Or(
                        Restrictions.In("fbr.Id", selectedFromBranch),
                        Restrictions.In("tbr.Id", selectedFromBranch)));
                }
                if (!(Array.Exists(selectedFromCampus, item => item == SelectionType.SelelectAll)))
                {

                    criteria.CreateAlias("fb.Campus", "fc");
                    criteria.CreateAlias("tb.Campus", "tc");
                    criteria.Add(Restrictions.Or(
                        Restrictions.In("fc.Id", selectedFromCampus),
                        Restrictions.In("tc.Id", selectedFromCampus)));
                }
            }
            else
            {
                if (!(Array.Exists(selectedFromBranch, item => item == SelectionType.SelelectAll)))
                {
                    criteria = criteria.CreateCriteria("fb.Branch", "fbr").Add(Restrictions.In("fbr.Id", selectedFromBranch));
                }
                if (!(Array.Exists(selectedFromCampus, item => item == SelectionType.SelelectAll)))
                {
                    criteria = criteria.CreateCriteria("fb.Campus", "fc").Add(Restrictions.In("fc.Id", selectedFromCampus));
                }

                if (!(Array.Exists(selectedToBranch, item => item == SelectionType.SelelectAll)))
                {
                    criteria = criteria.CreateCriteria("tb.Branch", "tbr").Add(Restrictions.In("tbr.Id", selectedToBranch));
                }
                if (!(Array.Exists(selectedToCampus, item => item == SelectionType.SelelectAll)))
                {
                    criteria = criteria.CreateCriteria("tb.Campus", "tc").Add(Restrictions.In("tc.Id", selectedToCampus));
                }
            }
            if (!String.IsNullOrEmpty(prnNo))
            {
                criteria.Add(Restrictions.Like("sp.PrnNo", prnNo, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("st.NickName", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                criteria.Add(Restrictions.Like("st.Mobile", mobile, MatchMode.Anywhere));
            }
            criteria.AddOrder(Order.Asc("sp.PrnNo"));
            IList<StudentTransfer> sTransferList = criteria.SetFirstResult(start).SetMaxResults(length).List<StudentTransfer>();

            return sTransferList;
        }

        public IList<StudentTransfer> LoadStudentTransferByCriteriaCount(long programId, long sessionId, long[] selectedFromBranch,
            long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string prnNo = "", string name = "",
            string mobile = "", DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var criteria = LoadStudentTransferByCriteria(programId, sessionId, selectedFromBranch, selectedFromCampus, selectedToBranch, selectedToCampus, prnNo, name, mobile, dateFrom, dateTo);
            IList<StudentTransfer> sTransferList = criteria.List<StudentTransfer>();
            return sTransferList;
        }


        public IList<StudentTransfer> LoadStudentTransferSettleByCriteria(int start, int length, long programId, long sessionId,
            long[] selectedFromBranch, long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus,
            string dateFrom, string dateTo, string prnNo, string name, string mobile)
        {
            ICriteria criteria = Session.CreateCriteria<StudentTransfer>();

            criteria.CreateCriteria("ToBatch", "tb").Add(Restrictions.Eq("tb.Session.Id", sessionId)).Add(Restrictions.Eq("tb.Program.Id", programId));
            criteria.CreateCriteria("FromBatch", "fb").Add(Restrictions.Eq("fb.Session.Id", sessionId)).Add(Restrictions.Eq("fb.Program.Id", programId));
            criteria.CreateCriteria("StudentProgram", "sp");
            criteria.CreateAlias("sp.Student", "st");
            if (!String.IsNullOrEmpty(dateFrom))
            {
                string df = dateFrom + " 00:00:00.000";
                DateTime dFrom = Convert.ToDateTime(df);
                criteria.Add(Restrictions.Ge("CreationDate", dFrom));
                //DateTime dt = DateTime.ParseExact(dateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //criteria.Add(Expression.Eq(Projections.SqlFunction("dateFrom", NHibernateUtil.DateTime, Projections.Property("CreationDate")), dt));
            }
            if (!String.IsNullOrEmpty(dateTo))
            {
                DateTime dTo = Convert.ToDateTime(dateTo + " 23:59:59.000");
                criteria.Add(Restrictions.Le("CreationDate", dTo));
                //DateTime dt = DateTime.ParseExact(dateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //criteria.Add(Expression.Eq(Projections.SqlFunction("dateTo", NHibernateUtil.DateTime, Projections.Property("CreationDate")), dt));
            }
            if (!(Array.Exists(selectedFromBranch, item => item == SelectionType.SelelectAll)))
            {
                criteria.CreateCriteria("fb.Branch", "fbr").Add(Restrictions.In("fbr.Id", selectedFromBranch));
            }
            if (!(Array.Exists(selectedFromCampus, item => item == SelectionType.SelelectAll)))
            {
                criteria.CreateCriteria("fb.Campus", "fc").Add(Restrictions.In("fc.Id", selectedFromCampus));
            }

            if (!(Array.Exists(selectedToBranch, item => item == SelectionType.SelelectAll)))
            {
                criteria.CreateCriteria("tb.Branch", "tbr").Add(Restrictions.In("tbr.Id", selectedToBranch));
            }
            if (!(Array.Exists(selectedToCampus, item => item == SelectionType.SelelectAll)))
            {
                criteria.CreateCriteria("tb.Campus", "tc").Add(Restrictions.In("tc.Id", selectedToCampus));
            }

            if (!String.IsNullOrEmpty(prnNo))
            {
                criteria.Add(Restrictions.Like("sp.PrnNo", prnNo, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("st.NickName", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                criteria.Add(Restrictions.Like("st.Mobile", mobile, MatchMode.Anywhere));
            }
            criteria.AddOrder(Order.Asc("sp.PrnNo"));
            IList<StudentTransfer> sTransferList = criteria.SetFirstResult(start).SetMaxResults(length).List<StudentTransfer>();

            return sTransferList;
        }

        public IList<StudentTransfer> LoadStudentTransferSettleByCriteriaCount(long programId, long sessionId, long[] selectedFromBranch,
            long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string dateFrom, string dateTo, string prnNo = "", string name = "",
            string mobile = "")
        {
            ICriteria criteria = Session.CreateCriteria<StudentTransfer>();

            criteria.CreateCriteria("ToBatch", "tb").Add(Restrictions.Eq("tb.Session.Id", sessionId)).Add(Restrictions.Eq("tb.Program.Id", programId));
            criteria.CreateCriteria("FromBatch", "fb").Add(Restrictions.Eq("fb.Session.Id", sessionId)).Add(Restrictions.Eq("fb.Program.Id", programId));
            criteria.CreateCriteria("StudentProgram", "sp");
            criteria.CreateAlias("sp.Student", "st");

            if (!(Array.Exists(selectedFromBranch, item => item == SelectionType.SelelectAll)))
            {
                criteria.CreateCriteria("fb.Branch", "fbr").Add(Restrictions.In("fbr.Id", selectedFromBranch));
            }
            if (!(Array.Exists(selectedFromCampus, item => item == SelectionType.SelelectAll)))
            {
                criteria.CreateCriteria("fb.Campus", "fc").Add(Restrictions.In("fc.Id", selectedFromCampus));
            }

            if (!(Array.Exists(selectedToBranch, item => item == SelectionType.SelelectAll)))
            {
                criteria.CreateCriteria("tb.Branch", "tbr").Add(Restrictions.In("tbr.Id", selectedToBranch));
            }
            if (!(Array.Exists(selectedToCampus, item => item == SelectionType.SelelectAll)))
            {
                criteria.CreateCriteria("tb.Campus", "tc").Add(Restrictions.In("tc.Id", selectedToCampus));
            }
            if (!String.IsNullOrEmpty(dateFrom))
            {
                string df = dateFrom + " 00:00:00.000";
                DateTime dFrom = Convert.ToDateTime(df);
                criteria.Add(Restrictions.Ge("CreationDate", dFrom));
                //DateTime dt = DateTime.ParseExact(dateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //criteria.Add(Expression.Eq(Projections.SqlFunction("dateFrom", NHibernateUtil.DateTime, Projections.Property("CreationDate")), dt));
            }
            if (!String.IsNullOrEmpty(dateTo))
            {
                DateTime dTo = Convert.ToDateTime(dateTo + " 23:59:59.000");
                criteria.Add(Restrictions.Le("CreationDate", dTo));
                //DateTime dt = DateTime.ParseExact(dateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //criteria.Add(Expression.Eq(Projections.SqlFunction("dateTo", NHibernateUtil.DateTime, Projections.Property("CreationDate")), dt));
            }
            if (!String.IsNullOrEmpty(prnNo))
            {
                criteria.Add(Restrictions.Like("sp.PrnNo", prnNo, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("st.NickName", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                criteria.Add(Restrictions.Like("st.Mobile", mobile, MatchMode.Anywhere));
            }
            criteria.AddOrder(Order.Asc("sp.PrnNo"));
            IList<StudentTransfer> sTransferList = criteria.List<StudentTransfer>();

            return sTransferList;
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function
        private ICriteria LoadStudentTransferByCriteria(long programId, long sessionId, long[] selectedFromBranch,
            long[] selectedFromCampus, long[] selectedToBranch, long[] selectedToCampus, string prnNo, string name, string mobile, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            ICriteria criteria = Session.CreateCriteria<StudentTransfer>();

            criteria.CreateCriteria("ToBatch", "tb");
            criteria.CreateCriteria("FromBatch", "fb");
            if (programId != SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("tb.Program.Id", programId));
                criteria.Add(Restrictions.Eq("fb.Program.Id", programId));
            }
            if (sessionId != SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("tb.Session.Id", sessionId));
                criteria.Add(Restrictions.Eq("fb.Session.Id", sessionId));
            }
            criteria.CreateCriteria("StudentProgram", "sp");
            criteria.CreateAlias("sp.Student", "st");
            if (dateFrom != null)
            {
                criteria.Add(Restrictions.Ge("TransferDate", dateFrom.Value.Date));
            }
            if (dateTo != null)
            {
                criteria.Add(Restrictions.Le("TransferDate", dateTo.Value.AddDays(1).Date));
            }
            if (selectedToBranch == null && selectedToCampus == null)
            {
                if (!(Array.Exists(selectedFromBranch, item => item == SelectionType.SelelectAll)))
                {
                    criteria.CreateAlias("fb.Branch", "fbr");
                    criteria.CreateAlias("tb.Branch", "tbr");
                    criteria.Add(Restrictions.Or(
                        Restrictions.In("fbr.Id", selectedFromBranch),
                        Restrictions.In("tbr.Id", selectedFromBranch)));
                }
                if (!(Array.Exists(selectedFromCampus, item => item == SelectionType.SelelectAll)))
                {
                    criteria.CreateAlias("fb.Campus", "fc");
                    criteria.CreateAlias("tb.Campus", "tc");
                    criteria.Add(Restrictions.Or(
                        Restrictions.In("fc.Id", selectedFromCampus),
                        Restrictions.In("tc.Id", selectedFromCampus)));
                }

            }
            else
            {
                if (!(Array.Exists(selectedFromBranch, item => item == SelectionType.SelelectAll)))
                {
                    criteria = criteria.CreateCriteria("fb.Branch", "fbr").Add(Restrictions.In("fbr.Id", selectedFromBranch));

                }
                if (!(Array.Exists(selectedFromCampus, item => item == SelectionType.SelelectAll)))
                {
                    criteria = criteria.CreateCriteria("fb.Campus", "fc").Add(Restrictions.In("fc.Id", selectedFromCampus));

                }
                if (!(Array.Exists(selectedToBranch, item => item == SelectionType.SelelectAll)))
                {
                    criteria = criteria.CreateCriteria("tb.Branch", "tbr").Add(Restrictions.In("tbr.Id", selectedToBranch));
                }
                if (!(Array.Exists(selectedToCampus, item => item == SelectionType.SelelectAll)))
                {
                    criteria = criteria.CreateCriteria("tb.Campus", "tc").Add(Restrictions.In("tc.Id", selectedToCampus));
                }

            }

            if (!String.IsNullOrEmpty(prnNo))
            {
                criteria.Add(Restrictions.Like("sp.PrnNo", prnNo, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("st.NickName", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                criteria.Add(Restrictions.Like("st.Mobile", mobile, MatchMode.Anywhere));
            }
            criteria.AddOrder(Order.Asc("sp.PrnNo"));
            return criteria;
        }

        public decimal GetTotalPayable(long programId, long sessionId, long[] selectedFromBranch,
            long[] selectedFromCampus, string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo)
        {

            var fromCampusQuery = "";
            var toCampusQuery = "";
            if (!selectedFromCampus.Contains(SelectionType.SelelectAll))
            {
                fromCampusQuery = @" and
						 (
							fc7_.Id in ({1})
							 
								)";
                toCampusQuery = @" and
						 (
							
								 tc8_.Id in ({1})
								)";
            }
            var query1 = @"SELECT
	 ISNULL(sum(this_.Payable),0) as Payable
	 FROM StudentTransfer this_ 
	 inner join StudentProgram sp3_ on this_.StudentProgramId=sp3_.Id 
	 inner join Student st4_ on sp3_.StudentId=st4_.Id 
	 inner join Batch fb2_ on this_.FromBatchId=fb2_.Id 
	 inner join Branch fbr5_ on fb2_.BranchId=fbr5_.Id 
	 inner join Campus fc7_ on fb2_.CampusId=fc7_.Id 
	 inner join Batch tb1_ on this_.ToBatchId=tb1_.Id 
	 inner join Branch tbr6_ on tb1_.BranchId=tbr6_.Id 
	 inner join Campus tc8_ on tb1_.CampusId=tc8_.Id 
WHERE ( fbr5_.Id in ({0}) ) " + fromCampusQuery;
            if (programId != SelectionType.SelelectAll)
            {
                query1 += @" and
	 tb1_.ProgramId = " + programId + @" and
			 fb2_.ProgramId =  " + programId;
            }
            if (sessionId != SelectionType.SelelectAll)
            {
                query1 += @" and tb1_.SessionId = " + sessionId + @" and
		 fb2_.SessionId =  " + sessionId;
            }



            query1 += @"and this_.TransferDate>='{2}' and this_.TransferDate<='{3}'
						and this_.Payable is not null";
            var query2 = @"SELECT
	 ISNULL(sum(this_.Receiveable),0) as Payable
	 FROM StudentTransfer this_ 
	 inner join StudentProgram sp3_ on this_.StudentProgramId=sp3_.Id 
	 inner join Student st4_ on sp3_.StudentId=st4_.Id 
	 inner join Batch fb2_ on this_.FromBatchId=fb2_.Id 
	 inner join Branch fbr5_ on fb2_.BranchId=fbr5_.Id 
	 inner join Campus fc7_ on fb2_.CampusId=fc7_.Id 
	 inner join Batch tb1_ on this_.ToBatchId=tb1_.Id 
	 inner join Branch tbr6_ on tb1_.BranchId=tbr6_.Id 
	 inner join Campus tc8_ on tb1_.CampusId=tc8_.Id 
        WHERE  
				 (
					
						 tbr6_.Id in ({0})
					) " + toCampusQuery;
            if (sessionId != SelectionType.SelelectAll)
            {
                query2 += @" and tb1_.SessionId = " + sessionId + @" and
		 fb2_.SessionId = " + sessionId;
            }
            if (programId != SelectionType.SelelectAll)
            {
                query2 += @" and
	 tb1_.ProgramId = " + programId + @"  and
			 fb2_.ProgramId =  " + programId;
            }


            query2 += @" and this_.TransferDate>='{2}' and this_.TransferDate<='{3}'
						and this_.Receiveable is not null";
            query1 = String.Format(query1, string.Join(", ", selectedFromBranch), string.Join(", ", selectedFromCampus), dateFrom.Date, dateTo.Date.AddDays(1));
            IQuery iQuery = Session.CreateSQLQuery(query1);
            iQuery.SetTimeout(2700);
            var payable1 = iQuery.UniqueResult<decimal>();
            query2 = String.Format(query2, string.Join(", ", selectedFromBranch), string.Join(", ", selectedFromCampus), dateFrom.Date, dateTo.Date.AddDays(1));
            IQuery iQuery2 = Session.CreateSQLQuery(query2);
            iQuery2.SetTimeout(2700);
            var payable2 = iQuery2.UniqueResult<decimal>();
            return payable1 + payable2;
        }

        public decimal GetTotalReceivable(long programId, long sessionId, long[] selectedFromBranch, long[] selectedFromCampus,
            string prnNo, string name, string mobile, DateTime dateFrom, DateTime dateTo)
        {
            var fromCampusQuery = "";
            var toCampusQuery = "";
            if (!selectedFromCampus.Contains(SelectionType.SelelectAll))
            {
                fromCampusQuery = @" and
						 (
							fc7_.Id in ({1})
							 
								)";
                toCampusQuery = @" and
						 (
							
								 tc8_.Id in ({1})
								)";
            }

            var query1 = @"SELECT
	 ISNULL(sum(this_.Receiveable),0) as Receiveable
	 FROM StudentTransfer this_ 
	 inner join StudentProgram sp3_ on this_.StudentProgramId=sp3_.Id 
	 inner join Student st4_ on sp3_.StudentId=st4_.Id 
	 inner join Batch fb2_ on this_.FromBatchId=fb2_.Id 
	 inner join Branch fbr5_ on fb2_.BranchId=fbr5_.Id 
	 inner join Campus fc7_ on fb2_.CampusId=fc7_.Id 
	 inner join Batch tb1_ on this_.ToBatchId=tb1_.Id 
	 inner join Branch tbr6_ on tb1_.BranchId=tbr6_.Id 
	 inner join Campus tc8_ on tb1_.CampusId=tc8_.Id 
WHERE
				 (
					fbr5_.Id in ({0})
					 
						) " + fromCampusQuery;
            if (sessionId != SelectionType.SelelectAll)
            {
                query1 += @" and tb1_.SessionId = " + sessionId + @"  and fb2_.SessionId = " + sessionId;
            }
            if (programId != SelectionType.SelelectAll)
            {
                query1 += @" and tb1_.ProgramId = " + programId + @" and
			 fb2_.ProgramId =   " + programId;
            }




            query1 += @" and this_.TransferDate>='{2}' and this_.TransferDate<='{3}'
						and this_.Receiveable is not null";
            var query2 = @"SELECT
	 ISNULL(sum(this_.Payable),0) as Payable
	 FROM StudentTransfer this_ 
	 inner join StudentProgram sp3_ on this_.StudentProgramId=sp3_.Id 
	 inner join Student st4_ on sp3_.StudentId=st4_.Id 
	 inner join Batch fb2_ on this_.FromBatchId=fb2_.Id 
	 inner join Branch fbr5_ on fb2_.BranchId=fbr5_.Id 
	 inner join Campus fc7_ on fb2_.CampusId=fc7_.Id 
	 inner join Batch tb1_ on this_.ToBatchId=tb1_.Id 
	 inner join Branch tbr6_ on tb1_.BranchId=tbr6_.Id 
	 inner join Campus tc8_ on tb1_.CampusId=tc8_.Id 
     WHERE (tbr6_.Id in ({0})
						) " + toCampusQuery;


            if (sessionId != SelectionType.SelelectAll)
            {
                query2 += @" and tb1_.SessionId = " + sessionId + @"  and fb2_.SessionId = " + sessionId;
            }
            if (programId != SelectionType.SelelectAll)
            {
                query2 += @" and tb1_.ProgramId = " + programId + @" and
			    fb2_.ProgramId = " + programId;
            }
            query2 += @" and this_.TransferDate>='{2}' and this_.TransferDate<='{3}'
						and this_.Payable is not null";
            query1 = String.Format(query1, string.Join(", ", selectedFromBranch), string.Join(", ", selectedFromCampus), dateFrom.Date, dateTo.Date.AddDays(1));
            IQuery iQuery = Session.CreateSQLQuery(query1);
            iQuery.SetTimeout(2700);
            var rec1 = iQuery.UniqueResult<decimal>();
            query2 = String.Format(query2, string.Join(", ", selectedFromBranch), string.Join(", ", selectedFromCampus), dateFrom.Date, dateTo.Date.AddDays(1));
            IQuery iQuery2 = Session.CreateSQLQuery(query2);
            iQuery2.SetTimeout(2700);
            var rec2 = iQuery2.UniqueResult<decimal>();
            return rec1 + rec2;
        }

        public StudentTransfer GetPreviousTransferInfo(long studentProgramId)
        {
            var previousTransferInfo =
                Session.QueryOver<StudentTransfer>()
                    .Where(x => x.StudentProgram.Id == studentProgramId && x.TransferDate < DateTime.Now)
                    .List<StudentTransfer>();
            if (previousTransferInfo != null)
            {
                var previousTransferInfoSingle = previousTransferInfo.OrderByDescending(x => x.TransferDate).FirstOrDefault();
                return previousTransferInfoSingle;
            }
            return null;

        }

        public IList<StudentTransferReportDto> LoadStudentTransferReport(int start, int length, long programId, long sessionId,
            long[] fromBranchIds, long[] selectedFromCampus, string prnNo, string name, string mobile, DateTime dateFrom,
            DateTime dateTo)
        {
            var query = @"Select y.RowNum,y.TransferId,y.StudentName,y.StudentRoll,y.PreviousBranch,y.PreviousCampus,y.CurrentBranch,y.CurrentCampus,CAST(y.Payable as varchar) Payable,CAST(y.Receivable as varchar) Receivable,y.DueAmount,y.TransferDate,y.IsSettled from (
                                SELECT    ROW_NUMBER() OVER ( ORDER BY x.StudentRoll ) AS RowNum, * from (
                                 " + StudentTransferReportQuery(fromBranchIds, dateFrom, dateTo, prnNo, name, mobile) + @" )x)y ";
            if (length > 0)
            {
                query += @" WHERE   RowNum >= " + (start + 1) + @" AND RowNum <= " + (start + length) +
                         " ORDER BY RowNum";
            }
            else
            {
                query += @"  ORDER BY RowNum";
            }
            query = string.Format(query, string.Join(", ", selectedFromCampus));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentTransferReportDto>());
            var list = iQuery.List<StudentTransferReportDto>().ToList();
            return list;
        }

        public int LoadStudentTransferReportCount(long programId, long sessionId, long[] fromBranchIds, long[] campusIds, string prnNo, string name, string mobile,
            DateTime dateFrom, DateTime dateTo)
        {
            var query = @"select  Count(*) from(" +
            StudentTransferReportQuery(fromBranchIds, dateFrom, dateTo, prnNo, name, mobile) + " )x";
            query = string.Format(query, string.Join(", ", campusIds));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public void GetOvetAllPayableReceivable(long programId, long sessionId, long[] fromBranchIds, long[] campusIds, string prnNo, string name, string mobile,
            DateTime dateFrom, DateTime dateTo, out decimal overallPayable, out decimal overallReceivable)
        {
            var query = @"select  CAST(SUM(ISNULL(x.Payable,0)) AS varchar(10))   Payable,CAST(SUM(ISNULL(x.Receivable,0)) AS varchar(10))  Receivable from(" +
            StudentTransferReportQuery(fromBranchIds, dateFrom, dateTo, prnNo, name, mobile) + " )x";
            query = string.Format(query, string.Join(", ", campusIds));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentTransferReportDto>());
            var list = iQuery.List<StudentTransferReportDto>().ToList();
            //var x = list.SingleOrDefault();
            overallPayable = Convert.ToDecimal(((StudentTransferReportDto)list[0]).Payable);
            overallReceivable = Convert.ToDecimal(((StudentTransferReportDto)list[0]).Receivable);
            //return list;
        }

        private static string StudentTransferReportQuery(long[] fromBranchIds, DateTime dateFrom, DateTime dateTo, string prnNo, string name, string mobile)
        {
            var query = @" select st.Id TransferId,s.FullName StudentName,sp.PrnNo StudentRoll,br.Name PreviousBranch ,c.Name  PreviousCampus,br2.Name CurrentBranch
								,c2.Name  CurrentCampus
                                ,ISNULL(st.Payable,0)  Payable,ISNULL(st.Receiveable,0)  Receivable,CAST(st.DueAmount AS varchar(10)) DueAmount
								,CAST(st.TransferDate AS varchar) TransferDate
                                , case st.IsSettled when 1 then 'True' when 0 then 'False' end IsSettled 
								 from StudentTransfer st inner join Batch b1 on b1.Id=st.FromBatchId 
                                inner join Campus c on b1.CampusId=c.Id
                                inner join Batch b2 on b2.Id=st.ToBatchId 
                                inner join Campus c2 on b2.CampusId=c2.Id
                                inner join Branch br on br.Id=c.BranchId
                                inner join Branch br2 on br2.Id=c2.BranchId
                                inner join StudentProgram sp on st.StudentProgramId=sp.Id
                                inner join Student s on sp.StudentId=s.Id
                                where br.Id=" + fromBranchIds[0] + @" and c.Id in({0})
                                and c2.Id not in({0})
                                and st.TransferDate>='" + dateFrom + @"' and st.TransferDate<='" + dateTo.AddDays(1) + "'";
            if (!string.IsNullOrEmpty(prnNo))
            {
                query += " and sp.PrnNo like '%" + prnNo.Trim() + "%'";
            }
            if (!string.IsNullOrEmpty(name))
            {
                query += " and s.fullname like '%" + name.Trim() + "%'";
            }
            if (!string.IsNullOrEmpty(mobile))
            {
                query += " and s.mobile like '%" + mobile.Trim() + "%'";
            }
            query += @" union all
                                --to
                                select st.Id TransferId,s.FullName StudentName,sp.PrnNo StudentRoll,br.Name PreviousBranch ,c.Name  PreviousCampus,br2.Name CurrentBranch
								,c2.Name  CurrentCampus
                                ,ISNULL(st.Receiveable,0)  Payable,ISNULL(st.Payable,0)  Receivable,CAST(st.DueAmount AS varchar(10)) DueAmount
								,CAST(st.TransferDate AS varchar) TransferDate
                                , case st.IsSettled when 1 then 'True' when 0 then 'False' end IsSettled 
                                from StudentTransfer st inner join Batch b1 on b1.Id=st.FromBatchId 
                                inner join Campus c on b1.CampusId=c.Id
                                inner join Batch b2 on b2.Id=st.ToBatchId 
                                inner join Campus c2 on b2.CampusId=c2.Id
                                inner join Branch br on br.Id=c.BranchId
                                inner join Branch br2 on br2.Id=c2.BranchId
                                inner join StudentProgram sp on st.StudentProgramId=sp.Id
                                inner join Student s on sp.StudentId=s.Id
                                where br2.Id=" + fromBranchIds[0] + @" and c2.Id in({0})
                                and c.Id not in({0})
                                and st.TransferDate>='" + dateFrom + @"' and st.TransferDate<='" + dateTo.AddDays(1) + "'";
            if (!string.IsNullOrEmpty(prnNo))
            {
                query += " and sp.PrnNo like '%" + prnNo.Trim() + "%'";
            }
            if (!string.IsNullOrEmpty(name))
            {
                query += " and s.fullname like '%" + name.Trim() + "%'";
            }
            if (!string.IsNullOrEmpty(mobile))
            {
                query += " and s.mobile like '%" + mobile.Trim() + "%'";
            }
            return query;

        }

        #endregion

    }
}