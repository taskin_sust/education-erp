﻿//zakat
var approvedRefundsuccess = true;
var zakatEdCdSuccess;
var csrEdCdSuccess;
//parse string to date
function ParseDateString(dateString) {
    console.log(dateString);
    var dateArray = dateString.split(" ");
    var month = dateArray[0];
    var year = dateArray[1];
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var monthNumber = $.inArray(month, months);
    var date = new Date(year, monthNumber, "01");
    console.log(date);
    date.setHours(0, 0, 0, 0);
    return date;
}

//find date difference by month
function FindMonthDifferenceOfDates(dateFrom, dateTo) {
    console.log("hello =>>> " + (dateTo.getMonth() - dateFrom.getMonth() + 1)
        + (12 * (dateTo.getFullYear() - dateFrom.getFullYear())));

    return (dateTo.getMonth() - dateFrom.getMonth() + 1)
        + (12 * (dateTo.getFullYear() - dateFrom.getFullYear()));

}

function ParseFormValidation() {
    $("#createLoanApplicationForm").data("validator", null);
    $.validator.unobtrusive.parse($("#createLoanApplicationForm"));
}
//disable closing date field
function DisableClosingDateField(dateField) {
    dateField.prop('disabled', true);
}

//enable closing date field
function EnableClosingDateField(dateField) {
    dateField.prop('disabled', false);
}
//CalculateZakatAmount
function CalculateZakatTotalAmount() {
    var zakatTotalAmount = 0;
    var zakatAmount = 0;
    var zakatAmountField = $('#zakatPanelBody').find("#ZakatAmount");
    if (zakatAmountField.val() != null && zakatAmountField.val() !== "" && !isNaN(zakatAmountField.val())) {
        zakatAmount = parseFloat(zakatAmountField.val());
    }
    var zakatEffectiveDateField = $('#zakatPanelBody').find("#ZakatEffectiveDate");
    var zakatClosingDateField = $('#zakatPanelBody').find("#ZakatClosingDate");
    var zakatEffectiveDate = ParseDateString(zakatEffectiveDateField.val());
    var zakatClosingDate = ParseDateString(zakatClosingDateField.val());
    if (zakatEffectiveDate <= zakatClosingDate) {
        var monthDiff = FindMonthDifferenceOfDates(zakatEffectiveDate, zakatClosingDate);
        var zpaymentType = $('#zakatPanelBody').find("#ZakatPaymentType").val();
        console.log("zpaymentType==>" + zpaymentType);

        if (zpaymentType == 1) { console.log("entered once"); zakatTotalAmount = parseFloat(zakatAmount); }
        else if (zpaymentType == 3) { console.log("entered monthly"); zakatTotalAmount = parseFloat(zakatAmount * monthDiff); }
        else if (zpaymentType == 4) {
            if (monthDiff > 12 && Math.ceil(monthDiff / 12) > 1) {
                console.log("______");
                zakatTotalAmount = parseFloat(zakatAmount * Math.ceil(monthDiff / 12));
            } else {
                console.log("___|___");
                zakatTotalAmount = parseFloat(zakatAmount);
            }
        }
        console.log("zta:" + zakatTotalAmount);
    }
    $('#zakatPanelBody').find("#ZakatTotalAmount").val(zakatTotalAmount);
    console.log("zed:" + zakatEffectiveDate);
    console.log("zcd:" + zakatClosingDate);
}
$(document).ready(function () {
    $('#zakatCheckbox').change(function () {
        var chkbox = $(this);
        if (chkbox.is(':checked')) {
            var zakatDivHtml = $("#zakatDiv").html();
            $("#zakatPanelBody").html(zakatDivHtml);

            //bind datepicker
            $('#zakatPanelBody').on('focus', '#ZakatEffectiveDate', function () {
                var $this = $(this);
                $this.datetimepicker({
                    startDate: new Date(),
                    format: "MM yyyy",
                    startView: "year",
                    minView: "year",
                    autoclose: true
                }).on('changeDate', function (ev) {
                    
                    CalculateZakatTotalAmount();
                });
            });
            $('#zakatPanelBody').on('focus', '#ZakatClosingDate', function () {
                var $this = $(this);
                $this.datetimepicker({
                    startDate: new Date(),
                    format: "MM yyyy",
                    startView: "year",
                    minView: "year",
                    autoclose: true
                }).on('changeDate', function (ev) {
                    CalculateZakatTotalAmount();
                });
            });
            $("#zakatPanelBody").show();

        } else {
            $("#zakatPanelBody").html("");
            $("#zakatPanelBody").hide();
        }
        ParseFormValidation();
    });



    //Zakat amount on change
    $('#zakatPanelBody').on('input propertychange', '#ZakatAmount, #ZakatPaymentType', function () {
        console.log("CalculateZakatTotalAmount calling");
        CalculateZakatTotalAmount();
    });

});