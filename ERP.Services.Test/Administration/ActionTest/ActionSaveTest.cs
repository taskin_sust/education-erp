﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.AdminisTration.ProgramTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ActionTest
{

    [Trait("Area", "Administration")]
    [Trait("Service", "Action")]
    public class ActionSaveTest : ActionBaseTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => _actionsService.Save(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var actionFactory =
                _actionsFactory.Create().WithController().WithNameAndRank(_name, 1)
                    .Persist();
            Actions actions = _actionsService.LoadById(actionFactory.Object.Id);
            Assert.NotNull(actions);
            Assert.True(actions.Id > 0);
        }

        [Fact]
        public void Save_Should_Check_Duplicate_Name_Entry()
        {
            var actionFactory1 =
                _actionsFactory.Create().WithController().WithNameAndRank(_name, 1)
                    .Persist();
            var controllers = actionFactory1.Object.AreaControllers;
            var actionFactory2 =
                _actionsFactory.Create()
                    .WithController(controllers)
                    .WithNameAndRank(_name, 1);
            Assert.Throws<DuplicateEntryException>(() => actionFactory2.Persist());
        }
        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Not_Save_Empty_Name()
        {
            var actionFactory1 =
                _actionsFactory.Create().WithController().WithNameAndRank("", 1);
            Assert.Throws<EmptyFieldException>(() => actionFactory1.Persist());
        }
        [Fact]
        public void Should_Not_Save_Empty_Controller()
        {
            var actionFactory1 =
                _actionsFactory.Create().WithController(null).WithNameAndRank(_name, 1);
            Assert.Throws<EmptyFieldException>(() => actionFactory1.Persist());
        }

        #endregion
    }
}
