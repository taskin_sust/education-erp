﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IAcademicExamDao : IBaseDao<BusinessModel.Entity.Hr.AcademicExam, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function

        IList<BusinessModel.Entity.Hr.AcademicExam> ExamList();

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class AcademicExamDao : BaseDao<BusinessModel.Entity.Hr.AcademicExam, long>, IAcademicExamDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
        public IList<BusinessModel.Entity.Hr.AcademicExam> ExamList()
        {
            try
            {
                var list = Session.QueryOver<BusinessModel.Entity.Hr.AcademicExam>().List();
                if (list != null && list.Count > 0)
                    return list;
                return null;
            }
            catch (NullReferenceException ex)
            {
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion
    }

}
