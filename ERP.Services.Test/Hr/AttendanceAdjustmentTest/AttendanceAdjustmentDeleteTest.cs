﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceAdjustmentTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceAdjustment")]
    public class AttendanceAdjustmentDeleteTest:AttendanceAdjustmentBaseTest
    {
        [Fact]
        public void Should_Return_AttendanceAdjustment_Delete_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016, 12, 5),
                new DateTime(2016, 12, 5, 10, 10, 10),new DateTime(2016, 12, 5, 11, 10, 10), "Due to",(int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm).Persist();
            Assert.True(_attendanceAdjustmentService.Delete(_attendanceAdjustmentFactory.Object));
        }

        [Fact]
        public void Should_Return_AttendanceAdjustment_Delete_Successfully_For_ApprovedStatus()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016, 12, 5),new DateTime(2016, 12, 5, 10, 10, 10),new DateTime(2016, 12, 5, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Approved, 0).WithTeamMember(tm).Persist();
            Assert.True(_attendanceAdjustmentService.Delete(_attendanceAdjustmentFactory.Object));
        }

        [Fact]
        public void Should_Return_AttendanceAdjustment_InvalidException_For_Null_Object()
        {
            Assert.Throws<InvalidDataException>(() => _attendanceAdjustmentService.Delete(null));
        }
    }
}
