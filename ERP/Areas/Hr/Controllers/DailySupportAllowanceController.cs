﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class DailySupportAllowanceController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private List<UserMenu> authorizeMenu;
        private ISession _session;

        private readonly IOrganizationService _organizationService;
        private readonly IDailySupportAllowanceSettingService _dailySupportAllowanceSettingService;
        private readonly IDesignationService _designationService;
        private readonly IUserService _userService;

        #endregion

        #region Constructor

        public DailySupportAllowanceController()
        {
            _session = NHibernateSessionFactory.OpenSession();

            _organizationService = new OrganizationService(_session);
            _dailySupportAllowanceSettingService = new DailySupportAllowanceSettingService(_session);
            _designationService = new DesignationService(_session);
            _userService = new UserService(_session);
        }

        #endregion

        #region Index

        public ActionResult Index()
        {
            return RedirectToAction("ManageDailySupportAllowance");
        }

        [HttpGet]
        public ActionResult ManageDailySupportAllowance()
        {
            try
            {
                Initialize();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult ManageDailySupportAlowance(int draw, int start, int length, long? organizationId)
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<DailySupportAllowanceSetting> dailySupportAllowances = _dailySupportAllowanceSettingService.LoadDailySupportAllowanceSetting(draw, start, length, authorizeMenu, organizationId);
                int count = _dailySupportAllowanceSettingService.GetDailySupportAllowanceCount(authorizeMenu, organizationId);

                var data = new List<object>();
                foreach (var supportAllowance in dailySupportAllowances)
                {
                    var str = new List<string>();
                    str.Add(supportAllowance.Organization.ShortName);
                    str.Add(supportAllowance.Designation.ShortName);
                    str.Add(supportAllowance.Amount.ToString());
                    str.Add(DateTime.MinValue != supportAllowance.EffectiveDate ? supportAllowance.EffectiveDate.ToString("MMM dd,yyyy") : " ");
                    str.Add(!String.IsNullOrEmpty(supportAllowance.ClosingDate.ToString()) ? supportAllowance.ClosingDate.Value.ToString("MMM dd,yyyy") : "");
                    str.Add(LinkGenerator.GetGeneratedDetailsEditLink("Details", "Edit", "DailySupportAllowance", supportAllowance.Id)
                            + LinkGenerator.GetDeleteLink("Delete", "DailySupportAllowance", supportAllowance.Id));

                    data.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = count,
                    recordsFiltered = count,
                    start = start,
                    length = length,
                    data = data
                });

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                var data = new List<object>();
                return Json(new
                 {
                     draw = draw,
                     recordsTotal = 0,
                     recordsFiltered = 0,
                     start = start,
                     length = length,
                     data = data
                     //mes="message no 1"
                 });
            }
        }

        #endregion

        #region Operational Function

        #region Save

        [HttpGet]
        public ActionResult Create(string message = "", int? type = 0)
        {
            try
            {
                Initialize();
                switch (type)
                {
                    case 1:
                        ViewBag.SuccessMessage = message;
                        break;
                    case 2:
                        ViewBag.ErrorMessage = message;
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveOrUpdate(DailySupportAllowanceViewModel supportAllowanceVm)
        {
            var messsage = "";
            try
            {
                if (!ModelState.IsValid)
                {
                    IEnumerable<ModelError> modelStateErrors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
                    messsage = modelStateErrors.Aggregate(messsage, (current, modelStateError) => current + (modelStateError.ErrorMessage + " "));
                    return RedirectToAction("Create", new { message = messsage, type = 2 /*failed*/ });
                }
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                DailySupportAllowanceSetting supportAllowance = FormModelFromVm(supportAllowanceVm);
                _dailySupportAllowanceSettingService.SaveOrUpdate(authorizeMenu, supportAllowance);
                messsage = "Operation completed successfully";
                return RedirectToAction("Create", new { message = messsage, type = 1 /*success*/ });
            }
            catch (NullObjectException nullObjectException)
            {
                messsage = nullObjectException.Message;
            }
            catch (InvalidDataException invalidDataException)
            {
                messsage = invalidDataException.Message;
            }
            catch (DuplicateEntryException duplicateEntryException)
            {
                messsage = duplicateEntryException.Message;
            }
            catch (MessageException me)
            {
                messsage = me.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                messsage = WebHelper.CommonErrorMessage;
            }
            return RedirectToAction("Create", new { message = messsage, type = 2 /*fail*/});
        }

        #endregion

        #region Edit

        [HttpGet]
        public ActionResult Edit(long id, string message = "", int? type = 0)
        {
            DailySupportAllowanceViewModel dailySupportAllowanceVm = null;
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                var dailySupportAllowanceObj = _dailySupportAllowanceSettingService.GetDailySupportAllowanceSetting(id);
                dailySupportAllowanceVm = FormVmFromModel(dailySupportAllowanceObj);
                ViewBag.DesiganationList = new SelectList(_designationService.LoadDesignation(new List<long>() { dailySupportAllowanceVm.Organization }), "Id", "Name", dailySupportAllowanceVm.Designation);
                ViewBag.orgList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName", dailySupportAllowanceVm.Organization);
                switch (type)
                {
                    case 1:
                        ViewBag.SuccessMessage = message;
                        break;
                    case 2:
                        ViewBag.ErrorMessage = message;
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View(dailySupportAllowanceVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DailySupportAllowanceViewModel supportAllowance)
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            var proxyObj = _dailySupportAllowanceSettingService.GetDailySupportAllowanceSetting(supportAllowance.Id);
            var messsage = "";
            try
            {
                if (!ModelState.IsValid)
                {
                    IEnumerable<ModelError> modelStateErrors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
                    messsage = modelStateErrors.Aggregate(messsage, (current, modelStateError) => current + (modelStateError.ErrorMessage + " "));
                    return RedirectToAction("Edit", new { id = proxyObj.Id, message = messsage, type = 2 /*failed*/ });
                }
                proxyObj.Organization = _organizationService.LoadById(supportAllowance.Organization);
                proxyObj.Designation = _designationService.LoadById(supportAllowance.Designation);
                proxyObj.Amount = supportAllowance.Amount;
                proxyObj.EffectiveDate = supportAllowance.EffectiveDate;
                proxyObj.ClosingDate = supportAllowance.ClosingDate;
                _dailySupportAllowanceSettingService.SaveOrUpdate(authorizeMenu, proxyObj);
                messsage = "Operation completed successfully";
                return RedirectToAction("Edit", new { id = proxyObj.Id, message = messsage, type = 1 /*success*/ });
            }
            catch (NullObjectException nullObjectException)
            {
                messsage = nullObjectException.Message;
            }
            catch (InvalidDataException invalidDataException)
            {
                messsage = invalidDataException.Message;
            }
            catch (DuplicateEntryException duplicateEntryException)
            {
                messsage = duplicateEntryException.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                messsage = WebHelper.CommonErrorMessage;
            }
            return RedirectToAction("Edit", new { id = proxyObj.Id, message = messsage, type = 2 /*fail*/});
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                var entity = _dailySupportAllowanceSettingService.GetDailySupportAllowanceSetting(id);
                _dailySupportAllowanceSettingService.Delete(entity);
                return Json(new { isSuccess = true, Text = "Settings Successfully Deleted!" });
            }
            catch (DependencyException ex)
            {
                return Json(new { isSuccess = false, Text = ex.Message });
            }
            catch (NullObjectException nullObjectException)
            {
                return Json(new { isSuccess = false, Text = nullObjectException.Message });
            }
            catch (InvalidDataException invalidDataException)
            {
                return Json(new { isSuccess = false, Text = invalidDataException.Message });
            }
            catch (DuplicateEntryException duplicateEntryException)
            {
                return Json(new { isSuccess = false, Text = duplicateEntryException.Message });
            }
            catch (MessageException me)
            {
                return Json(new { isSuccess = false, Text = me.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { isSuccess = false, Text = "Settings Delete Failed!" });
            }
        }

        #endregion

        #region Details

        [HttpGet]
        public ActionResult Details(long id)
        {
            try
            {
                DailySupportAllowanceSetting entity = _dailySupportAllowanceSettingService.GetDailySupportAllowanceSetting(id);
                var fm = FormVmFromModel(entity);
                ViewBag.CreateByStr = _userService.GetByAspNetUser(entity.CreateBy).AspNetUser.Email;
                ViewBag.ModifiedByStr = _userService.GetByAspNetUser(entity.CreateBy).AspNetUser.Email;
                ViewBag.CreationDateStr = entity.CreationDate.ToString("d MMM, yyyy");
                ViewBag.ModifiedDateStr = entity.ModificationDate.ToString("d MMM, yyyy");
                ViewBag.EffectiveDateStr = entity.EffectiveDate.ToString("d MMM, yyyy");
                ViewBag.ClosingDateStr = entity.ClosingDate != null ? entity.ClosingDate.Value.ToString("d MMM, yyyy") : string.Empty;
                fm.DesignationName = entity.Designation.ShortName;
                fm.Orgname = entity.Organization.ShortName;
                return View(fm);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return RedirectToAction("ManageDailySupportAllowance", "DailySupportAllowance");
            }
        }

        #endregion

        #endregion

        #region Helper

        private void Initialize()
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
            ViewBag.designationList = new SelectList(String.Empty, "Value", "Text");
        }

        private DailySupportAllowanceSetting FormModelFromVm(DailySupportAllowanceViewModel supportAllowanceVm)
        {
            if (supportAllowanceVm == null) throw new NullObjectException();
            if (supportAllowanceVm.Organization <= 0) throw new InvalidDataException("invalid object id");
            if (supportAllowanceVm.Designation <= 0) throw new InvalidDataException("invalid object id");
            var supportAllowance = new DailySupportAllowanceSetting
            {
                Organization = _organizationService.LoadById(supportAllowanceVm.Organization),
                Designation = _designationService.LoadById(supportAllowanceVm.Designation),
                Amount = supportAllowanceVm.Amount,
                EffectiveDate = supportAllowanceVm.EffectiveDate,
                ClosingDate = supportAllowanceVm.ClosingDate
            };
            return supportAllowance;
        }

        private DailySupportAllowanceViewModel FormVmFromModel(DailySupportAllowanceSetting supportAllowance)
        {
            if (supportAllowance == null) throw new NullObjectException();
            var supportAllowanceVm = new DailySupportAllowanceViewModel()
              {
                  Id = supportAllowance.Id,
                  Organization = supportAllowance.Organization.Id,
                  Designation = supportAllowance.Designation.Id,
                  Orgname = supportAllowance.Organization.ShortName,
                  DesignationName = supportAllowance.Designation.ShortName,
                  Amount = supportAllowance.Amount,
                  EffectiveDate = supportAllowance.EffectiveDate,
                  ClosingDate = supportAllowance.ClosingDate
              };
            return supportAllowanceVm;
        }

        #endregion

    }
}