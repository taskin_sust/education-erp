﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{

    public interface IComplementaryCourseDao : IBaseDao<ComplementaryCourse, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
       
        #endregion

        #region List Loading Function
        List<ComplementaryCourse> DeteteComplementaryCourseList(long programId, long sessionId, long courseId, List<long> complementaryCourseIds);        
        #endregion

        #region Others Function
        bool IsComplementaryCourse(List<long> organizationIds, List<long> programIds, List<long> sessionIds, long complementaryCourseId);
        bool AlreadyHasData(long programId, long sessionId, long courseId, long complementaryCourseId);
        List<long> LoadMainCourse(long[] complementaryCourseIds);
        IList<long> LoadComplementaryCourse(long studentprogramId);
        #endregion

        #region Helper Function
       
        #endregion

        #region Public API
       
        #endregion



        


        bool IsComplemented(long comId);
    }

    public class ComplementaryCourseDao : BaseDao<ComplementaryCourse, long>, IComplementaryCourseDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public List<ComplementaryCourse> DeteteComplementaryCourseList(long programId, long sessionId, long courseId, List<long> complementaryCourseIds)
        {
            ICriteria criteria = Session.CreateCriteria<ComplementaryCourse>();
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            criteria.Add(Restrictions.Eq("Course.Id", courseId));
            if (complementaryCourseIds != null && complementaryCourseIds.Count > 0)
                criteria.Add(Restrictions.Not( Restrictions.In("CompCourse.Id", complementaryCourseIds)));
            return criteria.List<ComplementaryCourse>().ToList();
        }
        #endregion

        #region Others Function

      

        public bool IsComplementaryCourse(List<long> organizationIds, List<long> programIds, List<long> sessionIds,
            long complementaryCourseId)
        {

            ICriteria criteria = Session.CreateCriteria<ComplementaryCourse>();
            criteria.CreateAlias("Program", "pr");
            criteria.Add(Restrictions.In("Program.Id", programIds));
            criteria.Add(Restrictions.In("Session.Id", sessionIds));
            criteria.Add(Restrictions.In("pr.Organization.Id", organizationIds));
            criteria.Add(Restrictions.Eq("CompCourse.Id", complementaryCourseId));
            criteria.SetProjection(Projections.RowCount());

            int rowCount = Convert.ToInt32(criteria.UniqueResult());
            if(rowCount > 0)
                return true;
            return false;
        }

        public bool AlreadyHasData(long programId, long sessionId, long courseId, long complementaryCourseId)
        {
            ICriteria criteria = Session.CreateCriteria<ComplementaryCourse>();
            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            criteria.Add(Restrictions.Eq("Course.Id", courseId));
            criteria.Add(Restrictions.Eq("CompCourse.Id", complementaryCourseId));
            criteria.SetProjection(Projections.RowCount());

            int rowCount = Convert.ToInt32(criteria.UniqueResult());
            if (rowCount > 0)
                return true;
            return false;
        }

        public List<long> LoadMainCourse(long[] complementaryCourseIds)  
        {
            ICriteria criteria = Session.CreateCriteria<ComplementaryCourse>();
            criteria.Add(Restrictions.In("CompCourse.Id", complementaryCourseIds));
            criteria.SetProjection(Projections.Distinct(Projections.Property("Course.Id")));
             return criteria.List<long>().ToList();
        }

        public bool IsComplemented(long comId)
        {
            var comCourse =
                Session.QueryOver<ComplementaryCourse>()
                    .Where(x => x.CompCourse.Id == comId && x.Status == ComplementaryCourse.EntityStatus.Active);
            if (comCourse != null)
            {
                return true;
            }
            return false;
        }

       public IList<long> LoadComplementaryCourse(long studentprogramId)
       {
           var query = @"select distinct cc.CompCourseId from StudentCourseDetails scd inner join CourseSubject cs on scd.CourseSubjectId=cs.Id
                            inner join Course c on c.Id=cs.CourseId inner join ComplementaryCourse cc on cc.CourseId=c.Id
                            where scd.StudentProgramId="+studentprogramId;
           IQuery iQuery = Session.CreateSQLQuery(query);
           iQuery.SetTimeout(2700);
           var list = iQuery.List<long>();
           return list;
           
       }
        #endregion

            #region Helper Function

            #endregion

            #region Public API

            #endregion
    }
}
