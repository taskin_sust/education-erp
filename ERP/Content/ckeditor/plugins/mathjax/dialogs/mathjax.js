/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

'use strict';

CKEDITOR.dialog.add( 'mathjax', function( editor ) {

	var preview,
		lang = editor.lang.mathjax;
	function clickResponse() {
	    // do something based on button selection here...
	   
	    var currentText = document.getElementById("cke_71_textarea").value;
	    var newText = currentText + this.alt;
	    document.getElementById("cke_71_textarea").value = newText;
	}
	return {
		title: lang.title,
		minWidth: 350,
		minHeight: 100,
		contents: [
			{
				id: 'info',
				elements: [
				    //{
				    //    //^{}
				    //    type: 'html',
				    //    html: '<img src="http://localhost:9021/eqtools/xpa.png" alt="^{}" style="width: 20px; height: 20px; cursor: pointer;" id="xpa" class="cutomToolBtn" />' +
				    //        '<img src="http://localhost:9021/eqtools/lim.png" alt="\lim_{}" style="width: 20px; height: 20px; cursor: pointer;" id="lim" class="cutomToolBtn" />',
				    //    onLoad: function () {
				    //       // var that = this;
				    //        //var currentText = document.getElementById("cke_72_textarea").value;
				    //        //var newText = currentText + "^{}";
				    //        //document.getElementById("cke_72_textarea").value = newText;

				    //      //  var xpa = document.getElementById('xpa');
				    //        var buttons = document.getElementsByClassName('cutomToolBtn');
				    //        var buttonsLength = buttons.length;
				    //        for (var i = 0; i < buttonsLength; i++) {
				    //            buttons[i].addEventListener('click', clickResponse, false);
				    //        };
				           
				    //    }
				    //},
                     //{
                        
                     //    type: 'html',
                     //    html: '<img src="http://localhost:9021/eqtools/lim.png" alt="" style="width: 20px; height: 20px; cursor: pointer;" />',
                     //    onClick: function () {
                     //       // var that = this;
                     //        var currentText = document.getElementById("cke_72_textarea").value;
                     //        var newText = currentText + "\lim_{}";
                     //        alert(newText);
                     //        document.getElementById("cke_72_textarea").value = newText;

                     //    }
                     //},
					{
						id: 'equation',
						type: 'textarea',
						label: lang.dialogInput,

						onLoad: function() {
							var that = this;

							if ( !( CKEDITOR.env.ie && CKEDITOR.env.version == 8 ) ) {
								this.getInputElement().on( 'keyup', function() {
									// Add \( and \) for preview.
									preview.setValue( '\\(' + that.getInputElement().getValue() + '\\)' );
								} );
							}
						},

						setup: function( widget ) {
							// Remove \( and \).
							this.setValue( CKEDITOR.plugins.mathjax.trim( widget.data.math ) );
						},

						commit: function( widget ) {
							// Add \( and \) to make TeX be parsed by MathJax by default.
							widget.setData( 'math', '\\(' + this.getValue() + '\\)' );
						}
					},
                    
					{
						id: 'documentation',
						type: 'html',
						html:
							'<div style="width:100%;text-align:right;margin:-8px 0 10px">' +
								'<a class="cke_mathjax_doc" href="' + lang.docUrl + '" target="_black" style="cursor:pointer;color:#00B2CE;text-decoration:underline">' +
									lang.docLabel +
								'</a>' +
							'</div>'
					},
					( !( CKEDITOR.env.ie && CKEDITOR.env.version == 8 ) ) && {
						id: 'preview',
						type: 'html',
						html:
							'<div style="width:100%;text-align:center;">' +
								'<iframe style="border:0;width:0;height:0;font-size:20px" scrolling="no" frameborder="0" allowTransparency="true" src="' + CKEDITOR.plugins.mathjax.fixSrc + '"></iframe>' +
							'</div>',

						onLoad: function() {
							var iFrame = CKEDITOR.document.getById( this.domId ).getChild( 0 );
							preview = new CKEDITOR.plugins.mathjax.frameWrapper( iFrame, editor );
						},

						setup: function( widget ) {
							preview.setValue( widget.data.math );
						}
					}
				]
			}
		]
	};
} );
