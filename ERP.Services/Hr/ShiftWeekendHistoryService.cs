﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Mapping;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{
  
    public interface IShiftWeekendHistoryService : IBaseService
    {
        #region Operational Function
        
        bool Save(ShiftWeekendHistory hrShiftWeekendHistory);
        bool AddShiftWeekendHistory(TeamMember member, long organizationId, long shiftId, string weekend, string effectiveDate);
        bool UpdateShiftWeekendHistory(long id, long organizationId, long shiftId, string weekend, string effectiveDate);
        bool DeleteShiftWeekendHistory(ShiftWeekendHistory shiftWeekendHistory);
        void SaveBatchShiftWeekendHistory(List<ShiftWeekendHistory> teamMemberShiftWeekendHistoryList);

        #endregion

        #region Single Instances Loading Function
        ShiftWeekendHistory GetShiftWeekendHistory(long id);
        #endregion

        #region List Loading Function
        IList<ShiftWeekendHistory> LoadShiftWeekendHistory(long teamMemberId, DateTime? searchingDate = null);

        #endregion

        #region Others Function

        DateTime? GetTeamMemberShiftWeekendHistoryIsPostDateTime(long? teamMemberId = null, int? pin = null);

        #endregion

        #region Helper Function

        #endregion
        
    }
    public class ShiftWeekendHistoryService : BaseService, IShiftWeekendHistoryService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IShiftWeekendHistoryDao _hrShiftWeekendHistoryDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly IShiftDao _hrShiftDao;
        private readonly ITeamMemberDao _teamMemberDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;
        private readonly ISalarySheetDao _salarySheetDao;
        private readonly IAttendanceSummaryDao _attendanceSummaryDao;
        private readonly IDailyAllowanceBlockDao _dailyAllowanceBlockDao;
        private readonly IHolidayWorkDao _holidayWorkDao;
        private readonly IOvertimeDao _overtimeDao;
        private readonly INightWorkDao _nightWorkDao;
        private readonly IAttendanceAdjustmentDao _attendanceAdjustmentDao;
        private readonly ILeaveApplicationDao _leaveApplicationDao;
        private readonly IDayOffAdjustmentDao _dayOffAdjustmentDao;
        
        public ShiftWeekendHistoryService(ISession session)
        {
            Session = session;
            _hrShiftWeekendHistoryDao=new ShiftWeekendHistoryDao(){Session = session};
            _organizationDao = new OrganizationDao {Session = session};
            _hrShiftDao = new ShiftDao {Session = session};
            _teamMemberDao = new TeamMemberDao { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao { Session = session };
            _salarySheetDao = new SalarySheetDao { Session = session };
            _attendanceSummaryDao = new AttendanceSummaryDao { Session = session };
            _dailyAllowanceBlockDao = new DailyAllowanceBlockDao { Session = session };
            _holidayWorkDao = new HolidayWorkDao { Session = session };
            _overtimeDao = new OvertimeDao { Session = session };
            _nightWorkDao = new NightWorkDao { Session = session };
            _attendanceAdjustmentDao = new AttendanceAdjustmentDao { Session = session };
            _leaveApplicationDao = new LeaveApplicationDao { Session = session };
            _dayOffAdjustmentDao = new DayOffAdjustmentDao { Session = session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Functions

        public bool Save(ShiftWeekendHistory hrShiftWeekendHistory)
        {
            ITransaction trans = null;
            try
            {
                if (hrShiftWeekendHistory == null) throw new InvalidDataException("Invalid data object");
                if (hrShiftWeekendHistory.Id > 0) throw new Exception("Invalid operation");
                using (trans = Session.BeginTransaction())
                {
                    _hrShiftWeekendHistoryDao.Save(hrShiftWeekendHistory);
                    var hrShiftWeekendHistoryLog = new ShiftWeekendHistoryLog();
                    hrShiftWeekendHistoryLog.Weekend = hrShiftWeekendHistory.Weekend;
                    hrShiftWeekendHistoryLog.EffectiveDate = hrShiftWeekendHistory.EffectiveDate;
                    hrShiftWeekendHistoryLog.OrganizationId = hrShiftWeekendHistory.Organization.Id;
                    hrShiftWeekendHistoryLog.ShiftId = hrShiftWeekendHistory.Shift.Id;
                    hrShiftWeekendHistoryLog.TeamMemberId = hrShiftWeekendHistory.TeamMember.Id;
                    hrShiftWeekendHistoryLog.ShiftWeekendHistoryId = hrShiftWeekendHistory.Id;
                    hrShiftWeekendHistoryLog.CreationDate = DateTime.Now;
                    hrShiftWeekendHistoryLog.CreateBy = GetCurrentUserId();
                    Session.Save(hrShiftWeekendHistoryLog);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool AddShiftWeekendHistory(TeamMember member, long organizationId, long shiftId, string weekend, string effectiveDate)
        {
           ITransaction trans = null;
            try
            {
                CheckBeforeSaveOrAdd(member, organizationId, shiftId, weekend,effectiveDate);
                ShiftWeekendHistory shiftWeekendHistory = new ShiftWeekendHistory();
                Organization organization = _organizationDao.LoadById(organizationId);
                Shift hrShift = _hrShiftDao.LoadById(shiftId);
                int weekendId = Convert.ToInt32(weekend);

                using (trans = Session.BeginTransaction())
                {
                    shiftWeekendHistory.TeamMember = member;
                    shiftWeekendHistory.Rank = 10;
                    shiftWeekendHistory.Organization = organization;
                    shiftWeekendHistory.Shift = hrShift;
                    shiftWeekendHistory.Weekend = weekendId;
                    shiftWeekendHistory.EffectiveDate = Convert.ToDateTime(effectiveDate);
                    shiftWeekendHistory.Status = ShiftWeekendHistory.EntityStatus.Active;
                    _hrShiftWeekendHistoryDao.SaveOrUpdate(shiftWeekendHistory);
                    AddShiftWeekendHistoryLog(shiftWeekendHistory);
                    trans.Commit();
                    return true;

                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        public bool UpdateShiftWeekendHistory(long id, long organizationId, long shiftId, string weekend, string effectiveDate)
        {
            ITransaction trans = null;
            try
            {
                if (id <= 0) throw new InvalidDataException("invalid id exception");
                ShiftWeekendHistory shiftWeekendHistory = _hrShiftWeekendHistoryDao.LoadById(id);
                if (shiftWeekendHistory == null) throw new InvalidDataException("Invalid Object");
                TeamMember member = shiftWeekendHistory.TeamMember;
                CheckBeforeSaveOrAdd(member, organizationId, shiftId, weekend, effectiveDate, id);
                bool canAddLog = false;

                using (trans = Session.BeginTransaction())
                {
                    if (shiftWeekendHistory.Organization.Id != organizationId)
                    {
                        Organization organization = _organizationDao.LoadById(organizationId);
                        shiftWeekendHistory.Organization = organization;
                        canAddLog = true;
                    }
                    if (shiftWeekendHistory.Shift.Id != shiftId)
                    {
                        Shift hrShift = _hrShiftDao.LoadById(shiftId);
                        shiftWeekendHistory.Shift = hrShift;
                        canAddLog = true;
                    }
                    if (shiftWeekendHistory.Weekend != Convert.ToInt32(weekend))
                    {
                        shiftWeekendHistory.Weekend = Convert.ToInt32(weekend);
                        canAddLog = true;
                    }
                    if (shiftWeekendHistory.EffectiveDate.Value.Date != Convert.ToDateTime(effectiveDate).Date)
                    {
                        shiftWeekendHistory.EffectiveDate = Convert.ToDateTime(effectiveDate);
                        canAddLog = true;
                    }

                    _hrShiftWeekendHistoryDao.SaveOrUpdate(shiftWeekendHistory);

                    if (canAddLog)
                        AddShiftWeekendHistoryLog(shiftWeekendHistory);

                    trans.Commit();
                    return true;

                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        public bool DeleteShiftWeekendHistory(ShiftWeekendHistory shiftWeekendHistory)
        {
            ITransaction trans = null;
            try
            {
                CheckBeforeDelete(shiftWeekendHistory);
                using (trans = Session.BeginTransaction())
                {
                    shiftWeekendHistory.Status = ShiftWeekendHistory.EntityStatus.Delete;
                    trans.Commit();
                    return true;

                }
            }
            catch (DependencyException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        public void SaveBatchShiftWeekendHistory(List<ShiftWeekendHistory> teamMemberShiftWeekendHistoryList)
        {
            ITransaction trans = null;
            try
            {
                List<ShiftWeekendHistory> validShiftWeekendHistoryList = new List<ShiftWeekendHistory>();
                foreach (ShiftWeekendHistory shiftWeekendHistory in teamMemberShiftWeekendHistoryList)
                {
                    Session.Evict(shiftWeekendHistory);

                    CheckBeforeSaveOrAdd(shiftWeekendHistory.TeamMember, shiftWeekendHistory.Organization.Id, shiftWeekendHistory.Shift.Id, (shiftWeekendHistory.Weekend == null) ? "" : shiftWeekendHistory.Weekend.ToString(), shiftWeekendHistory.EffectiveDate.ToString(), shiftWeekendHistory.Id, true);

                    validShiftWeekendHistoryList.Add(shiftWeekendHistory);
                }
                if (!validShiftWeekendHistoryList.Any())
                {
                    throw new InvalidDataException("No Valid TeamMember Shift Weekend History Found!");
                }
                using (trans = Session.BeginTransaction())
                {
                    foreach (ShiftWeekendHistory shiftWeekendHistory in validShiftWeekendHistoryList)
                    {
                        _hrShiftWeekendHistoryDao.SaveOrUpdate(shiftWeekendHistory);
                        AddShiftWeekendHistoryLog(shiftWeekendHistory);
                    }
                    trans.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }
        
        #endregion

        #region Single Instances Loading Function

        public ShiftWeekendHistory GetShiftWeekendHistory(long id)
        {
            try
            {
                if (id <= 0) throw new InvalidDataException("Invalid team member");
                return _hrShiftWeekendHistoryDao.GetShiftWeekendHistory(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public DateTime? GetTeamMemberShiftWeekendHistoryIsPostDateTime(long? teamMemberId = null, int? pin = null)
        {
            DateTime? postDateTime = null;
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            AllowanceSheet allowanceSheet = _allowanceSheetDao.GetTeamMemberLastIsSubmittedAllowanceSheet(teamMemberId, pin);
            SalarySheet salarySheet = _salarySheetDao.GetTeamMemberLastSalarySheet(teamMemberId, pin);
            AttendanceSummary attendanceSummary = _attendanceSummaryDao.GetTeamMemberLastIsPostAttendanceSummary(teamMemberId, pin);


            if (allowanceSheet != null && salarySheet != null && attendanceSummary != null)
            {
                postDateTime = salarySheet.EndDate;
                if (allowanceSheet.DateTo > postDateTime)
                    postDateTime = allowanceSheet.DateTo;
                if (attendanceSummary.AttendanceDate.Value.Date > postDateTime)
                    postDateTime = attendanceSummary.AttendanceDate.Value.Date;
            }
            else if (allowanceSheet == null)
            {
                if (salarySheet != null && attendanceSummary != null)
                {
                    postDateTime = salarySheet.EndDate;
                    if (attendanceSummary.AttendanceDate.Value.Date > postDateTime)
                        postDateTime = attendanceSummary.AttendanceDate.Value.Date;
                }
                else if (salarySheet == null && attendanceSummary != null)
                {
                    postDateTime = attendanceSummary.AttendanceDate.Value.Date;
                }
                else if (salarySheet != null)
                {
                    postDateTime = salarySheet.EndDate;
                }
            }
            else if (salarySheet == null)
            {
                postDateTime = allowanceSheet.DateTo;
                if (attendanceSummary != null && attendanceSummary.AttendanceDate.Value.Date > postDateTime)
                {
                    postDateTime = attendanceSummary.AttendanceDate.Value.Date;
                }
            }
            else
            {
                postDateTime = allowanceSheet.DateTo;
                if (salarySheet.EndDate > postDateTime)
                {
                    postDateTime = salarySheet.EndDate;
                }
            }
            return postDateTime;
        }

        #endregion

        #region List Loading Function
        public IList<ShiftWeekendHistory> LoadShiftWeekendHistory(long teamMemberId, DateTime? searchingDate = null)
        {
            try
            {
                if(teamMemberId<=0) throw new InvalidDataException("Invalid team member");
                return _hrShiftWeekendHistoryDao.LoadShiftWeekendHistory(teamMemberId, searchingDate).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function


        #endregion

        #region Helper function
        
        private void CheckBeforeDelete(ShiftWeekendHistory shiftWeekendHistory)
        {

            TeamMember teamMember = shiftWeekendHistory.TeamMember;
            DateTime? postDateTime = GetTeamMemberShiftWeekendHistoryIsPostDateTime(teamMember.Id);

            if (postDateTime != null && postDateTime.Value.Date >= shiftWeekendHistory.EffectiveDate.Value.Date)
                throw new InvalidDataException("TeamMember can only delete Shift weekend history after Date (" + postDateTime.Value.ToString("d MMM, yyyy") + ").");

            //already applied allowance check
            List<string> alreadyAppliedString = AllowanceEntryCheck(teamMember.Id, shiftWeekendHistory.EffectiveDate.Value.Date);
            if (alreadyAppliedString.Any())
            {
                throw new InvalidDataException("TeamMember's shift and weekend can only applicable for after Date (" + alreadyAppliedString[1] + ") beacuse " + alreadyAppliedString[0] + " is already applied");
            }

            int noOfShiftWeekendHistory = teamMember.ShiftWeekendHistory.Where(x => x.Status != ShiftWeekendHistory.EntityStatus.Delete).ToList().Count;
            if (noOfShiftWeekendHistory <= 1)
            {
                throw new DependencyException("Team member must have atleast one Shift Weekend history!!");
            }
        }

        private void AddShiftWeekendHistoryLog(ShiftWeekendHistory hrShiftWeekendHistory)
        {
            var hrShiftWeekendHistoryLog = new ShiftWeekendHistoryLog();
            hrShiftWeekendHistoryLog.Weekend = hrShiftWeekendHistory.Weekend;
            hrShiftWeekendHistoryLog.EffectiveDate = hrShiftWeekendHistory.EffectiveDate;
            hrShiftWeekendHistoryLog.OrganizationId = hrShiftWeekendHistory.Organization.Id;
            hrShiftWeekendHistoryLog.ShiftId = hrShiftWeekendHistory.Shift.Id;
            hrShiftWeekendHistoryLog.TeamMemberId = hrShiftWeekendHistory.TeamMember.Id;
            hrShiftWeekendHistoryLog.ShiftWeekendHistoryId = hrShiftWeekendHistory.Id;
            hrShiftWeekendHistoryLog.CreationDate = DateTime.Now;
            hrShiftWeekendHistoryLog.CreateBy = GetCurrentUserId();
            Session.Save(hrShiftWeekendHistoryLog);
        }

        private void CheckBeforeSaveOrAdd(TeamMember member, long organizationId, long shiftId, string weekend, string effectiveDate, long id = 0, bool commonMessageShow = false)
        {
            string commonMessage = "";
            if (member == null)
                throw new InvalidDataException("Invalid TeamMember");
            if (commonMessageShow)
                commonMessage = " for Pin " + member.Pin;
            Organization org = _organizationDao.LoadById(organizationId);
            if (org == null)
                throw new InvalidDataException("Invalid Organization" + commonMessage);
            Shift shift = _hrShiftDao.LoadById(shiftId);
            if (shift == null)
                throw new InvalidDataException("Invalid Shift" + commonMessage);
            List<int> weekDaysValue = new List<int>(Enum.GetValues(typeof(WeekDay)).Cast<int>());
            if (!weekDaysValue.Contains(Convert.ToInt32(weekend)))
            {
                throw new InvalidDataException("Invalid Weekend" + commonMessage);
            }

            DateTime? postDateTime = GetTeamMemberShiftWeekendHistoryIsPostDateTime(member.Id);
            //Is Post
            if (id > 0)
            {
                ShiftWeekendHistory originalShiftWeekendHistory = _hrShiftWeekendHistoryDao.GetShiftWeekendHistory(id);
                if(originalShiftWeekendHistory == null)
                    throw new InvalidDataException("Invalid Shift Weekend History found" + commonMessage);
                if (postDateTime != null && (postDateTime.Value.Date >= Convert.ToDateTime(effectiveDate).Date || postDateTime.Value.Date >= originalShiftWeekendHistory.EffectiveDate.Value.Date))
                    throw new InvalidDataException("TeamMember's shift and weekend can only edit after Date (" + postDateTime.Value.ToString("d MMM, yyyy") + ") because Is Post" + commonMessage);
            }
            else
            {
                if (postDateTime != null && postDateTime.Value.Date >= Convert.ToDateTime(effectiveDate).Date)
                    throw new InvalidDataException("TeamMember's shift and weekend can only edit after Date (" + postDateTime.Value.ToString("d MMM, yyyy") + ") because Is Post" + commonMessage);
            }
            //already applied allowance check
            List<string> alreadyAppliedString = new List<string>();
            alreadyAppliedString = AllowanceEntryCheck(member.Id, Convert.ToDateTime(effectiveDate).Date);
            if (alreadyAppliedString.Any())
            {
                throw new InvalidDataException("TeamMember's shift and weekend can only applicable for after Date (" + alreadyAppliedString[1] + ") beacuse " + alreadyAppliedString[0] + " is already applied" + commonMessage);
            }

            if (!member.EmploymentHistory.Where(x => x.EffectiveDate.Date <= Convert.ToDateTime(effectiveDate).Date && x.Status != EmploymentHistory.EntityStatus.Delete).ToList().Any())
                throw new InvalidDataException("This Team Member has no Employment record in this day" + commonMessage);

            int dublicateDataForSameEffectiveDate = member.ShiftWeekendHistory.Where(x => x.EffectiveDate == Convert.ToDateTime(effectiveDate).Date && x.Id != id).ToList().Count();
            if (dublicateDataForSameEffectiveDate >= 1)
            {
                throw new InvalidDataException("Already there is a Shift Weekend history data for this effective day" + commonMessage);
            }

            Organization currentOrganization = _teamMemberDao.GetTeamMemberOrganization(DateTime.Now, member.Id, null);
            if (currentOrganization != org || currentOrganization != shift.Organization)
            {
                throw new InvalidDataException("Organization Missmatch with current organization!" + commonMessage);
            }

            // check same to previous shift 
            ShiftWeekendHistory currentShiftWeekendHistory = _hrShiftWeekendHistoryDao.GetLastShiftWeekendHistory(member, Convert.ToDateTime(effectiveDate), id);
            if (currentShiftWeekendHistory != null && currentShiftWeekendHistory.Shift.Id == shiftId && currentShiftWeekendHistory.Weekend == Convert.ToInt32(weekend))
            {
                throw new InvalidDataException("Already active this shift and Weekedn in his/her profile! " + commonMessage);
            }

            // check same to future shift 
            ShiftWeekendHistory nextShiftWeekendHistory = _hrShiftWeekendHistoryDao.GetNextShiftWeekendHistory(member, Convert.ToDateTime(effectiveDate), id);
            if (nextShiftWeekendHistory != null && nextShiftWeekendHistory.Shift.Id == shiftId && nextShiftWeekendHistory.Weekend == Convert.ToInt32(weekend))
            {
                throw new InvalidDataException("This shift and Weekend already has as his/her next shift weekend ! " + commonMessage);
            }
        }

        private List<string> AllowanceEntryCheck(long teamMemberId, DateTime effectiveDate)
        {
            List<string> returnStrList = new List<string>();

            //DailyAllowanceBlock 
            DailySupportAllowanceBlock dailySupportAllowanceBlock = _dailyAllowanceBlockDao.GetTeamMemberLastIsBlockedDailySupportAllowanceBlock(teamMemberId);
            if (dailySupportAllowanceBlock != null && dailySupportAllowanceBlock.BlockingDate.Date >= effectiveDate.Date)
            {
                returnStrList.Add("Daily Allowance Block");
                returnStrList.Add(dailySupportAllowanceBlock.BlockingDate.ToString("d MMM, yyyy"));
            }
            //extra day allowance
            if (!returnStrList.Any())
            {
                HolidayWork holidayWork = _holidayWorkDao.GetTeamMemberLastApprovedHolidayWork(teamMemberId);
                if (holidayWork != null && holidayWork.HolidayWorkDate.Value.Date >= effectiveDate.Date)
                {
                    returnStrList.Add("Holiday Work");
                    returnStrList.Add(holidayWork.HolidayWorkDate.Value.Date.ToString("d MMM, yyyy"));
                }
            }
            //Overtime allowance
            if (!returnStrList.Any())
            {
                Overtime overtime = _overtimeDao.GetTeamMemberLastApprovedOvertime(teamMemberId);
                if (overtime != null && overtime.OverTimeDate.Value.Date >= effectiveDate.Date)
                {
                    returnStrList.Add("OverTime");
                    returnStrList.Add(overtime.OverTimeDate.Value.Date.ToString("d MMM, yyyy"));
                }
            }

            //Night Work allowance
            if (!returnStrList.Any())
            {
                NightWork nightWork = _nightWorkDao.GetTeamMemberLastApprovedNightWork(teamMemberId);
                if (nightWork != null && nightWork.NightWorkDate.Value.Date >= effectiveDate.Date)
                {
                    returnStrList.Add("Night Work");
                    returnStrList.Add(nightWork.NightWorkDate.Value.Date.ToString("d MMM, yyyy"));
                }
            }

            //Attandance Adjustment
            if (!returnStrList.Any())
            {
                AttendanceAdjustment attendanceAdjustment = _attendanceAdjustmentDao.GetTeamMemberLastAppliedAttendanceAdjustment(teamMemberId);
                if (attendanceAdjustment != null && attendanceAdjustment.Date.Value.Date >= effectiveDate.Date)
                {
                    returnStrList.Add("Attendance Adjustment");
                    returnStrList.Add(attendanceAdjustment.Date.Value.Date.ToString("d MMM, yyyy"));
                }
            }

            //Leave Application
            if (!returnStrList.Any())
            {
                LeaveApplication leaveApplication = _leaveApplicationDao.GetTeamMemberLastAppliedApplication(teamMemberId);
                if (leaveApplication != null && leaveApplication.DateTo.Date >= effectiveDate.Date)
                {
                    returnStrList.Add("Leave Application");
                    returnStrList.Add(leaveApplication.DateTo.Date.ToString("d MMM, yyyy"));
                }
            }

            //Day off Adjustment
            if (!returnStrList.Any())
            {
                DateTime? dateTimeDayOfAdjustment = _dayOffAdjustmentDao.GetTeamMemberLastAppliedDayOffAdjustment(teamMemberId);
                if (dateTimeDayOfAdjustment != null && dateTimeDayOfAdjustment.Value.Date >= effectiveDate.Date)
                {
                    returnStrList.Add("Day Off Adjustment");
                    returnStrList.Add(dateTimeDayOfAdjustment.Value.Date.ToString("d MMM, yyyy"));
                }
            }




            return returnStrList;
        }

        #endregion
    }
}
