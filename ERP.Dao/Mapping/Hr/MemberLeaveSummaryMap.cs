using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class MemberLeaveSummaryMap : BaseClassMap<MemberLeaveSummary,long> {
        
        public MemberLeaveSummaryMap() {
			Table("HR_MemberLeaveSummary");
			LazyLoad();
			References(x => x.Organization).Cascade.All().Column("OrganizationId");
            References(x => x.Leave).Column("LeaveId");
            References(x => x.TeamMember).Column("TeamMemberId");
            Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
            Map(x => x.TotalLeaveBalance).Column("TotalLeaveBalance");
            Map(x => x.AvailableBalance).Column("AvailableBalance");
			Map(x => x.ApprovedLeave).Column("ApprovedLeave");
			Map(x => x.PendingLeave).Column("PendingLeave");
			Map(x => x.CarryForwardLeave).Column("CarryForwardLeave");
			Map(x => x.CurrentYearLeave).Column("CurrentYearLeave");
			Map(x => x.EncashLeave).Column("EncashLeave");
			Map(x => x.VoidLeave).Column("VoidLeave");
			Map(x => x.Year).Column("Year");
        }
    }
}
