﻿using System.ServiceModel;

namespace Synchronizer.Updater.ProcessCommunication
{
    [ServiceContract(Namespace = "http://onnorokomsoftware.com")]
    interface IIpcOperation
    {
        [OperationContract]
        string GetSoftwareVersion();

        [OperationContract]
        string GetSoftwareName();

        [OperationContract]
        string GetApplicationTitle();

        [OperationContract]
        bool IsServiceRunning();

        [OperationContract]
        void StopService(bool exitApplication = false);
    }
}
