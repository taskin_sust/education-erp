﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.OverTimeAllowanceSettingTest
{
    public class OverTimeAllowanceSettingUpdateTest : OverTimeAllowanceSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Update_Should_Be_Successfull()
        {
            _organizationFactory.Create().Persist();
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
           // _overtimeAllowanceSettingService.SaveOrUpdate(_overtimeAllowanceSettingFactory.Object);
            _overtimeAllowanceSettingService.GetOvertimeAllowanceSetting(_overtimeAllowanceSettingFactory.Object.Id);
            var isPermanent = _overtimeAllowanceSettingFactory.Object.IsPermanent = false;
           // _overtimeAllowanceSettingService.SaveOrUpdate(_overtimeAllowanceSettingFactory.Object,true);
            Assert.Equal(isPermanent, _overtimeAllowanceSettingFactory.Object.IsPermanent);
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void Update_Should_Be_Fail_For_Object()
        {
           // Assert.Throws<NullObjectException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(null,true));
        }

        [Fact]
        public void Update_Should_Be_Fail_For_Null_Model_Validation()
        {
            _organizationFactory.Create().Persist();
            //Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(new OvertimeAllowanceSetting(),true));
        }

        [Fact]
        public void Update_Should_Be_Fail_For_Null_Organization()
        {
            _overtimeAllowanceSettingFactory.Create();
           // Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(_overtimeAllowanceSettingFactory.Object,true));
        }

        [Fact]
        public void Update_Should_Be_Fail_For_Null_Name()
        {
            _organizationFactory.Create().Persist();
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            //_overtimeAllowanceSettingService.SaveOrUpdate(_overtimeAllowanceSettingFactory.Object);
            _overtimeAllowanceSettingService.GetOvertimeAllowanceSetting(_overtimeAllowanceSettingFactory.Object.Id);
            _overtimeAllowanceSettingFactory.Object.Name = null;
           // Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(_overtimeAllowanceSettingFactory.Object,true));
        }

        [Fact]
        public void Update_Should_Be_Fail_For_Employment_Status()
        {
            _organizationFactory.Create().Persist();
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object).WithoutEmploymentStatus();
           // Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(_overtimeAllowanceSettingFactory.Object, true));
        }

        [Fact]
        public void Update_Should_Be_Fail_For_Overtime_Type()
        {
            _organizationFactory.Create().Persist();
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object).WithoutOvertimeType();
            //Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(_overtimeAllowanceSettingFactory.Object, true));
        }

        [Fact]
        public void Update_Should_Be_Fail_For_Invalid_CalculationOn()
        {
            _organizationFactory.Create().Persist();
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            _overtimeAllowanceSettingFactory.Object.MultiplyingFactor = 0;
           // Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(_overtimeAllowanceSettingFactory.Object, true));
        }

        [Fact]
        public void Update_Should_Be_Fail_For_Closing_Date()
        {
            _organizationFactory.Create().Persist();
            _overtimeAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            _overtimeAllowanceSettingFactory.Object.ClosingDate = _overtimeAllowanceSettingFactory.Object.EffectiveDate.AddDays(-10);
           // Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(_overtimeAllowanceSettingFactory.Object, true));
        }

        [Fact]
        public void Update_Should_Be_Fail_For_Equal_Date_Of_Effective_Date_Closing_Date()
        {
            var orgFactory = _organizationFactory.Create().Persist();
            _overtimeAllowanceSettingFactory.Create().WithOrganization(orgFactory.Object);
            _overtimeAllowanceSettingFactory.Object.ClosingDate = _overtimeAllowanceSettingFactory.Object.EffectiveDate;
           // Assert.Throws<InvalidDataException>(() => _overtimeAllowanceSettingService.SaveOrUpdate(_overtimeAllowanceSettingFactory.Object, true));
        }

        #endregion
    }
}
