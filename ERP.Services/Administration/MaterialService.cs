﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface IMaterialService : IBaseService
    {
        #region Operational Function
        bool MaterialSaveOrUpdate(List<Material> materialList);
        bool IsDelete(Material materialObj);
        bool UpdateRank(Material materialGroupOldObj, Material materialUpdateObj);
        #endregion

        #region Single Instances Loading Function
        Material GetMaterial(long id);
        Material GetMaterialByRankNextOrPrevious(int rank, string action);
        #endregion

        #region List Loading Function
        IList<Material> LoadMaterial(long sessionId, long programId);
        IList<Material> LoadMaterial(long[] materialTypeId, long programId, long sessionId);
        IList<Material> LoadMaterial(List<UserMenu> userMenu, int start, int length, string orderBy, string orderDir, string organizationId, string programId, string sessionName, string materialTypeName, string name, string status);
        #endregion

        #region Others Function
        int GetMaterialCount(List<UserMenu> userMenu, string orderBy, string orderDir, string organizationId, string programId, string sessionName, string materialTypeName, string name, string status);
        int GetMaximumRank(Material material);
        int GetMinimumRank(Material material);
        #endregion
    }
    public class MaterialService : BaseService, IMaterialService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IMaterialTypeDao _materialTypeDao;
        private readonly IMaterialDao _materialDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly CommonHelper _commonHelper;
        public MaterialService(ISession session)
        {
            //here Session is called from BaseService
            Session = session;
            _materialTypeDao = new MaterialTypeDao { Session = Session };
            _materialDao = new MaterialDao { Session = Session };
            _programDao = new ProgramDao { Session = Session };
            _sessionDao = new SessionDao { Session = Session };
            _organizationDao = new OrganizationDao { Session = Session };
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Function
        public bool MaterialSaveOrUpdate(List<Material> materialList)
        {
            try
            {
                if (materialList != null)
                {
                    foreach (var material in materialList)
                    {
                        var validationResult =
                            ValidationHelper.ValidateEntity<Material, Material.MaterialMetaData>(material);
                        if (validationResult.HasError)
                        {
                            string errorMessage = "";
                            validationResult.Errors.ForEach(
                                r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                            throw new InvalidDataException(errorMessage);
                        }

                        if ((material.Id < 1))
                        {
                            #region save
                            bool isSave = IsSavedMaterial(material);
                            if (!isSave)
                            {
                                return false;
                            }
                            #endregion
                        }
                        else
                        {
                            #region update
                            var oldmaterial = _materialDao.LoadById(material.Id);
                            if (oldmaterial != null)
                            {
                                oldmaterial.Name = material.Name;
                                oldmaterial.ShortName = material.ShortName;
                                oldmaterial.Status = material.Status;
                                bool isProcess = IsProcessFurther(oldmaterial, oldmaterial.Id);
                                if (isProcess)
                                {
                                    using (var trans = Session.BeginTransaction())
                                    {
                                        try
                                        {
                                            _materialDao.Update(oldmaterial);
                                            trans.Commit();
                                            return true;
                                        }
                                        catch (Exception e)
                                        {
                                            trans.Rollback();
                                            return false;
                                        }
                                    }
                                }
                                return false;
                            }
                            return false;
                            #endregion
                        }
                    }
                    return true;
                }
                else
                {
                    throw new NullObjectException("material can not be null.");
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (DuplicateEntryException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public bool IsSavedMaterial(Material material)
        {
            ITransaction trans = null;
            try
            {
                bool isProcess = IsProcessFurther(material);
                if (isProcess)
                {
                    material.Rank = _materialDao.GetMaximumRank(material) + 1;
                    using (trans = Session.BeginTransaction())
                    {
                        _materialDao.Save(material);
                        trans.Commit();
                    }
                    return true;
                }
                return false;
            }
            catch (DuplicateEntryException deex)
            {
                throw deex;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool IsDelete(Material materialObj)
        {
            ITransaction trans = null;
            try
            {
                if (materialObj.StudentPrograms!=null && materialObj.StudentPrograms.Count > 0)
                {
                    throw new DependencyException("This Material have one or more dependency data.");
                }
                materialObj.Status = Material.EntityStatus.Delete;
                using (trans = Session.BeginTransaction())
                {
                    _materialDao.Update(materialObj);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool UpdateRank(Material materialGroupOldObj, Material materialUpdateObj)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    _materialDao.Update(materialGroupOldObj);
                    _materialDao.Update(materialUpdateObj);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function
        public Material GetMaterial(long id)
        {
            try
            {
                return _materialDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public Material GetMaterialByRankNextOrPrevious(int rank, string action)
        {
            try
            {
                return _materialDao.LoadByRankDirection(rank, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        #endregion

        #region List Loading Function
        public IList<Material> LoadMaterial(List<UserMenu> userMenu, int start, int length, string orderBy, string orderDir, string organizationId, string programId, string sessionName, string materialTypeName, string name, string status)
        {
            try
            {
                long? convertedOrganizationId = String.IsNullOrEmpty(organizationId) ? (long?)null : Convert.ToInt32(organizationId);
                long? convertedProgramId = String.IsNullOrEmpty(programId) ? (long?)null : Convert.ToInt32(programId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (convertedProgramId != null) ? _commonHelper.ConvertIdToList((long)convertedProgramId) : null);

                return _materialDao.LoadMaterial(start, length, orderBy, orderDir, organizationIdList, programIdList, sessionName, materialTypeName, name, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public IList<Material> LoadMaterial(long[] materialTypeId, long programId, long sessionId)
        {
            try
            {
                return _materialDao.LoadMaterial(materialTypeId, programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public IList<Material> LoadMaterial(long sessionId, long programId)
        {
            try
            {
                return _materialDao.LoadMaterial(sessionId, programId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        #endregion

        #region Others Function
        public int GetMaximumRank(Material material)
        {
            int maximumRank;
            try
            {
                maximumRank = _materialDao.GetMaximumRank(material);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return maximumRank;
        }
        public int GetMinimumRank(Material material)
        {
            int minimumRank;
            try
            {
                minimumRank = _materialDao.GetMinimumRank(material);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return minimumRank;
        }
        public int GetMaterialCount(List<UserMenu> userMenu, string orderBy, string orderDir, string organizationId, string programId, string sessionName, string materialTypeName, string name, string status)
        {
            int materialCount;
            try
            {
                long? convertedOrganizationId = String.IsNullOrEmpty(organizationId) ? (long?)null : Convert.ToInt32(organizationId);
                long? convertedProgramId = String.IsNullOrEmpty(programId) ? (long?)null : Convert.ToInt32(programId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (convertedProgramId != null) ? _commonHelper.ConvertIdToList((long)convertedProgramId) : null);

                materialCount = _materialDao.GetMaterialCount(orderBy, orderDir, organizationIdList, programIdList, sessionName, materialTypeName, name, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return materialCount;
        }

        #endregion

        #region Helper Function
        public bool IsProcessFurther(Material material)
        {
            try
            {
                if (!String.IsNullOrEmpty(material.Name))
                {
                    bool isDuplicateMaterial =
                        _materialDao.IsItDuplicateMaterialByProgramAndSessionAndMaterialTypeValue(material.Program, material.Session, material.MaterialType, material.Name, material.ShortName);
                    if (isDuplicateMaterial)
                    {
                        throw new DuplicateEntryException("Duplicate Material Found");
                        //return false;
                    }
                    return true;
                }
                throw new NullObjectException("Material Name is empty");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public bool IsProcessFurther(Material material, long id)
        {
            try
            {
                if (material.Id != 0)
                {
                    bool isDuplicateMaterial =
                        _materialDao.IsItDuplicateMaterialForUpdatingByMaterial(id, material.Program, material.Session, material.MaterialType, material.Name, material.ShortName);
                    if (isDuplicateMaterial == true)
                    {
                        throw new DuplicateEntryException("Duplicate Material Found");
                        //return false;
                    }
                    return true;
                }
                throw new NullObjectException("Material Name is empty");
            }
            catch (Exception e)
            {
                throw e;
                //return false;

            }
        }

        #endregion
    }
}
