﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using NHibernate.Hql.Ast.ANTLR;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Teachers;
using UdvashERP.Services.Teachers;

namespace UdvashERP.Areas.Student.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Student")]
    public class StudentClassEvaluationReportController : Controller
    {
        #region Logger
        private ILog _logger = LogManager.GetLogger("StudentArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly IProgramService _programService;
        private readonly IBranchService _branchService;
        private readonly IBatchService _batchService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly ISubjectService _subjectService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IStudentClassAttendanceService _classAttendanceService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly ICampusService _campusService;
        private readonly IExamsService _examsService;
        private readonly IExamsStudentMarksService _examsStudentMarksService;
        private readonly IExamsDetailsService _examsDetailsService;
        private List<UserMenu> _userMenu;
        private readonly IOrganizationService _organizationService;
        private readonly ITeacherService _teacherService;
        private readonly IStudentClassAttendanceService _studentClassAttendanceService;
        private readonly ILectureService _lectureService;
        private readonly IStudentExamAttendanceService _studentExamAttendanceService;
        private readonly CommonHelper _commonHelper;
        public StudentClassEvaluationReportController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _teacherService = new TeacherService(session);
                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _batchService = new BatchService(session);
                _sessionService = new SessionService(session);
                _classAttendanceService = new StudentClassAttendanceService(session);
                _courseService = new CourseService(session);
                _subjectService = new SubjectService(session);
                _studentProgramService = new StudentProgramService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
                _campusService = new CampusService(session);
                _examsService = new ExamsService(session);
                _examsStudentMarksService = new ExamsStudentMarksService(session);
                _examsDetailsService = new ExamsDetailsService(session);
                _organizationService = new OrganizationService(session);
                _studentClassAttendanceService = new StudentClassAttendanceService(session);
                _studentExamAttendanceService = new StudentExamAttendanceService(session);
                _lectureService = new LectureService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                if (ex.InnerException != null)
                {
                    _logger.Error("Student Class Evaluation Report Controller Constructor Can't be Initialize  due to " + ex.InnerException);
                }
                else
                {
                    _logger.Error("Student Class Evaluation Report Constructor Can't be Initialize  due to " + ex.Message);
                }
                Console.WriteLine("{0}", ex.Message);
            }
        } 
        #endregion
        
        #region Class Evaluation Report
        public ActionResult ClassEvaluation()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //IList<Organization> organizations = _organizationService.LoadOrganization(Organization.EntityStatus.Active);
                IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                SelectList selectList = new SelectList(organizations, "Id", "ShortName");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
                ViewBag.SelectedExam = new SelectList(new List<Exams>(), "Id", "Name");
                ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
                ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
                ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
                ViewBag.orgList = selectList;
                ViewBag.SelectedLecture = new SelectList(new List<Subject>(), "Id", "Name");
                ViewBag.SelectedTeacher = new SelectList(new List<Subject>(), "Id", "Name");
                ViewBag.SelectedSubject = new SelectList(new List<Subject>(), "Id", "Name");
                IList<SelectListItem> generatedBtyList = new List<SelectListItem>()
                {
                    new SelectListItem() { Text = "Lecturewise", Value = "1" },
                    new SelectListItem() { Text = "Teacherwise", Value = "2" }
                };
                ViewBag.SelectedGenerateBy = new SelectList(generatedBtyList,"Value","Text");
                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        }
        [HttpPost]
        //public ActionResult ClassEvauation(StudentClassSurveyReportViewModel studentClassSurveyReportViewModel,string lectureIds,string teacherIds)
        public ActionResult ClassEvaluation(StudentClassSurveyReportViewModel studentClassSurveyReportViewModel, string lectureIds, string teacherIds)
        {
            try
            {
                var program = _programService.GetProgram(studentClassSurveyReportViewModel.ProgramId);
                var session = _sessionService.LoadById(studentClassSurveyReportViewModel.SessionId);
                var batchName = "";
                var branchName = "";
                var batchDays = "";
                var campusName = "";
                var batchTime = "";
                var courseName = "";
                var subjectName = "";
                var teacherName = "";
                var lectureName = "";
                if (!studentClassSurveyReportViewModel.SelectedBatchDays.Contains("0"))
                {
                    string batchDaysString = "";
                    foreach (var bd in studentClassSurveyReportViewModel.SelectedBatchDays)
                    {
                        batchDaysString += bd + "; ";
                    }
                    batchDaysString = batchDaysString.Remove(batchDaysString.Length - 2);
                    batchDays = batchDaysString;
                }
                else
                {
                    batchDays = "ALL";
                }
                if (studentClassSurveyReportViewModel.SelectedBatch[0] == 0)
                {
                    batchName = "ALL";
                }
                else
                {
                    IList<Batch> batches = _batchService.LoadBatch(null, null, null, null, null, studentClassSurveyReportViewModel.SelectedBatch.ToList());
                    batchName = string.Join(", ", batches.Select(x => x.Name.Trim()).Distinct().ToList());
                }
                if (studentClassSurveyReportViewModel.SelectedBatchTime[0] == "0")
                {
                    batchTime = "ALL";
                }
                else
                {
                    var tempBatchTime = new string[studentClassSurveyReportViewModel.SelectedBatchTime.Length];

                    for (int i = 0; i < studentClassSurveyReportViewModel.SelectedBatchTime.Length; i++)
                    {
                        var spl =new[] {"To"};
                        var dateFrom = Convert.ToDateTime(studentClassSurveyReportViewModel.SelectedBatchTime[i].Split(spl,StringSplitOptions.None)[0]).ToString("hh:mm tt");
                        var dateTo = Convert.ToDateTime(studentClassSurveyReportViewModel.SelectedBatchTime[i].Split(spl,StringSplitOptions.None)[1]).ToString("hh:mm tt");
                        tempBatchTime[i] = dateFrom + " to " + dateTo;
                    }
                    batchTime = string.Join(", ", tempBatchTime.Select(x => x.Trim()).ToArray().Distinct());
                }

                if (studentClassSurveyReportViewModel.SelectedBranch[0] == 0)
                {
                    branchName = "ALL";
                }
                else
                {
                    IList<Branch> branches = _branchService.LoadBranch(null,null,null,studentClassSurveyReportViewModel.SelectedBranch.ToList());
                    branchName = string.Join(", ", branches.Select(x => x.Name.Trim()).Distinct().ToList());
                }

                if (studentClassSurveyReportViewModel.SelectedCampus[0] == 0)
                {
                    campusName = "ALL";
                }
                else
                {
                    IList<Campus> campuses = _campusService.LoadCampus(null, null, null, null,studentClassSurveyReportViewModel.SelectedCampus.ToList());
                    campusName = string.Join(", ", campuses.Select(x => x.Name.Trim()).Distinct().ToList());
                }

                if (studentClassSurveyReportViewModel.SelectedCourse[0] == 0)
                {
                    courseName = "ALL";
                }
                else
                {
                    IList<Course> courses = _courseService.LoadCourse(studentClassSurveyReportViewModel.SelectedCourse);
                    courseName = string.Join(", ", courses.Select(x => x.Name.Trim()).Distinct().ToList());
                }
                if (studentClassSurveyReportViewModel.SelectedSubject[0] == 0)
                {
                    subjectName = "ALL";
                }
                else
                {
                    subjectName = string.Join(", ", _subjectService.LoadSubject().Where(x => x.Id.In(studentClassSurveyReportViewModel.SelectedSubject)).Select(x => x.ShortName.Trim()).Distinct().ToList());

                }
                if (studentClassSurveyReportViewModel.SelectedLecture[0] == 0)
                {
                    lectureName = "ALL";
                }
                else
                {
                    lectureName = string.Join(", ", _lectureService.LoadLecture(studentClassSurveyReportViewModel.SelectedLecture).Select(x => x.LectureName.Trim()).Distinct().ToList());

                }
                if (studentClassSurveyReportViewModel.SelectedTeacher[0] == 0)
                {
                    teacherName = "ALL";
                }
                else
                {
                    teacherName = string.Join(", ", _teacherService.LoadTeacher(studentClassSurveyReportViewModel.SelectedTeacher).Select(x => x.DisplayTeacherName.Trim()).Distinct().ToList());

                }
                ViewBag.Subject = subjectName;
                var json = new JavaScriptSerializer().Serialize(studentClassSurveyReportViewModel);
                ViewData["studentClassSurveyReportViewModel"] = json;
                ViewBag.ProgramSession = program.Name + "-" + session.Name;
                ViewBag.Branch = branchName;
                ViewBag.BatchName = batchName;
                ViewBag.BatchDays = batchDays;
                ViewBag.CampusName = campusName;
                ViewBag.BatchTime = batchTime;
                ViewBag.Lecture = lectureName;
                ViewBag.Teacher = teacherName;
                ViewBag.FromDate = studentClassSurveyReportViewModel.FromTime.ToString("dd-MM-yyyy");
                ViewBag.ToDate = studentClassSurveyReportViewModel.ToTime.ToString("dd-MM-yyyy");
                ViewBag.Course = courseName;
                ViewBag.informationViewList = studentClassSurveyReportViewModel.InformationViewList;
                ViewBag.lectureIds = lectureIds;
                ViewBag.teacherIds = teacherIds;
                ViewBag.GenerateBy = studentClassSurveyReportViewModel.GenerateBy;
             //   return View();
                return View("ClassEvaluationReportlist");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }

        }
        [HttpPost]
       
        #region Render Data Table
        public ActionResult ClassEvaluationReport(int draw, int start, int length, string studentClassSurveyReportViewModelObj,string lectureIds,string teacherIds)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                long[] lectureIdArray = null;
                long[] teacherIdArray = null;
                if (!String.IsNullOrEmpty(studentClassSurveyReportViewModelObj))
                {
                    var studentClassSurveyReportViewModel =
                        new JavaScriptSerializer().Deserialize<StudentClassSurveyReportViewModel>(
                            studentClassSurveyReportViewModelObj);
                    studentClassSurveyReportViewModel.FromTime = Convert.ToDateTime(studentClassSurveyReportViewModel.FromTime.AddDays(1).ToString("dd-MMM-yyyy"));
                    studentClassSurveyReportViewModel.ToTime = Convert.ToDateTime(studentClassSurveyReportViewModel.ToTime.AddDays(1).ToString("dd-MMM-yyyy"));
                
                    if (studentClassSurveyReportViewModel.SelectedLecture.Contains(SelectionType.SelelectAll))
                    {
                       lectureIdArray= lectureIds.Split(',').ToList().Select(x => Convert.ToInt64(x)).ToArray();
                    }
                    else
                    {

                        lectureIdArray = studentClassSurveyReportViewModel.SelectedLecture;
                    }
                    if (studentClassSurveyReportViewModel.SelectedTeacher.Contains(SelectionType.SelelectAll))
                    {
                        teacherIdArray = teacherIds.Split(',').ToList().Select(x => Convert.ToInt64(x)).ToArray();
                    }
                    else
                    {

                        teacherIdArray = studentClassSurveyReportViewModel.SelectedTeacher;
                    }
                    var classEvaluationReport=_studentClassAttendanceService.LoadClassEvaluation(_userMenu,studentClassSurveyReportViewModel.OrganizationId, studentClassSurveyReportViewModel.ProgramId, studentClassSurveyReportViewModel.SessionId, studentClassSurveyReportViewModel.SelectedBranch, studentClassSurveyReportViewModel.SelectedCampus, studentClassSurveyReportViewModel.SelectedBatchDays, studentClassSurveyReportViewModel.SelectedBatchTime, studentClassSurveyReportViewModel.SelectedBatch,
                     lectureIdArray, teacherIdArray, studentClassSurveyReportViewModel.FromTime, studentClassSurveyReportViewModel.ToTime, studentClassSurveyReportViewModel.GenerateBy, start, length);
                    
                    int recordsTotal = _studentClassAttendanceService.GetClassEvaluationReportCount(_userMenu, studentClassSurveyReportViewModel.OrganizationId, studentClassSurveyReportViewModel.ProgramId, studentClassSurveyReportViewModel.SessionId, studentClassSurveyReportViewModel.SelectedBranch, studentClassSurveyReportViewModel.SelectedCampus, studentClassSurveyReportViewModel.SelectedBatchDays, studentClassSurveyReportViewModel.SelectedBatchTime, studentClassSurveyReportViewModel.SelectedBatch,
                     lectureIdArray, teacherIdArray, studentClassSurveyReportViewModel.FromTime, studentClassSurveyReportViewModel.ToTime, studentClassSurveyReportViewModel.GenerateBy); 
                    long recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    int sl = start;
                    foreach (var classEvaluation in classEvaluationReport)
                    {
                        //sl++;
                        var str = new List<string>();
                        var teacher = _teacherService.GetTeacher(classEvaluation.TeacherId);
                        //str.Add(sl.ToString());
                        if (studentClassSurveyReportViewModel.GenerateBy==1)
                        {
                            str.Add(_lectureService.GetLecture(classEvaluation.LectureId).LectureName); 
                        }
                        str.Add(teacher.DisplayTeacherName);
                        str.Add(teacher.PersonalMobile);
                        str.Add(classEvaluation.Good);
                        str.Add(classEvaluation.Average);
                        str.Add(classEvaluation.Bad);
                        str.Add(classEvaluation.Total.ToString());
                        str.Add(classEvaluation.TeacherLate.ToString());
                        if (studentClassSurveyReportViewModel.GenerateBy==1)
                        {
                            str.Add("<a href='" +
                                                    Url.Action("IndividualClassEvaluationReport", "StudentClassEvaluationReport") +
                                                   "?lectureId=" + classEvaluation.LectureId + "&teacherId=" + classEvaluation.TeacherId + "&evaluationObj=" + studentClassSurveyReportViewModelObj + "' class='glyphicon glyphicon-th-list'></a>");
                            
                        }
                        data.Add(str);
                        
                    }

                    ViewData["studentExamSurveyReportViewModel"] = studentClassSurveyReportViewModel;
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 1,
                    recordsFiltered = 1,
                    start = start,
                    length = length,
                    data = new List<string>()
                });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        }  
        #endregion
        #endregion
        
        #region Export Class Evaluation Report To Excel
        public ActionResult ExportClassEvaluationReport(string programSessionHeader, string branchHeader, string campusHeader, string batchDayHeader, string batchTimeHeader, string batchHeader, string courseHeader, string subjectHeader, string teacherHeader, string dateRangeHeader, string lectureHeader, string studentClassSurveyReportViewModelObj, string lectureIds, string teacherIds)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (!String.IsNullOrEmpty(studentClassSurveyReportViewModelObj))
                {
                    var studentClassSurveyReportViewModel =
                        new JavaScriptSerializer().Deserialize<StudentClassSurveyReportViewModel>(
                            studentClassSurveyReportViewModelObj);
                    studentClassSurveyReportViewModel.FromTime = Convert.ToDateTime(studentClassSurveyReportViewModel.FromTime.AddDays(1).ToString("dd-MMM-yyyy"));
                    studentClassSurveyReportViewModel.ToTime = Convert.ToDateTime(studentClassSurveyReportViewModel.ToTime.AddDays(1).ToString("dd-MMM-yyyy"));
                    long[] lectureIdArray = null;
                    long[] teacherIdArray = null;
                    if (studentClassSurveyReportViewModel.SelectedLecture.Contains(SelectionType.SelelectAll))
                    {
                        lectureIdArray = lectureIds.Split(',').ToList().Select(x => Convert.ToInt64(x)).ToArray();
                    }
                    else
                    {

                        lectureIdArray = studentClassSurveyReportViewModel.SelectedLecture;
                    }
                    if (studentClassSurveyReportViewModel.SelectedTeacher.Contains(SelectionType.SelelectAll))
                    {
                        teacherIdArray = teacherIds.Split(',').ToList().Select(x => Convert.ToInt64(x)).ToArray();
                    }
                    else
                    {

                        teacherIdArray = studentClassSurveyReportViewModel.SelectedTeacher;
                    }
                    var studentClassEvaluationReport =
                                             _studentClassAttendanceService.LoadClassEvaluation(
                                                 _userMenu, studentClassSurveyReportViewModel.OrganizationId, studentClassSurveyReportViewModel.ProgramId, studentClassSurveyReportViewModel.SessionId, studentClassSurveyReportViewModel.SelectedBranch, studentClassSurveyReportViewModel.SelectedCampus, studentClassSurveyReportViewModel.SelectedBatchDays, studentClassSurveyReportViewModel.SelectedBatchTime, studentClassSurveyReportViewModel.SelectedBatch,
                     lectureIdArray, teacherIdArray, studentClassSurveyReportViewModel.FromTime, studentClassSurveyReportViewModel.ToTime,studentClassSurveyReportViewModel.GenerateBy, 0, 0);

                    var headerList = new List<string>();
                    headerList.Add(programSessionHeader);
                    headerList.Add("Class Survey Report");
                    headerList.Add("Branch : " + branchHeader);
                    headerList.Add("Campus : " + campusHeader);
                    headerList.Add("Batch Days : " + batchDayHeader);
                    headerList.Add("Batch Time : " + batchTimeHeader);
                    headerList.Add("Batch : " + batchHeader);
                    //headerList.Add("Course Type : ");
                    headerList.Add("Course : " + courseHeader);
                    headerList.Add("Subject : " + subjectHeader);
                    headerList.Add("Teacher : " + teacherHeader);
                    headerList.Add("Lecture : " + lectureHeader);
                    headerList.Add("Date Range : " + dateRangeHeader);
                    headerList.Add("");
                    var columnList = new List<string>();
                    if (studentClassSurveyReportViewModel.GenerateBy==1)
                    {
                        columnList.Add("Lecture Name"); 
                    }
                    columnList.Add("Nick Name");
                    columnList.Add("Personal Mobile");
                    columnList.Add("Good");
                    columnList.Add("Average");
                    columnList.Add("Bad");
                    columnList.Add("Total");
                    columnList.Add("Teacher Late");
                    var footerList = new List<string>();
                    footerList.Add("");
                    var studentExcelList = new List<List<object>>();
                    foreach (var classEvaluation in studentClassEvaluationReport)
                    {
                        var str = new List<string>();
                        
                        var xlsRow = new List<object>();
                        if (studentClassSurveyReportViewModel.GenerateBy==1)
                        {
                            xlsRow.Add(_lectureService.GetLecture(classEvaluation.LectureId).LectureName); 
                        }
                        var teacher = _teacherService.GetTeacher(classEvaluation.TeacherId);
                        xlsRow.Add(teacher.DisplayTeacherName);
                        xlsRow.Add(teacher.PersonalMobile);
                        xlsRow.Add(classEvaluation.Good);
                        xlsRow.Add(classEvaluation.Average);
                        xlsRow.Add(classEvaluation.Bad);
                        xlsRow.Add(classEvaluation.Total.ToString());
                        xlsRow.Add(classEvaluation.TeacherLate.ToString());
                        studentExcelList.Add(xlsRow);
                    }

                    ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "class-survey-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));


                }

                return View("AutoClose");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
        } 
        #endregion
        
        #region Individual Report
       [HttpGet]
       public ActionResult IndividualClassEvaluationReport(long lectureId, long teacherId, string evaluationObj)
       {
           try
           {
               var studentClassSurveyReportViewModel =
                       new JavaScriptSerializer().Deserialize<StudentClassSurveyReportViewModel>(
                           evaluationObj);
               studentClassSurveyReportViewModel.FromTime = Convert.ToDateTime(studentClassSurveyReportViewModel.FromTime.AddDays(1).ToString("dd-MMM-yyyy"));
               studentClassSurveyReportViewModel.ToTime = Convert.ToDateTime(studentClassSurveyReportViewModel.ToTime.AddDays(1).ToString("dd-MMM-yyyy"));
                
               var program = _programService.GetProgram(studentClassSurveyReportViewModel.ProgramId);
               var session = _sessionService.LoadById(studentClassSurveyReportViewModel.SessionId);
               var batchName = "";
               var branchName = "";
               var batchDays = "";
               var campusName = "";
               var batchTime = "";
               var courseName = "";
               var subjectName = "";
               var teacherName = "";
               if (!studentClassSurveyReportViewModel.SelectedBatchDays.Contains("0"))
               {
                   string batchDaysString = "";
                   foreach (var bd in studentClassSurveyReportViewModel.SelectedBatchDays)
                   {
                       batchDaysString += bd + "; ";
                   }
                   batchDaysString = batchDaysString.Remove(batchDaysString.Length - 2);
                   batchDays = batchDaysString;
               }
               else
               {
                   batchDays = "ALL";
               }
               if (studentClassSurveyReportViewModel.SelectedBatch[0] == 0)
               {
                   batchName = "ALL";
               }
               else
               {
                   IList<Batch> batches = _batchService.LoadBatch(null, null, null, null, null, studentClassSurveyReportViewModel.SelectedBatch.ToList());
                   batchName = string.Join(", ", batches.Select(x => x.Name.Trim()).Distinct().ToList());

               }

               // batchTime = studentClassSurveyReportViewModel.SelectedBatchTime[0] == "0" ? "ALL" : string.Join(" ; ", studentClassSurveyReportViewModel.SelectedBatchTime.ToArray());
               if (studentClassSurveyReportViewModel.SelectedBatchTime[0] == "0")
               {
                   batchTime = "ALL";
               }
               else
               {
                   for (int i = 0; i < studentClassSurveyReportViewModel.SelectedBatchTime.Length; i++)
                   {
                       var spl = new[] {"To"};
                       var dateFrom = Convert.ToDateTime(studentClassSurveyReportViewModel.SelectedBatchTime[i].Split(spl,StringSplitOptions.None)[0]).ToString("hh:mm tt");
                       var dateTo = Convert.ToDateTime(studentClassSurveyReportViewModel.SelectedBatchTime[i].Split(spl,StringSplitOptions.None)[1]).ToString("hh:mm tt");
                       studentClassSurveyReportViewModel.SelectedBatchTime[i] = dateFrom + " to " + dateTo;
                   }
                   batchTime = string.Join(", ", studentClassSurveyReportViewModel.SelectedBatchTime.Select(x => x.Trim()).ToArray().Distinct());

               }

               if (studentClassSurveyReportViewModel.SelectedBranch[0] == 0)
               {
                   branchName = "ALL";
               }
               else
               {
                   IList<Branch> branches = _branchService.LoadBranch(null,null,null,studentClassSurveyReportViewModel.SelectedBranch.ToList());
                   branchName = string.Join(", ", branches.Select(x => x.Name.Trim()).Distinct().ToList());

               }

               if (studentClassSurveyReportViewModel.SelectedCampus[0] == 0)
               {
                   campusName = "ALL";
               }
               else
               {
                   IList<Campus> campuses = _campusService.LoadCampus(null, null, null, null, studentClassSurveyReportViewModel.SelectedCampus.ToList());
                   campusName = string.Join(", ", campuses.Select(x => x.Name.Trim()).Distinct().ToList());

               }

               if (studentClassSurveyReportViewModel.SelectedCourse[0] == 0)
               {
                   courseName = "ALL";
               }
               else
               {
                   IList<Course> courses = _courseService.LoadCourse(studentClassSurveyReportViewModel.SelectedCourse);
                   courseName = string.Join(", ", courses.Select(x => x.Name.Trim()).Distinct().ToList());

               }
               subjectName = _lectureService.GetLecture(lectureId).LectureSettings.CourseSubject.Subject.ShortName;
               long[] teacherIds = new[] { teacherId };
               teacherName = _teacherService.LoadTeacher(teacherIds).Single().DisplayTeacherName;
               var json = new JavaScriptSerializer().Serialize(studentClassSurveyReportViewModel);
               ViewData["studentClassSurveyReportViewModel"] = json;
               ViewData["lectureId"] = lectureId;
               ViewData["teacherId"] = teacherId;
               ViewBag.informationViewList = studentClassSurveyReportViewModel.InformationViewList;
               ViewBag.ProgramSession = program.Name + "-" + session.Name;
               ViewBag.Branch = branchName;
               ViewBag.BatchName = batchName;
               ViewBag.BatchDays = batchDays;
               ViewBag.CampusName = campusName;
               ViewBag.BatchTime = batchTime;
               ViewBag.Subject = subjectName;
               ViewBag.Teacher = teacherName;
               ViewBag.FromDate = studentClassSurveyReportViewModel.FromTime.ToString("dd-MM-yyyy");
               ViewBag.ToDate = studentClassSurveyReportViewModel.ToTime.ToString("dd-MM-yyyy");
               ViewBag.Course = courseName;
               ViewBag.informationViewList = studentClassSurveyReportViewModel.InformationViewList;
               return View();
           }
           catch (Exception e)
           {
               _logger.Error(e.Message);
               return View();
           }

       }

       #region Render Data Table
       [HttpPost]
       public ActionResult ClassEvaluaionIndividualReport(int draw, int start, int length, long lectureId, long teacherId, string evaluationObj)
       {
           try
           {
               _userMenu = (List<UserMenu>)ViewBag.UserMenu;
               var studentClassSurveyReportViewModel =
                        new JavaScriptSerializer().Deserialize<StudentClassSurveyReportViewModel>(
                            evaluationObj);
               var individualReport = new IndividualStudentExamEvaluationReport();
               IList<StudentProgram> studentProgramList =
                   _studentProgramService.LoadStudentProgramByCriteriaForClassEvaluation(start, length, _userMenu,studentClassSurveyReportViewModel.OrganizationId, studentClassSurveyReportViewModel.ProgramId, studentClassSurveyReportViewModel.SessionId, studentClassSurveyReportViewModel.SelectedBranch, studentClassSurveyReportViewModel.SelectedCampus, studentClassSurveyReportViewModel.SelectedBatchDays, studentClassSurveyReportViewModel.SelectedBatchTime, lectureId, teacherId,
                       studentClassSurveyReportViewModel.SelectedBatch, studentClassSurveyReportViewModel.FromTime, studentClassSurveyReportViewModel.ToTime);
               

               int recordsTotal =
                   _studentProgramService.LoadStudentProgramCountForClassEvaluation(_userMenu, studentClassSurveyReportViewModel.OrganizationId, studentClassSurveyReportViewModel.ProgramId, studentClassSurveyReportViewModel.SessionId, studentClassSurveyReportViewModel.SelectedBranch, studentClassSurveyReportViewModel.SelectedCampus, studentClassSurveyReportViewModel.SelectedBatchDays, studentClassSurveyReportViewModel.SelectedBatchTime, lectureId, teacherId,
                       studentClassSurveyReportViewModel.SelectedBatch, studentClassSurveyReportViewModel.FromTime, studentClassSurveyReportViewModel.ToTime);
               int recordsFiltered = recordsTotal;
               var data = new List<object>();
               int sl = start;
               foreach (var studentProgram in studentProgramList)
               {
                   sl++;
                   var c = studentProgram;
                   var str = new List<string>();
                   string lectureName = _lectureService.GetLecture(lectureId).LectureName;
                   str.Add(sl.ToString());
                   str.Add(lectureName);
                   if (studentClassSurveyReportViewModel.InformationViewList != null)
                   {

                       foreach (string informationView in studentClassSurveyReportViewModel.InformationViewList)
                       {
                           switch (informationView)
                           {
                               case "Program Roll":
                                   str.Add(c.PrnNo);
                                   break;
                               case "Student's Name":
                                   if (c.Student != null && c.Student.FullName != null)
                                   {
                                       str.Add(c.Student.FullName);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Nick Name":
                                   if (c.Student != null && c.Student.NickName != null)
                                   {
                                       str.Add(c.Student.NickName);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Mobile Number (Student)":
                                   if (c.Student != null && c.Student.Mobile != null)
                                   {
                                       str.Add(c.Student.Mobile);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Mobile Number (Father)":
                                   if (c.Student != null && c.Student.GuardiansMobile1 != null)
                                   {
                                       str.Add(c.Student.GuardiansMobile1);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Mobile Number (Mother)":
                                   if (c.Student != null && c.Student.GuardiansMobile2 != null)
                                   {
                                       str.Add(c.Student.GuardiansMobile2);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Father's Name":
                                   if (c.Student != null && c.Student.FatherName != null)
                                   {
                                       str.Add(c.Student.FatherName);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Institute Name":
                                   if (c.Student.StudentPrograms[0].Institute != null)
                                   {
                                       str.Add(c.Student.StudentPrograms[0].Institute.Name);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Email":
                                   if (c.Student != null && c.Student.Email != null)
                                   {
                                       str.Add(c.Student.Email);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Branch":
                                   if (c.Batch != null && c.Batch.Branch != null)
                                   {
                                       str.Add(c.Batch.Branch.Name);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Campus":
                                   if (c.Student != null && c.Batch.Campus != null)
                                   {
                                       str.Add(c.Batch.Campus.Name);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Batch Days":
                                   if (c.Student != null && c.Batch.Days != null)
                                   {
                                       str.Add(c.Batch.Days);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Batch Time":
                                   if (c.Student != null && c.Batch.BatchTime != null)
                                   {
                                       str.Add(c.Batch.FormatedBatchTime);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               
                               case "Batch":
                                   if (c.Batch != null && c.Batch.Name != null)
                                   {
                                       str.Add(c.Batch.Name);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Version of Study":
                                   if (c.VersionOfStudy != null)
                                   {
                                       str.Add(((VersionOfStudy)c.VersionOfStudy).ToString());
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Gender":
                                   if (c.Student != null && c.Student.Gender != null)
                                   {
                                       str.Add(((Gender)c.Student.Gender).ToString());
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Religion":
                                   if (c.Student != null && c.Student.Religion != null)
                                   {
                                       str.Add(((Religion)c.Student.Religion).ToString());
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               default:
                                   break;
                           }

                       }
                   }
                   //foreach ( var )
                   //{
                       
                   //}

                   var classAttendanceDetails = c.StudentClassAttendanceDetails.Where(
                       x =>
                           x.StudentClassAttendence != null && x.StudentClassAttendence.Lecture != null &&
                           x.StudentClassAttendence.Teacher != null && x.StudentClassAttendence.Lecture.Id == lectureId &&
                           x.StudentClassAttendence.Teacher.Id == teacherId).ToList();
                           int feedback =classAttendanceDetails.FirstOrDefault().StudentFeedback;
                   string feedbackString = "";
                   if (feedback!=null)
                   {
                       if (feedback == 1)
                       {
                           feedbackString = "Good";
                       }
                       if (feedback == 2)
                       {
                           feedbackString = "Average";
                       }
                       if (feedback == 3)
                       {
                           feedbackString = "Bad";
                       }
                   }
                   
                   str.Add(feedbackString); 
                   data.Add(str);
               }
               return Json(new
               {
                   draw = draw,
                   recordsTotal = recordsTotal,
                   recordsFiltered = recordsFiltered,
                   start = start,
                   length = length,
                   data = data
               });

           }
           catch (Exception ex)
           {
               _logger.Error(ex);
               var data = new List<object>();
               return Json(new
               {
                   draw = draw,
                   recordsTotal = 0,
                   recordsFiltered = 0,
                   start = start,
                   length = length,
                   data = data
               });

           }
       }  
       #endregion
       #endregion

        #region Export Individual Report To Excel
       public ActionResult ExportClassEvaluaionIndividualReport(string programSessionHeader, string branchHeader, string campusHeader, string batchDayHeader, string batchTimeHeader, string batchHeader, string courseHeader, string subjectHeader, string teacherHeader, string dateRangeHeader, long lectureId, long teacherId, string evaluationObj, string dateFrom, string dateTo)
       {
           try
           {
               _userMenu = (List<UserMenu>)ViewBag.UserMenu;
               var studentClassSurveyReportViewModel =
                        new JavaScriptSerializer().Deserialize<StudentClassSurveyReportViewModel>(
                            evaluationObj);
               studentClassSurveyReportViewModel.FromTime = Convert.ToDateTime(studentClassSurveyReportViewModel.FromTime.AddDays(1).ToString("dd-MMM-yyyy"));
               studentClassSurveyReportViewModel.ToTime = Convert.ToDateTime(studentClassSurveyReportViewModel.ToTime.AddDays(1).ToString("dd-MMM-yyyy"));
                
               var individualReport = new IndividualStudentExamEvaluationReport();
               var headerList = new List<string>();
               headerList.Add(programSessionHeader);
               headerList.Add("Class Survey Report");
               headerList.Add("Branch : " + branchHeader);
               headerList.Add("Campus : " + campusHeader);
               headerList.Add("Batch Days : " + batchDayHeader);
               headerList.Add("Batch Time : " + batchTimeHeader);
               headerList.Add("Batch : " + batchHeader);
               //headerList.Add("Course Type : ");
               headerList.Add("Course : " + courseHeader);
               headerList.Add("Subject : " + subjectHeader);
               headerList.Add("Teacher : " + teacherHeader);
               headerList.Add("Date Range : " + dateRangeHeader);
               headerList.Add("");
               var columnList = new List<string>();
               columnList.Add("Lecture Name");
               var informationViewList = studentClassSurveyReportViewModel.InformationViewList;
               if (informationViewList != null)
               {
                   foreach (var info in informationViewList)
                   {
                       columnList.Add(info);
                   }
                   #region old code
                   //if (informationViewList.Contains("Program Roll"))
                   //{
                   //    columnList.Add("Program Roll");
                   //}
                   //if (informationViewList.Contains("Student's Name"))
                   //{
                   //    columnList.Add("Full Name");
                   //}
                   //if (informationViewList.Contains("Nick Name"))
                   //{
                   //    columnList.Add("Nick Name");
                   //}
                   //if (informationViewList.Contains("Mobile Number (Student)"))
                   //{
                   //    columnList.Add("Mobile Number (Student)");
                   //}
                   //if (informationViewList.Contains("Mobile Number (Father)"))
                   //{
                   //    columnList.Add("Mobile Number (Father)");
                   //}
                   //if (informationViewList.Contains("Mobile Number (Mother)"))
                   //{
                   //    columnList.Add("Mobile Number (Mother)");
                   //}
                   //if (informationViewList.Contains("Father's Name"))
                   //{
                   //    columnList.Add("Father's Name");
                   //}
                   //if (informationViewList.Contains("Institute Name"))
                   //{
                   //    columnList.Add("Institute Name");
                   //}
                   //if (informationViewList.Contains("Email"))
                   //{
                   //    columnList.Add("Email");
                   //}
                   //if (informationViewList.Contains("Branch"))
                   //{
                   //    columnList.Add("Branch");
                   //}
                   //if (informationViewList.Contains("Batch"))
                   //{
                   //    columnList.Add("Batch");
                   //}
                   //if (informationViewList.Contains("Version of Study"))
                   //{
                   //    columnList.Add("Version of Study");
                   //}
                   //if (informationViewList.Contains("Gender"))
                   //{
                   //    columnList.Add("Gender");

                   //}
                   //if (informationViewList.Contains("Religion"))
                   //{
                   //    columnList.Add("Religion");
                   //} 
                   #endregion

               }
               columnList.Add("Answer");

               var footerList = new List<string>();
               footerList.Add("");
               //footerList.Add("Developed By OnnoRokom Software Ltd.");

               DateTime _dateFrom = DateTime.ParseExact(dateFrom.Trim(), "dd-MM-yyyy",
                                  CultureInfo.InvariantCulture);
               DateTime _dateTo = DateTime.ParseExact(dateTo.Trim(), "dd-MM-yyyy",
                                  CultureInfo.InvariantCulture);
               
               IList<StudentProgram> studentProgramList =
                  _studentProgramService.LoadStudentProgramByCriteriaForClassEvaluation(0, 0, _userMenu, studentClassSurveyReportViewModel.OrganizationId, studentClassSurveyReportViewModel.ProgramId, studentClassSurveyReportViewModel.SessionId, studentClassSurveyReportViewModel.SelectedBranch, studentClassSurveyReportViewModel.SelectedCampus, studentClassSurveyReportViewModel.SelectedBatchDays, studentClassSurveyReportViewModel.SelectedBatchTime, lectureId, teacherId,
                       studentClassSurveyReportViewModel.SelectedBatch, _dateFrom.AddDays(-1), _dateTo.AddDays(-1));
               var studentExcelList = new List<List<object>>();
               foreach (var studentprogram in studentProgramList)
               {
                   var c = studentprogram;
                   var str = new List<object>();
                   str.Add(_lectureService.GetLecture(lectureId).LectureName);
                   #region old code
                   //if (informationViewList != null)
                   //{
                   //    if (informationViewList.Contains("Program Roll"))
                   //    {
                   //        if (xSes.PrnNo != null)
                   //            xlsRow.Add(xSes.PrnNo);
                   //    }
                   //    if (informationViewList.Contains("Student's Name"))
                   //    {
                   //        xlsRow.Add(xSes.Student.FullName ?? "");
                   //    }
                   //    if (informationViewList.Contains("Nick Name"))
                   //    {
                   //        xlsRow.Add(xSes.Student.NickName ?? "");
                   //    }
                   //    if (informationViewList.Contains("Mobile Number (Student)"))
                   //    {
                   //        xlsRow.Add(xSes.Student.Mobile ?? "");
                   //    }
                   //    if (informationViewList.Contains("Mobile Number (Father)"))
                   //    {
                   //        xlsRow.Add(xSes.Student.GuardiansMobile1 ?? "");
                   //    }
                   //    if (informationViewList.Contains("Mobile Number (Mother)"))
                   //    {
                   //        xlsRow.Add(xSes.Student.GuardiansMobile2 ?? "");
                   //    }
                   //    if (informationViewList.Contains("Father's Name"))
                   //    {
                   //        xlsRow.Add(xSes.Student.FatherName ?? "");
                   //    }
                   //    if (informationViewList.Contains("Institute Name"))
                   //    {
                   //        if (xSes.Institute != null)
                   //            xlsRow.Add(xSes.Institute.Name ?? "");
                   //        else
                   //            xlsRow.Add("N/A");
                   //    }
                   //    if (informationViewList.Contains("Email"))
                   //    {
                   //        xlsRow.Add(xSes.Student.Email ?? "");
                   //    }
                   //    if (informationViewList.Contains("Branch"))
                   //    {
                   //        if (xSes.Batch != null)
                   //            xlsRow.Add(xSes.Batch.Branch.Name ?? "");
                   //    }

                   //    if (informationViewList.Contains("Campus"))
                   //    {
                   //        if (xSes.Student != null && xSes.Batch.Campus != null)
                   //        {
                   //            xlsRow.Add(xSes.Batch.Campus.Name);

                   //        }
                   //        else
                   //        {
                   //            xlsRow.Add("");

                   //        }
                   //    }
                   //    if (informationViewList.Contains("Batch Days"))
                   //    {
                   //        if (xSes.Student != null && xSes.Batch.Days != null)
                   //        {
                   //            xlsRow.Add(xSes.Batch.Days);

                   //        }
                   //        else
                   //        {
                   //            xlsRow.Add("");

                   //        }
                   //    }
                   //    if (informationViewList.Contains("Batch Time"))
                   //    {

                   //        if (xSes.Student != null && xSes.Batch.BatchTime != null)
                   //        {
                   //            xlsRow.Add(xSes.Batch.FormatedBatchTime);

                   //        }
                   //        else
                   //        {
                   //            xlsRow.Add("");

                   //        }
                   //    }



                   //    if (informationViewList.Contains("Batch"))
                   //    {
                   //        if (xSes.Batch != null)
                   //            xlsRow.Add(xSes.Batch.Name ?? "");
                   //    }
                   //    if (informationViewList.Contains("Version of Study"))
                   //    {
                   //        xlsRow.Add(xSes.VersionOfStudy != null ? ((VersionOfStudy)xSes.VersionOfStudy).ToString() : "--");
                   //    }
                   //    if (informationViewList.Contains("Gender"))
                   //    {
                   //        if (xSes.Student != null && xSes.Student.Gender != null)
                   //        {
                   //            xlsRow.Add(((Gender)xSes.Student.Gender).ToString());
                   //        }
                   //        else
                   //        {
                   //            xlsRow.Add("--");
                   //        }
                   //    }
                   //    if (informationViewList.Contains("Religion"))
                   //    {
                   //        if (xSes.Student != null && xSes.Student.Religion != null)
                   //        {
                   //            xlsRow.Add(((Religion)xSes.Student.Religion).ToString());
                   //        }
                   //        else
                   //        {
                   //            xlsRow.Add("--");
                   //        }
                   //    }
                   //} 
                   #endregion
                   if (studentClassSurveyReportViewModel.InformationViewList != null)
                   {

                       foreach (string informationView in studentClassSurveyReportViewModel.InformationViewList)
                       {
                           switch (informationView)
                           {
                               case "Program Roll":
                                   str.Add(c.PrnNo);
                                   break;
                               case "Student's Name":
                                   if (c.Student != null && c.Student.FullName != null)
                                   {
                                       str.Add(c.Student.FullName);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Nick Name":
                                   if (c.Student != null && c.Student.NickName != null)
                                   {
                                       str.Add(c.Student.NickName);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Mobile Number (Student)":
                                   if (c.Student != null && c.Student.Mobile != null)
                                   {
                                       str.Add(c.Student.Mobile);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Mobile Number (Father)":
                                   if (c.Student != null && c.Student.GuardiansMobile1 != null)
                                   {
                                       str.Add(c.Student.GuardiansMobile1);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Mobile Number (Mother)":
                                   if (c.Student != null && c.Student.GuardiansMobile2 != null)
                                   {
                                       str.Add(c.Student.GuardiansMobile2);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Father's Name":
                                   if (c.Student != null && c.Student.FatherName != null)
                                   {
                                       str.Add(c.Student.FatherName);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Institute Name":
                                   if (c.Student.StudentPrograms[0].Institute != null)
                                   {
                                       str.Add(c.Student.StudentPrograms[0].Institute.Name);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Email":
                                   if (c.Student != null && c.Student.Email != null)
                                   {
                                       str.Add(c.Student.Email);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Branch":
                                   if (c.Batch != null && c.Batch.Branch != null)
                                   {
                                       str.Add(c.Batch.Branch.Name);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Campus":
                                   if (c.Student != null && c.Batch.Campus != null)
                                   {
                                       str.Add(c.Batch.Campus.Name);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Batch Days":
                                   if (c.Student != null && c.Batch.Days != null)
                                   {
                                       str.Add(c.Batch.Days);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Batch Time":
                                   if (c.Student != null && c.Batch.BatchTime != null)
                                   {
                                       str.Add(c.Batch.FormatedBatchTime);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }

                               case "Batch":
                                   if (c.Batch != null && c.Batch.Name != null)
                                   {
                                       str.Add(c.Batch.Name);
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Version of Study":
                                   if (c.VersionOfStudy != null)
                                   {
                                       str.Add(((VersionOfStudy)c.VersionOfStudy).ToString());
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Gender":
                                   if (c.Student != null && c.Student.Gender != null)
                                   {
                                       str.Add(((Gender)c.Student.Gender).ToString());
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               case "Religion":
                                   if (c.Student != null && c.Student.Religion != null)
                                   {
                                       str.Add(((Religion)c.Student.Religion).ToString());
                                       break;
                                   }
                                   else
                                   {
                                       str.Add("");
                                       break;
                                   }
                               default:
                                   break;
                           }

                       }
                   }
                   int feedback =
                      studentprogram.StudentClassAttendanceDetails.Where(
                          x =>
                              x.StudentClassAttendence.Lecture.Id == lectureId && x.StudentClassAttendence.Teacher.Id == teacherId).ToList()
                          .FirstOrDefault()
                          .StudentFeedback;
                   string feedbackString = "";
                   if (feedback!=null)
                   {
                       if (feedback == 1)
                       {
                           feedbackString = "Good";
                       }
                       if (feedback == 2)
                       {
                           feedbackString = "Average";
                       }
                       if (feedback == 3)
                       {
                           feedbackString = "Bad";
                       }
                   }
                   str.Add(feedbackString); 
                   
                   studentExcelList.Add(str);
               }
               ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "class-survey-report__"
                   + DateTime.Now.ToString("yyyyMMdd-HHmmss"));



           }
            catch (Exception e)
           {
               _logger.Error(e.Message);

           }
           return View("AutoClose");
       } 
       #endregion

        #region Ajax Request Function
       //[HttpPost]
       //public JsonResult GetProgramTeacherByOrganization(long organizatonId = 0)
       //{
       //    try
       //    {
       //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
       //        var programList = _programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(organizatonId));
       //        var teacherList = _teacherService.LoadTeacher(organizatonId).DistinctBy(x => x.Id).ToList();
       //        var programSelectList = new SelectList(programList, "Id", "Name");
       //        var teacherSelectList = new SelectList(teacherList, "Id", "NickName");
       //        return Json(new { programList = programSelectList, teacherList = teacherSelectList, IsSuccess = true });
       //    }
       //    catch (Exception ex)
       //    {

       //        _logger.Error(ex);
       //        return Json(new Response(false, WebHelper.CommonErrorMessage));
       //    }
       //}
       //[HttpPost]
       //public JsonResult AjaxRequestForSessions(long programId)
       //{
       //    if (Request.IsAjaxRequest())
       //    {
       //        try
       //        {
       //            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
       //            var allAuthorizedSessions = _sessionService.LoadAuthorizedSession(_userMenu, null, _commonHelper.ConvertIdToList(programId)).ToList();
       //            SelectList sessionSelectList = new SelectList(allAuthorizedSessions, "Id", "Name");
       //            List<SelectListItem> sessionList = sessionSelectList.ToList();
       //            sessionList.Insert(0, new SelectListItem() { Value = "0", Text = "Select Session" });
       //            return Json(new { sessionList = sessionList, IsSuccess = true });
       //        }
       //        catch (Exception ex)
       //        {
       //            _logger.Error(ex);
       //            return Json(new Response(false, WebHelper.CommonErrorMessage));
       //        }
       //    }
       //    else
       //    {
       //        return Json(new Response(false, WebHelper.CommonErrorMessage));
       //    }
       //}
       //[HttpPost]
       //public JsonResult AjaxRequestForCourses(long programId, long sessionId)
       //{
       //    if (Request.IsAjaxRequest())
       //    {
       //        try
       //        {
       //            SelectList courseSelectList = new SelectList(_courseService.LoadCourse(programId, sessionId).Where(c => c.Status == StudentProgram.EntityStatus.Active).OrderBy(x => x.Rank).ToList(), "Id", "Name");
       //            List<SelectListItem> courseList = courseSelectList.ToList();

       //            courseList.Insert(0, new SelectListItem() { Value = "0", Text = "Select Course" });


       //            return Json(new { courseList = courseList, IsSuccess = true });

       //        }
       //        catch (Exception ex)
       //        {
       //            _logger.Error(ex);
       //            return Json(new Response(false, WebHelper.CommonErrorMessage));
       //        }
       //    }
       //    else
       //    {
       //        return Json(new Response(false, WebHelper.CommonErrorMessage));
       //    }
       //}

       [HttpPost]
       public JsonResult AjaxRequestForSubject(long organizatonId, long programId, long sessionId, long[] courseId)
       {
           if (Request.IsAjaxRequest())
           {
               try
               {
                   var courseList = _courseService.LoadCourse(organizatonId, programId, sessionId, courseId);
                   List<SelectListItem> subjectList = new List<SelectListItem>();
                   List<Subject> subjects = new List<Subject>();
                   if (courseList != null)
                   {
                       foreach (var singleCourse in courseList)
                       {
                           var courseSubject = singleCourse.CourseSubjects.ToList();

                           if (courseSubject != null && courseSubject.Count > 0)
                           {
                               foreach (var subject in courseSubject)
                               {
                                   subjects.Add(subject.Subject);
                               }
                           }
                       }
                       //SelectList subjectSelectList = new SelectList(subjects.Where(sub => sub.Status == StudentProgram.EntityStatus.Active).OrderBy(x => x.Rank).ToList(), "Id", "Name");
                       SelectList subjectSelectList = new SelectList(subjects.Where(sub => sub.Status == StudentProgram.EntityStatus.Active).DistinctBy(x => x.Id).ToList(), "Id", "Name");
                       subjectList = subjectSelectList.ToList();
                   }
                   subjectList.Insert(0, new SelectListItem() { Value = "0", Text = "ALL SUBJECT" });
                   return Json(new { subjectList = subjectList, IsSuccess = true });
               }
               catch (Exception ex)
               {
                   _logger.Error(ex);
                   return Json(new Response(false, WebHelper.CommonErrorMessage));
               }
           }
           else
           {
               return Json(new Response(false, WebHelper.CommonErrorMessage));
           }
       }
       //[HttpPost]
       //public ActionResult AjaxRequestForSession(long programId)
       //{
       //    try
       //    {
       //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
       //        ViewBag.SessionList = _sessionService.LoadAuthorizedSession(userMenu, null, _commonHelper.ConvertIdToList(programId));
       //        return View("Partial/_SessionList");
       //    }
       //    catch (Exception ex)
       //    {
       //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
       //        _logger.Error(ex);
       //        return Json(new Response(false, WebHelper.CommonErrorMessage));
       //    }
       //}


       //[HttpPost]
       //public ActionResult AjaxRequestForBranch(long programId, long sessionId, long? organizationId = null)
       //{
       //    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
       //    try
       //    {
       //        long pIdLong = 0;
       //        if (programId != null)
       //            long.TryParse(programId.ToString(), out pIdLong);

       //        long sIdLong = 0;
       //        if (sessionId != null)
       //            long.TryParse(sessionId.ToString(), out sIdLong);

       //        List<SelectListItem> branchList = (new SelectList(_branchService.LoadAuthorizedBranch(userMenu, (organizationId==null)?null: _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId)), "Id", "Name")).ToList();
       //        if (branchList.Any())
       //            branchList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All BRANCH" });

       //        return Json(new { branchList = branchList, IsSuccess = true });
       //    }
       //    catch (Exception ex)
       //    {
       //        // _logger.Error("Batch. AjaxRequestForBranch.", ex);
       //        _logger.Error(ex);
       //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
       //        return Json(new Response(false, WebHelper.CommonErrorMessage));
       //    }
       //}


       //[HttpPost]
       //public ActionResult AjaxRequestForCampus(long programId, long sessionId, long[] branchId)
       //{
       //    try
       //    {
       //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;

       //        //long programIdLong = 0;
       //        //if (programId != null)
       //        //    long.TryParse(programId.ToString(), out programIdLong);

       //        //long sessionIdLong = 0;
       //        //if (sessionId != null)
       //        //    long.TryParse(sessionId.ToString(), out sessionIdLong);

       //        IList<Campus> campusList = new List<Campus>();
       //        if (branchId == null || branchId.Contains(SelectionType.SelelectAll))
       //            campusList = _campusService.LoadAuthorizeCampus(userMenu, null, _commonHelper.ConvertIdToList(programId),null, _commonHelper.ConvertIdToList(sessionId));
       //        else
       //            campusList = _campusService.LoadAuthorizeCampus(userMenu, branchId.ToList());

       //        List<SelectListItem> campusSelecteList = (new SelectList(campusList, "Id", "Name")).ToList();
       //        if (campusList.Any())
       //            campusSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All CAMPUS" });

       //        return Json(new { campusList = campusSelecteList, IsSuccess = true });
       //    }
       //    catch (Exception ex)
       //    {
       //        //_logger.Error("Batch. AjaxRequestForCampus.", ex);
       //        _logger.Error(ex);
       //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
       //        return Json(new Response(false, WebHelper.CommonErrorMessage));
       //    }
       //}

       //[HttpPost]
       //public ActionResult AjaxRequestForBatchDay(long programId, long sessionId, long[] branchId, long[] campusId)
       //{
       //    try
       //    {
       //        #region old code
       //        //long programIdLong = 0;
       //        //if (programId != null)
       //        //    long.TryParse(programId.ToString(), out programIdLong);

       //        //long sessionIdLong = 0;
       //        //if (sessionId != null)
       //        //    long.TryParse(sessionId.ToString(), out sessionIdLong);

       //        //IList<Batch> batchList = new List<Batch>();
       //        //if (branchId != null && !branchId.Contains(SelectionType.SelelectAll) && campusId != null && !campusId.Contains(SelectionType.SelelectAll))
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampusHabibXp(programIdLong, sessionIdLong, branchId, campusId);
       //        //else if (branchId != null && !branchId.Contains(SelectionType.SelelectAll))
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchHabibXp(programIdLong, sessionIdLong, branchId);
       //        //else if (campusId != null && !campusId.Contains(SelectionType.SelelectAll))
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionCampusHabibXp(programIdLong, sessionIdLong, campusId);
       //        //else
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionHabibXp(programIdLong, sessionIdLong);

       //        ////Check by Batch Day
       //        //IList<string> batchDayList = (from x in batchList select x.Days).Distinct().ToList();
       //        //List<SelectListItem> batchSelecteList = (new SelectList(batchDayList)).ToList();

       //        //if (batchSelecteList.Any())
       //        //    batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All BATCH DAY" });


       //        #endregion
       //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
       //        var batchList = _batchService.LoadAuthorizeBatchGroupByDays(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList());
       //        //Check by Batch Day
       //        //IList<string> batchDayList = (from x in batchList select x.Days).Distinct().ToList();
       //        List<SelectListItem> batchSelecteList = (new SelectList(batchList)).ToList();
       //        if (batchSelecteList.Any())
       //            batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Day" });


       //        return Json(new { batchDays = batchSelecteList, IsSuccess = true });
       //    }
       //    catch (Exception ex)
       //    {
       //        //_logger.Error("Batch. AjaxRequestForBatchDay.", ex);
       //        _logger.Error(ex);
       //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
       //        return Json(new Response(false, WebHelper.CommonErrorMessage));
       //    }
       //}

       //[HttpPost]
       //public ActionResult AjaxRequestForBatchTime(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays)
       //{
       //    try
       //    {

       //        #region old code
       //        //long programIdLong = 0;
       //        //if (programId != null)
       //        //    long.TryParse(programId.ToString(), out programIdLong);

       //        //long sessionIdLong = 0;
       //        //if (sessionId != null)
       //        //    long.TryParse(sessionId.ToString(), out sessionIdLong);

       //        //IList<Batch> batchList = new List<Batch>();
       //        //if (branchId != null && !branchId.Contains(SelectionType.SelelectAll) && campusId != null && !campusId.Contains(SelectionType.SelelectAll))
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampusHabibXp(programIdLong, sessionIdLong, branchId, campusId);
       //        //else if (branchId != null && !branchId.Contains(SelectionType.SelelectAll))
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchHabibXp(programIdLong, sessionIdLong, branchId);
       //        //else if (campusId != null && !campusId.Contains(SelectionType.SelelectAll))
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionCampusHabibXp(programIdLong, sessionIdLong, campusId);
       //        //else
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionHabibXp(programIdLong, sessionIdLong);

       //        ////Check Batch Day 
       //        //if (batchDays != null && !batchDays.Contains(SelectionType.SelelectAll.ToString()))
       //        //    batchList = (from a in batchList where batchDays.Any(ae => ae == a.Days) select a).ToList();


       //        //SelectList batchTimeSelectList = new SelectList(batchList, "BatchTime", "FormatedBatchTime");
       //        //List<SelectListItem> batchTimes = batchTimeSelectList.ToList().Distinct().ToList();


       //        //if (batchTimes.Any())
       //        //    batchTimes.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All BATCH TIME" });


       //        #endregion
       //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
       //        var batchList = _batchService.LoadAuthorizeBatchGroupByTimes(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays);
       //        //batchList = batchList.DistinctBy(x => x.BatchTime).ToList();
       //        //SelectList batchTimeSelectList = new SelectList(batchList, "BatchTime", "FormatedBatchTime");
       //        var batchTimeSelectList = new SelectList(batchList);
       //        List<SelectListItem> batchTimes = batchTimeSelectList.ToList();
       //        if (batchTimes.Any())
       //            batchTimes.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch Time" });
                
       //        return Json(new { batchTime = batchTimes, IsSuccess = true });
       //    }
       //    catch (Exception ex)
       //    {
       //        // _logger.Error("Batch. AjaxRequestForBatchTime.", ex);
       //        _logger.Error(ex);
       //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
       //        return Json(new Response(false, WebHelper.CommonErrorMessage));
       //    }
       //}

       //[HttpPost]
       //public ActionResult AjaxRequestForBatch(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, string[] batchTimeText)
       //{
       //    try
       //    {
       //        #region old code
       //        //long programIdLong = 0;
       //        //if (programId != null)
       //        //    long.TryParse(programId.ToString(), out programIdLong);


       //        //long sessionIdLong = 0;
       //        //if (sessionId != null)
       //        //    long.TryParse(sessionId.ToString(), out sessionIdLong);


       //        //IList<Batch> batchList = new List<Batch>();
       //        //if (branchId != null && !branchId.Contains(SelectionType.SelelectAll) && campusId != null && !campusId.Contains(SelectionType.SelelectAll))
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchCampusHabibXp(programIdLong, sessionIdLong, branchId, campusId);
       //        //else if (branchId != null && !branchId.Contains(SelectionType.SelelectAll))
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionBranchHabibXp(programIdLong, sessionIdLong, branchId);
       //        //else if (campusId != null && !campusId.Contains(SelectionType.SelelectAll))
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionCampusHabibXp(programIdLong, sessionIdLong, campusId);
       //        //else
       //        //    batchList = _batchService.LoadBatchDaysByProgramSessionHabibXp(programIdLong, sessionIdLong);


       //        ////Check Batch Day
       //        //if (batchDays != null && !batchDays.Contains(SelectionType.SelelectAll.ToString()))
       //        //    batchList = (from a in batchList where batchDays.Any(ae => ae == a.Days) select a).ToList();


       //        ////Check Batch Time
       //        //if (batchTime != null && !batchTime.Contains(SelectionType.SelelectAll.ToString()))
       //        //    batchList = (from x in batchList where batchTimeText.Any(y => y == x.FormatedBatchTime) select x).ToList();


       //        //List<SelectListItem> batchSelecteList = (new SelectList(batchList, "Id", "Name")).ToList();
       //        //if (batchSelecteList.Any())
       //        //    batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All BATCH" });


       //        #endregion
       //        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
       //        var batchList = _batchService.LoadAuthorizeBatch(userMenu, null, _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays, batchTime);
       //        List<SelectListItem> batchSelecteList = (new SelectList(batchList.ToList(), "Id", "Name")).ToList();
       //        if (batchSelecteList.Any())
       //            batchSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Batch" });
               

       //        return Json(new { batchName = batchSelecteList, IsSuccess = true });
       //    }
       //    catch (Exception ex)
       //    {
       //        //_logger.Error("Batch. AjaxRequestForBatch.", ex);
       //        _logger.Error(ex);
       //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
       //        return Json(new Response(false, WebHelper.CommonErrorMessage));
       //    }
       //}
       [HttpPost]
       public ActionResult GetLecture(long[] courseIdList, long[] subjectIdList, long?[] batchIdList, string dateFrom = null, string dateTo = null,List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null, List<long> sessionIdList = null, List<long> campusIdList = null, string[] batchDaysList = null, string[] batchTimeList = null)
       {
           try
           {
               var userMenu = (List<UserMenu>)ViewBag.UserMenu;
               //var lectureList = _lectureService.LoadLecture(courseIdList, subjectIdList, batchIdList, dateFrom, dateTo);
               var lectureList=_lectureService.LoadLecture(userMenu,courseIdList,subjectIdList, batchIdList,dateFrom,  dateTo,  organizationIdList , programIdList,  branchIdList, sessionIdList, campusIdList ,  batchDaysList,  batchTimeList);
               var lectureSelectList = new SelectList(lectureList.DistinctBy(x => x.Id), "Id", "LectureName").ToList();
               //lectureSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Lecture" });
               return Json(new { lectureList = lectureSelectList, IsSuccess = true });
           }
           catch (Exception ex)
           {
               _logger.Error(ex);
               ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
               return Json(new Response(false, WebHelper.CommonErrorMessage));
           }
       }
       [HttpPost]
       public ActionResult GetTeacher(long[] lectureIdList, long[] batchIdList, DateTime dateFrom, DateTime dateTo)
       {
           try
           {
               lectureIdList = lectureIdList.Where(x => x != 0).ToArray();
               batchIdList = batchIdList.Where(x => x != 0).ToArray();
               var teacherList = _teacherService.LoadTeacher(lectureIdList, batchIdList, dateFrom.Date, dateTo.Date);
               var teacherSelectList = new SelectList(teacherList.DistinctBy(x => x.Id), "Id", "DisplayTeacherName").ToList();
               return Json(new { teacherList = teacherSelectList, IsSuccess = true });
           }
           catch (Exception ex)
           {
               _logger.Error(ex);
               ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
               return Json(new Response(false, WebHelper.CommonErrorMessage));
           }
       }
       public JsonResult GetStudentCount(long[] batchIdList, long[] lectureIdList, long[] teacherIdList, DateTime dateFrom, DateTime dateTo)
       {
           //long organizationId,long programId,long sessionId,long[]branchIds,long[]campusIds,string[]batchDays,string []batchTimes,
           try
           {

               int recordsTotal = _studentClassAttendanceService.GetStudentCount(batchIdList,
                   lectureIdList, teacherIdList, dateFrom.Date, dateTo.Date);
               if (recordsTotal > 0)
               {
                   return Json(new { rowCount = recordsTotal, IsSuccess = true });
               }
               return Json(new { rowCount = recordsTotal, IsSuccess = false });
           }
           catch (Exception e)
           {
               _logger.Error(e.Message);
               throw;
           }
       }

       #endregion
    
    }
}
