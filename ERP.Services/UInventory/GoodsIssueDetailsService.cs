using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
public interface IGoodsIssueDetailsService : IBaseService
{
    #region Operational Function

    bool SaveOrUpdate(GoodsIssueDetails goodsIssueDetail);

    #endregion

    #region Single Instances Loading Function

    GoodsReturnDetails GetGoodsReturnDetails(long id);

    #endregion

    #region List Loading Function
    string[] BuildAuthProgramSession(List<UserMenu> userMenu, long orgId, long[] branchIds, long[] itemIds, string[] programIds = null);
    #endregion

    #region Others Function
    #endregion

    #region Helper Function

    #endregion
    
}
public class GoodsIssueDetailsService : BaseService, IGoodsIssueDetailsService {

    #region Logger
    private readonly ILog _logger = LogManager.GetLogger("InventoryService");
    #endregion

    #region Propertise & Object Initialization
    private readonly CommonHelper _commonHelper;
    private readonly IGoodsReturnDetailsDao _goodsIssueDetailDao;
    private readonly IBranchDao _branchDao;
    private readonly IProgramDao _programDao;
    private readonly ISessionDao _sessionDao;
    private readonly ProgramBranchSessionDao _programBranchSessionDao;

    public GoodsIssueDetailsService(ISession session)
    {
        Session = session;
        _commonHelper = new CommonHelper();
        _goodsIssueDetailDao = new GoodsReturnDetailsDao{ Session = session };
        _branchDao = new BranchDao() { Session = session };
        _programDao = new ProgramDao() { Session = session };
        _sessionDao = new SessionDao() { Session = session };
        _programBranchSessionDao = new ProgramBranchSessionDao() { Session = session };
    }

    #endregion

    #region Operational Functions

    public bool SaveOrUpdate(GoodsIssueDetails goodsIssueDetail)
    {
        

        return true;
    }

    #endregion

    #region Single Instances Loading Function

    public GoodsReturnDetails GetGoodsReturnDetails(long id)
    {
        try
        {
            var goodsReturnDetails = _goodsIssueDetailDao.LoadById(id);
            return goodsReturnDetails;
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
            throw;
        }
    }

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function


    #endregion

    #region Helper function

    public string[] BuildAuthProgramSession(List<UserMenu> authMenu, long organizationId, long[] branchIds, long[] itemIds, string[] programSessionIds = null)
    {
        try
        {
            List<long> authProIdList = new List<long>();
            List<long> brIds = branchIds.ToList();
            List<long> itemList = itemIds.ToList();
            if (itemList.Count == 1 && itemList.Contains(0)) itemList = null;
            if (brIds.Count == 1 && brIds.Contains(0)) brIds = null;
            List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authMenu, (organizationId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(organizationId) : null);
            List<long> authBr = AuthHelper.LoadBranchIdList(authMenu, authOrgList, null, brIds);
            if (programSessionIds == null) authProIdList = AuthHelper.LoadProgramIdList(authMenu, authOrgList, authBr);
            else
            {
                foreach (var programsession in programSessionIds)
                {
                    string[] programSessionArray = programsession.Split(new string[] { "::" }, StringSplitOptions.None);
                    Program program = _programDao.LoadById(Convert.ToInt64(programSessionArray[0]));
                    if (program != null)
                        authProIdList.Add(program.Id);
                }
                authProIdList = AuthHelper.LoadProgramIdList(authMenu, authOrgList, authBr, authProIdList);
            }

            if (!authProIdList.Any())
                throw new InvalidDataException("Program can't empty");
            var authbranchIdlist = new List<long>();
            foreach (var branchId in authBr)
            {
                if (branchId > 0)
                {
                    var branch = _branchDao.LoadById((long)branchId);
                    if (branch.IsCorporate)
                    {
                        authbranchIdlist = null;
                        break;
                    }
                    else
                        authbranchIdlist.Add((long)branchId);
                }
            }

            IList<ProgramSessionDto> proSesDtos = _programBranchSessionDao.LoadProgramSession(authOrgList, authbranchIdlist, authProIdList, itemList);
            var ary = new string[proSesDtos.Count];
            int index = 0;
            foreach (var programSessionDto in proSesDtos)
            {
                ary[index] = programSessionDto.ProgramAndSessionId;
                index++;
            }
            return ary;
        }
        catch (Exception e)
        {
            _logger.Error(e);
            throw;
        }
    }

    #endregion
}
}
                                    
