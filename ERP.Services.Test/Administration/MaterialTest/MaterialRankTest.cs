﻿using System;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MaterialTest
{
    interface IMaterialRankTest
    {
        #region Basic Test

        [Fact]
        void Get_Maximum_Rank_Not_Null_Check();
        void Get_Minimum_Rank_Not_Null_Check();
        #endregion

        #region Business Logic Test
        
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Material")]
    public class MaterialRankTest : MaterialBaseTest, IMaterialRankTest
    {
        #region Basic Test

        [Fact]
        public void Get_Maximum_Rank_Not_Null_Check()
        {
            MaterialFactory materialFactory =
                    _materialFactory.Create()
                    .Persist();
            int maxRank = _materialService.GetMaximumRank(materialFactory.Object);
            Assert.True(maxRank>0);

        }

        [Fact]
        public void Get_Minimum_Rank_Not_Null_Check()
        {
            MaterialFactory materialFactory =
                    _materialFactory.Create()
                    .Persist();
            int minRank = _materialService.GetMinimumRank(materialFactory.Object);
            Assert.True(minRank > 0);

        }
        #endregion
        
        #region Business Logic Test
        
        #endregion
    }
}
