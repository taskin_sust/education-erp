﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IProgramSessionSubjectDao : IBaseDao<ProgramSessionSubject, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
      
        #endregion

        #region List Loading Function

        IList<ProgramSessionSubject> LoadProgramSessionSubject(long programId, long sessionId);
        #endregion

        #region Others Function
        long HasManySubjectObject(long subjectId);
        #endregion
    }
    public class ProgramSessionSubjectDao : BaseDao<ProgramSessionSubject, long>, IProgramSessionSubjectDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
       
        #endregion

        #region List Loading Function
        public IList<ProgramSessionSubject> LoadProgramSessionSubject(long programId, long sessionId)
        {
            return Session.QueryOver<ProgramSessionSubject>().Where(x => x.Program.Id == programId && x.Session.Id == sessionId && x.Status == ProgramSessionSubject.EntityStatus.Active).List<ProgramSessionSubject>();
        }
        #endregion

        #region Others Function
        public long HasManySubjectObject(long subjectId)
        {
            var returnObj = Session.QueryOver<ProgramSessionSubject>().Where(x => x.Subject.Id == subjectId && x.Status != Subject.EntityStatus.Delete).List<ProgramSessionSubject>();
            return returnObj.Count;
        }
        #endregion
    }
}
