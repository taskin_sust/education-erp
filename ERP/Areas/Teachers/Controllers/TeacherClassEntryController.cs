﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Teachers;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Teachers.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Teachers")]
    public class TeacherClassEntryController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeachersArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly ITeacherService _teacherService;
        private readonly ITeacherClassEntryService _teacherClassEntryService;
        private readonly ITeacherClassEntryDetailsService _teacherClassEntryDetailsService;
        private readonly IUserService _userService;
        private List<UserMenu> _userMenu;
        public TeacherClassEntryController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _organizationService = new OrganizationService(session);
                _teacherClassEntryService = new TeacherClassEntryService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _branchService = new BranchService(session);
                _campusService = new CampusService(session);
                _teacherService = new TeacherService(session);
                _userService = new UserService(session);
                _teacherClassEntryDetailsService = new TeacherClassEntryDetailsService(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Index/Manage
        public ActionResult Index()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.SessionList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.HeldBranchList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.HeldCampusList = new SelectList(string.Empty, "Value", "Text");
                var paymentStatus = new SelectList(TeacherPaymentStatus.GetTeacherPaymentStatus(), "Key", "Value");
                ViewBag.PaymentStatusList = new SelectList(paymentStatus, "Value", "Text");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return HttpNotFound();
            }

            return View();
        }
        [HttpPost]
        public ActionResult Index(TeacherClassEntryManageViewModel data)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.SessionList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.HeldBranchList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.HeldCampusList = new SelectList(string.Empty, "Value", "Text");
                var paymentStatus = new SelectList(TeacherPaymentStatus.GetTeacherPaymentStatus(), "Key", "Value");
                ViewBag.PaymentStatusList = new SelectList(paymentStatus, "Value", "Text");
                if (data == null)
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    return View();
                }

                var json = new JavaScriptSerializer().Serialize(data);
                ViewBag.PageSize = Constants.PageSize;
                ViewData["TeacherClassEntryManageViewModelObj"] = json;
                return View("TeacherClassEntryReport", data);
            }
            catch (Exception ex)
            {
                //return View();
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return HttpNotFound();

            }
        }
        [HttpPost]
        public JsonResult GenerateClassEntryList(int draw, int start, int length, string teacherClassEntryManageViewModelObj, string teacherName = "", string heldDate = "", string trackingId = "", string tpin = "")
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;

                    string orderBy = "";
                    string orderDir = "";


                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                            default:
                                orderBy = "";
                                break;
                        }
                    }
                    var teacherClassEntryManageViewModel = new JavaScriptSerializer().Deserialize<TeacherClassEntryManageViewModel>(teacherClassEntryManageViewModelObj);
                    //long recordsTotal = 0;
                    long recordsTotal = _teacherClassEntryDetailsService.TeacherClassEntryDetailsRowCount(teacherClassEntryManageViewModel.Organization, teacherClassEntryManageViewModel.Program, teacherClassEntryManageViewModel.Session, teacherClassEntryManageViewModel.HeldBranch, teacherClassEntryManageViewModel.HeldCampus, Convert.ToInt32(teacherClassEntryManageViewModel.PaymentStatus),teacherName, heldDate, trackingId, tpin);
                    long recordsFiltered = recordsTotal;
                    // var teacherClassEntryDetailsList = new List<TeacherClassEntryDetails>(); 
                    List<TeacherClassEntryDto> TeacherClassEntryDtoList = _teacherClassEntryDetailsService.LoadTeacherClassEntryDetails(start, length, orderBy, orderDir.ToUpper(), teacherClassEntryManageViewModel.Organization, teacherClassEntryManageViewModel.Program, teacherClassEntryManageViewModel.Session,
                        teacherClassEntryManageViewModel.HeldBranch, teacherClassEntryManageViewModel.HeldCampus, Convert.ToInt32(teacherClassEntryManageViewModel.PaymentStatus),
                        teacherName, heldDate, trackingId,tpin).ToList();
                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var tce in TeacherClassEntryDtoList)
                    {
                        var str = new List<string>();
                        // str.Add(sl.ToString());source.Substring(source.Length - tail_length);
                        str.Add(tce.Tpin.ToString());
                        string hscyear = tce.HscPassingYear.ToString();
                        str.Add(tce.TeacherName + "(" + hscyear.Substring(hscyear.Length - 2) + ")" + " " + tce.TeacherFullName);
                        str.Add(tce.Description);
                        str.Add(tce.HeldDate != null ? tce.HeldDate.Value.ToString("dd/MM/yyyy") : "");

                        str.Add(tce.Duration.ToString());
                        str.Add(tce.Quantity.ToString());
                        str.Add(tce.Amount.ToString());
                        string actionString = "";
                        if (tce.PaymentStatus == TeacherPaymentStatus.Due)
                        {
                            actionString = "<a href='" + Url.Action("Edit", "TeacherClassEntry") + "?id=" + tce.Id.ToString() + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp;<a href='" + Url.Action("Details", "TeacherClassEntry") + "?id=" + tce.Id.ToString() + "' class='glyphicon glyphicon-th-list'> </a>&nbsp;&nbsp;<a id='" + tce.Id.ToString() + "' href='#'  class='glyphicon glyphicon-trash'> </a>";
                            str.Add("Due");
                        }
                        else if (tce.PaymentStatus == TeacherPaymentStatus.Paid)
                        {
                            actionString = "<a href='" + Url.Action("Details", "TeacherClassEntry") + "?id=" + tce.Id.ToString() + "' class='glyphicon glyphicon-th-list'> </a>";
                            str.Add("Paid");
                        }
                        str.Add("C" + tce.ClassEntryId);
                        str.Add(actionString);
                        data.Add(str);
                        sl++;
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    var data = new List<object>();
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            else
            {
                return Json(HttpNotFound());
            }
        }
        #endregion

        #region Save Operation
        public ActionResult Create()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.ClassTypeList = new SelectList(_teacherClassEntryService.LoadTeacherClassType(), "Id", "Name");
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.SessionList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.HeldBranchList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.HeldCampusList = new SelectList(string.Empty, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return HttpNotFound();
            }

        }

        [HttpPost]
        public ActionResult Create(TeacherClassEntryViewModel teacherClassEntryViewModel)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var teacherClassEntry = new TeacherClassEntry();


                if (teacherClassEntryViewModel == null)
                    return Json(new Response(false, WebHelper.CommonErrorMessage));

                var organization = _organizationService.LoadById(teacherClassEntryViewModel.Organization);
                if (organization == null)
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                teacherClassEntry.Organization = organization;

                var classType = _teacherClassEntryService.GetTeacherClassType(teacherClassEntryViewModel.ClassType);
                if (classType == null)
                    return Json(new Response(false, "No Class Type found"));
                teacherClassEntry.TeacherClassType = classType;

                if (teacherClassEntryViewModel.Program == -1)
                    teacherClassEntry.Program = null;
                else
                {
                    var program = _programService.GetProgram(teacherClassEntryViewModel.Program);
                    if (program == null)
                        return Json(new Response(false, "No Program found"));
                    teacherClassEntry.Program = program;
                }

                if (teacherClassEntryViewModel.Session == -1)
                    teacherClassEntry.Session = null;
                else
                {
                    var session = _sessionService.LoadById(teacherClassEntryViewModel.Session);
                    if (session == null)
                        return Json(new Response(false, "No Session found!"));
                    teacherClassEntry.Session = session;
                }

                if (teacherClassEntryViewModel.HeldBranch == -1)
                    teacherClassEntry.Branch = null;
                else
                {
                    var branch = _branchService.GetBranch(teacherClassEntryViewModel.HeldBranch);
                    if (branch == null)
                        return Json(new Response(false, "No Held Branch Found"));
                    teacherClassEntry.Branch = branch;
                }

                if (teacherClassEntryViewModel.HeldCampus == -1)
                    teacherClassEntry.Campus = null;
                else
                {
                    var campus = _campusService.GetCampus(teacherClassEntryViewModel.HeldCampus);
                    if (campus == null)
                        return Json(new Response(false, "No Held Campus found!"));
                    teacherClassEntry.Campus = campus;
                }
                teacherClassEntry.HeldDate = teacherClassEntryViewModel.HeldDate;

                if (!teacherClassEntryViewModel.TeacherClassEntryDetailsViewModels.Any())
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                IList<TeacherClassEntryDetails> teacherClassEntryDetails = new List<TeacherClassEntryDetails>();
                foreach (
                    var teacherClassEntryDetailsViewModel in
                        teacherClassEntryViewModel.TeacherClassEntryDetailsViewModels)
                {
                    var teacherClassEntryDetail = new TeacherClassEntryDetails();
                    var teacher = _teacherService.GetTeacher(teacherClassEntryDetailsViewModel.TeacherId);
                    if (teacher == null)
                        return Json(new Response(false, teacherClassEntryDetailsViewModel.TeacherName + " is Invalid Teacher"));

                    teacherClassEntryDetail.Teacher = teacher;

                    if (String.IsNullOrEmpty(teacherClassEntryDetailsViewModel.Description) ||
                        teacherClassEntryDetailsViewModel.Quantity < 1 || teacherClassEntryDetailsViewModel.Quantity >= 100 ||
                        teacherClassEntryDetailsViewModel.Duration < 1 || teacherClassEntryDetailsViewModel.Duration >= 1000 ||
                        teacherClassEntryDetailsViewModel.Amount < 1)
                        return Json(new Response(false, "Class Entry quantity is less than 1 or more than 99"));

                    teacherClassEntryDetail.Description = teacherClassEntryDetailsViewModel.Description;
                    teacherClassEntryDetail.Duration = teacherClassEntryDetailsViewModel.Duration;
                    teacherClassEntryDetail.Amount = teacherClassEntryDetailsViewModel.Amount;
                    teacherClassEntryDetail.Quantity = teacherClassEntryDetailsViewModel.Quantity;
                    teacherClassEntryDetail.PaymentStatus = TeacherPaymentStatus.Due;
                    teacherClassEntryDetails.Add(teacherClassEntryDetail);
                }

                if (!teacherClassEntryDetails.Any())
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                teacherClassEntry.TeacherClassEntryDetails = teacherClassEntryDetails;
                _teacherClassEntryService.Save(teacherClassEntry);

                return Json(new Response(true, "Success. Tracking Id: C" + teacherClassEntry.Id));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

        }

        #endregion

        #region Details Operation

        public ActionResult Details(long id)
        {
            try
            {
                long cId;
                bool isNum = long.TryParse(id.ToString(), out cId);
                if (!isNum)
                {
                    return HttpNotFound();
                }
                var teacherClassEntryDetails = _teacherClassEntryDetailsService.GetTeacherClassEntryDetails(id);
                if (teacherClassEntryDetails != null)
                {
                    teacherClassEntryDetails.CreateByText = _userService.GetUserNameByAspNetUserId(teacherClassEntryDetails.CreateBy);
                    teacherClassEntryDetails.ModifyByText = _userService.GetUserNameByAspNetUserId(teacherClassEntryDetails.ModifyBy);
                }
                return View(teacherClassEntryDetails);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = "Details View Fails because of " + e.Message;
                _logger.Error(e);
                return View();
            }
        }

        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isSuccess = _teacherClassEntryDetailsService.Delete(id);
                return Json(isSuccess ? new Response(true, "Class Entry sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Class Entry Delete Fail !"));
            }
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(long id)
        {
            try
            {
                long cId;
                bool isNum = long.TryParse(id.ToString(), out cId);
                if (!isNum)
                {
                    return HttpNotFound();
                }
                var teacherClassEntryDetails = _teacherClassEntryDetailsService.GetTeacherClassEntryDetails(id);

                return View(teacherClassEntryDetails);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                //var teacherClassEntryDetails = new TeacherClassEntryDetails();
                return View();
            }
        }

        [HttpPost]
        public ActionResult Edit(long id, string description, string duration, string quantity, string amount)
        {
            long cId;
            bool isNum = long.TryParse(id.ToString(), out cId);
            if (!isNum)
            {
                return HttpNotFound();
            }
            var teacherClassEntryDetails = _teacherClassEntryDetailsService.GetTeacherClassEntryDetails(id);
            try
            {
                if (!String.IsNullOrEmpty(description) && !String.IsNullOrEmpty(duration) &&
                    !String.IsNullOrEmpty(quantity) && !String.IsNullOrEmpty(amount))
                {
                    teacherClassEntryDetails.Description = description;
                    teacherClassEntryDetails.Duration = Convert.ToDecimal(duration);
                    teacherClassEntryDetails.Quantity = Convert.ToInt32(quantity);
                    teacherClassEntryDetails.Amount = Convert.ToDecimal(amount);
                    bool isSuccess = _teacherClassEntryDetailsService.Update(teacherClassEntryDetails);
                    if (isSuccess)
                        ViewBag.SuccessMessage = "Successfully updated.";
                    else
                        ViewBag.ErrorMessage = "Problem Occurred, during update.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Problem Occurred, during update.";
                }

            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
            }
            return View(teacherClassEntryDetails);
        }

        //[HttpPost]
        //public ActionResult Edit(long id, Session sessionObj)
        //{
        //    try
        //    {
        //        bool isSuccess = _sessionService.Update(id, sessionObj);
        //        if (isSuccess)
        //            ViewBag.SuccessMessage = "Session successfully updated.";
        //        else
        //            ViewBag.ErrorMessage = "Problem Occurred, during session update.";
        //    }
        //    catch (EmptyFieldException ex)
        //    {
        //        ViewBag.ErrorMessage = ex.Message;
        //        _logger.Error(ex);
        //    }
        //    catch (DuplicateEntryException ex)
        //    {
        //        ViewBag.ErrorMessage = ex.Message;
        //        _logger.Error(ex);
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = "Problem Occurred.";
        //        _logger.Error(ex);
        //    }
        //    var initializeSessionObj = InitializeUpdateView(id);
        //    return View(initializeSessionObj);
        //}
        #endregion

        #region Campus Routine

        public ActionResult TeacherCampusRoutine()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.SessionList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.HeldBranchList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.HeldCampusList = new SelectList(string.Empty, "Value", "Text");
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult TeacherCampusRoutine(TeacherCampusRoutineViewModel model)
        {
            ViewBag.OrganizationName = _organizationService.LoadById(model.Organization).Name;
            ViewBag.ProgramName = _programService.GetProgram(model.Program).Name;
            ViewBag.SessionName = _sessionService.LoadById(model.Session).Name;
            return View("GetTeacherCampusRoutine", model);
        }

        [HttpPost]
        public JsonResult GetTeacherCampusRoutineDataTable(int draw, int start, int length, long organizationId, long programId, long sessionId, long[] branchIds,
            long[] campusIds, string heldDate, string fullName, string nickName, string mobile, string[] informationViewList, string tpin)
         {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;

                    string orderBy = "";
                    string orderDir = "";


                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                            default:
                                orderBy = "";
                                break;
                        }
                    }
                    int totalRow = _teacherClassEntryService.CountTeacherCampusRoutine(organizationId, programId, sessionId, branchIds, campusIds, heldDate, fullName, nickName, mobile, tpin);

                    IList<TeacherCampusRoutineDto> classRoutineList = _teacherClassEntryService.ListTeacherCampusRoutine(start, length, orderBy, orderDir, organizationId, programId, sessionId, branchIds, campusIds, heldDate, fullName, nickName, mobile, tpin);
                    //int totalRow = classRoutineList.Count;
                    var data = new List<object>();
                    int sr = start + 1;
                    foreach (var classRoutine in classRoutineList)
                    {
                        
                        var str = new List<string>();
                        str.Add(sr.ToString());
                        #region InfomationViewList
                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Branch":
                                        if (!string.IsNullOrEmpty(classRoutine.BranchName))
                                        {
                                            str.Add(classRoutine.BranchName);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Campus":
                                        if (!string.IsNullOrEmpty(classRoutine.CampusName))
                                        {
                                            str.Add(classRoutine.CampusName);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Quantity":
                                        if (classRoutine.TotalQuantity!=0)
                                        {
                                            str.Add(classRoutine.TotalQuantity.ToString());
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Tpin":
                                        if (classRoutine.Tpin !=0)
                                        {
                                            str.Add(classRoutine.Tpin.ToString());
                                            break;
                                        }
                                        str.Add("-");
                                        break;
                                    case "Full Name":
                                        if (!string.IsNullOrEmpty(classRoutine.FullName))
                                        {
                                            str.Add(classRoutine.FullName);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Nick Name":
                                        if (!string.IsNullOrEmpty(classRoutine.NickName))
                                        {
                                            str.Add(classRoutine.NickName);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "T.Batch":
                                        if (classRoutine.TeacherBatch!=null)
                                        {
                                            str.Add(classRoutine.TeacherBatch.ToString());
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Institute":
                                        if (!string.IsNullOrEmpty(classRoutine.Institute))
                                        {
                                            str.Add(classRoutine.Institute);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Department":
                                        if (!string.IsNullOrEmpty(classRoutine.Department))
                                        {
                                            str.Add(classRoutine.Department);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Mobile Number 1":
                                        if (!string.IsNullOrEmpty(classRoutine.PersonalMobile))
                                        {
                                            str.Add(classRoutine.PersonalMobile);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Mobile Number 2":
                                        if (!string.IsNullOrEmpty(classRoutine.AlternativeMobile))
                                        {
                                            str.Add(classRoutine.AlternativeMobile);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Mobile Number (Room Mate)":
                                        if (!string.IsNullOrEmpty(classRoutine.RoomMatesMobile))
                                        {
                                            str.Add(classRoutine.RoomMatesMobile);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Mobile Number (Father)":
                                        if (!string.IsNullOrEmpty(classRoutine.FathersMobile))
                                        {
                                            str.Add(classRoutine.FathersMobile);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    case "Mobile Number (Mother)":
                                        if (!string.IsNullOrEmpty(classRoutine.MothersMobile))
                                        {
                                            str.Add(classRoutine.MothersMobile);
                                            break;
                                        }
                                        str.Add("");
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        sr++;
                        #endregion
                        
                        data.Add(str);
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = totalRow,
                        recordsFiltered = totalRow,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = 0
                    });
                }
            }
            else
            {
                return Json(HttpNotFound());
            }
            
        }

        public ActionResult ExportTeacherCampusRoutine(long organizationId, long programId, long sessionId, long[] branchIds,long[] campusIds, string heldDate, string fullName, string nickName, string mobile, string[] informationViewList, string tpin)
        {
            try
            {
                string orderBy = "";
                string orderDir = "";
                int start = 0;
                int TotalRow = _teacherClassEntryService.CountTeacherCampusRoutine(organizationId, programId, sessionId, branchIds, campusIds, heldDate, fullName, nickName, mobile, tpin);

                IList<TeacherCampusRoutineDto> classRoutineList = _teacherClassEntryService.ListTeacherCampusRoutine(start, TotalRow, orderBy, orderDir, organizationId, programId,
                    sessionId, branchIds, campusIds, heldDate, fullName, nickName, mobile, tpin);

                List<string> headerList = new List<string>();
                headerList.Add(ErpInfo.OrganizationNameFull);

                List<string> footerList = new List<string>();
                footerList.Add("");

                List<string> columnList = new List<string>();
                if (informationViewList != null)
                {
                    foreach (string informationView in informationViewList)
                    {
                        switch (informationView)
                        {
                            case "Branch":
                                columnList.Add("Branch");
                                break;
                            case "Campus":
                                columnList.Add("Campus");
                                break;
                            case "Quantity":
                                columnList.Add("Quantity");
                                break;
                            case "Tpin":
                                columnList.Add("TPIN");
                                break;
                            case "Full Name":
                                columnList.Add("Full Name");
                                break;
                            case "Nick Name":
                                columnList.Add("Nick Name");
                                break;
                            case "T.Batch":
                                columnList.Add("T.Batch");
                                break;
                            case "Institute":
                                columnList.Add("Institute");
                                break;
                            case "Department":
                                columnList.Add("Department");
                                break;
                            case "Mobile Number 1":
                                columnList.Add("Mobile Number 1");
                                break;
                            case "Mobile Number 2":
                                columnList.Add("Mobile Number 2");
                                break;
                            case "Mobile Number (Room Mate)":
                                columnList.Add("Mobile Number (Room Mate)");
                                break;
                            case "Mobile Number (Father)":
                                columnList.Add("Mobile Number (Father)");
                                break;
                            case "Mobile Number (Mother)":
                                columnList.Add("Mobile Number (Mother)");
                                break;
                            default:
                                break;
                        }
                    }
                }
                var studentExcelList = new List<List<object>>();

                foreach (var classRoutine in classRoutineList)
                {
                    var str = new List<object>();
                    #region InfomationViewList
                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case "Branch":
                                    if (!string.IsNullOrEmpty(classRoutine.BranchName))
                                    {
                                        str.Add(classRoutine.BranchName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Campus":
                                    if (!string.IsNullOrEmpty(classRoutine.CampusName))
                                    {
                                        str.Add(classRoutine.CampusName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Quantity":
                                    if (classRoutine.TotalQuantity != 0)
                                    {
                                        str.Add(classRoutine.TotalQuantity.ToString());
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Tpin":
                                    if (classRoutine.Tpin != 0)
                                    {
                                        str.Add(classRoutine.Tpin.ToString());
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case "Full Name":
                                    if (!string.IsNullOrEmpty(classRoutine.FullName))
                                    {
                                        str.Add(classRoutine.FullName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Nick Name":
                                    if (!string.IsNullOrEmpty(classRoutine.NickName))
                                    {
                                        str.Add(classRoutine.NickName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "T.Batch":
                                    if (classRoutine.TeacherBatch != null)
                                    {
                                        str.Add(classRoutine.TeacherBatch.ToString());
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Institute":
                                    if (!string.IsNullOrEmpty(classRoutine.Institute))
                                    {
                                        str.Add(classRoutine.Institute);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Department":
                                    if (!string.IsNullOrEmpty(classRoutine.Department))
                                    {
                                        str.Add(classRoutine.Department);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number 1":
                                    if (!string.IsNullOrEmpty(classRoutine.PersonalMobile))
                                    {
                                        str.Add(classRoutine.PersonalMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number 2":
                                    if (!string.IsNullOrEmpty(classRoutine.AlternativeMobile))
                                    {
                                        str.Add(classRoutine.AlternativeMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number (Room Mate)":
                                    if (!string.IsNullOrEmpty(classRoutine.RoomMatesMobile))
                                    {
                                        str.Add(classRoutine.RoomMatesMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number (Father)":
                                    if (!string.IsNullOrEmpty(classRoutine.FathersMobile))
                                    {
                                        str.Add(classRoutine.FathersMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number (Mother)":
                                    if (!string.IsNullOrEmpty(classRoutine.MothersMobile))
                                    {
                                        str.Add(classRoutine.MothersMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    #endregion

                    studentExcelList.Add(str);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, studentExcelList, footerList, "TeacherCampusRoutine-List-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }

        #endregion
    }
}