﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Mapping.UserAuth
{
    public partial class ImagesMap:BaseClassMap<Images,long>
    {
        public ImagesMap()
        {
            Table("Images");
            LazyLoad();
            Map(x => x.Name);
            //Map(x => x.GuidId);
            Map(x => x.Hash);
            //Map(x => x.UserImages).Column("Images").CustomSqlType("VARBINARY (MAX) FILESTREAM").Length(2147483647).LazyLoad();
            References(x => x.User).Column("User_id");
        }
    }
}
