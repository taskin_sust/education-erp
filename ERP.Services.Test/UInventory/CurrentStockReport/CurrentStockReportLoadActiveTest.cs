﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;
using Xunit;
using ReportType = UdvashERP.BusinessRules.ReportType;

namespace UdvashERP.Services.Test.UInventory.CurrentStockReport
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "CurrentStockSummary")]

    public class CurrentStockReportLoadActiveTest : CurrentStockReportBaseTest
    {
        private AuthorizeBranchNameDelegate branchNameDelegate;

        public CurrentStockReportLoadActiveTest()
        {
            branchNameDelegate = new AuthorizeBranchNameDelegate(OnGettingAuthorizeBranchNameList);
        }

        [Fact]
        public void Should_Return_Successfully()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            IList<dynamic> list = _currentStockSummaryService.LoadCurrentStock(branchNameDelegate, 0, 10, usermenu,
                _organizationFactory.Object.Id,
                _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
                _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
                _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
                _commonHelper.ConvertIdToList(_programFactory.Object.Id),
                _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
                 programSessionList);

            Assert.True(list.Count > 0);
        }

        [Fact]
        public void Should_Return_InvalidOrganization()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<MessageExceptions.InvalidDataException>(
               () => _currentStockSummaryService.LoadCurrentStock(branchNameDelegate, 0, 10, usermenu, 0,
              _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
               _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
               _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
               _commonHelper.ConvertIdToList(_programFactory.Object.Id),
               _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
                programSessionList));

        }

        [Fact]
        public void Should_Return_BranchWiseReport_InvalidBranch()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<MessageExceptions.InvalidDataException>(
            () => _currentStockSummaryService.LoadCurrentStock(branchNameDelegate, 0, 10, usermenu,
            _organizationFactory.Object.Id,
           new List<long>(),
            _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
            _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
            _commonHelper.ConvertIdToList(_programFactory.Object.Id),
            _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
             programSessionList));
        }

        [Fact]
        public void Should_Return_BranchWiseReport_InvalidUserMenu()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Object), _commonHelper.ConvertIdToList(_branchFactory.Object));
            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Throws<MessageExceptions.InvalidDataException>(
            () => _currentStockSummaryService.LoadCurrentStock(branchNameDelegate, 0, 10, null,
            _organizationFactory.Object.Id,
            _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
            _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
            _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
            _commonHelper.ConvertIdToList(_programFactory.Object.Id),
            _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
             programSessionList));
        }

        [Fact]
        public void Should_Return_Empty_BecauseOf_UnAuthorized_Program_Successfully()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemGroupFactory.Create().Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
            _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _sessionFactory.Create().Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(new List<SupplierBankDetails>() { _supplierBankDetailsFactory.Object }).Persist();

            var programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

            _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier(_supplierFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose()
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);
            var usermenu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object), _commonHelper.ConvertIdToList(_programFactory.Create().WithOrganization(_organizationFactory.Object).Persist().Object),
                _commonHelper.ConvertIdToList(_branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist().Object));

            Assert.Throws<MessageExceptions.InvalidDataException>(
            () => _currentStockSummaryService.LoadCurrentStock(branchNameDelegate, 0, 10, usermenu,
                _organizationFactory.Object.Id,
                _commonHelper.ConvertIdToList(_branchFactory.Object.Id),
                _commonHelper.ConvertIdToList(_itemGroupFactory.Object.Id),
                _commonHelper.ConvertIdToList(_itemFactory.Object.Id),
                _commonHelper.ConvertIdToList(_programFactory.Object.Id),
                _commonHelper.ConvertIdToList(_sessionFactory.Object.Id),
                 programSessionList));
        }

        [Fact(DisplayName = "will save 10 data but reurn only one data because of defined permission to see how authorization works")]
        //[Fact]
        public void Should_Return_OneData_For_a_Combination_Of_Organization_Program_Branch_Successfully()
        {
            List<Organization> orglist = new List<Organization>();
            List<Program> programList = new List<Program>();
            List<Branch> branchList = new List<Branch>();
            List<ItemGroup> itemGroupList = new List<ItemGroup>();
            List<Item> itemList = new List<Item>();
            List<Session> sessionList = new List<Session>();
            List<string> programSessionList = new List<string>();
            List<List<string>> psList = new List<List<string>>();

            for (int i = 0; i < 5; i++)
            {
                _organizationFactory.Create().Persist();
                _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
                _itemGroupFactory.Create().Persist();

                _supplierBankDetailsFactory.Create().WithBankBranch();
                _supplierFactory.Create().CreateWithSupplierBankDetails(new List<SupplierBankDetails>() { _supplierBankDetailsFactory.Object }).Persist();

                _itemFactory.CreateWith(0, _name + new Random().Next(0, int.MaxValue), (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate)
                    .WithItemGroup(_itemGroupFactory.Object).WithOrganization(_organizationFactory.Object).Persist();
                _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
                _sessionFactory.Create().Persist();
                _programSessionItemFactory.Create().WithProgramAndSession(_programFactory.Object, _sessionFactory.Object).WithItem(_itemFactory.Object).Persist();

                var studentExamList = _studentExamService.LoadBoardExam(true);
                int?[] selectedStudentExamList = studentExamList.Where(x => x.Name.Contains("SC")).Select(y => (int?)Convert.ToInt32(y.Id)).ToArray();
                var selectedAdmissionTypeList = new int?[] { 1, 2 };
                List<long> nonDeletableBranchIds;
                _programBranchSessionService.AssignProgram(_organizationFactory.Object.Id, _programFactory.Object.Id, _sessionFactory.Object.Id,
                    new[] { _branchFactory.Object.Id }, selectedAdmissionTypeList, selectedStudentExamList, out nonDeletableBranchIds);
                programSessionList = GenerateProgramSessionMix(_organizationFactory.Object, _programFactory.Object, _sessionFactory.Object);

                _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier(_supplierFactory.Object);
                _goodsReceiveDetailsFactory.Create().WithProgram(_programFactory.Object).WithSession(_sessionFactory.Object).WithItem(_itemFactory.Object).WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object);
                _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
                _goodsReceiveFactory.Persist(false);
                //_goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

                orglist.Add(_organizationFactory.Object);
                programList.Add(_programFactory.Object);
                branchList.Add(_branchFactory.Object);
                itemGroupList.Add(_itemGroupFactory.Object);
                itemList.Add(_itemFactory.Object);
                sessionList.Add(_sessionFactory.Object);
                psList.Add(programSessionList);
            }
            var usermenu = BuildUserMenu(orglist[0], programList[0], branchList[0]);

            Assert.True(_currentStockSummaryService.LoadCurrentStock(branchNameDelegate, 0, 10, usermenu,
                _organizationFactory.SingleObjectList[0].Id,
                _commonHelper.ConvertIdToList(_branchFactory.SingleObjectList[0].Id),
                _commonHelper.ConvertIdToList(_itemGroupFactory.SingleObjectList[0].Id),
                _commonHelper.ConvertIdToList(_itemFactory.SingleObjectList[0].Id),
                _commonHelper.ConvertIdToList(_programFactory.SingleObjectList[0].Id),
                _commonHelper.ConvertIdToList(_sessionFactory.SingleObjectList[0].Id),
                 psList[0]).Count == 1);

        }

    }
}
