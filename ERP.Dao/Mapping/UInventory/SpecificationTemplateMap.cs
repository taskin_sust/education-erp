using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class SpecificationTemplateMap : BaseClassMap<SpecificationTemplate, long>
    {
        public SpecificationTemplateMap()
        {
            Table("UINV_SpecificationTemplate");
            LazyLoad();
            Map(x => x.Name).Column("Name").Not.Nullable();
            Map(x => x.Rank).Column("Rank").Not.Nullable();
            HasMany(x => x.Items).KeyColumn("SpecificationTemplateId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.SpecificationCriteriaList).KeyColumn("SpecificationTemplateId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
