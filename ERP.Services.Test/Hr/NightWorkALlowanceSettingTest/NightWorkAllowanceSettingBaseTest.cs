﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.NightWorkALlowanceSettingTest
{
    public class NightWorkAllowanceSettingBaseTest : TestBase, IDisposable
    {
        #region Properties & Object Initialization

        protected readonly NightWorkAllowanceSettingFactory _nightworkAllowanceSettingFactory;
        protected readonly AttendanceSummaryFactory _attendanceSummaryFactory;

        protected readonly INightWorkAllowanceSettingService _nightWorkAllowanceSettingService;
        protected readonly IAllowanceSheetService _allowanceSheetService;
        protected readonly INightWorkService _nightWorkService;

        #endregion

        #region Constructor

        public NightWorkAllowanceSettingBaseTest()
        {
            _nightworkAllowanceSettingFactory = new NightWorkAllowanceSettingFactory(null, Session);
            _attendanceSummaryFactory = new AttendanceSummaryFactory(null, Session);
            //_attendanceAdjustmentFactory = new AttendanceAdjustmentFactory(null, Session);

            _nightWorkAllowanceSettingService = new NightWorkAllowanceSettingService(Session);
            _allowanceSheetService = new AllowanceSheetService(Session);
            _nightWorkService = new NightWorkService(session: Session);
        }

        #endregion

        #region disposal

        public new void Dispose()
        {
            _nightworkAllowanceSettingFactory.DeleteAll();
            base.Dispose();
        }

        #endregion
    }
}
