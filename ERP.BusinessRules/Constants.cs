﻿using System;
using System.ComponentModel;
using System.Reflection;


namespace UdvashERP.BusinessRules
{
    public static class Constants
    {
        public static int FirstBussinessYear { get { return 2014; } }
        public static int MessageSuccess { get { return 0; } }
        public static int MessageError { get { return 1; } }
        public static int PageSize { get { return 100; } }

        public static string PrnNoLength = "11";
        public static DateTime MaxDateTime { get { return DateTime.MaxValue; } }
        public static DateTime MinDateTime { get { return new DateTime(1950, 1, 1); } }

        //public static string CompanyName = "Udvash Academic & Admission Care";
    }

    public static class ApplicationRoles
    {
        public static string User
        {
            get { return "User"; }
        }
        public static string Developer
        {
            get { return "Developer"; }
        }
    }

    public static class ApplicationUsers
    {
        public static string SuperAdmin
        {
            get { return "superadmin"; }
        }
    }

    public static class SelectionType
    {
        public static int SelelectAll = 0;

        //Using Inventory
        public static int NotApplicable = 0;
        public static int SelectNone = -101;

        //payment status
        public static int IsPaid = 1;
        public static int IsDue = 0;

        public static int IsMaterialProvided = 1;
        public static int IsNotMaterialProvided = 0;

        public static int Present = 1;
        public static int Absent = 0;

        public static int TotalMarks = 1;
        public static int SubjectWiseMarks = 2;
        public static int AverageMarks = 2;

        public static int Male = 1;
        public static int Female = 2;
        public static int Other = 3;
        public static int All = 4;

        public static int MeritPosition = 1;
        public static int RollNumber = 2;

        public static int Complete = 1;
        public static int Pending = 2;

        public static int StudentWiseReport = 1;
        public static int SummaryReport = 2;


        public static int ProgramEvaluationByMgt = 1;
        public static int TeacherEvaluationByStd = 2;
        public static int TeacherEvaluationByBco = 3;

        //public static int CommunTypeSerialPort = 1;
        //public static int CommunTypeEthernet = 2;
        //public static int CommunTypeUSB = 3;


        //public static int TotalMarks = 1;


    }


    public sealed class RankOperation
    {

        public static readonly string Up = "up";
        public static readonly string Down = "down";
    }
    //public static class Search
    //{
    //    //public static int SelelectAll = 0;
    //}

    public enum RepeatType : int
    {
        EveryYear = 1,
        Once = 2,
        MaxTwo = 3
    }

    public enum StartingPoint : int
    {
        Joining = 1,
        Permanent = 2,
        One_Year_After_Permanent = 3
    }
    public enum PayType : int
    {
        WithPay = 1,
        WithoutPay = 0
    }

    public enum Gender
    {
        Male = 1,
        Female = 2,
        Combined = 3
    }
    public enum LeaveStatus
    {
        Approved = 2,
        Pending = 1,
        Rejected = 3,
        Cancelled = 4
    }
    public enum PublicLeave : int
    {
        Yes = 1,
        No = 0
    }
    public enum MemberEmploymentStatus : int
    {
        Probation = 1,
        Permanent = 2,
        PartTime = 3,
        Contractual = 4,
        Intern = 5,
        Retired = 6
    }
    public enum MaritalType : int
    {
        Single = 1,
        Married = 2,
        Widow = 3,
        Widower = 4,
        Divorced = 5
    }
    public enum Nationality : int
    {
        Bangladeshi = 1
    }

    public enum Religion : int
    {
        Islam = 1,
        Hinduism = 2,
        Christianity = 3,
        Buddhism = 4,
        Other = 5
    }
    public enum HrGradeScale
    {
        [Description("4")]
        Four = 4,
        [Description("5")]
        Five = 5,
    }
    public enum HrAcademicExamStatus
    {
        [Description("First Year")]
        FirstYear = 1,
        [Description("Second Year")]
        SecondYear = 2,
        [Description("Third Year")]
        ThirdYear = 3,
        [Description("Final Year")]
        FinalYear = 4,
        [Description("Appeared")]
        Appeared = 5
    }
    public enum HrAcademicResult
    {
        [Description("First Division")]
        FirstDivision = 1,
        [Description("Second Division")]
        SecondDivision = 2,
        [Description("Third Divison")]
        ThirdDivision = 3,
        [Description("Grade")]
        Grade = 4,
        [Description("Enrolled")]
        Appeared = 5
    }
    public enum HrAcademicResultType : int
    {
        Grade = 1,
        Division = 2,
    }

    public enum HrCompanyType : int
    {
        Govt = 1,
        Private = 2,
    }

    public enum StudentVisitType
    {
        [Description("Branch Visit")]
        BranchVisit = 1,
        [Description("Remote Visit")]
        RemoteVisit = 2
    }
    public enum ActivityPriority : int
    {
        First = 1,
        Second = 2,
        Third = 3

    }

    public enum University : int
    {
        BUET = 1,
        MEDICAL = 2,
        DU = 3,
        SUST = 4
    }
    public enum VersionOfStudy
    {
        Bangla = 1,
        English = 2,
        Combined = 3
    }
    public enum SurveyStatus
    {
        Complete = 1,
        Pending = 2
    }
    public enum BlockService
    {
        [Description("Auto Result")]
        AutoResult = 1,
        [Description("Merit List")]
        MeritList = 2,
        [Description("Send Result")]
        SendResult = 3,
        [Description("ID Card")]
        IdCard = 4,
        [Description("Material Distribution")]
        MaterialDistribution = 5,
        [Description("Student Transfer")]
        StudentTransfer = 6
    }
    public enum ConditionType
    {
        [Description("Payment Due")]
        PaymentDue = 1,
        [Description("Image Due")]
        ImageDue = 2,
        [Description("JSC/JDC Board info not updated")]
        Jsc = 3,
        [Description("SSC/Dakhil Board info not updated")]
        Ssc = 4,
        [Description("HSC/Alim Board info not updated")]
        Hsc = 5,
        [Description("Political Reference")]
        PoliticalReference = 6
    }
    public enum SurveyReportType
    {
        [Description("Student Wise Report")]
        StudentWiseReport = 1,
        [Description("Summary Report")]
        SummaryReport = 2
    }

    public enum SurveyType
    {
        [Description("Program Evaluation By Mgt")]
        ProgramEvaluationByMgt = 1,
        [Description("Teacher Evaluation By Std")]
        TeacherEvaluationByStd = 2,
        [Description("Teacher Evaluation By BCO")]
        TeacherEvaluationByBco = 3
    }
    public enum BoardExam
    {
        [Description("JSC/JDC")]
        Jsc = 1,
        [Description("SSC/Dakhil")]
        Ssc = 2,
        [Description("HSC/Alim")]
        Hsc = 3
    }

    public enum MeritPositionIn
    {
        Section = 1,
        Combined = 2
    }
    public enum ExamFeedback : int
    {
        Standard = 1,
        Hard = 2,
        Easy = 3
    }
    public enum BloodGroup
    {
        [Description("A+")]
        Apos = 1,
        [Description("A-")]
        Aneg = 2,
        [Description("B+")]
        Bpos = 3,
        [Description("B-")]
        Bneg = 4,
        [Description("AB+")]
        ABpos = 5,
        [Description("AB-")]
        ABneg = 6,
        [Description("O+")]
        Opos = 7,
        [Description("O-")]
        Oneg = 8,
    }
    public enum AttendanceAdjustmentStatus
    {
        Approved = 1,
        Rejected = 2,
        Pending = 3,
        Cancel = 4

    }

    public enum DayOffAdjustmentStatus
    {
        Approved = 1,
        Rejected = 2,
        Pending = 3,
        Cancel = 4

    }

    public enum DeviceCommunicationType
    {
        //[Description("Serial Port/RS485")]
        //CommunTypeSerialPort = 1,
        [Description("Ethernet")]
        Ethernet = 2,
        [Description("USB")]
        Usb = 3
    }
    public enum HouseStatus
    {
        In = 1,
        Out = 0
    }

    public enum ReportType
    {
        [Description("Programwise")]
        Programwise = 1,
        [Description("Branchwise")]
        Branchwise = 2
    }
    public enum HolidayTypeEnum
    {
        Management = 1,
        Gazetted = 2
    }
    public enum HolidayRepeatationType
    {
        Once = 1,
        Yearly = 2
    }
    public enum WeekDay
    {
        Saturday = 1,
        Sunday = 2,
        Monday = 3,
        Tuesday = 4,
        Wednesday = 5,
        Thursday = 6,
        Friday = 7

    }
    public enum HolidayWorkApprovalStatus
    {
        Half = 1,
        Full = 2
    }

    public enum DateTimeType
    {
        FullDateTime = 0,
        OnlyDate = 1,
        OnlyTime = 2
    }

    public enum AllowanceSettingHolidayType : int
    {
        Gazetted = 1,
        Management = 2,
        Weekend = 3
    }
    public enum AllowanceSettingEmployeeStatus : int
    {
        Probation = 1,
        Permanent = 2,
        PartTime = 3,
        Contractual = 4,
        Intern=5

    }

    public enum AllowanceSettingOverTimeType
    {
        [Description("Regular")]
        Regular = 1,
        [Description("Gazetted")]
        Gazetted = 2,
        [Description("Management/Weekend")]
        Management = 3
    }
    public enum CalculationOnTypeEnum
    {
        Basic = 1,
        Gross = 2
    }
    public static class EnumExtension
    {
        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr =
                           Attribute.GetCustomAttribute(field,
                             typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }
            return null;
        }
    }

    //public static class LoginOperationStatus
    //{
    //    public static int Success { get { return 1; } }
    //    public static int Fail { get { return 2; } }
    //    public static int ChangePassword { get { return 3; } }
    //    public static int Lock { get { return 4; } }
    //}

    public static class UniversityText
    {
        public static int BUET { get { return (int)University.BUET; } }
        public static int MEDICAL { get { return (int)University.MEDICAL; } }
        public static int DU { get { return (int)University.DU; } }
        public static int SUST { get { return (int)University.SUST; } }

    }

    public static class ExamFeedbackText
    {
        public static int Standard { get { return (int)ExamFeedback.Standard; } }
        public static int Hard { get { return (int)ExamFeedback.Hard; } }
        public static int Easy { get { return (int)ExamFeedback.Easy; } }

    }

    public static class DbConnectionString
    {
        public static string UerpDbConnectionString { get; set; }
        public static string MediaDbConnectionString { get; set; }
    }

    public class ErrorMessage
    {
        public int Pin { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
        public int RowNumber { get; set; }

    }

}
