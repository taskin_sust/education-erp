﻿namespace Qmaster.Print
{
    partial class MainUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuMainTop = new System.Windows.Forms.MenuStrip();
            this.mTopFile = new System.Windows.Forms.ToolStripMenuItem();
            this.smTopiLogin = new System.Windows.Forms.ToolStripMenuItem();
            this.smTopiLogout = new System.Windows.Forms.ToolStripMenuItem();
            this.smTopiPrintQuestion = new System.Windows.Forms.ToolStripMenuItem();
            this.smTopiTest = new System.Windows.Forms.ToolStripMenuItem();
            this.smTopiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mTopAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.mTopHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mTopWhat = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMainTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuMainTop
            // 
            this.menuMainTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mTopFile,
            this.mTopAbout,
            this.mTopHelp,
            this.mTopWhat});
            this.menuMainTop.Location = new System.Drawing.Point(0, 0);
            this.menuMainTop.Name = "menuMainTop";
            this.menuMainTop.Size = new System.Drawing.Size(981, 24);
            this.menuMainTop.TabIndex = 0;
            this.menuMainTop.Text = "menuStrip1";
            // 
            // mTopFile
            // 
            this.mTopFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.smTopiLogin,
            this.smTopiLogout,
            this.smTopiPrintQuestion,
            this.smTopiTest,
            this.smTopiExit});
            this.mTopFile.Name = "mTopFile";
            this.mTopFile.Size = new System.Drawing.Size(37, 20);
            this.mTopFile.Text = "File";
            // 
            // smTopiLogin
            // 
            this.smTopiLogin.Name = "smTopiLogin";
            this.smTopiLogin.Size = new System.Drawing.Size(152, 22);
            this.smTopiLogin.Text = "Login";
            this.smTopiLogin.Click += new System.EventHandler(this.smTopiLogin_Click);
            // 
            // smTopiLogout
            // 
            this.smTopiLogout.Name = "smTopiLogout";
            this.smTopiLogout.Size = new System.Drawing.Size(152, 22);
            this.smTopiLogout.Text = "Logout";
            this.smTopiLogout.Click += new System.EventHandler(this.smTopiLogout_Click);
            // 
            // smTopiPrintQuestion
            // 
            this.smTopiPrintQuestion.Name = "smTopiPrintQuestion";
            this.smTopiPrintQuestion.Size = new System.Drawing.Size(152, 22);
            this.smTopiPrintQuestion.Text = "Print Question";
            this.smTopiPrintQuestion.Click += new System.EventHandler(this.smTopiPrintQuestion_Click);
            // 
            // smTopiTest
            // 
            this.smTopiTest.Name = "smTopiTest";
            this.smTopiTest.Size = new System.Drawing.Size(152, 22);
            this.smTopiTest.Text = "Setup Printer";
            this.smTopiTest.Click += new System.EventHandler(this.smTopiTest_Click);
            // 
            // smTopiExit
            // 
            this.smTopiExit.Name = "smTopiExit";
            this.smTopiExit.Size = new System.Drawing.Size(152, 22);
            this.smTopiExit.Text = "Exit";
            this.smTopiExit.Click += new System.EventHandler(this.smTopiExit_Click);
            // 
            // mTopAbout
            // 
            this.mTopAbout.Name = "mTopAbout";
            this.mTopAbout.Size = new System.Drawing.Size(52, 20);
            this.mTopAbout.Text = "About";
            this.mTopAbout.Click += new System.EventHandler(this.mTopAbout_Click);
            // 
            // mTopHelp
            // 
            this.mTopHelp.Name = "mTopHelp";
            this.mTopHelp.Size = new System.Drawing.Size(44, 20);
            this.mTopHelp.Text = "Help";
            // 
            // mTopWhat
            // 
            this.mTopWhat.Name = "mTopWhat";
            this.mTopWhat.Size = new System.Drawing.Size(24, 20);
            this.mTopWhat.Text = "?";
            // 
            // MainUi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 765);
            this.Controls.Add(this.menuMainTop);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuMainTop;
            this.Name = "MainUi";
            this.Text = "Qmaster Print Home";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainUi_Load);
            this.menuMainTop.ResumeLayout(false);
            this.menuMainTop.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMainTop;
        private System.Windows.Forms.ToolStripMenuItem mTopFile;
        private System.Windows.Forms.ToolStripMenuItem smTopiLogin;
        private System.Windows.Forms.ToolStripMenuItem smTopiLogout;
        private System.Windows.Forms.ToolStripMenuItem smTopiPrintQuestion;
        private System.Windows.Forms.ToolStripMenuItem smTopiTest;
        private System.Windows.Forms.ToolStripMenuItem smTopiExit;
        private System.Windows.Forms.ToolStripMenuItem mTopAbout;
        private System.Windows.Forms.ToolStripMenuItem mTopHelp;
        private System.Windows.Forms.ToolStripMenuItem mTopWhat;
    }
}

