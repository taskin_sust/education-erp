﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceAdjustmentTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceAdjustment")]
    public class AttendanceAdjustmentLoadTest : AttendanceAdjustmentBaseTest
    {
        #region LoadById

        [Fact]
        public void Should_Return_LoadById_Not_Null_Check()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016, 12, 5),new DateTime(2016, 12, 5, 10, 10, 10),new DateTime(2016, 12, 5, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm).Persist();
            var getObj = _attendanceAdjustmentService.LoadById(_attendanceAdjustmentFactory.Object.Id + 1);
            Assert.Null(getObj);
        }

        [Fact]
        public void Should_Return_LoadById_Null_Check()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016, 12, 5),
                new DateTime(2016, 12, 5, 10, 10, 10),
                new DateTime(2016, 12, 5, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm).Persist();
            var getObj = _attendanceAdjustmentService.LoadById(_attendanceAdjustmentFactory.Object.Id);
            Assert.NotNull(getObj);
        }

        #endregion

        #region LoadPendingListForMentor

        [Fact]
        public void Should_Return_LoadPendingListForMentor_InvalidException_For_Mentor()
        {
            Assert.Throws<InvalidDataException>(() => _attendanceAdjustmentService.LoadPendingListForMentor(null));
        }

        [Fact]
        public void Should_Return_LoadPendingListForMentor_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            _attendanceAdjustmentFactory.CreateWith("Test", new DateTime(2016,12,5),new DateTime(2016, 12, 5, 10, 10, 10),new DateTime(2016, 12, 5, 11, 10, 10), "Due to",
                (int)AttendanceAdjustmentStatus.Pending, 0).WithTeamMember(tm).Persist();
            var mentor = tm.MentorHistory.Select(x => x.Mentor).OrderByDescending(x => x.CreationDate).FirstOrDefault();
            Assert.Equal(1, _attendanceAdjustmentService.LoadPendingListForMentor(mentor).Count());
        }

        #endregion

        #region LoadAllMentorAttendanceAdjustmentRecentHistory

        [Fact]
        public void Should_Return_LoadAllMentorAttendanceAdjustmentRecentHistory_Invalid_TeamMember()
        {
            Assert.Throws<InvalidDataException>(
                () => _attendanceAdjustmentService.LoadAllMentorAttendanceAdjustmentRecentHistory(0, 10, "", "", null, "", DateTime.Now, DateTime.Now));
        }
        [Fact]
        public void Should_Return_LoadAllMentorAttendanceAdjustmentRecentHistory_Successfully()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));
            _attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            Assert.True(_attendanceAdjustmentService.LoadAllMentorAttendanceAdjustmentRecentHistory(0, 100, "", "", tm.MentorHistory.FirstOrDefault().Mentor, "", new DateTime(2016, 12, 1), new DateTime(2016, 12, 31)).Any());
        }
        #endregion

        #region LoadPendingListForHr

        [Fact]
        public void Should_Return_LoadPendingListForHr_Invalid_TeamMember()
        {
            Assert.Throws<InvalidDataException>(
                () => _attendanceAdjustmentService.LoadPendingListForHr(null, ""));
        }

        [Fact]
        public void Should_Return_LoadPendingListForHr_Successfully()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;

            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));
            _attendanceAdjustmentFactory.CreateMoreWithPendingAdjustment(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);

            Assert.True(_attendanceAdjustmentService.LoadPendingListForHr(userMenu, "").Any());
        }

        [Fact]
        public void Should_Return_LoadPendingListForHr_Exception_For_Invalid_Organization()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;

            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));
            _attendanceAdjustmentFactory.CreateMoreWithPendingAdjustment(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);

            Assert.True(_attendanceAdjustmentService.LoadPendingListForHr(userMenu, "", commonHelper.ConvertIdToList(org.Id + 1)).Any());
        }

        #endregion

        #region LoadAllHrAttendanceAdjustmentRecentHistory

        [Fact]
        public void Should_Return_LoadAllHrAttendanceAdjustmentRecentHistory_Invalid_UserMenu()
        {
            Assert.Throws<InvalidDataException>(
                () => _attendanceAdjustmentService.LoadAllHrAttendanceAdjustmentRecentHistory(null, 0, 10, "", "", "", DateTime.Now, DateTime.Now, null, null, null, null));
        }

        [Fact]
        public void Should_Return_LoadAllHrAttendanceAdjustmentRecentHistory_Successfully()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));
            _attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var org = department.Organization;
            var campus = employmentHistory.Campus;
            var branch = employmentHistory.Campus.Branch;

            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            Assert.True(_attendanceAdjustmentService.LoadAllHrAttendanceAdjustmentRecentHistory(userMenu, 0, 100, "", "", tm.Pin.ToString(), new DateTime(2016, 12, 1),
                new DateTime(2016, 12, 31), commonHelper.ConvertIdToList(org.Id), commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id), commonHelper.ConvertIdToList(department.Id)).Any());
        }

        [Fact]
        public void Should_Return_LoadAllHrAttendanceAdjustmentRecentHistory_InvalidException_For_Organization()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));
            _attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var department = employmentHistory.Department;
            var org = department.Organization;
            var campus = employmentHistory.Campus;
            var branch = employmentHistory.Campus.Branch;

            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            Assert.Throws<InvalidDataException>(() => _attendanceAdjustmentService.LoadAllHrAttendanceAdjustmentRecentHistory(userMenu, 0, 100, "", "", tm.Pin.ToString(), new DateTime(2016, 12, 1),
                new DateTime(2016, 12, 31), commonHelper.ConvertIdToList(org.Id + 1), commonHelper.ConvertIdToList(branch.Id), commonHelper.ConvertIdToList(campus.Id)
                , commonHelper.ConvertIdToList(department.Id)));
        }


        #endregion

        #region LoadAllHrAttendanceAdjustmentRecentHistory

        [Fact]
        public void Should_Return_CountMentorAttendanceAdjustmentRecentHistory_Invalid_TeamMember()
        {
            Assert.Throws<InvalidDataException>(
                () => _attendanceAdjustmentService.CountMentorAttendanceAdjustmentRecentHistory(null, "", DateTime.Now, DateTime.Now));
        }
        [Fact]
        public void Should_Return_CountMentorAttendanceAdjustmentRecentHistory_Successfully()
        {

            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var tm = PersistTeamMember(joiningDate, dateTo);

            _attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);

            Assert.NotEqual(0, _attendanceAdjustmentService.CountMentorAttendanceAdjustmentRecentHistory(tm.MentorHistory.FirstOrDefault().Mentor, "", new DateTime(2016, 12, 1), new DateTime(2016, 12, 31)));
        }

        #endregion

        #region LoadAllHrAttendanceAdjustmentRecentHistory

        [Fact]
        public void Should_Return_CountHrAttendanceAdjustmentRecentHistory_Invalid_UserMenu()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));
            _attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);

            Assert.Throws<InvalidDataException>(
                () => _attendanceAdjustmentService.CountHrAttendanceAdjustmentRecentHistory(null, "", DateTime.Now, DateTime.Now, null, null, null, null));
        }

        [Fact]
        public void Should_Return_CountHrAttendanceAdjustmentRecentHistory_InvalidException_For_Organization()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));

            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;

            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));
            _attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);

            Assert.Throws<InvalidDataException>(() => _attendanceAdjustmentService.CountHrAttendanceAdjustmentRecentHistory(userMenu, tm.Pin.ToString(), new DateTime(2016, 12, 1),
                new DateTime(2016, 12, 1), commonHelper.ConvertIdToList(org.Id + 1), null, null, null));
        }


        [Fact]
        public void Should_Return_CountHrAttendanceAdjustmentRecentHistory_Successfully()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));

            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var org = employmentHistory.Department.Organization;
            var branch = employmentHistory.Campus.Branch;

            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));
            _attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);

            Assert.NotEqual(0, _attendanceAdjustmentService.CountHrAttendanceAdjustmentRecentHistory(userMenu, tm.Pin.ToString(), new DateTime(2016, 12, 1),
                new DateTime(2016, 12, 31), commonHelper.ConvertIdToList(org.Id), null, null, null));
        }

        #endregion

        #region LoadAllRowAttendanceAdjustmentSelfMember

        [Fact]
        public void Should_Return_LoadAllRowAttendanceAdjustmentSelfMember_Load_Successfully()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));
            _attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            Assert.True(_attendanceAdjustmentService.LoadAllRowAttendanceAdjustmentSelfMember(tm, 0, 100, "", "").Any());
        }

        [Fact]
        public void Should_Return_LoadAllRowAttendanceAdjustmentSelfMember_Load_Successfully_Without_TeamMember()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));
            _attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            Assert.True(_attendanceAdjustmentService.LoadAllRowAttendanceAdjustmentSelfMember(null, 0, 100, "", "").Any());
        }

        #endregion

        #region CountRowAttendanceAdjustmentSelfMember

        [Fact]
        public void Should_Return_CountRowAttendanceAdjustmentSelfMember_Successfully()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));
            _attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            Assert.NotEqual(0, _attendanceAdjustmentService.CountRowAttendanceAdjustmentSelfMember(tm));
        }

        [Fact]
        public void Should_Return_CountRowAttendanceAdjustmentSelfMember_Load_Successfully_Without_TeamMember()
        {
            var tm = PersistTeamMember(new DateTime(2016, 12, 1), new DateTime(2016, 12, 1));
            _attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            Assert.NotEqual(0, _attendanceAdjustmentService.CountRowAttendanceAdjustmentSelfMember(null));
        }

        #endregion

        #region GetTeamMemberAttendanceAdjustmentPostDateTime

        [Fact]
        public void Should_Return_GetTeamMemberAttendanceAdjustmentPostDateTime_Successfully()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;
            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();//

            _attendanceAdjustmentFactory.CreateMoreWithPendingAdjustment(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);//

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);//

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();//

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();//

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();//

            salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();//

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation);

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);

            DateTime? dtTime = _attendanceAdjustmentService.GetTeamMemberAttendanceAdjustmentPostDateTime(tm.Id);
            Assert.True(dtTime != null);
        }

        [Fact]
        public void Should_Return__GetTeamMemberAttendanceAdjustmentPostDateTime_Null_For_TeamMember()
        {
            Assert.Null(_attendanceAdjustmentService.GetTeamMemberAttendanceAdjustmentPostDateTime(null));
        }

        [Fact]
        public void Should_Return__GetTeamMemberAttendanceAdjustmentPostDateTime_Null_For_InvalidTeamMember()
        {
            var lastDayOfMonth = DateTime.DaysInMonth(2016, 12);
            var startDate = new DateTime(2016, 12, 1);
            var endDate = new DateTime(2016, 12, lastDayOfMonth);
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            //For holidaySettings
            const long orgId = 3;
            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), startDate.Month, startDate.Year);

            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var employmentHistory = tm.EmploymentHistory.FirstOrDefault();
            var branch = employmentHistory.Campus.Branch;
            var campus = employmentHistory.Campus;
            var dept = employmentHistory.Department;
            var designation = employmentHistory.Designation;

            zoneSettingFactory.Create(180, 2, 1).WithOrganization(org).Persist();//

            _attendanceAdjustmentFactory.CreateMoreWithPendingAdjustment(startDate, tm);

            DateTime holyDayDate = holidaySettingsList.FirstOrDefault().DateFrom;

            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.ObjectList);
            var userMenu = BuildUserMenu(commonHelper.ConvertIdToList(org), null, commonHelper.ConvertIdToList(branch));

            _holidayWorkFactory.CreateWith(holyDayDate, (int)HolidayWorkApprovalStatus.Half).WithTeamMember(tm).Persist(tm, false, true, userMenu);//

            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);

            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create("Permanent")
                .WithOrganization(org)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25)
                .WithEffectiveClosingDate(startDate, endDate.AddMonths(2))
                .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList)
                .Persist(userMenu);//

            _memberLoanApplicationFactory.Create().WithTeamMember(tm).Persist();//

            _memberLoanFactory.Create()
                .CreateWithLoanApprovedAmount(50000, 2000)
                .WithMemberLoanApplication(_memberLoanApplicationFactory.Object)
                .Persist();//

            _memberLoanRefundFactory.Create()
                .WithRefundAmount(2000)
                .WithRemark("test")
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryOrganization(org)
                .WithTeamMember(tm)
                .Persist();//

            salaryHistoryFactory.CreateWith(1000, 800, 200, DateTime.Now.AddMonths(-2))
               .WithPurpose()
               .WithTeamMember(tm)
               .WithCampus(campus)
               .WithDepartment(dept)
               .WithDesignation(designation)
               .WithOrganization(org).Persist();//

            _salarySheetFactory.Create().WithTeamMember(tm)
                .WithSalaryHistory(salaryHistoryFactory.Object)
                .WithSalaryOrganization(org)
                .WithSalaryBranch(branch)
                .WithSalaryCampus(campus)
                .WithSalaryDepartment(dept)
                .WithSalaryDesignation(designation)
                .WithJobOrganization(org)
                .WithJobBranch(branch)
                .WithJobCampus(campus)
                .WithJobDepartment(dept)
                .WithJobDesignation(designation);

            _salarySheetDetailsFactory.Create()
                    .WithZone(zoneSettingFactory.Object, 0, 0, (decimal)zoneSettingFactory.Object.DeductionMultiplier)
                    .WithAbsentDay((decimal)org.DeductionPerAbsent, 3, 120)
                    .WithSalarySheet(_salarySheetFactory.Object);

            var ebf = _employeeBenefitsFundSettingFactory.Object;

            _memberEbfHistoryFactory.CreateWith(0, ebf.CalculationOn, ebf.EmployeeContribution, ebf.EmployerContribution, startDate, null)
                .WithEmployeeBenifitsFundSetting(_employeeBenefitsFundSettingFactory.Object)
                .WithSalarySheet(_salarySheetFactory.Object);

            _salarySheetFactory
                .WithSalarySheetDetails(_salarySheetDetailsFactory.SingleObjectList)
                .WithMemberEbfHistory(_memberEbfHistoryFactory.SingleObjectList);

            _salarySheetService.SaveOrUpdate(_salarySheetFactory.SingleObjectList);
            Assert.Null(_attendanceAdjustmentService.GetTeamMemberAttendanceAdjustmentPostDateTime(tm.Id+1));
        }

        #endregion
    }
}