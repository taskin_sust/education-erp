﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.DirectGoodsReceive
{
    public class DirectGoodsReceiveBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper CommonHelper;
        protected readonly GoodsReceiveFactory _goodsReceiveFactory;
        protected readonly GoodsReceiveDetailsFactory _goodsReceiveDetailsFactory;
        protected readonly CurrentStockSummaryFactory _currentStockSummaryFactory;
        protected readonly ItemFactory _itemFactory;
        protected readonly ProgramFactory _programFactory;
        protected readonly SessionFactory _sessionFactory;
        protected readonly ProgramSessionItemFactory _programSessionItemFactory;
        protected readonly SupplierBankDetailsFactory _supplierBankDetailsFactory;
        protected readonly SupplierFactory _supplierFactory;
        protected readonly SupplierItemFactory _supplierItemFactory;
        protected readonly SupplierPriceQuoteFactory _supplierPriceQuoteFactory;
        protected readonly DirectWorkOrderFactory _directWorkOrderFactory;
        protected readonly WorkOrderCriteriaFactory _workOrderCriteriaFactory;
        protected readonly WorkOrderDetailsFactory _workOrderDetailsFactory;

        protected readonly IGoodsReceiveService _goodsReceiveService;

        public string _name = "Test_Demo_Quotation01" + new Random().Next(9999999);
        #endregion

        public DirectGoodsReceiveBaseTest()
        {
            CommonHelper = new CommonHelper();
            _goodsReceiveFactory = new GoodsReceiveFactory(null, Session);
            _goodsReceiveDetailsFactory = new GoodsReceiveDetailsFactory(null, Session);
            _currentStockSummaryFactory = new CurrentStockSummaryFactory(null, Session);
            _itemFactory = new ItemFactory(null, Session);
            _programFactory = new ProgramFactory(null, Session);
            _sessionFactory = new SessionFactory(null, Session);
            _programSessionItemFactory = new ProgramSessionItemFactory(null, Session);
            // _quotationFactory = new QuotationFactory(null, session);
            _supplierBankDetailsFactory = new SupplierBankDetailsFactory(null, Session);
            _supplierFactory = new SupplierFactory(null, Session);
            _supplierItemFactory = new SupplierItemFactory(null, Session);
            _supplierPriceQuoteFactory = new SupplierPriceQuoteFactory(null, Session);
            //_workOrderFactory = new WorkOrderFactory(null, session);
            _directWorkOrderFactory = new DirectWorkOrderFactory(null, Session);
            _workOrderCriteriaFactory = new WorkOrderCriteriaFactory(null, Session);
            _workOrderDetailsFactory = new WorkOrderDetailsFactory(null, Session);

            _goodsReceiveService = new GoodsReceiveService(Session);
        }

        public void Dispose()
        {
            if (_goodsReceiveFactory.Object != null)
                foreach (var recDetail in _goodsReceiveFactory.Object.GoodsReceiveDetails)
                {
                    if (recDetail.GoodsReceive.Branch != null && recDetail.Item != null)
                    {
                        string swData = "DELETE FROM [UINV_CurrentStockSummary] WHERE [BranchId] = " + recDetail.GoodsReceive.Branch.Id + " and [ItemId] =" + recDetail.Item.Id + " ; ";
                        Session.CreateSQLQuery(swData).ExecuteUpdate();
                    }
                }
            _goodsReceiveDetailsFactory.CleanUp();
            _goodsReceiveFactory.CleanUp();
            _workOrderDetailsFactory.CleanUp();
            _directWorkOrderFactory.CleanUp();
            _supplierBankDetailsFactory.CleanUp();
            _supplierItemFactory.CleanUp();
            _supplierFactory.CleanUp();
            _programSessionItemFactory.CleanUp();
            _itemFactory.CleanUp();
            branchFactory.Cleanup();
            _programFactory.Cleanup();
        }

    }
}
