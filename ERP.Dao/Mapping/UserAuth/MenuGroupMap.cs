using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Mapping.UserAuth {


    public partial class MenuGroupMap : BaseClassMap<MenuGroup, long>
    {
        
        public MenuGroupMap() {
			Table("MenuGroup");
			LazyLoad();
            //Not.LazyLoad();
            Map(x => x.Rank);
            Map(x => x.Name);
            Map(x => x.DisplayText);
            HasMany(x => x.Menus).KeyColumn("MenuGroupId").Cascade.All().Where(ss => ss.Status != MenuGroup.EntityStatus.Delete).OrderBy("Rank").LazyLoad(); 
        }
    }
}
