using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students {


    public partial class StudentIdCardMap : BaseClassMap<StudentIdCard, long>
    {
        
        public StudentIdCardMap() {
			Table("StudentIdCard");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");

            Map(x => x.IsDistributed).Column("IsDistributed");
            Map(x => x.PrintCount).Column("PrintCount");
            References(x => x.StudentProgram).Column("StudentProgramId");
            
        }
    }
}
