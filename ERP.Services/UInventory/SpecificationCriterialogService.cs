using log4net;
using NHibernate;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
public interface ISpecificationCriteriaLogService : IBaseService
{
    #region Operational Function

    bool SaveOrUpdate(SpecificationCriteriaLog specificationCriteriaLog);

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function

    #endregion

    #region Helper Function

    #endregion
}
public class SpecificationCriteriaLogService : BaseService, ISpecificationCriteriaLogService {
    #region Logger
    private readonly ILog _logger = LogManager.GetLogger("InventoryService");
    #endregion

    #region Propertise & Object Initialization
    private readonly CommonHelper _commonHelper;
    private readonly ISpecificationCriteriaLogDao _specificationCriteriaLogDao;
    public SpecificationCriteriaLogService(ISession session)
    {
        Session = session;
        _commonHelper = new CommonHelper();
        _specificationCriteriaLogDao=new SpecificationCriteriaLogDao{Session = session};}
    #endregion

    #region Operational Functions

    public bool SaveOrUpdate(SpecificationCriteriaLog specificationCriteriaLog)
    {

        return true;
    }

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function


    #endregion

    #region Helper function



    #endregion
}
}
                                    
