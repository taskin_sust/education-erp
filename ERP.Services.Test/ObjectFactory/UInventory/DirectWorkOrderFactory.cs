using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class DirectWorkOrderFactory : ObjectFactoryBase<WorkOrder>
    {
        #region Object Initialization

        private ItemFactory _itemFactory;
        private ProgramFactory _programFactory;
        private SessionFactory _sessionFactory;
        private SupplierFactory _supplierFactory;
        private BranchFactory _branchFactory;
        private WorkOrderCriteriaFactory _workOrderCriteriaFactory;
        private WorkOrderDetailsFactory _workOrderDetailsFactory;

        private SupplierBankDetailsFactory _supplierBankDetailsFactory;
        private readonly IWorkOrderService _workOrderService;

        public DirectWorkOrderFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _itemFactory = new ItemFactory(null, _session);
            _programFactory = new ProgramFactory(this, _session);
            _sessionFactory = new SessionFactory(this, _session);
            _supplierFactory = new SupplierFactory(this, _session);
            _branchFactory = new BranchFactory(this, _session);
            _supplierBankDetailsFactory = new SupplierBankDetailsFactory(this, _session);
            _workOrderCriteriaFactory = new WorkOrderCriteriaFactory(this, _session);
            _workOrderDetailsFactory = new WorkOrderDetailsFactory(this, _session);

            _workOrderService = new WorkOrderService(session);
        }

        #endregion

        #region Create

        public DirectWorkOrderFactory Create()
        {
            Object = new WorkOrder()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                //Name = "",
                //Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = WorkOrder.EntityStatus.Active,
                WorkOrderStatus = WorkOrderStatus.Issued,
                DeliveryDate = DateTime.Now.AddDays(1),
                OrderedQuantity = 15,
                TotalCost = 150,
                WorkOrderDate = DateTime.Now.AddDays(1),
                WorkOrderNo = Guid.NewGuid().ToString().Substring(0, 5) + "123456",
                Remarks = "this is a work order",
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public DirectWorkOrderFactory WithSupplier()
        {
            var bankDetails = _supplierBankDetailsFactory.Create().WithBankBranch();
            var supplier = _supplierFactory.Create().CreateWithSupplierBankDetails(bankDetails.SingleObjectList).Persist().Object;
            //var supplier = _supplierFactory.Create().Persist().Object;
            Object.Supplier = supplier;
            return this;
        }

        public DirectWorkOrderFactory WithSupplier(Supplier supplier)
        {
            Object.Supplier = supplier;
            return this;
        }

        public DirectWorkOrderFactory WithBranch(bool isCorporate = false)
        {
            var branch =
                _branchFactory.Create().IsCorporate(isCorporate).WithOrganization().Persist().Object;
            Object.Branch = branch;
            return this;
        }

        public DirectWorkOrderFactory WithBranch(Branch branch)
        {
            Object.Branch = branch;
            return this;
        }

        public DirectWorkOrderFactory WithWorkOrderDetails()
        {
            var item = _itemFactory.Create(ItemType.ProgramItem).WithItemGroup().WithOrganization().Persist().Object;
            var program = _programFactory.Create().WithOrganization().Persist().Object;
            var session = _sessionFactory.Create().Persist().Object;
            
            var workOrderDetails = new WorkOrderDetails()
            {
                Item = item,
                Program = program,
                Session = session,
                WorkOrder = Object,
                Purpose = (int)Purpose.ProgramPurpose,
                PurposeProgram = program,
                PurposeSession = session,
                OrderQuantity = 15,
                UnitCost = 10,
                TotalCost = 150
            };
            var workOrderCriteriaList = new List<WorkOrderCriteria>()
            {
                new WorkOrderCriteria()
                {
                    Criteria = "Brand",
                    CriteriaValue = "Bashundhara",
                    WorkOrderDetail = workOrderDetails
                },
                new WorkOrderCriteria()
                {
                    Criteria = "Amount",
                    CriteriaValue = "10",
                    WorkOrderDetail = workOrderDetails
                }
            };
            workOrderDetails.WorkOrderCriteriaList = workOrderCriteriaList;
            Object.WorkOrderDetails = new List<WorkOrderDetails>()
            {
                workOrderDetails
            };
            return this;
        }

        public DirectWorkOrderFactory WithWorkOrderDetails(Organization organization, ItemType itemType = ItemType.ProgramItem, int? purpose = (int)Purpose.ProgramPurpose)
        {
            var item = _itemFactory.Create(itemType).WithItemGroup().WithOrganization(organization).Persist().Object;
            Program program = _programFactory.Create().WithOrganization(organization).Persist().Object;
            Session session = _sessionFactory.Create().Persist().Object;
            
            if (itemType != ItemType.ProgramItem)
            {
                program = null;
                session = null;
            }
            
            var workOrderDetails = new WorkOrderDetails()
            {
                Item = item,
                Program = program,
                Session = session,
                WorkOrder = Object,
                Purpose = purpose,
                OrderQuantity = 15,
                UnitCost = 10,
                TotalCost = 150
            };
            
            if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose == null)
            {
                workOrderDetails.PurposeProgram = null;
                workOrderDetails.PurposeSession = null;
            }
            else if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose != null)
            {
                workOrderDetails.PurposeProgram = program;
                workOrderDetails.PurposeSession = session;
            }
            else if (item.ItemType == (int)ItemType.CommonItem && workOrderDetails.Purpose == null)
            {
                workOrderDetails.PurposeProgram = null;
                workOrderDetails.PurposeSession = null;
            }
            else if (item.ItemType == (int)ItemType.CommonItem && (workOrderDetails.Purpose > 0 && workOrderDetails.Purpose <= 3))
            {
                workOrderDetails.PurposeProgram = program;
                workOrderDetails.PurposeSession = session;
            }
            else
            {
                workOrderDetails.PurposeProgram = program;
                workOrderDetails.PurposeSession = session;
            }

            var workOrderCriteriaList = new List<WorkOrderCriteria>()
            {
                new WorkOrderCriteria()
                {
                    Criteria = "Brand",
                    CriteriaValue = "Bashundhara",
                    WorkOrderDetail = workOrderDetails
                },
                new WorkOrderCriteria()
                {
                    Criteria = "Amount",
                    CriteriaValue = "10",
                    WorkOrderDetail = workOrderDetails
                }
            };
            workOrderDetails.WorkOrderCriteriaList = workOrderCriteriaList;
            Object.WorkOrderDetails = new List<WorkOrderDetails>()
            {
                workOrderDetails
            };
            return this;
        }

        public DirectWorkOrderFactory WithWorkOrderDetails(Organization organization, Program program, Session session)
        {
            var item = _itemFactory.Create().WithItemGroup().WithOrganization(organization).Persist().Object;
            
            var workOrderDetails = new WorkOrderDetails()
            {
                Item = item,
                //Program = program,
                //Session = session,
                WorkOrder = Object,
                Purpose = (int)Purpose.ProgramPurpose,
                OrderQuantity = 15,
                UnitCost = 10,
                TotalCost = 150
            };

            if (item.ItemType == (int)ItemType.ProgramItem)
            {
                workOrderDetails.Program = program;
                workOrderDetails.Session = session;
            }
            else
            {
                program = null;
                session = null;
            }

            if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose == null)
            {
                workOrderDetails.PurposeProgram = null;
                workOrderDetails.PurposeSession = null;
            }
            else if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose != null)
            {
                workOrderDetails.PurposeProgram = program;
                workOrderDetails.PurposeSession = session;
            }
            else if (item.ItemType == (int)ItemType.CommonItem && workOrderDetails.Purpose == null)
            {
                workOrderDetails.PurposeProgram = null;
                workOrderDetails.PurposeSession = null;
            }
            else if (item.ItemType == (int)ItemType.CommonItem && (workOrderDetails.Purpose > 0 && workOrderDetails.Purpose <= 3))
            {
                workOrderDetails.PurposeProgram = program;
                workOrderDetails.PurposeSession = session;
            }
            else
            {
                workOrderDetails.PurposeProgram = program;
                workOrderDetails.PurposeSession = session;
            }

            var workOrderCriteriaList = new List<WorkOrderCriteria>()
            {
                new WorkOrderCriteria()
                {
                    Criteria = "Brand",
                    CriteriaValue = "Bashundhara",
                    WorkOrderDetail = workOrderDetails
                },
                new WorkOrderCriteria()
                {
                    Criteria = "Amount",
                    CriteriaValue = "10",
                    WorkOrderDetail = workOrderDetails
                }
            };
            workOrderDetails.WorkOrderCriteriaList = workOrderCriteriaList;
            Object.WorkOrderDetails = new List<WorkOrderDetails>()
            {
                workOrderDetails
            };
            return this;
        }

        public DirectWorkOrderFactory WithWorkOrderDetails(IList<WorkOrderDetails> workOrderDetailses)
        {
            Object.WorkOrderDetails = workOrderDetailses;
            return this;
        }

        public DirectWorkOrderFactory WithWorkOrderDetails(Item item, Program program, Session session, WorkOrder workOrder, List<WorkOrderCriteria> workOrderCriterias)
        {
            var workOrderDetails = new WorkOrderDetails
            {
                Item = item,
                //Program = program,
                //Session = session,
                WorkOrder = workOrder,
                Purpose = (int)Purpose.AdministrativePurpose,
                OrderQuantity = 15,
                UnitCost = 10,
                TotalCost = 150,
                WorkOrderCriteriaList = workOrderCriterias
            };

            if (item.ItemType == (int)ItemType.ProgramItem)
            {
                workOrderDetails.Program = program;
                workOrderDetails.Session = session;
            }
            else
            {
                program = null;
                session = null;
            }

            if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose == null)
            {
                workOrderDetails.PurposeProgram = null;
                workOrderDetails.PurposeSession = null;
            }
            else if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose != null)
            {
                workOrderDetails.PurposeProgram = program;
                workOrderDetails.PurposeSession = session;
            }
            else if (item.ItemType == (int)ItemType.CommonItem && workOrderDetails.Purpose == null)
            {
                workOrderDetails.PurposeProgram = null;
                workOrderDetails.PurposeSession = null;
            }
            else if (item.ItemType == (int)ItemType.CommonItem && (workOrderDetails.Purpose > 0 && workOrderDetails.Purpose <= 3))
            {
                workOrderDetails.PurposeProgram = program;
                workOrderDetails.PurposeSession = session;
            }
            else
            {
                workOrderDetails.PurposeProgram = program;
                workOrderDetails.PurposeSession = session;
            }


            Object.WorkOrderDetails = new List<WorkOrderDetails>()
            {
                workOrderDetails
            };

            return this;
        }

        public DirectWorkOrderFactory CreateMore(int numOfWorkOrder)
        {
            CreateWorkOrderList(numOfWorkOrder);
            return this;
        }

        private DirectWorkOrderFactory CreateWorkOrderList(int numberOfWorkOrder)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<WorkOrder>();
            var program = _programFactory.Create().WithOrganization().Persist().Object;
            var session = _sessionFactory.Create().Persist().Object;
            var branch = _branchFactory.Create().WithOrganization(program.Organization).Persist().Object;
            var item = _itemFactory.Create(ItemType.ProgramItem).WithItemGroup().WithOrganization(program.Organization).Persist().Object;
            //var bank =
            //    _bankFactory.Create()
            //        .Persist().Object;
            //var bankBranch = _bankBranchFactory.Create().WithBank(bank).Persist();
            //var supplier = _supplierFactory.Create().Persist().Object;
            var bankDetails = _supplierBankDetailsFactory.Create().WithBankBranch();
            var supplier =
                _supplierFactory.Create().CreateWithSupplierBankDetails(bankDetails.SingleObjectList).Persist().Object;
            for (int i = 0; i < numberOfWorkOrder; i++)
            {
                var obj = new WorkOrder()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    //Name = "",
                    //Rank = 1,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = WorkOrder.EntityStatus.Active,
                    WorkOrderStatus = WorkOrderStatus.Issued,
                    DeliveryDate = DateTime.Now.AddDays(1),
                    OrderedQuantity = 15,
                    TotalCost = 150,
                    WorkOrderDate = DateTime.Now.AddDays(1),
                    WorkOrderNo = Guid.NewGuid().ToString().Substring(0, 5) + "123456",
                    Remarks = "this is a work order",
                    Branch = branch,
                    Supplier = supplier
                };
                var workOrderDetails = new WorkOrderDetails
                {
                    Item = item,
                    //Program = program,
                    //Session = session,
                    WorkOrder = obj,
                    Purpose = (int)Purpose.AdministrativePurpose,
                    OrderQuantity = 15,
                    UnitCost = 10,
                    TotalCost = 150
                };

                if (item.ItemType == (int)ItemType.ProgramItem)
                {
                    workOrderDetails.Program = program;
                    workOrderDetails.Session = session;
                }
                else
                {
                    program = null;
                    session = null;
                }

                if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose == null)
                {
                    workOrderDetails.PurposeProgram = null;
                    workOrderDetails.PurposeSession = null;
                }
                else if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose != null)
                {
                    workOrderDetails.PurposeProgram = program;
                    workOrderDetails.PurposeSession = session;
                }
                else if (item.ItemType == (int)ItemType.CommonItem && workOrderDetails.Purpose == null)
                {
                    workOrderDetails.PurposeProgram = null;
                    workOrderDetails.PurposeSession = null;
                }
                else if (item.ItemType == (int)ItemType.CommonItem && (workOrderDetails.Purpose > 0 && workOrderDetails.Purpose <= 3))
                {
                    workOrderDetails.PurposeProgram = program;
                    workOrderDetails.PurposeSession = session;
                }
                else
                {
                    workOrderDetails.PurposeProgram = program;
                    workOrderDetails.PurposeSession = session;
                }

                var workOrderCriteriaList = new List<WorkOrderCriteria>
                {
                    new WorkOrderCriteria()
                    {
                        Criteria = "Brand" + i,
                        CriteriaValue = "Bashundhara" + i,
                        WorkOrderDetail = workOrderDetails
                    },
                    new WorkOrderCriteria
                    {
                        Criteria = "Amount" + i,
                        CriteriaValue = "10" + i,
                        WorkOrderDetail = workOrderDetails
                    }
                };
                workOrderDetails.WorkOrderCriteriaList = workOrderCriteriaList;
                obj.WorkOrderDetails = new List<WorkOrderDetails>
                {
                    workOrderDetails
                };
                workOrderDetails.WorkOrderCriteriaList = workOrderCriteriaList;
                list.Add(obj);
            }
            ObjectList.AddRange(list);
            return this;
        }

        public DirectWorkOrderFactory CreateMore(int numberOfWorkOrder, Item item, Program program, Session session, List<WorkOrderCriteria> workOrderCriteriaList)
        {
            CreateWorkOrderList(numberOfWorkOrder, item, program, session, workOrderCriteriaList);
            return this;
        }

        private DirectWorkOrderFactory CreateWorkOrderList(int numberOfWorkOrder, Item item, Program program, Session session, List<WorkOrderCriteria> workOrderCriteriaList)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<WorkOrder>();
            var branch = _branchFactory.Create().Persist().Object;
            var supplier = _supplierFactory.Create().Persist().Object;
            for (int i = 0; i < numberOfWorkOrder; i++)
            {
                var obj = new WorkOrder()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    //Name = "",
                    //Rank = 1,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = WorkOrder.EntityStatus.Active,
                    WorkOrderStatus = WorkOrderStatus.Issued,
                    DeliveryDate = DateTime.Now.AddDays(1),
                    OrderedQuantity = 15,
                    TotalCost = 150,
                    WorkOrderDate = DateTime.Now.AddDays(1),
                    WorkOrderNo = Guid.NewGuid().ToString().Substring(0, 5) + "123456",
                    Remarks = "this is a work order",
                    Branch = branch,
                    Supplier = supplier
                };
                var workOrderDetails = new WorkOrderDetails()
                {
                    Item = item,
                    //Program = program,
                    //Session = session,
                    WorkOrder = Object,
                    Purpose = (int)Purpose.AdministrativePurpose,
                    OrderQuantity = 15,
                    UnitCost = 10,
                    TotalCost = 150
                };

                if (item.ItemType == (int)ItemType.ProgramItem)
                {
                    workOrderDetails.Program = program;
                    workOrderDetails.Session = session;
                }
                else
                {
                    program = null;
                    session = null;
                }
                if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose == null)
                {
                    workOrderDetails.PurposeProgram = null;
                    workOrderDetails.PurposeSession = null;
                }
                else if (item.ItemType == (int)ItemType.ProgramItem && workOrderDetails.Purpose != null)
                {
                    workOrderDetails.PurposeProgram = program;
                    workOrderDetails.PurposeSession = session;
                }
                else if (item.ItemType == (int)ItemType.CommonItem && workOrderDetails.Purpose == null)
                {
                    workOrderDetails.PurposeProgram = null;
                    workOrderDetails.PurposeSession = null;
                }
                else if (item.ItemType == (int)ItemType.CommonItem && (workOrderDetails.Purpose > 0 && workOrderDetails.Purpose <= 3))
                {
                    workOrderDetails.PurposeProgram = program;
                    workOrderDetails.PurposeSession = session;
                }
                else
                {
                    workOrderDetails.PurposeProgram = program;
                    workOrderDetails.PurposeSession = session;
                }
                workOrderCriteriaList = new List<WorkOrderCriteria>()
                {
                    new WorkOrderCriteria()
                    {
                        Criteria = "Brand" + i,
                        CriteriaValue = "Bashundhara" + i,
                        WorkOrderDetail = workOrderDetails
                    },
                    new WorkOrderCriteria()
                    {
                        Criteria = "Amount" + i,
                        CriteriaValue = "10" + i,
                        WorkOrderDetail = workOrderDetails
                    }
                };
                workOrderDetails.WorkOrderCriteriaList = workOrderCriteriaList;
                Object.WorkOrderDetails = new List<WorkOrderDetails>()
                {
                    workOrderDetails
                };
                workOrderDetails.WorkOrderCriteriaList = workOrderCriteriaList;
                Object.WorkOrderDetails = new List<WorkOrderDetails>()
                {
                    workOrderDetails
                };
                list.Add(obj);
            }
            ObjectList.AddRange(list);
            return this;
        }

        public DirectWorkOrderFactory WithWorkOrderDetails(Item item, Program program, Session session, WorkOrder workOrder)
        {
            _workOrderDetailsFactory.Create()
                .WithItem(item)
                .WithProgram(program)
                .WithPurposeOfProgramAndSession(program,session)
                .WithSession(session)
                .WithWorkOrder(workOrder)
                .WithWorkOrderCriteria(_workOrderDetailsFactory.Object);
            Object.WorkOrderDetails = _workOrderDetailsFactory.SingleObjectList;
            return this;
        }

        public DirectWorkOrderFactory Persist()
        {
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x => { if (x.Id == 0) _workOrderService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x => { if (x.Id == 0)_workOrderService.SaveOrUpdate(x); });
            }
            return this;
        }

        #endregion

        #region CleanUp

        public void CleanUp()
        {
            _workOrderDetailsFactory.CleanUp();
            _sessionFactory.Cleanup();
            //_branchFactory.CleanupWithoutOrganization();
            _programFactory.CleanupWithoutOrganization();
            _supplierBankDetailsFactory.CleanUp();
            _itemFactory.CleanUp();
            DeleteAll();
            _branchFactory.Cleanup();
            _supplierFactory.CleanUp();
        }

        #endregion


    }
}
