using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Services.Hr;
namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class ChildrenAllowanceSettingFactory : ObjectFactoryBase<ChildrenAllowanceSetting>
    {

        private readonly IChildrenAllowanceSettingService _prChildrenAllowanceService;

        public ChildrenAllowanceSettingFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _prChildrenAllowanceService = new ChildrenAllowanceSettingService(session);

        }

        #region Create

        public ChildrenAllowanceSettingFactory Create()
        {
            Object = new ChildrenAllowanceSetting
            {
                Amount = 100,
                Name = "Allowance" + Guid.NewGuid(),
                MaxNoOfChild = 10,
                StartingAge = 10,
                ClosingAge = 50,
                EffectiveDate = DateTime.Now,
                ClosingDate = DateTime.Now.AddDays(5),
                IsContractual = true,
                IsPartTime = true,
                IsPermanent = true,
                IsProbation = true,
                IsIntern = true,
                PaymentType = (int)PaymentType.Yearly
            };
            Object.MemberEmploymentStatuses = new List<MemberEmploymentStatus>() { MemberEmploymentStatus.Intern, MemberEmploymentStatus.Permanent, MemberEmploymentStatus.Contractual, MemberEmploymentStatus.PartTime, MemberEmploymentStatus.Probation, MemberEmploymentStatus.Retired };
            SingleObjectList.Add(Object);
            return this;
        }

        public ChildrenAllowanceSettingFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }
        public ChildrenAllowanceSettingFactory WithOrganization(BusinessModel.Entity.Administration.Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public ChildrenAllowanceSettingFactory WithStartAndClosingAge(int startAge, int closingAge)
        {
            Object.StartingAge = startAge;
            Object.ClosingAge = closingAge;
            return this;
        }

        public ChildrenAllowanceSettingFactory WithEffectiveAndCloxingDate(DateTime dateTime, DateTime dateTime1)
        {
            Object.EffectiveDate = dateTime;
            Object.ClosingDate = dateTime1;
            return this;
        }

        public ChildrenAllowanceSettingFactory Persist(List<UserMenu> menus)
        {
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x => { if (x.Id == 0) _prChildrenAllowanceService.SaveOrUpdate(menus, x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x => { if (x.Id == 0)_prChildrenAllowanceService.SaveOrUpdate(menus, x); });

            }
            return this;
        }

        public ChildrenAllowanceSettingFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new ChildrenAllowanceSetting() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public ChildrenAllowanceSettingFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion

    }
}
