﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using FluentNHibernate.Testing.Values;
using log4net;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.Controllers;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Exam.Controllers
{
    [UerpArea("Exam")]
    [Authorize]
    [AuthorizeAccess]
    public class UploadAnswerSheetController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("ExamArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly IBranchService _branchService;
        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly ICourseService _courseService;
        private readonly IExamsService _examsService;
        private readonly IExamsStudentAnswerScriptService _examsStudentAnswerScriptService;
        private readonly IUserService _userService;
        private readonly IOrganizationService _organizationService;
        private readonly CommonHelper _commonHelper;
        private List<UserMenu> _userMenu;
        const string DropDownOptionView = "Partial/_GenerateDropDown";
        const string DropDownOptionViewForExam = "Partial/_GenerateDropDownExam";
        #endregion

        public UploadAnswerSheetController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _courseService = new CourseService(session);
                _examsService = new ExamsService(session);
                _examsStudentAnswerScriptService = new ExamsStudentAnswerScriptService(session);
                _userService = new UserService(session);
                _organizationService = new OrganizationService(session);
                _commonHelper  = new CommonHelper();
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        #region Answer Sheet Upload
        public ActionResult UploadAnswer()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                //var programList = _programService.LoadAuthorizedProgram(_userMenu);
                //var programSelectList = new SelectList(programList, "Id", "Name");
                var setCode = new SelectList(_examsStudentAnswerScriptService.LoadSetCodes().ToList(), "Value", "Key");
                ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text"); 
                ViewBag.SetCode = setCode;
                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        #endregion

        #region Ajax Requests
        //[HttpPost]
        //public ActionResult GetProgram(string organizationId)
        //{
        //    try
        //    {
        //        organizationId = !String.IsNullOrEmpty(organizationId) ? organizationId.Trim() : "-1";
        //        //for authorization
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var programList = _programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)));
        //        return programList != null
        //            ? Json(new
        //            {
        //                ProgramOptions = 
        //                    this.RenderRazorViewToOptions(DropDownOptionView, programList, 0, "Select Program")
        //            })
        //            : Json(new Response(false, "Program not found"));
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = "Problem Occurred. " + ex.Message;
        //        _logger.Error(ex);
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}

        //[HttpPost]
        //public ActionResult GetSession(string programId)
        //{
        //    try
        //    {
        //        long pId = 0;
        //        pId = !String.IsNullOrEmpty(programId) ? Convert.ToInt64(programId.Trim()) : -1;
        //        //for authorization
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var authorizedSessions = _sessionService.LoadAuthorizedSession(_userMenu, null,_commonHelper.ConvertIdToList(pId));
        //        //var sessionList = _sessionService.LoadAuthorizedSession(_userMenu, Convert.ToInt64(programId));
        //        return authorizedSessions != null
        //            ? Json(new
        //            {
        //                SessionOptions =
        //                    this.RenderRazorViewToOptions(DropDownOptionView, authorizedSessions, 0, "Select Session")
        //            })
        //            : Json(new Response(false, "Session not found"));
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = "Problem Occurred. " + ex.Message;
        //        _logger.Error(ex);
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //}

        //[HttpPost]
        //public ActionResult GetCourse(string programId, string sessionId)
        //{
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(programId) && !String.IsNullOrEmpty(sessionId))
        //        {
        //            programId = programId.Trim();
        //            sessionId = sessionId.Trim();
        //        }
        //        else
        //        {
        //            programId = "-1";
        //            sessionId = "-1";
        //        }
        //        IList<Course> courses = _courseService.LoadCourse(Convert.ToInt64(programId),
        //                Convert.ToInt64(sessionId));
        //        return Json(new
        //        {
        //            CourseOptions = this.RenderRazorViewToOptions(DropDownOptionView, courses, 0, "Select Course")
        //        });

        //        //return Json(new Response(false, WebHelper.CommonErrorMessage));
        //    }
        //    catch (Exception e)
        //    {
        //        _logger.Error(e);
        //        return Json(new Response(false, WebHelper.CommonErrorMessage));
        //        //throw;
        //    }
        //}

        [HttpPost]
        public ActionResult GetExam(string query, string programId, string sessionId, string courseId, string isMcq = "")
        {
            try
            {
               bool? examTypeValue = (isMcq != "") ? Convert.ToBoolean(isMcq) : (bool?) null;

                if (!String.IsNullOrEmpty(query) && !String.IsNullOrEmpty(programId) && !String.IsNullOrEmpty(sessionId) &&
                    !String.IsNullOrEmpty(courseId))
                {
                    programId = programId.Trim();
                    sessionId = sessionId.Trim();
                    courseId = courseId.Trim();
                    query = query.Trim();
                }
                else
                {
                    programId = "-1";
                    sessionId = "-1";
                    courseId = "-1";
                    query = "-1";

                }
                //IList<Exams> exams = _examsService.GetExamByProgramSessionAndCourse(Convert.ToInt64(programId), Convert.ToInt64(sessionId), Convert.ToInt64(courseId));
                SelectList examsList = new SelectList(_examsService.GetExamByProgramSessionCourseAutoComplete(query,
                      Convert.ToInt64(programId), Convert.ToInt64(sessionId), Convert.ToInt64(courseId), examTypeValue), "Id", "Code");
                List<SelectListItem> selectExamList = examsList.ToList();
                return Json(new { returnList = selectExamList, IsSuccess = true });

                //return Json(new
                //{
                //    ExamsOptions = this.RenderRazorViewToOptions(DropDownOptionViewForExam, exams, 0, "Select Exam")
                //});

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json("");

            }
        }
        #endregion

        #region Operational Function
        public QuesAnswer ReadXmlFile(string file)
        {
            string setCode = null;
            string answerList = null;
            string examCode = null;
            string programName = null;
            var isExtractedData = false;
            using (XmlReader reader = XmlReader.Create(file))
            {
                while (reader.Read() && !isExtractedData)
                {
                    if (!reader.IsStartElement()) continue;
                    switch (reader.Name.ToString())
                    {
                        case "Answer":
                            var s = reader.GetAttribute("Set");
                            if (s != null)
                            {
                                setCode = s.Trim();
                                setCode = _examsStudentAnswerScriptService.GetAssociateSecCode(setCode);
                            }
                            var attribute = reader.GetAttribute("Ans");
                            if (attribute != null)
                            {
                                answerList = attribute.Trim();
                            }
                            isExtractedData = true;
                            break;

                        case "Student":
                            var attribute1 = reader.GetAttribute("ExamCode");
                            if (attribute1 != null)
                            {
                                examCode = attribute1.Trim();
                            }
                            var s1 = reader.GetAttribute("Program");
                            if (s1 != null)
                            {
                                programName = s1.Trim();
                            }
                            break;
                    }
                }
            }
            return new QuesAnswer
            {
                ExamCode = examCode,
                Answer = answerList,
                Set = setCode,
                //Program = programName
            };

        }
        [HttpPost]
        public ActionResult UploadFile()
        {
            try
            {
                var xmlFile = Request.Files[0];
                var formData = Request.Form;
                var programId = formData["Program"];
                var sessionId = formData["Session"];
                var courseId = formData["Course"];
                var examId = formData["Exam"];
                var setCode = formData["SetCode"];
                //var programId = formData["Program"];
                bool checkIsDuplicate =
                    _examsStudentAnswerScriptService.CheckIsDuplicateAnswer(Convert.ToInt64(programId.Trim()), Convert.ToInt64(sessionId.Trim()),
                    Convert.ToInt64(courseId.Trim()), Convert.ToInt64(examId.Trim()), Convert.ToInt32(setCode.Trim()));
                if (!checkIsDuplicate)
                {
                    return Json(new Response(true, "Continue"));
                }
                return Json(new Response(false, "DuplicateStop"));

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        [HttpPost]
        public ActionResult UploadFilePost()
        {
            try
            {
                var xmlFile = Request.Files[0];
                var formData = Request.Form;
                var programId = formData["Program"];
                var sessionId = formData["Session"];
                var courseId = formData["Course"];
                var examId = formData["Exam"];
                var setCode = formData["SetCode"];
                var isOverWrite = formData["overwrite"];
                var totalQues = 0;
                if (xmlFile == null) return View();
                var loginUser = _userService.LoadAspNetUserById(Convert.ToInt64(IdentityExtensions.GetUserId(System.Web.HttpContext.Current.User.Identity)));
                var actualPath = FileUpload.XmlFileValidationCheck(loginUser.FullName, xmlFile);
                if (actualPath != "")
                {
                    var data = ReadXmlFile(actualPath);
                    data.Program = _programService.GetProgram(Convert.ToInt64(programId.Trim()));
                    data.Session = _sessionService.LoadById(Convert.ToInt64(sessionId.Trim()));
                    data.Course = _courseService.GetCourse(Convert.ToInt64(courseId.Trim()));
                    var examObj = _examsService.LoadById(Convert.ToInt64(examId.Trim()));
                    data.ExamName = examObj.Name;
                    if (data.ExamCode.Trim() != examObj.Code.Trim())
                    {
                        return Json(new Response(false, "Wrong Answer Sheet Uploaded Exam Code different"));
                    }
                    bool isValidAnswerSheet = _examsStudentAnswerScriptService.IsValidAnswerSheet(data.Answer, examId, out totalQues);

                    if (isValidAnswerSheet)
                    {
                        ViewData["xmlsetCode"] = data.Set;
                        data.TotalQues = totalQues;
                        string answerSheet = this.RenderRazorViewToString("Partial/_AnswerSheet", data);
                        return Json(new Response(true, answerSheet));
                    }
                    return Json(new Response(false, "Wrong Answer Sheet Uploaded"));
                }
                return View();
            }
            catch (Exception e)
            {
                if (e.Message == "Can't upload mcq answer to a written exam")
                {
                    return Json(new Response(false, e.Message));
                }
                return Json(new Response(false, "Something occured wrong"));
            }

        }
        [HttpPost]
        public ActionResult GetEmptyExamSheetForm(string program, string session, string course, string examCode, string setCode,long examId)
        {
            try
            {
                //long examId=0;
                if (!String.IsNullOrEmpty(program) && !String.IsNullOrEmpty(session) &&
                    !String.IsNullOrEmpty(course) && !String.IsNullOrEmpty(examCode) && !String.IsNullOrEmpty(setCode))
                {
                    program = program.Trim();
                    session = session.Trim();
                    course = course.Trim();
                   // examId = Convert.ToInt64(examCode);
                }
                else
                {
                    program = "-1";
                    session = "-1";
                    course = "-1";
                    examCode = "-1";
                    setCode = "-1";
                }
                Exams examObj = _examsService.GetExams(examCode,Convert.ToInt64(course));
                if (examObj.IsTakeAnswerSheetWhenUploadMarks)
                {
                    return Json(new Response(false, "When you will upload marks This kind of exam's answer sheet will be added instantly.you can't upload answer sheet from here!!"));
                }
                //IList<ExamsStudentAnswerScript> examsStudentAnswerScripts = _examsStudentAnswerScriptService.
                //    GetExamStudentAnsScriptByExamId(Convert.ToInt64(examCode), Convert.ToInt32(setCode));
                IList<ExamsStudentAnswerScript> examsStudentAnswerScripts = _examsStudentAnswerScriptService.
                    GetExamStudentAnsScriptByExamId(examObj.Id, Convert.ToInt32(setCode));
                QuesAnswer quesAnswer = ReformObject(examsStudentAnswerScripts);
                quesAnswer.TotalQues = examsStudentAnswerScripts.Count;
                if (quesAnswer.TotalQues == 0)
                {
                    var totalQues = 0;
                    
                    Exams exams = _examsService.GetExamById(examId);
                    bool isValidAnswerSheet = _examsStudentAnswerScriptService.IsValidAnswerSheet("", examId.ToString(), out totalQues);
                    var programName = _programService.GetProgram(Convert.ToInt64(program));
                    var sessionName = _sessionService.LoadById(Convert.ToInt64(session));
                    var courseName = _courseService.GetCourse(Convert.ToInt64(course));
                    quesAnswer.Answer = "";
                    quesAnswer.TotalQues = totalQues;
                    quesAnswer.Program = programName;
                    quesAnswer.Session = sessionName;
                    quesAnswer.ExamCode = exams.Code;
                    quesAnswer.Course = courseName;
                    quesAnswer.SetCode = setCode;
                    quesAnswer.Set = setCode;
                    ViewData["xmlsetCode"] = quesAnswer.Set;

                    string answerSheet = this.RenderRazorViewToString("Partial/_EmptyAnswerSheet", quesAnswer);
                    return Json(new Response(true, answerSheet));
                }

                if (!String.IsNullOrEmpty(quesAnswer.Set))
                {
                    ViewData["xmlsetCode"] = quesAnswer.Set;
                    ViewData["overwrite"] = "overwrite";
                    string answerSheet = this.RenderRazorViewToString("Partial/_AnswerSheet", quesAnswer);
                    return Json(new Response(true, answerSheet));
                }
                return Json(new Response(false, "No Answer Sheet Found"));
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Something went wrong ! Try again later"));
            }
        }

        [HttpPost]
        public ActionResult GetExamAnswerSheet(string program, string session, string course, string examId, string setCode)
        {
            try
            {
                
                if (!String.IsNullOrEmpty(program) && !String.IsNullOrEmpty(session) &&
                    !String.IsNullOrEmpty(course) && !String.IsNullOrEmpty(examId) && !String.IsNullOrEmpty(setCode))
                {
                    program = program.Trim();
                    session = session.Trim();
                    course = course.Trim();
                }
                else
                {
                    program = "-1";
                    session = "-1";
                    course = "-1";
                    examId = "-1";
                    setCode = "-1";
                }
                Exams examObj = _examsService.GetExamById(Convert.ToInt64(examId)); //here examcode is not examcode.. it is exam id 
                //IList<ExamsStudentAnswerScript> examsStudentAnswerScripts = _examsStudentAnswerScriptService.
                //    GetExamStudentAnsScriptByExamId(Convert.ToInt64(examCode), Convert.ToInt32(setCode));
                IList<ExamsStudentAnswerScript> examsStudentAnswerScripts = _examsStudentAnswerScriptService.
                    GetExamStudentAnsScriptByExamId(examObj.Id, Convert.ToInt32(setCode));
                QuesAnswer quesAnswer = ReformObject(examsStudentAnswerScripts);
                quesAnswer.TotalQues = examsStudentAnswerScripts.Count;
                //bool isValidAnswerSheet = _examsStudentAnswerScriptService.IsValidAnswerSheet(data.Answer, examId, out totalQues);

                if (!String.IsNullOrEmpty(quesAnswer.Set))
                {
                    ViewData["xmlsetCode"] = quesAnswer.Set;
                    string answerSheet = this.RenderRazorViewToString("Partial/_AnswerSheet", quesAnswer);
                    return Json(new Response(true, answerSheet));
                }
                return Json(new Response(false, "No Answer Sheet Found"));
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Something went wrong ! Try again later"));
                //throw;
            }
            //return View();
        }

        private QuesAnswer ReformObject(IList<ExamsStudentAnswerScript> examsStudentAnswerScripts)
        {
            try
            {
                if (examsStudentAnswerScripts.Any())
                {
                    var answer = "";
                    int kk = 0;
                    string examCode = "";
                    string set = "";
                    string examName = "";
                    Program program = null;
                    Session session = null;
                    Course course = null;
                    foreach (var examstdAnsScript in examsStudentAnswerScripts)
                    {
                        if (kk == 0)
                        {
                            examCode = examstdAnsScript.ExamsDetails.Exams.Code;
                            set = examstdAnsScript.SetCode.ToString();
                            program = examstdAnsScript.ExamsDetails.Exams.Program;
                            session = examstdAnsScript.ExamsDetails.Exams.Session;
                            course = examstdAnsScript.ExamsDetails.Exams.Course;
                            examName = examstdAnsScript.ExamsDetails.Exams.Name;
                            kk = 1;
                        }
                        answer += examstdAnsScript.QuestionAnswer + ",";
                    }
                    answer = answer.Trim().Remove(answer.Length - 1, 1);
                    return new QuesAnswer
                    {
                        ExamCode = examCode,
                        Answer = answer,
                        Set = set,
                        Program = program,
                        Session = session,
                        Course = course,
                        ExamName = examName
                    };
                }
                return new QuesAnswer();

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return new QuesAnswer();
                //throw;
            }
        }
        #endregion

        #region Save
        [HttpPost]
        public JsonResult SaveAnswerManually(string answers, string setCode, string examId, string overwrite = "")
        {
            try
            {
                if (String.IsNullOrEmpty(answers) || String.IsNullOrEmpty(setCode) || String.IsNullOrEmpty(examId)
                    )
                    return
                        Json(new Response(false,
                            "Please Check Your SetCode ExamCode and AnsSheet Again!! Something Wrong "));
                bool isSaveSuccess = false;
                if (String.IsNullOrEmpty(overwrite))
                {
                    isSaveSuccess = _examsStudentAnswerScriptService.IsAnswerSaveSuccessfully(answers, setCode,
                   Convert.ToInt64(examId.Trim()));
                    if (isSaveSuccess)
                    {
                        return Json(new Response(true, "Answer Sheet Uploaded Successfully"));
                    }
                }
                else
                {
                    isSaveSuccess = _examsStudentAnswerScriptService.IsAnswerUpdateSuccessfully(answers, setCode,
                   Convert.ToInt64(examId.Trim()));
                    if (isSaveSuccess)
                    {
                        return Json(new Response(true, "Answer Sheet Updated Successfully"));
                    }
                }
                return Json(new Response(false, "Database Operation Failed "));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Something Went Wrong "));
            }
        }

        [HttpPost]
        public JsonResult SaveAnswer(string answers, string setCode, string examId, string xmlSetcode, string overwrite = "")
        {
            try
            {
                if (String.IsNullOrEmpty(answers) || String.IsNullOrEmpty(setCode) || String.IsNullOrEmpty(examId) ||
                    String.IsNullOrEmpty(xmlSetcode))
                    return
                        Json(new Response(false,
                            "Please Check Your SetCode ExamCode and AnsSheet Again!! Something Wrong "));
                if (!setCode.Trim().Equals(xmlSetcode.Trim()))
                    return Json(new Response(false, "Wrong Answer Sheet Uploaded. Setcode Different"));
                if (overwrite.Equals("true"))
                {
                    var isSaveSuccess = _examsStudentAnswerScriptService.IsAnswerUpdateSuccessfully(answers, setCode,
                        Convert.ToInt64(examId.Trim()));
                    if (isSaveSuccess)
                    {
                        return Json(new Response(true, "Answer Sheet Updated Successfully"));
                    }
                    return Json(new Response(false, "Database Operation Failed "));
                }
                else
                {
                    var isSaveSuccess = _examsStudentAnswerScriptService.IsAnswerSaveSuccessfully(answers, setCode,
                        Convert.ToInt64(examId.Trim()));
                    if (isSaveSuccess)
                    {
                        return Json(new Response(true, "Answer Sheet Uploaded Successfully"));
                    }
                    return Json(new Response(false, "Database Operation Failed"));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Something Went Wrong "));
            }
        }
        #endregion
        //string program, string session, string course, string examCode, string setCode, HttpPostedFileBase xmlFiles

        #region Edit
        [HttpGet]
        public ActionResult ManageAnswerSheet()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                //var programList = _programService.LoadAuthorizedProgram(_userMenu);
                //var programSelectList = new SelectList(programList, "Id", "Name");
                var setCode = new SelectList(_examsStudentAnswerScriptService.LoadSetCodes().ToList(), "Value", "Key");
                ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text"); 
                ViewBag.SetCode = setCode;
                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
        #endregion

        #region Delete
        public JsonResult DeleteAnswer(string answers, string setCode, string examId, string xmlSetcode)
        {
            try
            {
                if (!String.IsNullOrEmpty(answers) && !String.IsNullOrEmpty(setCode) && !String.IsNullOrEmpty(examId) &&
                    !String.IsNullOrEmpty(xmlSetcode))
                {
                    if (setCode.Trim().Equals(xmlSetcode.Trim()))
                    {
                        try
                        {
                            var isDeleteSuccess = _examsStudentAnswerScriptService.IsAnswerDeleteSuccessfully(setCode,
                            Convert.ToInt64(examId.Trim()));
                            if (isDeleteSuccess)
                            {
                                return Json(new Response(true, "Answer Sheet Deleted Successfully"));
                            }
                            return Json(new Response(false, "Database Operation Failed "));
                        }
                        catch (Exception e)
                        {

                            return Json(new Response(false, e.Message));
                        }

                    }
                }
                return Json(new Response(false, "Invalid value Found"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Something went wrong ! Try again later"));
            }

        }
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
        }
        #endregion

        // GET: Exam/UploadAnswerSheet
        public ActionResult Index()
        {
            return View();
        }
    }
}
