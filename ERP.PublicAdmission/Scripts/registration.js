$(document).ready(function () {

    $("#transactionIds").find(".tridclass").each(function() {
        if ($(this).val() == "") {
            $(this).parents(".form-group").remove();
        }
        if ($("#transactionIds").find(".form-group").length <= 0) {
            var newTrId = '<div class="form-group">';
            newTrId += '<label for="TransactionId" class="control-label col-md-4">Transaction Id</label>';
            newTrId += '<div class="col-md-8">';
            newTrId += '<input type="text" name="transactionId[]" placeholder="Enter Transaction ID" class="form-control tridclass" style="float: left; margin-right: 5px; " maxlength="10"  size="10" />';
            newTrId += '<button type="button" class="btn btn-default plus">';
            newTrId += '<span class="glyphicon glyphicon-plus-sign"></span>';
            newTrId += '</button>';
            newTrId += '<div class="clearfix"></div>';
            newTrId += '</div>';
            newTrId += '</div>';

            $("#transactionIds").find(".plus").removeClass("plus").addClass("minus");
            $("#transactionIds").find("span").removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
            $("#transactionIds").append(newTrId);
        }       
    });

    $(document).on("click", ".plus", function () {
        var newTrId = '<div class="form-group">';
        newTrId += '<label for="TransactionId" class="control-label col-md-4">Transaction Id</label>';
        newTrId += '<div class="col-md-8">';
        newTrId += '<input type="text" name="transactionId[]" placeholder="Enter Transaction ID" class="form-control tridclass" style="float: left; margin-right: 5px; " maxlength="10"  size="10" />';
        newTrId += '<button type="button" class="btn btn-default plus">';
        newTrId += '<span class="glyphicon glyphicon-plus-sign"></span>';
        newTrId += '</button>';
        newTrId += '<div class="clearfix"></div>';
        newTrId += '</div>';
        newTrId += '</div>';

        $("#transactionIds").find(".plus").removeClass("plus").addClass("minus");
        $("#transactionIds").find("span").removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
        $("#transactionIds").append(newTrId);
    });
    $(document).on("click", ".minus", function () {
        $(this).parents(".form-group").remove();        
    });

    $("#transactionIds .plus").addClass("minus").removeClass("plus").find(".glyphicon-plus-sign").addClass("glyphicon-minus-sign").removeClass("glyphicon-plus-sign");
    $("#transactionIds .minus:last").addClass("plus").removeClass("minus").find(".glyphicon-minus-sign").addClass("glyphicon-plus-sign").removeClass("glyphicon-minus-sign");
    
});

$(document).on("input propertychange", ".tridclass ", function (event) {
    var input = $(this),
    text = input.val().replace(/[^0-9.]/g, "");
    input.val(text);
});