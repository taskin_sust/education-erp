﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MenuTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Menu")]
    public class MenuUpdateTest : MenuBaseTest
    {
        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var menuFactory =
               _menuFactory.Create().WithNameAndRank(_name, 1).WithMenuGroup().WithAction()
                   .Persist();
            Assert.Throws<NullObjectException>(() => _menuService.Update(menu:null));
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var menuFactory =
                _menuFactory.Create().WithNameAndRank(_name, 1).WithMenuGroup().WithAction()
                    .Persist();
            Menu menu = _menuService.MenuLoadById(menuFactory.Object.Id);
            menu.Name = menu.Name + " DEF GHI";
            var success = _menuService.Update(menu);
            Assert.True(success);
        }
        
        #endregion

        #region Business Logic Test
        
        #endregion
    }
}
