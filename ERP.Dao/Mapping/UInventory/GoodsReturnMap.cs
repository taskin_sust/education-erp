using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{


    public class GoodsReturnMap : BaseClassMap<GoodsReturn, long>
    {
        public GoodsReturnMap()
        {
            Table("UINV_GoodsReturn");
            LazyLoad();
            References(x => x.GoodsReceive).Column("GoodsReceivedId");
            Map(x => x.ReturnedQuantity).Column("ReturnedQuantity");
            Map(x => x.Remarks).Column("Remarks");
            Map(x => x.ReturnStatus).Column("ReturnStatus").Not.Nullable();
            Map(x => x.GoodsReturnNo).Column("GoodsReturnNo").Not.Nullable();
            HasMany(x => x.GoodsReturnDetails).KeyColumn("GoodsReturnId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
