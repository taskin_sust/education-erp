using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{
    
    
    public partial class DailySupportAllowanceSettingMap : BaseClassMap<DailySupportAllowanceSetting,long> {

        public DailySupportAllowanceSettingMap()
        {
			Table("HR_DailySupportAllowanceSetting");
			LazyLoad();
			References(x => x.Organization).Column("OrganizationId");
			References(x => x.Designation).Column("DesignationId");
			Map(x => x.Amount).Column("Amount").Not.Nullable();
			Map(x => x.EffectiveDate).Column("EffectiveDate").Not.Nullable();
            Map(x => x.ClosingDate).Column("ClosingDate").Nullable();
        }
    }
}
