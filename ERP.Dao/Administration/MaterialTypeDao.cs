﻿using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IMaterialTypeDao : IBaseDao<MaterialType, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<MaterialType> LoadMaterialType(long? organizationId = null);
        IList<MaterialType> LoadMaterialType(long sessionId, long programId);
        IList<MaterialType> LoadMaterialType(int start, int length, string orderBy, string orderDir, List<long> organizationList, string name, string shortName, string rank, string status);
        #endregion

        #region Others Function
        bool HasDuplicateByName(string name, long? organizationId = null, long? id = null);
        bool HasDuplicateByShortName(string shortName, long? organizationId = null, long? id = null);
        int GetMaterialTypeCount(string orderBy, string orderDir, List<long> organizationList , string name, string shortName, string rank, string status);
        //implement the method
        bool CheckDuplicateMaterialType(out string fieldName, string name = null, string code = null, long id = 0);
        #endregion

    }
    public class MaterialTypeDao : BaseDao<MaterialType, long>, IMaterialTypeDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<MaterialType> LoadMaterialType(long? organizationId = null)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(MaterialType));
            criteria.Add(Restrictions.Eq("Status", Material.EntityStatus.Active));
            if (organizationId != null)
            {
                criteria.CreateAlias("Organization", "organization").Add(Restrictions.Eq("organization.Status", Organization.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("organization.Id", organizationId));
            }
            return criteria.List<MaterialType>();
        }
        public IList<MaterialType> LoadMaterialType(long sessionId, long programId)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(MaterialType));
            criteria.Add(Restrictions.Eq("Status", MaterialType.EntityStatus.Active));
            criteria.CreateCriteria("Materials", "m", JoinType.InnerJoin)
                .Add(Restrictions.Eq("m.Status", Material.EntityStatus.Active)).Add(Restrictions.Eq("m.Program.Id", programId))
                .Add(Restrictions.Eq("m.Session.Id", sessionId)).SetProjection(Projections.Distinct(Projections.Property("m.MaterialType")));

            var materialTypeList = criteria.List<MaterialType>();
            return materialTypeList;
        }
        public IList<MaterialType> LoadMaterialType(int start, int length, string orderBy, string orderDir, List<long> organizationList , string name, string shortName, string rank, string status)
        {
            ICriteria criteria = GetMaterialTypeCriteria(orderBy, orderDir, organizationList, name, shortName, rank, status);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<MaterialType>();
        }
        #endregion

        #region Others Function
        public bool CheckDuplicateMaterialType(out string fieldName, string name = null, string code = null, long id = 0)
        {
            fieldName = "";

            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<MaterialType>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", MaterialType.EntityStatus.Delete)));
            if (id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (name != null)
            {
                criteria.Add(Restrictions.Eq("Name", name));
                fieldName = "Name";
            }
            else if (code != null)
            {
                criteria.Add(Restrictions.Eq("ShortName", code));
                fieldName = "ShortName";
            }
            var rowList = criteria.List<MaterialType>();
            if (rowList != null && rowList.Count > 0)
            {
                checkValue = true;
            }
            return checkValue;
        }
        public bool HasDuplicateByShortName(string shortName, long? organizationId = null, long? id = null)
        {
            var criteria = Session.CreateCriteria<MaterialType>().Add(Restrictions.Not(Restrictions.Eq("Status", MaterialType.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("ShortName", shortName));
            if (organizationId != null)
            {
                criteria.Add(Restrictions.Eq("Organization.Id", organizationId));
            }
            if (id != null)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            criteria.SetProjection(Projections.RowCount());
            var result = Convert.ToInt32(criteria.UniqueResult());
            if (result > 0)
                return true;
            return false;
        }
        public bool HasDuplicateByName(string name, long? organizationId = null, long? id = null)
        {
            var criteria = Session.CreateCriteria<MaterialType>().Add(Restrictions.Not(Restrictions.Eq("Status", MaterialType.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("Name", name));
            if (organizationId != null)
            {
                criteria.Add(Restrictions.Eq("Organization.Id", organizationId));
            }
            if (id != null)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            criteria.SetProjection(Projections.RowCount());
            var result = Convert.ToInt32(criteria.UniqueResult());
            if (result > 0)
                return true;
            return false;
        }
        public int GetMaterialTypeCount(string orderBy, string orderDir, List<long> organizationList , string name, string shortName, string rank, string status)
        {
            ICriteria criteria = GetMaterialTypeCriteria(orderBy, orderDir, organizationList, name, shortName, rank, status, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }
        #endregion

        #region Helper Function
        private ICriteria GetMaterialTypeCriteria(string orderBy, string orderDir, List<long> organizationList, string name, string shortName, string rank, string status, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<MaterialType>().Add(Restrictions.Not(Restrictions.Eq("Status", MaterialType.EntityStatus.Delete)));
            criteria.CreateAlias("Organization", "organization").Add(Restrictions.Eq("organization.Status", Organization.EntityStatus.Active));

            //if (!String.IsNullOrEmpty(organization))
            //{
            //    criteria.Add(Restrictions.Eq("Organization.Id", Convert.ToInt64(organization)));
            //}
            if (organizationList != null)
            {
                criteria.Add(Restrictions.In("Organization.Id", organizationList));
            }
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(shortName))
            {
                criteria.Add(Restrictions.Like("ShortName", shortName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy) : Order.Desc(orderBy));
            }

            return criteria;
        }

        #endregion
    }
}