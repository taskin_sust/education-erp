﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.MessageExceptions
{
    public class EmptyFieldException:Exception
    {

        public EmptyFieldException():base("Empty field found")
        {

        }

        public EmptyFieldException(string message)
            : base(message)
        {

        }
    }
}
