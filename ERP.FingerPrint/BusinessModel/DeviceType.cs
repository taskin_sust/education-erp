﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.FingerPrint.BusinessModel
{
    public class DeviceType
    {
        public string ModelName { get; set; }
        public long ModelId { get; set; }
        public string Index0 { get; set; }
        public string Index1 { get; set; }
        public string Index2 { get; set; }
        public string Index3 { get; set; }
        public string Index4 { get; set; }
        public string Index5 { get; set; }
        public string Index6 { get; set; }
        public string Index7 { get; set; }
        public string Index8 { get; set; }
        public string Index9 { get; set; }
    }
}
