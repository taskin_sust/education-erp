function autoCompleteFunc() {     
    $('.autoComplete').each(function () {
        var $this = $(this);       
            $this.typeahead({
                source: function (query, process) {
                    var programId = $("#Program").val();
                    var sessionId = $("#Session").val();
                    var courseId = $("#Course").val();
                    var data = { query: query, programId: programId, sessionId: sessionId, courseId: courseId }
                    $.ajax({
                        type: "POST",
                        url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForExamCoad",
                        cache: false,
                        async: true,
                        data: data,

                        success: function(result) {
                            if (result.IsSuccess) {
                                process(result.returnList);
                            }
                        },
                        complete: function() {
                        },
                        error: function(result) {
                        }
                    });
                },
                updater: function(item) {
                    $("#ExamId").val(item.Value);
                    $("#ExamType option").remove();
                    $("#ExamCode").val("");
                    $("#ExamName").val("");
                    $.ajax({
                        type: "post",
                        url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForExamInformation",
                        cache: false,
                        async: true,
                        data: { "id": item.Value },
                        beforeSend: function() {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function(result) {
                            $.unblockUI();
                            if (result.IsSuccess) {
                                
                                
                                console.log(result.returnList.Name);
                                if (result.returnList.hasOwnProperty("Name")) {
                                    $("#ExamName").val(result.returnList.Name);
                                    showErrorMessageBelowCtrl("ExamName", "", false);
                                   // $("#ExamCode").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").text("");
                                }
                                    
                                if (result.returnList.IsMcq == "True" && result.returnList.IsWritten == "True") {
                                    showErrorMessageBelowCtrl("ExamType", "", false);
                                  //  $('#ExamType').parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").text("");
                                    $('#ExamType').append("<option value='" + result.returnList.IsMcqValue + "'>MCQ</option>");
                                    $('#ExamType').append("<option value='" + result.returnList.IsWrittenValue + "'>Written</option>");
                                }
                                else if (result.returnList.IsMcq == "True" && result.returnList.IsWritten == "False") {
                                    showErrorMessageBelowCtrl("ExamType", "", false);
                                  //  $('#ExamType').parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").text("");
                                    $('#ExamType').append("<option value='" + result.returnList.IsMcqValue + "'>MCQ</option>");
                                }
                                else if (result.returnList.IsMcq == "False" && result.returnList.IsWritten == "True") {
                                    showErrorMessageBelowCtrl("ExamType", "", false);
                                    //$('#ExamType').parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").text("");
                                    $('#ExamType').append("<option value='" + result.returnList.IsWrittenValue + "'>Written</option>");
                                }
                                else {
                                    $('#ExamType').append("<option value=''>--Select Exam Types--</option>");
                                    showErrorMessageBelowCtrl("ExamType", "", false);
                                    //$("#ExamType").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").text("The Exam Type Field is required.");
                                }
                            }
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        error: function(result) {
                            $.unblockUI();
                            console.log("Failed -----");
                        }
                    });
                    return item.Text;
                },
                matcher: function(item) {
                    if (item.Text.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                        return true;
                    }
                    return false;
                },
                sorter: function(items) {
                    return items.sort();
                },
                highlighter: function(item) {
                    var regex = new RegExp('(' + this.query + ')', 'gi');
                    return item.Text.replace(regex, "<strong>$1</strong>");
                },
                onselect: function(obj) {
                    alert('Selected ' + obj);
                }
            });                
    });
}

$(document).on("input propertychange", ".autoComplete", function (event) {
    $("#manualMarksEntry").empty();
    $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria and give a prnNo.</th></tr></thead>"); 
    autoCompleteFunc();
    if ($(this).val() == "") {
        showErrorMessageBelowCtrl("ExamCode", "", false);
      //  $("#ExamCode").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").text("The Exam Code field is required.");
    }
});

function responseReturn() {
    $("#manualMarksEntry").empty();
    $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria and give a prnNo.</th></tr></thead>");
    var check = true;

    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var course = $("#Course").val();
    var examCode = $("#ExamCode").val();
    var examType = $("#ExamType").val();
    var prnNo = $("#PrnNo").val();
    if (organizationId == "" || organizationId == null) {
        showErrorMessageBelowCtrl("Organization", "", false);
        showErrorMessageBelowCtrl("Organization", "The Organization field is required.", true);
        check = false;
    }

    if (programId == "" || programId == null) {
        showErrorMessageBelowCtrl("Program", "", false);
        showErrorMessageBelowCtrl("Program", "The Program field is required.", true);
        check = false;
    }

    if (sessionId == "" || sessionId == null) {
        showErrorMessageBelowCtrl("Session", "", false);
        showErrorMessageBelowCtrl("Session", "The Session field is required.", true);
        check = false;
    }
    if (course == "" || course == null) {
        showErrorMessageBelowCtrl("Course", "", false);
        showErrorMessageBelowCtrl("Course", "The Course field is required.", true);
        check = false;
    }
    if (examCode == "" || examCode == null) {
        showErrorMessageBelowCtrl("ExamCode", "", false);
        showErrorMessageBelowCtrl("ExamCode", "The Exam Code field is required.", true);
        check = false;
    }
    if (examType == "" || examType == null) {
        showErrorMessageBelowCtrl("ExamType", "", false);
        showErrorMessageBelowCtrl("ExamType", "The Exam Type field is required.", true);
        check = false;
    }
    if (prnNo == "") {
        showErrorMessageBelowCtrl("PrnNo", "", false);
        showErrorMessageBelowCtrl("PrnNo", "The Program Roll field is required.", true);
        check = false;
    }   
    
    if (check) {        
        var examId = $("#ExamId").val();
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForDrawingManageEntryTable",
            cache: false,
            async: true,
            data: { "examId": examId, "prnNo": prnNo, "examType": examType, "programId": programId, "sessionId": sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" />Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (!result.hasOwnProperty("IsSuccess")) {
                    $("#manualMarksEntry").html(result);
                    $('#marksPanel').keydown(function (e) {
                        if (e.keyCode == 13) {
                            return false;
                        }
                    });
                    $('[data-toggle="tooltip"]').tooltip();                  
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
}


$(document).ready(function () {
    //$('[data-toggle="tooltip"]').tooltip();
    //$('[data-toggle="popover"]').popover();

    //$("#manualMarksEntryForm").submit(function () {
    //    return false;
    //});


    $('body').keydown(function (e) {
        if (e.keyCode == 13) {
            responseReturn();
        }
    });

    $("#displayMarksPrnNo").click(function () {
        responseReturn();
    });
    $("#Organization").change(function () {
        $("#manualMarksEntry").empty();
        $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria and give a prnNo.</th></tr></thead>");
        $("#Program option").remove();
        $('#Program').append("<option value=''>--Select Program--</option>");
        $("#Session option").remove();
        $('#Session').append("<option value=''>--Select Session--</option>");
        $("#Course option").remove();
        $('#Course').append("<option value=''>--Select Course--</option>");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");
        if ($(this).val() != "") {
            showErrorMessageBelowCtrl("Organization", "", false);
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForGetProgram",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",

                cache: false,
                async: true,
                data: { "organizationIds": $(this).val(), isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        //$.each(result.returnList, function (i, v) {
                        //    $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        $.each(response.returnProgramList, function (i, v) {
                            $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function () {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
    });
    $("#Program").change(function () {
        $("#manualMarksEntry").empty();
        $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria and give a prnNo.</th></tr></thead>");        
        
        $("#Session option").remove();
        $('#Session').append("<option value=''>--Select Session--</option>");

        $("#Course option").remove();
        $('#Course').append("<option value=''>--Select Course--</option>");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");
        var organizationId = $("#Organization").val();
        var programId = $("#Program").val();
        if (organizationId != "" && programId != "") {
            showErrorMessageBelowCtrl("Program", "", false);
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForGetSesstion",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                cache: false,
                async: true,
                //data: { "pId": $(this).val() },
                data: { organizationIds: organizationId, programIds: programId, isAuthorized: true },

                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        //$.each(result.returnList, function (i, v) {
                        //    $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        $.each(response.returnSessionList, function (i, v) {
                            $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function () {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }   
    });

    $("#Session").change(function () {
        $("#manualMarksEntry").empty();
        $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria and give a prnNo.</th></tr></thead>");
        $("#Course option").remove();
        $('#Course').append("<option value=''>--Select Course--</option>");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");

        var organizationId = $("#Organization").val();
        var programId = $("#Program").val();
        var sessionId = $("#Session").val();

        if (organizationId != "" && organizationId != null && programId != "" && programId != null && sessionId != "" && sessionId != null) {
            showErrorMessageBelowCtrl("Session", "", false);
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForGetCourse",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
                cache: false,
                async: true,
                //data: { "programId": $("#Program").val(), "sessionId": $("#Session").val() },
                data: { organizationIds: organizationId, programIds: programId, sessionIds: sessionId },

                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {

                    $.unblockUI();
                    if (response.IsSuccess) {
                        //$.each(result.returnList, function(i, v) {
                        //    $('#Course').append($('<option>').text(v.Text).attr('value', v.Value));
                        //});
                        $.each(response.returnCourse, function (i, v) {
                            $('#Course').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                },
                complete: function() {
                    $.unblockUI();
                },
                error: function() {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }             
    });
    $("#Course").change(function () {
        $("#manualMarksEntry").empty();
        $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria and give a prnNo.</th></tr></thead>");
        $("#ExamId").val("");
        $("#ExamCode").val("");
        $("#ExamName").val("");
        $("#ExamType option").remove();
        $('#ExamType').append("<option value=''>--Select Exam Type--</option>");

        if ($(this).val() != "") {
            showErrorMessageBelowCtrl("Course", "", false);
        }
    });    
});

$(document).on("input propertychange", "#PrnNo", function (event) {
    $("#manualMarksEntry").empty();
    $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria.</th></tr></thead>");
    if ($(this).val() != "") {
        showErrorMessageBelowCtrl("PrnNo", "", false);
    }
});


function checkRowValidity(placeHolder) {
    var maxSubject = placeHolder.attr("data-max-subject");
    var totalCompulsary = 0;
    var tatalCompulsaryFillup = 0;
    var totalField = 0;
    var totalFillup = 0;
    var isError = 0;    
    
    placeHolder.find(".marks").each(function () {
        var currentField = $(this);
        var currentVal = currentField.val();      

        totalField = totalField + 1;
        var isCompusary = currentField.attr("data-is-compulsary");
        var maxMarks = currentField.attr("data-max-marks");
        currentField.removeClass("highlight");
       

        if (isCompusary == "True") {          
            totalCompulsary = totalCompulsary + 1;

            

            if (currentVal != "" && /^(?=.*\d)\d*(?:\.\d+)?$/.test(currentVal) && parseFloat(currentVal) <= parseFloat(maxMarks)) {
                //console.log(currentVal != "");
                //console.log(/^(?=.*\d)\d*(?:\.\d+)?$/.test(currentVal));
                //console.log(parseFloat(currentVal) <= parseFloat(maxMarks));
                tatalCompulsaryFillup = tatalCompulsaryFillup + 1;
                totalFillup = totalFillup + 1;
            }
            else {
                currentField.addClass("highlight");
            }
        }
        else {
            if (currentVal != "" && /^(?=.*\d)\d*(?:\.\d+)?$/.test(currentVal) && parseFloat(currentVal) <= parseFloat(maxMarks)) {
                totalFillup = totalFillup + 1;
            }
            else {
                
                if (currentVal != "") {
                    if (parseFloat(currentVal) > parseFloat(maxMarks)) {
                        currentField.addClass("highlight");
                        console.log(currentVal + " " + maxMarks + " " + (parseFloat(currentVal) <= parseFloat(maxMarks)));
                        isError = isError + 1;
                    }
                }
            }
        }
        if (totalFillup > parseInt(maxSubject)) {
            currentField.addClass("highlight");
            //isError = isError + 1;
        }
    });

    console.log(totalFillup + " " + maxSubject + " " + totalCompulsary + " " + tatalCompulsaryFillup + " " + isError);
    if (totalFillup <= parseInt(maxSubject) && totalCompulsary == tatalCompulsaryFillup && isError<=0) {              
        placeHolder.find(".marks").removeClass("highlight");
        placeHolder.removeClass("danger");
        placeHolder.addClass("info");
        return 1;
    }
    else {
        placeHolder.removeClass("info");
        placeHolder.addClass("danger");
        return 3;
    }    
}

$(document).on("blur", ".marks ", function (event) {
    checkRowValidity($(this).parent().parent());
});

$(document).on("input propertychange", ".marks ", function (event) {
    var input = $(this),
    text = input.val().replace(/[^0-9.]/g, "");    
    input.val(text);
});
$(document).on("click", "#deleteMarks", function (event) {
    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var courseId = $("#Course").val();
    var examId = $("#ExamId").val();
    var examType = $("#ExamType").val();
    var prnNo = $("#PrnNo").val();

    if (organizationId!="" && programId != "" && sessionId != "" && courseId != "" && examId != "" && examType != "" && prnNo != "") {
        var data = JSON.stringify({ "Program": programId, "Session": sessionId, "Course": courseId, "ExamId": examId, "ExamType": examType, "PrnNo": prnNo });

        bootbox.deleteConfirm("<h3>Are you sure you want to delete ?</h3>", function (result) {
            if (result) {
                $.ajax({
                    type: "post",
                    url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForDeleteStudentMarks", 
                    cache: false,
                    async: true,
                    dataType: "json",
                    contentType: "application/json",
                    data: data,
                    beforeSend: function() {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function(result) {
                        $.unblockUI();
                        if (result.IsSuccess) {
                            var str = '<div class="text-center"><div class="alert alert-success" style="margin-bottom: 0px;"><a class="close" data-dismiss="alert">X</a><strong>Success!</strong> ' + result.Message + '</div></div>';
                            $("#manualMarksEntry tbody").remove();
                            $("#manualMarksEntry thead tr").empty();
                            $("#manualMarksEntry thead tr").append("<th>" + str + "</th>");
                            //manualMarksEntry
                        }
                        else {
                            var str = '<div class="text-center"><div class="alert alert-danger" style="margin-bottom: 0px;"><a class="close" data-dismiss="alert">X</a><strong>Error!</strong> ' + result.Message + '</div></div>';
                            //$("#manualMarksEntry").remove();
                            $(str).insertBefore("#manualMarksEntry");
                        }
                
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    error: function(result) {
                        $.unblockUI();
                        console.log("Failed -----");
                    }
                });
            }
        }).css({ 'margin-top': (($(window).height() / 4)) });           
    }
});

$(document).on("click", "#updateMarks", function (event) {
    var validRow = 0;
    var inValidRow = 0;
    var blankRow = 0;
    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var courseId = $("#Course").val();
    var examId = $("#ExamId").val();
    var examType = $("#ExamType").val();
    var dataArray = [];
    var isValidRow;

    $("#manualMarksEntry tbody tr").not(':last').each(function () {       
        isValidRow = checkRowValidity($(this));
       
        console.log(isValidRow);
        if (isValidRow == 1) {
            var marks = [];
            validRow = validRow + 1;
            obj = {};
            
            obj["Sl"] = $(this).attr("id").slice(6);
            obj["PrnNo"] = $(this).find(".prnNoClass").val();  
            obj["SetCode"] = $(this).find(".setCodeClass").val();

            $(this).find(".marks").each(function() {
                obj1 = {};               
                obj1["ExamDetailsId"] = $(this).attr("data-id");
                obj1["Marks"] = $(this).val();
                marks.push(obj1);
            });
            obj["MarksList"] = marks;
            dataArray.push(obj);
        }
        else {
            inValidRow = inValidRow + 1;
        }        
    });



   
    //console.log(programId + " " + sessionId + " " + courseId + " " + examId + " " + examType);
    //console.log(dataArray);
    if (organizationId!="" && programId != "" && sessionId != "" && courseId != "" && examId != "" && examType != "" && dataArray.length > 0) {
        var data = JSON.stringify({ "Program": programId, "Session": sessionId, "Course": courseId, "ExamId": examId, "ExamType": examType, "StudentMarksList": dataArray });
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Exam/MarksEntry/AjaxRequestForUpdateStudentMarks", 
            cache: false,
            async: true,
            dataType: "json",
            contentType: "application/json",
            data: data,
            //data: { "programId": programId, "sessionId": sessionId, "courseId": courseId, "examId": examId, "examType": examType, "dataArray": dataArray },
            beforeSend: function() {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function(result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $.each(result.returnList, function (i, v) {
                        var rowId = "rowId_" + v.Key;
                        $.each(v.Group, function (ii, vv) {
                            console.log(vv);
                            if (vv.mesageType == 0) {
                                // $("#" + rowId).removeClass("info").addClass("success");
                                //$("#" + rowId).removeClass("info").addClass("success");
                               
                                $("#PrnNo").val("");
                                $("#manualMarksEntry").empty();
                                $("#manualMarksEntry").append("<thead><tr><th>Please Fillup Exam details criteria.</th></tr></thead>");
                            }
                            else {
                                $("#" + rowId).removeClass("info").addClass("danger");
                            }
                        });
                    });
                    $.fn.customMessage({
                        displayMessage: "Marks update Successfully!",
                        displayMessageType: "success",
                        displayTime: 100000
                    });
                }
                else {

                }
                $("body, html").animate({
                    scrollTop: $("#marksPanel").offset().top
                }, 600);
            },
            complete: function() {
                $.unblockUI();
            },
            error: function(result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
    else {
        var firstErrorId = $("#manualMarksEntry tbody .danger:first").attr("id");       
        if (firstErrorId == undefined) {
            firstErrorId = "marksPanels";
        }
        else {
            if (parseInt(firstErrorId.slice(6)) < 3) {
                firstErrorId = "marksPanel";
            }
            else {
                firstErrorId = "rowId_" + (parseInt(firstErrorId.slice(6))-3);
            }
        }

        console.log(firstErrorId);
        $("body, html").animate({
            scrollTop: $("#" + firstErrorId).offset().top
        }, 600);
    }    
});


