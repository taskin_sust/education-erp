using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{
    
    
    public partial class FestivalBonusSettingCalculationMap : BaseClassMap<FestivalBonusSettingCalculation,long> {

        public FestivalBonusSettingCalculationMap()
        {
            Table("HR_FestivalBonusSettingCalculation");
			LazyLoad();
			References(x => x.PrFestivalBonusSetting).Column("FestivalBonusSettingId");
			Map(x => x.EmploymentStatus).Column("EmploymentStatus").Not.Nullable();
			Map(x => x.CalculationOn).Column("CalculationOn").Not.Nullable();
			Map(x => x.BonusPercentage).Column("BonusPercentage").Not.Nullable();
        }
    }
}
