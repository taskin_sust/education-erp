﻿using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;
using NHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Mapping.UserAuth
{
    public class LoginActivityLogMap:ClassMap<LoginActivityLog>
    {
        public LoginActivityLogMap()
        {
            Table("LoginActivityLog");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.CreationDate);
            Map(x => x.IpAddress);
            Map(x => x.Device);
            Map(x => x.OsName);
            Map(x => x.Browser);
            Map(x => x.UserName);
            Map(x => x.OperationStatus);
            Map(x => x.Remarks);
            Map(x => x.UserAgent);
        }
    }
}
