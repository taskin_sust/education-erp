﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.WorkOrderTest
{
    interface IWorkOrderGetLastWorkOrderNoTest
    {
        #region basic Test

        void Should_Return_LastWorkOrderNo();
        void Should_Return_Empty_String();

        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "WorkOrder")]
    public class WorkOrderGetLastWorkOrderNoTest : WorkOrderBaseTest, IWorkOrderGetLastWorkOrderNoTest
    {
        [Fact]
        public void Should_Return_LastWorkOrderNo()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.PertialIssued, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create()
                .CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList)
                .Persist();

            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierFactory.Object.SupplierPriceQuoteList.Add(_supplierPriceQuoteFactory.Object);//Add price quote list in supplier

            _workOrderFactory.Create().WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object)
                .Persist();
            Assert.Equal(_workOrderFactory.Object.WorkOrderNo,_workOrderService.GetLastWorkOrderNo(_workOrderFactory.Object.Branch.Id));
        }

        [Fact]
        public void Should_Return_Empty_String()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.PertialIssued, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create()
                .CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList)
                .Persist();

            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierFactory.Object.SupplierPriceQuoteList.Add(_supplierPriceQuoteFactory.Object);//Add price quote list in supplier

            _workOrderFactory.Create().WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object)
                .Persist();
            Assert.Equal("", _workOrderService.GetLastWorkOrderNo(_workOrderFactory.Object.Branch.Id+1));
        }
    }
}
