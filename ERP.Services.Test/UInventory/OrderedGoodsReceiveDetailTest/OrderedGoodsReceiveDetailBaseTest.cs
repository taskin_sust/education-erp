﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.OrderedGoodsReceiveDetailTest
{
    public class OrderedGoodsReceiveDetailBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper CommonHelper;
        protected readonly GoodsReceiveFactory _goodsReceiveFactory;
        protected readonly GoodsReceiveDetailsFactory _goodsReceiveDetailsFactory;
        protected readonly CurrentStockSummaryFactory _currentStockSummaryFactory;
        protected readonly ItemFactory _itemFactory;
        protected readonly ProgramFactory _programFactory;
        protected readonly SessionFactory _sessionFactory;

        protected readonly IGoodsReceiveService _goodsReceiveService;
        protected readonly IGoodsReceiveDetailService _GoodsReceiveDetailService;
        #endregion

        public OrderedGoodsReceiveDetailBaseTest()
        {
            CommonHelper = new CommonHelper();
            _goodsReceiveFactory = new GoodsReceiveFactory(null, Session);
            _goodsReceiveDetailsFactory = new GoodsReceiveDetailsFactory(null, Session);
            _currentStockSummaryFactory = new CurrentStockSummaryFactory(null, Session);
            _itemFactory = new ItemFactory(null, Session);
            _programFactory = new ProgramFactory(null, Session);
            _sessionFactory = new SessionFactory(null, Session);

            _goodsReceiveService = new GoodsReceiveService(Session);
            _GoodsReceiveDetailService = new GoodsReceiveDetailsService(Session);
        }

        public void Disposal()
        {
            if (_goodsReceiveFactory.Object != null)
                foreach (var recDetail in _goodsReceiveFactory.Object.GoodsReceiveDetails)
                {
                    if (recDetail.GoodsReceive.Branch != null && recDetail.Item != null)
                    {
                        string swData = "DELETE FROM [UINV_CurrentStockSummary] WHERE [BranchId] = " + recDetail.GoodsReceive.Branch.Id + " and [ItemId] =" + recDetail.Item.Id + " ; ";
                        Session.CreateSQLQuery(swData).ExecuteUpdate();
                    }
                }
            _goodsReceiveDetailsFactory.CleanUp();
            _goodsReceiveFactory.CleanUp();
            base.Dispose();
        }
    }
}
