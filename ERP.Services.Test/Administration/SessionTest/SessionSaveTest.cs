﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.SessionTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Session")]
    public class SessionSaveTest : SessionBaseTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _sessionService.Save(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var sessionFactory =
                _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber())
                    .Persist();
            Session sessionObj = _sessionService.LoadById(sessionFactory.Object.Id);
            Assert.NotNull(sessionObj);
            Assert.True(sessionObj.Id > 0);
        }
        #endregion

        #region Business Logic Test
        
        [Fact]
        public void Should_Not_Save_Empty_Name()
        {
            var sessionFactory =
               _sessionFactory.Create().WithNameAndRank("", 1).WithCode(_sessionFactory.GetRandomNumber());
            Assert.Throws<EmptyFieldException>(() => sessionFactory.Persist());
        }
        [Fact]
        public void Should_Not_Save_Empty_Code()
        {
            var sessionFactory =
               _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode("");
            Assert.Throws<EmptyFieldException>(() => sessionFactory.Persist());
        }

        [Fact]
        public void Save_Should_Check_Duplicate_Name_Entry()
        {
            var sessionFactory =
                _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber())
                    .Persist();
            var sessionFactory2 =
                _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber());
            Assert.Throws<DuplicateEntryException>(() => sessionFactory2.Persist());
        }
        [Fact]
        public void Save_Should_Check_Duplicate_Code_Entry()
        {
            var sessionFactory =
               _sessionFactory.Create().WithNameAndRank(_name, 1).WithCode(_sessionFactory.GetRandomNumber())
                   .Persist();
            var code = sessionFactory.Object.Code;
            var sessionFactory2 =
                _sessionFactory.Create().WithNameAndRank(_name + " DFGDD", 1).WithCode(code);
            Assert.Throws<DuplicateEntryException>(() => sessionFactory2.Persist());
        }

        [Fact]
        public void Model_Validation_For_Invalid_Code_Max_Lenght()
        {
            _sessionFactory.Create().WithCode("111");
            Assert.Throws<InvalidDataException>(() => _sessionFactory.Persist());
        }
        [Fact]
        public void Model_Validation_For_Invalid_Code_Min_Lenght()
        {
            _sessionFactory.Create().WithCode("1");
            Assert.Throws<InvalidDataException>(() => _sessionFactory.Persist());
        }
        [Fact]
        public void Model_Validation_For_Invalid_Code_Format()
        {
            _sessionFactory.Create().WithCode("QQ");
            Assert.Throws<InvalidDataException>(() => _sessionFactory.Persist());
        }
        [Fact]
        public void Model_Validation_Invalid_Code_Format()
        {
            _sessionFactory.Create().WithCode("00");
            Assert.Throws<InvalidDataException>(() => _sessionFactory.Persist());
        }

        #endregion
    }
}
