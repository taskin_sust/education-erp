﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MenuTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Menu")]
    public class MenuSaveTest : MenuBaseTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _menuService.Save(menu:null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var menuFactory =
                _menuFactory.Create().WithNameAndRank(_name, 1).WithMenuGroup().WithAction()
                    .Persist();
            Menu menu = _menuService.MenuLoadById(menuFactory.Object.Id);
            Assert.NotNull(menu);
            Assert.True(menu.Id > 0);
        }
        [Fact]
        public void Should_Save_Successfully_With_Parent_Menu()
        {
            var menuFactory =
                _menuFactory.CreateMore(2).Persist();
            var parentMenu = menuFactory.GetLastCreatedObjectList()[0];
            var childMenu = menuFactory.GetLastCreatedObjectList()[1];
            childMenu.MenuSelfMenu = parentMenu;
            Menu menu = _menuService.MenuLoadById(childMenu.Id);
            Assert.NotNull(menu);
            Assert.True(menu.Id > 0);
        }
        //[Fact]
        //public void Save_Should_Check_Duplicate_Name_Entry()
        //{
        //    var menuFactory =
        //        _menuFactory.Create().WithNameAndRank(_name, 1).WithMenuGroup().WithAction()
        //            .Persist();
        //    var menuFactory2 =
        //        _menuFactory.Create().WithNameAndRank(_name, 1).WithMenuGroup().WithAction()
        //            .Persist();
        //    Assert.Throws<DuplicateEntryException>(() => menuFactory2.Persist());
        //}
        
        #endregion

        #region Business Logic Test
        
        //[Fact]
        //public void Should_Not_Save_Empty_Name()
        //{
        //    var menuFactory =
        //        _menuFactory.Create().WithNameAndRank(_name, 1).WithMenuGroup().WithAction()
        //            .Persist();
        //    Assert.Throws<EmptyFieldException>(() => menuFactory.Persist());
        //}
        
        
        #endregion
    }
}
