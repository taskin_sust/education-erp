﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Cache;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ChildrenInfoTest
{
    public class ChildrenInfoBaseTest:TestBase, IDisposable
    {
        #region Propertise & Object Initialization
        protected readonly IChildrenInfoService ChildrenInfoService;
        internal ChildrenInfoFactory ChildrenInfoFactory;
        private ISession _session;

        #endregion

        public ChildrenInfoBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            ChildrenInfoService = new ChildrenInfoService(_session);
            ChildrenInfoFactory = new ChildrenInfoFactory(null, _session);
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public new void Dispose()
        {
            ChildrenInfoFactory.LogCleanUp(teamMemberFactory.Object);
            ChildrenInfoFactory.CleanUp();
            base.Dispose();
        }

    }
}
