﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Common;

namespace UdvashERP.Dao.Mapping.Common
{
    public partial class ActivityLogMap:BaseClassMap<ActivityLog,long>
    {
        public ActivityLogMap()
        {
            Table("ActivityLog");
            LazyLoad();
            //Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name);
            Map(x => x.Description);
        }
    }
}
