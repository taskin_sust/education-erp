﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.SpecificationTemplateTest
{
    public class SpecificationTemplateBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal SpecificationTemplateFactory _specificationTemplateFactory;
        internal ISpecificationTemplateService _specificationTemplateService;
        internal TestBaseService<SpecificationTemplate> _specificationTamplaTestBaseService;
        #endregion

        public SpecificationTemplateBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _specificationTemplateFactory = new SpecificationTemplateFactory(null, _session);
            _specificationTamplaTestBaseService = new TestBaseService<SpecificationTemplate>(_session);
            _specificationTemplateService = new SpecificationTemplateService(_session);
        }
        public void CleanUpSpecificationCriteria(IList<SpecificationCriteria> specificationCriterias)
        {
            _specificationTamplaTestBaseService.DeleteSpecificationCriteria(specificationCriterias);
        }
        
        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            base.Dispose();
        }
    }
}
