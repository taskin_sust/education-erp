﻿using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.UserAuth
{
    public interface IAreaControllersDao : IBaseDao<AreaControllers, long>
    {
        #region Single Instances Loading Function
        AreaControllers LoadByRank(int rank);
        AreaControllers LoadByControllerName(string controller);
        AreaControllers LoadByAreaController(string area, string controller);
        #endregion

        #region List Loading Function
        IList<AreaControllers> LoadActive();
        IList<AreaControllers> LoadArea();
        IList<AreaControllers> LoadControllersByArea(string area);
        IList<AreaControllers> LoadActive(int start, int length, string orderBy, string orderDir, string name, string area, string status, string rank);   
        IList<AreaControllers> LoadLike(string name);
        #endregion

        #region Others Function
        bool HasDuplicateByName(string name);
        bool HasDuplicateByName(string name, long id);
        bool CheckDataByAreaController(string aName, string name);
        int AreaControllersRowCount(string name, string area, string status, string rank);
        #endregion

        //no need
        //new IList<AreaControllers> LoadByName(string name);
    }
    public class AreaControllersDao : BaseDao<AreaControllers, long>, IAreaControllersDao
    {
        #region Single Instances Loading Function
        public AreaControllers LoadByAreaController(string area, string controller)
        {
            return
                Session.QueryOver<AreaControllers>()
                    .Where(
                        x => x.Area == area && x.Name == controller && x.Status == AreaControllers.EntityStatus.Active).SingleOrDefault<AreaControllers>();
        }
        public AreaControllers LoadByControllerName(string controller)
        {
            return Session.QueryOver<AreaControllers>().Where(x => x.Name == controller && x.Status == AreaControllers.EntityStatus.Active).SingleOrDefault<AreaControllers>();
        }
        public AreaControllers LoadByRank(int rank)
        {
            return Session.QueryOver<AreaControllers>().Where(x => x.Rank == rank && x.Status == AreaControllers.EntityStatus.Active).SingleOrDefault<AreaControllers>();
        }
        #endregion

        #region List Loading Function
        public IList<AreaControllers> LoadActive()
        {
            ICriteria criteria = Session.CreateCriteria(typeof(AreaControllers));
            criteria.Add(Expression.Eq("Status", AreaControllers.EntityStatus.Active));
            return criteria.List<AreaControllers>();
        }
        public IList<AreaControllers> LoadArea()
        {
            ICriteria criteria = Session.CreateCriteria(typeof(AreaControllers));
            criteria.Add(Expression.Eq("Status", AreaControllers.EntityStatus.Active));
            criteria.SetProjection(Projections.Distinct(Projections.Alias(Projections.Property("Area"), "Area")));
            criteria.SetResultTransformer(
                new NHibernate.Transform.AliasToBeanResultTransformer(typeof(AreaControllers)));
            return criteria.List<AreaControllers>();
        }
        public IList<AreaControllers> LoadControllersByArea(string area)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(AreaControllers));
            criteria.Add(Expression.Eq("Status", AreaControllers.EntityStatus.Active));
            criteria.Add(Expression.Eq("Area", area));
            return criteria.List<AreaControllers>();
        }
        public IList<AreaControllers> LoadLike(string name)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(AreaControllers));
            criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            criteria.Add(Expression.Eq("Status", AreaControllers.EntityStatus.Active));
            return criteria.List<AreaControllers>();
        }
        public IList<AreaControllers> LoadActive(int start, int length, string orderBy, string orderDir, string name, string area, string status, string rank)
        {
            ICriteria criteria =
                    Session.CreateCriteria<AreaControllers>()
                        .Add(Restrictions.Not(Restrictions.Eq("Status", AreaControllers.EntityStatus.Delete)));


            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }

            if (!String.IsNullOrEmpty(area))
            {
                criteria.Add(Restrictions.Like("Area", area, MatchMode.Anywhere));
            }

            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));

            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }

            if (!string.IsNullOrEmpty(orderBy.Trim()))
            {
                if (orderDir == "ASC")
                {
                    criteria.AddOrder(Order.Asc(orderBy));
                }
                else
                {
                    criteria.AddOrder(Order.Desc(orderBy));
                } 
            }

            return (List<AreaControllers>)criteria.SetFirstResult(start).SetMaxResults(length).List<AreaControllers>();
        }
        #endregion

        #region Others Function
        public bool CheckDataByAreaController(string aName, string name)
        {
            var rowList =
                Session.QueryOver<AreaControllers>()
                    .Where(x => x.Name == name && x.Area == aName && x.Status == AreaControllers.EntityStatus.Active)
                    .RowCount();
            if (rowList < 1)
                return false;
            return true;
        }
        public bool HasDuplicateByName(string name)
        {
            IQuery query = Session.CreateQuery("from AreaControllers c where c.Name=:name and c.Status=:status");
            query.SetString("name", name);
            query.SetInt32("status", AreaControllers.EntityStatus.Active);
            IList<AreaControllers> rowList = query.List<AreaControllers>();

            if (rowList == null || rowList.Count < 1)
                return false;
            return true;
        }
        public bool HasDuplicateByName(string name, long id)
        {
            IQuery query = Session.CreateQuery("from AreaControllers c where c.Name=:name and c.Status=:status and c.Id<>:id");
            query.SetString("name", name);
            query.SetInt32("status", AreaControllers.EntityStatus.Active);
            query.SetInt64("id", id);
            IList<AreaControllers> rowList = query.List<AreaControllers>();

            if (rowList == null || rowList.Count < 1)
                return true;
            return false;
        }
        public int AreaControllersRowCount(string name, string Area, string status, string rank)
        {
            ICriteria criteria = Session.CreateCriteria<AreaControllers>();

            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(Area))
            {
                criteria.Add(Restrictions.Like("Area", Area, MatchMode.Anywhere));

            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }

            IList list = criteria.Add(Restrictions.Not(Restrictions.Eq("Status", AreaControllers.EntityStatus.Delete))).List();
            return list.Count;
        }
        #endregion

        //no need
        //public new IList<AreaControllers> LoadByName(string name)
        //{
        //    IQuery query = Session.CreateQuery("from AreaControllers c where c.Name=:name and c.Status=:status");
        //    query.SetString("name", name);
        //    query.SetInt32("status", AreaControllers.EntityStatus.Active);

        //    IList<AreaControllers> rowList = query.List<AreaControllers>();

        //    if (rowList == null || rowList.Count < 1)
        //        return null;
        //    return rowList;
        //}
    }
}
