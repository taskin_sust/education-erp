﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.AreaControllersTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "AreaControllers")]
    public class AreaControllersTest : AreaControllersBaseTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => _areaControllersService.Save(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var areaControllersFactory =
                _areaControllersFactory.Create().WithNameAndRank(_name, 1).WithArea(_areaName)
                    .Persist();
            AreaControllers areaObj = _areaControllersService.LoadById(areaControllersFactory.Object.Id);
            Assert.NotNull(areaObj);
            Assert.True(areaObj.Id > 0);
        }
        
        [Fact]
        public void Save_Should_Check_Duplicate_Name_Entry()
        {
            var areaControllersFactory =
                _areaControllersFactory.Create().WithNameAndRank(_name, 1).WithArea(_areaName)
                    .Persist();
            var sessionFactory2 =
                _areaControllersFactory.Create().WithNameAndRank(_name, 1).WithArea(_areaName);
            Assert.Throws<DuplicateEntryException>(() => sessionFactory2.Persist());
        }
        
        #endregion

        #region Business Logic Test
        
        [Fact]
        public void Should_Not_Save_Empty_Name()
        {
            var areaControllersFactory =
               _areaControllersFactory.Create().WithNameAndRank("", 1).WithArea(_areaName);
            Assert.Throws<EmptyFieldException>(() => areaControllersFactory.Persist());
        }
        [Fact]
        public void Should_Not_Save_Empty_areaName()
        {
            var areaControllersFactory =
               _areaControllersFactory.Create().WithNameAndRank(_name, 1).WithArea("");
            Assert.Throws<EmptyFieldException>(() => areaControllersFactory.Persist());
        }
        
        #endregion
    }
}
