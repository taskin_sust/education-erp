﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;
using UdvashERP.Services.Hr;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class AttendanceSynchronizerController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICampusService _campusService;
        private readonly IBranchService _branchService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IAttendanceDeviceService _hrAttendanceDeviceService;
        private readonly IAttendanceSynchronizerService _hrAttendanceSynchronizerService;

        public AttendanceSynchronizerController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _campusService = new CampusService(session);
            _branchService = new BranchService(session);
            _userService = new UserService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _hrAttendanceDeviceService = new AttendanceDeviceService(session);
            _hrAttendanceSynchronizerService = new AttendanceSynchronizerService(session);
        }

        #endregion

        #region Save Operation

        public ActionResult Create()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.CampusList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.CommunicationTypeList = Enum.GetValues(typeof(DeviceCommunicationType)).Cast<DeviceCommunicationType>().Select(x => new SelectListItem { Text = x.GetDescription(), Value = ((int)x).ToString() }).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AttendanceSynchronizerViewModel hrAttendanceSynchronizerObj, string saveType = "1")
        {

            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");

                if (hrAttendanceSynchronizerObj.Organization <= 0)
                {
                    ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
                }
                else
                {
                    List<long> organizationIdList = new List<long>();
                    organizationIdList.Add(hrAttendanceSynchronizerObj.Organization);
                    ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, organizationIdList), "Id", "Name");
                }

                if (hrAttendanceSynchronizerObj.Branch <= 0)
                {
                    ViewBag.CampusList = new SelectList(string.Empty, "Value", "Text");
                }
                else
                {
                    List<long> branchIdList = new List<long>();
                    branchIdList.Add(hrAttendanceSynchronizerObj.Branch);
                    ViewBag.CampusList = new SelectList(_campusService.LoadAuthorizeCampus(userMenu, null, null, branchIdList), "Id", "Name");
                }


                if (ModelState.IsValid)
                {
                    var sync = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(hrAttendanceSynchronizerObj.SynchronizerKey);
                    if (sync != null)
                    {
                        throw new DuplicateEntryException("SynchronizerKey Already Used");
                    }
                    var obj = new AttendanceSynchronizer();

                    obj.Campus = _campusService.GetCampus(hrAttendanceSynchronizerObj.Campus);
                    obj.Name = hrAttendanceSynchronizerObj.Name;
                    obj.SynchronizerKey = hrAttendanceSynchronizerObj.SynchronizerKey;
                    obj.DataCallingInterval = hrAttendanceSynchronizerObj.DataCallingInterval;
                    obj.OperatorContact = hrAttendanceSynchronizerObj.OperatorContact;
                    obj.IpWhiteList = hrAttendanceSynchronizerObj.IpWhiteList;
                    obj.LastMemberInfoUpdateTime = DateTime.Now;


                    _hrAttendanceSynchronizerService.Save(obj);
                    if (saveType == "0")
                    {
                        ViewBag.SuccessMessage = "Synchronizer Add successfull";
                        ModelState.Clear();
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = "Duplicate Synchronizer found";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        #endregion

        #region Index/Manage Page

        public ActionResult Index()
        {
            SelectList branch = null, organization = null, campus = null;
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
            ViewBag.organizationlist = organization;
            ViewBag.BranchId = new SelectList(string.Empty, "Value", "Text");
            ViewBag.CampusId = new SelectList(string.Empty, "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult Index(int draw, int start, int length, string organizationId, string branchId, string campusId, string iPAddress)
        {
            var data = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = "SynchronizerKey";
                        orderDir = nvc["order[0][dir]"];
                    }

                    #endregion

                    recordsTotal = _hrAttendanceSynchronizerService.SynchronizerRowCount(organizationId, branchId, campusId);
                    recordsFiltered = recordsTotal;
                    List<AttendanceSynchronizer> dataList = _hrAttendanceSynchronizerService.LoadSynchronizer(start, length, orderBy, orderDir.ToUpper(), organizationId, branchId, campusId);
                    foreach (var c in dataList)
                    {
                        var str = new List<string>();
                        str.Add(c.Campus.Branch.Organization.ShortName);
                        str.Add(c.Campus.Branch.Name);
                        str.Add(c.Campus.Name);
                        str.Add(c.Name);
                        str.Add(c.SynchronizerKey);
                        str.Add(c.DataCallingInterval.ToString());
                        str.Add(c.OperatorContact);
                        str.Add(c.LastMemberInfoUpdateTime.ToString());
                        str.Add(c.IpWhiteList);
                        str.Add(c.Status == 1 ? "Active" : "Inactive");
                        str.Add(LinkGenerator.GetEditLink("Edit", "AttendanceSynchronizer", c.Id) + LinkGenerator.GetDeleteLinkForModal(c.Id, c.Name));
                        data.Add(str);
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                    _logger.Error(ex);
                }
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        #endregion

        #region Delete Operation

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                AttendanceSynchronizer deleteHRAttendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(id);
                deleteHRAttendanceSynchronizer.Status = AttendanceSynchronizer.EntityStatus.Delete;
                _hrAttendanceSynchronizerService.Delete(deleteHRAttendanceSynchronizer);
                return Json(new Response(true, "Attendance Synchronizer Delete Successful !"));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Update Operation

        public ActionResult Edit(long id)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                AttendanceSynchronizer hRAttendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(id);
                if (hRAttendanceSynchronizer == null)
                {
                    return HttpNotFound();
                }
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                var statusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key", hRAttendanceSynchronizer.Status);
                ViewBag.StatusList = statusList;
                List<long> organizationIdList = new List<long>();
                organizationIdList.Add(hRAttendanceSynchronizer.Campus.Branch.Organization.Id);
                ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, organizationIdList, null, null, false), "Id", "Name");

                List<long> branchIdList = new List<long>();
                branchIdList.Add(hRAttendanceSynchronizer.Campus.Branch.Id);
                ViewBag.CampusList = new SelectList(_campusService.LoadAuthorizeCampus(userMenu, null, null, branchIdList, null, false), "Id", "Name");
                
                AttendanceSynchronizerViewModel obj = new AttendanceSynchronizerViewModel();
                obj.Organization = hRAttendanceSynchronizer.Campus.Branch.Organization.Id;
                obj.Branch = hRAttendanceSynchronizer.Campus.Branch.Id;
                obj.Campus = hRAttendanceSynchronizer.Campus.Id;
                obj.Name = hRAttendanceSynchronizer.Name;
                obj.SynchronizerKey = hRAttendanceSynchronizer.SynchronizerKey;
                obj.DataCallingInterval = hRAttendanceSynchronizer.DataCallingInterval;
                obj.OperatorContact = hRAttendanceSynchronizer.OperatorContact;
                obj.IpWhiteList = hRAttendanceSynchronizer.IpWhiteList;
                obj.Status = hRAttendanceSynchronizer.Status;
                return View(obj);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AttendanceSynchronizerViewModel hrAttendanceSynchronizerObj, long id)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                AttendanceSynchronizer obj = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(id);
                if (obj == null)
                {
                    return HttpNotFound();
                }

                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key", obj.Status);
                if (hrAttendanceSynchronizerObj.Organization <= 0)
                {
                    ViewBag.BranchList = new SelectList(string.Empty, "Value", "Text");
                }
                else
                {
                    List<long> organizationIdList = new List<long>();
                    organizationIdList.Add(hrAttendanceSynchronizerObj.Organization);
                    ViewBag.BranchList = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, organizationIdList, null, null, false), "Id", "Name");
                }

                if (hrAttendanceSynchronizerObj.Branch <= 0)
                {
                    ViewBag.CampusList = new SelectList(string.Empty, "Value", "Text");
                }
                else
                {
                    List<long> branchIdList = new List<long>();
                    branchIdList.Add(hrAttendanceSynchronizerObj.Branch);
                    ViewBag.CampusList = new SelectList(_campusService.LoadAuthorizeCampus(userMenu, null, null, branchIdList, null, false), "Id", "Name");
                }

                if (hrAttendanceSynchronizerObj.Campus <= 0)
                {
                    ViewBag.SynchronizerList = new SelectList(string.Empty, "Value", "Text");
                }
                else
                {
                    List<long> campusIdList = new List<long>();
                    campusIdList.Add(hrAttendanceSynchronizerObj.Campus);
                    ViewBag.SynchronizerList = new SelectList(_hrAttendanceSynchronizerService.LoadSynchronizer(null, null, campusIdList), "Id", "Name");
                }

                ViewBag.CommunicationTypeList = Enum.GetValues(typeof(DeviceCommunicationType)).Cast<DeviceCommunicationType>().Select(x => new SelectListItem { Text = x.GetDescription(), Value = ((int)x).ToString() }).ToList();

                if (ModelState.IsValid)
                {
                    var sync = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(hrAttendanceSynchronizerObj.SynchronizerKey);
                    if (sync != null && sync.Id != obj.Id)
                    {
                        throw new DuplicateEntryException("SynchronizerKey Already Used");
                    }
                    obj.Campus = _campusService.GetCampus(hrAttendanceSynchronizerObj.Campus);
                    obj.Name = hrAttendanceSynchronizerObj.Name;
                    obj.SynchronizerKey = hrAttendanceSynchronizerObj.SynchronizerKey;
                    obj.DataCallingInterval = hrAttendanceSynchronizerObj.DataCallingInterval;
                    obj.OperatorContact = hrAttendanceSynchronizerObj.OperatorContact;
                    obj.IpWhiteList = hrAttendanceSynchronizerObj.IpWhiteList;
                    obj.Status = hrAttendanceSynchronizerObj.Status;
                    _hrAttendanceSynchronizerService.Update(obj);
                    ViewBag.SuccessMessage = "Synchronizer Update successfull";
                }
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        #endregion
    }
}