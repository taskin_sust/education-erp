﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ChildrenAllowanceSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ChildrenAllowanceSettingService")]
    public class ChildrenAllowanceSettingSaveTest : ChildrenAllowanceSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Save_Successfully()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            Assert.True(_childrenAllowanceSettingFactory.Object.Id > 0);
        }

        #region validation test

        [Fact]
        public void Should_Return_AuthenticationException()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            //var menu = BuildUserMenu(_commonHelper.ConvertIdToList(_organizationFactory.Object));
            Assert.Throws<AuthenticationException>(() => _childrenAllowanceSettingService.SaveOrUpdate(null, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            _childrenAllowanceSettingFactory.Object.EffectiveDate = DateTime.Now.AddDays(5);
            _childrenAllowanceSettingFactory.Object.ClosingDate = DateTime.Now;
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_NullObjectException()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            Assert.Throws<NullObjectException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, null));
        }

        #endregion

        #region Invalid Property

        [Fact]
        public void Should_Check_MaxNoOfChild_For_Range_MinValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.MaxNoOfChild = 0;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_MaxNoOfChild_For_Range_MaxValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.MaxNoOfChild = 11;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_Amount_For_Range_MinValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.Amount = 0;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_Amount_For_Range_MaxValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.Amount = 1000001;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_StartingAge_For_Range_MinValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.StartingAge = -1;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));

        }

        [Fact]
        public void Should_Check_StartingAge_For_Range_MaxValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.StartingAge = 101;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));

        }

        [Fact]
        public void Should_Check_ClosingAge_For_Range_MinValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.ClosingAge = -1;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_ClosingAge_For_Range_MaxValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.ClosingAge = 101;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }
        [Fact]
        public void Should_Check_PaymentType_For_Range_MinValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.PaymentType = 1;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_PaymentType_For_Range_MaxValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.PaymentType = 5;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }


        #endregion

        #endregion

        #region Authorization Check

        [Fact]
        public void Should_Return_UnAuthorized_Menu_Message()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { _organizationFactory.Create().Persist().Object }, null, null);
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel));
            //TODO: This test case will take place at SheetService Test
        }

        #endregion

        #region Business Logic

        [Fact]
        public void Should_Return_InvalidDataException_For_Organization()
        {
            _organizationFactory.Create().Persist();
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Create();
            _childrenAllowanceSettingFactory.Object.PaymentType = Convert.ToInt32(null);
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_EmploymentType()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.IsContractual = false;
            _childrenAllowanceSettingFactory.Object.IsPartTime = false;
            _childrenAllowanceSettingFactory.Object.IsPermanent = false;
            _childrenAllowanceSettingFactory.Object.IsProbation = false;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_PaymentType()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingFactory.Object.PaymentType = Convert.ToInt32(null);
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_IsPost_Condition_ForSave_And_Return_InvalidDataException_Sheet_GeneratedFor_This_Month()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList);
            _childrenAllowanceSettingFactory.Create()
                .WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2016, 01, 01), new DateTime(2017, 12, 30))
                .WithOrganization(org);
            var cldAllPbj = _childrenAllowanceSettingFactory.SingleObjectList[1];
            Assert.Throws<MessageException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menus, cldAllPbj));

        }

        [Fact]
        public void Should_Check_IsPost_Condition_ForUpdate_And_Return_InvalidDataException_BecauseOf_AssignedToMembers()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 12, 30));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01), new DateTime(2017, 12, 30))
                .WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList);
            _childrenAllowanceSettingFactory.SingleObjectList[0].EffectiveDate = new DateTime(2017, 03, 01);
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menus, _childrenAllowanceSettingFactory.SingleObjectList[0]));
        }

        [Fact]
        public void Should_Check_IsPost_Condition_ForUpdate_And_Return_InvalidDataException_BecauseOf_ClosingdateSmaller_Than_EffectiveDate()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, null);
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10).WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01),
                new DateTime(2017, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList);
            _childrenAllowanceSettingFactory.Object.ClosingDate = new DateTime(2014, 1, 30);
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menus, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_IsPost_Condition_ForUpdate_And_Return_InvalidDataException_BecauseOf_Blocked_Members()
        {
            TeamMember member = PersistTeamMember(new DateTime(2017, 01, 01), new DateTime(2017, 12, 30));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, null);
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10).WithEffectiveAndCloxingDate(new DateTime(2017, 01, 01), new DateTime(2017, 12, 30)).WithOrganization(org).Persist(menus);
            _allowanceBlockFactory.Create()
                .WithTeamMember(teamMemberFactory.SingleObjectList[0])
                .WithCkAllowance(_childrenAllowanceSettingFactory.Object).WithDateTime(new DateTime(2017, 03, 01), new DateTime(2017, 10, 30));
            _allowanceBlockService.SaveOrUpdate(menus, _allowanceBlockFactory.Object);
            _childrenAllowanceSettingFactory.Object.ClosingDate = new DateTime(2017, 08, 01);
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menus, _childrenAllowanceSettingFactory.Object));
        }

        #endregion
    }
}
