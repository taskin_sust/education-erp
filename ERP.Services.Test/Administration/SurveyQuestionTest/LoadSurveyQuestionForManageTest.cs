﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Test.AdminisTration.SurveyQuestionTest;
using Xunit;

namespace UdvashERP.Services.Test.Administration.SurveyQuestionTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "SurveyQuestion")]
    public class LoadSurveyQuestionForManageTest : SurveyQuestionBaseTest
    {
        [Fact]
        public void Should_Return_Zero()
        {
           // _surveyQuestionFactory.Create().WithQuestionAndRank(_question, 10);
           // _surveyQuestionAnswerFactory.CreateMore(2, _surveyQuestionFactory.Object);
          //  _surveyQuestionFactory.WithAnswer(_surveyQuestionAnswerFactory.ObjectList).Persist();

            int start = 0;
            int length = 10;
            string orderBy = "Question";
            string orderDir = "Asc";
            int status = SurveyQuestion.EntityStatus.Active;
            var obj = _surveyQuestionService.LoadSurveyQuestion(start, length, orderBy, orderDir, _question, status.ToString());
            Assert.Equal(obj.Count, 0);

        }

        [Fact]
        public void Should_Return_One()
        {
            _surveyQuestionFactory.Create().WithQuestionAndRank(_question, 10);
            _surveyQuestionAnswerFactory.CreateMore(2, _surveyQuestionFactory.Object);
            _surveyQuestionFactory.WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList()).Persist();
            int start = 0;
            int length = 10;
            string orderBy = "Question";
            string orderDir = "Asc";
            int status = SurveyQuestion.EntityStatus.Active;
            var obj = _surveyQuestionService.LoadSurveyQuestion(start, length, orderBy, orderDir, _question, status.ToString());
            Assert.Equal(obj.Count, 1);
        }

        [Fact]
        public void Should_Return_Top_Ten()
        {
            _surveyQuestionAnswerFactory.CreateMore(30, null);
            _surveyQuestionFactory.CreateMore(15, _surveyQuestionAnswerFactory.GetLastCreatedObjectList(), _question).Persist();
            int start = 0;
            int length = 10;
            string orderBy = "Question";
            string orderDir = "Asc";
            int status = SurveyQuestion.EntityStatus.Active;
            var obj = _surveyQuestionService.LoadSurveyQuestion(start, length, orderBy, orderDir, _question, status.ToString());
            Assert.Equal(obj.Count, 10);
        }
    }
}
