﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.UserAuth
{
    public interface IAreaControllersService : IBaseService
    {
        #region Operational Function
        void Save(AreaControllers erpAreaControllers);
        bool Update(long id, AreaControllers erpAreaControllers);
        bool Delete(long id, AreaControllers areaControllersObj);
        #endregion

        #region Single Instances Loading Function
        AreaControllers LoadById(long id);
        AreaControllers LoadByAreaController(string area, string controller);
        AreaControllers LoadByControllerName(string controller);
        #endregion

        #region List Loading Function
        IList<AreaControllers> LoadActive();
        IList<AreaControllers> LoadLike(string name);
        IList<AreaControllers> LoadArea();
        IList<AreaControllers> LoadControllersByArea(string area);
        IList<AreaControllers> LoadActive(int start, int length, string orderBy, string orderDir, string name, string area, string status, string rank);      
        #endregion

        #region Others Function
        int GetMaxRank(AreaControllers areaControllersObj);
        bool CheckDataByAreaController(string aName, string name);
        int AreaControllersRowCount(string name, string area, string status, string rank);
        #endregion
    }  
    public class AreaControllersService : BaseService, IAreaControllersService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UserAuthService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IAreaControllersDao _areacontrollersDao;
        public AreaControllersService(ISession session)
        {
            Session = session;
            _areacontrollersDao = new AreaControllersDao() { Session = Session };
        }
        #endregion

        #region Operational Function
        private void CheckBeforeSave(AreaControllers erpControllers)
        {
            //check duplicate name
            CheckDuplicateName(erpControllers);
            
        }

        private void CheckDuplicateName(AreaControllers erpControllers)
        {
            if (_areacontrollersDao.HasDuplicateByName(erpControllers.Name) == true)
                throw new DuplicateEntryException("Duplicate Controllers found");
        }

        public void Save(AreaControllers erpControllers)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {

                    CheckEmptyField(erpControllers);
                    CheckBeforeSave(erpControllers);
                    erpControllers.Rank = _areacontrollersDao.GetMaximumRank(erpControllers) + 1;
                    _areacontrollersDao.Save(erpControllers);
                    transaction.Commit();
                }

            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (ArgumentNullException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }


        }
        private void CheckBeforeUpdate(AreaControllers erpSession)
        {
            //check duplicate
            if (_areacontrollersDao.HasDuplicateByName(erpSession.Name, erpSession.Id) == false)
                throw new DuplicateEntryException("Duplicate controllers found");


        }

        public bool Update(long id, AreaControllers controllersObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    if (controllersObj == null) throw new ArgumentNullException("erpControllers");
                    var tempAreaControllers = _areacontrollersDao.LoadById(id);
                    tempAreaControllers.Id = id;
                    tempAreaControllers.Name = controllersObj.Name;
                    tempAreaControllers.Area = controllersObj.Area;
                    CheckEmptyField(tempAreaControllers);
                    CheckBeforeUpdate(tempAreaControllers);
                    _areacontrollersDao.Update(tempAreaControllers);
                    transaction.Commit();
                    return true;
                }
            }
            catch (ArgumentNullException ex)
            {
                throw;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();

            }
            return false;
        }
        public bool Delete(long id, AreaControllers controllersObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    var tempControllers = _areacontrollersDao.LoadById(id);
                    controllersObj.Id = tempControllers.Id;
                    controllersObj.Name = tempControllers.Name;
                    controllersObj.Area = tempControllers.Area;
                    tempControllers.Status = AreaControllers.EntityStatus.Delete;
                    _areacontrollersDao.Delete(controllersObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
            return false;
        }
        #endregion

        #region Single Instances Loading Function
        public AreaControllers LoadByAreaController(string area, string controller)
        {
            try
            {
                return _areacontrollersDao.LoadByAreaController(area, controller);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public AreaControllers LoadById(long id)
        {
            try
            {
                return _areacontrollersDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public AreaControllers LoadByControllerName(string controller)
        {
            try
            {
                return _areacontrollersDao.LoadByControllerName(controller);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        #endregion

        #region List Loading Function
        public IList<AreaControllers> LoadActive()
        {
            try
            {
                return _areacontrollersDao.LoadActive();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public IList<AreaControllers> LoadArea()
        {
            try
            {
                return _areacontrollersDao.LoadArea();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public IList<AreaControllers> LoadControllersByArea(string area)
        {
            try
            {
                return _areacontrollersDao.LoadControllersByArea(area);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public IList<AreaControllers> LoadLike(string name)
        {
            try
            {
                return _areacontrollersDao.LoadLike(name);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<AreaControllers> LoadActive(int start, int length, string orderBy, string orderDir, string name, string area, string status, string rank)
        {
            try
            {
                return _areacontrollersDao.LoadActive(start, length, orderBy, orderDir, name, area, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        #endregion

        #region Others Function
        public bool CheckDataByAreaController(string aName, string name)
        {
            try
            {
                return _areacontrollersDao.CheckDataByAreaController(aName, name);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public int GetMaxRank(AreaControllers controllersObj)
        {
            try
            {
                return _areacontrollersDao.GetMaximumRank(controllersObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        private void CheckEmptyField(AreaControllers erpControllers)
        {
            if (erpControllers == null)
            {
                throw new ArgumentNullException("erpControllers");
            }
            if (string.IsNullOrEmpty(erpControllers.Name) || string.IsNullOrEmpty(erpControllers.Area))
                throw new EmptyFieldException("AreaControllers name/area name in empty");

        }
        public int AreaControllersRowCount(string name, String area, string status, string rank)
        {
            try
            {
                return _areacontrollersDao.AreaControllersRowCount(name, area, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        #endregion
    }
}
