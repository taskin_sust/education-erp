﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UdvashERP.Areas.Student.Models
{
    public class VisitedStudentSearch
    {
        [Required]
        public virtual string Class { get; set; }
        [Required]
        public virtual string Year { get; set; }
        [Display(Name = "Visit Type")]
        public virtual string StudentVisitType { get; set; }
        [Display(Name = "User List")]
        public virtual string UserList { get; set; }
        [Display(Name = "Interested Organization")]
        public virtual string InterestedOrganization { get; set; }
        [Display(Name = "Interested Branch")]
        public long[] InterestedBranch { get; set; }
        [Required]
        public string[] InformationViewList { get; set; }



        [Display(Name = "Student Type")]
        public int StudentType { get; set; }
        [Display(Name = "Organization")]
        public long OrganizationId { get; set; }
        [Display(Name = "Program")]
        public long ProgramId { get; set; }
        [Display(Name = "Session")]
        public long SessionId { get; set; }
        [Display(Name = "Branch")]
        public long[] BranchIds { get; set; }
        [Display(Name = "User List")]
        public long[] UserIds { get; set; }
    }
}