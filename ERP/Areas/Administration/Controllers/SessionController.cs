﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessRules;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class SessionController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly ISessionService _sessionService;
        private readonly ICommonHelper _commonHelper;
        private readonly IUserService _userService;

        public SessionController()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            _sessionService = new SessionService(nHsession);
            _userService = new UserService(nHsession);
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Index/Manage Page
        public ActionResult Index()
        {
            try
            {
                GetStatusText();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                GetStatusText();
                return View();
            }
        }

        #region Render Data Table
        public JsonResult SessionList(int draw, int start, int length, string name, string code, string rank, string status)
        {
            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "Code";
                            break;
                        case "2":
                            orderBy = "Rank";
                            break;
                        case "3":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                int recordsTotal = _sessionService.SessionRowCount(name, code, rank, status);
                long recordsFiltered = recordsTotal;
                List<Session> sessionList = _sessionService.LoadSession(start, length, orderBy, orderDir.ToUpper(), name, code, rank, status).ToList();
                var obj = new Session();
                var maxMinRank = new int[2];
                maxMinRank[0] = _sessionService.GetMinimumRank(obj);
                maxMinRank[1] = _sessionService.GetMaximumRank(obj);
                foreach (var session in sessionList)
                {
                    var str = new List<string> { session.Name, session.Code };
                    var rankbtn = session.Rank.ToString();
                    if (session.Rank > maxMinRank[0])
                        rankbtn += "<a href='#'  id='" + session.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                    if (session.Rank < maxMinRank[1])
                        rankbtn += "<a href='#'  id='" + session.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                    str.Add(rankbtn);
                    str.Add(StatusTypeText.GetStatusText(session.Status));
                    str.Add(LinkGenerator.GetGeneratedLink("Details", "Edit", "Delete", "Session", session.Id, session.Name));
                    getData.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData,
                    isSuccess = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }
        #endregion

        #endregion

        #region Operational Function

        #region Save Operation
        public ActionResult Create(string message)
        {
            ViewBag.SuccessMessage = message;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Session sessionObj)
        {
            try
            {
                bool isSuccess = _sessionService.Save(sessionObj);
                if (isSuccess)
                    return RedirectToAction("Create", new { message = "Session successfully saved." });
            }
            catch (EmptyFieldException ex)
            {
                //_logger.Error("EmptyFieldException",ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (DuplicateEntryException ex)
            {
                // _logger.Error("DuplicateEntryException", ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error("UnknownException", ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                return View();
            }
            return View();
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(long id)
        {
            try
            {
                var sessionObj = _sessionService.LoadById(id);
                GetStatusText();
                return View(sessionObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                GetStatusText();
                return View();
            }
        }

        [HttpPost]
        public ActionResult Edit(long id, Session sessionObj)
        {
            try
            {
                bool isSuccess = _sessionService.Update(id, sessionObj);
                if (isSuccess)
                    ViewBag.SuccessMessage = "Session successfully updated.";
                else
                    ViewBag.ErrorMessage = "Problem Occurred, during session update.";
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
            }
            var initializeSessionObj = InitializeUpdateView(id);
            return View(initializeSessionObj);
        }

        #region Initialize Update View
        private Session InitializeUpdateView(long id)
        {
            GetStatusText();
            var sessionObj = _sessionService.LoadById(id);
            return sessionObj;
        }

        #endregion

        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isSuccess = _sessionService.Delete(id);
                return Json(isSuccess ? new Response(true, "Session sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Session Delete Fail !"));
            }
        }
        #endregion

        #region Details Operation

        public ActionResult Details(int id)
        {
            var sessionObj = _sessionService.LoadById(id);
            if (sessionObj != null)
            {
                sessionObj.CreateByText = _userService.GetUserNameByAspNetUserId(sessionObj.CreateBy);
                sessionObj.ModifyByText = _userService.GetUserNameByAspNetUserId(sessionObj.ModifyBy);
            }
            return View(sessionObj);
        }
        #endregion

        #region Rank Operation
        public JsonResult RankChange(long id, int current, string action)
        {
            try
            {
                var isSuccess = _sessionService.UpdateRank(id, current, action);
                return Json(isSuccess ? "Rank Changes" : "Something Wrong Occured!!");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Rank Changes, Something Wrong Occured!!"));
            }

        }
        #endregion

        #endregion

        #region Helper Function
        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
        }
        #endregion
    }
}
