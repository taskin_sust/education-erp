﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Xml.Serialization;
using System.IO;
using UdvashERP.FingerPrint.BusinessModel;
using UdvashERP.FingerPrint.ApiReference;
using System.Web.Script.Serialization;
using System.Net;
using UdvashERP.FingerPrint.BusinessRule;
using zkemkeeper;
using UdvashERP.FingerPrint.Helper;
using UdvashERP.FingerPrint.Service;

namespace UdvashERP.FingerPrint.FormUI
{
    public partial class ScanFinger : BaseForm
    {
        #region Propertise & Object Initialization

        AttendanceDeviceApiModel onlineDevice;
        private readonly ApiRequestResponse _apiRequestResponse;
        JsonResponse memberInfo;
        MemberInfo newMemberInfoObj;
        FingerPrintScannerService _fingerPrintScannerService;
        List<AttendanceDeviceApiModel> deviceTypeList;

        bool isDataFound = false;

        bool isRescanIndex0, isRescanIndex1, isRescanIndex2, isRescanIndex3, isRescanIndex4, isRescanIndex5, isRescanIndex6, isRescanIndex7, isRescanIndex8, isRescanIndex9 = false;

        public ScanFinger()
        {
            InitializeComponent();

            _apiRequestResponse = new ApiRequestResponse();
            onlineDevice = new AttendanceDeviceApiModel();
            newMemberInfoObj = new MemberInfo();

            _fingerPrintScannerService = new FingerPrintScannerService();
            btnD0.Enabled = btnD1.Enabled = btnD2.Enabled = btnD3.Enabled = btnD4.Enabled = btnD5.Enabled = btnD6.Enabled = btnD7.Enabled = btnD8.Enabled = btnD9.Enabled = false;
            btnD0.Visible = btnD1.Visible = btnD2.Visible = btnD3.Visible = btnD4.Visible = btnD5.Visible = btnD6.Visible = btnD7.Visible = btnD8.Visible = btnD9.Visible = false;

        }

        private void ScanFinger_Load(object sender, EventArgs e)
        {
            GetFingerIndex();
        }


        #endregion

        #region Operational Function

        #region 0_Index

        private void btnD0_Click(object sender, EventArgs e)
        {
            //Cursor = Cursors.WaitCursor;
            //int fingerIndex = 0;
            //ScanFingerTemplate(fingerIndex, isRescanIndex0);
            //Cursor = Cursors.Default;

            int fingerIndex = 0;
            if (memberInfo == null)
            {
                MessageBox.Show("Team Member Not Found.");
                return;
            }

            AttendanceDeviceApiModel onlineDevice = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (onlineDevice == null)
            {
                MessageBox.Show("Device Not Selected.");
                return;
            }

            Device offlineSevice = DeviceHelper.GetDeviceInfo(onlineDevice.ModelName);
            if (offlineSevice == null)
            {
                MessageBox.Show("Device Not In The Configuration File.");
                return;
            }

            _fingerPrintScannerService.Scanning(memberInfo, onlineDevice, offlineSevice, fingerIndex);
            lueDeviceType_EditValueChanged(null, null);
        }


        #endregion

        #region 1_Index

        private void btnD1_Click(object sender, EventArgs e)
        {
            //Cursor = Cursors.WaitCursor;
            //int fingerIndex = 1;
            //ScanFingerTemplate(fingerIndex, isRescanIndex1);
            //Cursor = Cursors.Default;

            int fingerIndex = 1;
            if (memberInfo == null)
            {
                MessageBox.Show("Team Member Not Found.");
                return;
            }

            AttendanceDeviceApiModel onlineDevice = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (onlineDevice == null)
            {
                MessageBox.Show("Device Not Selected.");
                return;
            }

            Device offlineSevice = DeviceHelper.GetDeviceInfo(onlineDevice.ModelName);
            if (offlineSevice == null)
            {
                MessageBox.Show("Device Not In The Configuration File.");
                return;
            }

            _fingerPrintScannerService.Scanning(memberInfo, onlineDevice, offlineSevice, fingerIndex);
            lueDeviceType_EditValueChanged(null, null);
        }


        #endregion

        #region 2_Index

        private void btnD2_Click(object sender, EventArgs e)
        {
            //Cursor = Cursors.WaitCursor;
            //int fingerIndex = 2;
            //ScanFingerTemplate(fingerIndex, isRescanIndex2);
            //Cursor = Cursors.Default;

            int fingerIndex = 2;
            if (memberInfo == null)
            {
                MessageBox.Show("Team Member Not Found.");
                return;
            }

            AttendanceDeviceApiModel onlineDevice = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (onlineDevice == null)
            {
                MessageBox.Show("Device Not Selected.");
                return;
            }

            Device offlineSevice = DeviceHelper.GetDeviceInfo(onlineDevice.ModelName);
            if (offlineSevice == null)
            {
                MessageBox.Show("Device Not In The Configuration File.");
                return;
            }

            _fingerPrintScannerService.Scanning(memberInfo, onlineDevice, offlineSevice, fingerIndex);
            lueDeviceType_EditValueChanged(null, null);
        }


        #endregion

        #region 3_Index

        private void btnD3_Click(object sender, EventArgs e)
        {
            //Cursor = Cursors.WaitCursor;
            //int fingerIndex = 3;
            //ScanFingerTemplate(fingerIndex,isRescanIndex3);
            //Cursor = Cursors.Default;

            int fingerIndex = 3;
            if (memberInfo == null)
            {
                MessageBox.Show("Team Member Not Found.");
                return;
            }

            AttendanceDeviceApiModel onlineDevice = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (onlineDevice == null)
            {
                MessageBox.Show("Device Not Selected.");
                return;
            }

            Device offlineSevice = DeviceHelper.GetDeviceInfo(onlineDevice.ModelName);
            if (offlineSevice == null)
            {
                MessageBox.Show("Device Not In The Configuration File.");
                return;
            }

            _fingerPrintScannerService.Scanning(memberInfo, onlineDevice, offlineSevice, fingerIndex);
            lueDeviceType_EditValueChanged(null, null);
        }



        #endregion

        #region 4_Index

        private void btnD4_Click(object sender, EventArgs e)
        {
            //Cursor = Cursors.WaitCursor;
            //int fingerIndex = 4;
            //ScanFingerTemplate(fingerIndex, isRescanIndex4);
            //Cursor = Cursors.Default;
            ////ChangeButtonAppearance(btnD4, fingerIndex, true);

            int fingerIndex = 4;
            if (memberInfo == null || memberInfo.Data==null)
            {
                MessageBox.Show("Team Member Not Found.");
                return;
            }

            AttendanceDeviceApiModel onlineDevice = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (onlineDevice == null)
            {
                MessageBox.Show("Device Not Selected.");
                return;
            }

            Device offlineSevice = DeviceHelper.GetDeviceInfo(onlineDevice.ModelName);
            if (offlineSevice == null)
            {
                MessageBox.Show("Device Not In The Configuration File.");
                return;
            }

            _fingerPrintScannerService.Scanning(memberInfo, onlineDevice, offlineSevice, fingerIndex);
            lueDeviceType_EditValueChanged(null, null);
        }

        #endregion

        #region 5_Index

        private void btnD5_Click(object sender, EventArgs e)
        {
            //Cursor = Cursors.WaitCursor;
            //int fingerIndex = 5;
            //ScanFingerTemplate(fingerIndex, isRescanIndex5);
            //Cursor = Cursors.Default;
            int fingerIndex = 5;
            if (memberInfo == null)
            {
                MessageBox.Show("Team Member Not Found.");
                return;
            }

            AttendanceDeviceApiModel onlineDevice = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (onlineDevice == null)
            {
                MessageBox.Show("Device Not Selected.");
                return;
            }

            Device offlineSevice = DeviceHelper.GetDeviceInfo(onlineDevice.ModelName);
            if (offlineSevice == null)
            {
                MessageBox.Show("Device Not In The Configuration File.");
                return;
            }


            _fingerPrintScannerService.Scanning(memberInfo, onlineDevice, offlineSevice, fingerIndex);
            lueDeviceType_EditValueChanged(null, null);
        }


        #endregion

        #region 6_Index

        private void btnD6_Click(object sender, EventArgs e)
        {
            //Cursor = Cursors.WaitCursor;
            //int fingerIndex = 6;
            //ScanFingerTemplate(fingerIndex, isRescanIndex6);
            //Cursor = Cursors.Default;

            int fingerIndex = 6;
            if (memberInfo == null)
            {
                MessageBox.Show("Team Member Not Found.");
                return;
            }

            AttendanceDeviceApiModel onlineDevice = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (onlineDevice == null)
            {
                MessageBox.Show("Device Not Selected.");
                return;
            }

            Device offlineSevice = DeviceHelper.GetDeviceInfo(onlineDevice.ModelName);
            if (offlineSevice == null)
            {
                MessageBox.Show("Device Not In The Configuration File.");
                return;
            }


            _fingerPrintScannerService.Scanning(memberInfo, onlineDevice, offlineSevice, fingerIndex);
            lueDeviceType_EditValueChanged(null, null);
        }



        #endregion

        #region 7_Index

        private void btnD7_Click(object sender, EventArgs e)
        {
            //Cursor = Cursors.WaitCursor;
            //int fingerIndex = 7;
            //ScanFingerTemplate(fingerIndex,isRescanIndex7);
            //Cursor = Cursors.Default;

            int fingerIndex = 7;
            if (memberInfo == null)
            {
                MessageBox.Show("Team Member Not Found.");
                return;
            }

            AttendanceDeviceApiModel onlineDevice = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (onlineDevice == null)
            {
                MessageBox.Show("Device Not Selected.");
                return;
            }

            Device offlineSevice = DeviceHelper.GetDeviceInfo(onlineDevice.ModelName);
            if (offlineSevice == null)
            {
                MessageBox.Show("Device Not In The Configuration File.");
                return;
            }

            _fingerPrintScannerService.Scanning(memberInfo, onlineDevice, offlineSevice, fingerIndex);
            lueDeviceType_EditValueChanged(null, null);
        }


        #endregion

        #region 8_Index

        private void btnD8_Click(object sender, EventArgs e)
        {
            //Cursor = Cursors.WaitCursor;
            //int fingerIndex = 8;
            //ScanFingerTemplate(fingerIndex,isRescanIndex8);
            //Cursor = Cursors.Default;     

            int fingerIndex = 8;
            if (memberInfo == null)
            {
                MessageBox.Show("Team Member Not Found.");
                return;
            }

            AttendanceDeviceApiModel onlineDevice = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (onlineDevice == null)
            {
                MessageBox.Show("Device Not Selected.");
                return;
            }

            Device offlineSevice = DeviceHelper.GetDeviceInfo(onlineDevice.ModelName);
            if (offlineSevice == null)
            {
                MessageBox.Show("Device Not In The Configuration File.");
                return;
            }

            _fingerPrintScannerService.Scanning(memberInfo, onlineDevice, offlineSevice, fingerIndex);
            lueDeviceType_EditValueChanged(null, null);
        }



        #endregion

        #region 9_Index

        private void btnD9_Click(object sender, EventArgs e)
        {
            //Cursor = Cursors.WaitCursor;
            //int fingerIndex = 9;
            //ScanFingerTemplate(fingerIndex,isRescanIndex9);
            //Cursor = Cursors.Default;         

            int fingerIndex = 9;
            if (memberInfo == null)
            {
                MessageBox.Show("Team Member Not Found.");
                return;
            }

            AttendanceDeviceApiModel onlineDevice = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (onlineDevice == null)
            {
                MessageBox.Show("Device Not Selected.");
                return;
            }

            Device offlineSevice = DeviceHelper.GetDeviceInfo(onlineDevice.ModelName);
            if (offlineSevice == null)
            {
                MessageBox.Show("Device Not In The Configuration File.");
                return;
            }

            _fingerPrintScannerService.Scanning(memberInfo, onlineDevice, offlineSevice, fingerIndex);
            lueDeviceType_EditValueChanged(null, null);
        }



        #endregion

        #endregion

        #region Others Function

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        #endregion

        #region Helper Function

        private void DrawDropDownForDeviceType(JsonResponse memberInfo)
        {
            deviceTypeList = new List<AttendanceDeviceApiModel>();
            //deviceTypeBindingSource.DataSource = deviceTypeList;
            AttendanceDeviceApiModel deviceType = new AttendanceDeviceApiModel();
            deviceType.ModelName = "Select a Device Type";
            deviceType.ModelId = 0;
            deviceType.Index0 = deviceType.Index1 = deviceType.Index2 = deviceType.Index3 = deviceType.Index4 = deviceType.Index5 = deviceType.Index6 = deviceType.Index7 = deviceType.Index8 = deviceType.Index9 = null;
            //memberInfo.Data.AttendanceDeviceApiModel.Insert(0, deviceType);
            deviceTypeList.Insert(0, deviceType);

            foreach (var device in memberInfo.Data.AttendanceDeviceApiModel)
            {
                if (device.IsFinger)
                    deviceTypeList.Add(device);
            }

            deviceTypeBindingSource.DataSource = deviceTypeList;
        }



        private void lueDeviceType_EditValueChanged(object sender, EventArgs e)
        {
            var x = lueDeviceType.GetSelectedDataRow();

            var deviceType = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            ChangeButtonDefaultAppearance();
            if (deviceType == null)
            {
                ChangeButtonDefaultAppearance();
                return;
            }

            if (deviceType.ModelId == 0)
            {
                ChangeButtonDefaultAppearance();
                return;
            }

            foreach (var eDevice in memberInfo.Data.AttendanceDeviceApiModel)
            {
                if (eDevice.ModelId == deviceType.ModelId)
                {
                    if (!String.IsNullOrEmpty(eDevice.Index0))
                    {
                        isRescanIndex0 = true;
                        btnD0.ForeColor = Color.Green;
                        btnD0.BackgroundImage = Properties.Resources._01;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index1))
                    {
                        isRescanIndex1 = true;
                        btnD1.ForeColor = Color.Green;
                        btnD1.BackgroundImage = Properties.Resources._02;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index2))
                    {
                        isRescanIndex2 = true;
                        btnD2.ForeColor = Color.Green;
                        btnD2.BackgroundImage = Properties.Resources._03;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index3))
                    {
                        isRescanIndex3 = true;
                        btnD3.ForeColor = Color.Green;
                        btnD3.BackgroundImage = Properties.Resources._04;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index4))
                    {
                        isRescanIndex4 = true;
                        btnD4.ForeColor = Color.Green;
                        btnD4.BackgroundImage = Properties.Resources._05;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index5))
                    {
                        isRescanIndex5 = true;
                        btnD5.ForeColor = Color.Green;
                        btnD5.BackgroundImage = Properties.Resources._06;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index6))
                    {
                        isRescanIndex6 = true;
                        btnD6.ForeColor = Color.Green;
                        btnD6.BackgroundImage = Properties.Resources._07;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index7))
                    {
                        isRescanIndex7 = true;
                        btnD7.ForeColor = Color.Green;
                        btnD7.BackgroundImage = Properties.Resources._08;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index8))
                    {
                        isRescanIndex8 = true;
                        btnD8.ForeColor = Color.Green;
                        btnD8.BackgroundImage = Properties.Resources._09;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index9))
                    {
                        isRescanIndex9 = true;
                        btnD9.ForeColor = Color.Green;
                        btnD9.BackgroundImage = Properties.Resources._10;
                    }
                }
                else
                {
                    //eDevice.Index1 = null;
                    if (!String.IsNullOrEmpty(eDevice.Index0))
                    {
                        btnD0.ForeColor = Color.Black;
                        btnD0.BackgroundImage = Properties.Resources._1D;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index1))
                    {
                        btnD1.ForeColor = Color.Black;
                        btnD1.BackgroundImage = Properties.Resources._2D;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index2))
                    {
                        btnD2.ForeColor = Color.Black;
                        btnD2.BackgroundImage = Properties.Resources._3D;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index3))
                    {
                        btnD3.ForeColor = Color.Black;
                        btnD3.BackgroundImage = Properties.Resources._4D;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index4))
                    {
                        btnD4.ForeColor = Color.Black;
                        btnD4.BackgroundImage = Properties.Resources._5D;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index5))
                    {
                        btnD5.ForeColor = Color.Black;
                        btnD5.BackgroundImage = Properties.Resources._6D;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index6))
                    {
                        btnD6.ForeColor = Color.Black;
                        btnD6.BackgroundImage = Properties.Resources._7D;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index7))
                    {
                        btnD7.ForeColor = Color.Black;
                        btnD7.BackgroundImage = Properties.Resources._8D;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index8))
                    {
                        btnD8.ForeColor = Color.Black;
                        btnD8.BackgroundImage = Properties.Resources._9D;
                    }
                    if (!String.IsNullOrEmpty(eDevice.Index9))
                    {
                        btnD9.ForeColor = Color.Black;
                        btnD9.BackgroundImage = Properties.Resources._10D;
                    }
                }
            }

        }

        private void ChangeButtonDefaultAppearance()
        {
            btnD0.ForeColor = Color.Black;
            btnD0.BackgroundImage = Properties.Resources._1D;

            btnD1.ForeColor = Color.Black;
            btnD1.BackgroundImage = Properties.Resources._2D;

            btnD2.ForeColor = Color.Black;
            btnD2.BackgroundImage = Properties.Resources._3D;

            btnD3.ForeColor = Color.Black;
            btnD3.BackgroundImage = Properties.Resources._4D;

            btnD4.ForeColor = Color.Black;
            btnD4.BackgroundImage = Properties.Resources._5D;

            btnD5.ForeColor = Color.Black;
            btnD5.BackgroundImage = Properties.Resources._6D;

            btnD6.ForeColor = Color.Black;
            btnD6.BackgroundImage = Properties.Resources._7D;

            btnD7.ForeColor = Color.Black;
            btnD7.BackgroundImage = Properties.Resources._8D;

            btnD8.ForeColor = Color.Black;
            btnD8.BackgroundImage = Properties.Resources._9D;

            btnD9.ForeColor = Color.Black;
            btnD9.BackgroundImage = Properties.Resources._10D;
        }

        private void ChangeButtonAppearance(Button btn, int fingerIndex, bool isDefault = false)
        {
            if (isDefault)
            {
                btn.ForeColor = Color.Black;
                if (fingerIndex == 0)
                    btn.BackgroundImage = Properties.Resources._1D;
                if (fingerIndex == 1)
                    btn.BackgroundImage = Properties.Resources._2D;
                if (fingerIndex == 2)
                    btn.BackgroundImage = Properties.Resources._3D;
                if (fingerIndex == 3)
                    btn.BackgroundImage = Properties.Resources._4D;
                if (fingerIndex == 4)
                    btn.BackgroundImage = Properties.Resources._5D;
                if (fingerIndex == 5)
                    btn.BackgroundImage = Properties.Resources._6D;
                if (fingerIndex == 6)
                    btn.BackgroundImage = Properties.Resources._7D;
                if (fingerIndex == 7)
                    btn.BackgroundImage = Properties.Resources._8D;
                if (fingerIndex == 8)
                    btn.BackgroundImage = Properties.Resources._9D;
                if (fingerIndex == 9)
                    btn.BackgroundImage = Properties.Resources._10D;
            }
            else
            {
                btn.ForeColor = Color.Green;
                if (fingerIndex == 0)
                    btn.BackgroundImage = Properties.Resources._01;
                if (fingerIndex == 1)
                    btn.BackgroundImage = Properties.Resources._02;
                if (fingerIndex == 2)
                    btn.BackgroundImage = Properties.Resources._03;
                if (fingerIndex == 3)
                    btn.BackgroundImage = Properties.Resources._04;
                if (fingerIndex == 4)
                    btn.BackgroundImage = Properties.Resources._05;
                if (fingerIndex == 5)
                    btn.BackgroundImage = Properties.Resources._06;
                if (fingerIndex == 6)
                    btn.BackgroundImage = Properties.Resources._07;
                if (fingerIndex == 7)
                    btn.BackgroundImage = Properties.Resources._08;
                if (fingerIndex == 8)
                    btn.BackgroundImage = Properties.Resources._09;
                if (fingerIndex == 9)
                    btn.BackgroundImage = Properties.Resources._10D;
            }
        }

        private void GetFingerIndex()
        {
            btnD0.Enabled = btnD0.Visible = Boolean.Parse(ConfigHelper.GetString(FingerPrintConstant.GetIndex0));
            btnD1.Enabled = btnD1.Visible = Boolean.Parse(ConfigHelper.GetString(FingerPrintConstant.GetIndex1));
            btnD2.Enabled = btnD2.Visible = Boolean.Parse(ConfigHelper.GetString(FingerPrintConstant.GetIndex2));
            btnD3.Enabled = btnD3.Visible = Boolean.Parse(ConfigHelper.GetString(FingerPrintConstant.GetIndex3));
            btnD4.Enabled = btnD4.Visible = Boolean.Parse(ConfigHelper.GetString(FingerPrintConstant.GetIndex4));
            btnD5.Enabled = btnD5.Visible = Boolean.Parse(ConfigHelper.GetString(FingerPrintConstant.GetIndex5));
            btnD6.Enabled = btnD6.Visible = Boolean.Parse(ConfigHelper.GetString(FingerPrintConstant.GetIndex6));
            btnD7.Enabled = btnD7.Visible = Boolean.Parse(ConfigHelper.GetString(FingerPrintConstant.GetIndex7));
            btnD8.Enabled = btnD8.Visible = Boolean.Parse(ConfigHelper.GetString(FingerPrintConstant.GetIndex8));
            btnD9.Enabled = btnD9.Visible = Boolean.Parse(ConfigHelper.GetString(FingerPrintConstant.GetIndex9));
        }

        #endregion

        #region Online Operation

        private void btnRetriveOnlineData_Click(object sender, EventArgs e)
        {
            try
            {
                isDataFound = false;
                ChangeButtonDefaultAppearance();
                lueDeviceType.Properties.NullText = "Select a Device Type";
                int teamMemberPin = 0;
                string enrollnumber = txtUserID.Text.Trim();

                Int32.TryParse(enrollnumber, out teamMemberPin);

                isRescanIndex0 = isRescanIndex1 = isRescanIndex2 = isRescanIndex3 = isRescanIndex4 = isRescanIndex5 = isRescanIndex6 = isRescanIndex7 = isRescanIndex8 = isRescanIndex9 = false;
                memberInfo = new JsonResponse();
                memberInfo = _apiRequestResponse.GetMemberInfo(teamMemberPin.ToString());

                if (memberInfo.IsSuccess)
                {

                    DrawDropDownForDeviceType(memberInfo);

                    lblName.Text = memberInfo.Data.Name;
                    lblOrganization.Text = memberInfo.Data.Organization;
                    lblBranch.Text = memberInfo.Data.Branch;
                    lblCampus.Text = memberInfo.Data.Campus;
                    lblDepartment.Text = memberInfo.Data.Department;
                    lblDesignation.Text = memberInfo.Data.Designation;
                    if (memberInfo.Data.Image != null)
                    {
                        pictureBox1.Image = byteArrayToImage(memberInfo.Data.Image);
                    }
                    else
                    {
                        pictureBox1.Image = Properties.Resources.noimagefound;
                    }

                    isDataFound = true;
                }
                else
                {
                    lblName.Text = "-";
                    lblOrganization.Text = "-";
                    lblBranch.Text = "-";
                    lblCampus.Text = "-";
                    lblDepartment.Text = "-";
                    lblDesignation.Text = "-";
                    pictureBox1.Image = Properties.Resources.noimagefound;
                    deviceTypeBindingSource.DataSource = DBNull.Value;

                }
                if (isDataFound)
                    lueDeviceType_EditValueChanged(null, null);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(this, ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSaveToOnline_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (memberInfo.Data == null)
            {
                MessageBox.Show("Team Member Not Found.");
                Cursor = Cursors.Default;
                return;
            }

            if (lueDeviceType.Text == string.Empty)
            {
                MessageBox.Show("Select a Device Type");
                Cursor = Cursors.Default;
                return;
            }

            var deviceApiModel = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (deviceApiModel == null)
            {
                MessageBox.Show("Select a Device Type");
                Cursor = Cursors.Default;
                return;
            }

            Device device = DeviceHelper.GetDeviceInfo(deviceApiModel.ModelName);
            if (device == null)
            {
                MessageBox.Show("Device Is Not Found, In The Configuration File.");
                Cursor = Cursors.Default;
                return;
            }

            string teamMemberPin = txtUserID.Text.Trim();

            //var xc=memberInfo.Data.AttendanceDeviceApiModel.Where(m => m.ModelId == deviceApiModel.ModelId).FirstOrDefault();

            var deviceList = memberInfo.Data.AttendanceDeviceApiModel.Where(x => x.IsFinger).ToList();

            memberInfo.Data.AttendanceDeviceApiModel.Clear();
            memberInfo.Data.AttendanceDeviceApiModel = deviceList;
            //newMemberInfoObj = new MemberInfo();

            //newMemberInfoObj = memberInfo.Data;
            //foreach (var item in memberInfo.Data.AttendanceDeviceApiModel)
            //{
            //    if (item.IsFinger)
            //    {
            //        newMemberInfoObj.AttendanceDeviceApiModel.Clear();
            //        newMemberInfoObj.AttendanceDeviceApiModel.Add(item);
            //    }
            //}

            var json = new JavaScriptSerializer().Serialize(memberInfo.Data);
            var param = new StringBuilder();
            param.Append("memberInfo=" + json);

            Response response = _apiRequestResponse.SetMemberFingerPrint(param.ToString());

            if (response.IsSuccess)
            {
                MessageBox.Show("Data Successfully Saved.");
            }
            else
            {
                MessageBox.Show(response.Data);
            }
            Cursor = Cursors.Default;
        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            int fingerIndex = 5;
            if (memberInfo == null)
            {
                MessageBox.Show("Team Member Not Found.");
                return;
            }

            AttendanceDeviceApiModel onlineDevice = (AttendanceDeviceApiModel)lueDeviceType.GetSelectedDataRow();
            if (onlineDevice == null)
            {
                MessageBox.Show("Device Not Selected.");
                return;
            }

            Device offlineSevice = DeviceHelper.GetDeviceInfo(onlineDevice.ModelName);
            if (offlineSevice == null)
            {
                MessageBox.Show("Device Not In The Configuration File.");
                return;
            }


            _fingerPrintScannerService.Scanning(memberInfo, onlineDevice, offlineSevice, fingerIndex);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            _fingerPrintScannerService.GetUserInfo();


        }

        //private void button1_Click_1(object sender, EventArgs e)
        //{

        //    _fingerPrintScannerService.GetUserInfo();


        //}

    }
}