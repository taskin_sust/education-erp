﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.AssignSupplierTest
{
    public class SupplierItemBaseTest : TestBase, IDisposable
    {
        #region Object Initiallization
        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal ISupplierItemService _SupplierItemService;
        
        internal SupplierItemFactory _SupplierItemFactory;
        internal SupplierFactory _SupplierFactory;
        internal ItemFactory _ItemFactory;
        #endregion

        public SupplierItemBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _SupplierItemService = new SupplierItemService(Session);
            _SupplierItemFactory = new SupplierItemFactory(null, Session);
            //_SupplierFactory = new SupplierFactory(this,session);
        }

        public void Dispose()
        {
            _SupplierItemFactory.CleanUp();
            base.Dispose();
        }

    }
}
