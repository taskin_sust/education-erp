﻿
$(document).ready(function () {

    validateDropDownField("Organization", "please select Organization");
    validateDropDownField("ItemGroup", "please select Itemgroup");

    $(document).on('change', '#Organization', function () {

        try {
            var orgId = $("#Organization").val();

            if (orgId.length <= 0) {
                $(".itemBlur").find(".control-label").addClass("inithide");
                $(".itemBlur").find(".col-md-4").addClass("inithide");
                $(".sol-container").remove();
                $(".checkbox-selected:checkbox").removeAttr('checked');
                $(".checkbox-select-all:checkbox").removeAttr('checked');
                $("#ItemGroup").firstChild("option").attr('selected');
            }

        } catch (e) {
            console.log(e);
        }
    });

    $(document).on('change', '#ItemGroup', function () {

        try {
            var orgId = $("#Organization").val();
            var itemGroupId = $("#ItemGroup").val();
            var url = $("body").attr("data-project-root") + "UInventory/CommonAjaxUInventory/LoadItemList";
            var reqType = "Post";
            var data = {  itemGroupIds: itemGroupId , organizationId: orgId};

            if (orgId.length <= 0) {
                window.showErrorMessageBelowCtrl("Organization", "", false);
                window.showErrorMessageBelowCtrl("Organization", "Please Select Organization", true);
                return;
            }
            if (itemGroupId.length <= 0) {
                window.showErrorMessageBelowCtrl("ItemGroup", "", false);
                window.showErrorMessageBelowCtrl("ItemGroup", "Please Select ItemGroup", true);
                return;
            }
            $.ajax({
                type: reqType,
                url: url,
                data: data,
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $(".control-label").removeClass("inithide");
                    $(".col-md-4").removeClass("inithide");
                    $(".sol-container").remove();

                    $('#my-select').searchableOptionList({
                        maxHeight: '250px',
                        data: function () {
                            var childrenary = [];
                            var jsonFormatAry = [];
                            var obj = {};
                            obj["type"] = "optiongroup";
                            var responseData = response.returnResult;
                            $.each(responseData, function (k, v) {
                                childrenary.push({ "type": "option", "value": v.Value, "label": v.Text });
                            });
                            obj["children"] = childrenary;
                            jsonFormatAry.push(obj);
                            return jsonFormatAry;
                        }
                    });
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.fn.customMessage({
                        displayMessage: window.AJAX_ERROR,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });

        } catch (e) {
            console.log(e);
        }
    });

    $(document).on('change', '.sol-checkbox', function () {

        try {
            var counter = 1;
            $(".checkbox-selected").each(function (index) {
                if ($(this).is(":checked")) {
                    console.log("first -->>" + counter);
                    $(this).attr("checked", false);
                }
            });
            var checkedValues = $('.sol-checkbox:checked').map(function () {
                return this.value;
            }).get();
            var url = $("body").attr("data-project-root") + "UInventory/Supplier/GetItemSupplier";
            var reqType = "Post";
            var data = { itemIds: checkedValues };
            $.ajax({
                type: reqType,
                url: url,
                data: data,
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $(".supplierFromServer").html(response);
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.fn.customMessage({
                        displayMessage: window.AJAX_ERROR,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        } catch (e) {
            console.log(e);
        }
    });

    function validationCheck(filedNameAry) {
        try {
            var isValid = true;
            $.each(filedNameAry, function (i, v) {
                if ($("#" + v).val().length <= 0) {
                    window.showErrorMessageBelowCtrl(v, "", false);
                    window.showErrorMessageBelowCtrl(v, "Please Select " + v, true);
                    isValid = false;
                    return false;
                }
            });
            return isValid;
        } catch (e) {
            console.log(e);
        }
    }

    $(document).on('click', '#submit', function () {

        try {
            var idAry = new Array("Organization", "ItemGroup");
            if (validationCheck(idAry) == true) {
                var checkeditmsIds = $('.sol-checkbox:checked').map(function () {
                    return this.value;
                }).get();
                var checkedSupplierIds = $('.checkbox-selected:checked').map(function () {
                    return this.value;
                }).get();
                var url = $("body").attr("data-project-root") + "UInventory/Supplier/AssignSupplier";
                var reqType = "Post";
                var data = { itemIds: checkeditmsIds, supplierIds: checkedSupplierIds };
                $.ajax({
                    type: reqType,
                    url: url,
                    data: data,
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (response) {
                        if (response.IsSuccess) {
                            $.fn.customMessage({
                                displayMessage: response.Message,
                                displayMessageType: "s",
                                displayTime: 120000,
                            });
                        } else {
                            $.fn.customMessage({
                                displayMessage: response.Message,
                                displayMessageType: "e"
                            });
                        }
                        $.unblockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (result) {
                        $.fn.customMessage({
                            displayMessage: window.AJAX_ERROR,
                            displayMessageType: "error",
                            displayTime: 5000
                        });
                    }
                });
            }
            //else {
            //    console.log("invalid request!!");
            //    $.fn.customMessage({
            //        displayMessage: "invalid request",
            //        displayMessageType: "e"
            //    });
            //}
        } catch (e) {
            console.log(e);
        }
    });


    $(".checkbox-select-all").change(function () {
        $(".checkbox-selected:checkbox").prop('checked', $(this).prop("checked"));
    });

});
