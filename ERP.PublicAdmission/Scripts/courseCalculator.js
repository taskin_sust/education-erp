
$(document).on('change', '#Program', function (event) {
    $("#Session option").remove();
    $('#Session').append("<option value=''>Select Session</option>");
    $("#CourseList").html("");
    
    var programId = $(this).val();
    if (programId != "" && (/^\d+$/.test(programId))) {
        $("#Program").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Program").removeClass("highlight");
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForSession",
            cache: false,
            async: true,
            data: { "programId": programId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $.each(result.returnList, function (i, v) {
                        $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
            }
        });
    }
    else {
        $("#Program").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Program field is required.");
        $("#Program").addClass("highlight");
    }
});

$(document).on('change', '#Session', function (event) {    
    var programId = $("#Program").val();
    var sessionId = $(this).val();
    if (sessionId != "" && (/^\d+$/.test(sessionId))) {
        $("#Session").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Session").removeClass("highlight");
    }
    else {
        $("#Session").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Session field is required.");
        $("#Session").addClass("highlight");
    }

    if (programId != "" && (/^\d+$/.test(programId)) && sessionId != "" && (/^\d+$/.test(sessionId))) {        
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForCourseInformationCalculator",
            cache: false,
            async: true,
            data: { "programId": programId, "sessionId": sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.hasOwnProperty("IsSuccess")) {

                } else {
                    $("#CourseList").html(result);
                    $("#displayPaymentInfo").val("Calculate");
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
            }
        });
    }
});
$(document).on('click', '.course-subject-check', function (event) {
    var totalChecked = $(this).parent().find(".course-subject-check:checked").length;
    if (totalChecked == 0) {
        $(this).parents(".panel-default").find(".course-name-check").prop('checked', false);
    }
    else {
        $(this).parents(".panel-default").find(".course-name-check").prop('checked', true);
    }

    var courseError = 0;
    var message1 = '';
    $(".course-name-check").each(function () {
        var indCourseError = 0;
        var totalChecked = $(this).parents(".panel-default").find(".course-subject-check:checked").length;
        var maxsubject = $(this).attr("data-maxsubject");
        var minsubject = $(this).attr("data-minsubject");
        var isCompulsary = $(this).attr("data-is-compulsary");
        var courseName = $(this).attr("data-course-name");
        console.log(minsubject);
        console.log(totalChecked);
        if ($(this).is(':checked')) {
            if (totalChecked > maxsubject) {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select maximum ' + maxsubject + ' subjects for course: ' + courseName + ' </div>';
                indCourseError = 1;
            }
            else if (totalChecked < minsubject) {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select minimum ' + minsubject + ' subjects for course: ' + courseName + '</div>';
                indCourseError = 1;
            }
        }
        else {
            if (isCompulsary == "True") {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> ' + courseName + ' is a Compulsary Course</div>';
                indCourseError = 1;
            }
        }

        if (indCourseError == 1)
            courseError = 1;

    });

    if (courseError == 1) {
        $("#courseError").show().html(message1);
    }
    else {
        var totalChecked = $(".course-name-check:checked").length;
        if (totalChecked == 0) {
            $("#courseError").show().html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You must select one or more Course </div>');
        }
        else {
            $("#courseError").empty().hide();
        }
    }
});
$(document).on('click', '.course-name-check', function (event) {
    event.stopPropagation();
    if ($(this).is(':checked')) {
        $(this).parents(".panel-default").find(".course-subject-check").each(function () {
            $(this).prop('checked', true);
        });
    }
    else {
        $(this).parents(".panel-default").find(".course-subject-check").each(function () {
            $(this).prop('checked', false);
        });
    }

    var courseError = 0;
    var message1 = '';
    $(".course-name-check").each(function () {
        var indCourseError = 0;
        var totalChecked = $(this).parents(".panel-default").find(".course-subject-check:checked").length;
        var maxsubject = $(this).attr("data-maxsubject");
        var minsubject = $(this).attr("data-minsubject");
        var isCompulsary = $(this).attr("data-is-compulsary");
        var courseName = $(this).attr("data-course-name");
        console.log(minsubject);
        console.log(totalChecked);
        if ($(this).is(':checked')) {
            if (totalChecked > maxsubject) {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select maximum ' + maxsubject + ' subjects for course: ' + courseName + ' </div>';
                indCourseError = 1;
            }
            else if (totalChecked < minsubject) {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select minimum ' + minsubject + ' subjects for course: ' + courseName + '</div>';
                indCourseError = 1;
            }
        }
        else {
            if (isCompulsary == "True") {
                message1 += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> ' + courseName + ' is a Compulsary Course</div>';
                indCourseError = 1;
            }
        }

        if (indCourseError == 1)
            courseError = 1;

    });

    if (courseError == 1) {
        $("#courseError").show().html(message1);
    }
    else {
        var totalChecked = $(".course-name-check:checked").length;
        if (totalChecked == 0) {
            $("#courseError").show().html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You must select one or more Course </div>');
        }
        else {
            $("#courseError").empty().hide();
        }
    }
});
$(document).on('click', '#displayPaymentInfo', function (event) {
    studentInfo = {};
    var isSuccess = 0;    
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();     
    if (programId == "") {
        $("#Program").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Program field is required.");
        $("#Program").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(programId))) {
        $("#Program").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Program field is required.");
        $("#Program").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Program").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Program").removeClass("highlight");
        //isSuccess = 1;
    }

    if (sessionId == "") {
        $("#Session").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Session field is required.");
        $("#Session").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else if (!(/^\d+$/.test(sessionId))) {
        $("#Session").parent().find("span").removeClass("field-validation-valid").addClass("field-validation-error").html("The Session field is required.");
        $("#Session").addClass("highlight");
        isSuccess = isSuccess + 1;
    }
    else {
        $("#Session").parent().find("span").removeClass("field-validation-error").addClass("field-validation-valid").html("");
        $("#Session").removeClass("highlight");

    }


    var courseError = 0;
    var courseList = [];
    var message = '';
    $(".course-name-check").each(function () {
        var indCourseError = 0;
        var totalChecked = $(this).parents(".panel-default").find(".course-subject-check:checked").length;
        var maxsubject = $(this).attr("data-maxsubject");
        var minsubject = $(this).attr("data-minsubject");
        var isCompulsary = $(this).attr("data-is-compulsary");
        var courseName = $(this).attr("data-course-name");
        var courseId = $(this).attr("data-course-id");
        console.log(minsubject);
        console.log(totalChecked);
        if ($(this).is(':checked')) {
            if (totalChecked > maxsubject) {
                message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select maximum ' + maxsubject + ' subjects for course: ' + courseName + ' </div>';
                indCourseError = 1;
            }
            else if (totalChecked < minsubject) {
                message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You can select minimum ' + minsubject + ' subjects for course: ' + courseName + '</div>';
                indCourseError = 1;
            }
            else {
                course = {};
                course["Id"] = courseId;
                course["Name"] = courseName;
                course["ProgramId"] = programId;
                course["SessionId"] = sessionId;
                course["MaxSubject"] = maxsubject;
                course["PublicMinSubject"] = minsubject;
                course["PublicMinPayment"] = 0;
                course["IsPublicCompulsary"] = "True";
                course["Id"] = courseId;
                var subjectList = [];
                $(this).parents(".panel-default").find(".course-subject-check").each(function () {
                    if ($(this).is(':checked')) {
                        subject = {};
                        subject["Id"] = $(this).attr("data-subject-id");
                        subject["Name"] = $(this).attr("data-subject-name");
                        subject["ShortName"] = $(this).attr("data-subject-shortname");
                        subjectList.push(subject);
                    }
                });
                course["SubjectViewModels"] = subjectList;
                courseList.push(course);
                indCourseError = 0;
            }

        }
        else {
            if (isCompulsary == "True") {
                message += '<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> ' + courseName + ' is a Compulsary Course</div>';
                indCourseError = 1;
            }
            else {
                indCourseError = 0;
            }
        }
        if (indCourseError == 1)
            courseError = 1;

    });

    if (courseError == 1) {
        $("#courseError").show().html(message);
        isSuccess = isSuccess + 1;
    }
    else {
        var totalChecked = $(".course-name-check:checked").length;
        if (totalChecked == 0) {
            $("#courseError").show().html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> You must select one or more Course </div>');
            isSuccess = isSuccess + 1;
        }
        else {
            $("#courseError").empty().hide();          
        }
    }


    if (isSuccess == 0) {
        studentPayment = {};
        studentPayment["OfferedDiscount"] = 0;
        studentPayment["NetReceivable"] = 0;
        studentPayment["CashBackAmount"] = 0;
        studentPayment["ReceivableAmount"] = 0;
        studentPayment["ConsiderationAmount"] = 0;
        studentPayment["PayableAmount"] = 0;
        studentPayment["DiscountAmount"] = 0;
        studentPayment["CourseFee"] = 0;
        studentPayment["ReceivedAmount"] = 0;
        studentPayment["PaymentMethod"] = 2;
        studentPayment["SpDiscountAmount"] = 0;
        studentPayment["ReferrerId"] = 0;
        studentPayment["Remarks"] = "";
        studentPayment["NextReceivedDate"] = "";
        studentPayment["NextRecDate"] = new Date();
        studentPayment["CourseViewModels"] = courseList;
        studentPayment["PaymentTransactionViewModels"] = null;
        studentPayment["MinPaymentAmount"] = 0;



        studentInfo = {};
        studentInfo["Id"] = 0;
        studentInfo["PrnNo"] = "";
        studentInfo["ErrorMessage"] = "";
        studentInfo["Name"] = "";
        studentInfo["MobNumber"] = "";
        studentInfo["VersionOfStudy"] = 0;
        studentInfo["Gender"] = 0;
        studentInfo["Religion"] = 0;
        studentInfo["Program"] = programId;
        studentInfo["Session"] = sessionId;
        studentInfo["Branch"] = 0;
        studentInfo["Campus"] = 0;
        studentInfo["BatchDays"] = 0;
        studentInfo["BatchTime"] = 0;
        studentInfo["Batch"] = 0;
        studentInfo["CourseViewModels"] = courseList;
        studentInfo["PaymentTransactionViewModel"] = null;
        studentInfo["StudentPayment"] = studentPayment;
        var studentInfostringify = JSON.stringify(studentInfo);
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Home/AjaxRequestForCourseCalculator",
            cache: false,
            async: true,
            dataType: "json",
            contentType: "application/json",
            data: studentInfostringify,
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.hasOwnProperty("IsSuccess")) {
                    $(".customMessage").html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">x</a><strong>Error!</strong> ' + result.returnList + '</div>');
                }
                else {
                    $("#paymentInfo").html(result);
                    $("#displayPaymentInfo").hide();
                    $("#newAdmissionForm input[type=text]").attr('readonly', 'readonly');
                    $("#newAdmissionForm select").attr('disabled', true);
                    $('#CouseCheckList input[type=checkbox]').attr('disabled', true);
                    $("#editStudentInfo").val("Calculate Again"); 
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                console.log(result);
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});

$(document).on("click", "#editStudentInfo", function (event) {
    $("#paymentInfo").html("");
    $("#displayPaymentInfo").show();
    $("#newAdmissionForm input[type=text]").removeAttr('readonly');
    $("#CouseCheckList input[type=checkbox]").removeAttr('disabled');
    $("#newAdmissionForm select").removeAttr('disabled');
});