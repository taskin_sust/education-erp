﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IServiceBlockDao : IBaseDao<ServiceBlock,long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        BusinessModel.Entity.Administration.ServiceBlock GetServiceBlock(long programId, long sessionId, int serviceType, int contentType);
        #endregion

        #region List Loading Function
        IList<ServiceBlock> LoadContentTypeList(long? proId, long? sessId, int? serviceTypeId);
        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        bool CheckedSelectedContentType(long programId, long sessionId, int serviceType, int contentType);
        #endregion

        #region Public API

        #endregion
        
        
    }

    public class ServiceBlockDao:BaseDao<ServiceBlock,long>,IServiceBlockDao
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public ServiceBlock GetServiceBlock(long programId, long sessionId, int serviceType, int contentType)
        {
            var row =
                Session.QueryOver<ServiceBlock>()
                    .Where(x => x.Program.Id == programId && x.Session.Id == sessionId && x.ServiceType == serviceType
                                && x.ConditionType == contentType).SingleOrDefault();
            return row;
        }

        public IList<ServiceBlock> LoadContentTypeList(long? proId, long? sessId, int? serviceTypeId)
        {
            ICriteria criteria = Session.CreateCriteria<ServiceBlock>().Add(Restrictions.Eq("Status", ServiceBlock.EntityStatus.Active));
            criteria.CreateAlias("Program", "pr").Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Session", "s").Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            if (proId!=null)
                criteria.Add(Restrictions.Eq("pr.Id", proId));
            if(sessId!=null)
            criteria.Add(Restrictions.Eq("s.Id", sessId));
            if(serviceTypeId!=null)
            criteria.Add(Restrictions.Eq("ServiceType", serviceTypeId));
            return criteria.List<ServiceBlock>();
        }
        #endregion

        #region Others Function
        public bool CheckedSelectedContentType(long programId, long sessionId, int serviceType, int contentType)
        {
            var rowList = Session.QueryOver<ServiceBlock>().Where(x => x.Program.Id == programId && x.Session.Id == sessionId && x.ServiceType == serviceType && x.ConditionType == contentType).RowCount();
            if (rowList < 1)
                return false;
            return true;
        }
        #endregion

        #region Helper Function

        #endregion

        #region Public API

        #endregion
       
    }
}
