﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions.Inspections;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class ExamProgressMap : BaseClassMap<ExamProgress, long>
    {
        public ExamProgressMap()
        {
            Table("ExamProgress");
            LazyLoad();
            Map(x => x.HeldDate);
            References(x => x.Exam).Column("ExamId");
            References(x => x.Batch).Column("BatchId");
            References(x => x.Course).Column("CourseId");
            
        }
    }
}
