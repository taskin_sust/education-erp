﻿using System.Collections.Generic;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Teachers
{
 
    public interface ITeacherSubjectPriorityDao : IBaseDao<TeacherSubjectPriority, long>
    {
        
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        IList<TeacherSubjectPriority> _teacherSubjectPriorityService(long id);
        #endregion

        #region Others Function
        #endregion
        
    }

    public class TeacherSubjectPriorityDao : BaseDao<TeacherSubjectPriority, long>, ITeacherSubjectPriorityDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        public IList<TeacherSubjectPriority> _teacherSubjectPriorityService(long id)
        {
            return
                Session.QueryOver<TeacherSubjectPriority>()
                    .Where(x => x.Teacher.Id == id && x.Status == TeacherSubjectPriority.EntityStatus.Active)
                    .List<TeacherSubjectPriority>();
        }
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion
    }
}
