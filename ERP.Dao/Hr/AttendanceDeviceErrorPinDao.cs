﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IAttendanceDeviceErrorPinDao : IBaseDao<AttendanceDeviceErrorPin, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function
        bool CheckDuplicate(int pin, long DeviceId);
        #endregion

        #region Helper Function

        #endregion

        
    }
    public class AttendanceDeviceErrorPinDao : BaseDao<AttendanceDeviceErrorPin, long>, IAttendanceDeviceErrorPinDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function        

        #endregion

        #region Others Function        
        public bool CheckDuplicate(int pin, long DeviceId)
        {
            var rowList = Session.QueryOver<AttendanceDeviceErrorPin>()
                    .Where(x => x.Pin == pin && x.AttendanceDevice.Id == DeviceId)
                    .RowCount();
            if (rowList < 1)
                return false;
            return true;
        }
        #endregion

        #region Helper Function

        #endregion
        
    }

}
