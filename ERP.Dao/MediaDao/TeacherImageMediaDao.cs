﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.MediaDao
{
    public interface ITeacherImageMediaDao : IBaseDao<TeacherMediaImage, long>
    {

        //void Save(BusinessModel.Entity.MediaDb.TeacherMediaImage mediaImage);

        //void Delete(BusinessModel.Entity.MediaDb.TeacherMediaImage mediaImage);

        IEnumerable<TeacherMediaImage> GetByHash(string hash);
        TeacherMediaImage GetByRefId(long teacherImageId);
    }
    //public class TeacherImageMediaDao : ITeacherImageMediaDao
    public class TeacherImageMediaDao : BaseDao<TeacherMediaImage, long>, ITeacherImageMediaDao
    {
        //private ISession Session = null;
        //public TeacherImageMediaDao(ISession session)
        //{
        //    Session = session;
        //}

        //public void Save(TeacherMediaImage mediaImage)
        //{
        //    //Session.Save(mediaImage);
        //    //Session.SaveOrUpdate(mediaImage);
        //    Session.Update(mediaImage);
        //}

        //public void Delete(TeacherMediaImage mediaImage)
        //{
        //    Session.Delete(mediaImage);
        //}

        public IEnumerable<TeacherMediaImage> GetByHash(string hash)
        {
            return Session.QueryOver<TeacherMediaImage>().Where(x => x.Hash == hash).List<TeacherMediaImage>().Take(1);
        }

        public TeacherMediaImage GetByRefId(long teacherImageId)
        {
            TeacherMediaImage teacherMediaImage =  Session.QueryOver<TeacherMediaImage>().Where(x => x.ImageRef == teacherImageId).OrderBy(x=>x.Id).Desc.List<TeacherMediaImage>().Take(1).SingleOrDefault();
            return teacherMediaImage;
        }
    }
}

