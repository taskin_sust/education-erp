﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.SubjectTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Subject")]
    public class SubjectLoadActiveTest : SubjectBaseTest
    {
        #region Basic Test
        [Fact]
        public void LoadSubject_should_return_list()
        {
            _subjectFactory.CreateMore(100).Persist();
            var subjectList = _subjectService.LoadSubject();
            Assert.True(subjectList.Count > 0);
        }
        [Fact]
        public void LoadActive_should_return_list()
        {
            _subjectFactory.CreateMore(100).Persist();
            var subjectList = _subjectService.LoadSubject(0, 100, "", "", "", "", "", "1");
            Assert.Equal(100, subjectList.Count);
        }

        #endregion
    }
}
