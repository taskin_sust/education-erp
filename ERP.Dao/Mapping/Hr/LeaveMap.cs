using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{
    public partial class LeaveMap : BaseClassMap<Leave,long> {
        
        public LeaveMap() {
			Table("HR_Leave");
			LazyLoad();
			References(x => x.Organization).Column("OrganizationId");
			Map(x => x.Name).Column("Name");
			Map(x => x.IsPublic).Column("IsPublic");
			Map(x => x.PayType).Column("PayType");
			Map(x => x.RepeatType).Column("RepeatType");
			Map(x => x.NoOfDays).Column("NoOfDays");
			Map(x => x.StartFrom).Column("StartFrom");
			Map(x => x.IsCarryforward).Column("IsCarryforward");
			Map(x => x.MaxCarryDays).Column("MaxCarryDays");
			Map(x => x.IsTakingLimit).Column("IsTakingLimit");
			Map(x => x.MaxTakingLimit).Column("MaxTakingLimit");
			Map(x => x.Applicationtype).Column("Applicationtype");
			Map(x => x.ApplicationDays).Column("ApplicationDays");
			Map(x => x.IsEncash).Column("IsEncash");
			Map(x => x.MinEncashReserveDays).Column("MinEncashReserveDays");
			Map(x => x.EffectiveDate).Column("EffectiveDate");
			Map(x => x.ClosingDate).Column("ClosingDate");
			Map(x => x.IsMale).Column("IsMale");
			Map(x => x.IsFemale).Column("IsFemale");
			Map(x => x.IsProbation).Column("IsProbation");
			Map(x => x.IsPermanent).Column("IsPermanent");
			Map(x => x.IsPartTime).Column("IsPartTime");
			Map(x => x.IsContractual).Column("IsContractual");
			Map(x => x.IsIntern).Column("IsIntern");
			Map(x => x.IsSingle).Column("IsSingle");
			Map(x => x.IsMarried).Column("IsMarried");
			Map(x => x.IsWidow).Column("IsWidow");
			Map(x => x.IsWidower).Column("IsWidower");
			Map(x => x.IsDevorced).Column("IsDevorced");
			HasMany(x => x.LeaveApplication).KeyColumn("LeaveId");
            HasMany(x => x.LeaveApplicationLogs).KeyColumn("LeaveId");
			HasMany(x => x.MembersLeaveSummary).KeyColumn("LeaveId");
        }
    }
}
