﻿$(document).ready(function () {
    window.validateDropDownField("LeaveId", "Please Select Leave.");
    //window.validateTextField("ResponsibleMemberPin", "Please Enter Responsible Person's Pin.");
    window.validateTextField("LeaveNote", "Please Enter Leave Note.");

    $("label:contains('*')").each(function () {
        $(this).html($(this).html().replace("*", "<span class='red'>*</span>"));
    });
    var today = new Date();
    var month = today.getMonth();
    var startDate = today.getFullYear().toString() + "-" + (month+1) + "-" + "01";
    $('.form_datetime1').datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: false,
        showMeridian: true,
        initialDate: new Date(),
        startDate: startDate,
        startView: 2,
        minView: 2,
        maxView: 4
    }).on('changeDate', function (ev) {
        if ($(this).val().length <= 0) {
            window.showErrorMessageBelowCtrl("DateFrom", "", false);
            window.showErrorMessageBelowCtrl("DateFrom", "Please Enter Date From.", true);
        } else {
            window.showErrorMessageBelowCtrl("DateFrom", "", false);
            dateDifference();
        }
    });
    $('.form_datetime2').datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: false,
        showMeridian: true,
        initialDate: new Date(),
        startDate: startDate,
        startView: 2,
        minView: 2,
        maxView: 4
    }).on('changeDate', function (ev) {
        if ($(this).val().length <= 0) {
            window.showErrorMessageBelowCtrl("DateTo", "", false);
            window.showErrorMessageBelowCtrl("DateTo", "Please Enter Date From.", true);
        } else {
            window.showErrorMessageBelowCtrl("DateTo", "", false);
            dateDifference();
        }
    });

    //Only Number Input
    $(document).on("keypress keyup blur", "#ResponsibleMemberPin", function (e) {
        if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }

    });

    $(document).on("input propertychange", "#ResponsibleMemberPin", function (event) {
        var responsiblePinPrevious = parseInt($(this).val().trim());
        console.log(responsiblePinPrevious);
        setTimeout(function () {
            var responsiblePin = parseInt($("#ResponsibleMemberPin").val().trim());
            $("#ResponsibleMemberName").val("");
            if (responsiblePin != "" && responsiblePin == responsiblePinPrevious) {
                if (responsiblePin > 0) {
                    $.ajax({
                        //url: "/Hr/LeaveApplication/GetTeamMemberInfo",
                        url: $("body").attr("data-project-root") + "Hr/CommonAjaxHr/GetTeamMemberInformation",
                        type: "POST",
                        data: { pin: responsiblePin },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (data) {
                            $.unblockUI();
                            if (data.IsSuccess) {
                                $('#ResponsibleMemberName').val(data.teamMemberName);
                            } else {
                                $('#ResponsibleMemberName').val('');
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (data) {
                            $.unblockUI();
                            bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                        }
                    });
                }
                //else {
                //    window.showErrorMessageBelowCtrl("ResponsibleMemberPin", "Please enter valid PIN", true);
                //    window.showErrorMessageBelowCtrl("ResponsibleMemberName", false);
                //    return false;
                //}
            }
        }, 1000);
    });

    $(document).on('click', '#ApplyLeave', function (e) {
        $('.field-validation-error').remove();
        var isSuccess = true;
        var leaveId = $("#LeaveId").val();
        var dateFrom = $("#DateFrom").val();
        var dateTo = $("#DateFrom").val();
        var leaveNote = $("#LeaveNote").val();
        //var responsibleMemberPin = $("#ResponsibleMemberPin").val();

        if (leaveId == null || leaveId <= 0) {
            window.showErrorMessageBelowCtrl("LeaveId", "", false);
            window.showErrorMessageBelowCtrl("LeaveId", "Please Select Leave.", true);
            isSuccess = false;
        }
        if (dateFrom == null || dateFrom.length <= 0) {
            window.showErrorMessageBelowCtrl("DateFrom", "", false);
            window.showErrorMessageBelowCtrl("DateFrom", "Please Select Date From.", true);
            isSuccess = false;
        }
        if (dateTo == null || dateTo.length <= 0) {
            window.showErrorMessageBelowCtrl("DateTo", "", false);
            window.showErrorMessageBelowCtrl("DateTo", "Please Select Date To.", true);
            isSuccess = false;
        }
        if (leaveNote == null || leaveNote.length < 4 || leaveNote.length > 250) {
            window.showErrorMessageBelowCtrl("LeaveNote", "", false);
            window.showErrorMessageBelowCtrl("LeaveNote", "Leave Note must be a string with a minimum length of 5 and a maximum length of 250.", true);
            isSuccess = false;
        }

        if (dateFrom == null && dateTo == null && dateFrom.length > 0 && dateTo.length > 0) {
            var fromDateFormated = new Date(fromDate);
            var toDateFormated = new Date(toDate);
            if (fromDateFormated > toDateFormated) {
                window.showErrorMessageBelowCtrl("DateFrom", "", false);
                window.showErrorMessageBelowCtrl("DateFrom", "From Date is greater then To Date.", true);
                $('#TotalLeaveDay').val('');
                isSuccess = false;
            }
        }

        //if (responsibleMemberPin == null || responsibleMemberPin.length <= 0) {
        //    window.showErrorMessageBelowCtrl("ResponsibleMemberPin", "", false);
        //    window.showErrorMessageBelowCtrl("ResponsibleMemberPin", "Please Enter Responsible Person's Pin.", true);
        //    isSuccess = false;
        //}
        if (isSuccess == true) {
            var postData = $("#ApplyLeaveApplication").serializeArray();
            console.log(postData);
            $.ajax({
                url: $("body").attr("data-project-root") + "Hr/LeaveApplication/LeaveApplication",
                type: "POST",
                data: postData,
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    $.unblockUI();
                    if (data.IsSuccess) {
                        $("#LeaveId")[0].selectedIndex = 0;
                        $("#DateFrom").val('');
                        $("#DateTo").val('');
                        $("#LeaveNote").val('');
                        $("#ResponsibleMemberPin").val('');
                        $("#ResponsibleMemberName").val('');
                        $('#TotalLeaveDay').val('');
                            $.fn.customMessage({
                                displayMessage: data.Message,
                                displayMessageType: "s"
                            });

                    } else {
                        $.fn.customMessage({
                            //displayMessage: ERROR_MESS,
                            displayMessage: data.Message,
                            displayMessageType: "e",
                            //displayTime: 5000
                        });
                    }
                },
                error: function () {
                    $.unblockUI();
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });


        }
        e.preventDefault();

    });

    $(document).on("click", ".glyphicon-trash", function () {
        var id = $(this).attr("data-id");
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Hr/LeaveApplication/CancelLeave",
            cache: false,
            async: true,
            data: { "id": id },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $.unblockUI();
                if (data.IsSuccess) {
                    $("#status-" + id).html(data.statusText);
                    $.fn.customMessage({
                        displayMessage: data.Message,
                        displayMessageType: "s"
                    });

                } else {
                    $.fn.customMessage({
                        //displayMessage: ERROR_MESS,
                        displayMessage: data.Message,
                        displayMessageType: "e",
                        //displayTime: 5000
                    });
                }
            },
            error: function () {
                $.unblockUI();
                $.fn.customMessage({
                    displayMessage: ajaxError,
                    displayMessageType: "error",
                    displayTime: 5000
                });
            }
        });
    });

});

function dateDifference() {
    var fromDate = $('#DateFrom').val();
    var toDate = $('#DateTo').val();
    var daysDiff = "";
    if (fromDate.length > 0 && toDate.length > 0) {
        var fromDateFormated = new Date(fromDate);
        var toDateFormated = new Date(toDate);
        if (fromDateFormated > toDateFormated) {
            window.showErrorMessageBelowCtrl("DateFrom", "", false);
            window.showErrorMessageBelowCtrl("DateFrom", "From Date is greater then To Date.", true);
            $('#TotalLeaveDay').val('');
        } else {
            window.showErrorMessageBelowCtrl("DateFrom", "", false);
            window.showErrorMessageBelowCtrl("DateFrom", "", false);
            daysDiff = DateDiff(fromDateFormated, toDateFormated);
            $('#TotalLeaveDay').val(daysDiff);
        }
    }
}

function DateDiff(start, end) {

    var df = Math.floor(start / (3600 * 24 * 1000)); //days as integer from..
    var dt = Math.floor(end / (3600 * 24 * 1000)); //days as integer from..
    var daysDiff = (dt - df) + 1; // exact dates
    return daysDiff;
}