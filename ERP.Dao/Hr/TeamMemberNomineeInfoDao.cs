﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface ITeamMemberNomineeIfnoDao : IBaseDao<NomineeInfo, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        void GetCurrentTeamMemberNomineeInfo(long hrMemberId, out string nomineeName, out string nomineeNameRelation, out string nomineeContactNumber);
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class TeamMemberNomineeIfnoDao : BaseDao<NomineeInfo, long>, ITeamMemberNomineeIfnoDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public void GetCurrentTeamMemberNomineeInfo(long hrMemberId, out string nomineeName, out string nomineeNameRelation,
            out string nomineeContactNumber)
        {
            nomineeName = "";
            nomineeNameRelation = "";
            nomineeContactNumber = "";

            var entity = Session.QueryOver<NomineeInfo>()
                .Where(x => x.TeamMember.Id == hrMemberId)
                .Where(x => x.Status == NomineeInfo.EntityStatus.Active)
                .Take(1).SingleOrDefault<NomineeInfo>();
            if (entity != null)
            {
                nomineeName = entity.Name.ToString(CultureInfo.CurrentCulture);
                nomineeNameRelation = entity.Relation.ToString(CultureInfo.CurrentCulture);
                nomineeContactNumber = entity.MobileNo.ToString(CultureInfo.CurrentCulture);
            }
        }
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
