﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using Xunit;

namespace UdvashERP.Services.Test.Hr.EmploymentHistoryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "EmploymentHistory")]
    public class EmploymentHistoryLoadTest : EmploymentHistoryBaseTest
    {
        #region Basic Test

        [Fact]
        public void GetEmploymentHistory_Should_Return_Employment_Entity_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryFactory.Object.EffectiveDate = _employmentHistoryFactory.Object.EffectiveDate.AddDays(1);
            _employmentHistoryService.Save(_employmentHistoryFactory.Object);

            var entity = _employmentHistoryService.GetEmploymentHistory(_employmentHistoryFactory.Object.Id);
            Assert.NotNull(entity);
            Assert.Equal(entity.Id, _employmentHistoryFactory.Object.Id);
        }

        [Fact]
        public void GetTeamMemberEmploymentHistory_Should_Return_Employment_Entity_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryFactory.Object.EffectiveDate = _employmentHistoryFactory.Object.EffectiveDate.AddDays(1);
            _employmentHistoryService.Save(_employmentHistoryFactory.Object);

            var entity = _employmentHistoryService.GetTeamMemberEmploymentHistory(_employmentHistoryFactory.Object.EffectiveDate, teamMember.Id, teamMember.Pin);
            Assert.NotNull(entity);
            Assert.Equal(entity.Id, _employmentHistoryFactory.Object.Id);
        }

        [Fact]
        public void GetCurrentEmployementStatus_Should_Return_EmploymentStatus_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryFactory.Object.EmploymentStatus = (int) MemberEmploymentStatus.Permanent;
            _employmentHistoryFactory.Object.EffectiveDate = _employmentHistoryFactory.Object.EffectiveDate.AddDays(1);
            _employmentHistoryService.Save(_employmentHistoryFactory.Object);
            var status = _employmentHistoryService.GetCurrentEmployementStatus(teamMemberFactory.Object.Id);
            Assert.Equal(status, _employmentHistoryFactory.Object.EmploymentStatus);
        }

        [Fact]
        public void LoadEmploymentHistories_Should_Return_Employment_List()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryFactory.Object.EffectiveDate = _employmentHistoryFactory.Object.EffectiveDate.AddDays(1);
            _employmentHistoryService.Save(_employmentHistoryFactory.Object);
            var list = _employmentHistoryService.LoadEmploymentHistories(teamMember, _employmentHistoryFactory.Object.EffectiveDate.AddDays(1));
            Assert.NotNull(list);
            Assert.True(list.Count > 0);
            Assert.Equal(list[0].Id, _employmentHistoryFactory.Object.Id);
        }

        #endregion

        #region Business Logic Test


        #endregion
    }
}
