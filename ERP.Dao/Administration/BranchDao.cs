﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Transform;
using NHibernate.Util;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Administration
{
    public interface IBranchDao : IBaseDao<Branch, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        Branch GetBranch(long orgtanizationId, string shortName, bool caseSensitive = true);
        Branch GetBranch(string branchCode);

        #endregion

        #region List Loading Function
        IList<Branch> LoadBranch(List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, List<long> branchIdList = null,
            List<int?> admissionType = null, bool? isCorporate = null);
        IList<Branch> LoadBranch(int start, int length, string orderBy, string orderDir, string organization, string name, string code, string status);
        IList<Branch> LoadAuthorizedBranch(List<long> authBranchIds, List<long> programIds, List<long> sessionIds, bool isProgramBranSession = true, bool? isCorporate = null);
        IList<Branch> LoadBranchByProgramSessionForTransaction(long programId, long sessionId, long[] authorizedProgramIds, long[] authorizedSessionIds,
            List<long> allAuthorizedBranches);
        IList<Branch> LoadBranchByProgramSession(long programId, long sessionId, bool? anyAdmissionType = null);
        IList<Branch> LoadBranch(long[] branchIds);
        IList<string> LoadBranchNameList(List<long> branchIdList);

        #endregion

        #region Others Function
        bool IsDuplicateBranch(out string fieldName, long organizationId, string name = null, string shortName = null, string code = null, long id = 0);
        int GetBranchCount(string orderBy, string orderDir, string organization, string name, string code, string status);
        #endregion

        #region Public API
        IList<Branch> LoadPublicBranchByProgramSession(string orgBusinedssId, long programId, long sessionId);
        #endregion

        List<string> LoadAllCode();
        IList<Branch> LoadBranchByOrganization(long organizationId, bool isCorporate);

    }
    public class BranchDao : BaseDao<Branch, long>, IBranchDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public Branch GetBranch(long organizationId, string shortName, bool caseSensitive = true)
        {
            ICriteria criteria = Session.CreateCriteria<Branch>().Add(Restrictions.Eq("Status", Batch.EntityStatus.Active));
            if (!string.IsNullOrEmpty(shortName))
            {
                criteria.Add(caseSensitive
                    ? Restrictions.Eq("ShortName", shortName)
                    : Restrictions.Eq("ShortName", shortName).IgnoreCase());
            }
            return criteria.UniqueResult<Branch>();
        }

        public Branch GetBranch(string branchCode)
        {
            ICriteria criteria = Session.CreateCriteria<Branch>().Add(Restrictions.Eq("Status", Batch.EntityStatus.Active));
            if (!string.IsNullOrEmpty(branchCode))
            {
                criteria.Add(Restrictions.Eq("Code", branchCode));
            }
            return criteria.UniqueResult<Branch>();
        }

        #endregion

        #region List Loading Function
        public IList<Branch> LoadBranch(int start, int length, string orderBy, string orderDir, string organization, string name, string code, string status)
        {
            ICriteria criteria = GetBranchCriteria(orderBy, orderDir, organization, name, code, status);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Branch>();
        }

        public IList<Branch> LoadBranch(List<long> organizationIdList = null, List<long> programIdList = null, List<long> sessionIdList = null, List<long> branchIdList = null, List<int?> admissionType = null, bool? isCorporate = null)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(Branch)).Add(Restrictions.Eq("Status", Branch.EntityStatus.Active));

            if (organizationIdList != null)
            {
                criteria.CreateAlias("Organization", "orga").Add(Restrictions.Eq("orga.Status", Organization.EntityStatus.Active));
                if (!organizationIdList.Contains(SelectionType.SelelectAll))
                    criteria.Add(Restrictions.In("orga.Id", organizationIdList));
            }

            if (branchIdList != null)
            {
                if (!branchIdList.Contains(SelectionType.SelelectAll))
                    criteria.Add(Restrictions.In("Id", branchIdList));
            }

            if (isCorporate != null)
            {
                criteria.Add(Restrictions.Eq("IsCorporate", isCorporate));
            }

            if (programIdList != null || sessionIdList != null)
            {
                DetachedCriteria branchInProgramBranchSession = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")));
                if (programIdList != null)
                {
                    branchInProgramBranchSession.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
                    if (!programIdList.Contains(SelectionType.SelelectAll))
                        branchInProgramBranchSession.Add(Restrictions.In("p.Id", programIdList));
                }
                if (sessionIdList != null)
                {
                    branchInProgramBranchSession.CreateAlias("Session", "s").Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
                    if (!sessionIdList.Contains(SelectionType.SelelectAll))
                        branchInProgramBranchSession.Add(Restrictions.In("s.Id", sessionIdList));
                }

                if (admissionType != null)
                {
                    if (admissionType.Contains(0))
                    {
                        branchInProgramBranchSession.Add(Restrictions.And(Restrictions.Eq("IsOffice", false), Restrictions.Eq("IsPublic", false)));
                    }
                    else if (admissionType.Contains(1) && admissionType.Contains(2))
                    {
                        branchInProgramBranchSession.Add(Restrictions.Eq("IsOffice", true));
                        branchInProgramBranchSession.Add(Restrictions.Eq("IsPublic", true));
                    }
                    else
                    {
                        if (admissionType.Contains(1))
                        {
                            branchInProgramBranchSession.Add(Restrictions.Eq("IsOffice", true));
                            branchInProgramBranchSession.Add((Restrictions.Eq("IsPublic", false)));
                        }
                        if (admissionType.Contains(2))
                        {
                            branchInProgramBranchSession.Add(Restrictions.Eq("IsPublic", true));
                            branchInProgramBranchSession.Add(Restrictions.Eq("IsOffice", false));
                        }
                    }
                }
                criteria.Add(Subqueries.PropertyIn("Id", branchInProgramBranchSession));
                criteria.AddOrder(Order.Asc("Rank"));
            }
            return criteria.List<Branch>().OrderBy(x => x.Rank).ToList();
        }

        public IList<Branch> LoadAuthorizedBranch(List<long> authBranchIds, List<long> programIds, List<long> sessionIds, bool isProgramBranSession = true, bool? isCorporate = null)
        {
            ICriteria criteria = Session.CreateCriteria<Branch>().Add(Restrictions.Eq("Status", Branch.EntityStatus.Active));

            if (isCorporate != null)
            {
                criteria.Add(Restrictions.Eq("IsCorporate", isCorporate));
            }
            if (authBranchIds != null)
            {
                criteria.Add(Restrictions.In("Id", authBranchIds));
            }

            if (isProgramBranSession)
            {
                DetachedCriteria programSessionBranch = DetachedCriteria.For<ProgramBranchSession>()
                    .SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
                    .CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
                programSessionBranch.CreateAlias("Program", "pr")
                    .Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
                programSessionBranch.CreateAlias("Session", "s")
                    .Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));


                if (programIds != null)
                {
                    programSessionBranch.Add(Restrictions.In("pr.Id", programIds));
                }
                if (sessionIds != null)
                {
                    programSessionBranch.Add(Restrictions.In("s.Id", sessionIds));
                }
                criteria.Add(Subqueries.PropertyIn("Id", programSessionBranch));
            }


            var branchList = criteria.List<Branch>().OrderBy(x => x.Rank).ToList();
            return branchList;
        }
        //public IList<Branch> LoadAuthorizedBranch(List<long> authOrganizationIds, List<long> authProgramIds, List<long> authBranchIds, List<long> organizationIds, List<long> programIds, List<long> sessionIds)
        //{
        //    ICriteria criteria = Session.CreateCriteria<Branch>().Add(Restrictions.Eq("Status", Branch.EntityStatus.Active));

        //    DetachedCriteria programSessionBranch = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Branch.Id")))
        //        .CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
        //    programSessionBranch.CreateAlias("Program", "pr").Add(Restrictions.Eq("pr.Status", Program.EntityStatus.Active));
        //    programSessionBranch.CreateAlias("Session", "s").Add(Restrictions.Eq("s.Status", Model.Entity.Administration.Session.EntityStatus.Active));

        //    if (authOrganizationIds != null)
        //    {
        //        criteria.Add(Restrictions.In("Organization.Id", authOrganizationIds));// authorize Organization Lists From Menu  
        //    }
        //    if (authProgramIds != null)
        //    {
        //        programSessionBranch.Add(Restrictions.In("pr.Id", authProgramIds));//authorize Program Lists From Menu  
        //    }
        //    if (authBranchIds != null)
        //    {
        //        criteria.Add(Restrictions.In("Id", authBranchIds));// authorize Branch Lists From Menu  
        //    }

        //    if (organizationIds != null)
        //    {
        //        if (!organizationIds.Contains(SelectionType.SelelectAll))
        //            criteria.Add(Restrictions.In("Organization.Id", organizationIds));// selected organization 
        //    }
        //    if (programIds != null)
        //    {
        //        if (!programIds.Contains(SelectionType.SelelectAll))
        //            programSessionBranch.Add(Restrictions.In("pr.Id", programIds)); //selected program      
        //    }
        //    if (sessionIds != null)
        //    {
        //        if (!sessionIds.Contains(SelectionType.SelelectAll))
        //            programSessionBranch.Add(Restrictions.In("s.Id", sessionIds)); //selected Session      
        //    }

        //    criteria.Add(Subqueries.PropertyIn("Id", programSessionBranch));//Detach Criteria will apply on Program Table on Id

        //    var branchList = criteria.List<Branch>().OrderBy(x => x.Rank).ToList();
        //    return branchList;
        //}

        public IList<Branch> LoadBranchByProgramSession(long programId, long sessionId, bool? anyAdmissionType = null)
        {
            ICriteria criteria = Session.CreateCriteria<ProgramBranchSession>();
            if (anyAdmissionType == false)
            {
                criteria.Add(Restrictions.Eq("IsOffice", false));
                criteria.Add(Restrictions.Eq("IsPublic", false));
            }
            criteria.CreateAlias("Branch", "b").Add(Restrictions.Eq("b.Status", Branch.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Program.Id", programId)).Add(Restrictions.Eq("Session.Id", sessionId));
            criteria.SetProjection(Projections.Distinct(Projections.Property("Branch")));
            var blist = criteria.List<Branch>();
            return blist;
        }



        public IList<Branch> LoadBranchByProgramSessionForTransaction(long programId, long sessionId, long[] authorizedProgramIds, long[] authorizedSessionIds, List<long> allAuthorizedBranches)
        {
            ICriteria criteria = Session.CreateCriteria<ProgramBranchSession>().Add(Restrictions.Eq("Status", StudentPayment.EntityStatus.Active));
            if (sessionId != BusinessRules.SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            }
            else
            {
                criteria.Add(Restrictions.In("Session.Id", authorizedSessionIds));
            }
            if (programId != BusinessRules.SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("Program.Id", programId));
            }
            else
            {
                criteria.Add(Restrictions.In("Program.Id", authorizedProgramIds));
            }
            criteria.Add(Restrictions.In("Branch.Id", allAuthorizedBranches));
            IList<ProgramBranchSession> programBranchSessionList = criteria.List<ProgramBranchSession>();
            IList<Branch> branchList = new List<Branch>();
            if (programBranchSessionList != null && programBranchSessionList.Count > 0)
            {
                programBranchSessionList.ForEach(a => branchList.Add(a.Branch));
            }
            return branchList;
        }
        public IList<Branch> LoadBranch(long[] branchIds)
        {
            ICriteria criteria = Session.CreateCriteria<Branch>().Add(Restrictions.Eq("Status", Branch.EntityStatus.Active));
            if (branchIds.Any())
            {
                criteria.Add(Restrictions.In("Id", branchIds));
            }
            return criteria.List<Branch>();
        }
        public IList<Branch> LoadBranchByOrganization(long organizationId, bool isCorporate)
        {
            var list = Session.QueryOver<Branch>().Where(x => x.Organization.Id == organizationId)
                        .Where(x => x.Status == Branch.EntityStatus.Active)
                        .List<Branch>();
            return list.ToList<Branch>();
        }

        public IList<string> LoadBranchNameList(List<long> branchIdList)
        {
            ICriteria criteria = Session.CreateCriteria<Branch>().Add(Restrictions.Eq("Status", Branch.EntityStatus.Active));
            if (branchIdList != null && branchIdList.Any())
            {
                criteria.Add(Restrictions.In("Id", branchIdList));
            }
            criteria.SetProjection(Projections.Distinct(Projections.ProjectionList()
                .Add(Projections.Alias(Projections.Property("Name"), "Name"))
                ));
            return criteria.List<string>();
        }

        #endregion

        #region Others Function
        public bool IsDuplicateBranch(out string fieldName, long organizationId, string name = null, string shortName = null, string code = null, long id = 0)
        {
            fieldName = "";
            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<Branch>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Branch.EntityStatus.Delete)));

            if (id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (name != null)
            {
                criteria.Add(Restrictions.Eq("Organization.Id", organizationId));
                criteria.Add(Restrictions.Eq("Name", name));
                fieldName = "Name";
            }
            else if (shortName != null)
            {
                criteria.Add(Restrictions.Eq("Organization.Id", organizationId));
                criteria.Add(Restrictions.Eq("ShortName", shortName));
                fieldName = "ShortName";
            }
            else if (code != null)
            {
                criteria.Add(Restrictions.Eq("Code", code));
                fieldName = "Code";
            }
            var branchList = criteria.List<Branch>();
            if (branchList != null && branchList.Count > 0)
            {
                checkValue = true;
            }
            return checkValue;
        }
        public int GetBranchCount(string orderBy, string orderDir, string organization, string name, string code, string status)
        {
            ICriteria criteria = GetBranchCriteria(orderBy, orderDir, organization, name, code, status, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());

        }



        #endregion

        #region Helper Function
        private ICriteria GetBranchCriteria(string orderBy, string orderDir, string organization, string name, string code, string status, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<Branch>().Add(Restrictions.Not(Restrictions.Eq("Status", Branch.EntityStatus.Delete)));
            criteria.CreateAlias("Organization", "organization").Add(Restrictions.Not(Restrictions.Eq("organization.Status", Branch.EntityStatus.Delete)));
            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(code))
            {
                criteria.Add(Restrictions.Like("Code", code, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(organization))
            {
                criteria.Add(Restrictions.Eq("organization.Id", Convert.ToInt64(organization)));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }
        #endregion

        #region Public API
        public IList<Branch> LoadPublicBranchByProgramSession(string orgBusinessId, long programId, long sessionId)
        {
            ICriteria criteria = Session.CreateCriteria<Branch>()
                .Add(Restrictions.Eq("Status", Branch.EntityStatus.Active))
                .CreateAlias("ProgramBranchSessions", "pbs")
                .Add(Restrictions.Eq("pbs.IsPublic", true))
                .Add(Restrictions.Eq("pbs.Status", ProgramBranchSession.EntityStatus.Active))
                .CreateAlias("pbs.Program", "p")
                .Add(Restrictions.Eq("p.Id", programId))
                .Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active))
                .CreateAlias("p.Organization", "o")
                .Add(Restrictions.Eq("o.BusinessId", orgBusinessId))
                .Add(Restrictions.Eq("o.Status", Organization.EntityStatus.Active))
                .CreateAlias("pbs.Session", "s")
                .Add(Restrictions.Eq("s.Id", sessionId))
                .Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));

            return criteria.List<Branch>().OrderBy(x => x.Rank).Distinct().ToList();
        }

        public List<string> LoadAllCode()
        {
            ICriteria criteria = Session.CreateCriteria<Branch>();
            criteria.SetProjection(Projections.Distinct(Projections.Property("Code")));
            return criteria.List<string>().ToList();
        }

        #endregion
    }
}