﻿//load course dropdownlist based on program and session

$(document).ready(function () {
    $("#StudentProgramRoll").css('font-size', '150%');

    $("#date").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: false,
        showMeridian: true,
        endDate: '+0d',
        startView: 2,
        minView: 2,
        maxView: 4
    }).on('changeDate', function (ev) {
        if ($("#date").val().length > 0) {
            $('#dtmsg').text("");
        }
    });
    $(document).on('change', '#ProgramId', function () {
        var programId = $("#ProgramId").val();
        // var sessionId = $("#SessionId").val();

        $("#courseId").empty();
        $("#SessionId").empty();
        $("#subjectId").empty();

        if (programId != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Student/Attendance/AjaxRequestForSessions",
                cache: false,
                async: true,
                data: { programId: programId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    if (result.IsSuccess) {
                        $.each(result.sessionList, function (i, v) {
                            $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                            $("#SessionId").find('option:eq(0)').prop('selected', true);
                        });
                    }
                    else {

                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }

    });
    $(document).on('change', '#SessionId', function () {
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();

        $("#courseId").empty();




        if (sessionId != "" && programId != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Student/Attendance/AjaxRequestForCourses",
                cache: false,
                async: true,
                data: { programId: programId, sessionId: sessionId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    if (result.IsSuccess) {
                        $.each(result.courseList, function (i, v) {
                            $('#courseId').append($('<option>').text(v.Text).attr('value', v.Value));
                            $("#courseId").find('option:eq(0)').prop('selected', true);
                        });
                    }
                    else {

                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }

    });
    $(document).on('change', '#courseId', function () {
        // alert("hello");
        var courseId = $("#courseId").val();


        $("#subjectId").empty();




        if (courseId != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Student/Attendance/AjaxRequestForSubject",
                cache: false,
                async: true,
                data: { courseId: courseId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    if (result.IsSuccess) {
                        $.each(result.subjectList, function (i, v) {
                            $('#subjectId').append($('<option>').text(v.Text).attr('value', v.Value));
                            $("#subjectId").find('option:eq(0)').prop('selected', true);
                        });
                    }
                    else {

                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }

    });
    function cleanArray(actual) {
        var newArray = new Array();
        for (var i = 0; i < actual.length; i++) {
            if (actual[i]) {
                newArray.push(actual[i]);
            }
        }
        return newArray;
    }
    var rolls = [];
    $("#StudentProgramRoll").val('');
    var maxLength = 11;
    $('#StudentProgramRoll').on('input focus keydown keyup', function (e) {

        //if ((e.keyCode >= 65 && e.keyCode <= 90)) {

        //    return false;
        //}
       // else {
            var text = $(this).val();
            var lines = text.split(/(\r\n|\n|\r)/gm);
            for (var i = 0; i < lines.length; i++) {
                if (lines[i].length > maxLength) {
                    lines[i] = lines[i].substring(0, maxLength);
                }
            }
            $(this).val(lines.join(''));
       // }
    });
   
        $("#StudentProgramRoll").on("input propertychange", function (e) {


            rolls = $('#StudentProgramRoll').val().split("\n");
            rolls = cleanArray(rolls);
            console.log(rolls);
            if (rolls.length > 0) {

                $("#prmsg").text("");

                if (rolls[rolls.length - 1] == "") {
                    $('#lastId').text(rolls[rolls.length - 2]);
                } else {
                    $('#lastId').text(rolls[rolls.length - 1]);
                }
            } else {
                $('#lastId').text("");
            }

        });
        $("#ProgramId").on("change", function (e) {
            $('#pmsg').text("");
        });
        $("#SessionId").on("change", function (e) {
            $('#smsg').text("");
        });
        $("#courseId").on("change", function (e) {
            $('#cmsg').text("");
        });
        $("#subjectId").on("change", function (e) {
            $('#submsg').text("");
        });
        $("#ClassName").on("input", function (e) {
            $('#cnmsg').text("");
        });
        $("#Teacher").on("input", function (e) {
            $('#tmsg').text("");
        });
        $('#Submit').click(function (e) {
            $('#pmsg').text("");
            $('#smsg').text("");
            $('#cmsg').text("");
            $('#submsg').text("");
            $('#cnmsg').text("");
            $('#tmsg').text("");
            $('#dtmsg').text("");


            function isNull(inputArray) {

                var allNull = true;
                if (inputArray.length > 0) {
                    for (var i = 0; i < inputArray.length; i++) {
                        if (inputArray[i].trim().length == 0) {
                            continue;
                        } else {
                            allNull = false;
                            break;
                        }
                    }
                } else {
                    return allNull;
                }
                return allNull;
            }


            var message = "";
            var programId = $("#ProgramId").val();
            var sessionId = $("#SessionId").val();
            var subjectId = $("#subjectId").val();
            var courseId = $("#courseId").val();
            var ClassName = $("#ClassName").val();
            var Teacher = $("#Teacher").val();
            var date = $("#date").val();
            console.log($("#date").val);
            //var StudentProgramRoll = $("StudentProgramRoll").val();
            var rolls = $('#StudentProgramRoll').val().split("\n");
            for (i = 0; i < rolls.length; i++) {
                if (rolls[i] == null || rolls[i] == "") {
                    rolls.splice(i, 1);
                }
            }
            var isSuccess = true;
            if (programId == null || programId <= 0) {
                $('#pmsg').text("");
                // var programMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="pmsg">Select Program</span></span>';
                $('#pmsg').append("Select Program");
                isSuccess = false;
            }

            if (sessionId == null || sessionId <= 0) {
                $('#smsg').text("");
                //var sessionMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="smsg">Select Session</span></span>';

                $('#smsg').append("Select Session");
                isSuccess = false;
            }

            if (subjectId < 1) {
                $('#submsg').text("");
                //var branchMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="brmsg">Select Branch</span></span>';
                $('#submsg').append("Select Subject");
                isSuccess = false;
            }

            if (courseId < 1) {
                $('#cmsg').text("");
                // var campusMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="campmsg">Select Campus</span></span>';
                $('#cmsg').append("Select Course");
                isSuccess = false;
            }

            if (ClassName == null || ClassName == "") {
                $('#cnmsg').text("");
                // var batchDayMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="bdmsg">Select Batch Day</span></span>';
                $('#cnmsg').append("Enter Class name");
                isSuccess = false;
            }

            if (Teacher == null || Teacher == "") {
                $('#tmsg').text("");
                //var batchTimeMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="btmsg">Select Batch Time</span></span>';
                $('#tmsg').append("Enter Teacher Name");
                isSuccess = false;
            }

            if (date == null || date == "") {
                $('#dtmsg').text("");
                // var batchNameMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="bmsg">Select Batch</span></span>';
                $('#dtmsg').append("Enter a Date");
                isSuccess = false;
            }
            if (rolls.length < 1 && isNull(rolls) == true) {
                $('#prmsg').text("");
                // var batchNameMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="bmsg">Select Batch</span></span>';
                $('#prmsg').append("Enter Program Roll");
                isSuccess = false;
            }


            if (isSuccess == true) {
                $('form#ClassAttendanceForm').submit();
                return true;
            } else {
                e.preventDefault();
                return false;
            }


        });

    
});



