﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceSynchronizerSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceSynchronizerSettingService")]
    public class AttendanceSynchronizerSettingSaveTest : AttendanceSynchronizerSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_DuplicateEntry_Exception()
        {
            _attendanceSynchronizerSettingFactory.Create().Persist();
            
            _attendanceSynchronizerSettingFactory.Create();
            _attendanceSynchronizerSettingFactory.SingleObjectList[1].Description =
                _attendanceSynchronizerSettingFactory.SingleObjectList[0].Description;
            _attendanceSynchronizerSettingFactory.SingleObjectList[1].Rank =
                _attendanceSynchronizerSettingFactory.SingleObjectList[0].Rank;
            _attendanceSynchronizerSettingFactory.SingleObjectList[1].ReleaseDate =
                _attendanceSynchronizerSettingFactory.SingleObjectList[0].ReleaseDate;
            _attendanceSynchronizerSettingFactory.SingleObjectList[1].UpdateUrl =
                _attendanceSynchronizerSettingFactory.SingleObjectList[0].UpdateUrl;

            Assert.Throws<DuplicateEntryException>(() => _attendanceSynchronizerSettingService.Save
                (_attendanceSynchronizerSettingFactory.SingleObjectList[1]));
        }

        [Fact]
        public void Should_Return_LowerVersion_Exception()
        {
            _attendanceSynchronizerSettingFactory.Create();
            _attendanceSynchronizerSettingFactory.Object.SynchronizerVersion = 2;
            _attendanceSynchronizerSettingFactory.Persist();

            //_attendanceSynchronizerSettingFactory.Create();
            //_attendanceSynchronizerSettingFactory.Object.SynchronizerVersion = 3;
            //_attendanceSynchronizerSettingFactory.Persist();

            _attendanceSynchronizerSettingFactory.Create(0, "test", DateTime.Now, "www.randomurll.com/111",1);
            //_attendanceSynchronizerSettingFactory.SingleObjectList[1].SynchronizerVersion = 1;
            //_attendanceSynchronizerSettingFactory.SingleObjectList[1].UpdateUrl = "www.randomurll.com/111";

            Assert.Throws<InvalidDataException>(() => _attendanceSynchronizerSettingService.Save(_attendanceSynchronizerSettingFactory.SingleObjectList[1]));
        }

        [Fact]
        public void Should_Return_InvalidData_Exception()
        {
            _attendanceSynchronizerSettingFactory.Create();
            _attendanceSynchronizerSettingFactory.Object.Id = 1;
            Assert.Throws<InvalidDataException>(
                () => _attendanceSynchronizerSettingService.Save(_attendanceSynchronizerSettingFactory.Object));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            Assert.True(_attendanceSynchronizerSettingFactory.Create().Persist().Object.Id > 0);
        }

        #endregion

        #region Business Logic Test

        #endregion
    }
}
