﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IInstituteCategoryDao : IBaseDao<InstituteCategory, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        InstituteCategory LoadInstituteCategoryById(long id);
        #endregion

        #region List Loading Function
        IList<InstituteCategory> GetInstituteCategoryList(bool isUniversity);
        IList<InstituteCategory> GetInstituteCategoryList();
        #endregion

        #region Others Function

        #endregion
    }

    public class InstituteCategoryDao : BaseDao<InstituteCategory, long>, IInstituteCategoryDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public InstituteCategory LoadInstituteCategoryById(long id)
        {
            ICriteria criteria =
                Session.CreateCriteria<InstituteCategory>()
                    .Add(Restrictions.Eq("Id", id))
                    .Add(Restrictions.Eq("Status", Institute.EntityStatus.Active));
            return (InstituteCategory)criteria.UniqueResult();
        }
        #endregion

        #region List Loading Function
        public IList<InstituteCategory> GetInstituteCategoryList(bool isUniversity)
        {
            ICriteria criteria =Session.CreateCriteria<InstituteCategory>().Add(Restrictions.Eq("Status", Institute.EntityStatus.Active)).Add(Restrictions.Eq("IsUniversity", isUniversity));
            return criteria.List<InstituteCategory>();
        }
        public IList<InstituteCategory> GetInstituteCategoryList()
        {
            ICriteria criteria =Session.CreateCriteria<InstituteCategory>().Add(Restrictions.Eq("Status", Institute.EntityStatus.Active));
            return criteria.List<InstituteCategory>();
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}