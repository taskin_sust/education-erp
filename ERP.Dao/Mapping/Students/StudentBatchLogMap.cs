using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students {


    public partial class StudentBatchLogMap : BaseClassMap<StudentBatchLog, long>
    {
        
        public StudentBatchLogMap() {
			Table("StudentBatchLog");
			LazyLoad();
			
            Map(x => x.TransferDate).Column("TransferDate");

            References(x => x.StudentProgram).Column("StudentProgramId");
            References(x => x.FromBatch).Column("FromBatchId");
            References(x => x.ToBatch).Column("ToBatchId");
        }
    }
}
