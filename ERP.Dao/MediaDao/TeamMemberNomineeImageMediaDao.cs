﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Data;
using NHibernate;
using NHibernate.Linq;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.MediaDao
{
    public interface ITeamMemberNomineeImageMediaDao : IBaseDao<TeamMemberNomineeMediaImage, long>
    {

        TeamMemberNomineeMediaImage GetTeamMemberNomineeImageMediaByRefId(long id);

        void DeleteTeamMemberNomineeMediaImage(long refId);
    }
    public class TeamMemberNomineeImageMediaDao : BaseDao<TeamMemberNomineeMediaImage, long>, ITeamMemberNomineeImageMediaDao
    {
        public TeamMemberNomineeMediaImage GetTeamMemberNomineeImageMediaByRefId(long id)
        {
            var entity = Session.QueryOver<TeamMemberNomineeMediaImage>().Where(x => x.ImageRef == id).Take(1).SingleOrDefault<TeamMemberNomineeMediaImage>();
            return entity;
        }

        public void DeleteTeamMemberNomineeMediaImage(long refId)
        {
            var teamMemberMediaImage =
                Session.QueryOver<TeamMemberNomineeMediaImage>().Where(x => x.ImageRef == refId).SingleOrDefault<TeamMemberNomineeMediaImage>();
            Session.Delete(teamMemberMediaImage);
        }
    }
}
