﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.Hr
{
    interface IFestivalBonusSheetDao : IBaseDao<FestivalBonusSheet, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        FestivalBonusSheet GetTeamMemberObject(TeamMember teamMember, DateTime startDate, DateTime endDate, FestivalBonusSetting festivalBonusSetting);

        #endregion

        #region List Loading Function

        List<FestivalBonusSheet> LoadFestivalBonusSheetsList(List<long> teamMemberIds, long festivalBonusSettingId,
            DateTime startDate, DateTime endDate, bool isSubmit);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class FestivalBonusSheetDao : BaseDao<FestivalBonusSheet, long>, IFestivalBonusSheetDao
    {
        #region Propertise & Object Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public FestivalBonusSheet GetTeamMemberObject(TeamMember teamMember, DateTime startDate, DateTime endDate, FestivalBonusSetting festivalBonusSetting)
        {
            return Session.QueryOver<FestivalBonusSheet>()
                .Where(x => x.TeamMember == teamMember && x.FestivalBonusSetting == festivalBonusSetting && (x.StartDate >= startDate && x.EndDate <= endDate))
                .SingleOrDefault();
        }

        #endregion

        #region List Loading Function

        public List<FestivalBonusSheet> LoadFestivalBonusSheetsList(List<long> teamMemberIds, long festivalBonusSettingId, DateTime startDate, DateTime endDate, bool isSubmit)
        {
            ICriteria criteria =
                    Session.CreateCriteria<FestivalBonusSheet>()
                        .Add(Restrictions.Eq("Status", FestivalBonusSheet.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("FestivalBonusSetting.Id", festivalBonusSettingId));
            criteria.Add(Restrictions.Ge("StartDate", startDate)).Add(Restrictions.Le("EndDate", endDate));
            criteria.Add(Restrictions.Eq("IsSubmit", isSubmit));
            criteria.Add(Restrictions.In("TeamMember.Id", teamMemberIds.ToArray()));
            return criteria.List<FestivalBonusSheet>().ToList();
        }


        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion

        
    }
}
