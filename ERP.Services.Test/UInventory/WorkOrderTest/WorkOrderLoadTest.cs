﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.WorkOrderTest
{
    interface IWorkOrderLoadTest
    {
        #region basic Test

        void Should_Return_GetByWorkOrderNo_InvalidDataException_Message();
        void Should_Return_GetByWorkOrderNo_Object_Successfully();

        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "WorkOrder")]
    public class WorkOrderLoadTest : WorkOrderBaseTest, IWorkOrderLoadTest
    {
        [Fact]
        public void Should_Return_GetByWorkOrderNo_InvalidDataException_Message()
        {
            Assert.Throws<InvalidDataException>(() => _workOrderService.GetByWorkOrderNo(""));
        }

        [Fact]
        public void Should_Return_GetByWorkOrderNo_Object_Successfully()
        {
            _organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.PertialIssued, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now)
                .WithBranch(_branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(_organizationFactory.Object, _branchFactory.Object).Persist();
            
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _workOrderFactory.Create().WithBranch(_branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object)
                .Persist();
            Assert.True(_workOrderService.GetByWorkOrderNo(_workOrderFactory.Object.WorkOrderNo).Id > 0);
        }
    }
}
