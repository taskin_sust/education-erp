using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class QuotationMap : BaseClassMap<Quotation, long>
    {
        public QuotationMap()
        {
            Table("UINV_Quotation");
            LazyLoad();
            References(x => x.Item).Column("ItemId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.Branch).Column("BranchId");
            References(x => x.PurposeProgram).Column("PurposeProgramId");
            References(x => x.PurposeSession).Column("PurposeSessionId");
            Map(x => x.Purpose).Column("Purpose");
            Map(x => x.QuotationNo).Column("QuotationNo").Not.Nullable();
            Map(x => x.QuotationQuantity).Column("QuotationQuantity").Not.Nullable();
            Map(x => x.QuotationStatus).Column("QuotationStatus").Not.Nullable();
            Map(x => x.SubmissionDeadLine).Column("SubmissionDeadLine").Not.Nullable();
            Map(x => x.DeliveryDate).Column("DeliveryDate").Not.Nullable();
            Map(x => x.PublishedDate).Column("PublishedDate");
            Map(x => x.Remarks).Column("Remarks");
            HasMany(x => x.QuotationCriteriaList).KeyColumn("QuotationId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.SupplierPriceQuoteList).KeyColumn("QuotationId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.WorkOrderList).KeyColumn("QuotationId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
