﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.UserAuth
{
    public interface IUserMenuService : IBaseService
    {
        #region Operational Function
        void Save(UserMenu erpUserMenu);
        void Delete(long id, UserMenu userMenu);
        #endregion

        #region Bulk Operational Function

        void BulkUserPermissionSet(long createBy, List<UserProfile> userProfileList, long[] organizationIdList,
            long[] programIdList, long[] branchIdList, List<long> menuIdList);
        #endregion

        #region Single Instances Loading Function
        UserMenu LoadById(long id);
        UserMenu LoadDataByUserBranchProgram(long userId, long? orId, long? brId, long? prId, long? menuId);
        #endregion

        #region List Loading Function
        IList<UserMenu> LoadDataByUserBranchProgramOrganization(long[] listUser, long[] organizationIds, long[] program, long[] branch);
        List<UserMenu> LoadUsrMenuByUsrAndMenuNullBP(UserProfile userProfile, Menu menu, long Id);
        List<UserMenu> LoadByProfile(UserProfile userProfile);  
        #endregion

        #region Others Function
        bool CheckMenuByUserBranchProgram(long userId, long? orId, long? brId, long? prId, long? menuId);
        void GetAllOrganizationProgramBranchMenuPermission(long userProfileId, long menuId, out bool allOrganization, out bool allBranch, out bool allProgram, List<long> organizationIdList = null);
        List<UserMenu> LoadDetailsObservePermission(long userProfileId, long menuId);
        //List<UserMenu> LoadUserMenuForPermission(long userProfileId, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null);

        #endregion
    }
    public class UserMenuService :BaseService,IUserMenuService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UserAuthService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IUserMenuDao _userMenuDao;
        private readonly IMenuDao _menuDao;
        private readonly IOrganizationDao _organizationDao;

        public UserMenuService(ISession session)
        {
            Session = session;
            _userMenuDao = new UserMenuDao() { Session = Session };
            _menuDao = new MenuDao(){Session = Session};
            _organizationDao = new OrganizationDao() { Session = Session };
        }
        #endregion

        #region Operational Function
        public void Save(UserMenu erpUserMenu)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    _userMenuDao.Save(erpUserMenu);
                    transaction.Commit();
                }

            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        public void Delete(long id, UserMenu usermenu)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    
                    usermenu.Menu.UserMenus.Remove(usermenu);
                    _menuDao.Update(usermenu.Menu);
                    
                    //_userMenuDao.Delete(usermenu);
                    
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw;
            }
        }
        #endregion

        #region Bulk Operational Function

        public void BulkUserPermissionSet(long createBy, List<UserProfile> userProfileList, long[] organizationIdList, long[] programIdList, long[] branchIdList, List<long> menuIdList)
        {
            const long allId = -1;

            #region ------Validateion & UserMenu template generate-------
            ////Validate ORG, Program & Branch Data
            ////Generate template by valid data

            var umListByOPB = new List<UserMenu>();
            var orgList = _organizationDao.LoadOrganization(true);
            foreach (var orgId in organizationIdList)
            {
                foreach (var proId in programIdList)
                {
                    foreach (var brId in branchIdList)
                    {
                        if (orgId == allId && proId == allId && brId == allId)
                        {
                            //1 time (0, 0, 0)
                            umListByOPB.Add(new UserMenu());
                        }
                        else
                        {
                            var obj = new Organization();
                            //7 times
                            if (proId == allId)
                            {
                                obj = (from org in orgList
                                         //  from pro in org.Programs
                                           from br in org.Branches
                                           where (orgId == allId || org.Id == orgId)
                                          //       && (proId == allId || pro.Id == proId)
                                                 && (brId == allId || br.Id == brId)
                                           select org).FirstOrDefault();
                            }
                            else
                            {

                                obj = (from org in orgList
                                    from pro in org.Programs
                                    from br in org.Branches
                                    where (orgId == allId || org.Id == orgId)
                                          && (proId == allId || pro.Id == proId)
                                          && (brId == allId || br.Id == brId)
                                    select org).FirstOrDefault();
                            }
                            if (obj != null)
                            {
                                var um = new UserMenu
                                {
                                    Organization = new Organization() { Id = obj.Id },
                                    Program = proId == allId ? null : new Program() { Id = proId },
                                    Branch = brId == allId ? null : new Branch() { Id = brId }
                                };
                                umListByOPB.Add(um);
                            }
                        }
                    }
                }
            }

            //Set MenuList
            var umListTemplate = new List<UserMenu>();
            foreach (var menuId in menuIdList)
            {
                foreach (var um in umListByOPB)
                {
                    var obj = (UserMenu)um.Clone();
                    obj.Menu = new Menu() { Id = menuId };
                    umListTemplate.Add(obj);
                }
            }

            //filter valid orgIdList
            var organizationIdListFiltered = new List<long>();
            var hasAll = umListByOPB.Count(x => x.Organization == null) > 0;
            if (hasAll)
                organizationIdListFiltered.Add(allId);
            else
                organizationIdListFiltered = (from um in umListByOPB
                                              where um.Organization != null
                                              select um.Organization.Id).Distinct().ToList();

            #endregion


            #region ------Set Permission for each User-------

            foreach (var userProfile in userProfileList)
            {
                //Get cuttent permission from DB
                var curUpList = _userMenuDao.LoadByProfile(userProfile) ?? new List<UserMenu>();
                var removeUpListDb = new List<UserMenu>();
                var filterUpListDb = new List<UserMenu>();

                #region Find db permission by selected criteria

                //filter permission by selected criteria
                if (organizationIdListFiltered.Contains(allId) && (programIdList.Contains(allId) && (branchIdList.Contains(allId))))
                {
                    /* [Case] => [All All All]
                     * [Condition] =>
                     * Select All Previous Data and insert
                     * Select [All All All]
                    */

                    filterUpListDb = (
                                        from up in curUpList
                                        where (
                                            menuIdList.Contains(up.Menu.Id)
                                            || (!menuIdList.Contains(up.Menu.Id)
                                                && up.Organization == null && up.Program == null && up.Branch == null
                                            )
                                        )
                                        select up).ToList();
                }
                else if (programIdList.Contains(allId) && (branchIdList.Contains(allId)))
                {
                    /* [Case] => [Org All All]
                     * [Condition] =>
                     * Select [All All All]
                     * Select All Where Org = this Org
                     * Select [Org All All]
                     */

                    filterUpListDb = (
                                        from up in curUpList
                                        where (
                                            (menuIdList.Contains(up.Menu.Id) && up.Organization == null && up.Program == null && up.Branch == null)
                                            || (menuIdList.Contains(up.Menu.Id) && up.Organization != null && organizationIdListFiltered.Contains(up.Organization.Id))
                                            || (!menuIdList.Contains(up.Menu.Id)
                                                && up.Organization != null && organizationIdListFiltered.Contains(up.Organization.Id)
                                                && up.Program == null && up.Branch == null
                                            )
                                        )
                                        select up).ToList();
                }
                else if (programIdList.Contains(allId))
                {
                    /* [Case] => [Org All Br]
                     * [Condition] =>
                     * Select [All All All]
                     * Select All Where Org = this Org And (Br = this Br or Br = All)
                     * Select [Org All Br]
                     */

                    filterUpListDb = (
                                        from up in curUpList
                                        where (
                                            (menuIdList.Contains(up.Menu.Id) && up.Organization == null && up.Program == null && up.Branch == null)
                                            || (up.Organization != null && organizationIdListFiltered.Contains(up.Organization.Id)
                                                && up.Program == null
                                                && (up.Branch != null && branchIdList.Contains(up.Branch.Id))
                                            )
                                            || (menuIdList.Contains(up.Menu.Id)
                                                && up.Organization != null && organizationIdListFiltered.Contains(up.Organization.Id)
                                                && (up.Branch != null && branchIdList.Contains(up.Branch.Id))
                                            )
                                            || (!menuIdList.Contains(up.Menu.Id)
                                                && up.Organization != null && organizationIdListFiltered.Contains(up.Organization.Id)
                                                && up.Program != null && programIdList.Contains(up.Program.Id)
                                                && up.Branch == null
                                            )
                                        )
                                        select up).ToList();
                }
                else if (branchIdList.Contains(allId))
                {
                    /* [Case] => [Org Pro All]
                     * [Condition] =>
                     * Select [All All All]
                     * Select All Where Org = this Org And (Pro = this Pro or Pro = All)
                     * Select [Org Pro All]
                     */

                    filterUpListDb = (
                                        from up in curUpList
                                        where (
                                            (menuIdList.Contains(up.Menu.Id) && up.Organization == null && up.Program == null && up.Branch == null)
                                            || (/*menuIdList.Contains(up.Menu.Id) && */
                                                up.Organization != null && organizationIdListFiltered.Contains(up.Organization.Id)
                                                && up.Branch == null
                                                && (up.Program != null && programIdList.Contains(up.Program.Id))
                                            )
                                            || (menuIdList.Contains(up.Menu.Id)
                                                && up.Organization != null && organizationIdListFiltered.Contains(up.Organization.Id)
                                                && (up.Program != null && programIdList.Contains(up.Program.Id))
                                            )
                                            || (!menuIdList.Contains(up.Menu.Id)
                                                && up.Organization != null && organizationIdListFiltered.Contains(up.Organization.Id)
                                                && up.Program == null
                                                && up.Branch != null && branchIdList.Contains(up.Branch.Id)
                                            )
                                        )
                                        select up).ToList();
                }
                else
                {
                    /* [Case] => [Org Pro Br]
                     * [Condition] =>
                     * Select [All All All]
                     * Select All Where Org = this Org 
                            * And (Pro = this Pro or Pro = All) 
                            * And (Br = this Br or Br = All)
                     * Select [Org Pro Br]
                     */

                    filterUpListDb = (
                                        from up in curUpList
                                        where (
                                            (menuIdList.Contains(up.Menu.Id) && up.Organization == null && up.Program == null && up.Branch == null)
                                            || (menuIdList.Contains(up.Menu.Id)
                                                && up.Organization != null && organizationIdListFiltered.Contains(up.Organization.Id)
                                                && (up.Program == null || programIdList.Contains(up.Program.Id))
                                                && (up.Branch == null || branchIdList.Contains(up.Branch.Id))
                                            )
                                            || (!menuIdList.Contains(up.Menu.Id)
                                                && up.Organization != null && organizationIdListFiltered.Contains(up.Organization.Id)
                                                && up.Program != null && programIdList.Contains(up.Program.Id)
                                                && up.Branch != null && branchIdList.Contains(up.Branch.Id)
                                            )
                                        )
                                        select up).ToList();
                }
                #endregion

                #region Generate New & Removable permission list

                //Find removable permission
                var removeExcept = (from first in filterUpListDb
                                    join second in umListTemplate
                                        on first.Menu.Id equals second.Menu.Id
                                    where
                                        ((first.Organization == null && second.Organization == null) ||
                                         (first.Organization != null && second.Organization != null &&
                                          first.Organization.Id == second.Organization.Id))
                                        && ((first.Program == null && second.Program == null) ||
                                            (first.Program != null && second.Program != null &&
                                             first.Program.Id == second.Program.Id))
                                        && ((first.Branch == null && second.Branch == null) ||
                                            (first.Branch != null && second.Branch != null &&
                                             first.Branch.Id == second.Branch.Id))
                                    select first).ToList();
                var deSelectUpList = filterUpListDb.Except(removeExcept).ToList();
                removeUpListDb.AddRange(deSelectUpList);


                //Find new permission
                var newExcept = (from first in umListTemplate
                                 join second in filterUpListDb
                                     on first.Menu.Id equals second.Menu.Id
                                 where
                                     ((first.Organization == null && second.Organization == null) ||
                                      (first.Organization != null && second.Organization != null &&
                                       first.Organization.Id == second.Organization.Id))
                                     && ((first.Program == null && second.Program == null) ||
                                         (first.Program != null && second.Program != null &&
                                          first.Program.Id == second.Program.Id))
                                     && ((first.Branch == null && second.Branch == null) ||
                                         (first.Branch != null && second.Branch != null &&
                                          first.Branch.Id == second.Branch.Id))
                                 select first).ToList();
                var newUpList = umListTemplate.Except(newExcept).ToList();

                #endregion

                #region Generate SQL

                #region Remove unwanted permission from DB SQL

                var deleteSqlList = new List<string>();
                int removeSplitSize = 5000;
                var idList = (from um in removeUpListDb select um.Id + "").ToList();
                while (idList.Any())
                {
                    var splitIdList = idList.Take(removeSplitSize).ToList();
                    idList = idList.Skip(removeSplitSize).ToList();
                    var removeIdCsv = string.Join(",", splitIdList);
                    var deleteSql = "DELETE FROM [UserMenu] WHERE [Id] IN (" + removeIdCsv + "); " + Environment.NewLine;
                    deleteSqlList.Add(deleteSql);
                }
                #endregion

                #region Add new permission to DB SQL
                //1000 row per query
                var insertSqlList = new List<string>();
                string insertSqlPre = @"INSERT INTO [UserMenu]([VersionNumber],[CreationDate],[ModificationDate], 
                                [Status],[CreateBy],[ModifyBy],
                                [MenuId],[BranchId],[ProgramId],[UserProfileId],[OrganizationId]) VALUES ";
                string insertSql = insertSqlPre;
                int insertCount = 0;
                foreach (var newUp in newUpList)
                {
                    if (insertCount >= 999)
                    {
                        insertCount = 0;
                        insertSql = insertSql.Substring(0, insertSql.Length - 2) + "; ";
                        insertSqlList.Add(insertSql);
                        insertSql = insertSqlPre;
                    }

                    insertSql += Environment.NewLine;
                    insertSql += string.Format(@"(1,GETDATE(),GETDATE(),{0},{1},{1},{2},{3},{4},{5},{6}), ",
                        UserMenu.EntityStatus.Active,
                        createBy,
                        newUp.Menu.Id,
                        newUp.Branch == null ? "NULL" : newUp.Branch.Id.ToString(),
                        newUp.Program == null ? "NULL" : newUp.Program.Id.ToString(),
                        userProfile.Id,
                        newUp.Organization == null ? "NULL" : newUp.Organization.Id.ToString()
                        );
                    insertCount++;
                }
                if (insertCount > 0)
                {
                    insertCount = 0;
                    insertSql = insertSql.Substring(0, insertSql.Length - 2) + "; ";
                    insertSqlList.Add(insertSql);
                    insertSql = "";
                }
                #endregion

                #endregion

                #region DB Operation

                ITransaction transaction = null;
                try
                {
                    using (transaction = Session.BeginTransaction())
                    {
                        //delete
                        foreach (var sql in deleteSqlList)
                            _userMenuDao.ExecuteQuery(sql);

                        //insert
                        foreach (var sql in insertSqlList)
                            _userMenuDao.ExecuteQuery(sql);

                        transaction.Commit();
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    if (transaction != null && transaction.IsActive)
                        transaction.Rollback();

                    throw;
                }

                #endregion
            }

            #endregion
        }

        #endregion

        #region Single Instances Loading Function
        public UserMenu LoadById(long id)
        {
            try
            {
                return _userMenuDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public UserMenu LoadDataByUserBranchProgram(long userId, long? orId, long? brId, long? prId, long? menuId)
        {
            try
            {
                return _userMenuDao.LoadDataByUserBranchProgram(userId, orId, brId, prId, menuId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function
        public IList<UserMenu> LoadDataByUserBranchProgramOrganization(long[] listUser, long[] organizationIds, long[] program, long[] branch)
        {
            try
            {
                return _userMenuDao.LoadDataByUserBranchProgramOrganization(listUser,organizationIds, program, branch);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public List<UserMenu> LoadUsrMenuByUsrAndMenuNullBP(UserProfile userProfile, Menu menu, long Id)
        {
            try
            {
                return _userMenuDao.LoadUsrMenuByUsrAndMenuNullBP(userProfile, menu, Id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public List<UserMenu> LoadByProfile(UserProfile userProfile)
        {
            return _userMenuDao.LoadByProfile(userProfile);
        }
        #endregion

        #region Others Function
        public bool CheckMenuByUserBranchProgram(long userId, long? orId, long? brId, long? prId, long? menuId)
        {
            return _userMenuDao.CheckMenuByUserBranchProgram(userId, orId, brId, prId, menuId);
        }

        public void GetAllOrganizationProgramBranchMenuPermission(long userProfileId, long menuId, out bool allOrganization, out bool allBranch, out bool allProgram, List<long> organizationIdList = null)
        {
            try
            {
                allOrganization = false;
                allBranch = false;
                allProgram = false;

                List<UserMenu> userMenuList = _userMenuDao.LoadUserMenuByUserProfileIdAndMenuId(userProfileId, menuId, (organizationIdList != null && (!organizationIdList.Contains(-1)) ? organizationIdList : null));

                if (userMenuList != null)
                {
                    if (userMenuList.Where(x => x.Organization != null).Count()== 0)
                        allOrganization = true;
                    if (userMenuList.Where(x => x.Branch != null).Count() == 0)
                        allBranch = true;
                    if (userMenuList.Where(x => x.Program != null).Count() == 0)
                        allProgram = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<UserMenu> LoadDetailsObservePermission(long userProfileId, long menuId)
        {
            try
            {

                List<UserMenu> userMenuList = _userMenuDao.LoadDetailsObservePermission(userProfileId, menuId);
                return userMenuList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        //public List<UserMenu> LoadUserMenuForPermission(long userProfileId, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null)
        //{
        //    try
        //    {
        //        List<UserMenu> userMenuList = _userMenuDao.LoadUserMenuForPermission(userProfileId, (organizationIdList != null && (!organizationIdList.Contains(-1)) ? organizationIdList : null), (programIdList != null && (!programIdList.Contains(-1)) ? programIdList : null), (branchIdList != null && (!branchIdList.Contains(-1)) ? branchIdList : null));

        //        return userMenuList;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}

        #endregion
    }
}
