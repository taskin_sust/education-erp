﻿using System;
using System.Configuration;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Mapping.MediaMapping;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.MediaDb;
using Configuration = NHibernate.Cfg.Configuration;
using System.Reflection;

namespace UdvashERP.Services.Helper
{
    public static class  NHibernateSessionFactory
    {
        private static ISessionFactory _sessionFactory;
        private static FluentConfiguration cfg;
        private static Configuration config;
        public static ISessionFactory GetSessionFactory()
        {
            try
            {
                if (_sessionFactory == null)                
                {
                    cfg = Fluently.Configure()
                        .Database(MsSqlConfiguration.MsSql2012.ConnectionString(DbConnectionString.UerpDbConnectionString).ShowSql())
                        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<StudentDao>().Conventions.AddFromAssemblyOf<StudentDao>());
                    _sessionFactory = cfg.BuildSessionFactory();

                    //NOTE: Before calling this method ensure that ASP.Net membership related tables are created. 
                    //CreateUserProfileForeginKey(_sessionFactory.OpenSession());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            return _sessionFactory;
        }

        private static void BuildSchema(Configuration cfg)
        {
            try
            {
                var se = new SchemaExport(cfg);
                se.Execute(true, true, false);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        private static void CreateUserProfileForeginKey(ISession session)
        {
            const string fkQuery = @"ALTER TABLE UserProfile
                                    ADD CONSTRAINT fk_UserProfile_AspNetUser
                                    FOREIGN KEY (AspNetUserId)
                                    REFERENCES AspNetUsers(Id)";

            var query = session.CreateSQLQuery(fkQuery);
            query.ExecuteUpdate();
            session.Close();

        }

        public static ISession OpenSession()
        {
            return GetSessionFactory().OpenSession();
        }

        public static String GetTableName(Type entityType)
        {
            if (config == null)
            {
                config = cfg.BuildConfiguration();
            }
            return config.GetClassMapping(entityType).RootTable.Name;
        }
    }

    public static class NHibernateMediaSessionFactory
    {
        private static ISessionFactory _sessionFactory;

        public static ISessionFactory GetSessionFactory()
        {
            try
            {
                if (_sessionFactory == null)
                {
                    _sessionFactory = Fluently.Configure().
                        Database(MsSqlConfiguration.MsSql2012.ConnectionString(DbConnectionString.MediaDbConnectionString)
                           .ShowSql())
                       //Database(MsSqlConfiguration.MsSql2012.ConnectionString(ConfigurationManager
                       //    .ConnectionStrings["UdvashErpMediaConnectionString"].ConnectionString)
                       //    .ShowSql())
                       .Mappings(m => m.FluentMappings.AddFromAssemblyOf<StudentMediaImageMap>())
                       .BuildSessionFactory();
                    //NOTE: Before calling this method ensure that ASP.Net membership related tables are created. 
                    //CreateUserProfileForeginKey(_sessionFactory.OpenSession());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            return _sessionFactory;
        }

        public static ISession OpenSession()
        {
            return GetSessionFactory().OpenSession();
        }
    }

}

//.Database(
//    MsSqlConfiguration.MsSql2008.ConnectionString(
//        x => x
//            .Server("TASKIN_OSL")
//            .Database("udvash_erp_test")
//            .Username("taskin")
//            .Password("123456")
//        )
//)

//.Database(
//    MsSqlConfiguration.MsSql2008.ConnectionString(
//        x => x
//            .Server("192.168.0.7")
//            .Database("udvash_erp_v1")
//            .Username("coder")
//            .Password("i_am_coder")
//        )
//)