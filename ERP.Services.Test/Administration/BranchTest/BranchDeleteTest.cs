﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BranchTest
{
    interface IBranchDeleteTest
    {
        #region Basic Test
        void Should_Delete_Successfully();
        void Delete_Should_Return_Null_Exception();
        //void Delete_Holiday_Setting_Should_Not_Delete_Organization();
        void Branch_Delete_Should_Not_Delete_organization();

        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Branch")]
    public class BranchDeleteTest : BranchBaseTest, IBranchDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            BranchFactory branchFactory =
                    _branchFactory.Create()
                    .WithOrganization()
                    .WithNameAndRank(_name, 1)
                    .Persist();
            Branch branch = _branchService.GetBranch(branchFactory.Object.Id);
            var success = _branchService.IsDelete(branch.Id);
            Assert.True(success);
            branch = _branchService.GetBranch(branchFactory.Object.Id);
            Assert.Null(branch);

        }

        [Fact]
        public void Delete_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _branchService.IsDelete(0));
        }
        [Fact]
        public void Branch_Delete_Should_Not_Delete_organization()
        {
            _branchFactory.Create().WithNameAndRank(_name, 1).WithOrganization().Persist();
            _branchService.IsDelete(_branchFactory.Object.Id);
            Organization org = _organizationService.LoadById(_branchFactory.Object.Organization.Id);
            Assert.NotNull(org);
        }
       
        //*********should be modified later***************//
        //[Fact]
        //public void Delete_Should_Throw_Dependency_Exception()
        //{
        //    BranchFactory branchFactory =
        //         _branchFactory.Create()
        //                       .WithOrganization()
        //                       .WithNameAndRank("", 1)
        //                       .Persist();
        //    Branch branch = _branchService.GetBranch(branchFactory.Object.Id);
        //    branch.Programs.Add(new Program());
        //    branch.Branches.Add(new Branch());
        //    branch.Teachers.Add(new Teacher());
        //    branch.ExtraCurricularActivities.Add(new ExtraCurricularActivity());
        //    branch.SmsMasks.Add(new SmsMask());
        //    branch.YearlyHolidays.Add(new YearlyHoliday());
        //    branch.MemberLeaveSummaries.Add(new MemberLeaveSummary());
        //    Assert.Throws<DependencyException>(() => _branchService.IsDelete(branch.Id));
        //}

        #endregion

    }
}
