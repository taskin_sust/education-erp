﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using UdvashERP.FingerPrint.ApiReference;

namespace UdvashERP.FingerPrint.Helper
{
    public class HttpHelper
    {
        public string GetRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                }
                throw;
            }
        }


        public Response HttpPostRequestX(string url, string data)
        {
            Response respon = new Response();
            string output = String.Empty;
            var req = System.Net.WebRequest.Create(url);
            try
            {
                req.Method = "POST";
                req.Timeout = 1000000;
                req.ContentType = "application/x-www-form-urlencoded";
                byte[] sentData = Encoding.ASCII.GetBytes(data);
                req.ContentLength = sentData.Length;
                using (var sendStream = req.GetRequestStream())
                {
                    sendStream.Write(sentData, 0, sentData.Length);
                    sendStream.Close();
                }
                var response = req.GetResponse();
                output = new StreamReader(response.GetResponseStream()).ReadToEnd();

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                respon = serializer.Deserialize<Response>(output);
            }
            catch (WebException ex)
            {

                throw;
            }
            catch (Exception ex)
            {

                throw;
            }
            return respon;
        }

        public void PostRequest(string url, string jsonContent)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";



            //System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            //Byte[] byteArray = encoding.GetBytes(jsonContent);

            //request.ContentLength = byteArray.Length;
            //request.ContentType = @"application/json";

            //using (Stream dataStream = request.GetRequestStream())
            //{
            //    dataStream.Write(byteArray, 0, byteArray.Length);
            //}
            //long length = 0;
            try
            {
                //    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                //    {
                //        length = response.ContentLength;
                //    }




            }

            catch (WebException ex)
            {
                // Log exception and throw as for GET example above
            }
        }
    }
}
