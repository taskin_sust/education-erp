﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Exam;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.Services.Base;
namespace UdvashERP.Services.Exam
{
    public interface IExamsSettings : IBaseService
    {
        #region Operational Functions
        void Save(IList<ExamsSettings> examsSettings);

        void Update(ExamsSettings examsSettings);
        #endregion
       
        #region List Loading Functions
        IList<ExamsSettings> LoadByType(int type); 
        #endregion   
    }

    public class ExamsSettingsService : BaseService, IExamsSettings
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("ExamService");
        #endregion
        
        #region Propertise & Object Initialization
        private readonly IExamsSettingsDao _examsSettingsDao;
        public ExamsSettingsService(ISession session)
        {
            Session = session;
            _examsSettingsDao = new ExamsSettingsDao() { Session = session };

        } 
        #endregion
       
        #region Operational Function
        public void Save(IList<ExamsSettings> examSettingsList)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {

                    for (int i = 0; i < 3; i++)
                    {
                        _examsSettingsDao.Save(examSettingsList[i]);

                    }

                    transaction.Commit();
                    //return true;
                }
            }
            catch (Exception ex)
            {
               _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        public void Update(ExamsSettings examsSettings)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    var examsSettingsObj = _examsSettingsDao.LoadById(examsSettings.Id);
                    examsSettingsObj.Status = ExamsSettings.EntityStatus.Active;
                    _examsSettingsDao.Update(examsSettingsObj);
                    transaction.Commit();
                    //return true;
                }
            }
            catch (Exception ex)
            {
               _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        } 
        #endregion
       
        #region List Loading Functions
        public IList<ExamsSettings> LoadByType(int type)
        {
            try
            {
                return _examsSettingsDao.LoadByType(type);
            }
            catch (Exception ex)
            {
               _logger.Error(ex); 
                throw ex;
            }
        } 
        #endregion    
    }
}
