﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.GoodsReturnTest
{
    public interface IGoodsReturnReportTest
    {
        void Load_WorkOrder_Should_Return_List();
    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReturn")]
    public class GoodsReturnReportTest : GoodsReturnBaseTest, IGoodsReturnReportTest
    {
        #region Basic Test

        [Fact]
        public void Load_WorkOrder_Should_Return_List()
        {
            _goodsReturnFactory.CreateMore(5).Persist();
            var goodsReturn = _goodsReturnFactory.GetLastCreatedObjectList()[0];
            var goodreceivedetails = goodsReturn.GoodsReceive.GoodsReceiveDetails[0];
            var purposeList = new List<long> { (long)goodreceivedetails.Purpose };
            var itemGroupIdList = new List<long> { goodreceivedetails.Item.ItemGroup.Id };
            var itemIdList = new List<long> { goodreceivedetails.Item.Id };
            var branch = goodsReturn.GoodsReceive.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = goodreceivedetails.Program;
            var sessionObj = goodreceivedetails.Session;
            var supplierIdList = new List<long> { goodreceivedetails.GoodsReceive.Supplier.Id };
            var programIdList = new List<long> { program.Id };
            var sessionIdList = new List<long> { sessionObj.Id };
            var branchIdList = new List<long> { branchId };
            var columnList = new List<string> { program.ShortName };
            var usermenu = BuildUserMenu(branch.Organization, program, branch);
            var grList = _goodsReturnService.LoadGoodsReturnReport(0, 5, usermenu, organizationId, branchIdList,
                purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, supplierIdList, DateTime.Now,
                DateTime.Now.AddDays(1), columnList, 1,
                new List<string> { program.Id + "::" + sessionObj.Id });
            Assert.True(grList.Count > 0);
        }
        [Fact]
        public void Load_WorkOrder_Count_Should_Be_Greater_Than_Zero()
        {
            _goodsReturnFactory.CreateMore(5).Persist();
            var goodsReturn = _goodsReturnFactory.GetLastCreatedObjectList()[0];
            var goodreceivedetails = goodsReturn.GoodsReceive.GoodsReceiveDetails[0];
            var purposeList = new List<long> { (long)goodreceivedetails.Purpose };
            var itemGroupIdList = new List<long> { goodreceivedetails.Item.ItemGroup.Id };
            var itemIdList = new List<long> { goodreceivedetails.Item.Id };
            var branch = goodsReturn.GoodsReceive.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = goodreceivedetails.Program;
            var sessionObj = goodreceivedetails.Session;
            var supplierIdList = new List<long> { goodreceivedetails.GoodsReceive.Supplier.Id };
            var programIdList = new List<long> { program.Id };
            var sessionIdList = new List<long> { sessionObj.Id };
            var branchIdList = new List<long> { branchId };
            var columnList = new List<string> { program.ShortName };
            var usermenu = BuildUserMenu(branch.Organization, program, branch);
            var count = _goodsReturnService.GetGoodsReturnReportCount(usermenu, organizationId, branchIdList,
                purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, supplierIdList, DateTime.Now,
                DateTime.Now.AddDays(1), columnList, 1,
                new List<string> { program.Id + "::" + sessionObj.Id });
            Assert.True(count > 0);
        }
        #endregion
    }
}

