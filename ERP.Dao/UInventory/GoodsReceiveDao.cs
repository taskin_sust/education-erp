using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using NHibernate;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface IGoodsReceiveDao : IBaseDao<GoodsReceive, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        GoodsReceive GetGoodsReceive(string goodsReceiveNo);

        #endregion

        #region List Loading Function

        List<ItemOpeningBalanceDetailsViewModel> LoadOpeningBalanceItem(string programSessionIdList, string goodsReceiveNo, string itemGroupId, string itemType);

        List<ItemOpeningBalanceDetailsViewModel> LoadOpeningBalanceItem(List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, string goodsReceiveNo, long? itemGroupId, long itemType);

        List<OrderedGoodsRecViewModel> LoadGoodsReceived(int draw, int start, int length, List<long> authOrgList, List<long> authProgramList, List<long> authBrList, long organizationId, long branchId, string itemName, string receivedDate, string workOrderNo, string workOrderDate, string goodStatus);

        IList<GoodsReceive> LoadGoodsReceiveByWorkOrder(long workOrderId);

        IList<SupplierGoodsSummaryReportDto> LoadSuppliersReceivedGoodsSummary(int start, int length, string orderBy, string orderDir,
                            long[] branchIds, int[] purposeIds, long[] itemGroupIds, long[] itemIds, string[] programSession, long[] supplierIds, DateTime dateFrom, DateTime dateTo);


        #endregion

        #region Others Function

        int CountGoodsReceive(List<long> authOrgList, List<long> authProgramList, List<long> authBrList, long organizationId, long branchId, string itemName, string receivedDate, string workOrderNo, string workOrderDate, string goodStatus);

        int CountSuppliersReceivedGoodsCount(long[] bIds, int[] purposeIds, long[] itemGroupIds, long[] itemIds, string[] programSession, long[] supplierIds, DateTime dateFrom, DateTime dateTo);

        #endregion

    }

    public class GoodsReceiveDao : BaseDao<GoodsReceive, long>, IGoodsReceiveDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public GoodsReceive GetGoodsReceive(string goodsReceiveNo)
        {
            var goodsReceive = Session.QueryOver<GoodsReceive>().Where(x => x.GoodsReceivedNo == goodsReceiveNo).Take(1).List<GoodsReceive>().FirstOrDefault();
            return goodsReceive;
        }

        #endregion

        #region List Loading Function

        public List<ItemOpeningBalanceDetailsViewModel> LoadOpeningBalanceItem(List<long> organizationIdList, List<long> programIdList, List<long> sessionIdList, string goodsReceiveNo, long? itemGroupId, long itemType)
        {
            string organization = "";
            organization = string.Format(@" AND( OrganizationId IN (" + string.Join(",", organizationIdList) + ") OR OrganizationId IS NULL) ");

            string condition = "";
            if (programIdList == null && sessionIdList == null)
            {
                condition += @" AND (ProgramId IS NULL AND SessionId IS NULL) ";
            }
            else if (programIdList != null && sessionIdList != null)
            {
                condition += @" AND (ProgramId IN (" + string.Join(",", programIdList) + ") AND SessionId IN (" + string.Join(",", sessionIdList) + " ) ) ";
            }
            condition += organization;
            if (itemGroupId != null)
                condition += " AND ItemGroupId = " + itemGroupId;
            condition += " AND ItemType = " + itemType;

            string query = string.Format(@" SELECT ( CASE
                                                       WHEN b.goodsreceiveid IS NULL THEN 0
                                                       ELSE b.goodsreceiveid
                                                     END )              AS GoodsReceiveId,
                                                   a.itemid             AS ItemId,
                                                   a.itemname           AS ItemName,
                                                   0                    AS ReceivedQuantity,
                                                   Cast('0' AS DECIMAL) AS Rate,
                                                   Cast('0' AS DECIMAL) AS TotalCost
                                            FROM   (SELECT psi.programid,
                                                           psi.sessionid,
                                                           psi.itemid,
                                                           i.NAME AS itemName
                                                    FROM   uinv_programsessionitem AS psi
                                                           INNER JOIN [dbo].[uinv_item] AS i
                                                                   ON psi.itemid = i.id
                                                                      AND i.status = 1
                                                    WHERE  1 = 1 {0}) AS a
                                                   LEFT JOIN (SELECT a.*,
                                                                     b.id AS goodsReceiveDetailsId,
                                                                     b.itemid,
                                                                     b.programid,
                                                                     b.sessionid
                                                              FROM   (SELECT branchid,
                                                                             goodsreceivedno,
                                                                             id AS goodsReceiveId
                                                                      FROM   uinv_goodsreceive
                                                                      WHERE  [goodsreceivedno] LIKE
                                                                             '" + goodsReceiveNo + @"%') AS
                                                                     a
                                                                     INNER JOIN uinv_goodsreceivedetails AS b
                                                                             ON a.goodsreceiveid = b.goodsrecieveid) AS b
                                                          ON a.itemid = b.itemid
                                            WHERE  b.itemid IS NULL  "
                                    , condition);

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ItemOpeningBalanceDetailsViewModel>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<ItemOpeningBalanceDetailsViewModel>().ToList();
            return list;

        }


        public List<OrderedGoodsRecViewModel> LoadGoodsReceived(int draw, int start, int length, List<long> authOrgList, List<long> authProgramList, List<long> authBrList, long organizationId, long branchId, string itemName, string receivedDate, string workOrderNo, string workOrderDate, string goodStatus)
        {
            string query = GenerateGoodsReceiveQuery(authOrgList, authProgramList, authBrList, organizationId, branchId, itemName, receivedDate, workOrderNo, workOrderDate, goodStatus);
            query += @" order by SEMI_F.GoodRecDate  ";
            if (length > 0)
            {
                query += " offset " + start + " rows fetch next " + length +
                        "rows only ;";
            }
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<OrderedGoodsRecViewModel>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<OrderedGoodsRecViewModel>().ToList();
            return list;
        }

        public IList<GoodsReceive> LoadGoodsReceiveByWorkOrder(long workOrderId)
        {
            return Session.QueryOver<GoodsReceive>().Where(x => x.Workorder.Id == workOrderId).List<GoodsReceive>();
        }

        public IList<SupplierGoodsSummaryReportDto> LoadSuppliersReceivedGoodsSummary(int start, int length, string orderBy, string orderDir, long[] branchIds,
                                                                                            int[] purposeIds, long[] itemGroupIds, long[] itemIds, string[] programSession
                                                                                            , long[] supplierIds, DateTime dateFrom, DateTime dateTo)
        {
            var query = GetSupplierGoodsReceivedSummaryQuery(orderBy, orderDir, branchIds, purposeIds, programSession, itemGroupIds, itemIds, supplierIds, dateFrom, dateTo);
            var iq = Session.CreateSQLQuery(query);
            iq.SetFirstResult(start).SetMaxResults(length);
            iq.SetResultTransformer(Transformers.AliasToBean<SupplierGoodsSummaryReportDto>());
            var list = iq.List<SupplierGoodsSummaryReportDto>().ToList();
            return list;
        }

        public List<ItemOpeningBalanceDetailsViewModel> LoadOpeningBalanceItem(string programSessionIdList, string goodsReceiveNo, string itemGroupId, string itemType)
        {


            string condition = "";
            if (String.IsNullOrEmpty(programSessionIdList))
            {
                condition += @" AND (ProgramId IS NULL AND SessionId IS NULL) ";
            }
            else
            {
                var spltValue = programSessionIdList.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                condition += @" AND (ProgramId = " + Convert.ToInt64(spltValue[0]) + " AND SessionId = " + Convert.ToInt64(spltValue[1]) + ") ";
            }

            if (!String.IsNullOrEmpty(itemGroupId))
            {
                condition += " AND ItemGroupId = " + itemGroupId;
            }
            if (!String.IsNullOrEmpty(itemType))
            {
                condition += " AND ItemType = " + itemType;
            }

            string query = @"Select 
	        (CASE WHEN b.goodsReceiveId IS NULL THEN 0 ELSE b.goodsReceiveId END) as GoodsReceiveId
	        ,a.ItemId as ItemId, a.itemName as ItemName
	        ,0 as ReceivedQuantity, CAST('0' as decimal) as Rate, CAST('0' as decimal) as TotalCost
	        from (
	         SELECT psi.ProgramId,psi.SessionId,psi.ItemId,i.Name as itemName FROM [dbo].[UINV_ProgramSessionItem] as psi
	         INNER JOIN [dbo].[UINV_Item] as i ON psi.ItemId = i.Id AND i.Status = 1
	         WHERE 1=1 " + condition + @"
	         ) as a 
	         Left Join (
		         Select a.*,b.Id as goodsReceiveDetailsId, b.ItemId, b.ProgramId, b.SessionId From (
			        SELECT BranchId,GoodsReceivedNo,Id as goodsReceiveId FROM [dbo].[UINV_GoodsReceive] Where [GoodsReceivedNo] LIKE '" + goodsReceiveNo + @"%'
		         ) as a
		         Inner Join [dbo].[UINV_GoodsReceiveDetails] AS b ON a.goodsReceiveId = b.GoodsRecieveId 
	         ) as b on a.ItemId = b.ItemId
	         Where b.ItemId IS NULL";

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ItemOpeningBalanceDetailsViewModel>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<ItemOpeningBalanceDetailsViewModel>().ToList();
            return list;

            //Select 
            //(CASE WHEN b.goodsReceiveId IS NULL THEN 0 ELSE b.goodsReceiveId END) as GoodsReceiveId
            //,a.ItemId as ItemId, a.itemName as ItemName
            //,0 as ReceivedQuantity, 0 as Rate, 0 as TotalCost
            //from (
            // SELECT psi.ProgramId,psi.SessionId,psi.ItemId,i.Name as itemName FROM [dbo].[UINV_ProgramSessionItem] as psi
            // INNER JOIN [dbo].[UINV_Item] as i ON psi.ItemId = i.Id AND i.Status = 1
            // WHERE 1=1 AND ((psi.ProgramId = 62 AND psi.SessionId = 44)) --AND ItemType = 1 AND ItemGroupId = 1
            // ) as a 
            // Left Join (
            //     Select a.*,b.Id as goodsReceiveDetailsId, b.ItemId, b.ProgramId, b.SessionId From (
            //        SELECT BranchId,GoodsReceivedNo,Id as goodsReceiveId FROM [dbo].[UINV_GoodsReceive] Where [GoodsReceivedNo] = 'UDV01GR000001'
            //     ) as a
            //     Inner Join [dbo].[UINV_GoodsReceiveDetails] AS b ON a.goodsReceiveId = b.GoodsRecieveId 
            // ) as b on a.ItemId = b.ItemId
            // Where b.ItemId IS NULL



        }



        #endregion

        #region Others Function


        public int CountGoodsReceive(List<long> authOrgList, List<long> authProgramList, List<long> authBrList, long organizationId, long branchId, string itemName, string receivedDate, string workOrderNo, string workOrderDate, string goodStatus)
        {
            string query = GenerateGoodsReceiveQuery(authOrgList, authProgramList, authBrList, organizationId, branchId, itemName, receivedDate, workOrderNo, workOrderDate, goodStatus);
            string countQuery = @" SELECT COUNT(AA.GoodRecNo) FROM( ";
            query = countQuery + query + " ) AS AA";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int CountSuppliersReceivedGoodsCount(long[] branchIds, int[] purposeIds, long[] itemGroupIds, long[] itemIds,
            string[] programSession, long[] supplierIds, DateTime dateFrom, DateTime dateTo)
        {
            var query = GetSupplierGoodsReceivedSummaryQuery("", "", branchIds, purposeIds, programSession, itemGroupIds, itemIds, supplierIds, dateFrom, dateTo, true);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function

        private string GenerateGoodsReceiveQuery(List<long> authOrgList, List<long> authProgramList, List<long> authBrList, long organizationId, long branchId, string itemName, string receivedDate, string workOrderNo, string workOrderDate, string goodStatus)
        {
            String query = @" SELECT SEMI_F.*, WO.WorkOrderNo, WO.WorkOrderDate, WO.WorkOrderStatus FROM (
                            SELECT org.ShortName AS [OrganizationName], br.Name AS [BranchName] ,it.Name AS [ItemName], gd.GoodsReceivedDate AS [GoodRecDate], gd.GoodsReceivedNo AS [GoodRecNo],  br.Id as [BrId], gd.Id as [GdId],
                            gdetail.ItemId, WO.ID AS [WorkOrderId]
                            FROM [UINV_GoodsReceive]  as gd
	                                            INNER JOIN [Branch] as br
		                                            on gd.BranchId = br.Id  ";
            query += @" and br.Id in(" + string.Join(",", authBrList) + ")";
            if (branchId > 0)
            {
                query += " and br.id= " + branchId;
            }

            query += @" INNER JOIN [Organization] as org on br.OrganizationId = org.Id  ";
            query += @" and org.Id in(" + string.Join(",", authOrgList) + ") ";
            if (organizationId > 0)
            {
                query += " and org.id= " + organizationId;
            }

            query += @" INNER JOIN [UINV_GoodsReceiveDetails] gdetail 
		                                            on  gd.Id = gdetail.GoodsRecieveId ";
            query += @" and (gdetail.ProgramId in(" + string.Join(",", authProgramList) + ") or gdetail.ProgramId is null ) ";
            query += @" INNER JOIN [UINV_Item] as it 
						                            on gdetail.ItemId= it.Id ";
            if (!String.IsNullOrEmpty(itemName))
            {
                query += " and it.Name like " + "'%" + itemName + "%'";
            }
            query += @" LEFT JOIN [UINV_WorkOrder] AS WO 
						                            ON WO.Id = gd.WorkOrderId

					                            ) AS SEMI_F 
					                            LEFT JOIN [dbo].[UINV_WorkOrder] AS WO 
						                            ON SEMI_F.WorkOrderId= WO.Id WHERE";
            if (!String.IsNullOrEmpty(workOrderNo))
            {
                query += @"  WO.WorkOrderNo = " + "'" + workOrderNo + "' and";
            }
            if (!String.IsNullOrEmpty(workOrderDate))
            {
                query += @" CAST(WO.WorkOrderDate AS DATE)= '" + Convert.ToDateTime(workOrderDate).ToString("yyyy-MM-dd") + "' and";
            }
            if (!String.IsNullOrEmpty(receivedDate))
            {
                query += @" CAST(SEMI_F.GoodRecDate AS DATE)= '" + Convert.ToDateTime(receivedDate).ToString("yyyy-MM-dd") + "' and";
            }
            if (!String.IsNullOrEmpty(goodStatus))
            {
                query += @" WO.WorkOrderStatus =" + Convert.ToInt32(goodStatus) + "";
            }
            string[] words = query.Split(' ');
            if (words[words.Length - 1] == "WHERE" || words[words.Length - 1] == "and")
            {
                words = words.Take(words.Count() - 1).ToArray();
            }
            return String.Join(" ", words);

        }

        private static string GetSupplierGoodsReceivedSummaryQuery(string orderBy, string orderDir, long[] branchIds, int[] purposeIds, string[] psArray, long[] itemGroupIds, long[] itemIds, long[] supplierIds
                                                                , DateTime dateFrom, DateTime dateTo, bool isCountQuery = false)
        {
            string whereClause = " WHERE ";

            whereClause += " gr.CreationDate >= '" + dateFrom.ToShortDateString() + " 00:00:00' ";
            whereClause += " AND gr.CreationDate <= '" + dateTo.ToShortDateString() + " 23:59:59' ";

            if (branchIds.Any())
            {
                whereClause += " AND b.Id IN( " + string.Join(", ", branchIds) + ")";
            }

            if (supplierIds.Any() && supplierIds[0] > 0)
            {
                whereClause += " AND sup.Id IN( " + string.Join(", ", supplierIds) + ")";
            }

            if (purposeIds.Any() && !purposeIds.Contains(0))
            {
                if (purposeIds.Contains(-1))
                {
                    var list = purposeIds.ToList<int>();
                    list.RemoveAt(list.IndexOf(-1));
                    purposeIds = list.ToArray();
                    whereClause += purposeIds.Length > 0
                        ? " AND (grd.Purpose IN( " + string.Join(", ", purposeIds) + ") OR grd.Purpose IS NULL)"
                        : " AND grd.Purpose IS NULL ";
                }
                else
                {
                    whereClause += " AND grd.Purpose IN( " + string.Join(", ", purposeIds) + ")";
                }
            }
            if (psArray.Any() && psArray[0] != "" && psArray[0] != "0")
            {
                string psConditionsStr = "";

                foreach (var ps in psArray)
                {
                    string[] psIds = ps.Split(new string[] { "::" }, StringSplitOptions.None);
                    if (psIds.Length == 2)
                    {
                        psConditionsStr += "(p.Id = " + psIds[0] + " AND s.Id = " + psIds[1] + ")";
                    }
                    else
                    {
                        psConditionsStr += "(p.Id IS NULL AND s.Id IS NULL)";
                    }
                    psConditionsStr += " OR ";
                }
                psConditionsStr = psConditionsStr.TrimEnd();
                psConditionsStr = psConditionsStr.Substring(0, psConditionsStr.Length - 2);

                whereClause += " AND (" + psConditionsStr + ") ";
            }

            if (itemIds.Any() && itemIds[0] > 0)
            {
                whereClause += " AND i.Id IN( " + string.Join(", ", itemIds) + ")";
            }
            if (itemGroupIds.Any() && itemGroupIds[0] > 0)
            {
                whereClause += " AND ig.Id IN( " + string.Join(", ", itemGroupIds) + ")";
            }
            const string relationQuery = @" FROM UINV_GoodsReceive gr
                                INNER JOIN UINV_Supplier sup ON gr.SupplierId = sup.Id
                                INNER JOIN Branch b ON gr.BranchId = b.Id
	                            INNER JOIN UINV_GoodsReceiveDetails grd ON gr.Id = grd.GoodsRecieveId
	                            INNER JOIN UINV_Item i ON grd.ItemId = i.Id
	                            INNER JOIN UINV_ItemGroup ig ON i.ItemGroupId = ig.Id
	                            LEFT OUTER JOIN Program p ON grd.ProgramId = p.Id
	                            LEFT OUTER JOIN Session s ON grd.SessionId = s.Id";
            const string groupBy = @" GROUP BY sup.Name, ig.Name, i.Name, p.ShortName, s.Name, grd.Rate ";
            string query = "SELECT sup.Name AS SupplierName" +
                    ", ig.Name as ItemGroup, i.Name AS ItemName" +
                    ", CASE WHEN p.ShortName IS NULL THEN 'N/A' ELSE (p.ShortName + ' ' + s.Name) END AS ProgramSession" +
                    ", SUM(grd.ReceivedQuantity) as Quantity" +
                    ", grd.Rate AS Rate" +
                    ", SUM(grd.TotalCost) AS Total" +
                    ", SUM(gr.TotalCost) AS GrandTotal"
                    + relationQuery + whereClause + groupBy;
            if (!isCountQuery)
            {
                query += " ORDER BY  " + orderBy + " " + orderDir.ToUpper();

            }
            else
            {
                query = "SELECT COUNT(CountQuery.Rate) FROM (" + query + ")AS CountQuery";
            }
            return query;
        }

        #endregion

    }
}
