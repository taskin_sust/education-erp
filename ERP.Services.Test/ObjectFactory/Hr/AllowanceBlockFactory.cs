using System;
using System.Globalization;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;
namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class AllowanceBlockFactory : ObjectFactoryBase<ChildrenAllowanceBlock>
    {

        private readonly IChildrenAllowanceBlockService _prAllowanceBlockService;

        public AllowanceBlockFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _prAllowanceBlockService = new ChildrenAllowanceBlockService(session);

        }

        #region Create

        public AllowanceBlockFactory Create()
        {
            DateTime effectiveDate;
            DateTime.TryParse(DateTime.Today.ToString(CultureInfo.InvariantCulture), out effectiveDate);
            effectiveDate = new DateTime(effectiveDate.Year, effectiveDate.Month, effectiveDate.Day, 0, 0, 0);
            var endOfMonth = new DateTime(effectiveDate.Year, effectiveDate.Month, DateTime.DaysInMonth(effectiveDate.Year, effectiveDate.Month));
            var closingDate = new DateTime(endOfMonth.Year, endOfMonth.Month, endOfMonth.Day, 23, 23, 59);

            Object = new ChildrenAllowanceBlock()
            {
                EffectiveDate = effectiveDate,
                ClosingDate = closingDate,

            };

            SingleObjectList.Add(Object);
            return this;
        }

        public AllowanceBlockFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public AllowanceBlockFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public AllowanceBlockFactory WithDateTime(DateTime effectiveDate, DateTime closingDate)
        {
            Object.EffectiveDate = effectiveDate;
            Object.ClosingDate = closingDate;
            return this;
        }

        public AllowanceBlockFactory WithCkAllowance(ChildrenAllowanceSetting childrenAllowanceSetting)
        {
            Object.ChildrenAllowanceSetting = childrenAllowanceSetting;
            return this;
        }

        //public AllowanceBlockFactory Persist()       {

        //   if (SingleObjectList != null && SingleObjectList.Count > 0){

        //       SingleObjectList.ForEach(x =>{if (x.Id == 0) _prAllowanceBlockService.SaveOrUpdate(x);});
        //   }

        //   if (ObjectList != null && ObjectList.Count > 0){

        //       ObjectList.ForEach(x =>{if (x.Id == 0)_prAllowanceBlockService.SaveOrUpdate(x);});

        //   }
        //   return this;
        //}

        public AllowanceBlockFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new ChildrenAllowanceBlock() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public AllowanceBlockFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion

    }
}
