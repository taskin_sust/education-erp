﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using System.Configuration;
using NHibernate;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using UdvashERP.Services.Test.ObjectFactory.UserAuth;
using UdvashERP.Services.UserAuth;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Services.Test
{
    public class TestBase : IDisposable
    {
        public ISession Session;
        //public ISession MediaSession;

        internal AllowanceFactory allowanceFactory;
        internal AllowanceSheetFactory allowanceSheetFactory;
        internal AttendanceAdjustmentFactory attendanceAdjustmentFactory;
        internal ChildrenInfoFactory childrenInfoFactory;
        internal EmploymentHistoryFactory employmentHistoryFactory;
        internal LeaveApplicationFactory leaveApplicationFactory;
        internal LeaveFactory leaveFactory;
        internal MaritalInfoFactory maritalInfoFactory;
        internal MemberLeaveSummaryFactory memberLeaveSummaryFactory;
        internal MemberOfficialDetailFactory memberOfficialDetailFactory;
        internal MentorHistoryFactory mentorHistoryFactory;
        internal NightWorkFactory nightWorkFactory;
        internal IChildrenInfoService childrenInfoService;
        internal ISalaryHistoryService _salaryHistoryService;
        internal SalaryHistoryFactory salaryHistoryFactory;
        internal ShiftFactory shiftFactory;
        internal ShiftWeekendHistoryFactory shiftWeekendHistoryFactory;
        internal TeamMemberFactory teamMemberFactory;
        internal TeamMemberService teamMemberService;
        internal UserProfileFactory userProfileFactory;
        internal UserService userService;
        protected ZoneSettingFactory zoneSettingFactory;
        public BranchFactory branchFactory;
        public CommonHelper commonHelper;
        public OrganizationFactory organizationFactory;
        public ProgramFactory programFactory;

        public TestBase()
        {
            //for database connection string 
            DbConnectionString.UerpDbConnectionString = ConfigurationManager.ConnectionStrings["UdvashErpConnectionString"].ConnectionString;
            DbConnectionString.MediaDbConnectionString = ConfigurationManager.ConnectionStrings["UdvashErpMediaConnectionString"].ConnectionString;
            Session = NHibernateSessionFactory.OpenSession();
            allowanceFactory = new AllowanceFactory(null, Session);
            allowanceSheetFactory = new AllowanceSheetFactory(null, Session);
            attendanceAdjustmentFactory = new AttendanceAdjustmentFactory(null, Session);
            branchFactory = new BranchFactory(null, Session);
            childrenInfoFactory = new ChildrenInfoFactory(null, Session);
            childrenInfoService = new ChildrenInfoService(Session);
            commonHelper = new CommonHelper();
            employmentHistoryFactory = new EmploymentHistoryFactory(null, Session);
            leaveApplicationFactory = new LeaveApplicationFactory(null, Session);
            leaveFactory = new LeaveFactory(null, Session);
            maritalInfoFactory = new MaritalInfoFactory(null, Session);
            memberLeaveSummaryFactory = new MemberLeaveSummaryFactory(null, Session);
            memberOfficialDetailFactory = new MemberOfficialDetailFactory(null, Session);
            mentorHistoryFactory = new MentorHistoryFactory(null, Session);
            nightWorkFactory = new NightWorkFactory(null, Session);
            organizationFactory = new OrganizationFactory(null, Session);
            programFactory = new ProgramFactory(null, Session);
            salaryHistoryFactory = new SalaryHistoryFactory(null, Session);
            _salaryHistoryService = new SalaryHistoryService(Session);
            shiftFactory = new ShiftFactory(null, Session);
            shiftWeekendHistoryFactory = new ShiftWeekendHistoryFactory(null, Session);
            teamMemberFactory = new TeamMemberFactory(null, Session);
            teamMemberService = new TeamMemberService(Session);
            userProfileFactory = new UserProfileFactory(null, Session);
            userService = new UserService(Session);
            zoneSettingFactory = new ZoneSettingFactory(null, Session);

            if (AppConfigHelper.GetBoolean("NHibernateProfiler-Enable", false))
                HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
        }

        public TeamMember PersistTeamMember(int mentorId = 0)
        {
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;
            mentorId = mentorId > 0 ? mentorId : 1;

            #region leave Variable

            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 10;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;

            #endregion

            //Initially create team member without mentor for testing. Mentor as a team member  
            organizationFactory.Create().Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.Object);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.Object);
            maritalInfoFactory.CreateMore(1);

            var mentorObj = teamMemberService.GetMember(mentorId);

            var mentorEmployment = mentorObj.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit,
                maxTakingLimit, applicationtype, applicationDays, isEncash, minEncashReserveDays,
                effectiveDate, null, isMale, isFemale, isProbation, isPermanent, isPartTime, isContractual, isIntern,
                isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organizationFactory.Object)
                .Persist();

            var dept = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj, dept, designation, organizationFactory.Object
                , mentorEmployment.Department, mentorEmployment.Designation, mentorEmployment.Department.Organization);

            teamMemberFactory.Create()
                .WithEmploymentHistory(employmentHistoryFactory.ObjectList)
                .WithShiftWeekend(shiftWeekendHistoryFactory.ObjectList)
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(mentorHistoryFactory.ObjectList)
                .Persist();

            return teamMemberFactory.Object;
        }

        public TeamMember PersistTeamMember(DateTime startDate, DateTime endDate, Organization organization = null, bool isSalarySheet = false, bool isAutoDeduction = false)
        {
            //DateTime startDate = DateTime.Now;
            //DateTime endDate = DateTime.Now;
            int mentorId = 1;
            //var org = new Organization();

            #region leave Variable

            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 10;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = startDate;
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;

            #endregion

            //Initially create team member without mentor for testing. Mentor as a team member  
            var org = organization ?? organizationFactory.Create().Persist().Object;

            employmentHistoryFactory.CreateMore(1, org);
            shiftWeekendHistoryFactory.CreateWith(0, 1, startDate).WithOrganization(org).WithShift(org);
            maritalInfoFactory.CreateMore(1);

            var mentorObj = teamMemberService.GetMember(mentorId);

            var mentorEmployment = mentorObj.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit,
                maxTakingLimit, applicationtype, applicationDays, isEncash, minEncashReserveDays,
                effectiveDate, null, isMale, isFemale, isProbation, isPermanent, isPartTime, isContractual, isIntern,
                isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(org)
                .Persist();

            var dept = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();
            mentorHistoryFactory.CreateMore(1, mentorObj, dept, designation, org
                  , mentorEmployment.Department, mentorEmployment.Designation, mentorEmployment.Department.Organization, startDate);
            teamMemberFactory.Create()
                  .WithEmploymentHistory(employmentHistoryFactory.ObjectList)
                  .WithShiftWeekend(shiftWeekendHistoryFactory.SingleObjectList)
                  .WithEffectiveStartAndEndDate(startDate, endDate)
                  .WithMaritalInfo(maritalInfoFactory.ObjectList)
                  .WithMentorHistory(mentorHistoryFactory.ObjectList)
                  .WithIsSalarySheet(isSalarySheet)
                  .WithIsAutoDeduction(isAutoDeduction)
                  .Persist();
            childrenInfoService.UpdateChildrenInfo(teamMemberFactory.Object, new List<TeammemberChildrenArray>()
            {
                new TeammemberChildrenArray()
                {
                    Dob = new DateTime(2015, 10, 20).ToString(),
                    Gender = Convert.ToInt16(Gender.Male).ToString(),
                    Name = "Abdul Karim " + new Random().Next(99999),
                    Status = "1",
                    Studying = "1"
                }
            });
            _salaryHistoryService.Save(new SalaryHistory()
            {
                BankAmount = 2500,
                CashAmount = 1000,
                SalaryCampus = teamMemberFactory.Object.EmploymentHistory.FirstOrDefault().Campus,
                SalaryDepartment = teamMemberFactory.Object.EmploymentHistory.FirstOrDefault().Department,
                SalaryDesignation = teamMemberFactory.Object.EmploymentHistory.FirstOrDefault().Designation,
                SalaryOrganization = teamMemberFactory.Object.EmploymentHistory.FirstOrDefault().Department.Organization,
                Salary = 2500,
                TeamMember = teamMemberFactory.Object,
                SalaryPurpose = SalaryPurpose.BrandingPurpose,
                Rank = 10,
                Status = SalaryHistory.EntityStatus.Active,
                EffectiveDate = startDate
            });
            IList<ChildrenInfo> cinfoList = childrenInfoService.GetTeamMemberChildrenInfo(teamMemberFactory.Object.Id);
            IList<SalaryHistory> sHistoryList = _salaryHistoryService.LoadSalaryHistories(teamMemberFactory.Object);
            var teamMemberObj = teamMemberService.LoadById(teamMemberFactory.Object.Id);
            teamMemberObj.ChildrenInfo = cinfoList;
            teamMemberObj.SalaryHistory = sHistoryList;
            return teamMemberObj;
        }

        public IList<TeamMember> PersistTeamMember(int num, Organization organization, Branch branch, Campus campus, Department department, Designation designation, int mentorId = 0)
        {
            if (mentorId <= 0)
            {
                mentorId = 2;
            }
            shiftWeekendHistoryFactory.CreateMore(1, organization);
            DateTime effectiveDatex = DateTime.Parse("2016-01-01");
            //_employmentHistoryFactory.CreateMore(1, MemberEmploymentStatus.Permanent, null, effectiveDatex, campus, department, designation);
            maritalInfoFactory.CreateMore(1);

            // DateTime startDate = DateTime.Now.AddYears(-3);
            //DateTime endDate = DateTime.Now.AddYears(-1);

            #region leave Variable

            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 10;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = false;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;

            leaveFactory.CreateWith(id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward
            , maxCarryDays, isTakingLimit, maxTakingLimit, applicationtype, applicationDays, isEncash, minEncashReserveDays
            , effectiveDate, null, isMale, isFemale, isProbation, isPermanent, isPartTime, isContractual, isIntern, isSingle, isMarried
            , isWidow, isWidower, isDevorced).WithOrganization(organization).Persist();

            #endregion

            for (int i = 0; i < num; i++)
            {
                var mentorObj = teamMemberService.GetMember(mentorId);
                var mentorEmployment = mentorObj.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();
                mentorHistoryFactory.CreateMore(1, mentorObj, department, designation, organization, mentorEmployment.Department, mentorEmployment.Designation, mentorEmployment.Department.Organization);

                employmentHistoryFactory.CreateMore(1, MemberEmploymentStatus.Permanent, null, effectiveDatex, campus, department, designation);

                teamMemberFactory.Create().WithPersonalContact(i).WithEmploymentHistory(employmentHistoryFactory.ObjectList).WithShiftWeekend(shiftWeekendHistoryFactory.ObjectList).WithEffectiveStartAndEndDate(effectiveDatex, effectiveDatex)
                    .WithMaritalInfo(maritalInfoFactory.ObjectList)
                    .WithMentorHistory(mentorHistoryFactory.ObjectList);
                //_teamMemberFactory.Persist();
            }
            teamMemberFactory.Persist();
            return teamMemberFactory.SingleObjectList;
        }

        public TeamMember PersistTeamMemberForHistory()
        {
            DateTime startDate = DateTime.Now.AddDays(-15);
            DateTime endDate = DateTime.Now;

            #region leave Variable

            long id = 0;
            bool isPublic = true;
            int payType = 1;
            int repeatType = 1;
            int noOfDays = 10;
            int startFrom = 1;
            bool isCarryforward = true;
            int maxCarryDays = 10;
            bool isTakingLimit = true;
            int maxTakingLimit = 2;
            int applicationtype = 1;
            int applicationDays = 2;
            bool isEncash = false;
            int minEncashReserveDays = -2;
            DateTime effectiveDate = DateTime.Now.AddDays(-5);
            DateTime closingDate;
            bool isMale = true;
            bool isFemale = false;
            bool isProbation = true;
            bool isPermanent = true;
            bool isPartTime = false;
            bool isContractual = false;
            bool isIntern = false;
            bool isSingle = true;
            bool isMarried = false;
            bool isWidow = false;
            bool isWidower = false;
            bool isDevorced = false;

            #endregion

            //TODO:Initially create team member without mentor for testing. Mentor as a team member  
            organizationFactory.Create().Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.Object);
            shiftWeekendHistoryFactory.CreateMore(2, organizationFactory.Object);
            maritalInfoFactory.CreateMore(1);

            var mentorObj = teamMemberService.GetMember(160);
            var mentorEmployment = mentorObj.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();
            leaveFactory.CreateWith(
                id, isPublic, payType, repeatType, noOfDays, startFrom, isCarryforward, maxCarryDays, isTakingLimit,
                maxTakingLimit, applicationtype, applicationDays, isEncash, minEncashReserveDays,
                effectiveDate, null, isMale, isFemale, isProbation, isPermanent, isPartTime, isContractual, isIntern,
                isSingle, isMarried,
                isWidow, isWidower, isDevorced).WithOrganization(organizationFactory.Object)
                .Persist();

            var dept = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();

            mentorHistoryFactory.CreateMore(1, mentorObj, dept, designation, organizationFactory.Object
                , mentorEmployment.Department, mentorEmployment.Designation, mentorEmployment.Department.Organization);

            teamMemberFactory.Create()
                .WithEmploymentHistory(employmentHistoryFactory.ObjectList)
                .WithShiftWeekend(shiftWeekendHistoryFactory.ObjectList)
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(mentorHistoryFactory.ObjectList)
                .Persist();
            return teamMemberFactory.Object;
        }
        public void Dispose()
        {
            if (teamMemberFactory.Object != null)
            {
                var teamMember = teamMemberFactory.Object;
                nightWorkFactory.LogCleanUp();
                shiftWeekendHistoryFactory.LogCleanUp(teamMember);
                attendanceAdjustmentFactory.LogCleanUp();
                mentorHistoryFactory.LogCleanUp(teamMember);
                salaryHistoryFactory.CleanupTeamMemberSalaryHistoryLog(salaryHistoryFactory);
                employmentHistoryFactory.LogCleanUp(employmentHistoryFactory);
                memberOfficialDetailFactory.LogCleanUp(teamMember);
                allowanceFactory.CleanUpWithLog(teamMember);
            }
            teamMemberFactory.TeamMemberAttendanceSummaryCleanUp();
            teamMemberFactory.TeamMemberChieldCleanup();
            zoneSettingFactory.Cleanup();
            leaveApplicationFactory.CleanUp();
            leaveFactory.Cleanup();
            branchFactory.Cleanup();
            programFactory.Cleanup();
            userProfileFactory.UserProgfileWithImageCleanup(userProfileFactory.Object);
            userProfileFactory.UserProgfileWithImageCleanup(teamMemberFactory.Object);
            shiftWeekendHistoryFactory.CleanUp();
            childrenInfoFactory.CleanUp();
            if (teamMemberFactory.Object != null)
            {
                attendanceAdjustmentFactory.CleanUpByTeamMember(teamMemberFactory.Object.Id);
                childrenInfoFactory.CleanUpByTeamMember(teamMemberFactory.Object.Id);
                nightWorkFactory.CleanUpByTeamMember(teamMemberFactory.Object.Id);
                childrenInfoFactory.CleanUp();
                allowanceSheetFactory.CleanUpByTeamMember(teamMemberFactory.Object.Id);
                salaryHistoryFactory.CleanUpByTeamMember(teamMemberFactory.Object.Id);
            }
            salaryHistoryFactory.CleanUp();
            employmentHistoryFactory.CleanUpParentFactory();

            teamMemberFactory.Cleanup();
            organizationFactory.Cleanup();
        }

        #region Build UserMenu for Test
        public List<UserMenu> BuildUserMenu(Organization org, Program program, Branch branch)
        {
            return BuildUserMenu(CommonHelper.ConvertToList<Organization>(org), CommonHelper.ConvertToList<Program>(program), CommonHelper.ConvertToList<Branch>(branch));
        }

        public List<UserMenu> BuildUserMenu(Organization org, Program program, List<Branch> branchList)
        {
            return BuildUserMenu(CommonHelper.ConvertToList<Organization>(org), CommonHelper.ConvertToList<Program>(program), branchList);
        }

        public List<UserMenu> BuildUserMenu(Organization org, List<Program> programList, Branch branch)
        {
            return BuildUserMenu(CommonHelper.ConvertToList<Organization>(org), programList, CommonHelper.ConvertToList<Branch>(branch));
        }

        public List<UserMenu> BuildUserMenu(Organization org, List<Program> programList, List<Branch> branchList)
        {
            return BuildUserMenu(CommonHelper.ConvertToList<Organization>(org), programList, branchList);
        }

        public List<UserMenu> BuildUserMenu(List<Organization> orgList, List<Program> programList = null, List<Branch> branchList = null)
        {
            var userMenuList = new List<UserMenu>();

            //no organization so allow all
            if (orgList == null || orgList.Count == 0)
            {
                userMenuList.Add(new UserMenu());
                return userMenuList;
            }

            //for each organization 
            foreach (var org in orgList)
            {
                var filterProList = new List<Program>();
                if (programList != null)
                    filterProList = (from p in programList
                                     where p.Organization.Id == org.Id
                                     select p).ToList();

                var filterBrList = new List<Branch>();
                if (branchList != null)
                    filterBrList = (from b in branchList
                                    where b.Organization.Id == org.Id
                                    select b).ToList();

                //program & branch both has data
                if (programList != null && programList.Any() && branchList != null && branchList.Any())
                {
                    foreach (var pro in filterProList)
                    {
                        foreach (var br in filterBrList)
                        {
                            var um = new UserMenu();
                            um.Organization = org;
                            um.Program = pro;
                            um.Branch = br;
                            userMenuList.Add(um);
                        }
                    }
                }
                else if (programList != null && programList.Any())
                {
                    //only program has data
                    foreach (var pro in filterProList)
                    {
                        var um = new UserMenu();
                        um.Organization = org;
                        um.Program = pro;
                        userMenuList.Add(um);
                    }
                }
                else if (branchList != null && branchList.Any())
                {
                    //only branch has data
                    foreach (var br in filterBrList)
                    {
                        var um = new UserMenu();
                        um.Organization = org;
                        um.Branch = br;
                        userMenuList.Add(um);
                    }
                }
                else
                {
                    var um = new UserMenu();
                    um.Organization = org;
                    userMenuList.Add(um);
                }
            }

            //return final list
            return userMenuList;
        }
        #endregion

    }
}
