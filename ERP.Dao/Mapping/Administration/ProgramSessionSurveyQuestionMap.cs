﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public partial class ProgramSessionSurveyQuestionMap: BaseClassMap<ProgramSessionSurveyQuestion,long> 
    {
        public ProgramSessionSurveyQuestionMap()
        {
            Table("ProgramSessionSurveyQuestion");
            LazyLoad();
            Map(x => x.SurveyType).Column("SurveyType");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.SurveyQuestion).Column("SurveyQuestionId");
        }
    }
}
