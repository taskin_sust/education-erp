﻿function CheckEmpty(className, message) {

    var flag = true;

    $('.' + className).each(function (index, value) {
        if ($(this).val() == "") {
            //alert(message);
            this.focus();
            $(this).addClass("error");
            flag = false;
        }

        if (flag == false)
            return false;
    });

    return flag;
}

function IsValidMobile(mobileNo) {
    var validMobileNo = new RegExp(/^(88)01[15-9]\d{8}$/);
    if (validMobileNo.test(mobileNo)) {
        return true;
    } else {
        return false;
    }
}

function CheckValues(className, message) {

    var flag = true;

    $('.' + className).each(function (index, value) {
        if ($(this).val() == "") {
            //alert(message);
            this.focus();
            $(this).addClass("error");
            flag = false;
        }

        if (!isItNumber($(this).val())) {
            // alert(message);
            this.focus();
            $(this).addClass("error");
            flag = false;
        };

        if (flag == false)
            return false;
    });

    return flag;
}

function isItNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}


$(document).on('change', '#OrganizationId', function (e) {
    $("#BranchId option").remove();
    $('#BranchId').append("<option value=''>Select Branch</option>");
    var orgId = $("#OrganizationId").val();
  
    if (orgId != "") {
        $.ajax({
            type: "POST",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
            cache: false,
            async: true,
            data: { organizationIds: orgId, isAuthorized: true, isProgramBranSession: false },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    $.each(result.returnBranchList, function (i, v) {
                        $('#BranchId').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
                else {
                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "error",
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }

});




$(document).ready(function () {

    // Add button click handler
    var counter = 1;
    $(document).on('click', '.addButton', function () {

        //var existRow = $('.generatedRow').length;

        //counter = existRow + 1;
        counter += 1;
        var newDiv = '<div class="form-group generatedRow"><div class="control-label col-md-2 RoomSerial"><b>Sl ' + counter + '.</b></div><div class="col-md-2"><input type="text" name="RoomNo" class="form-control roomNoField" placeholder="Room no." /></div><div class="col-md-2"><input type="text" name="ClassCapacity"  id="ClassCapacity" class="form-control classCapacityField" placeholder="Class Cap." /></div><div class="col-md-2"><input type="text" name="ExamCapacity"  id="ExamCapacity" class="form-control examCapacityField" placeholder="Exam Cap." /></div><div class="col-md-2"><button type="button" class="btn btn-warning removeButton" name="append">-</button></div></div>';
        var oldDiv = $('#templateBottom2');
        oldDiv.before(newDiv);
    });

    // Remove button click handler
    $(document).on('click', '.removeButton', function () {
        var $row = $(this).parents('.form-group'),
            $option = $row.find('[name="option[]"]');
        // Remove element containing the option
        $row.remove();

        counter = 0;
        $(".RoomSerial").each(function () {
            counter += 1;
            $(this).html("<b>Sl " + counter + ".</b>");
            //optionTexts.push($(this).text())
        });
    });

    $(document).on('click', '#createCampus', function (e) {


        $('.field-validation-error').remove();

        var branchId = $('#BranchId').val();
        var name = $('#Name').val();
        var location = $('#Location').val();
        var contactNumber = $('#ContactNumber').val();

        var isSuccess = true;

        if (branchId == null || branchId <= 0) {
            showErrorMessageBelowCtrl("BranchId", "", false);
            showErrorMessageBelowCtrl("BranchId", "Select Branch.", true);
            isSuccess = false;
        }
        if (name == null || name.length <= 0) {
            showErrorMessageBelowCtrl("Name", "", false);
            showErrorMessageBelowCtrl("Name", "Campus name is required.", true);
            isSuccess = false;
        }
        if (location == null || location.length <= 0) {
            showErrorMessageBelowCtrl("Location", "", false);
            showErrorMessageBelowCtrl("Location", "Location is required.", true);
            isSuccess = false;
        }
        if (contactNumber == null || contactNumber.length <= 0) {
            showErrorMessageBelowCtrl("ContactNumber", "", false);
            showErrorMessageBelowCtrl("ContactNumber", "Contact number is required.", true);
            isSuccess = false;
        }
        //else if (!IsValidMobile(contactNumber)) {
        //    showErrorMessageBelowCtrl("ContactNumber", "", false);
        //    showErrorMessageBelowCtrl("ContactNumber", "Please enter valid Batch Capacity.", true);
        //    isSuccess = false;
        //}

        if (CheckEmpty("roomNoField", "Please enter room number") == false) {
            isSuccess = false;
        }
        else if (CheckValues("classCapacityField", "Please enter class capacity") == false) {
            isSuccess = false;
        }
        else if (CheckValues("examCapacityField", "Please enter exam capacity") == false) {
            isSuccess = false;
        }

        if (isSuccess == true) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }

    });

    $(document).on('click', '#updateCampus', function (e) {


        $('.field-validation-error').remove();

        var branchId = $('#BranchId').val();
        var name = $('#Name').val();
        var location = $('#Location').val();
        var contactNumber = $('#ContactNumber').val();
        var isSuccess = true;

        if (branchId == null || branchId <= 0) {
            showErrorMessageBelowCtrl("BranchId", "", false);
            showErrorMessageBelowCtrl("BranchId", "Select Branch.", true);
            isSuccess = false;
        }
        if (name == null || name.length <= 0) {
            showErrorMessageBelowCtrl("Name", "", false);
            showErrorMessageBelowCtrl("Name", "Campus name is required.", true);
            isSuccess = false;
        }
        if (location == null || location.length <= 0) {
            showErrorMessageBelowCtrl("Location", "", false);
            showErrorMessageBelowCtrl("Location", "Location is required.", true);
            isSuccess = false;
        }
        if (contactNumber == null || contactNumber.length <= 0) {
            showErrorMessageBelowCtrl("ContactNumber", "", false);
            showErrorMessageBelowCtrl("ContactNumber", "Contact number is required.", true);
            isSuccess = false;
        } else if (mobileCheck(contactNumber) == null) {
            showErrorMessageBelowCtrl("ContactNumber", "", false);
            showErrorMessageBelowCtrl("ContactNumber", "Please enter valid mobile number.", true);
            isSuccess = false;
        }
        //else if (!IsValidMobile(contactNumber)) {
        //    var customValidationMssageForEmptyContactNumberValidity = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Enter a valid mobile number (8801XXXXXXXXX)</span></span>';
        //    $('#ContactNumber').parent().append(customValidationMssageForEmptyContactNumberValidity);
        //    isSuccess = false;
        //}

        if (CheckEmpty("roomNoField", "Please enter room number") == false) {
            isSuccess = false;
        }
        else if (CheckValues("classCapacityField", "Please enter class capacity") == false) {
            isSuccess = false;
        }
        else if (CheckValues("examCapacityField", "Please enter exam capacity") == false) {
            isSuccess = false;
        }


        if (isSuccess == true) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });
});

