﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Mapping.UserAuth
{
    

    public partial class AreaControllersMap : BaseClassMap<AreaControllers, long>
    {
        public AreaControllersMap()
        {
            Table("AreaControllers");
            LazyLoad();
            Map(x => x.Name);
            Map(x => x.Area);
            Map(x => x.Rank);
            HasMany(x => x.Actions).KeyColumn("ControllersId").Cascade.AllDeleteOrphan().LazyLoad();
        }
    }
}
