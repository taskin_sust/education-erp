﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Administration.MaterialTypeTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "MaterialType")]
    public class MeterialTypeDeleteTest : MaterialTypeBaseTest
    {
        [Fact]
        public void MeterialType_Delete_Return_True()
        {
            organizationFactory.Create().Persist();
            _materialTypeFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            var typeId = _materialTypeFactory.Object.Id;
            Assert.True(_materialTypeService.Delete(typeId));
            var obj = _materialTypeService.GetMaterialType(typeId);
            Assert.Null(obj);
        }

        [Fact]
        public void MeterialType_Delete_Return_Depandency_Error()
        {
            organizationFactory.Create().Persist();
            _materialTypeFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _materialTypeFactory.Object.Materials = new List<Material>()
            {
                new Material()
            };

            var typeId = _materialTypeFactory.Object.Id;
            Assert.Throws<DependencyException>(() => _materialTypeService.Delete(typeId));
        }

    }
}
