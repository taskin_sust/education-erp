﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions.Inspections;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class CourseProgressMap : BaseClassMap<CourseProgress, long>
    {
        public CourseProgressMap()
        {
            Table("CourseProgress");
            LazyLoad();
            Map(x => x.HeldDate);
            References(x => x.Lecture).Column("LectureId");
            References(x => x.Batch).Column("BatchId");
            References(x => x.CourseSubject).Column("CourseSubjectId");
            References(x => x.Course).Column("CourseId");
            
        }
    }
}
