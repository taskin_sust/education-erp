﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Exam;

namespace UdvashERP.Dao.Mapping.Exam
{
    public class ExamParentMap : BaseClassMap<ExamParent, long>
    {
        public ExamParentMap()
        {
            Table("ExamParent");
            LazyLoad();
            Map(x => x.Name).Column("Name");
            Map(x => x.ExamNamePrefix).Column("ExamNamePrefix");
            Map(x => x.ExamShortNamePrefix).Column("ExamShortNamePrefix");
            Map(x => x.IsTakeAnswerSheetWhenUploadMarks).Column("IsTakeAnswerSheetWhenUploadMarks");
            Map(x => x.NumberOfExam).Column("NumberOfExam");
            References(x => x.Course).Column("CourseId");
            References(x => x.Branch).Column("BranchId");
            References(x => x.Batch).Column("BatchId");
            HasMany(x => x.Examses).KeyColumn("ExamParentId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
