﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Administration.ProgramSettingTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "ProgramSetting")]
    public class ProgramSettingSaveTest : ProgramSettingBaseTest
    {
        #region Basic Test
        [Fact]
        public void ProgramSetting_Save_Should_Return_True()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);
            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, nextProgramSession);
            bool isSuccess = _programSettingService.Save(_programSettingFactory.Object);
            Assert.True(isSuccess);
        }
        #endregion
        #region Business Logic Test
        [Fact]
        public void ProgramSetting_Save_EmptyFieldException_For_Null_Program()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);
            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(new Program(), nextProgram, programSession, nextProgramSession);
            Assert.Throws<EmptyFieldException>(() => _programSettingService.Save(_programSettingFactory.Object));
        }
        [Fact]
        public void ProgramSetting_Save_EmptyFieldException_For_Null_NextProgram()
        {
            _programFactory.CreateMore(1).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(program, new Program(), programSession, nextProgramSession);
            Assert.Throws<EmptyFieldException>(() => _programSettingService.Save(_programSettingFactory.Object));
        }
        [Fact]
        public void ProgramSetting_Save_EmptyFieldException_For_Null_Session()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);
            _sessionFactory.CreateMore(1).Persist();
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);

            _programSettingFactory.Create().WithProperties(program, nextProgram, new Session(), nextProgramSession);
            Assert.Throws<EmptyFieldException>(() => _programSettingService.Save(_programSettingFactory.Object));
        }
        [Fact]
        public void ProgramSetting_Save_EmptyFieldException_For_Null_NextSession()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);
            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, new Session());
            Assert.Throws<EmptyFieldException>(() => _programSettingService.Save(_programSettingFactory.Object));
        }
        [Fact]
        public void ProgramSetting_Save_EmptyFieldException_For_Same_Program()
        {
            _programFactory.CreateMore(1).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(program, program, programSession, nextProgramSession);
            Assert.Throws<DuplicateEntryException>(() => _programSettingService.Save(_programSettingFactory.Object));
        }
        [Fact]
        public void ProgramSetting_Save_EmptyFieldException_For_Reverse_ProgramAndSession()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);

            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, nextProgramSession).Persist();
            var programSettingObj = _programSettingService.GetProgramSetting(Convert.ToInt32(_programSettingFactory.Object.Id));
            programSettingObj.Program = nextProgram;
            programSettingObj.NextProgram = program;
            programSettingObj.Session = nextProgramSession;
            programSettingObj.NextSession = programSession;

            Assert.Throws<DuplicateEntryException>(() => _programSettingService.Save(programSettingObj));
        }
        #endregion
    }
}
