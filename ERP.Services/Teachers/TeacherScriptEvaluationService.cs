﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherScriptEvaluationService : IBaseService
    {
        #region Operational Function
        bool Save(TeacherScriptEvaluation teacherScriptEvaluation);
        TeacherScriptEvaluation LoadById(long id);
        #endregion

        #region Other function
        int? GetSerialNumber();

        #endregion

        bool Update(TeacherScriptEvaluationDetails scriptEvaluationDetails);
        bool Delete(int id);
    }

    public class TeacherScriptEvaluationService:BaseService,ITeacherScriptEvaluationService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeacherScriptService");
        private readonly ITeacherScriptEvaluationDao _teacherScriptEvaluationDao;
        private readonly ITeacherScriptEvaluationDetailsDao _teacherScriptEvaluationDetailsDao;
        #endregion

        #region Propertise & Object Initialization
            
            public TeacherScriptEvaluationService(ISession session)
            {
                Session = session;
                _teacherScriptEvaluationDao = new TeacherScriptEvaluationDao(){Session = session};
                _teacherScriptEvaluationDetailsDao = new TeacherScriptEvaluationDetailsDao(){Session = session};
               
            }
        #endregion

        #region Operational Function
            public bool Save(TeacherScriptEvaluation teacherScriptEvaluation)
            {
                ITransaction transaction = null;
                try
                {
                    using (transaction = Session.BeginTransaction())
                    {
                        _teacherScriptEvaluationDao.Save(teacherScriptEvaluation);
                        transaction.Commit();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    if (transaction != null && transaction.IsActive)
                        transaction.Rollback();
                    _logger.Error(ex);
                    throw;
                }
            }

        public TeacherScriptEvaluation LoadById(long id)
        {
            try
            {
                return _teacherScriptEvaluationDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region other function
        public int? GetSerialNumber()
        {
            try
            {
                int? serial = 200001;
                var scriptEvaluation = _teacherScriptEvaluationDao.GetTeacherScriptEvaluation();
                if (scriptEvaluation != null)
                {
                    serial = scriptEvaluation.SerialNumber + 1;
                }
                return serial;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool Update(TeacherScriptEvaluationDetails scriptEvaluationDetails)
        {
            ITransaction transaction = null;
            try
            {

                using (transaction = Session.BeginTransaction())
                {
                    _teacherScriptEvaluationDetailsDao.Update(scriptEvaluationDetails);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool Delete(int id)
        {
            ITransaction transaction = null;
            try
            {
                var tempObj = _teacherScriptEvaluationDetailsDao.LoadById(id);
                if (tempObj.PaymentStatus == 1)
                    throw new DependencyException("You can't delete this Script Evaluation, payment is already paid.");

                using (transaction = Session.BeginTransaction())
                {
                    tempObj.Status = TeacherClassEntryDetails.EntityStatus.Delete;
                    _teacherScriptEvaluationDetailsDao.Update(tempObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

    }
}
