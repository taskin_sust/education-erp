﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceSynchronizerTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceSynchronizer")]
    public class AttendanceSynchronizerSaveTest : AttendanceSynchronizerBaseTest
    {
        #region Basic test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _attendanceSynchronizerService.Save(null));
        }

        [Fact]
        public void Save_Should_Return_Model_inValid_SyncKey() 
        {
            _campusRoomFactory.CreateMore(1, null);
            _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList());
            _attendanceSynchronizerFactory.Object.SynchronizerKey = "";
            var exception = Assert.Throws<MessageException>(() => _attendanceSynchronizerService.Save(_attendanceSynchronizerFactory.Object));
            Assert.Throws<MessageException>(() => _attendanceSynchronizerService.Save(_attendanceSynchronizerFactory.Object));
            Assert.Contains("required", exception.Message);
        }

        [Fact]
        public void Save_Should_Return_Model_Invalid_Campus()
        {
            _attendanceSynchronizerFactory.Create();
            var exception = Assert.Throws<MessageException>(() => _attendanceSynchronizerService.Save(_attendanceSynchronizerFactory.Object));
            //Assert.Throws<MessageException>(() => _attendanceSynchronizerService.Save(_attendanceSynchronizerFactory.Object));
            Assert.Contains("required", exception.Message);
        }

        [Fact]
        public void Should_Save_Successfully() 
        {
            _campusRoomFactory.CreateMore(1, null);
            _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList());
            _attendanceSynchronizerFactory.Persist();

            Assert.NotNull(_attendanceSynchronizerFactory.Object.Id);
            Assert.NotEqual(_attendanceSynchronizerFactory.Object.Id,0);            
        }

        [Fact]
        public void Save_Should_Check_Duplicate() 
        {
            _campusRoomFactory.CreateMore(1, null);
            _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList()).WithSynchronizerKey(_synchronizerKey).Persist();
            _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList()).WithSynchronizerKey(_synchronizerKey);
            Assert.Throws<DuplicateEntryException>(() => _attendanceSynchronizerFactory.Persist());
        }                
        #endregion

        #region Business Logic Test

        #endregion
    }
}
