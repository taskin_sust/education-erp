﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.MentorHisotyTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "MentorHistory")]
    public class MentorHistorySaveTest : MentorHistoryBaseTest
    {
        #region Basic Test

        [Fact]
        public void Mentor_History_Save_Sucessfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                .WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember);
            _mentorHistoryService.Save(teamMember.MentorHistory[0].Mentor.Pin, _mentorHistoryFactory.Object);
            Assert.True(true);
        }

        [Fact]
        public void Mentor_History_AddMentorHistory_Should_Save_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                .WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember);
            _mentorHistoryService.AddMentorHistory(_mentorHistoryFactory.Object.TeamMember, _mentorHistoryFactory.Object.Mentor.Pin, Convert.ToDateTime(_mentorHistoryFactory.Object.EffectiveDate).AddDays(2).ToString());
            Assert.True(true);
        } 

        #endregion

        #region Business Logic Test

        [Fact]
        public void InvalidDataException_Mentor_History_Save_For_Null_Organization()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                //.WithOrganization(organization)
                .WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember);
            Assert.Throws<NullReferenceException>(() => _mentorHistoryService.Save(teamMember.MentorHistory[0].Mentor.Pin, _mentorHistoryFactory.Object));
        }

        [Fact]
        public void InvalidDataException_Mentor_History_Save_For_Null_Department()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                //.WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember);
            Assert.Throws<NullReferenceException>(() => _mentorHistoryService.Save(teamMember.MentorHistory[0].Mentor.Pin, _mentorHistoryFactory.Object));
        }

        [Fact]
        public void InvalidDataException_Mentor_History_Save_For_Null_Desigination()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                .WithDepartment(department)
                //.WithDesignation(desigination)
                .WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember);
            Assert.Throws<NullReferenceException>(() => _mentorHistoryService.Save(teamMember.MentorHistory[0].Mentor.Pin, _mentorHistoryFactory.Object));
        }


        [Fact]
        public void InvalidDataException_Mentor_History_AddMentorHistory_For_Null_TeamMember()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                .WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember);
            Assert.Throws<InvalidDataException>(() => _mentorHistoryService.AddMentorHistory(null, _mentorHistoryFactory.Object.Mentor.Pin, Convert.ToDateTime(_mentorHistoryFactory.Object.EffectiveDate).AddDays(2).ToString()));
        }

        [Fact]
        public void InvalidDataException_Mentor_History_AddMentorHistory_For_Null_Mentor()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var desigination = teamMember.EmploymentHistory[0].Designation;
            var organization = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            _mentorHistoryFactory.Create()
                .WithOrganization(organization)
                .WithDepartment(department)
                .WithDesignation(desigination).WithMentor(teamMember.MentorHistory[0].Mentor).WithTeamMember(teamMember);
            Assert.Throws<InvalidDataException>(() => _mentorHistoryService.AddMentorHistory(null, 0, Convert.ToDateTime(_mentorHistoryFactory.Object.EffectiveDate).AddDays(2).ToString()));
        }


        #endregion

    }
}
