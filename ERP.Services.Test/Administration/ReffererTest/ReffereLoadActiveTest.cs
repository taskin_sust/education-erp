﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ReffererTest
{
    interface IReffererLoadActiveTest
    {
       
    }
    [Trait("Area", "Administration")]
    [Trait("Service", "Refferer")]
    public class ReffererLoadActiveTest : ReffererBaseTest, IReffererLoadActiveTest
    {
        #region Basic Test
        
        [Fact]
        public void LoadActive_should_return_list()
        {

            _referrerFactory.CreateMore(100).Persist();
            var referrerList = _referenceService.GetReferrerList(0,100,"","","","","1","");
            Assert.Equal(100, referrerList.Count);
        }

        #endregion
    }
}
