﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceSynchronizerTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceSynchronizer")]
    public class AttendanceSynchronizerLoadTest : AttendanceSynchronizerBaseTest
    {
        #region Basic Test

        [Fact]
        public void Shud_Return_Zero()
        {
            var organizationIds = new List<long>() { -1 };
            var branchIds = new List<long>() { -1 };
            var campusIds = new List<long>() { -1 };
            var obj = _attendanceSynchronizerService.LoadSynchronizer(organizationIds, branchIds, campusIds);
            Assert.Equal(obj.Count, 0);
        }

        [Fact]
        public void Shud_Return_One()
        {
            _campusRoomFactory.CreateMore(1, null);
            _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            var organizationIds = new List<long>() { _attendanceSynchronizerFactory.Object.Campus.Branch.Organization.Id };
            var branchIds = new List<long>() { _attendanceSynchronizerFactory.Object.Campus.Branch.Id };
            var campusIds = new List<long>() { _attendanceSynchronizerFactory.Object.Campus.Id };
            var obj = _attendanceSynchronizerService.LoadSynchronizer(organizationIds, branchIds, campusIds);
            Assert.Equal(obj.Count, 1);
        }

        public void Shud_Parameter_Check()
        {
            _organizationFactory.CreateMore(2).Persist();
            _branchFactory.CreateMore(2, _organizationFactory.GetLastCreatedObjectList()).Persist();
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.CreateMore(2, _branchFactory.GetLastCreatedObjectList(), _campusRoomFactory.GetLastCreatedObjectList()).Persist();
            _attendanceSynchronizerFactory.CreateMore(2, _campusFactory.GetLastCreatedObjectList()).Persist();

            var organizationIds = _organizationFactory.GetLastCreatedObjectList().Select(x => x.Id).ToList();
            var branchIds = _branchFactory.GetLastCreatedObjectList().Select(x => x.Id).ToList();
            var campusIds = _campusFactory.GetLastCreatedObjectList().Select(x => x.Id).ToList();
            var obj = _attendanceSynchronizerService.LoadSynchronizer(organizationIds, branchIds, campusIds);
            Assert.Equal(obj.Count, 16);
            obj = _attendanceSynchronizerService.LoadSynchronizer(organizationIds, branchIds);
            Assert.Equal(obj.Count, 16);
            obj = _attendanceSynchronizerService.LoadSynchronizer(organizationIds);
            Assert.Equal(obj.Count, 16);
            obj = _attendanceSynchronizerService.LoadSynchronizer(organizationIds, branchIds, campusIds.Take(1).ToList());
            Assert.Equal(obj.Count, 2);
            obj = _attendanceSynchronizerService.LoadSynchronizer(organizationIds, branchIds.Take(1).ToList());
            Assert.Equal(obj.Count, 4);
            obj = _attendanceSynchronizerService.LoadSynchronizer(organizationIds.Take(1).ToList());
            Assert.Equal(obj.Count, 8);
        }

        #endregion

        #region AttendanceSynchronizer LoadSynchronizerForManage Test

        #region Basic test

        [Fact]
        public void Shud_Return_Zero_LoadSynchronizerForManage()
        {
            int start = 0;
            int length = 10;
            string orderBy = "SynchronizerKey";
            string orderDir = "Desc";
            string organizationId = "-1";
            string branchId = "-1";
            string campusId = "-1";
            var obj = _attendanceSynchronizerService.LoadSynchronizer(start, length, orderBy, orderDir, organizationId, branchId, campusId);
            Assert.Equal(obj.Count, 0);
        }

        [Fact]
        public void Shud_Return_One_LoadSynchronizerForManage()
        {
            _campusRoomFactory.CreateMore(1, null);
            _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.ObjectList).Persist();

            int start = 0;
            int length = 10;
            string orderBy = "SynchronizerKey";
            string orderDir = "Desc";
            string organizationId = _attendanceSynchronizerFactory.Object.Campus.Branch.Organization.Id.ToString();
            string branchId = _attendanceSynchronizerFactory.Object.Campus.Branch.Id.ToString();
            string campusId = _attendanceSynchronizerFactory.Object.Campus.Id.ToString();
            var obj = _attendanceSynchronizerService.LoadSynchronizer(start, length, orderBy, orderDir, organizationId, branchId, campusId);
            Assert.Equal(obj.Count, 1);
        }

        [Fact]
        public void Shud_Return_Top_Ten()
        {
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithBranch().WithCampusRoomList(_campusRoomFactory.ObjectList).Persist();
            _attendanceSynchronizerFactory.CreateMore(20, _campusFactory.Object).Persist();

            int start = 0;
            int length = 10;
            string orderBy = "SynchronizerKey";
            string orderDir = "Desc";
            string organizationId = _campusFactory.Object.Branch.Organization.Id.ToString();
            string branchId = _campusFactory.Object.Branch.Id.ToString();
            string campusId = _campusFactory.Object.Id.ToString();
            var obj = _attendanceSynchronizerService.LoadSynchronizer(start, length, orderBy, orderDir, organizationId, branchId, campusId);
            Assert.Equal(obj.Count, 10);
        }

        #endregion

        #endregion

        #region AttendanceSynchronizer GetAttendanceSynchronizer Test

        #region Basic test

        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _attendanceSynchronizerService.GetAttendanceSynchronizer(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            _campusRoomFactory.CreateMore(1, null);
            _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            var obj = _attendanceSynchronizerService.GetAttendanceSynchronizer(_attendanceSynchronizerFactory.Object.Id);
            Assert.NotNull(obj);
        }

        #endregion

        #endregion

        #region AttendanceSynchronizerGetAttendanceSynchronizerBySynchronizerKeyTest

        #region Basic test

        [Fact]
        public void GetBySynchronizerKey_Null_Check()
        {
            var obj = _attendanceSynchronizerService.GetAttendanceSynchronizer("#####");
            Assert.Null(obj);
        }

        [Fact]
        public void GetBySynchronizerKey_NotNull_Check()
        {
            _campusRoomFactory.CreateMore(1, null);
            _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            var obj = _attendanceSynchronizerService.GetAttendanceSynchronizer(_attendanceSynchronizerFactory.Object.SynchronizerKey);
            Assert.NotNull(obj);
        }

        #endregion

        #endregion

        #region AttendanceSynchronizer SynchronizerRowCount Test

        #region Basic test
        
        [Fact]
        public void SynchronizerRowCount_Should_Return_Zero()
        {
            var count = _attendanceSynchronizerService.SynchronizerRowCount("0", "0", "0");
            Assert.Equal(0, count);
        }

        [Fact]
        public void SynchronizerRowCount_Should_Return_One()
        {
            _campusRoomFactory.CreateMore(1, null);
            _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
            var count = _attendanceSynchronizerService.SynchronizerRowCount(
                _attendanceSynchronizerFactory.Object.Campus.Branch.Organization.Id.ToString(),
                _attendanceSynchronizerFactory.Object.Campus.Branch.Id.ToString(),
                _attendanceSynchronizerFactory.Object.Campus.Id.ToString()
            );
            Assert.Equal(1, count);
        }

        [Fact]
        public void SynchronizerRowCount_Should_Return_Valid()
        {
            int number = 7;
            _campusRoomFactory.CreateMore(1, null);
            _campusFactory.Create().WithCampusRoomList(_campusRoomFactory.GetLastCreatedObjectList()).WithBranch().Persist();
            _attendanceSynchronizerFactory.CreateMore(7, _campusFactory.Object).Persist();
            var count = _attendanceSynchronizerService.SynchronizerRowCount(
                _campusFactory.Object.Branch.Organization.Id.ToString(),
                _campusFactory.Object.Branch.Id.ToString(),
                _campusFactory.Object.Id.ToString()
            );
            Assert.Equal(number, count);
        }

        #endregion

        #endregion
    }
}
