﻿using System.Linq;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ChildrenAllowanceBlockTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ChildrenAllowanceBlockService")]
    public class AllowanceBlockLoadTest : ChildrenAllowanceBlockBaseTest
    {
        #region List Load Test

        [Fact]
        public void Should_Return_Invalid_Permission_Exception()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;
            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object);

            Assert.Throws<UnauthorizeDataException>(() => _allowanceBlockService.LoadAll(0, 0, int.MaxValue, null));
        }

        [Fact]
        public void Should_Return_AllData_Successfully()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;
            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object);

            var list = _allowanceBlockService.LoadAll(0, 0, int.MaxValue, menu);
            //Assert.Equal(_allowanceBlockFactory.Object.Id, list.Select(x => x.Id).SingleOrDefault());
            Assert.True(list.Count > 0);
        }

        #endregion

        #region Single Object Load Test

        [Fact]
        public void Should_Return_Invalid_Data_Exception()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;
            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object);

            Assert.Throws<InvalidDataException>(() => _allowanceBlockService.GetById(0));
        }

        [Fact]
        public void Should_Return_Obj_Successfully()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;
            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object);

            Assert.Equal(_allowanceBlockFactory.Object.Id, _allowanceBlockService.GetById(_allowanceBlockFactory.Object.Id).Id);
        }

        #endregion

        #region Count Test

        [Fact]
        public void Should_Return_Invalid_Permission_Exception_ForCount()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;

            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object);

            Assert.Throws<UnauthorizeDataException>(() => _allowanceBlockService.CountBlockAllowance(null));
        }

        [Fact]
        public void Should_Return_One_Data_Successfully()
        {
            var teamMember = PersistTeamMember();
            _childrenAllowanceSettingFactory.Create().WithOrganization(organizationFactory.Object);
            var menu = BuildUserMenu(_commonHelper.ConvertIdToList(organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _allowanceBlockFactory.Create();
            _allowanceBlockFactory.Object.TeamMember = teamMember;
            _allowanceBlockFactory.Object.ChildrenAllowanceSetting = _childrenAllowanceSettingFactory.Object;

            _allowanceBlockService.SaveOrUpdate(menu, _allowanceBlockFactory.Object);
            var count = _allowanceBlockService.CountBlockAllowance(menu, teamMember.Pin.ToString(), "", organizationFactory.Object.Id
                                                                        , _allowanceBlockFactory.Object.ChildrenAllowanceSetting.Id);
            Assert.True(count == 1);
        }

        #endregion

        
    }
}
