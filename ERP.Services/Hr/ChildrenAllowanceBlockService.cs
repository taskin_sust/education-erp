using System;
using System.Collections.Generic;
using System.Security.Authentication;
using FluentNHibernate.Utils;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.Hr
{
    public interface IChildrenAllowanceBlockService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdate(List<UserMenu> authorizeMenu, ChildrenAllowanceBlock prAllowanceBlock, bool isEdit = false);
        void Delete(List<UserMenu> authorizeMenu, ChildrenAllowanceBlock allowanceBlock);
        #endregion

        #region Single Instances Loading Function

        ChildrenAllowanceBlock GetById(long id);
        AllowanceSheet GetTeamMemberLastIsSubmittedAllowanceSheet(ChildrenAllowanceBlock entity);

        #endregion

        #region List Loading Function

        IList<ChildrenAllowanceBlockViewModel> LoadAll(int draw, int start, int length, List<UserMenu> authorizeMenu, string pinNo = ""
                                                        , string blockName = "", long organizationId = 0, long? allowanceType = null);

        IList<ChildrenAllowanceBlock> LoadAll(long childrenAllowanceSettingId);

        #endregion

        #region Others Function

        int CountBlockAllowance(List<UserMenu> authorizeMenu, string pinNo = "", string blockName = "", long organizationId = 0, long? allowanceType = null);

        #endregion

        #region Helper Function

        #endregion


    }
    public class ChildrenAllowanceBlockService : BaseService, IChildrenAllowanceBlockService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("PayrollService");
        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IChildrenAllowanceBlockDao _allowanceBlockDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;
        public ChildrenAllowanceBlockService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _allowanceBlockDao = new ChildrenAllowanceBlockDao { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao { Session = session };
        }

        #endregion

        #region Operational Functions

        public void SaveOrUpdate(List<UserMenu> authorizeMenu, ChildrenAllowanceBlock prAllowanceBlock, bool isEdit = false)
        {
            try
            {
                if (prAllowanceBlock == null) throw new InvalidDataException("Invalid object ");
                CheckValidation(prAllowanceBlock);
                CheckBeforeSave(authorizeMenu, prAllowanceBlock, isEdit);
                if (!isEdit)
                    _allowanceBlockDao.Save(prAllowanceBlock);
                else
                {
                    using (var trans = Session.BeginTransaction())
                    {
                        _allowanceBlockDao.Update(prAllowanceBlock);
                        trans.Commit();
                    }
                }
            }
            catch (AuthenticationException ae)
            {
                _logger.Error(ae);
                throw ae;
            }
            catch (DuplicateEntryException dex)
            {
                _logger.Error(dex);
                throw dex;
            }
            catch (InvalidDataException ide)
            {
                _logger.Error(ide);
                throw ide;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public void Delete(List<UserMenu> authorizeMenu, ChildrenAllowanceBlock allowanceBlock)
        {
            try
            {
                if (allowanceBlock == null) throw new InvalidDataException("Invalid Object");
                if (allowanceBlock.Id <= 0) throw new InvalidDataException("invalid Object");
                var allowanceSheet = GetTeamMemberLastIsSubmittedAllowanceSheet(allowanceBlock);
                if (allowanceSheet != null && allowanceSheet.DateTo >= allowanceBlock.EffectiveDate)
                {
                    throw new InvalidDataException("Salary Sheet found for this Children Allowance Setting!");
                }
                allowanceBlock.Status = ChildrenAllowanceBlock.EntityStatus.Delete;
                using (var trans = Session.BeginTransaction())
                {
                    _allowanceBlockDao.Update(allowanceBlock);
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public ChildrenAllowanceBlock GetById(long id)
        {
            try
            {
                if (id <= 0) throw new InvalidDataException("invalid id");
                return _allowanceBlockDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function


        public IList<ChildrenAllowanceBlockViewModel> LoadAll(int draw, int start, int length, List<UserMenu> authorizeMenu, string pinNo = "", string blockName = "",
            long organizationId = 0, long? allowanceType = null)
        {
            try
            {
                if (authorizeMenu == null || authorizeMenu.Count <= 0) throw new UnauthorizeDataException("invalid user permission");
                var organizationList = new List<long>();
                if (organizationId > 0)
                {
                    var authOrgIds = AuthHelper.LoadOrganizationIdList(authorizeMenu, _commonHelper.ConvertIdToList(organizationId));
                    if (authOrgIds.Count <= 0)
                        throw new InvalidDataException("Permission Denied for selected Organization!");
                    organizationList.Add(authOrgIds[0]);
                }
                else
                {
                    organizationList = AuthHelper.LoadOrganizationIdList(authorizeMenu);
                    if (organizationList.Count <= 0)
                        throw new InvalidDataException("Permission Denied for Organization!");
                }
                return _allowanceBlockDao.LoadAll(draw, start, length, organizationList, pinNo, blockName, allowanceType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ChildrenAllowanceBlock> LoadAll(long childrenAllowanceSettingId)
        {
            var list =
                Session.QueryOver<ChildrenAllowanceBlock>()
                    .Where(x => x.ChildrenAllowanceSetting.Id == childrenAllowanceSettingId)
                    .WhereNot(x => x.Status == ChildrenAllowanceBlock.EntityStatus.Delete)
                    .List<ChildrenAllowanceBlock>();
            return list;
        }

        #endregion

        #region Others Function

        public int CountBlockAllowance(List<UserMenu> authorizeMenu, string pinNo = "", string blockName = "", long organizationId = 0, long? allowanceType = null)
        {
            try
            {
                if (authorizeMenu == null || authorizeMenu.Count <= 0) throw new UnauthorizeDataException("invalid user permission");
                var organizationList = new List<long>();
                if (organizationId > 0)
                {
                    var authOrgIds = AuthHelper.LoadOrganizationIdList(authorizeMenu, _commonHelper.ConvertIdToList(organizationId));
                    if (authOrgIds.Count <= 0)
                        throw new InvalidDataException("Permission Denied for selected Organization!");
                    organizationList.Add(authOrgIds[0]);
                }
                else
                {
                    organizationList = AuthHelper.LoadOrganizationIdList(authorizeMenu);
                    if (organizationList.Count <= 0)
                        throw new InvalidDataException("Permission Denied for Organization!");
                }
                return _allowanceBlockDao.CountBlocks(organizationList, pinNo, blockName, allowanceType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public AllowanceSheet GetTeamMemberLastIsSubmittedAllowanceSheet(ChildrenAllowanceBlock entity)
        {
            try
            {

                var lastSalarySheet = _allowanceSheetDao.GetTeamMemberLastIsSubmittedAllowanceSheet(entity.TeamMember.Id);
                return lastSalarySheet;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        private void CheckBeforeSave(List<UserMenu> authorizeMenu, ChildrenAllowanceBlock prAllowanceBlock, bool isEdit = false)
        {
            if (authorizeMenu == null) throw new AuthenticationException("unauthorizes organization");
            if (prAllowanceBlock.ClosingDate.ToString().Length > 0)
            {
                if (prAllowanceBlock.EffectiveDate > prAllowanceBlock.ClosingDate)
                {
                    throw new InvalidDataException("Closing date must be bigger than effective date");
                }
            }
            CheckForDateValidation(prAllowanceBlock, isEdit);
        }

        private void CheckForDateValidation(ChildrenAllowanceBlock model, bool isEdit = false)
        {
            var lastSalarySheet = _allowanceSheetDao.GetTeamMemberLastIsSubmittedAllowanceSheet(model.TeamMember.Id);

            if (lastSalarySheet != null)
            {
                if (!isEdit && lastSalarySheet.DateTo >= model.EffectiveDate)
                {
                    throw new DuplicateEntryException("Salary Sheet renamed by Allowance sheet!");
                }
                else if (isEdit && (model.ClosingDate == null || lastSalarySheet.DateTo > model.ClosingDate))
                {
                    throw new DuplicateEntryException("Salary Sheet renamed by Allowance sheet!");
                }
            }



            var preDateList = _allowanceBlockDao.LoadByPinAndAllowanceTypeAndEffectiveDate(model.TeamMember.Id, model.ChildrenAllowanceSetting.Id,
                                                                                        model.EffectiveDate, model.Id);
            if (preDateList.Count > 0)
            {
                var row = preDateList[0];
                if (row.EffectiveDate == model.EffectiveDate)
                {
                    throw new DuplicateEntryException("duplicate entry for effective date," +
                                                      model.ChildrenAllowanceSetting.Name
                                                      + " already block for member " + model.TeamMember.Pin +
                                                      "!");
                }
                if (row.EffectiveDate < model.EffectiveDate &&
                    (row.ClosingDate == null || row.ClosingDate >= model.EffectiveDate))
                {
                    throw new DuplicateEntryException("duplicate entry for effective date, " +
                                                      model.ChildrenAllowanceSetting.Name
                                                      + " already block for member " + model.TeamMember.Pin +
                                                      "!");
                }
            }
            else
            {
                var postDateList = _allowanceBlockDao.LoadByPinAndAllowanceTypeAndEffectiveDate(model.TeamMember.Id, model.ChildrenAllowanceSetting.Id,
                                                                                        model.EffectiveDate, model.Id, false);
                if (postDateList.Count > 0)
                {
                    var row = postDateList[0];
                    if (row.EffectiveDate > model.EffectiveDate
                        && (model.ClosingDate == null || row.EffectiveDate <= model.ClosingDate))
                    {
                        throw new DuplicateEntryException("duplicate entry for closing date, " +
                                                          model.ChildrenAllowanceSetting.Name
                                                          + " already block for member " + model.TeamMember.Pin +
                                                          "!");
                    }
                }
            }

        }

        private static void CheckValidation(ChildrenAllowanceBlock prAllowanceBlock)
        {
            var validationResult = ValidationHelper.ValidateEntity<ChildrenAllowanceBlock, ChildrenAllowanceBlock>(prAllowanceBlock);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        #endregion


    }
}

