﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;
using System.Collections;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class DepartmentFactory : ObjectFactoryBase<Department>
    {
        #region Object Initialization

        protected readonly IDepartmentService _departmentService;
        protected readonly IEmploymentHistoryService _employmentHistoryService;
        private OrganizationFactory _organizationFactory;

        public List<EmploymentHistory> EmploymentHistoryList { get; set; }

        public DepartmentFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _departmentService = new DepartmentService(_session);
            _employmentHistoryService = new EmploymentHistoryService(_session);
            _organizationFactory = new OrganizationFactory(this, _session);
            EmploymentHistoryList = new List<EmploymentHistory>();
        }

        #endregion

        #region Create
                
        public DepartmentFactory Create()
        {
            Object = new Department()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Department.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public DepartmentFactory CreateWith(long id,string name)
        {
            Object = new Department()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                Name = name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Department.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public DepartmentFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }

        public DepartmentFactory WithOrganization()
        {
            _organizationFactory.Create().Persist();
            Object.Organization = _organizationFactory.Object;
            return this;
        }

        public DepartmentFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        //public DepartmentFactory WithEmploymentHistory(int numberOfEmployment)
        //{
        //    for (int i = 0; i < numberOfEmployment; i++)
        //    {
        //        var eh = new EmploymentHistory()
        //        {
        //            CreateBy = 1,
        //            Department = Object,
        //            EmploymentStatus = 1,
        //            LeaveDate = DateTime.Now,
        //            ModifyBy = 1,
        //            Name = "EmploymentHistory_" + i,
        //            Rank = i,
        //            Status = 1
        //        };
        //        EmploymentHistoryList.Add(eh);
        //        _employmentHistoryService.Save(eh);
        //    }
        //    Object.EmploymentHistory = EmploymentHistoryList;
        //    return this;
        //}

        public DepartmentFactory WithOrganization(string orgName, int rank, string shortName, string contact, decimal basicSalary, decimal houseRent, decimal medicalAllowance, decimal convenyance, int monthlyWorkingDay, decimal deductionPerAbsent)
        {
            _organizationFactory.CreateWith(orgName, rank, shortName, contact, basicSalary, houseRent, medicalAllowance, convenyance, monthlyWorkingDay, deductionPerAbsent);
            Object.Organization = _organizationFactory.Object;
            return this;
        }
        
        public DepartmentFactory CreateMore(int numberOfDepartment)
        {
            CreateDepartmentList(numberOfDepartment);
            return this;
        }

        private List<Department> CreateDepartmentList(int numberOfDepartment)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Department>();
            var orgNamePrefix = Guid.NewGuid().ToString();
            _organizationFactory.Create()
                .WithNameAndRank(orgNamePrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"), 1)
                .Persist();

            for (int i = 0; i < numberOfDepartment; i++)
            {
                var dept = new Department()
                {
                    Id = 0,
                    Name = Name + "_" + i,
                    Rank = i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Department.EntityStatus.Active
                };
                dept.Organization = _organizationFactory.Object;
                list.Add(dept);
            }
            ObjectList.AddRange(list);
            return list;
        }

        public DepartmentFactory Persist()
        {
            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _departmentService.Save(item);
                    }
                }
            }

            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _departmentService.Save(item);
                    }
                }
            }

            return this;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup

        public void Cleanup()
        {
            DeleteAll();
            _organizationFactory.Cleanup();
        }

        public void OnlyDepartmentCleanUp()
        {
            DeleteAll();
        }

        #endregion
    }
}
