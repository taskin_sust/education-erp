﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ShiftWeekendHistoryTest
{
    public interface IShiftWeekendHistoryUpdateTest
    {
        #region Business logic and validation Test

        void Should_Return_Invalid_Id_Exception();
        void Should_Return_Invalid_Id();

        #endregion
    }

    [Trait("Area", "Hr")]
    [Trait("Service", "ShiftWeekendHistoryService")]
    public class ShiftWeekendHistoryUpdateTest : ShiftWeekendHistoryBaseTest, IShiftWeekendHistoryUpdateTest
    {
        #region Business logic and validation Test
       
        [Fact]
        public void Should_Return_Invalid_Id_Exception()
        {
            var teamMember = PersistTeamMember();
            _shiftWeekendHistoryService.SaveBatchShiftWeekendHistory(teamMember.ShiftWeekendHistory.ToList());
            var organizationId = teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id;
            var shiftId = teamMember.ShiftWeekendHistory[0].Shift.Id;
            var weekend = teamMember.ShiftWeekendHistory[0].Weekend.ToString();
            var effectiveDate = DateTime.Now;
            Assert.Throws<InvalidDataException>(
                () =>
                    _shiftWeekendHistoryService.UpdateShiftWeekendHistory(0, organizationId, shiftId, weekend,
                        effectiveDate.ToString()));
        }

        [Fact]
        public void Should_Return_Invalid_Id()
        {
            var teamMember = PersistTeamMember();
            _shiftWeekendHistoryService.SaveBatchShiftWeekendHistory(teamMember.ShiftWeekendHistory.ToList());
            var organizationId = teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id;
            var shiftId = teamMember.ShiftWeekendHistory[0].Shift.Id;
            var weekend = teamMember.ShiftWeekendHistory[0].Weekend.ToString();
            var effectiveDate = DateTime.Now;
            Assert.Throws<InvalidDataException>(
                () =>
                    _shiftWeekendHistoryService.UpdateShiftWeekendHistory(int.MaxValue-1, organizationId, shiftId, weekend,
                        effectiveDate.ToString()));
        }

        #endregion
    }
}
