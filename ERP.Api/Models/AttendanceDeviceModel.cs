﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UdvashERP.BusinessRules;

namespace UdvashERP.Api.Models
{
    public class AttendanceDeviceModel
    {
        public AttendanceDeviceModel()
        {
            TeamMembers = new List<TeamMemberModel>();
        }
        public virtual long Id { get; set; }
        public virtual string DeviceTypeCode { get; set; }
        public virtual string Name { get; set; }
        public virtual string DeviceModelNo { get; set; }
        public virtual DeviceCommunicationType CommunicationType { get; set; }
        public virtual string IPAddress { get; set; }
        public virtual int? Port { get; set; }
        public virtual string CommunicationKey { get; set; }
        public virtual int MachineNo { get; set; }
        public virtual List<TeamMemberModel> TeamMembers { get; set; }
        public virtual bool IsReset { get; set; }
        public virtual string LastUpdateTime { get; set; }
    }
}