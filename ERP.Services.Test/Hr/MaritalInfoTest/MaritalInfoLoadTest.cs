﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using Xunit;

namespace UdvashERP.Services.Test.Hr.MaritalInfoTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "MaritalInfo")]
    public class MaritalInfoLoadTest : MaritalInfoBaseTest
    {
        [Fact]
        public void GetTeamMemberMaritalInfo_Load_Test()
        {
            var teamMember = PersistTeamMember(MentorId);
            _maritalInformationFactory.CreateWith(DateTime.Now.AddYears(-1), (int)MaritalType.Single).WithTeamMember(teamMember).Persist();
            var entity = _maritalInfoService.GetTeamMemberMaritalInfo(teamMember.Id);
            Assert.NotNull(entity);
            Assert.Equal(entity.TeamMember.Id, teamMember.Id);
        }

        [Fact]
        public void GetCurrentMaritalStatus_Should_Return_MaritalStatus()
        {
            var teamMember = PersistTeamMember(MentorId);
            _maritalInformationFactory.CreateWith(DateTime.Now.AddYears(-1), (int)MaritalType.Married).WithTeamMember(teamMember).Persist();

            var status = _maritalInfoService.GetCurrentMaritalStatus(teamMember.Id);
            Assert.True(status > 0);
            Assert.Equal(status, (int)MaritalType.Married);
        }
    }
}
