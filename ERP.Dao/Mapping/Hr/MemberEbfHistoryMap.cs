﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Hr
{
    public class MemberEbfHistoryMap : BaseClassMap<MemberEbfHistory,long>
    {
        public MemberEbfHistoryMap()
        {
            Table("HR_MemberEbfHistory");
            LazyLoad();
            Map(x => x.EmployeeContributionAmount).Column("EmployeeContributionAmount");
            Map(x => x.EmployoyerContributionAmount).Column("EmployerContributionAmount");
            Map(x => x.CalculationOn).Column("CalculationOn");
            Map(x => x.DateFrom).Column("DateFrom");
            Map(x => x.DateTo).Column("DateTo");
            References(x => x.SalarySheet).Column("SalarySheetId");
            References(x => x.EmployeeBenefitsFundSetting).Column("EmployeeBenefitsFundSettingId");
        }
    }
}