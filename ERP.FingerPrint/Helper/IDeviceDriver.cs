﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.FingerPrint.BusinessModel;

namespace UdvashERP.FingerPrint.Helper
{
    public interface IDeviceDriver
    {
        bool Connect(Device device);
        bool SetUserFingerTemplate(string enrollNumber, int fingerIndex, int flag);
        bool GetUserFingerTemplate(int machineNo, string enrollNumber, int fingerIndex, out int flag, out byte[] tmpData, out int tmpLength);
        bool DeleteUserTemplate(int machineNo, string enrollNumber, int fingerIndex);
        void Disconnect();
    }
}
