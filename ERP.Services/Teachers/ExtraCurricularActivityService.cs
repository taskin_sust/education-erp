﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Teachers
{
    public interface IExtraCurricularActivityService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        ExtraCurricularActivity GetExtraCurricularActivity(long id);
        #endregion

        #region List Loading Function
        IList<ExtraCurricularActivity> LoadExtraCurricularActivity();
        IList<ExtraCurricularActivity> LoadExtraCurricularActivity(string[] organizationIds);
        #endregion

        #region Others Function

        #endregion
    }

    public class ExtraCurricularActivityService : BaseService, IExtraCurricularActivityService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IExtraCurricularActivityDao _extraCurricularActivityDao;
        public ExtraCurricularActivityService(ISession session)
        {
            _extraCurricularActivityDao = new ExtraCurricularActivityDao() { Session = session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public ExtraCurricularActivity GetExtraCurricularActivity(long id)
        {
            try
            {
                return _extraCurricularActivityDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function
        public IList<ExtraCurricularActivity> LoadExtraCurricularActivity()
        {
            try
            {
                return _extraCurricularActivityDao.LoadExtraCurricularActivity();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ExtraCurricularActivity> LoadExtraCurricularActivity(string[] organizationIds)
        {
            try
            {
              return _extraCurricularActivityDao.LoadExtraCurricularActivity(organizationIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function

        #endregion
    }
}
