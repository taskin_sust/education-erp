﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.OrderedGoodsReceive
{
    public interface IOrderedGoodsReceiveCountTest
    {
        void Should_Return_Count_More_than_1_OrderedGoodsReceive_SuccessFully_From_Quotation();
        void Should_Return_Count_More_than_1_OrderedGoodsReceive_SuccessFully_From_DirectOrder();

    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReceive")]
    public class OrderedGoodsReceiveCountTest : OrderedGoodsReceiveBaseTest, IOrderedGoodsReceiveCountTest
    {
        [Fact]
        public void Should_Return_Count_More_than_1_OrderedGoodsReceive_SuccessFully_From_Quotation()
        {
            // quotation save 
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();

            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.CreateWith(0, 1000, Purpose.ProgramPurpose, QuotationStatus.Running, DateTime.Now.AddDays(5), DateTime.Now.AddDays(1), DateTime.Now)
                .WithBranch(branchFactory.Object)
                .WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, branchFactory.Object).Persist();

            // workorder against quotation 
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _quotationFactory.Object.QuotationStatus = QuotationStatus.PertialIssued;

            _workOrderFactory.Create().WithBranch(branchFactory.Object)
                .WithQuotation(_quotationFactory.Object)
                .WithSupplier(_supplierFactory.Object)
                .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _workOrderFactory.Object)
                .Persist();

            // receive against workorder number
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_workOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_workOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            var usermenu = BuildUserMenu(commonHelper.ConvertIdToList(organizationFactory.Object), commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program),
                commonHelper.ConvertIdToList(branchFactory.Object));
            int count = _goodsReceiveService.CountGoodsReceive(usermenu, organizationFactory.Object.Id, branchFactory.Object.Id, _itemFactory.Object.Name, DateTime.Now.ToString(),
                _workOrderFactory.Object.WorkOrderNo, _workOrderFactory.Object.WorkOrderDate.ToString(), _goodsReceiveFactory.Object.GoodsReceiveStatus.ToString());
            Assert.True(count > 0);

        }

        [Fact]
        public void Should_Return_Count_More_than_1_OrderedGoodsReceive_SuccessFully_From_DirectOrder()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(organizationFactory.Object, commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();
            //receive
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            List<OrderedGoodsRecViewModel> list = _goodsReceiveService.LoadGoodsReceived(0, 0, int.MaxValue, userMenus, organizationFactory.Object.Id, branchFactory.Object.Id,
                _itemFactory.Object.Name, _goodsReceiveFactory.Object.GoodsReceivedDate.ToString(), _directWorkOrderFactory.Object.WorkOrderNo, _directWorkOrderFactory.Object.WorkOrderDate.ToString(), "");
            Assert.True(list.Count > 0);
        }
    }
}
