﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.RequisitionTest
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "Requisition")]
    public class RequisitionGoodsIssueTest : RequisitionBaseTest
    {
        #region Basic Test

        [Fact]
        public void Requisition_Goods_Issue_Test()
        {
            
            organizationFactory.Create().Persist();
            _sessionFactory.Create().Persist();
            _programFactory.Create().WithOrganization(organizationFactory.Object).Persist();

            

            _ItemFactory.CreateWith(0, "test Item"+Guid.NewGuid().ToString(), (int)ItemType.ProgramItem, (int)ItemUnit.Pcs, (int)CostBearer.Corporate)
                        .WithItemGroup()
                        .WithOrganization(organizationFactory.Object).Persist();
            _programSessionItemFactory.Create()
                .WithProgramAndSession(_programFactory.Object, _sessionFactory.Object)
                .WithItem(_ItemFactory.Object)
                .Persist();
            
            var branchFactory = _branchFactory.CreateMore(2, organizationFactory.Object);
            branchFactory.ObjectList[0].IsCorporate = true;
            branchFactory.Persist();
            var userMenu = BuildUserMenu(new List<Organization>() {organizationFactory.Object},
                                            new List<Program>(){ _programFactory.Object },
                                            new List<Branch>() { branchFactory.ObjectList[0], branchFactory.ObjectList[1] });

            var requisitionFactory = _requisitionFactory.CreateWithBranch(organizationFactory.Object, branchFactory.ObjectList[1]);
            var rdFactory = _requisitionDetailsFactory.Create()
                .WithItem(_ItemFactory.Object)
                .WithProgram(_programFactory.Object)
                .WithSession(_sessionFactory.Object)
                .WithRequisition(requisitionFactory.Object);
            requisitionFactory.Object.RequisitionDetails.Add(rdFactory.Object);
            
            _requisitionService.SaveRequisition(requisitionFactory.Object, userMenu);
            
            //requisitionFactory.Persist();

            _goodsReceiveFactory.Create().WithBranch(branchFactory.ObjectList[0]);
            _goodsReceiveDetailsFactory.Create()
                .WithProperty((int) (rdFactory.Object.RequiredQuantity * 2))
                .WithGoodsReceive(true, _goodsReceiveFactory.Object)
                .WithItem(_ItemFactory.Object)
                .WithProgram(_programFactory.Object)
                .WithSession(_sessionFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Persist(true);

            _goodsIssueFactory.Create(_requisitionFactory.Object.Branch).WithRequisition(requisitionFactory.Object).WithBranch(_branchFactory.ObjectList[1]);
            _goodsIssueDetailsFactory.Create()
                .WithGoodsIssue(_goodsIssueFactory.Object)
                .WithItem(rdFactory.Object.Item)
                .WithProgram(rdFactory.Object.Program)
                .WithSession(rdFactory.Object.Session).WithProperty(rdFactory.Object.RequiredQuantity != null ? rdFactory.Object.RequiredQuantity.Value : 0);
            _goodsIssueFactory.Object.GoodsIssueDetails.Add(_goodsIssueDetailsFactory.Object);
            Assert.True(_goodsIssueService.AuthorizeSaveOrUpdateRequisitionGoodsIssue(userMenu, _goodsIssueFactory.Object));
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void InvalidDataException_For_Requisition_Goods_Issue_Null_Object()
        {
            Assert.Throws<NullObjectException>(() => _goodsIssueService.SaveOrUpdateRequisitionGoodsIssue(null));
        }
        
        [Fact]
        public void InvalidDataException_For_Requisition_Goods_Issue_Null_Object_List()
        {
            Assert.Throws<NullObjectException>(() => _goodsIssueService.SaveOrUpdateRequisitionGoodsIssue(new GoodsIssue()));
        }

        [Fact]
        public void InvalidDataException_For_Requisition_Goods_Issue_Details_Rows_Invalid_IssuedQty()
        {
            organizationFactory.Create().Persist();
            _sessionFactory.Create().Persist();
            _programFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _ItemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).Persist();

            var branchFactory = _branchFactory.CreateMore(2, organizationFactory.Object);
            branchFactory.ObjectList[0].IsCorporate = true;
            branchFactory.Persist();

            var requisitionFactory = _requisitionFactory.CreateWithBranch(organizationFactory.Object, branchFactory.ObjectList[1]);
            var rdFactory = _requisitionDetailsFactory.Create()
                .WithItem(_ItemFactory.Object)
                .WithProgram(_programFactory.Object)
                .WithSession(_sessionFactory.Object)
                .WithRequisition(requisitionFactory.Object);
            requisitionFactory.Object.RequisitionDetails.Add(rdFactory.Object);
            requisitionFactory.Persist();

            _goodsReceiveFactory.Create().WithBranch(branchFactory.ObjectList[0]);
            _goodsReceiveDetailsFactory.Create()
                .WithProperty((int)(rdFactory.Object.RequiredQuantity * 2))
                .WithGoodsReceive(true, _goodsReceiveFactory.Object)
                .WithItem(_ItemFactory.Object)
                .WithProgram(_programFactory.Object)
                .WithSession(_sessionFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Persist(true);

            _goodsIssueFactory.Create(_requisitionFactory.Object.Branch).WithRequisition(requisitionFactory.Object).WithBranch(_branchFactory.ObjectList[1]);
            _goodsIssueDetailsFactory.Create()
                .WithGoodsIssue(_goodsIssueFactory.Object)
                .WithItem(rdFactory.Object.Item)
                .WithProgram(rdFactory.Object.Program)
                .WithSession(rdFactory.Object.Session).WithProperty(0);
            _goodsIssueFactory.Object.GoodsIssueDetails.Add(_goodsIssueDetailsFactory.Object);
            Assert.Throws<InvalidDataException>(() => _goodsIssueService.SaveOrUpdateRequisitionGoodsIssue(_goodsIssueFactory.Object));
        }

        [Fact]
        public void InvalidDataException_For_Requisition_Goods_Issue_Details_Low_Stock_Quantity()
        {
            organizationFactory.Create().Persist();
            _sessionFactory.Create().Persist();
            _programFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _ItemFactory.Create().WithItemGroup().WithOrganization(organizationFactory.Object).Persist();

            var branchFactory = _branchFactory.CreateMore(2, organizationFactory.Object);
            branchFactory.ObjectList[0].IsCorporate = true;
            branchFactory.Persist();

            var requisitionFactory = _requisitionFactory.CreateWithBranch(organizationFactory.Object, branchFactory.ObjectList[1]);
            var rdFactory = _requisitionDetailsFactory.Create()
                .WithItem(_ItemFactory.Object)
                .WithProgram(_programFactory.Object)
                .WithSession(_sessionFactory.Object)
                .WithRequisition(requisitionFactory.Object);
            requisitionFactory.Object.RequisitionDetails.Add(rdFactory.Object);
            requisitionFactory.Persist();

            _goodsReceiveFactory.Create().WithBranch(branchFactory.ObjectList[0]);
            _goodsReceiveDetailsFactory.Create()
                .WithGoodsReceive(true, _goodsReceiveFactory.Object)
                .WithItem(_ItemFactory.Object)
                .WithProgram(_programFactory.Object)
                .WithSession(_sessionFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Persist(true);

            _goodsIssueFactory.Create(_requisitionFactory.Object.Branch).WithRequisition(requisitionFactory.Object).WithBranch(_branchFactory.ObjectList[1]);
            _goodsIssueDetailsFactory.Create()
                .WithGoodsIssue(_goodsIssueFactory.Object)
                .WithItem(rdFactory.Object.Item)
                .WithProgram(rdFactory.Object.Program)
                .WithSession(rdFactory.Object.Session).WithProperty(rdFactory.Object.RequiredQuantity != null ? rdFactory.Object.RequiredQuantity.Value : 0);
            _goodsIssueFactory.Object.GoodsIssueDetails.Add(_goodsIssueDetailsFactory.Object);
            Assert.Throws<InvalidDataException>(() => _goodsIssueService.SaveOrUpdateRequisitionGoodsIssue(_goodsIssueFactory.Object));
        }



        #endregion
    }
}
