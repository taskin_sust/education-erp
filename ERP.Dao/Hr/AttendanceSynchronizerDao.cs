﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr 
{
    public interface IAttendanceSynchronizerDao : IBaseDao<AttendanceSynchronizer, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        AttendanceSynchronizer GetSynchronizer(string synchronizerKey, long id);
        #endregion

        #region List Loading Function
        IList<AttendanceSynchronizer> LoadSynchronizer(List<long> organizationIds, List<long> branchIds,List<long> campusIds);
        List<AttendanceSynchronizer> LoadSynchronizer(int start, int length, string orderBy, string orderDir,
            string organizationId, string branchId, string campusId); 
        #endregion

        #region Others Function

        int SynchronizerRowCount(string organizationId, string branchId, string campusId);

        #endregion

        #region Helper Function

        #endregion

        AttendanceSynchronizer GetSynchronizer(string synchronizerKey);


    }
    public class AttendanceSynchronizerDao : BaseDao<AttendanceSynchronizer, long>, IAttendanceSynchronizerDao{

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public AttendanceSynchronizer GetSynchronizer(string synchronizerKey, long id)
        {
            if (id > 0)
            {
                return Session.QueryOver<AttendanceSynchronizer>().Where(x => x.Status != AttendanceSynchronizer.EntityStatus.Delete && x.SynchronizerKey == synchronizerKey && x.Id != id).SingleOrDefault<AttendanceSynchronizer>();
            }
            else
            {
                return Session.QueryOver<AttendanceSynchronizer>().Where(x => x.Status == AttendanceSynchronizer.EntityStatus.Active && x.SynchronizerKey == synchronizerKey).SingleOrDefault<AttendanceSynchronizer>();
            }
        }
        #endregion

        #region List Loading Function
        public IList<AttendanceSynchronizer> LoadSynchronizer(List<long> organizationIds, List<long> branchIds, List<long> campusIds)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceSynchronizer>();
            criteria.Add(Restrictions.Eq("Status", AttendanceSynchronizer.EntityStatus.Active));

            criteria.CreateAlias("Campus", "c").Add(Restrictions.Eq("c.Status", Campus.EntityStatus.Active));
            criteria.CreateAlias("c.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            if (organizationIds != null)
            {
                    criteria.Add(Restrictions.In("br.Organization.Id", organizationIds));
            }
            if (branchIds != null)
            {
                criteria.Add(Restrictions.In("br.Id", branchIds));
            }
            if (campusIds != null)
            {
                criteria.Add(Restrictions.In("c.Id", campusIds));
            }
            return criteria.List<AttendanceSynchronizer>().OrderBy(x => x.Rank).ToList();
        }

        public List<AttendanceSynchronizer> LoadSynchronizer(int start, int length, string orderBy, string orderDir, string organizationId, string branchId,
            string campusId)
        {
            ICriteria criteria = GetSynchronizerCriteria(organizationId, branchId, campusId);
            if (orderDir == "ASC")
            {
                criteria.AddOrder(Order.Asc(orderBy));
            }
            else
            {
                criteria.AddOrder(Order.Desc(orderBy));
            }

            return (List<AttendanceSynchronizer>)criteria.SetFirstResult(start).SetMaxResults(length).List<AttendanceSynchronizer>();
        }

        #endregion

        #region Others Function
        public int SynchronizerRowCount(string organizationId, string branchId, string campusId)
        {
            ICriteria criteria = GetSynchronizerCriteria(organizationId, branchId, campusId);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }
        #endregion

        #region Helper Function

        private ICriteria GetSynchronizerCriteria(string organizationId, string branchId, string campusId)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceSynchronizer>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", AttendanceSynchronizer.EntityStatus.Delete)));
            criteria.CreateAlias("Campus", "c").Add(Restrictions.Eq("c.Status", Campus.EntityStatus.Active));
            criteria.CreateAlias("c.Branch", "b").Add(Restrictions.Eq("b.Status", Branch.EntityStatus.Active));

            if (!String.IsNullOrEmpty(organizationId))
            {
                criteria.CreateAlias("b.Organization", "o").Add(Restrictions.Eq("o.Status", Branch.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("o.Id", Convert.ToInt64(organizationId)));
            }
            if (!String.IsNullOrEmpty(branchId))
            {
                criteria.Add(Restrictions.Eq("b.Id", Convert.ToInt64(branchId)));
            }
            if (!String.IsNullOrEmpty(campusId))
            {
                criteria.Add(Restrictions.Eq("c.Id", Convert.ToInt64(campusId)));
            }
            return criteria;
        }

        #endregion

        //for api 
        public AttendanceSynchronizer GetSynchronizer(string synchronizerKey)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceSynchronizer>();
            criteria.Add(Restrictions.Eq("Status", AttendanceSynchronizer.EntityStatus.Active));
            criteria.CreateAlias("AttendanceDevice", "attendanceDevice").Add(Restrictions.Eq("attendanceDevice.Status", AttendanceDevice.EntityStatus.Active));
            return criteria.UniqueResult<AttendanceSynchronizer>();
        }
        
    }

}
