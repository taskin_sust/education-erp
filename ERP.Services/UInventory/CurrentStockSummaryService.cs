using System;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
using System.Collections.Generic;
using NHibernate.Util;
using UdvashERP.MessageExceptions;

namespace UdvashERP.Services.UInventory
{
    public interface ICurrentStockSummaryService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(CurrentStockSummary currentStockSummary);

        #endregion

        #region Single Instances Loading Function

        //int GetItemCurrentStock(List<BusinessModel.Entity.UserAuth.UserMenu> userMenus, long itemId, long branchId, long? programId, long? sessionId);
        //CurrentStockSummary GetStockQuantity(List<BusinessModel.Entity.UserAuth.UserMenu> userMenus, string programSession, long branchIdFrom, long itemId);
        CurrentStockSummary GetCurrentStockSummary(List<BusinessModel.Entity.UserAuth.UserMenu> userMenus, long itemId, long branchId, long? programId, long? sessionId);

        #endregion

        #region List Loading Function

        IList<dynamic> LoadCurrentStock(AuthorizeBranchNameDelegate branchNameDelegate, int start, int length, List<UserMenu> userMenu, long organizationId, List<long> branchIdList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<string> proSessionList);

        #endregion

        #region Others Function

        int GetCurrentStockQuantity(List<UserMenu> userMenus, long itemId, long branchId, long? programId = null, long? sessionId = null);
        int CountCurrentStock(AuthorizeBranchNameDelegate branchNameDelegate, List<UserMenu> userMenu, long organizationId, List<long> branchIdList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList, List<string> proSessionList);


        #endregion

        #region Helper Function

        #endregion




    }
    public class CurrentStockSummaryService : BaseService, ICurrentStockSummaryService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("InventoryService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly ICurrentStockSummaryDao _currentStockSummaryDao;
        private readonly IProgramDao _programDao;
        private readonly IProgramBranchSessionDao _programBranchSessionDao;
        private readonly IBranchDao _branchDao;
        public CurrentStockSummaryService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _currentStockSummaryDao = new CurrentStockSummaryDao { Session = session };
            _programDao = new ProgramDao() { Session = session };
            _programBranchSessionDao = new ProgramBranchSessionDao() { Session = session };
            _branchDao = new BranchDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(CurrentStockSummary currentStockSummary)
        {

            return true;
        }

        //public int GetItemCurrentStock(List<BusinessModel.Entity.UserAuth.UserMenu> userMenus, long itemId, long branchId, long? programId, long? sessionId)
        //{
        //    try
        //    {
        //        return _currentStockSummaryDao.GetItemCurrentStock(itemId, branchId, programId, sessionId);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}



        #endregion

        #region Single Instances Loading Function

        public CurrentStockSummary GetCurrentStockSummary(List<BusinessModel.Entity.UserAuth.UserMenu> userMenus, long itemId, long branchId, long? programId, long? sessionId)
        {
            try
            {
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, null, programId != null ? _commonHelper.ConvertIdToList((long)programId) : null, _commonHelper.ConvertIdToList(branchId));
                if (!EnumerableExtensions.Any(authBranchIdList))
                    throw new MessageException("You are not authorize.");

                if (programId != null)
                {
                    List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenus, null, authBranchIdList, _commonHelper.ConvertIdToList((long)programId));
                    if (!EnumerableExtensions.Any(authProgramIdList))
                        throw new MessageException("You are not authorize.");
                }

                return _currentStockSummaryDao.GetCurrentStockSummary(itemId, branchId, programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<dynamic> LoadCurrentStock(AuthorizeBranchNameDelegate branchNameDelegate, int start, int length, List<UserMenu> userMenus,
            long organizationId, List<long> branchIdList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList,
            List<long> sessionIdList, List<string> programsession)
        {
            try
            {
                if (organizationId <= 0)
                    throw new MessageExceptions.InvalidDataException("Invalid Organization");
                if (!EnumerableExtensions.Any(branchIdList))
                    throw new MessageExceptions.InvalidDataException("Invalid Branch");
                if (userMenus == null || !EnumerableExtensions.Any(userMenus))
                    throw new MessageExceptions.InvalidDataException("Invalid User menu");
                bool isAddNa = false;
                bool isOnlyNa = false;
                string[] psAry = programsession.ToArray();
                if (psAry.Length > 0)
                    psAry = psAry.Distinct().ToArray();

                // selectNone means N/A
                if (psAry.Contains(SelectionType.SelectNone.ToString()))
                {
                    isAddNa = true;
                    psAry = psAry.Skip(1).Take(psAry.Length - 1).ToArray();
                    if (psAry.Length <= 0) isOnlyNa = true;
                }
                if (psAry.Length == 1 && psAry.Contains(SelectionType.SelelectAll.ToString()))
                    isAddNa = true;

                string[] programSessionAuthArrayList = BuildAuthProgramSession(userMenus, organizationId, branchIdList.ToArray(), itemIdList.ToArray(), psAry);
                var proSessionList = programSessionAuthArrayList.ToList();
                if (isOnlyNa) { proSessionList = new List<string>(); }
                if (isAddNa) { proSessionList.Insert(0, SelectionType.SelectNone.ToString()); }

                List<long> brIds = branchIdList;
                if (brIds.Contains(SelectionType.SelelectAll))
                    brIds = null;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, programIdList, brIds);
                List<long> authProgramList = AuthHelper.LoadProgramIdList(userMenus, organizationIdList, authBranchIdList, programIdList);
                string branchNameStr = GetBranchNameByAuthorizedBranch(branchNameDelegate, userMenus, organizationIdList, authBranchIdList);
                return _currentStockSummaryDao.LoadCurrentStock(start, length, authBranchIdList, itemGroupIdList, itemIdList, authProgramList,
                    sessionIdList, branchNameStr, proSessionList, isOnlyNa);
            }
            catch (MessageExceptions.InvalidDataException ide)
            {
                _logger.Error(ide);
                throw ide;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        public int GetCurrentStockQuantity(List<UserMenu> userMenus, long itemId, long branchId, long? programId = null, long? sessionId = null)
        {
            try
            {
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, null, programId != null ? _commonHelper.ConvertIdToList((long)programId) : null, _commonHelper.ConvertIdToList(branchId));
                if (!EnumerableExtensions.Any(authBranchIdList))
                    throw new MessageException("You are not authorize.");

                if (programId != null)
                {
                    List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenus, null, authBranchIdList, _commonHelper.ConvertIdToList((long)programId));
                    if (!EnumerableExtensions.Any(authProgramIdList))
                        throw new MessageException("You are not authorize.");
                }

                return _currentStockSummaryDao.GetCurrentStockQuantity(itemId, branchId, programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int CountCurrentStock(AuthorizeBranchNameDelegate branchNameDelegate, List<UserMenu> userMenus, long organizationId,
            List<long> branchIdList, List<long> itemGroupIdList, List<long> itemIdList, List<long> programIdList, List<long> sessionIdList,
            List<string> proSession)
        {
            try
            {
                if (organizationId <= 0)
                    throw new InvalidDataException("Invalid Organization");
                if (!EnumerableExtensions.Any(branchIdList))
                    throw new InvalidDataException("Invalid Branch");
                if (userMenus == null || !EnumerableExtensions.Any(userMenus))
                    throw new InvalidDataException("Invalid User menu");
                bool isAddNa = false;
                bool isOnlyNa = false;
                string[] psAry = proSession.ToArray();
                if (psAry.Length > 0)
                    psAry = psAry.Distinct().ToArray();

                // selectNone means N/A
                if (psAry.Contains(SelectionType.SelectNone.ToString()))
                {
                    isAddNa = true;
                    psAry = psAry.Skip(1).Take(psAry.Length - 1).ToArray();
                    if (psAry.Length <= 0) isOnlyNa = true;
                }
                if (psAry.Length == 1 && psAry.Contains(SelectionType.SelelectAll.ToString()))
                    isAddNa = true;

                string[] programSessionAuthArrayList = BuildAuthProgramSession(userMenus, organizationId, branchIdList.ToArray(), itemIdList.ToArray(), psAry);
                var proSessionList = programSessionAuthArrayList.ToList();
                if (isOnlyNa) { proSessionList = new List<string>(); }
                if (isAddNa) { proSessionList.Insert(0, SelectionType.SelectNone.ToString()); }
                List<long> brIds = branchIdList;
                if (brIds.Contains(SelectionType.SelelectAll))
                    brIds = null;
                var organizationIdList = _commonHelper.ConvertIdToList(organizationId);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenus, organizationIdList, programIdList, brIds);
                string branchNameStr = GetBranchNameByAuthorizedBranch(branchNameDelegate, userMenus, organizationIdList, authBranchIdList);
                return _currentStockSummaryDao.CountCurrentStock(authBranchIdList, itemGroupIdList, itemIdList, programIdList,
                    sessionIdList, branchNameStr, proSessionList, isOnlyNa);

            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #endregion

        #region Helper function

        private string GetBranchNameByAuthorizedBranch(AuthorizeBranchNameDelegate branchNameDelegate, List<UserMenu> userMenus, List<long> organizationIdList, List<long> authBranchIdList)
        {
            IList<string> branchNameList = branchNameDelegate(userMenus, organizationIdList, authBranchIdList);
            string branchNameStr = "";
            for (int i = 0; i < branchNameList.Count; i++)
            {
                branchNameStr += "[" + branchNameList[i].Trim() + "]";
                if (i < branchNameList.Count - 1)
                    branchNameStr += ",";
            }
            return branchNameStr;
        }


        public string[] BuildAuthProgramSession(List<UserMenu> authMenu, long organizationId, long[] branchIds, long[] itemIds, string[] programSessionIds = null)
        {
            try
            {
                List<long> authProIdList = new List<long>();
                List<long> brIds = Enumerable.ToList(branchIds);
                List<long> itemList = itemIds.ToList();
                if (itemList.Count == 1 && itemList.Contains(0)) itemList = null;
                if (brIds.Count == 1 && brIds.Contains(0)) brIds = null;
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(authMenu, (organizationId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(organizationId) : null);
                List<long> authBr = AuthHelper.LoadBranchIdList(authMenu, authOrgList, null, brIds);
                if (programSessionIds == null) authProIdList = AuthHelper.LoadProgramIdList(authMenu, authOrgList, authBr);
                else
                {
                    foreach (var programsession in programSessionIds)
                    {
                        string[] programSessionArray = programsession.Split(new string[] { "::" }, StringSplitOptions.None);
                        Program program = _programDao.LoadById(Convert.ToInt64(programSessionArray[0]));
                        if (program != null)
                            authProIdList.Add(program.Id);
                    }
                    authProIdList = AuthHelper.LoadProgramIdList(authMenu, authOrgList, authBr, authProIdList);
                }

                if (!EnumerableExtensions.Any(authProIdList))
                    throw new MessageExceptions.InvalidDataException("Program can't be empty");
                var authbranchIdlist = new List<long>();
                foreach (var branchId in authBr)
                {
                    if (branchId > 0)
                    {
                        var branch = _branchDao.LoadById((long)branchId);
                        if (branch.IsCorporate)
                        {
                            authbranchIdlist = null;
                            break;
                        }
                        else
                            authbranchIdlist.Add((long)branchId);
                    }
                }

                IList<ProgramSessionDto> proSesDtos = _programBranchSessionDao.LoadProgramSession(authOrgList, authbranchIdlist, authProIdList, itemList);
                var ary = new string[proSesDtos.Count];
                int index = 0;
                foreach (var programSessionDto in proSesDtos)
                {
                    ary[index] = programSessionDto.ProgramAndSessionId;
                    index++;
                }
                return ary;
            }
            catch (MessageExceptions.InvalidDataException ide)
            {
                _logger.Error(ide);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #endregion
    }
}

