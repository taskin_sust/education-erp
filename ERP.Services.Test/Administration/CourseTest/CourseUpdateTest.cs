﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Administration.CourseTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Course")]
    public class CourseUpdateTest : CourseBaseTest
    {
        #region Basic test
        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _courseService.CourseSaveOrUpdate(null));
        }

        [Fact]
        public void Update_Should_Return_Model_inValid()
        {
            List<Subject> subjectList = _subjectFactory.CreateMore(5).Persist().GetLastCreatedObjectList();
            var subjectAssign = new SubjectAssign
            {
                SelectedSubjects = subjectList.Select(x => Convert.ToInt32(x.Id)).ToArray()
            };
            _courseFactory.Create().WithName(_name).WithStartDateAndEndDate(DateTime.Now.AddDays(3), DateTime.Now.AddDays(5))
                           .WithOrganization().WithProgram().WithSession()
                           .WithMaxMinSubject(4, 2, 2);
            var course = _courseFactory.Object;
            Program program = course.Program;
            Session sessionObj = course.RefSession;
            Organization organization = course.Organization;
            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, program.Id, sessionObj.Id);
            _courseSubjectFactory.CreateMore(_courseFactory.Object, subjectList);
            _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList()).Persist();
            Assert.NotEqual(_courseFactory.Object.Id, 0);
            Assert.NotNull(_courseFactory.Object.Id);
            CleanUpProgramSessionSubject(program.Id, sessionObj.Id);
            _courseSubjectFactory.CreateMore(_courseFactory.Object, subjectList);
            _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList()).Persist();
            _courseFactory.WithMaxMinSubject(400, 2, 2);
            Assert.Throws<InvalidDataException>(() => _courseService.CourseSaveOrUpdate(_courseFactory.Object));
            CleanUpProgramSessionSubject(program.Id, sessionObj.Id);
        }

        [Fact]
        public void Update_Should_Check_Duplicate()
        {
            Organization organization = organizationFactory.Create().Persist().Object;
            Program program = _programFactory.Create().WithOrganization(organization).Persist().Object;
            Session sessionObj = _sessionFactory.Create().Persist().Object;
            List<Subject> subjectList = _subjectFactory.CreateMore(5).Persist().GetLastCreatedObjectList();


            var subjectAssign = new SubjectAssign
            {
                SelectedSubjects = subjectList.Select(x => Convert.ToInt32(x.Id)).ToArray()
            };

            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, program.Id, sessionObj.Id);

            _courseFactory.Create().WithName(_name).WithStartDateAndEndDate(DateTime.Now.AddDays(5), DateTime.Now.AddDays(35))
                .WithOrganization(organization).WithProgram(program).WithSession(sessionObj)
                .WithMaxMinSubject(4, 2, 2);
            _courseSubjectFactory.CreateMore(_courseFactory.Object, subjectList);
            var object1 = _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList()).Persist().Object;


            _courseFactory.Create().WithName(_name + " Update").WithStartDateAndEndDate(DateTime.Now.AddDays(5), DateTime.Now.AddDays(35))
                .WithOrganization(organization).WithProgram(program).WithSession(sessionObj)
                .WithMaxMinSubject(4, 2, 2);
            _courseSubjectFactory.CreateMore(_courseFactory.Object, subjectList);
            var object2 = _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList()).Persist().Object;
            object2.Name = object1.Name;
            Assert.Throws<DuplicateEntryException>(() => _courseService.CourseSaveOrUpdate(object2));
            CleanUpProgramSessionSubject(program.Id,sessionObj.Id);
        }

        [Fact]
        public void Update_Should_Successfully()
        {
            Organization organization = organizationFactory.Create().Persist().Object;
            Program program = _programFactory.Create().WithOrganization(organization).Persist().Object;
            Session sessionObj = _sessionFactory.Create().Persist().Object;
            List<Subject> subjectList = _subjectFactory.CreateMore(5).Persist().GetLastCreatedObjectList();


            var subjectAssign = new SubjectAssign
            {
                SelectedSubjects = subjectList.Select(x => Convert.ToInt32(x.Id)).ToArray()
            };

            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, program.Id, sessionObj.Id);

            _courseFactory.Create().WithName(_name).WithStartDateAndEndDate(DateTime.Now.AddDays(5), DateTime.Now.AddDays(35))
                .WithOrganization(organization).WithProgram(program).WithSession(sessionObj)
                .WithMaxMinSubject(4, 2, 2);
            _courseSubjectFactory.CreateMore(_courseFactory.Object, subjectList);
            var object1 = _courseFactory.WithCourseSubject(_courseSubjectFactory.GetLastCreatedObjectList()).Persist().Object;
            object1.Name = _name + "Updated";
            _courseService.CourseSaveOrUpdate(object1);
            var getObj = _courseService.GetCourse(object1.Id);

            Assert.Equal(_name + "Updated", getObj.Name);

            CleanUpProgramSessionSubject(program.Id,sessionObj.Id);
        }

        #endregion
    }
}
