using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface IItemService : IBaseService
    {

        #region Operational Function

        bool SaveOrUpdate(IList<Item> itemList);
        bool IsDelete(long id);

        #endregion

        #region Single Instances Loading Function

        Item LoadById(long id);

        #endregion

        #region List Loading Function

        IList<Item> LoadItem(int start, int length, string orderBy, string orderDir, long? organization, int? itemType, long? itemGroup, string name, int? status);
        IList<Item> LoadAssignItemList(string[] programSessionIdList, int itemType, long itemGroupId,long organizationId);
        IList<Item> LoadItem(long? organizationId = null, long? itemGroupId = null, int? itemType = null, bool? hasSpecificationCriteria = null,bool withCommonItems=false);
        IList<Item> LoadItem(List<UserMenu> userMenu,long fromOrganizationId, long toOrganizationId, long[] itemGroupId);
        IList<Item> LoadItem(long[] itemGroupIds, long? organizationId);
        IList<Item> LoadItemList(long[] itemIds);
        IList<Item> LoadItemForOrganizationOrGroup(List<UserMenu> userMenu, long? organizationId, long[] itemGroupId = null);
        //IList<Item> LoadItemForOrganizationOrGroup(long? organizationId, long[] itemGroupId = null); 
        
        #endregion

        #region Others Function

        int LoadItemCount(long? organization, int? itemType, long? itemGroup, string name, int? status);
        
        #endregion

        #region Helper Function
        #endregion
        
    }
    public class ItemService : BaseService, IItemService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("InventoryService");

        #endregion

        #region Propertise & Object Initialization

        private readonly IItemDao _itemDao;
        private readonly IWorkOrderDetailDao _workOrderDetailsDao;
        private ICommonHelper _commonHelper;
        public ItemService(ISession session)
        {
            Session = session;
            _itemDao = new ItemDao { Session = session };
            _workOrderDetailsDao = new WorkOrderDetailsDao() { Session = session };
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(IList<Item> itemList)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeSaveOrUpdate(itemList);
                using (transaction = Session.BeginTransaction())
                {
                    foreach (var item in itemList)
                    {
                        Item obj = item;
                        if (item.Id == 0)
                        {
                            obj.Status = Item.EntityStatus.Active;
                            obj.Rank = _itemDao.GetMaximumRank(item) + 1;
                        }
                        else
                        {
                            obj = _itemDao.LoadById(item.Id);
                            obj.Organization = item.Organization;
                            obj.ItemUnit = item.ItemUnit;
                            obj.ItemType = item.ItemType;
                            obj.ItemGroup = item.ItemGroup;
                            obj.Name = item.Name;
                            obj.Status = item.Status;
                        }
                        _itemDao.SaveOrUpdate(obj);
                    }
                    transaction.Commit();
                    return true;
                }
            }
            catch (MessageException)
            {
                throw;
            }
            catch (EmptyFieldException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (DuplicateEntryException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public bool IsDelete(long id)
        {
            ITransaction trans = null;
            try
            {
                var obj = _itemDao.LoadById(id);
                if (obj == null)
                    throw new NullObjectException("Item is not valid");

                CheckBeforeDelete(obj);
                using (trans = Session.BeginTransaction())
                {
                    obj.Status = Item.EntityStatus.Delete;
                    _itemDao.Update(obj);
                    trans.Commit();
                    return true;
                }
            }
            catch (DependencyException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public Item LoadById(long id)
        {
            try
            {
                return _itemDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<Item> LoadItem(int start, int length, string orderBy, string orderDir, long? organization, int? itemType, long? itemGroup, string name, int? status)
        {
            try
            {
                return _itemDao.LoadItem(start, length, orderBy, orderDir, organization, itemType, itemGroup, name, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Item> LoadItem(long? organizationId = null, long? itemGroupId = null, int? itemType = null, bool? hasSpecificationCriteria = null,bool withCommonItems=false)
        {
            try
            {
                return _itemDao.LoadItem(organizationId, itemGroupId, itemType, hasSpecificationCriteria,withCommonItems);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Item> LoadAssignItemList(string[] programSessionIdList, int itemType, long itemGroupId,long organizationId)
        {
            try
            {
                //if (programSessionIdList.Length==0)
                //    throw new NullObjectException("Program and session is empty");
                return _itemDao.LoadAssignItemList(programSessionIdList, itemType, itemGroupId,organizationId);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Item> LoadItem(long[] itemGroupIds, long? organizationId)
        {
            try
            {
                return _itemDao.LoadItem(itemGroupIds, organizationId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Item> LoadItemList(long[] itemIds)
        {
            try
            {
                var itemList=_itemDao.LoadItemList(itemIds);
                return itemList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Item> LoadItemForOrganizationOrGroup(List<UserMenu> userMenu, long? organizationId, long[] itemGroupIds = null)
        {
            try
            {
                var organizationIdList = organizationId!=null ? _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)) : null;
                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organizationIdList);
                List<long> authProgramList = AuthHelper.LoadProgramIdList(userMenu, authOrganizationIdList);
                return _itemDao.LoadItemForOrganizationOrGroup(authOrganizationIdList,authProgramList,itemGroupIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Item> LoadItem(List<UserMenu> userMenu,long fromOrganizationId, long toOrganizationId, long[] itemGroupId)
        {
            try
            {
                List<long> orgIdList = new List<long> {fromOrganizationId, toOrganizationId};
                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, orgIdList);
                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu, authOrganizationIdList);
                return _itemDao.LoadItemForOrganizationOrGroup(authOrganizationIdList, authProgramIdList, itemGroupId);
                //return _itemDao.LoadItem(fromOrganizationId, toOrganizationId, itemGroupId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        //public IList<Item> LoadItemForOrganizationOrGroup(long? organizationId, long[] itemGroupId = null)
        //{
        //    try
        //    {
        //        return _itemDao.LoadItemForOrganizationOrGroup(organizationId, itemGroupId);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //}

        #endregion

        #region Others Function

        public int LoadItemCount(long? organization, int? itemType, long? itemGroup, string name, int? status)
        {
            try
            {
                return _itemDao.LoadItemCount(organization, itemType, itemGroup, name, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        private void CheckBeforeDelete(Item obj)
        {
            
            bool count = _workOrderDetailsDao.HasManyItemObjects(obj);
            if (count == false){
                throw new DependencyException("You can't delete this Item , item is already declared or assign.");
            }

        }

        private void CheckBeforeSaveOrUpdate(IList<Item> itemList)
        {
            if (itemList==null)
                throw new NullObjectException("Item list is empty.");

            if (!itemList.Any())
                throw new NullObjectException("Item list is empty.");

            foreach (var item in itemList)
            {
                string fieldName;
                CustomModelValidationCheck.DataAnnotationCheck(item, new Item.ItemMetaData());
                
                if (item == null)
                    throw new InvalidDataException("Invalid item found!");

                if (!_commonHelper.ContainsEnumId<ItemType>(item.ItemType))
                    throw new InvalidDataException("Invalid item type found!");

                if (!_commonHelper.ContainsEnumId<ItemUnit>(item.ItemUnit))
                    throw new InvalidDataException("Invalid item unit found!");

                if (!_commonHelper.ContainsEnumId<CostBearer>(item.CostBearer))
                    throw new InvalidDataException("Invalid cost bearer found!");
                if (item.ItemGroup == null)
                    throw new InvalidDataException("Invalid item group found!");

                var checkDuplicateName = _itemDao.IsDuplicateItem(out fieldName, item.Name, item.Id);
               
                if (checkDuplicateName)
                {
                    if (fieldName == "Name")
                        throw new DuplicateEntryException("Duplicate item found");
                }
            }
        }

        #endregion

    }
}

