﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model.BusinessModel;

namespace DeviceSynchronizer.DeviceHelper
{
    public delegate DateTime? GetStandardTime();
    public delegate void MessagePass(string message, MessageType messageType);
}
