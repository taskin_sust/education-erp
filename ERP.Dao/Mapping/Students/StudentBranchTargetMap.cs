﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentBranchTargetMap : BaseClassMap<StudentBranchTarget, long>
    {
        public StudentBranchTargetMap()
        {
            Table("StudentAdmissionBranchTarget");
            LazyLoad();
            //Map(x => x.InternalTarget);
            //Map(x => x.ExternalTarget);
            Map(x => x.Target);
            References(x => x.Branch).Column("BranchId");
            References(x => x.StudentAdmissionTarget).Column("StudentAdmissionTargetId");
        }
    }
}
