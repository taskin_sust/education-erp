﻿using System.Collections.Generic;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface IExtraCurricularActivityDao : IBaseDao<ExtraCurricularActivity, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<ExtraCurricularActivity> LoadExtraCurricularActivity();
        IList<ExtraCurricularActivity> LoadExtraCurricularActivity(string[] organizationIds);
        #endregion

        #region Others Function

        #endregion
    }

    public class ExtraCurricularActivityDao : BaseDao<ExtraCurricularActivity, long>, IExtraCurricularActivityDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<ExtraCurricularActivity> LoadExtraCurricularActivity()
        {
            return
                Session.QueryOver<ExtraCurricularActivity>()
                    .Where(x => x.Organization == null && x.Status == ExtraCurricularActivity.EntityStatus.Active)
                    .List<ExtraCurricularActivity>();

            //var criteria = Session.CreateCriteria<ExtraCurricularActivity>();
            //criteria.Add(Restrictions.Eq("Status", ExtraCurricularActivity.EntityStatus.Active));
            //return criteria.List<ExtraCurricularActivity>();
        }

        public IList<ExtraCurricularActivity> LoadExtraCurricularActivity(string[] organizationIds)
        {
            var criteria = Session.CreateCriteria<ExtraCurricularActivity>();
            criteria.Add(Restrictions.In("Organization.Id", organizationIds));
            return criteria.List<ExtraCurricularActivity>();
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
