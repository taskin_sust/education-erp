﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qmaster.Print.Model
{
    public class Exam
    {
        public virtual long Id { get; set; }
        public virtual long CourseId { get; set; }
        public virtual int UniqueSet { get; set; }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual bool IsBanglaVersion { get; set; }
        public virtual bool IsEnglishVersion { get; set; }

    }


    public class ExamUniqueSet
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }

    }
}
