using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{


    public partial class SmsStudentRegistrationReceiverMap : BaseClassMap<SmsStudentRegistrationReceiver, long>
    {

        public SmsStudentRegistrationReceiverMap()
        {
            Table("SmsStudentRegistrationReceiver");
            LazyLoad();
            
            References(x => x.SmsReceiver).Column("SmsReceiverId");
            References(x => x.SmsType).Column("SmsTypeId");
            References(x => x.SmsStudentRegistrationSetting).Column("SmsStudentRegistrationSettingId");
        }
    }
}
