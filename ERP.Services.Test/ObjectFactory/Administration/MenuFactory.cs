﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class MenuFactory : ObjectFactoryBase<Menu>
    {
        #region Object Initialization

        protected readonly IMenuService _menuService;
        private MenuGroupFactory _menuGroupFactory;
        private AreaControllersFactory _areaControllersFactory;
        private ActionsFactory _actionsFactory;

        public MenuFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _menuService = new MenuService(_session);
            _menuGroupFactory = new MenuGroupFactory(this, _session);
            _areaControllersFactory = new AreaControllersFactory(this, _session);
            _actionsFactory = new ActionsFactory(this, _session);
        }

        #endregion

        #region Create

        public MenuFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }

        public MenuFactory WithVariable(string variable)
        {
            Object.VariableNames = variable;
            return this;
        }

        public MenuFactory WithMenuGroup()
        {
            var menuGroup =
                _menuGroupFactory.Create().WithNameAndRank(Guid.NewGuid() + "Name_ABC DEF GHI", 1).WithDisplayText(Guid.NewGuid() + "DisplayText_ABC DEF GHI")
                    .Persist().Object;
            Object.MenuGroup = menuGroup;
            return this;
        }

        public MenuFactory WithMenuGroup(MenuGroup menuGroup)
        {
            Object.MenuGroup = menuGroup;
            return this;
        }

        public MenuFactory WithAction()
        {
            var controller = _areaControllersFactory.Create().WithArea(Guid.NewGuid() + " HFG RTTRT GGFG").Persist().Object;
            var action = _actionsFactory.Create().WithController(controller).WithNameAndRank(Guid.NewGuid() + "Name_ABC DEF GHI", 1).Persist().Object;
            Object.DefaultAction = action;
            return this;
        }

        public MenuFactory WithAction(Actions action)
        {
            Object.DefaultAction = action;
            return this;
        }


        public MenuFactory Create()
        {
            Object = new Menu()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Menu.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MenuFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _menuService.Save(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _menuService.Save(item);
                    }
                }
            }
            return this;
        }

        public MenuFactory CreateMore(int numberOfMenu)
        {
            CreateMenuList(numberOfMenu);
            return this;
        }

        private List<Menu> CreateMenuList(int numberOfMenu)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Menu>();
            var controller = _areaControllersFactory.Create().WithArea(Guid.NewGuid() + " HFG RTTRT GGFG").Persist().Object;
            var action =
                _actionsFactory.Create()
                    .WithController(controller)
                    .WithNameAndRank(Guid.NewGuid() + "Name_ABC DEF GHI", 1)
                    .Persist()
                    .Object;
            var menuGroup =
                _menuGroupFactory.Create()
                    .WithNameAndRank(Guid.NewGuid() + "Name_ABC DEF GHI", 1)
                    .WithDisplayText(Guid.NewGuid() + "DisplayText_ABC DEF GHI")
                    .Persist().Object;
            for (int i = 0; i < numberOfMenu; i++)
            {
                var menu = new Menu()
                {
                    Id = 0,
                    Name = Guid.NewGuid() + "_" + i,
                    Rank = i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Menu.EntityStatus.Active,
                    DefaultAction = action,
                    MenuGroup = menuGroup
                };
                list.Add(menu);
            }
            ObjectList.AddRange(list);
            return list;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void Cleanup()
        {
            DeleteAll();
            _actionsFactory.Cleanup();
            _areaControllersFactory.Cleanup();
        }

        #endregion

    }
}
