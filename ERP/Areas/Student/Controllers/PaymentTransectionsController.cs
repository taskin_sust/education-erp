﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.AspNet.Identity;
using UdvashERP.App_code;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.App_Start;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;
using WebGrease.Css.Extensions;
using Constants = UdvashERP.BusinessRules.Constants;
using System.Drawing;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using UdvashERP.BusinessRules.Student;
using UdvashERP.MessageExceptions;
using Rectangle = iTextSharp.text.Rectangle;
using UdvashERP.Properties;
using InvalidDataException = System.IO.InvalidDataException;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class PaymentTransectionsController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly int _duplicatePaymentBlockingTimeMinute = 10;

        private readonly IAdmissionService _admissionService;
        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly IBatchService _batchService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly ICourseService _courseService;
        private readonly IStudentService _studentService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly IStudentCourseDetailsService _studentCourseDetailsService;
        private readonly ICourseSubjectService _courseSubjectService;
        private readonly IDiscountService _discountService;
        private readonly ICommonHelper _commonHelper;
        private readonly IReferenceService _referenceService;
        private readonly IStudentPaymentService _studentPaymentService;
        private readonly IStudentPaymentTransectionsService _studentPaymentTransectionsService;
        private readonly IStudentIdCardService _studentIdCardService;
        private readonly IMaterialDistributionService _materialDistributionService;
        private IOrganizationService _organizationService;
        private IDiscountDetailService _discountDetailService;

        private decimal receivableAmountWithoutDiscount = 0;
        private decimal receivableAmountWithDiscount = 0;
        private readonly IUserService _userService;
        private List<UserMenu> _userMenu;

        public PaymentTransectionsController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _userService = new UserService(session);
                _admissionService = new AdmissionService(session);
                _sessionService = new SessionService(session);
                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _campusService = new CampusService(session);
                _batchService = new BatchService(session);
                _courseService = new CourseService(session);
                _studentService = new StudentService(session);
                _studentProgramService = new StudentProgramService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
                _studentCourseDetailsService = new StudentCourseDetailsService(session);
                _courseSubjectService = new CourseSubjectService(session);
                _discountService = new DiscountService(session);
                _commonHelper = new CommonHelper();
                _referenceService = new ReferenceService(session);
                _studentPaymentService = new StudentPaymentService(session);
                _studentPaymentTransectionsService = new StudentPaymentTransectionsService(session);
                _studentIdCardService = new StudentIdCardService(session);
                _materialDistributionService = new MaterialDistributionService(session);
                _organizationService = new OrganizationService(session);
                _discountDetailService = new DiscountDetailService(session);
                _duplicatePaymentBlockingTimeMinute = AppConfigHelper.GetInt32("DuplicatePaymentBlockingTimeInMinute", 10);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        #endregion

        #region Operational Function

        public ActionResult Index()
        {
            return RedirectToAction("TransactionsReport");
        }

        #region Create 
        public ActionResult Create()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionList = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new SelectList(new List<Campus>(), "Id", "Name");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public JsonResult Create(StudentPaymentTransections paymentTransections)
        {
            string message = "";
            bool successState = false;
            try
            {
                paymentTransections.Serial2 = paymentTransections.Serial1;
                paymentTransections.AdmissionDate = paymentTransections.ReceivedDate;
                paymentTransections.PaymentMethod = (int)PaymentMethod.Cash;
                paymentTransections.PaymentType = PaymentType.MoneyReceipt;
                paymentTransections.IsSubmitted =true;
                _studentPaymentTransectionsService.StudentPaymentTransectionsSave(paymentTransections);
                message = "Successfully Save";
                successState = true;
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        #endregion

        #region Delete

        public ActionResult TransactionsReportEdit()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                organizations.Insert(0, new Organization() { ShortName = "Select Organization", Id = -1 }); //0 for all
                ViewBag.organizationList = new SelectList(organizations, "Id", "ShortName");
                //get session list
                var sessionList = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Select Session", Value = "" } }, "Value", "Text");
                List<SelectListItem> sessionListAll = sessionList.ToList();
                ViewBag.SessionId = new SelectList(sessionListAll, "Value", "Text");
                ViewBag.PaymentMethod = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentMethod>(new List<int> { (int)PaymentMethod.Cheque }), "Key", "Value");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return View();
        }

        [HttpGet]
        public ActionResult GenerateTransactionReportEdit()
        {
            return RedirectToAction("TransactionsReportEdit");
        }

        [HttpPost]
        public ActionResult GenerateTransactionReportEdit(long organizationId, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] userIds, string datef, string datet, int paymentMethod = 0, int pageSize = 10)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<Program> programs = _programService.LoadAuthorizedProgram(_userMenu);
            IList<Session> sessions = _sessionService.LoadAuthorizedSession(_userMenu, null);
            IList<Branch> branches = _branchService.LoadAuthorizedBranch(_userMenu);
            List<long> allAuthorizedBrancheIds = branches.Select(x => x.Id).ToList();
            long[] authorizedProgramIds = programs.Select(x => x.Id).ToArray();
            long[] authorizedSessionIds = sessions.Select(x => x.Id).ToArray();

            IList<Branch> authorizedBranchesByProgramSession = _branchService.LoadBranchByProgramSessionForTransaction(programId[0], sessionId[0], authorizedProgramIds, authorizedSessionIds, allAuthorizedBrancheIds).Distinct().ToList();
            IList<Branch> branchByIds = new List<Branch>();
            IList<Campus> campusList = new List<Campus>();
            Organization org = _organizationService.LoadById(organizationId);

            if (!programId.Contains(SelectionType.SelelectAll))
            {
                programs = programs.Where(x => x.Id.In(programId)).ToList();
                ViewBag.programName = string.Join(", ", programs.Select(p => p.Name).ToList());
            }
            else
            {
                ViewBag.programName = "All Programs";
            }

            if (!sessionId.Contains(SelectionType.SelelectAll))
            {
                sessions = sessions.Where(x => x.Id.In(sessionId)).ToList();
                ViewBag.sessionName = string.Join(", ", sessions);
            }
            else
            {
                ViewBag.sessionName = "All";
            }

            if (branchId.Contains(SelectionType.SelelectAll))
            {
                branchByIds = authorizedBranchesByProgramSession;
            }
            else
            {
                branchByIds = authorizedBranchesByProgramSession.Where(br => br.Id.In(branchId)).ToList();
            }

            if (!campusId.Contains(SelectionType.SelelectAll))
            {
                campusList = _campusService.LoadCampus(null, null, null, null, campusId.ToList());
                IList<string> campusName = campusList.Select(c => c.Name).Distinct().ToList();
                ViewBag.campusName = string.Join(", ", campusName);
            }
            else
            {
                ViewBag.campusName = "All";
            }

            if (branchId.Contains(SelectionType.SelelectAll))
            {
                ViewBag.branchName = "All";
            }
            else
            {
                ViewBag.branchName = string.Join(", ", branchByIds.Select(x => x.Name).ToList());
            }
            ViewBag.OrganizationName = org.Name;
            ViewBag.programId = programId;
            ViewBag.sessionId = sessionId;
            ViewBag.branchId = branchId;
            ViewBag.campusId = campusId;
            ViewBag.UserIds = userIds;
            ViewBag.datef = datef;
            ViewBag.datet = datet;
            ViewBag.PaymentMethod = paymentMethod;
            ViewBag.PageSize = pageSize;
            return PartialView("Partial/_TransactionReportEdit");
        }

        [HttpPost]
        public JsonResult TransactionReportResultEdit(int draw, int start, int length, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] userIds, DateTime datef, DateTime datet, int paymentMethod = 0)
        {
            var data = new List<object>();
            long recordsFiltered = 0;
            int recordsTotal = 0;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime dateFrom = Convert.ToDateTime(datef);
                DateTime dateTo = Convert.ToDateTime(datet);
                dateTo = dateTo.AddDays(1).AddMilliseconds(-1);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                if (branchIdList == null)
                {
                    branchIdList = _branchService.LoadBranch().Select(x => x.Id).ToList();
                }
                long[] authorizedProgramIds = _programService.LoadAuthorizedProgram(_userMenu).Select(x => x.Id).ToArray();
                long[] authorizedSessionIds = _sessionService.LoadAuthorizedSession(_userMenu, null).Select(x => x.Id).ToArray();
                IList<Campus> campusList = _campusService.LoadAuthorizeCampus(_userMenu);//LoadAuthorizedCampusesByBranch(branchId, authorizedBranchesByProgramSession);
                var campusIdList = campusList.Select(x => x.Id).ToArray();

                IList<StudentPaymentTransections> studentPaymentList = _studentPaymentTransectionsService.GetTransactionReportVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, paymentMethod, start, length, 1);

                recordsTotal = _studentPaymentTransectionsService.GetTransactionReportCountVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, paymentMethod, 1);

                recordsFiltered = recordsTotal;

                #region datatable functionalities

                decimal totalReceived = 0;
                int sl = start + 1;
                foreach (var c in studentPaymentList)
                {
                    var str = new List<string>();
                    if (studentPaymentList != null)
                    {
                        str.Add(sl.ToString());
                        string name = c.StudentFullName == null ? c.StudentNickName : c.StudentFullName;
                        str.Add(name);
                        if (c.ReceivedAmount != null)
                        {
                            if (c.ReceivedAmount != 0)
                            {
                                str.Add(String.Format("{0:0}", c.ReceivedAmount));
                                totalReceived += Convert.ToDecimal(c.ReceivedAmount);
                            }
                            else
                            {
                                str.Add("");
                            }
                        }
                        else
                        {
                            str.Add("");
                        }
                        str.Add("<a  id='" + c.Id.ToString() + "'    href='#' data-name='" + name + " with amount " + c.ReceivedAmount + " ' class='glyphicon glyphicon-trash'></a> ");
                        //srn++;
                        sl++;
                        data.Add(str);
                    }
                }
                var summary = new List<string>();
                summary.Add("");
                summary.Add("<strong>Total Summary</strong>");
                //summary.Add("");
                summary.Add("<strong>" + String.Format("{0:0}", totalReceived) + "<strong>");
                summary.Add("");
                data.Add(summary);
                #endregion

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }


            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            string message = "";
            bool successState = false;
            try
            {
                StudentPaymentTransections paymentTransections = _studentPaymentTransectionsService.GetPaymentTransections(id);
                _studentPaymentTransectionsService.Delete(paymentTransections);
                message = "Deleted Successfully";
                successState = true;

            }
            catch (MessageExceptions.InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        #endregion

        #endregion

        #region Helper Function

        #endregion

        #region Transactions Report


        #region old code
        //public ActionResult TransactionsReport()
        //{
        //    try
        //    {
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
        //        organizations.Insert(0, new Organization() { ShortName = "Select Organization", Id = -1 }); //0 for all
        //        ViewBag.organizationList = new SelectList(organizations, "Id", "ShortName");
        //        //get session list
        //        var sessionList = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Select Session", Value = "" } }, "Value", "Text");
        //        List<SelectListItem> sessionListAll = sessionList.ToList();
        //        // sessionListAll.Insert(0, new SelectListItem() { Value = "0", Text = "All" });
        //        ViewBag.SessionId = new SelectList(sessionListAll, "Value", "Text");
        //        //get active user
        //        IList<UserProfile> users = new List<UserProfile>();
        //        users = _userService.LoadUser().Where(x => x.AspNetUser.Id != 1).ToList();
        //        var userList = new SelectList(users, "AspNetUser.Id", "AspNetUser.UserName");
        //        List<SelectListItem> userListAll = userList.ToList();
        //        userListAll.Insert(0, new SelectListItem() { Value = "0", Text = "All" });
        //        ViewBag.UserIds = new SelectList(userListAll, "Value", "Text");
        //        //get payment method
        //        //var paymentMethod = new SelectList(_commonHelper.GetPaymentMethodsForReport(), "Key", "Value");
        //        //var paymentMethodList = (from KeyValuePair<string, int> statusKeyValuePair in paymentMethod.Items select new SelectListItem() { Text = statusKeyValuePair.Key, Value = statusKeyValuePair.Value.ToString() }).ToList();
        //        //ViewBag.PaymentMethod = paymentMethodList;
        //        ViewBag.PaymentMethod = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentMethod>(new List<int> { (int)PaymentMethod.Cheque }), "Key", "Value");
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        _logger.Error(ex);
        //    }
        //    return View();
        //}

        //[HttpGet]
        //public ActionResult GenerateTransactionReport()
        //{
        //    return RedirectToAction("TransactionsReport");
        //}
        //[HttpPost]
        //public ActionResult GenerateTransactionReport(long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] UserIds, string datef, string datet, int PaymentMethod = 0)
        //{
        //    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //    IList<Program> programs = _programService.LoadAuthorizedProgram(_userMenu);
        //    IList<Session> sessions = _sessionService.LoadAuthorizedSession(_userMenu, null);
        //    IList<Branch> branches = _branchService.LoadAuthorizedBranch(_userMenu);
        //    List<long> allAuthorizedBrancheIds = branches.Select(x => x.Id).ToList();
        //    long[] authorizedProgramIds = programs.Select(x => x.Id).ToArray();
        //    long[] authorizedSessionIds = sessions.Select(x => x.Id).ToArray();

        //    IList<Branch> authorizedBranchesByProgramSession = _branchService.LoadBranchByProgramSessionForTransaction(programId[0], sessionId[0], authorizedProgramIds, authorizedSessionIds, allAuthorizedBrancheIds).Distinct().ToList();
        //    IList<Branch> branchList = new List<Branch>();
        //    IList<Branch> branchByIds = new List<Branch>();
        //    IList<Campus> campusList = new List<Campus>();
        //    IList<Course> courseList = new List<Course>();

        //    if (!programId.Contains(0))
        //    {
        //        programs = programs.Where(x => x.Id.In(programId)).ToList();
        //        ViewBag.programName = string.Join(", ", programs.Select(p => p.Name).ToList());
        //    }
        //    else
        //    {
        //        ViewBag.programName = "All Programs";
        //    }

        //    if (!sessionId.Contains(0))
        //    {
        //        sessions = sessions.Where(x => x.Id.In(sessionId)).ToList();
        //        ViewBag.sessionName = string.Join(", ", sessions);
        //    }
        //    else
        //    {
        //        ViewBag.sessionName = "All";
        //    }

        //    // campusList = _campusService.LoadAuthorizedCampusesByBranch(branchId, authorizedBranchesByProgramSession);
        //    if (branchId.Contains(0))
        //    {
        //        branchByIds = authorizedBranchesByProgramSession;
        //    }
        //    else
        //    {
        //        branchByIds = authorizedBranchesByProgramSession.Where(br => br.Id.In(branchId)).ToList();
        //    }

        //    if (!campusId.Contains(0))
        //    {
        //        // campusList = _campusService.LoadAllOk().Where(camp => camp.Id.In(campusId) && camp.Status == Campus.EntityStatus.Active).ToList();
        //        campusList = _campusService.LoadCampus(null, null, null, null, campusId.ToList());
        //        IList<string> campusName = campusList.Select(c => c.Name).Distinct().ToList();
        //        ViewBag.campusName = string.Join(", ", campusName);
        //    }
        //    else
        //    {
        //        ViewBag.campusName = "All";
        //    }

        //    if (branchId.Contains(0))
        //    {
        //        ViewBag.branchName = "All";
        //    }
        //    else
        //    {
        //        ViewBag.branchName = string.Join(", ", branchByIds.Select(x => x.Name).ToList());
        //    }

        //    //    courseList = _courseService.GetCourseByProgramAndSessionForTransaction(programId[0], sessionId[0]);
        //    //if (course.Contains(0))
        //    //{

        //    //    ViewBag.courseName = "All";
        //    //}
        //    //else
        //    //{
        //    //    courseList = _courseService.LoadCourse(course);
        //    //    ViewBag.courseName = string.Join(", ", courseList.Select(c => c.Name).ToList());
        //    //}
        //    ViewBag.programId = programId;
        //    ViewBag.sessionId = sessionId;
        //    ViewBag.branchId = branchId;
        //    ViewBag.campusId = campusId;
        //    //ViewBag.course = course;
        //    ViewBag.UserIds = UserIds;
        //    ViewBag.datef = datef;
        //    ViewBag.datet = datet;
        //    ViewBag.PaymentMethod = PaymentMethod;
        //    ViewBag.PageSize = Constants.PageSize;
        //    return View();
        //}


        //[HttpPost]
        //public JsonResult TransactionReportResult(int draw, int start, int length, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] UserIds, DateTime datef, DateTime datet, int PaymentMethod = 0)
        //{
        //    if (Request.IsAjaxRequest())
        //    {
        //        try
        //        {
        //            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //            NameValueCollection nvc = Request.Form;
        //            DateTime dateFrom = Convert.ToDateTime(datef);
        //            DateTime dateTo = Convert.ToDateTime(datet);
        //            dateTo = dateTo.AddDays(1).AddMilliseconds(-1);
        //            //bool allBranch = false;
        //            //long[] branchIdList = AuthHelper.GetUserBranchIdList(_userMenu, out allBranch);
        //            //branchIdList = branchIdList.Distinct().ToArray();

        //            List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);


        //            IList<UserProfile> users = new List<UserProfile>();
        //            users = _userService.LoadUser();
        //            if (branchIdList == null)
        //            {
        //                branchIdList = _branchService.LoadBranch().Select(x => x.Id).ToList();
        //            }
        //            IList<Campus> campusList = new List<Campus>();
        //            long[] authorizedProgramIds = _programService.LoadAuthorizedProgram(_userMenu).Select(x => x.Id).ToArray();
        //            long[] authorizedSessionIds = _sessionService.LoadAuthorizedSession(_userMenu, null).Select(x => x.Id).ToArray();
        //            IList<Branch> authorizedBranchesByProgramSession = _branchService.LoadBranchByProgramSessionForTransaction(programId[0], sessionId[0], authorizedProgramIds, authorizedSessionIds, branchIdList).Distinct().ToList();
        //            campusList = _campusService.LoadAuthorizeCampus(_userMenu);//LoadAuthorizedCampusesByBranch(branchId, authorizedBranchesByProgramSession);
        //            var campusIdList = campusList.Select(x => x.Id).ToArray();
        //            IList<StudentPaymentTransections> studentPaymentList = _studentPaymentTransectionsService.GetTransactionReportVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, PaymentMethod, start, length, 1);
        //            int recordsTotal = _studentPaymentTransectionsService.GetTransactionReportCountVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, PaymentMethod, 1);
        //            long recordsFiltered = recordsTotal;
        //            #region datatable functionalities
        //            decimal totalReceived = 0;
        //            int sl = start + 1;
        //            var data = new List<object>();
        //            foreach (var c in studentPaymentList)
        //            {
        //                var str = new List<string>();
        //                if (studentPaymentList != null)
        //                {
        //                    //sl.
        //                    //str.Add(c.Serial2.ToString());
        //                    str.Add(sl.ToString());
        //                    //date
        //                    //if (c.ReceivedDate != null)
        //                    //{
        //                    //    str.Add(String.Format("{0:dd/MM/yyyy}", c.ReceivedDate));
        //                    //}
        //                    //else
        //                    //{
        //                    //    str.Add("");
        //                    //}
        //                    string name = c.StudentFullName == null ? c.StudentNickName : c.StudentFullName;
        //                    str.Add(name);

        //                    //str.Add(c.ReceivedAmount.ToString());
        //                    if (c.ReceivedAmount != null)
        //                    {
        //                        if (c.ReceivedAmount != 0)
        //                        {
        //                            str.Add(String.Format("{0:0}", c.ReceivedAmount));
        //                            totalReceived += Convert.ToDecimal(c.ReceivedAmount);
        //                        }
        //                        else
        //                        {
        //                            str.Add("");
        //                        }
        //                    }
        //                    else
        //                    {
        //                        str.Add("");
        //                    }
        //                    sl++;
        //                    data.Add(str);
        //                }
        //            }
        //            var summary = new List<string>();
        //            summary.Add("");
        //            summary.Add("<strong>Total Summary</strong>");
        //            //summary.Add("");
        //            summary.Add("<strong>" + String.Format("{0:0}", totalReceived) + "<strong>");
        //            data.Add(summary);
        //            return Json(new
        //            {
        //                draw = draw,
        //                recordsTotal = recordsTotal,
        //                recordsFiltered = recordsFiltered,
        //                start = start,
        //                length = length,
        //                data = data
        //            });
        //            #endregion
        //        }
        //        catch (Exception ex)
        //        {
        //            var data = new List<object>();
        //            _logger.Error(ex);
        //            return Json(new
        //            {
        //                draw = draw,
        //                recordsTotal = 0,
        //                recordsFiltered = 0,
        //                start = start,
        //                length = length,
        //                data = data
        //            });
        //        }
        //    }
        //    return Json(HttpNotFound());
        //}

        //[HttpGet]
        //public ActionResult TransactionReportToExcel(long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] UserIds, string programName, string sessionName, string campusName, string branchName, string datef, string datet, int PaymentMethod)
        //{
        //    try
        //    {
        //        decimal totalReceived = 0;
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        NameValueCollection nvc = Request.Form;
        //        DateTime dateFrom = Convert.ToDateTime(datef);
        //        DateTime dateTo = Convert.ToDateTime(datet);
        //        dateTo = dateTo.AddDays(1).AddMilliseconds(-1);
        //        //bool allBranch = false;
        //        //long[] branchIdList = AuthHelper.GetUserBranchIdList(_userMenu, out allBranch);


        //        List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);

        //        //branchIdList = branchIdList.Distinct().ToArray();
        //        IList<UserProfile> users = new List<UserProfile>();
        //        users = _userService.LoadUser();
        //        if (branchIdList == null)
        //        {
        //            branchIdList = _branchService.LoadBranch().Select(x => x.Id).ToList();
        //        }
        //        IList<Campus> campusList = new List<Campus>();
        //        long[] authorizedProgramIds = _programService.LoadAuthorizedProgram(_userMenu).Select(x => x.Id).ToArray();
        //        long[] authorizedSessionIds = _sessionService.LoadAuthorizedSession(_userMenu, null).Select(x => x.Id).ToArray();
        //        IList<Branch> authorizedBranchesByProgramSession = _branchService.LoadBranchByProgramSessionForTransaction(programId[0], sessionId[0], authorizedProgramIds, authorizedSessionIds, branchIdList).Distinct().ToList();
        //        campusList = _campusService.LoadAuthorizeCampus(_userMenu);//LoadAuthorizedCampusesByBranch(branchId, authorizedBranchesByProgramSession);
        //        var campusIdList = campusList.Select(x => x.Id).ToArray();
        //        List<string> headerList = new List<string>();
        //        headerList.Add(ErpInfo.OrganizationNameFull);
        //        headerList.Add(programName + " " + sessionName);
        //        headerList.Add("Transaction Report");
        //        //headerList.Add("Session: " + sessionName);
        //        headerList.Add("Branch: " + branchName);
        //        headerList.Add("Campus: " + campusName);
        //        headerList.Add("Date: " + String.Format("{0:dd/MM/yyyy}", dateFrom) + " to " + String.Format("{0:dd/MM/yyyy}", dateTo));
        //        headerList.Add("");
        //        // headerList.Add("course: " + courseName);
        //        List<string> footerList = new List<string>();

        //        List<string> columnList = new List<string>();
        //        columnList.Add("Name");
        //        columnList.Add("Received");
        //        var transactionXlsList = new List<List<object>>();
        //        IList<StudentPaymentTransections> studentPaymentList = _studentPaymentTransectionsService.GetTransactionReportVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, PaymentMethod, 0, 0, 1);
        //        foreach (var c in studentPaymentList)
        //        {
        //            string name = "",receivedAmountValue = "";
        //            if (studentPaymentList != null)
        //            {
        //                name = c.StudentFullName == null ? c.StudentNickName : c.StudentFullName;

        //                //recieved
        //                if (c.ReceivedAmount != null)
        //                {
        //                    if (c.ReceivedAmount != 0)
        //                    {
        //                        receivedAmountValue = String.Format("{0:0}", c.ReceivedAmount);
        //                        totalReceived += Convert.ToDecimal(c.ReceivedAmount);
        //                    }

        //                }

        //            }
        //            var xlsRow = new List<object> { name, receivedAmountValue };
        //            transactionXlsList.Add(xlsRow);
        //        }
        //        //var summaryRow = new List<object> { "Total Summary", String.Format("{0:0}", totalReceived) };
        //        //transactionXlsList.Add(summaryRow);
        //        footerList.Add("Total Received Amount: " + String.Format("{0:0}", totalReceived));
        //        //footerList.Add("Total Cash Back: " + String.Format("{0:0}", totalCashBack));
        //        //footerList.Add("Total: " + String.Format("{0:0}", (totalReceived - totalCashBack)));
        //        //footerList.Add("");
        //        //footerList.Add("Developed By OnnoRokom Software Ltd.");
        //        ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "Transactions-Report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
        //        return View("AutoClose");
        //    }

        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        return View("AutoClose");
        //    }

        //}
        #endregion

        public ActionResult TransactionsReport()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                organizations.Insert(0, new Organization() { ShortName = "Select Organization", Id = -1 }); //0 for all
                ViewBag.organizationList = new SelectList(organizations, "Id", "ShortName");
                //get session list
                var sessionList = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Select Session", Value = "" } }, "Value", "Text");
                List<SelectListItem> sessionListAll = sessionList.ToList();
                ViewBag.SessionId = new SelectList(sessionListAll, "Value", "Text");
               ViewBag.PaymentMethod = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentMethod>(new List<int> { (int)PaymentMethod.Cheque }), "Key", "Value");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return View();
        }

        [HttpGet]
        public ActionResult GenerateTransactionReport()
        {
            return RedirectToAction("TransactionsReport");
        }

        [HttpPost]
        public ActionResult GenerateTransactionReport(long organizationId, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] userIds, string datef, string datet, int paymentMethod = 0, int pageSize = 10)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<Program> programs = _programService.LoadAuthorizedProgram(_userMenu);
            IList<Session> sessions = _sessionService.LoadAuthorizedSession(_userMenu, null);
            IList<Branch> branches = _branchService.LoadAuthorizedBranch(_userMenu);
            List<long> allAuthorizedBrancheIds = branches.Select(x => x.Id).ToList();
            long[] authorizedProgramIds = programs.Select(x => x.Id).ToArray();
            long[] authorizedSessionIds = sessions.Select(x => x.Id).ToArray();

            IList<Branch> authorizedBranchesByProgramSession = _branchService.LoadBranchByProgramSessionForTransaction(programId[0], sessionId[0], authorizedProgramIds, authorizedSessionIds, allAuthorizedBrancheIds).Distinct().ToList();
            IList<Branch> branchByIds = new List<Branch>();
            IList<Campus> campusList = new List<Campus>();
            Organization org = _organizationService.LoadById(organizationId);

            if (!programId.Contains(SelectionType.SelelectAll))
            {
                programs = programs.Where(x => x.Id.In(programId)).ToList();
                ViewBag.programName = string.Join(", ", programs.Select(p => p.Name).ToList());
            }
            else
            {
                ViewBag.programName = "All Programs";
            }

            if (!sessionId.Contains(SelectionType.SelelectAll))
            {
                sessions = sessions.Where(x => x.Id.In(sessionId)).ToList();
                ViewBag.sessionName = string.Join(", ", sessions);
            }
            else
            {
                ViewBag.sessionName = "All";
            }

            if (branchId.Contains(SelectionType.SelelectAll))
            {
                branchByIds = authorizedBranchesByProgramSession;
            }
            else
            {
                branchByIds = authorizedBranchesByProgramSession.Where(br => br.Id.In(branchId)).ToList();
            }

            if (!campusId.Contains(SelectionType.SelelectAll))
            {
                campusList = _campusService.LoadCampus(null, null, null, null, campusId.ToList());
                IList<string> campusName = campusList.Select(c => c.Name).Distinct().ToList();
                ViewBag.campusName = string.Join(", ", campusName);
            }
            else
            {
                ViewBag.campusName = "All";
            }

            if (branchId.Contains(SelectionType.SelelectAll))
            {
                ViewBag.branchName = "All";
            }
            else
            {
                ViewBag.branchName = string.Join(", ", branchByIds.Select(x => x.Name).ToList());
            }
            ViewBag.OrganizationName = org.Name;
            ViewBag.programId = programId;
            ViewBag.sessionId = sessionId;
            ViewBag.branchId = branchId;
            ViewBag.campusId = campusId;
            ViewBag.UserIds = userIds;
            ViewBag.datef = datef;
            ViewBag.datet = datet;
            ViewBag.PaymentMethod = paymentMethod;
            ViewBag.PageSize = pageSize;
            return PartialView("Partial/_TransactionReport");
        }

        [HttpPost]
        public JsonResult TransactionReportResult(int draw, int start, int length, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] userIds, DateTime datef, DateTime datet, int paymentMethod = 0)
        {
            var data = new List<object>();
            long recordsFiltered = 0;
            int recordsTotal = 0;
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    DateTime dateFrom = Convert.ToDateTime(datef);
                    DateTime dateTo = Convert.ToDateTime(datet);
                    dateTo = dateTo.AddDays(1).AddMilliseconds(-1);
                    List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                    if (branchIdList == null)
                    {
                        branchIdList = _branchService.LoadBranch().Select(x => x.Id).ToList();
                    }
                    long[] authorizedProgramIds = _programService.LoadAuthorizedProgram(_userMenu).Select(x => x.Id).ToArray();
                    long[] authorizedSessionIds = _sessionService.LoadAuthorizedSession(_userMenu, null).Select(x => x.Id).ToArray();
                    IList<Campus> campusList = _campusService.LoadAuthorizeCampus(_userMenu);//LoadAuthorizedCampusesByBranch(branchId, authorizedBranchesByProgramSession);
                    var campusIdList = campusList.Select(x => x.Id).ToArray();
                    
                    IList<StudentPaymentTransections> studentPaymentList = _studentPaymentTransectionsService.GetTransactionReportVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, paymentMethod, start, length, 1);

                    recordsTotal = _studentPaymentTransectionsService.GetTransactionReportCountVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, paymentMethod, 1);

                    recordsFiltered = recordsTotal;
                    
                    #region datatable functionalities
                    
                    decimal totalReceived = 0;
                    int sl = start + 1;
                    foreach (var c in studentPaymentList)
                    {
                        var str = new List<string>();
                        if (studentPaymentList != null)
                        {
                            //sl.
                            //str.Add(c.Serial2.ToString());
                            str.Add(sl.ToString());
                            //date
                            //if (c.ReceivedDate != null)
                            //{
                            //    str.Add(String.Format("{0:dd/MM/yyyy}", c.ReceivedDate));
                            //}
                            //else
                            //{
                            //    str.Add("");
                            //}
                            string name = c.StudentFullName == null ? c.StudentNickName : c.StudentFullName;
                            str.Add(name);

                            //str.Add(c.ReceivedAmount.ToString());
                            if (c.ReceivedAmount != null)
                            {
                                if (c.ReceivedAmount != 0)
                                {
                                    str.Add(String.Format("{0:0}", c.ReceivedAmount));
                                    totalReceived += Convert.ToDecimal(c.ReceivedAmount);
                                }
                                else
                                {
                                    str.Add("");
                                }
                            }
                            else
                            {
                                str.Add("");
                            }
                            sl++;
                            data.Add(str);
                        }
                    }
                    var summary = new List<string>();
                    summary.Add("");
                    summary.Add("<strong>Total Summary</strong>");
                    //summary.Add("");
                    summary.Add("<strong>" + String.Format("{0:0}", totalReceived) + "<strong>");
                    data.Add(summary);
                    #endregion

                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }


                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });
        }

        [HttpGet]
        public ActionResult TransactionReportToExcel(long organizationId, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] userIds, string programName, string sessionName, string campusName, string branchName, string datef, string datet, int paymentMethod = 0)
        {
            try
            {
                decimal totalReceived = 0;
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime dateFrom = Convert.ToDateTime(datef);
                DateTime dateTo = Convert.ToDateTime(datet);
                dateTo = dateTo.AddDays(1).AddMilliseconds(-1);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                Organization organization = _organizationService.LoadById(organizationId);

                IList<UserProfile> users = new List<UserProfile>();
                if (branchIdList == null)
                {
                    branchIdList = _branchService.LoadBranch().Select(x => x.Id).ToList();
                }
                IList<Campus> campusList = new List<Campus>();
                long[] authorizedProgramIds = _programService.LoadAuthorizedProgram(_userMenu).Select(x => x.Id).ToArray();
                long[] authorizedSessionIds = _sessionService.LoadAuthorizedSession(_userMenu, null).Select(x => x.Id).ToArray();

                campusList = _campusService.LoadAuthorizeCampus(_userMenu);//LoadAuthorizedCampusesByBranch(branchId, authorizedBranchesByProgramSession);
                var campusIdList = campusList.Select(x => x.Id).ToArray();
                List<string> headerList = new List<string>();

                headerList.Add(organization.Name);
                headerList.Add(programName + " " + sessionName);
                headerList.Add("Transaction Report");
                //headerList.Add("Session: " + sessionName);
                headerList.Add("Branch: " + branchName);
                headerList.Add("Campus: " + campusName);
                headerList.Add("Date: " + String.Format("{0:dd/MM/yyyy}", dateFrom) + " to " + String.Format("{0:dd/MM/yyyy}", dateTo));
                headerList.Add("");
                // headerList.Add("course: " + courseName);
                List<string> footerList = new List<string>();

                List<string> columnList = new List<string>();
                columnList.Add("Name");
                columnList.Add("Received");
                var transactionXlsList = new List<List<object>>();
                IList<StudentPaymentTransections> studentPaymentList = _studentPaymentTransectionsService.GetTransactionReportVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, paymentMethod, 0, 0, 1);
                foreach (var c in studentPaymentList)
                {
                    string name = "", receivedAmountValue = "";
                    if (studentPaymentList != null)
                    {
                        name = c.StudentFullName == null ? c.StudentNickName : c.StudentFullName;

                        //recieved
                        if (c.ReceivedAmount != null)
                        {
                            if (c.ReceivedAmount != 0)
                            {
                                receivedAmountValue = String.Format("{0:0}", c.ReceivedAmount);
                                totalReceived += Convert.ToDecimal(c.ReceivedAmount);
                            }

                        }

                    }
                    var xlsRow = new List<object> { name, receivedAmountValue };
                    transactionXlsList.Add(xlsRow);
                }
                //var summaryRow = new List<object> { "Total Summary", String.Format("{0:0}", totalReceived) };
                //transactionXlsList.Add(summaryRow);
                footerList.Add("Total Received Amount: " + String.Format("{0:0}", totalReceived));
                //footerList.Add("Total Cash Back: " + String.Format("{0:0}", totalCashBack));
                //footerList.Add("Total: " + String.Format("{0:0}", (totalReceived - totalCashBack)));
                //footerList.Add("");
                //footerList.Add("Developed By OnnoRokom Software Ltd.");
                ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "Transactions-Report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }

        }

        #endregion

        #region Student Report

        #region old code
        //public ActionResult StudentReport()
        //{
        //    try
        //    {
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
        //        organizations.Insert(0, new Organization() { ShortName = "Select Organization", Id = -1 });
        //        ViewBag.organizationList = new SelectList(organizations, "Id", "ShortName");
        //        //get session list
        //        var sessionList = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Select Session", Value = "" } }, "Value", "Text");
        //        List<SelectListItem> sessionListAll = sessionList.ToList();
        //        // sessionListAll.Insert(0, new SelectListItem() { Value = "0", Text = "All" });
        //        ViewBag.SessionId = new SelectList(sessionListAll, "Value", "Text");
        //        //get active user
        //        IList<UserProfile> users = new List<UserProfile>();
        //        users = _userService.LoadUser().Where(x => x.AspNetUser.Id != 1).ToList();
        //        var userList = new SelectList(users, "AspNetUser.Id", "AspNetUser.UserName");
        //        List<SelectListItem> userListAll = userList.ToList();
        //        userListAll.Insert(0, new SelectListItem() { Value = "0", Text = "All" });
        //        ViewBag.UserIds = new SelectList(userListAll, "Value", "Text");
        //        //get payment method
        //        //var paymentMethod = new SelectList(_commonHelper.GetPaymentMethodsForReport(), "Key", "Value");
        //        //var paymentMethodList = (from KeyValuePair<string, int> statusKeyValuePair in paymentMethod.Items select new SelectListItem() { Text = statusKeyValuePair.Key, Value = statusKeyValuePair.Value.ToString() }).ToList();
        //        //ViewBag.PaymentMethod = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentMethod>(new List<int> { (int)PaymentMethod.Cheque }), "Key", "Value"); 
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
        //        _logger.Error(ex);
        //    }
        //    return View();
        //}

        //[HttpGet]
        //public ActionResult GenerateStudentReport()
        //{
        //    return RedirectToAction("StudentReport");
        //}

        //[HttpPost]
        //public ActionResult GenerateStudentReport(long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] UserIds, string datef, string datet, int PaymentMethod = 0)
        //{
        //    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //    IList<Program> programs = _programService.LoadAuthorizedProgram(_userMenu);
        //    IList<Session> sessions = _sessionService.LoadAuthorizedSession(_userMenu, null);
        //    IList<Branch> branches = _branchService.LoadAuthorizedBranch(_userMenu);
        //    List<long> allAuthorizedBrancheIds = branches.Select(x => x.Id).ToList();
        //    long[] authorizedProgramIds = programs.Select(x => x.Id).ToArray();
        //    long[] authorizedSessionIds = sessions.Select(x => x.Id).ToArray();

        //    IList<Branch> authorizedBranchesByProgramSession = _branchService.LoadBranchByProgramSessionForTransaction(programId[0], sessionId[0], authorizedProgramIds, authorizedSessionIds, allAuthorizedBrancheIds).Distinct().ToList();
        //    IList<Branch> branchList = new List<Branch>();
        //    IList<Branch> branchByIds = new List<Branch>();
        //    IList<Campus> campusList = new List<Campus>();
        //    IList<Course> courseList = new List<Course>();

        //    if (!programId.Contains(0))
        //    {
        //        programs = programs.Where(x => x.Id.In(programId)).ToList();
        //        ViewBag.programName = string.Join(", ", programs.Select(p => p.Name).ToList());
        //    }
        //    else
        //    {
        //        ViewBag.programName = "All Programs";
        //    }

        //    if (!sessionId.Contains(0))
        //    {
        //        sessions = sessions.Where(x => x.Id.In(sessionId)).ToList();
        //        ViewBag.sessionName = string.Join(", ", sessions);
        //    }
        //    else
        //    {
        //        ViewBag.sessionName = "All";
        //    }

        //    // campusList = _campusService.LoadAuthorizedCampusesByBranch(branchId, authorizedBranchesByProgramSession);
        //    if (branchId.Contains(0))
        //    {
        //        branchByIds = authorizedBranchesByProgramSession;
        //    }
        //    else
        //    {
        //        branchByIds = authorizedBranchesByProgramSession.Where(br => br.Id.In(branchId)).ToList();
        //    }

        //    if (!campusId.Contains(0))
        //    {
        //        // campusList = _campusService.LoadAllOk().Where(camp => camp.Id.In(campusId) && camp.Status == Campus.EntityStatus.Active).ToList();
        //        campusList = _campusService.LoadCampus(null, null, null, null, campusId.ToList());
        //        IList<string> campusName = campusList.Select(c => c.Name).Distinct().ToList();
        //        ViewBag.campusName = string.Join(", ", campusName);
        //    }
        //    else
        //    {
        //        ViewBag.campusName = "All";
        //    }

        //    if (branchId.Contains(0))
        //    {
        //        ViewBag.branchName = "All";
        //    }
        //    else
        //    {
        //        ViewBag.branchName = string.Join(", ", branchByIds.Select(x => x.Name).ToList());
        //    }

        //    //    courseList = _courseService.GetCourseByProgramAndSessionForTransaction(programId[0], sessionId[0]);
        //    //if (course.Contains(0))
        //    //{

        //    //    ViewBag.courseName = "All";
        //    //}
        //    //else
        //    //{
        //    //    courseList = _courseService.LoadCourse(course);
        //    //    ViewBag.courseName = string.Join(", ", courseList.Select(c => c.Name).ToList());
        //    //}
        //    ViewBag.programId = programId;
        //    ViewBag.sessionId = sessionId;
        //    ViewBag.branchId = branchId;
        //    ViewBag.campusId = campusId;
        //    //ViewBag.course = course;
        //    ViewBag.UserIds = UserIds;
        //    ViewBag.datef = datef;
        //    ViewBag.datet = datet;
        //    ViewBag.PaymentMethod = PaymentMethod;
        //    ViewBag.PageSize = Constants.PageSize;
        //    return View();
        //}

        //[HttpPost]
        //public JsonResult StudentReportResult(int draw, int start, int length, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] UserIds, DateTime datef, DateTime datet, int PaymentMethod = 0)
        //{
        //    if (Request.IsAjaxRequest())
        //    {
        //        try
        //        {
        //            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //            NameValueCollection nvc = Request.Form;
        //            DateTime dateFrom = Convert.ToDateTime(datef);
        //            DateTime dateTo = Convert.ToDateTime(datet);
        //            dateTo = dateTo.AddDays(1).AddMilliseconds(-1);
        //            //bool allBranch = false;
        //            //long[] branchIdList = AuthHelper.GetUserBranchIdList(_userMenu, out allBranch);
        //            //branchIdList = branchIdList.Distinct().ToArray();

        //            List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);


        //            IList<UserProfile> users = new List<UserProfile>();
        //            users = _userService.LoadUser();
        //            if (branchIdList == null)
        //            {
        //                branchIdList = _branchService.LoadBranch().Select(x => x.Id).ToList();
        //            }
        //            IList<Campus> campusList = new List<Campus>();
        //            long[] authorizedProgramIds = _programService.LoadAuthorizedProgram(_userMenu).Select(x => x.Id).ToArray();
        //            long[] authorizedSessionIds = _sessionService.LoadAuthorizedSession(_userMenu, null).Select(x => x.Id).ToArray();
        //            IList<Branch> authorizedBranchesByProgramSession = _branchService.LoadBranchByProgramSessionForTransaction(programId[0], sessionId[0], authorizedProgramIds, authorizedSessionIds, branchIdList).Distinct().ToList();
        //            campusList = _campusService.LoadAuthorizeCampus(_userMenu);//LoadAuthorizedCampusesByBranch(branchId, authorizedBranchesByProgramSession);
        //            var campusIdList = campusList.Select(x => x.Id).ToArray();
        //            IList<StudentPaymentTransections> studentPaymentList = _studentPaymentTransectionsService.GetTransactionReportVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, PaymentMethod, start, length, 3);
        //            int recordsTotal = _studentPaymentTransectionsService.GetTransactionReportCountVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, PaymentMethod, 3);
        //            long recordsFiltered = recordsTotal;
        //            #region datatable functionalities
        //            //decimal totalReceived = 0;
        //            int sl = start + 1;
        //            var data = new List<object>();
        //            foreach (var c in studentPaymentList)
        //            {
        //                var str = new List<string>();
        //                if (studentPaymentList != null)
        //                {
        //                    str.Add(sl.ToString());
        //                    string FullName = c.StudentFullName == null ? c.StudentNickName : c.StudentFullName;
        //                    str.Add(FullName);
        //                    sl++;
        //                    data.Add(str);
        //                }
        //            }
        //            return Json(new
        //            {
        //                draw = draw,
        //                recordsTotal = recordsTotal,
        //                recordsFiltered = recordsFiltered,
        //                start = start,
        //                length = length,
        //                data = data
        //            });
        //            #endregion
        //        }
        //        catch (Exception ex)
        //        {
        //            var data = new List<object>();
        //            _logger.Error(ex);
        //            return Json(new
        //            {
        //                draw = draw,
        //                recordsTotal = 0,
        //                recordsFiltered = 0,
        //                start = start,
        //                length = length,
        //                data = data
        //            });
        //        }
        //    }
        //    return Json(HttpNotFound());
        //}
        #endregion 

        public ActionResult StudentReport()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                organizations.Insert(0, new Organization() { ShortName = "Select Organization", Id = -1 });
                ViewBag.organizationList = new SelectList(organizations, "Id", "ShortName");
                //get session list
                var sessionList = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Select Session", Value = "" } }, "Value", "Text");
                List<SelectListItem> sessionListAll = sessionList.ToList();
                ViewBag.SessionId = new SelectList(sessionListAll, "Value", "Text");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult GenerateStudentReport(long organizationId, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] userIds, string datef, string datet, int paymentMethod = 0, int pageSize = 10)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            Organization organization = _organizationService.LoadById(organizationId);
            IList<Program> programs = _programService.LoadAuthorizedProgram(_userMenu);
            IList<Session> sessions = _sessionService.LoadAuthorizedSession(_userMenu);
            IList<Branch> branches = _branchService.LoadAuthorizedBranch(_userMenu);
            List<long> allAuthorizedBrancheIds = branches.Select(x => x.Id).ToList();
            long[] authorizedProgramIds = programs.Select(x => x.Id).ToArray();
            long[] authorizedSessionIds = sessions.Select(x => x.Id).ToArray();

            IList<Branch> authorizedBranchesByProgramSession = _branchService.LoadBranchByProgramSessionForTransaction(programId[0], sessionId[0], authorizedProgramIds, authorizedSessionIds, allAuthorizedBrancheIds).Distinct().ToList();

            IList<Branch> branchByIds = new List<Branch>();
            IList<Campus> campusList = new List<Campus>();

            ViewBag.OrganizationName = organization.Name;

            if (!programId.Contains(SelectionType.SelelectAll))
            {
                programs = programs.Where(x => x.Id.In(programId)).ToList();
                ViewBag.programName = string.Join(", ", programs.Select(p => p.Name).ToList());
            }
            else
            {
                ViewBag.programName = "All Programs";
            }

            if (!sessionId.Contains(SelectionType.SelelectAll))
            {
                sessions = sessions.Where(x => x.Id.In(sessionId)).ToList();
                ViewBag.sessionName = string.Join(", ", sessions);
            }
            else
            {
                ViewBag.sessionName = "All";
            }

            if (branchId.Contains(SelectionType.SelelectAll))
            {
                branchByIds = authorizedBranchesByProgramSession;
            }
            else
            {
                branchByIds = authorizedBranchesByProgramSession.Where(br => br.Id.In(branchId)).ToList();
            }

            if (!campusId.Contains(SelectionType.SelelectAll))
            {
                campusList = _campusService.LoadCampus(null, null, null, null, campusId.ToList());
                IList<string> campusName = campusList.Select(c => c.Name).Distinct().ToList();
                ViewBag.campusName = string.Join(", ", campusName);
            }
            else
            {
                ViewBag.campusName = "All";
            }

            if (branchId.Contains(SelectionType.SelelectAll))
            {
                ViewBag.branchName = "All";
            }
            else
            {
                ViewBag.branchName = string.Join(", ", branchByIds.Select(x => x.Name).ToList());
            }

            ViewBag.programId = programId;
            ViewBag.sessionId = sessionId;
            ViewBag.branchId = branchId;
            ViewBag.campusId = campusId;
            ViewBag.UserIds = userIds;
            ViewBag.datef = datef;
            ViewBag.datet = datet;
            ViewBag.PaymentMethod = paymentMethod;
            ViewBag.PageSize = pageSize;//Constants.PageSize;
            return PartialView("Partial/_StudentReport");
            //return View();
        }

        [HttpPost]
        public JsonResult StudentReportResult(int draw, int start, int length, long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] userIds, DateTime datef, DateTime datet, int paymentMethod = 0)
        {
                var data = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    DateTime dateFrom = Convert.ToDateTime(datef);
                    DateTime dateTo = Convert.ToDateTime(datet);
                    dateTo = dateTo.AddDays(1).AddMilliseconds(-1);
                    List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu) ?? _branchService.LoadBranch().Select(x => x.Id).ToList();
                    long[] authorizedProgramIds = _programService.LoadAuthorizedProgram(_userMenu).Select(x => x.Id).ToArray();
                    long[] authorizedSessionIds = _sessionService.LoadAuthorizedSession(_userMenu, null).Select(x => x.Id).ToArray();

                    IList<Campus> campusList = _campusService.LoadAuthorizeCampus(_userMenu);//LoadAuthorizedCampusesByBranch(branchId, authorizedBranchesByProgramSession);
                    var campusIdList = campusList.Select(x => x.Id).ToArray();
                    IList<StudentPaymentTransections> studentPaymentList = _studentPaymentTransectionsService.GetTransactionReportVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, paymentMethod, start, length, 3);

                    recordsTotal = _studentPaymentTransectionsService.GetTransactionReportCountVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, paymentMethod, 3);
                    recordsFiltered = recordsTotal;
                    #region datatable functionalities
                    int sl = start + 1;
                    foreach (var c in studentPaymentList)
                    {
                        var str = new List<string>();
                        if (studentPaymentList != null)
                        {
                            str.Add(sl.ToString());
                            string FullName = c.StudentFullName == null ? c.StudentNickName : c.StudentFullName;
                            str.Add(FullName);
                            sl++;
                            data.Add(str);
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }

            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        [HttpGet]
        public ActionResult StudentReportToExcel(long organizationId , long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] userIds, string programName, string sessionName, string campusName, string branchName, string datef, string datet, int paymentMethod)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime dateFrom = Convert.ToDateTime(datef);
                DateTime dateTo = Convert.ToDateTime(datet);
                dateTo = dateTo.AddDays(1).AddMilliseconds(-1);
                List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
                if (branchIdList == null)
                {
                    branchIdList = _branchService.LoadBranch().Select(x => x.Id).ToList();
                }
                Organization organization = _organizationService.LoadById(organizationId);
                long[] authorizedProgramIds = _programService.LoadAuthorizedProgram(_userMenu).Select(x => x.Id).ToArray();
                long[] authorizedSessionIds = _sessionService.LoadAuthorizedSession(_userMenu, null).Select(x => x.Id).ToArray();
                IList<Campus> campusList  = _campusService.LoadAuthorizeCampus(_userMenu);
                var campusIdList = campusList.Select(x => x.Id).ToArray();
                List<string> headerList = new List<string>();
                headerList.Add(organization.Name);
                headerList.Add(programName + " " + sessionName);
                headerList.Add("Student List Report");
                headerList.Add("Branch: " + branchName);
                headerList.Add("Campus: " + campusName);
                headerList.Add("Date: " + String.Format("{0:dd/MM/yyyy}", dateFrom) + " to " + String.Format("{0:dd/MM/yyyy}", dateTo));
                headerList.Add("");

                List<string> footerList = new List<string>();
                List<string> columnList = new List<string>();
                columnList.Add("Name");
                var transactionXlsList = new List<List<object>>();
                IList<StudentPaymentTransections> studentPaymentList = _studentPaymentTransectionsService.GetTransactionReportVat(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, branchIdList, campusIdList, dateFrom, dateTo, paymentMethod, 0, 0, 3);
                foreach (var c in studentPaymentList)
                {
                    string name = "",
                              receivedAmountValue = "";
                    if (studentPaymentList != null)
                    {
                        name = c.StudentFullName ?? c.StudentNickName;
                    }
                    var xlsRow = new List<object> { name, receivedAmountValue };
                    transactionXlsList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "Student-List-Report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }

        }

        #endregion

        #region Transactions Settlement
        public ActionResult TransactionsSettlement()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                organizations.Insert(0, new Organization() { ShortName = "Select Organization", Id = -1 }); //0 for all
                ViewBag.organizationList = new SelectList(organizations, "Id", "ShortName");
                //get session list
                var sessionList = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Select Session", Value = "" } }, "Value", "Text");
                List<SelectListItem> sessionListAll = sessionList.ToList();
                // sessionListAll.Insert(0, new SelectListItem() { Value = "0", Text = "All" });
                ViewBag.SessionId = new SelectList(sessionListAll, "Value", "Text");
                //get active user
                IList<UserProfile> users = new List<UserProfile>();
                users = _userService.LoadUser().Where(x => x.AspNetUser.Id != 1).ToList();
                var userList = new SelectList(users, "AspNetUser.Id", "AspNetUser.UserName");
                List<SelectListItem> userListAll = userList.ToList();
                userListAll.Insert(0, new SelectListItem() { Value = "0", Text = "All" });
                ViewBag.UserIds = new SelectList(userListAll, "Value", "Text");
                //get payment method
                //var paymentMethod = new SelectList(_commonHelper.GetPaymentMethodsForReport(), "Key", "Value");
                //var paymentMethodList = (from KeyValuePair<string, int> statusKeyValuePair in paymentMethod.Items select new SelectListItem() { Text = statusKeyValuePair.Key, Value = statusKeyValuePair.Value.ToString() }).ToList();
                //ViewBag.PaymentMethod = paymentMethodList;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult TransactionsSettlement(long[] programId, long[] sessionId, long[] branchId, long[] campusId, string datef, string datet, double Persent = 100.0)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<Program> programs = _programService.LoadAuthorizedProgram(_userMenu);
            IList<Session> sessions = _sessionService.LoadAuthorizedSession(_userMenu, null);
            IList<Branch> branches = _branchService.LoadAuthorizedBranch(_userMenu);
            List<long> allAuthorizedBrancheIds = branches.Select(x => x.Id).ToList();
            long[] authorizedProgramIds = programs.Select(x => x.Id).ToArray();
            long[] authorizedSessionIds = sessions.Select(x => x.Id).ToArray();

            IList<Branch> authorizedBranchesByProgramSession = _branchService.LoadBranchByProgramSessionForTransaction(programId[0], sessionId[0], authorizedProgramIds, authorizedSessionIds, allAuthorizedBrancheIds).Distinct().ToList();
            IList<Branch> branchList = new List<Branch>();
            IList<Branch> branchByIds = new List<Branch>();
            IList<Campus> campusList = new List<Campus>();
            IList<Course> courseList = new List<Course>();

            if (!programId.Contains(0))
            {
                programs = programs.Where(x => x.Id.In(programId)).ToList();
                ViewBag.programName = string.Join(", ", programs.Select(p => p.Name).ToList());
            }
            else
            {
                ViewBag.programName = "All Programs";
            }

            if (!sessionId.Contains(0))
            {
                sessions = sessions.Where(x => x.Id.In(sessionId)).ToList();
                ViewBag.sessionName = string.Join(", ", sessions);
            }
            else
            {
                ViewBag.sessionName = "All";
            }

            // campusList = _campusService.LoadAuthorizedCampusesByBranch(branchId, authorizedBranchesByProgramSession);
            if (branchId.Contains(0))
            {
                branchByIds = authorizedBranchesByProgramSession;
            }
            else
            {
                branchByIds = authorizedBranchesByProgramSession.Where(br => br.Id.In(branchId)).ToList();
            }

            if (!campusId.Contains(0))
            {
                // campusList = _campusService.LoadAllOk().Where(camp => camp.Id.In(campusId) && camp.Status == Campus.EntityStatus.Active).ToList();
                campusList = _campusService.LoadCampus(null, null, null, null, campusId.ToList());
                IList<string> campusName = campusList.Select(c => c.Name).Distinct().ToList();
                ViewBag.campusName = string.Join(", ", campusName);
            }
            else
            {
                ViewBag.campusName = "All";
            }

            if (branchId.Contains(0))
            {
                ViewBag.branchName = "All";
            }
            else
            {
                ViewBag.branchName = string.Join(", ", branchByIds.Select(x => x.Name).ToList());
            }

            //    courseList = _courseService.GetCourseByProgramAndSessionForTransaction(programId[0], sessionId[0]);
            //if (course.Contains(0))
            //{

            //    ViewBag.courseName = "All";
            //}
            //else
            //{
            //    courseList = _courseService.LoadCourse(course);
            //    ViewBag.courseName = string.Join(", ", courseList.Select(c => c.Name).ToList());
            //}
            ViewBag.programId = programId;
            ViewBag.sessionId = sessionId;
            ViewBag.branchId = branchId;
            ViewBag.campusId = campusId;
            ViewBag.datef = datef;
            ViewBag.datet = datet;
            ViewBag.PageSize = Constants.PageSize;
            return View();
        }


        [HttpPost]
        public JsonResult GetTransactionsSettlementStartDate(long programId, long sessionId, long branchId, long campusId)
        {
            if (Request.IsAjaxRequest())
            {
                DateTime StartDate = DateTime.Now;
                try
                {
                    StartDate = _studentPaymentTransectionsService.GetTransactionsSettlementStartDate(sessionId, programId, branchId, campusId);

                    if (StartDate == null || StartDate < new DateTime(2015, 1, 1))
                    {
                        StartDate = DateTime.Now;
                    }
                    else
                    {
                        return Json(new { startDate = StartDate.ToString("yyyy-MM-dd"), IsSuccess = true });
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
                return Json(new { startDate = StartDate.ToString("yyyy-MM-dd"), IsSuccess = false });
            }
            return Json(HttpNotFound());
        }

        [HttpPost]
        public JsonResult GetTransactionsSettlementSummary(long programId, long sessionId, long branchId, long campusId, DateTime datef, DateTime datet)
        {
            if (Request.IsAjaxRequest())
            {
                //DateTime StartDate = DateTime.Now;
                try
                {
                    DateTime dateFrom = Convert.ToDateTime(datef);
                    DateTime dateTo = Convert.ToDateTime(datet);
                    dateTo = dateTo.AddDays(1).AddMilliseconds(-1);

                    decimal totalCollection = _studentPaymentService.GetTotalCollectionAmount(sessionId, programId, branchId, campusId, dateFrom, dateTo);
                    decimal totalRefund = _studentPaymentService.GetTotalRefundAmount(sessionId, programId, branchId, campusId, dateFrom, dateTo);
                    decimal netCollection = totalCollection - totalRefund;
                    decimal preSettledAmt = _studentPaymentTransectionsService.GetPreSettledAmount(sessionId, programId, branchId, campusId, new DateTime(dateFrom.Year, dateFrom.Month, 1), dateFrom.AddDays(1).AddMilliseconds(-1));
                    decimal vatStudentCollection = _studentPaymentTransectionsService.GetTotalCollectionAmountByReferenceId(sessionId, programId, branchId, campusId, dateFrom, dateTo, 39);
                    decimal taxStudentCollection = _studentPaymentTransectionsService.GetTotalCollectionAmountByReferenceId(sessionId, programId, branchId, campusId, dateFrom, dateTo, 40);
                    decimal bkashStudentCollection = _studentPaymentTransectionsService.GetTotalCollectionAmountByPaymentMethod(sessionId, programId, branchId, campusId, dateFrom, dateTo, (int)PaymentMethod.BKash);
                    decimal minVatAmount = vatStudentCollection + taxStudentCollection + bkashStudentCollection;
                    decimal minVatPercent = 0;
                    try { 
                        minVatPercent = (minVatAmount / netCollection) * 100; 
                    }
                    catch (Exception ex)
                    {
                        //may be divide by zero error can occar
                    }

                    return Json(new { IsSuccess = true, preSettledAmt = preSettledAmt, totalCollection = totalCollection, totalRefund = totalRefund, netCollection = netCollection, taxStudentCollection = taxStudentCollection, vatStudentCollection = vatStudentCollection, minVatPercent = minVatPercent, minVatAmount = minVatAmount });

                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
            return Json(HttpNotFound());
        }

        [HttpPost]
        public JsonResult GetTransactionsSettlementCalculate(long programId, long sessionId, long branchId, long campusId, DateTime datef, DateTime datet, decimal vatPercent)
        {
            if (Request.IsAjaxRequest())
            {
                //DateTime StartDate = DateTime.Now;
                try
                {
                    DateTime dateFrom = Convert.ToDateTime(datef);
                    DateTime dateTo = Convert.ToDateTime(datet);
                    dateTo = dateTo.AddDays(1).AddMilliseconds(-1);

                    decimal totalCollection = _studentPaymentService.GetTotalCollectionAmount(sessionId, programId, branchId, campusId, dateFrom, dateTo);
                    decimal totalRefund = _studentPaymentService.GetTotalRefundAmount(sessionId, programId, branchId, campusId, dateFrom, dateTo);
                    decimal netCollection = totalCollection - totalRefund;
                    decimal vatStudentCollection = _studentPaymentTransectionsService.GetTotalCollectionAmountByReferenceId(sessionId, programId, branchId, campusId, dateFrom, dateTo, 39);
                    decimal taxStudentCollection = _studentPaymentTransectionsService.GetTotalCollectionAmountByReferenceId(sessionId, programId, branchId, campusId, dateFrom, dateTo, 40);
                    decimal bkashStudentCollection = _studentPaymentTransectionsService.GetTotalCollectionAmountByPaymentMethod(sessionId, programId, branchId, campusId, dateFrom, dateTo, (int)PaymentMethod.BKash);
                    decimal minVatAmount = vatStudentCollection + taxStudentCollection + bkashStudentCollection;
                    decimal minVatPercent = (minVatAmount / netCollection) * 100;

                    decimal percent = vatPercent < minVatPercent ? minVatPercent : vatPercent;
                    decimal totalAmount = (netCollection * percent) / 100;

                    IList<StudentPaymentTransections> studentPaymentTransections = _studentPaymentTransectionsService.LoadTransactionsSettlementCalculate(sessionId, programId, branchId, campusId, dateFrom, dateTo, totalAmount);
                    string sessionName = programId.ToString() + "_" + sessionId.ToString() + "_" + branchId.ToString() + "_" + campusId.ToString() + "_" + dateFrom.ToString("yyyyMMdd") + "_" + dateTo.ToString("yyyyMMdd") + "_" + User.Identity.Name;

                    Session[sessionName] = studentPaymentTransections;

                    decimal calculatedVatAmount = 0;
                    decimal calculatedVatPercent = 0;

                    int totalTransection = 0;
                    int oldStudent = 0;
                    decimal duePayment = 0;
                    int newStudent = 0;
                    decimal newPayment = 0;
                    decimal maxReceivingAmount = 0;

                    if (studentPaymentTransections != null && studentPaymentTransections.Count > 0)
                    {
                        calculatedVatAmount = studentPaymentTransections.Max(t => t.BAmount);
                        calculatedVatPercent = (calculatedVatAmount / netCollection) * 100;

                        totalTransection = studentPaymentTransections.Count();
                        oldStudent = studentPaymentTransections.Where(t => t.Priority == 3).Count();
                        duePayment = studentPaymentTransections.Where(t => t.Priority == 3).Sum(a => a.ReceivedAmount);
                        newStudent = studentPaymentTransections.Where(t => t.Priority == 5).Count();
                        newPayment = studentPaymentTransections.Where(t => t.Priority == 5).Sum(a => a.ReceivedAmount);
                        maxReceivingAmount = studentPaymentTransections.Max(t => t.ReceivedAmount);
                    }
                    //StartDate = _studentPaymentTransectionsService.GetTransactionsSettlementStartDate(sessionId, programId, branchId, campusId, authorizedProgramIds, authorizedSessionIds, authorizedBranchIds, authorizedCampusIds);


                    return Json(new
                    {
                        IsSuccess = true,
                        calculatedVatPercent = calculatedVatPercent,
                        calculatedVatAmount = calculatedVatAmount,
                        totalTransection = totalTransection,
                        maxReceivingAmount = maxReceivingAmount,
                        oldStudent = oldStudent,
                        duePayment = duePayment,
                        newStudent = newStudent,
                        newPayment = newPayment
                    });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
            return Json(HttpNotFound());
        }

        [HttpPost]
        public JsonResult TransactionsSettlementFinalPost(long programId, long sessionId, long branchId, long campusId, DateTime datef, DateTime datet, decimal vatPercent, decimal calculatedVatPercent, decimal calculatedVatAmount, int totalTransection, decimal maxReceivingAmount, int oldStudent, decimal duePayment, int newStudent, decimal newPayment)
        {
            if (Request.IsAjaxRequest())
            {
                //DateTime StartDate = DateTime.Now;
                try
                {
                    DateTime dateFrom = Convert.ToDateTime(datef);
                    DateTime dateTo = Convert.ToDateTime(datet);
                    dateTo = dateTo.AddDays(1).AddMilliseconds(-1);

                    decimal totalCollection = _studentPaymentService.GetTotalCollectionAmount(sessionId, programId, branchId, campusId, dateFrom, dateTo);
                    decimal totalRefund = _studentPaymentService.GetTotalRefundAmount(sessionId, programId, branchId, campusId, dateFrom, dateTo);
                    decimal netCollection = totalCollection - totalRefund;
                    decimal vatStudentCollection = _studentPaymentTransectionsService.GetTotalCollectionAmountByReferenceId(sessionId, programId, branchId, campusId, dateFrom, dateTo, 39);
                    decimal taxStudentCollection = _studentPaymentTransectionsService.GetTotalCollectionAmountByReferenceId(sessionId, programId, branchId, campusId, dateFrom, dateTo, 40);
                    decimal bkashStudentCollection = _studentPaymentTransectionsService.GetTotalCollectionAmountByPaymentMethod(sessionId, programId, branchId, campusId, dateFrom, dateTo, (int)PaymentMethod.BKash);
                    decimal minVatAmount = vatStudentCollection + taxStudentCollection + bkashStudentCollection;
                    decimal minVatPercent = (minVatAmount / netCollection) * 100;

                    decimal percent = vatPercent < minVatPercent ? minVatPercent : vatPercent;
                    decimal totalAmount = (netCollection * percent) / 100;

                    string sessionName = programId.ToString() + "_" + sessionId.ToString() + "_" + branchId.ToString() + "_" + campusId.ToString() + "_" + dateFrom.ToString("yyyyMMdd") + "_" + dateTo.ToString("yyyyMMdd") + "_" + User.Identity.Name;

                    IList<StudentPaymentTransections> studentPaymentTransections = (IList<StudentPaymentTransections>)Session[sessionName];
                    Session[sessionName] = null;
                    bool status = true;
                    if (studentPaymentTransections != null && studentPaymentTransections.Count > 0)
                    {
                        if (calculatedVatAmount != studentPaymentTransections.Max(t => t.BAmount)) { status = false; }
                        if (calculatedVatPercent != decimal.Round((calculatedVatAmount / netCollection) * 100, 2, MidpointRounding.AwayFromZero)) { status = false; }

                        if (totalTransection != studentPaymentTransections.Count()) { status = false; }
                        if (oldStudent != studentPaymentTransections.Where(t => t.Priority == 3).Count()) { status = false; }
                        if (duePayment != studentPaymentTransections.Where(t => t.Priority == 3).Sum(a => a.ReceivedAmount)) { status = false; }
                        if (newStudent != studentPaymentTransections.Where(t => t.Priority == 5).Count()) { status = false; }
                        if (newPayment != studentPaymentTransections.Where(t => t.Priority == 5).Sum(a => a.ReceivedAmount)) { status = false; }
                        if (maxReceivingAmount != studentPaymentTransections.Max(t => t.ReceivedAmount)) { status = false; }
                    }
                    //else { status = false; }


                    if (status == true)
                    {
                        List<long> studentPaymentTransectionsIdList = studentPaymentTransections.Select(t => t.Id).ToList();
                        long userId = Convert.ToInt64(User.Identity.GetUserId());
                        bool isComplete = _studentPaymentTransectionsService.TransactionsSettlementFinalPost(sessionId, programId, branchId, campusId, dateFrom, dateTo, studentPaymentTransectionsIdList, userId);
                        if (isComplete == true)
                        {
                            return Json(new { IsSuccess = true, message = "Operation successfully completed." });
                        }
                        else
                        {
                            return Json(new { IsSuccess = false, message = "Database Operation failed. Please Try again." });
                        }
                    }
                    else
                    {
                        return Json(new { IsSuccess = false, message = "Some value changed. Please calculate again." });
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
            return Json(HttpNotFound());
        }

        #endregion

        #region Branch Wise Transactions Settlement
        public ActionResult BranchWiseTransactionsSettlement()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var organizations = _organizationService.LoadAuthorizedOrganization(_userMenu).ToList();
                organizations.Insert(0, new Organization() { ShortName = "Select Organization", Id = -1 }); //0 for all
                ViewBag.organizationList = new SelectList(organizations, "Id", "ShortName");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult BranchWiseTransactionsSettlement(long[] programId, long[] sessionId, long[] branchId, long[] campusId, string datef, string datet, double Persent = 100.0)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<Program> programs = _programService.LoadAuthorizedProgram(_userMenu);
            IList<Session> sessions = _sessionService.LoadAuthorizedSession(_userMenu, null);
            IList<Branch> branches = _branchService.LoadAuthorizedBranch(_userMenu);
            List<long> allAuthorizedBrancheIds = branches.Select(x => x.Id).ToList();
            long[] authorizedProgramIds = programs.Select(x => x.Id).ToArray();
            long[] authorizedSessionIds = sessions.Select(x => x.Id).ToArray();

            IList<Branch> authorizedBranchesByProgramSession = _branchService.LoadBranchByProgramSessionForTransaction(programId[0], sessionId[0], authorizedProgramIds, authorizedSessionIds, allAuthorizedBrancheIds).Distinct().ToList();
            IList<Branch> branchList = new List<Branch>();
            IList<Branch> branchByIds = new List<Branch>();
            IList<Campus> campusList = new List<Campus>();
            IList<Course> courseList = new List<Course>();

            if (!programId.Contains(0))
            {
                programs = programs.Where(x => x.Id.In(programId)).ToList();
                ViewBag.programName = string.Join(", ", programs.Select(p => p.Name).ToList());
            }
            else
            {
                ViewBag.programName = "All Programs";
            }

            if (!sessionId.Contains(0))
            {
                sessions = sessions.Where(x => x.Id.In(sessionId)).ToList();
                ViewBag.sessionName = string.Join(", ", sessions);
            }
            else
            {
                ViewBag.sessionName = "All";
            }

            // campusList = _campusService.LoadAuthorizedCampusesByBranch(branchId, authorizedBranchesByProgramSession);
            if (branchId.Contains(0))
            {
                branchByIds = authorizedBranchesByProgramSession;
            }
            else
            {
                branchByIds = authorizedBranchesByProgramSession.Where(br => br.Id.In(branchId)).ToList();
            }

            if (!campusId.Contains(0))
            {
                // campusList = _campusService.LoadAllOk().Where(camp => camp.Id.In(campusId) && camp.Status == Campus.EntityStatus.Active).ToList();
                campusList = _campusService.LoadCampus(null, null, null, null, campusId.ToList());
                IList<string> campusName = campusList.Select(c => c.Name).Distinct().ToList();
                ViewBag.campusName = string.Join(", ", campusName);
            }
            else
            {
                ViewBag.campusName = "All";
            }

            if (branchId.Contains(0))
            {
                ViewBag.branchName = "All";
            }
            else
            {
                ViewBag.branchName = string.Join(", ", branchByIds.Select(x => x.Name).ToList());
            }

            //    courseList = _courseService.GetCourseByProgramAndSessionForTransaction(programId[0], sessionId[0]);
            //if (course.Contains(0))
            //{

            //    ViewBag.courseName = "All";
            //}
            //else
            //{
            //    courseList = _courseService.LoadCourse(course);
            //    ViewBag.courseName = string.Join(", ", courseList.Select(c => c.Name).ToList());
            //}
            ViewBag.programId = programId;
            ViewBag.sessionId = sessionId;
            ViewBag.branchId = branchId;
            ViewBag.campusId = campusId;
            ViewBag.datef = datef;
            ViewBag.datet = datet;
            ViewBag.PageSize = Constants.PageSize;
            return View();
        }

        [HttpPost]
        public JsonResult GetBranchWiseTransactionsSettlementSummary(long branchId, DateTime datef, DateTime datet)
        {
            if (Request.IsAjaxRequest())
            {
                //DateTime StartDate = DateTime.Now;
                try
                {
                    DateTime dateFrom = Convert.ToDateTime(datef);
                    DateTime dateTo = Convert.ToDateTime(datet);
                    dateTo = dateTo.AddDays(1).AddMilliseconds(-1);

                    List<dynamic> branchWiseSummery = _studentPaymentTransectionsService.LoadBranchWiseSummery(branchId, dateFrom, dateTo);
                    List<dynamic> branchWiseSummeryFormated = new List<dynamic>();
                    foreach (var b in branchWiseSummery)
                    {
                        var c = b[11].ToString("#.##");
                        //if (b[6] + b[9] > 0 && Convert.ToDouble((((b[6] + b[9]) / b[7]) * 100)) >= 0.01)
                        //{
                        //    c = (((b[6] + b[9]) / b[7]) * 100).ToString("#.##");
                        //}
                        branchWiseSummeryFormated.Add(new
                        {
                            BranchId = b[0],
                            CampusId = b[1],
                            ProgramId = b[2],
                            SessionId = b[3],
                            Campus = b[4],
                            ProgramSession = b[5],
                            AlreadySettledAmount = b[6],
                            NetCollection = b[7],
                            NotSettledAmount = b[8],
                            VatTaxBkash = b[9],
                            TotalVatTaxBkash = b[10],
                            MinPercent = b[11],
                            OriginalMinPercent = b[12],
                            CalculatedMinPercent = c,
                            AlreadySettledDate = b[13]
                        });
                    }
                    return Json(new { IsSuccess = true, branchWiseSummery = branchWiseSummeryFormated });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
            return Json(HttpNotFound());
        }

        [HttpPost]
        public JsonResult GetBranchWiseTransactionsSettlementCalculate(long branchId, DateTime datef, DateTime datet, long[] campusIds, long[] programIds, long[] sessionIds, decimal[] vatPercents)
        {
            if (Request.IsAjaxRequest())
            {
                //DateTime StartDate = DateTime.Now;
                try
                {
                    DateTime dateFrom = Convert.ToDateTime(datef);
                    DateTime dateTo = Convert.ToDateTime(datet);
                    dateTo = dateTo.AddDays(1).AddMilliseconds(-1);


                    List<dynamic> branchWiseSummery = _studentPaymentTransectionsService.LoadBranchWiseSummery(branchId, dateFrom, dateTo);
                    List<dynamic> branchWiseCalculatedSummery = new List<dynamic>();

                    for (int i = 0; i < vatPercents.Count(); i++)
                    {
                        dynamic branchWiseData = branchWiseSummery.Where(x => x[0] == branchId && x[1] == campusIds[i] && x[2] == programIds[i] && x[3] == sessionIds[i]).FirstOrDefault();

                        var BranchId = branchWiseData[0]; //["BranchId"];
                        var CampusId = branchWiseData[1]; //["CampusId"];
                        var ProgramId = branchWiseData[2]; //["ProgramId"];
                        var SessionId = branchWiseData[3]; //["SessionId"];
                        var Campus = branchWiseData[4]; //["Campus"];
                        var ProgramSession = branchWiseData[5]; //["ProgramSession"];
                        var AlreadySettledAmount = branchWiseData[6]; //["AlreadySettledAmount"];
                        var NetCollection = branchWiseData[7]; //["NetCollection"];
                        var NotSettledAmount = branchWiseData[8]; //["NotSettledAmount"];
                        var VatTaxBkash = branchWiseData[9]; //["VatTaxBkash"];
                        var TotalVatTaxBkash = branchWiseData[10]; //["VatTaxBkash"];
                        var MinPercent = branchWiseData[11]; //["MinPercent"];
                        var OriginalMinPercent = branchWiseData[12]; //["OriginalMinPercent"];
                        var AlreadySettledDate = branchWiseData[13]; //["AlreadySettledDate"];
                        //decimal minVatAmount = vatStudentCollection + taxStudentCollection + bkashStudentCollection;
                        //decimal minVatPercent = (minVatAmount / netCollection) * 100;

                        //var CalculatedMinPercent = (((AlreadySettledAmount + VatTaxBkash) / NetCollection) * 100);
                        var CalculatedMinPercent = ((VatTaxBkash / NetCollection) * 100);
                        decimal percent = vatPercents[i] < CalculatedMinPercent ? CalculatedMinPercent : vatPercents[i];

                        IList<StudentPaymentTransections> studentPaymentTransections = new List<StudentPaymentTransections>();


                        decimal totalAmount = 0;
                        if (percent > 0)
                        {
                            totalAmount = (NotSettledAmount * percent) / 100;
                        }
                        //decimal totalAmount = ((NetCollection * percent) / 100) - AlreadySettledAmount;
                        if (totalAmount < VatTaxBkash) totalAmount = VatTaxBkash;

                        studentPaymentTransections = _studentPaymentTransectionsService.LoadTransactionsSettlementCalculate(SessionId, ProgramId, branchId, CampusId, dateFrom, dateTo, totalAmount);
                        string sessionName = ProgramId.ToString() + "_" + SessionId.ToString() + "_" + branchId.ToString() + "_" + CampusId.ToString() + "_" + dateFrom.ToString("yyyyMMdd") + "_" + dateTo.ToString("yyyyMMdd") + "_" + User.Identity.Name;

                        Session[sessionName] = studentPaymentTransections;


                        #region New Calculation
                        decimal calculatedVatAmount = 0;
                        //decimal calculatedVatPercent = 0;

                        int totalTransection = 0;
                        int newStudent = 0;
                        decimal newPayment = 0;
                        int oldStudent = 0;
                        decimal duePayment = 0;
                        //decimal maxReceivingAmount = 0;

                        if (studentPaymentTransections != null && studentPaymentTransections.Count > 0)
                        {
                            calculatedVatAmount = studentPaymentTransections.Max(t => t.BAmount);
                            //calculatedVatPercent = (calculatedVatAmount / netCollection) * 100;

                            totalTransection = studentPaymentTransections.Count();
                            newStudent = studentPaymentTransections.Where(t => t.IsExistingStudent == false).Count();
                            newPayment = studentPaymentTransections.Where(t => t.IsExistingStudent == false).Sum(a => a.ReceivedAmount);
                            oldStudent = studentPaymentTransections.Where(t => t.IsExistingStudent == true).Count();
                            duePayment = studentPaymentTransections.Where(t => t.IsExistingStudent == true).Sum(a => a.ReceivedAmount);
                            //maxReceivingAmount = studentPaymentTransections.Max(t => t.ReceivedAmount);
                        }
                        #endregion

                        branchWiseCalculatedSummery.Add(new
                        {
                            BranchId = BranchId,
                            CampusId = CampusId,
                            ProgramId = ProgramId,
                            SessionId = SessionId,
                            Campus = Campus,
                            ProgramSession = ProgramSession,
                            AlreadySettledAmount = AlreadySettledAmount,
                            NetCollection = NetCollection,
                            NotSettledAmount = NotSettledAmount,
                            VatTaxBkash = VatTaxBkash,
                            TotalVatTaxBkash = TotalVatTaxBkash,
                            //MinPercent = MinPercent,
                            MinPercent = Convert.ToDouble(percent) >= 0.01 ? percent.ToString("#.##") : "0",
                            OriginalMinPercent = OriginalMinPercent,
                            AlreadySettledDate = AlreadySettledDate,

                            CalculatedVatAmount = calculatedVatAmount,
                            TotalTransection = totalTransection,
                            NewStudent = newStudent,
                            NewPayment = newPayment,
                            OldStudent = oldStudent,
                            DuePayment = duePayment
                        });
                    }


                    return Json(new { IsSuccess = true, branchWiseCalculatedSummery = branchWiseCalculatedSummery });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
            return Json(HttpNotFound());
        }

        [HttpPost]
        public JsonResult BranchWiseTransactionsSettlementFinalPost(long branchId, DateTime datef, DateTime datet, long[] campusIds, long[] programIds, long[] sessionIds, decimal[] vatPercents)
        {
            if (Request.IsAjaxRequest())
            {
                //DateTime StartDate = DateTime.Now;
                try
                {
                    DateTime dateFrom = Convert.ToDateTime(datef);
                    DateTime dateTo = Convert.ToDateTime(datet);
                    dateTo = dateTo.AddDays(1).AddMilliseconds(-1);


                    List<dynamic> branchWiseSummery = _studentPaymentTransectionsService.LoadBranchWiseSummery(branchId, dateFrom, dateTo);
                    List<dynamic> branchWiseCalculatedSummery = new List<dynamic>();
                    bool finalStatus = true;
                    for (int i = 0; i < vatPercents.Count(); i++)
                    {
                        dynamic branchWiseData = branchWiseSummery.Where(x => x[0] == branchId && x[1] == campusIds[i] && x[2] == programIds[i] && x[3] == sessionIds[i]).FirstOrDefault();

                        var BranchId = branchWiseData[0];
                        var CampusId = branchWiseData[1];
                        var ProgramId = branchWiseData[2];
                        var SessionId = branchWiseData[3];
                        var Campus = branchWiseData[4];
                        var ProgramSession = branchWiseData[5];
                        var AlreadySettledAmount = branchWiseData[6];
                        var NetCollection = branchWiseData[7];
                        var NotSettledAmount = branchWiseData[8];
                        var VatTaxBkash = branchWiseData[9];
                        var TotalVatTaxBkash = branchWiseData[10];
                        var MinPercent = branchWiseData[11];
                        var OriginalMinPercent = branchWiseData[12];

                        //var CalculatedMinPercent = (((AlreadySettledAmount + VatTaxBkash) / NetCollection) * 100);
                        var CalculatedMinPercent = ((VatTaxBkash / NetCollection) * 100);
                        decimal percent = vatPercents[i] < CalculatedMinPercent ? CalculatedMinPercent : vatPercents[i];

                        if (percent >= 0)
                        {
                            string sessionName = ProgramId.ToString() + "_" + SessionId.ToString() + "_" + BranchId.ToString() + "_" + CampusId.ToString() + "_" + dateFrom.ToString("yyyyMMdd") + "_" + dateTo.ToString("yyyyMMdd") + "_" + User.Identity.Name;

                            IList<StudentPaymentTransections> studentPaymentTransections = (IList<StudentPaymentTransections>)Session[sessionName];
                            Session[sessionName] = null;

                            if (studentPaymentTransections != null && studentPaymentTransections.Count >= 0)
                            {
                                List<long> studentPaymentTransectionsIdList = studentPaymentTransections.Select(t => t.Id).ToList();
                                long userId = Convert.ToInt64(User.Identity.GetUserId());
                                bool isComplete = _studentPaymentTransectionsService.TransactionsSettlementFinalPost(SessionId, ProgramId, BranchId, CampusId, dateFrom, dateTo, studentPaymentTransectionsIdList, userId);
                                if (isComplete != true)
                                {
                                    finalStatus = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (finalStatus == true)
                        return Json(new { IsSuccess = true, message = "Operation successfully completed." });
                    else
                        return Json(new { IsSuccess = false, message = "Database Operation failed. Please Try again." });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
            return Json(HttpNotFound());           
        }

        #endregion

        #region Settled Comparison Report
        public ActionResult SettledComparisonReport()
        {
            ViewBag.IsPost = false;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.ProgramId = null;
            try
            {
                ViewBag.DateFrom = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ProgramList = new SelectList(new List<Program>(), "Id", "Name");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }
        [HttpPost]
        public ActionResult SettledComparisonReport(long organizationId, long programId, string dateFrom, string dateTo)
        {
            ViewBag.IsPost = true;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.ProgramList = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.DateFrom = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
            //ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = null;
            ViewBag.ProgramId = null;
            ViewBag.OrganizationName = "";
            ViewBag.ProgramName = "";
            ViewBag.SettledComparisonReport = new List<SettledComparisonReportDto>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationName = _organizationService.LoadById(organizationId).Name;
                ViewBag.ProgramName = (programId != 0) ? _programService.GetProgram(programId).Name : "All Program";
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", organizationId);

                List<SelectListItem> programSelectList = new SelectList(_programService.LoadAuthorizedProgram(_userMenu, _commonHelper.ConvertIdToList(organizationId)), "Id", "Name", programId).ToList();
                programSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All Program" });
                ViewBag.ProgramList = new SelectList(programSelectList, "Value", "Text", programId);

                ViewBag.DateFrom = Convert.ToDateTime(dateFrom).ToString("yyyy-MM-dd");
                ViewBag.DateTo = Convert.ToDateTime(dateTo).ToString("yyyy-MM-dd");
                if (Convert.ToDateTime(dateFrom) > Convert.ToDateTime(dateTo))
                    throw new InvalidDataException("From Date must be equal or Less than To Date");

                List<SettledComparisonReportDto> settledComparisonReportList = _studentPaymentService.SettledComparisonReport(_userMenu, organizationId, programId, dateFrom, dateTo);
                ViewBag.SettledComparisonReport = settledComparisonReportList;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }
        #endregion Collection Summary Report

    }
}