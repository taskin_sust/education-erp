﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.AssignItemTest
{
    interface IProgramSessionItemSaveOrUpdateTest
    {
        #region Basic Test

        void Should_Return_Save_Null_Exception();
        void Should_Return_Save_Successfully();
        void Should_Return_Null_Item_Exception();
        
        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "ProgramSessionItem")]
    public class ProgramSessionItemSaveOrUpdateTest : ProgramSessionItemBaseTest, IProgramSessionItemSaveOrUpdateTest
    {
        [Fact]
        public void Should_Return_Save_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _ProgramSessionItemService.SaveOrUpdate(null));

            Assert.Throws<NullObjectException>(
                () => _ProgramSessionItemFactory.CreateWithProperties(0, null, null, null).Persist()); //TODO: Never use Persist() method for testing a service method call. Use appropriate service method.

            organizationFactory.Create().Persist();
            _ProgramSessionItemFactory.Create().WithItem(organizationFactory.Object).WithProgramAndSession(organizationFactory.Object);
            Assert.Throws<NullObjectException>(() => _ProgramSessionItemFactory.Persist()); //TODO: Never use Persist() method for testing a service method call. Use appropriate service method.
        }

        [Fact]
        public void Should_Return_Save_Successfully() //TODO: This is actually entity loading test. Save entity using service method and Load using service method.
        {
            _ProgramSessionItemFactory.CreateMore(10).Persist();
            Assert.StrictEqual(10, _ProgramSessionItemFactory.ObjectList.Count);
            
        }

        [Fact]
        public void Should_Return_Null_Item_Exception()
        {
            _ProgramSessionItemFactory.Create().WithProgramAndSession();
            Assert.Throws<NullObjectException>(() => _ProgramSessionItemFactory.Persist()); //TODO: Never use Persist() method for testing a service method call. Use appropriate service method.
        }

        //NOTE: Every test should call service method. And required paramiters may be created by Factories. 

        //TODO: No test written for update

        //TODO: No test for business logic verification
        

    }
}
