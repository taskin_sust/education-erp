﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FluentNHibernate.Conventions;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Administration
{
    public interface ISurveyQuestionService : IBaseService
    {
        #region Operational Function
        void Save(SurveyQuestion erpQuestion);
        void Update(SurveyQuestion erpQuestion);   
        void Delete(SurveyQuestion deleteSurveyQuestion);
        void UpdateRank(SurveyQuestion surveyQuestionOldObj, SurveyQuestion surveyQuestionUpdateObj);
        string SaveUpdateAssignSurveyQuestion(SurveyQuestionAssign surveyQuestionAssign, long programId, long sessionId);
        #endregion

        #region Single Instances Loading Function
        SurveyQuestion LoadByRank(int newRank);
        SurveyQuestion GetSurveyQuestion(long id);
        #endregion

        #region List Loading Function
        IList<SurveyQuestion> LoadSurveyQuestion(int start, int length, string orderBy, string orderDir, string question, string status);
        IList<SurveyQuestion> LoadSurveyQuestion();
       
        #endregion

        #region Others Function
        int SurveyQuestionRowCount(string question, string status);
        int GetMaximumRank(SurveyQuestion surveyQuestion);
        #endregion
    }
    public class SurveyQuestionService : BaseService, ISurveyQuestionService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ISurveyQuestionDao _surveyQuestionDao;
        private readonly ISurveyQuestionAnswerDao _surveyQuestionAnswerDao;
        private readonly IProgramSessionSurveyQuestionDao _programSessionSurveyQuestionDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        public SurveyQuestionService(ISession session)
        {
            Session = session;
            _surveyQuestionDao = new SurveyQuestionDao() { Session = Session };
            _surveyQuestionAnswerDao = new SurveyQuestionAnswerDao() { Session = Session };
            _programSessionSurveyQuestionDao = new ProgramSessionSurveyQuestionDao() { Session = Session };
            _programDao = new ProgramDao() { Session = Session };
            _sessionDao = new SessionDao() { Session = Session };
        }
        #endregion

        #region Operational Function
        public void Save(SurveyQuestion erpQuestion)
        {
            ITransaction transaction = null;
            try
            {
                //CheckEmptyField(erpQuestion);
                if (erpQuestion == null)
                {
                    throw new NullObjectException("Survey Question can not be null");
                }

                var validationContext = new ValidationContext(erpQuestion, null, null);
                var validationResults = new List<ValidationResult>();
                var isValid = Validator.TryValidateObject(erpQuestion, validationContext, validationResults);

                if (!isValid)
                {
                    string errorMessage = "";
                    validationResults.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                    throw new MessageException(errorMessage);
                }
                CheckEmptyField(erpQuestion);
                CheckBeforeSave(erpQuestion);
                if (erpQuestion.Rank<1)
                {
                    erpQuestion.Rank = _surveyQuestionDao.GetMaximumRank(erpQuestion) + 1;
                }
                
                
                using (transaction = Session.BeginTransaction())
                {
                    _surveyQuestionDao.Save(erpQuestion);
                    transaction.Commit();
                }
                
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (MessageException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        public void Update(SurveyQuestion erpQuestion)
        {
            ITransaction transaction = null;
            try
            {
                if (erpQuestion == null)
                {
                    throw new NullObjectException("Survey Question can not be null");
                }
                SurveyQuestion erpQuestionUpdate = _surveyQuestionDao.LoadById(erpQuestion.Id);
                if (erpQuestionUpdate == null)
                {
                    throw new NullObjectException("Survey Question can not be null");
                }

                erpQuestionUpdate.Question = erpQuestion.Question;

                var validationContext = new ValidationContext(erpQuestionUpdate, null, null);
                var validationResults = new List<ValidationResult>();
                var isValid = Validator.TryValidateObject(erpQuestionUpdate, validationContext, validationResults);

                if (!isValid)
                {
                    string errorMessage = "";
                    validationResults.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                    throw new MessageException(errorMessage);
                }


                if (erpQuestionUpdate.Status == SurveyQuestion.EntityStatus.Active && 
                        erpQuestion.Status == SurveyQuestion.EntityStatus.Inactive && 
                        erpQuestionUpdate.ProgramSessionSurveyQuestions.Any()
                 )
                    throw new DependencyException("This Question is already assigned for a program.");

                erpQuestionUpdate.Status = erpQuestion.Status;
                CheckBeforeSave(erpQuestionUpdate);

                foreach (var sq in erpQuestionUpdate.SurveyQuestionAnswers)
                {
                    if (sq.StudentSurveyAnswers.Any())
                    {
                        foreach (var eq in erpQuestion.SurveyQuestionAnswers)
                        {
                            if (sq.Id == eq.Id)
                            {
                                throw new DependencyException("Student already attend this survey.");
                            }
                        }
                    }
                }

                //check already exist answer and generate answer list
                var csqa = new List<long>();

                IList<SurveyQuestionAnswer> sqaList = new List<SurveyQuestionAnswer>();
                foreach (var sqa in erpQuestion.SurveyQuestionAnswers)
                {

                    var sqans = new SurveyQuestionAnswer();
                    if (sqa.Id != 0)
                    {
                        sqans = _surveyQuestionAnswerDao.LoadById(sqa.Id);
                    }
                    sqans.Answer = sqa.Answer;
                    sqans.SurveyQuestion = erpQuestionUpdate;
                    sqans.Status = SurveyQuestionAnswer.EntityStatus.Active;
                    sqaList.Add(sqans);
                    csqa.Add(sqa.Id);
                }

                using (transaction = Session.BeginTransaction())
                {
                    foreach (var sq in erpQuestionUpdate.SurveyQuestionAnswers)
                    {
                        bool exist = csqa.Any(item => item == sq.Id);
                        if (!exist)
                        {
                            sq.SurveyQuestion = null;
                            //delete answer which is uncheck by user
                            _surveyQuestionAnswerDao.Delete(sq);
                        }
                    }
                    erpQuestionUpdate.SurveyQuestionAnswers = sqaList;

                    _surveyQuestionDao.Update(erpQuestionUpdate);
                    transaction.Commit();
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }

           
            
        }
        public void Delete(SurveyQuestion deleteSurveyQuestion)
        {
            ITransaction trans = null;
            try
            {
                if (deleteSurveyQuestion.ProgramSessionSurveyQuestions.Any() || deleteSurveyQuestion.StudentSurveyAnswers.Any())
                {
                    throw new DependencyException("This Survey Question already assigned.");
                }
                else
                {
                    using (trans = Session.BeginTransaction())
                    {
                        _surveyQuestionDao.Update(deleteSurveyQuestion);
                        trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public void UpdateRank(SurveyQuestion surveyQuestionOldObj, SurveyQuestion surveyQuestionUpdateObj)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    _surveyQuestionDao.Update(surveyQuestionOldObj);
                    _surveyQuestionDao.Update(surveyQuestionUpdateObj);
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
        }
        public string SaveUpdateAssignSurveyQuestion(SurveyQuestionAssign surveyQuestionAssign, long programId, long sessionId)
        {
            try
            {
                ITransaction transaction = null;
                using (transaction = Session.BeginTransaction())
                {
                    try
                    {
                        #region Checking All Old Assign List Using This Program & Session

                        IList<ProgramSessionSurveyQuestion> existingSurveyQuestionList = _programSessionSurveyQuestionDao.LoadSurveyQuestionByProgramSession(programId, sessionId, surveyQuestionAssign.SurveyType);

                        IList<long> nonDeleteAbleSurveyQuestionList = new List<long>();
                        string returnMessage = "";

                        foreach (var exisSur in existingSurveyQuestionList)
                        {
                            bool isStudentExitWithThisSurvey = false;
                            if (exisSur.SurveyType == 1)
                            {
                                isStudentExitWithThisSurvey = _programSessionSurveyQuestionDao.IsExistCheckStudentByProgramSessionAndSurveyQuestion(programId, sessionId, exisSur.SurveyQuestion.Id);
                            }

                            if (isStudentExitWithThisSurvey)
                            {
                                nonDeleteAbleSurveyQuestionList.Add(exisSur.SurveyQuestion.Id);
                                returnMessage += exisSur.SurveyQuestion.Question + " ,";
                            }
                            else
                            {
                                _programSessionSurveyQuestionDao.Delete(exisSur);
                            }

                        }

                        #endregion

                        #region Assign SurveyQuestion
                        string returnSucessMessage = "";
                        var surveyType = surveyQuestionAssign.SurveyType;
                        if (surveyQuestionAssign.SelectedSurveyQuestions != null)
                        {
                            foreach (var surId in surveyQuestionAssign.SelectedSurveyQuestions)
                            {
                                bool isExistInNonDeleteableList = nonDeleteAbleSurveyQuestionList.Contains(surId);

                                if (!isExistInNonDeleteableList)
                                {
                                    var surveyQuestionObj = _surveyQuestionDao.LoadById(surId);
                                    var programObj = _programDao.LoadById(programId);
                                    var sessionObj = _sessionDao.LoadById(sessionId);

                                    var programSessionSurveyQuestion = new ProgramSessionSurveyQuestion
                                    {
                                        SurveyQuestion = surveyQuestionObj,
                                        Program = programObj,
                                        Session = sessionObj,
                                        SurveyType = surveyType
                                    };
                                    _programSessionSurveyQuestionDao.Save(programSessionSurveyQuestion);
                                    returnSucessMessage += surveyQuestionObj.Question + " ,";
                                }
                            }
                        }

                        #endregion

                        transaction.Commit();
                        if (returnMessage != "")
                            return "These Survey Question have already been assigned- " + returnMessage;
                        //return "E:These Survey Question have already been assigned- " + returnMessage + "|||S:These Survey Question is successfully assigned- " + returnSucessMessage;
                        else
                        {
                            return "";
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        if (transaction != null && transaction.IsActive)
                            transaction.Rollback();
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function
        public SurveyQuestion GetSurveyQuestion(long id)
        {
            try
            {
                return _surveyQuestionDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }

        }
        public SurveyQuestion LoadByRank(int newRank)
        {
            try
            {
                return _surveyQuestionDao.LoadByRank(newRank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region List Loading Function
        public IList<SurveyQuestion> LoadSurveyQuestion()
        {
            try
            {
                return _surveyQuestionDao.LoadActive();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<SurveyQuestion> LoadSurveyQuestion(int start, int length, string orderBy, string orderDir, string question, string status)
        {
            try
            {
                return _surveyQuestionDao.GetSurveyQuestionList(start, length, orderBy, orderDir, question, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Others Function
        public int SurveyQuestionRowCount(string question, string status)
        {
            try
            {
                return _surveyQuestionDao.SurveyQuestionRowCount(question, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public int GetMaximumRank(SurveyQuestion surveyQuestion)
        {
            try
            {
                return _surveyQuestionDao.GetMaximumRank(surveyQuestion);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Helper Function
        private void CheckEmptyField(SurveyQuestion erpQuestion)
        {
            if (string.IsNullOrEmpty(erpQuestion.Question))
                throw new EmptyFieldException("Question is empty");
            if(!erpQuestion.SurveyQuestionAnswers.Any())
                throw new EmptyFieldException("Question Answer List is empty");
            var isEmptySurveyQuestionReference =
                erpQuestion.SurveyQuestionAnswers.Where(x => x.SurveyQuestion == null).ToList();
            if(isEmptySurveyQuestionReference.Any())
                throw new EmptyFieldException("Question Answer List is empty");

        }
        private void CheckBeforeSave(SurveyQuestion erpQuestion)
        {
            //CheckEmptyField(erpQuestion);
            //check duplicate Question
            CheckDuplicateName(erpQuestion);

        }
        private void CheckDuplicateName(SurveyQuestion erpQuestion)
        {
            if (_surveyQuestionDao.HasDuplicateByName(erpQuestion.Question, erpQuestion.Id) == true)
                throw new DuplicateEntryException("Duplicate Question found");
            
        }
        #endregion
    }
}
