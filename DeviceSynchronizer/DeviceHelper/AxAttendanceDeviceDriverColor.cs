﻿using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model.ApiModel;
using DeviceSynchronizer.Model.BusinessModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceSynchronizer.DeviceHelper
{
    public class AxAttendanceDeviceDriverColor : AxAttendanceDeviceDriver
    {
        public AxAttendanceDeviceDriverColor(AttendanceDeviceModel attendanceDevice, MessagePass throwMessage)
            : base(attendanceDevice, throwMessage)
        {
        }

        protected override bool Register(TeamMemberModel member, string sPassword, int iPrivilege)
        {
            bool returnResult = false, isExitToDevice = false;
            string name, password;
            int priv;
            try
            {
                bool isEnabled = (member.Status == MemberStatus.Active);
                if (!String.IsNullOrEmpty(member.CardNumber))
                {
                    bool isSet = AxDevice.SetStrCardNumber(member.CardNumber);
                    if (!isSet) return false;
                    if (isEnabled)
                        returnResult = AxDevice.SSR_SetUserInfo(_attendanceDevice.MachineNo, member.EnrollNo.ToString(), member.Name, sPassword, iPrivilege, isEnabled);
                    else
                    {
                        //check if user exits in device. Delete User if only Exit
                        AxDevice.SSR_GetUserInfo(_attendanceDevice.MachineNo, member.EnrollNo.ToString(), out name, out password, out priv, out isExitToDevice);
                        if (isExitToDevice)
                            returnResult = AxDevice.SSR_DeleteEnrollData(_attendanceDevice.MachineNo, member.EnrollNo.ToString(), BusinessRules.BackupNumber);
                        else
                            return returnResult;
                    }

                    if (returnResult)
                        OnThrowingMessage("Register user " + member.Name + " in device with pin " + member.Pin, MessageType.Info);
                    else
                        OnThrowingMessage("Failed to save user " + member.Name + " in device ", MessageType.Error);
                }
                return returnResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

        public override List<AttendanceLog> LoadLogList()
        {
            //for color device
            var idwEnrollNumber = "";
            var idwVerifyMode = 0;
            var idwInOutMode = 0;
            var idwYear = 0;
            var idwMonth = 0;
            var idwDay = 0;
            var idwHour = 0;
            var idwMinute = 0;
            var idwSecond = 0;
            var idwWorkCode = 0;
            var attdlogList = new List<AttendanceLog>();
            try
            {
                OnThrowingMessage("Retriving attendance data from device", MessageType.Info);

                //for color
                while (AxDevice.SSR_GetGeneralLogData(_attendanceDevice.MachineNo, out idwEnrollNumber, out idwVerifyMode, out idwInOutMode,
                    out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkCode))
                {
                    var attdLogObj = new AttendanceLog
                    {
                        EnrollNumber = Convert.ToInt32(idwEnrollNumber),
                        PunchType = idwVerifyMode,
                        PunchTime = new DateTime(idwYear, idwMonth, idwDay, idwHour, idwMinute, 0).ToString("yyyy-MM-dd HH:mm:ss"),
                        DeviceId = _attendanceDevice.Id
                    };

                    if (attdLogObj.EnrollNumber != 0)
                        attdlogList.Add(attdLogObj);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
            return attdlogList;
        }
    }
}
