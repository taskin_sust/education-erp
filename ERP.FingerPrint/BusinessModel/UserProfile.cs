﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.FingerPrint.BusinessModel
{
    public class UserProfile
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string CurrentToken { get; set; }
    }
}
