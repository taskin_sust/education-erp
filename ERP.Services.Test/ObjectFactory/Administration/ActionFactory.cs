﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class ActionsFactory : ObjectFactoryBase<Actions>
    {
        #region Object Initialization

        protected readonly IActionsService _actionService;
        private AreaControllersFactory _areaControllersFactory;

        public ActionsFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _actionService = new ActionsService(_session);
            _areaControllersFactory = new AreaControllersFactory(this, _session);
        }

        #endregion

        #region Create

        public ActionsFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }

        public ActionsFactory Create()
        {
            Object = new Actions()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Branch.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public ActionsFactory WithController()
        {
            _areaControllersFactory.Create().WithArea(Guid.NewGuid() + " HFG RTTRT GGFG").Persist();
            Object.AreaControllers = _areaControllersFactory.Object;
            return this;
        }

        public ActionsFactory WithController(AreaControllers areaControllers)
        {
            Object.AreaControllers = areaControllers;
            return this;
        }

        public ActionsFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _actionService.Save(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _actionService.Save(item);
                    }
                }
            }
            return this;
        }

        public ActionsFactory CreateMore(int numberOfAction, IList<AreaControllers> areaControllersList)
        {
            CreateActionList(numberOfAction, areaControllersList);
            return this;
        }

        private List<Actions> CreateActionList(int numberOfAction, IList<AreaControllers> areaControllersList)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Actions>();
            foreach (var areaController in areaControllersList)
            {
                for (int i = 0; i < numberOfAction; i++)
                {
                    var action = new Actions()
                    {
                        Id = 0,
                        Name = "_" + i,
                        Rank = i,
                        CreateBy = 1,
                        ModifyBy = 1,
                        Status = Program.EntityStatus.Active
                    };
                    action.AreaControllers = areaController;
                    list.Add(action);
                }

            }
            ObjectList.AddRange(list);
            return list;
        }


        public ActionsFactory CreateMore(int numberOfAction)
        {
            CreateActionList(numberOfAction);
            return this;
        }

        private List<Actions> CreateActionList(int numberOfAction)
        {
            ListStartIndex = ObjectList.Count;
            var areaControllerNamePrefix = Guid.NewGuid().ToString();
            _areaControllersFactory.Create()
                .WithNameAndRank(areaControllerNamePrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"), 1)
                .WithArea(Guid.NewGuid() + " FGDF FGF GFGFG")
                .Persist();
            var list = new List<Actions>();
            for (int i = 0; i < numberOfAction; i++)
            {
                var action = new Actions
                {
                    Id = 0,
                    Name = "_" + i,
                    Rank = i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Program.EntityStatus.Active,
                    AreaControllers = _areaControllersFactory.Object
                };
                list.Add(action);
            }
            ObjectList.AddRange(list);
            return list;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup
        public void CleanupActions(List<long> actionsIdList)
        {
            _session.CreateSQLQuery("Delete from Actions where id in(" + string.Join(", ", actionsIdList) + ")").ExecuteUpdate();
        }
        public void Cleanup()
        {
            DeleteAll();
            _areaControllersFactory.Cleanup();
        }

        #endregion

    }
}
