﻿using System;
using System.IO;
using System.Net;
using System.Text;
using log4net;
using Newtonsoft.Json;
using Synchronizer.Updater.Model;

namespace Synchronizer.Updater.Helper
{
    public class HttpHelper
    {
        #region Objects/Properties/Services/Dao & Initialization

        private static readonly ILog Log = LogManager.GetLogger("HelperLogger");

        //return JsonResponse object
        public JsonResponse HttpPostRequest(string url, string data)
        {
            string output = String.Empty;
            var req = WebRequest.Create(url);
            try
            {
                req.Method = "POST";
                req.Timeout = 1000000;
                req.ContentType = "application/x-www-form-urlencoded";
                byte[] sentData = Encoding.ASCII.GetBytes(data);
                req.ContentLength = sentData.Length;
                using (var sendStream = req.GetRequestStream())
                {
                    sendStream.Write(sentData, 0, sentData.Length);
                    sendStream.Close();
                }
                var response = req.GetResponse();
                output = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (WebException ex)
            {
                Log.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
            return ConvertToJsonResponse(output);
        }

        #endregion

        public JsonResponse HttpGetRequest(string url)
        {
            string outputStr = String.Empty;
            var req = WebRequest.Create(url);
            try
            {
                var resp = req.GetResponse();
                using (var stream = resp.GetResponseStream())
                {
                    if (stream != null)
                        using (var sr = new StreamReader(stream))
                        {
                            outputStr = sr.ReadToEnd();
                            sr.Close();
                        }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
            return ConvertToJsonResponse(outputStr);
        }

        #region Private Methods

        private JsonResponse ConvertToJsonResponse(string response)
        {
            try
            {
                return JsonConvert.DeserializeObject<JsonResponse>(response);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        #endregion
    }
}
