﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Dto.Exam;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Exam;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Exam;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Exam
{
    public interface IExamMcqQuestionSetService:IBaseService
    {
        #region Operational Function

        IList<ExamMcqQuestionSet> SaveOrUpdate(ExamMcqQuestionSetViewModel viewModel);

        void Delete(IList<ExamMcqQuestionSet> setList);

        #endregion

        #region Single Instances Loading Function

        ExamMcqQuestionSet LoadLastSetIdByExamId(long examId);

        #endregion

        #region List Loading Function

        IList<IGrouping<int, ExamMcqQuestionSet>> LoadExamMcqQuestionSetList(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds, int start, int length,
            string orderBy, string p, string organization, string program, string session, string course, string uniqueSet);
        IList<ExamMcqQuestionSet> LoadExamMcqQuestionSet(long examId, List<int> uniqueSet = null, int? examMcqQuestionSerial = null, int? setId = null);

        IList<ExamMcqQuestionSetDto> LoadExamMcqQuestionSet(List<UserMenu> userMenu, int start,
            int length, string orderBy, string orderDir, string organization, string program, string session, string course, string uniqueSet, string exam);

        List<QuestionAnswerDto> LoadExamMcqQuestionSet(long examId, string code); 

        #endregion

        #region Others Function

        int GetExamMcqQuestionSetRowCount(List<UserMenu> userMenu, string organization, string program,
                    string session, string course, string uniqueSet,string exam);

        #endregion
        
    }
    public class ExamMcqQuestionSetService : BaseService, IExamMcqQuestionSetService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("ExamService");

        #endregion

        #region Propertise & Object Initialization

        private readonly IExamMcqQuestionDao _examMcqQuestionDao;
        private readonly IExamMcqQuestionSetDao _examMcqQuestionSetDao;
        private readonly IExamsDao _examsDao;
        private readonly IExamMcqQuestionSetPrintDao _examMcqQuestionSetPrintDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly IBranchDao _branchDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        public ExamMcqQuestionSetService(ISession session)
        {
            Session = session;
            _examMcqQuestionDao = new ExamMcqQuestionDao() { Session = session };
            _examMcqQuestionSetDao = new ExamMcqQuestionSetDao(){Session = session};
            _examsDao = new ExamsDao(){Session = session};
            _examMcqQuestionSetPrintDao = new ExamMcqQuestionSetPrintDao(){Session = session};
            _organizationDao = new OrganizationDao(){Session = session};
            _branchDao = new BranchDao(){Session = session};
            _programDao = new ProgramDao(){Session = session};
            _sessionDao = new SessionDao(){Session = session};
        }

        #endregion

        #region Operational Function

        public IList<ExamMcqQuestionSet> SaveOrUpdate(ExamMcqQuestionSetViewModel viewModel)
        {
            
            int i = 0;
            ITransaction transaction = null;
            Exams exam = _examsDao.LoadById(viewModel.ExamId);
            try
            {
                Random rndm = new Random();
                var rndmValue = Convert.ToInt32(rndm.Next(0, 100000).ToString("000000"));
                
                if (exam == null)
                    throw new InvalidDataException("Invalid exam");

                if (exam.IsBanglaVersion == false && exam.IsEnglishversion == false)
                {
                    throw new InvalidDataException("Exam must be declare bangla or english version");
                }
                if (exam.IsSetGenerationRunning == 0)
                {
                    try
                    {
                        using (transaction = Session.BeginTransaction())
                        {
                            Session.Evict(exam);
                            exam.IsSetGenerationRunning = rndmValue;
                            _examsDao.SaveOrUpdate(exam); 
                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        throw ex;
                    }
                    finally
                    {
                        if (transaction != null && transaction.IsActive)
                            transaction.Rollback();
                    }
                }
                else if (exam.IsSetGenerationRunning != rndmValue)
                {
                    throw new MessageException("Already question set generate running.");
                }

                int examQuestionCount = exam.ExamDetails.Where(x => x.ExamType == (int) ExamType.Mcq && x.Status == (int)ExamsDetails.EntityStatus.Active).Sum(x=>x.TotalQuestion);

                if(examQuestionCount<=0)
                    throw new InvalidDataException("This exam has no total question");

                List<int> examSetNo = new List<int>();
                if (viewModel.UniqueSet == null || viewModel.UniqueSet.Contains(SelectionType.SelelectAll))
                {
                    for (int j = 1; j <= exam.TotalUniqueSet; j++)
                    {
                        if (exam.IsBanglaVersion)
                        {
                            var totalBanglaQuestionList = _examMcqQuestionDao.LoadExamMcqQuestion(exam.Id, j, (int)ExamVersion.Bangla);
                            if (totalBanglaQuestionList != null && totalBanglaQuestionList.Any() && examQuestionCount == totalBanglaQuestionList.Count())
                            {
                                examSetNo.Add(j);
                            }
                        }
                        else if (exam.IsEnglishversion)
                        {
                            var totalEnglishQuestion = _examMcqQuestionDao.LoadExamMcqQuestion(exam.Id, j, (int)ExamVersion.English);
                            if (totalEnglishQuestion != null && totalEnglishQuestion.Any() && examQuestionCount == totalEnglishQuestion.Count())
                            {
                                examSetNo.Add(j);
                            }
                        }
                    }
                }
                else
                {
                    examSetNo = viewModel.UniqueSet;
                }
    
                List<ExamMcqQuestionSet> examQuestionSetList = new List<ExamMcqQuestionSet>();
                ExamMcqQuestionSet lastExamQuestionSet = _examMcqQuestionSetDao.LoadLastSetIdByExamId(exam.Id);
                int lastSetId = (lastExamQuestionSet == null) ? 0 : lastExamQuestionSet.SetId;
                int setId = lastSetId + 1;
                int generatingSetNoIncrement = 0;

                var questionList = _examMcqQuestionDao.LoadExamMcqQuestionList(exam, examSetNo);
                if (questionList == null || !questionList.Any())
                    throw new InvalidDataException("This exam has no question");

                while (generatingSetNoIncrement < viewModel.NumberOfSet)
                {
               
                    foreach (int uniqueSet in examSetNo)
                    {
                        int questionNo = 1;
                        var questionUniqueSetWiseList = questionList.Where(x => x.Exams.Id == exam.Id && x.UniqueSet==uniqueSet).ToList();
                        if (questionUniqueSetWiseList.Count() != examQuestionCount)
                        {
                           throw new InvalidDataException("This exam total question has mismatch.");
                        }
                        var randomQuestionList = RandomQuestionList(questionUniqueSetWiseList);
                        foreach (ExamMcqQuestion examMcqQuestion in randomQuestionList)
                        {
                            var tempQuestionOption = new McqQuestionViewModel()
                            {
                                CorrectAnswer = examMcqQuestion.CorrectAnswer,
                                IsShuffle = examMcqQuestion.IsShuffle,
                                OptionA = examMcqQuestion.OptionA,
                                OptionB = examMcqQuestion.OptionB,
                                OptionC = examMcqQuestion.OptionC,
                                OptionD = examMcqQuestion.OptionD,
                                OptionE = examMcqQuestion.OptionE,
                                QuestionText = examMcqQuestion.QuestionText,
                                Version = examMcqQuestion.Version
                            };

                            tempQuestionOption = examMcqQuestion.IsShuffle ? RandomQuestionOption(tempQuestionOption) : QuestionOption(tempQuestionOption);

                            var tempExamQuestionSet = new ExamMcqQuestionSet();
                            tempExamQuestionSet.Exams = exam;
                            tempExamQuestionSet.UniqueSet = examMcqQuestion.UniqueSet;
                            tempExamQuestionSet.ExamMcqQuestionSerial = examMcqQuestion.QuestionSerial;
                            tempExamQuestionSet.SetId = setId;
                            tempExamQuestionSet.SetNo = setId.ToString();
                            tempExamQuestionSet.QuestionSerial = questionNo;
                            tempExamQuestionSet.OptionA = tempQuestionOption.OptionA;
                            tempExamQuestionSet.OptionB = tempQuestionOption.OptionB;
                            tempExamQuestionSet.OptionC = tempQuestionOption.OptionC;
                            tempExamQuestionSet.OptionD = tempQuestionOption.OptionD;
                            tempExamQuestionSet.OptionE = tempQuestionOption.OptionE;
                            tempExamQuestionSet.CorrectAnswer = tempQuestionOption.CorrectAnswer;
                            tempExamQuestionSet.GeneratedBy = GetCurrentUserId();
                            tempExamQuestionSet.GeneratedDate = DateTime.Now;
                            tempExamQuestionSet.Status = ExamMcqQuestionSet.EntityStatus.Active;
                            tempExamQuestionSet.CreateBy = GetCurrentUserId();
                            tempExamQuestionSet.ModifyBy = GetCurrentUserId();
                            examQuestionSetList.Add(tempExamQuestionSet);
                            
                            questionNo++;
                        }
                        generatingSetNoIncrement++;
                        if (generatingSetNoIncrement == viewModel.NumberOfSet)
                            break;
                        setId++;
                    }
                }
                try
                {
                    using (transaction = Session.BeginTransaction())
                    {
                        if (examQuestionSetList.Any())
                        {
                            foreach (var examMcqQuestionSet in examQuestionSetList)
                            {
                                _examMcqQuestionSetDao.SaveOrUpdate(examMcqQuestionSet);
                            }
                        }

                        Session.Evict(exam);
                        var xz = _examsDao.LoadById(exam.Id);
                        xz.IsSetGenerationRunning = 0;
                        _examsDao.SaveOrUpdate(xz);

                        transaction.Commit();
                        return examQuestionSetList;
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    throw ex;
                }
                finally
                {
                    if (transaction != null && transaction.IsActive)
                        transaction.Rollback();
                }
            }
            catch (MessageException)
            {
                throw;
            }
            catch (EmptyFieldException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (DuplicateEntryException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                Session.Evict(exam);
                var xm = _examsDao.LoadById(viewModel.ExamId);
                if (xm != null && xm.IsSetGenerationRunning != 0)
                {
                    
                    try
                    {                        
                        using (transaction = Session.BeginTransaction())
                        {
                            xm.IsSetGenerationRunning = 0;
                            _examsDao.SaveOrUpdate(xm);
                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        throw ex;
                    }
                    finally
                    {
                        if (transaction != null && transaction.IsActive)
                            transaction.Rollback();
                    }
                }
                else
                {
                    if (transaction != null && transaction.IsActive)
                        transaction.Rollback();
                }
            }
        }

        public void Delete(IList<ExamMcqQuestionSet> setList)
        {
            ITransaction transaction = null;
            try
            {
                if (setList == null || !setList.Any())
                    throw new NullObjectException("Set list is Empty for this exam");

                var examId = setList.FirstOrDefault().Exams.Id;

                var setPrintList  = _examMcqQuestionSetPrintDao.LoadExamMcqQuestionSetPrintList(examId);
                if (setPrintList != null && setPrintList.Any())
                    throw new InvalidDataException("This exam mcq question set is already exists in mcq question set print");

                using (transaction = Session.BeginTransaction())
                {
                    foreach (var examMcqQuestionSet in setList)
                    {
                        examMcqQuestionSet.Status = ExamMcqQuestionSet.EntityStatus.Delete;
                        _examMcqQuestionSetDao.Delete(examMcqQuestionSet);
                    }
                    transaction.Commit();
                }
            }
            catch (MessageException)
            {
                throw;
            }
            catch (EmptyFieldException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (DuplicateEntryException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public ExamMcqQuestionSet LoadLastSetIdByExamId(long examId)
        {
            try
            {
                return _examMcqQuestionSetDao.LoadLastSetIdByExamId(examId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ExamMcqQuestionSet> LoadExamMcqQuestionSet(long examId, List<int> uniqueSet = null, int? examMcqQuestionSerial = null, int? setId = null)
        {
            try
            {
                return _examMcqQuestionSetDao.LoadExamMcqQuestionSet( examId, uniqueSet, examMcqQuestionSerial, setId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ExamMcqQuestionSetDto> LoadExamMcqQuestionSet(List<UserMenu> userMenu, int start,
            int length, string orderBy, string orderDir, string organization, string program, string session, string course,
            string uniqueSet, string exam)
        {
            try
            {
                List<long> authorizeOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organization != "" ? CommonHelper.ConvertToList(Convert.ToInt64(organization)) : null);
                List<long> authorizeProgramIdList = AuthHelper.LoadProgramIdList(userMenu, authorizeOrganizationIdList, null, program!=""?CommonHelper.ConvertToList(Convert.ToInt64(program)):null);
                List<long> authorizeBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authorizeOrganizationIdList,
                    authorizeProgramIdList);
                var authorizeSessionIdList = _sessionDao.LoadAuthorizedSession(authorizeProgramIdList,
                    authorizeBranchIdList,null,null);

                if (authorizeOrganizationIdList != null && authorizeOrganizationIdList.Any() && authorizeProgramIdList!=null && authorizeProgramIdList.Any()
                    && authorizeSessionIdList!=null && authorizeSessionIdList.Any())
                {
                    List<long> authSessionList;
                    if (session != "")
                        authSessionList = authorizeSessionIdList.Where(x => x.Id == Convert.ToInt64(session)).Select(x => x.Id).ToList();
                    else
                    {
                        authSessionList = authorizeSessionIdList.Select(x => x.Id).ToList();
                    }

                    return _examMcqQuestionSetDao.LoadExamMcqQuestionSet(authorizeOrganizationIdList, authorizeProgramIdList,authSessionList, start, length, orderBy,orderDir, course, uniqueSet, exam);
                }
                else return new List<ExamMcqQuestionSetDto>();


            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        
        #endregion

        #region List Loading Function

        public IList<IGrouping<int, ExamMcqQuestionSet>> LoadExamMcqQuestionSetList(long[] authorizeOrgIds, long[] authorizeProgramIds, long[] authorizeSessionIds,
            int start, int length, string orderBy, string orderDir, string organization, string program, string session, string course,
            string uniqueSet)
        {
            try
            {
                return _examMcqQuestionSetDao.LoadExamMcqQuestionSetList(authorizeOrgIds, authorizeProgramIds,
                    authorizeSessionIds, start, length, orderBy, orderDir, organization, program, session, course, uniqueSet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        

        public List<QuestionAnswerDto> LoadExamMcqQuestionSet(long examId, string code)
        {
            try
            {
                return _examMcqQuestionSetDao.LoadExamMcqQuestionSet(examId, code);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int GetExamMcqQuestionSetRowCount(List<UserMenu> userMenu,string organization, string program, string session, string course, string uniqueSet,string exam)
        {
            try
            {
                List<long> authorizeOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organization != "" ? CommonHelper.ConvertToList(Convert.ToInt64(organization)) : null);
                List<long> authorizeProgramIdList = AuthHelper.LoadProgramIdList(userMenu, authorizeOrganizationIdList, null, program!=""?CommonHelper.ConvertToList(Convert.ToInt64(program)):null);
                List<long> authorizeBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authorizeOrganizationIdList,
                    authorizeProgramIdList);
                var authorizeSessionIdList = _sessionDao.LoadAuthorizedSession(authorizeProgramIdList,
                    authorizeBranchIdList,null,null);

                if (authorizeOrganizationIdList != null && authorizeOrganizationIdList.Any() &&
                    authorizeProgramIdList != null && authorizeProgramIdList.Any()
                    && authorizeSessionIdList != null && authorizeSessionIdList.Any())
                {
                    List<long> authSessionList;
                    if (session != "")
                        authSessionList = authorizeSessionIdList.Where(x => x.Id == Convert.ToInt64(session)).Select(x => x.Id).ToList();
                    else
                    {
                        authSessionList = authorizeSessionIdList.Select(x => x.Id).ToList();
                    }
                    return _examMcqQuestionSetDao.GetExamMcqQuestionSetRowCount(authorizeOrganizationIdList, authorizeProgramIdList, authSessionList,
                        course, uniqueSet, exam);
                }
                return 0;


            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper Function
        
        private List<ExamMcqQuestion> RandomQuestionList(List<ExamMcqQuestion> questionList)
        {
            Random rng = new Random();
            List<ExamMcqQuestion> randomQuestionList = new List<ExamMcqQuestion>();
            int mainQuestionListLength = questionList.Count;

            if (mainQuestionListLength > 0)
            {
                List<long> randomShuffelQuestionIdList = new List<long>();
                while (true)
                {
                    if (questionList.Count == randomQuestionList.Count)
                        break;
                    Thread.Sleep(1);
                    int randIndex = rng.Next(0, questionList.Count * 100000) / 100000;
                    ExamMcqQuestion tempQuestion = questionList[randIndex];
                    //first Time
                    if (!randomShuffelQuestionIdList.Any())
                    {
                        //var tquestion = RandomQuestionOption(tempQuestion);
                        randomQuestionList.Add(tempQuestion);
                        randomShuffelQuestionIdList.Add(tempQuestion.Id);
                    }
                    // other time
                    else if (randomShuffelQuestionIdList.Any() && !randomShuffelQuestionIdList.Contains(tempQuestion.Id))
                    {
                        //var tquestion = RandomQuestionOption(tempQuestion);
                        randomQuestionList.Add(tempQuestion);
                        randomShuffelQuestionIdList.Add(tempQuestion.Id);
                    }
                }
            }

            return randomQuestionList;
        }

        private McqQuestionViewModel RandomQuestionOption(McqQuestionViewModel question)
        {
            Random rng = new Random();
            List<string> originalOptionList = new List<string>();
            List<string> correctOptionList = new List<string>();
            //int originalOptionListCount = 0;
            string randomQuestionCorrectOption = "";
            List<string> randomQuestionOptionList = new List<string>();
            if (!String.IsNullOrEmpty(question.OptionA))
                originalOptionList.Add("A");
            if (!String.IsNullOrEmpty(question.OptionB))
                originalOptionList.Add("B");
            if (!String.IsNullOrEmpty(question.OptionC))
                originalOptionList.Add("C");
            if (!String.IsNullOrEmpty(question.OptionD))
                originalOptionList.Add("D");
            if (!String.IsNullOrEmpty(question.OptionE))
                originalOptionList.Add("E");

            if (!String.IsNullOrEmpty(question.CorrectAnswer))
            {
                foreach (char c in question.CorrectAnswer)
                {
                    var toupper = char.ToUpper(c);
                    switch (toupper)
                    {
                        case 'A':
                            correctOptionList.Add("A");
                            break;
                        case 'B':
                            correctOptionList.Add("B");
                            break;
                        case 'C':
                            correctOptionList.Add("C");
                            break;
                        case 'D':
                            correctOptionList.Add("D");
                            break;
                        case 'E':
                            correctOptionList.Add("E");
                            break;
                    }
                }
            }

            if (originalOptionList.Any())
            {
                while (true)
                {
                    if (randomQuestionOptionList.Count == originalOptionList.Count)
                        break;
                    Thread.Sleep(1);

                    int randIndex = rng.Next(0, originalOptionList.Count * 100000) / 100000;
                    string tempQuestionOption = originalOptionList[randIndex];
                    //first Time
                    if (!randomQuestionOptionList.Any())
                    {
                        randomQuestionOptionList.Add(tempQuestionOption);
                    }
                    // other time
                    else if (randomQuestionOptionList.Any() && !randomQuestionOptionList.Contains(tempQuestionOption))
                    {
                        randomQuestionOptionList.Add(tempQuestionOption);
                    }
                }
            }
            if (randomQuestionOptionList.Any())
            {
                int index = 1;
                foreach (string s in randomQuestionOptionList)
                {
                    switch (index)
                    {
                        case 1:
                            question.OptionA = s;
                            if (correctOptionList.Contains(s))
                                randomQuestionCorrectOption += "A";
                            break;
                        case 2:
                            question.OptionB = s;
                            if (correctOptionList.Contains(s))
                                randomQuestionCorrectOption += "B";
                            break;
                        case 3:
                            question.OptionC = s;
                            if (correctOptionList.Contains(s))
                                randomQuestionCorrectOption += "C";
                            break;
                        case 4:
                            question.OptionD = s;
                            if (correctOptionList.Contains(s))
                                randomQuestionCorrectOption += "D";
                            break;
                        case 5:
                            question.OptionE = s;
                            if (correctOptionList.Contains(s))
                                randomQuestionCorrectOption += "E";
                            break;
                    }
                    index++;
                }
                question.CorrectAnswer = randomQuestionCorrectOption;
            }
            return question;
        }

        private McqQuestionViewModel QuestionOption(McqQuestionViewModel question)
        {
            List<string> originalOptionList = new List<string>();
            List<string> correctOptionList = new List<string>();
            string questionCorrectOption = "";
            if (!String.IsNullOrEmpty(question.OptionA))
                originalOptionList.Add("A");
            if (!String.IsNullOrEmpty(question.OptionB))
                originalOptionList.Add("B");
            if (!String.IsNullOrEmpty(question.OptionC))
                originalOptionList.Add("C");
            if (!String.IsNullOrEmpty(question.OptionD))
                originalOptionList.Add("D");
            if (!String.IsNullOrEmpty(question.OptionE))
                originalOptionList.Add("E");

            if (!String.IsNullOrEmpty(question.CorrectAnswer))
            {
                foreach (char c in question.CorrectAnswer)
                {
                    var toupper = char.ToUpper(c);
                    switch (toupper)
                    {
                        case 'A':
                            correctOptionList.Add("A");
                            break;
                        case 'B':
                            correctOptionList.Add("B");
                            break;
                        case 'C':
                            correctOptionList.Add("C");
                            break;
                        case 'D':
                            correctOptionList.Add("D");
                            break;
                        case 'E':
                            correctOptionList.Add("E");
                            break;
                    }
                }
            }
            if (originalOptionList.Any())
            {
                int index = 1;
                foreach (string s in originalOptionList)
                {
                    switch (index)
                    {
                        case 1:
                            question.OptionA = s;
                            if (correctOptionList.Contains(s))
                                questionCorrectOption += "A";
                            break;
                        case 2:
                            question.OptionB = s;
                            if (correctOptionList.Contains(s))
                                questionCorrectOption += "B";
                            break;
                        case 3:
                            question.OptionC = s;
                            if (correctOptionList.Contains(s))
                                questionCorrectOption += "C";
                            break;
                        case 4:
                            question.OptionD = s;
                            if (correctOptionList.Contains(s))
                                questionCorrectOption += "D";
                            break;
                        case 5:
                            question.OptionE = s;
                            if (correctOptionList.Contains(s))
                                questionCorrectOption += "E";
                            break;
                    }
                    index++;
                }
                question.CorrectAnswer = questionCorrectOption;
            }
            return question;
        }

        #endregion
    }
}
