﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public class TeacherVersionOfStudyPriorityMap : BaseClassMap<TeacherVersionOfStudyPriority, long>
    {
        public TeacherVersionOfStudyPriorityMap()
        {
            Table("TeacherVersionOfStudyPriority");
            LazyLoad();
            Map(x => x.Priority);
            Map(x => x.VersionOfStudy);
            References(x => x.Teacher).Column("TeacherId");
           
        }
    }
}
