﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using NHibernate.Mapping;
using UdvashERP.App_code;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.Services.Administration;
using UdvashERP.App_Start;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class DiscountController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly IOrganizationService _organizationService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly IDiscountService _discountService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly ICourseSubjectService _courseSubjectService;
        private readonly IDiscountDetailService _discountDetailService;
        private List<UserMenu> _userMenu;

        public DiscountController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _organizationService = new OrganizationService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _courseService = new CourseService(session);
                _discountService = new DiscountService(session);
                _userService = new UserService(session);
                _commonHelper = new CommonHelper();
                _courseSubjectService = new CourseSubjectService(session);
                _discountDetailService=new DiscountDetailService(session);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        #endregion

        #region Index/Manage Page

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ManageDiscount()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.Program = new SelectList(_programService.LoadAuthorizedProgram(_userMenu), "Id", "Name");
                ViewBag.Session = new SelectList(_sessionService.LoadAuthorizedSession(_userMenu), "Id", "Name");
                List<long> authorizeOrganization = AuthHelper.LoadOrganizationIdList(_userMenu);
                List<long> authorizeProgram = AuthHelper.LoadProgramIdList(_userMenu);
                ViewBag.Course = new SelectList(_courseService.LoadCourse(authorizeOrganization, authorizeProgram), "Id", "Name");
                ViewBag.StatusText = _commonHelper.GetStatus();
                return View("ManageDiscount");
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(e);
                _logger.Error(e);
                return View();
            }
        }

        #region Render Data Table

        [HttpPost]
        public ActionResult ManageDiscount(int draw, int start, int length, string organizationId, string programId, string sessionId,string courseId, string formatDate, string toDate, string amount, string status)
        {
            var data = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                
                #region OrderBy and Direction
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "organization.Name";
                            break;
                        case "1":
                            orderBy = "program.Name";
                            break;
                        case "2":
                            orderBy = "session.Name";
                            break;
                        case "4":
                            orderBy = "StartDate";
                            break;
                        case "5":
                            orderBy = "EndDate";
                            break;
                        case "6":
                            orderBy = "Amount";
                            break;
                        case "7":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                #endregion

                recordsTotal = _discountService.GetDiscountCount(orderBy, orderDir.ToUpper(), _userMenu, organizationId, programId, sessionId, courseId, formatDate, toDate, amount, status);
                recordsFiltered = recordsTotal;
                List<Discount> discountList = _discountService.LoadDiscount(start, length, orderBy, orderDir.ToUpper(), _userMenu, organizationId, programId, sessionId, courseId, formatDate, toDate,
                    amount, status).ToList();
                
                foreach (var discountObj in discountList)
                {
                    var courses = "";
                    var str = new List<string>();
                    str.Add(discountObj.Program.Organization.ShortName);
                    str.Add(discountObj.Program.Name);
                    str.Add(discountObj.Session.Name);
                    var ddList = _discountDetailService.GetAllDiscountDetailByDiscountId(discountObj.Id);
                    var ddListCount = ddList.Count;
                    var i = 0;
                    foreach (var dd in ddList)
                    {
                        i++;
                        var courseObj = _courseService.GetCourse(dd.Course.Id);
                        courses += courseObj.Name;
                        if (ddListCount > i)
                            courses += ", ";
                    }
                    str.Add(courses);
                    if (discountObj.StartDate != null) str.Add(discountObj.StartDate.Value.ToString("dd/MM/yyyy"));
                    if (discountObj.EndDate != null) str.Add(discountObj.EndDate.Value.ToString("dd/MM/yyyy"));
                    var displayAmount = Math.Ceiling(Convert.ToDecimal(discountObj.Amount));
                    str.Add(displayAmount.ToString());
                    str.Add(_commonHelper.GetSatus(discountObj.Status.ToString(CultureInfo.InvariantCulture).Trim()));
                    str.Add(LinkGenerator.GetGeneratedDetailsEditModelDeleteLink("Details", "Edit", "Discount", discountObj.Id, discountObj.Name));
                    data.Add(str);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        #endregion

        #endregion

        #region Operational Function

        #region Save Operation

        [HttpGet]
        public ActionResult AddDiscount()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return HttpNotFound();

            }
        }

        [HttpPost]
        public ActionResult AddDiscount(string discountObj)
        {
            try
            {
                var jjScriptSerializer = new JavaScriptSerializer();
                var discountViewModel = jjScriptSerializer.Deserialize<DiscountViewModel>(discountObj);
                var courseFees = GetTotalCourseFees(discountViewModel.DiscountObjProperty);
                if (courseFees == -1 || courseFees <= Convert.ToDecimal(discountViewModel.DiscountAmount.Trim()))
                    return Json(new Response(false, String.Format("Total Course Fees Is {0} So Discount Amount Must Be smaller than that ", courseFees)));
                try
                {
                    var discount = new Discount();
                    var program = _programService.GetProgram(Convert.ToInt64(discountViewModel.ProgramId));
                    var session = _sessionService.LoadById(Convert.ToInt64(discountViewModel.SessionId));
                    var discountAmount = Convert.ToDecimal(discountViewModel.DiscountAmount);
                    discount.Program = program;
                    discount.Session = session;
                    discount.Amount = discountAmount;
                    discount.Status = Discount.EntityStatus.Active;
                    discount.StartDate = discountViewModel.StartTime;
                    discount.EndDate = discountViewModel.EndTime;
                    if (discount.StartDate <= discount.EndDate)
                    {
                        foreach (var discountDetailsObj in discountViewModel.DiscountObjProperty.Select(disCountDetails => new DiscountDetail
                        {
                            Course = _courseService.GetCourse(Convert.ToInt64(disCountDetails.courseId)),
                            MinSubject = Convert.ToInt32(disCountDetails.minSub),
                            Discount = discount,
                            Status = DiscountDetail.EntityStatus.Active,
                            CreationDate = DateTime.Now,
                            ModificationDate = DateTime.Now
                        }))
                        {
                            discount.DiscountDetails.Add(discountDetailsObj);
                        }
                        bool isDiscountSave = _discountService.IsSave(discount);
                        if (isDiscountSave)
                        {
                            return Json(new Response(true, "Discount Add Successfully"));
                        }
                        return Json(new Response(false, "Discount Not Added"));
                    }
                    return Json(new Response(false, "End datetime must be Greater than Start date time"));
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(e)));

                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new Response(false, WebHelper.SetExceptionMessage(e)));
            }
        }

        #endregion

        #region Update Operation

        [HttpGet]
        public ActionResult Edit(long id)
        {
            try
            {
                long dId;
                bool isNum = long.TryParse(id.ToString(), out dId);
                if (!isNum)
                {
                    return HttpNotFound();
                }
                Discount discount = _discountService.GetDiscount(id);
                IList<Course> courseList = _courseService.LoadCourse(discount.Program.Id, discount.Session.Id);
                ViewBag.assignedCourseList = courseList;
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                ViewData["disId"] = id;
                return View(discount);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult Edit(string discountObj, string disId)
        {
            try
            {
                if (!String.IsNullOrEmpty(disId))
                {
                    disId = disId.Trim();
                    Discount oldDiscount = _discountService.GetDiscount(Convert.ToInt64(disId));
                    if (DateTime.Now <= oldDiscount.EndDate)
                    {
                        var jjScriptSerializer = new JavaScriptSerializer();
                        var discountViewModel = jjScriptSerializer.Deserialize<DiscountViewModel>(discountObj);
                        var courseFees = GetTotalCourseFees(discountViewModel.DiscountObjProperty);
                        if (courseFees != -1 && courseFees > Convert.ToDecimal(discountViewModel.DiscountAmount.Trim()))
                        {
                            try
                            {
                                var discount = new Discount();
                                var discountAmount = Convert.ToDecimal(discountViewModel.DiscountAmount);
                                discount.Amount = discountAmount;
                                if (discountViewModel.StartTime <= discountViewModel.EndTime)
                                {
                                    discount.StartDate = discountViewModel.StartTime;
                                    discount.EndDate = discountViewModel.EndTime;
                                    discount.Status = Convert.ToInt32(discountViewModel.Status.Trim());

                                    oldDiscount.Status = discount.Status;
                                    oldDiscount.Amount = discount.Amount;
                                    oldDiscount.StartDate = discount.StartDate;
                                    oldDiscount.EndDate = discount.EndDate;
                                    foreach (var disCountDetails in discountViewModel.DiscountObjProperty)
                                    {
                                        var flag = 0;
                                        var ii = 0;
                                        foreach (var oldDDetails in oldDiscount.DiscountDetails)
                                        {
                                            ii++;
                                            if (oldDDetails.Discount.Id == Convert.ToInt64(disId) && oldDDetails.Course.Id == Convert.ToInt64(disCountDetails.courseId))
                                            {
                                                oldDDetails.Course = _courseService.GetCourse(Convert.ToInt64(disCountDetails.courseId));
                                                oldDDetails.MinSubject = Convert.ToInt32(disCountDetails.minSub);
                                                oldDDetails.Discount = oldDiscount;
                                                oldDDetails.Status = DiscountDetail.EntityStatus.Active;
                                                oldDDetails.CreationDate = DateTime.Now;
                                                oldDDetails.ModificationDate = DateTime.Now;
                                                oldDDetails.Status = oldDiscount.Status;
                                                discount.DiscountDetails.Add(oldDDetails);
                                                flag = 1;
                                            }
                                            if (flag == 1) { break; }
                                            if (flag == 0 && ii == oldDiscount.DiscountDetails.Count)
                                            {
                                                var dd = new DiscountDetail
                                                {
                                                    Course =
                                                        _courseService.GetCourse(
                                                            Convert.ToInt64(disCountDetails.courseId)),
                                                    MinSubject = Convert.ToInt32(disCountDetails.minSub),
                                                    Discount = oldDiscount,
                                                    Status = DiscountDetail.EntityStatus.Active,
                                                    CreationDate = DateTime.Now,
                                                    ModificationDate = DateTime.Now
                                                };
                                                dd.Status = oldDiscount.Status;
                                                discount.DiscountDetails.Add(dd);
                                            }
                                        }
                                    }
                                    bool isDiscountUpdate = _discountService.IsUpdate(oldDiscount, discount);
                                    if (isDiscountUpdate)
                                    {
                                        return Json(new Response(true, "Discount Update Successfully"));
                                    }
                                    return Json(new Response(false, "Discount Not Edited"));
                                }
                                return Json(new Response(false, "End datetime must be Greater than Start date time"));
                            }
                            catch (Exception e)
                            {
                                _logger.Error(e);
                                return Json(new Response(false, WebHelper.SetExceptionMessage(e)));
                            }
                        }
                        return Json(new Response(false, String.Format("Total Course Fees Is {0} So Discount Amount Must Be smaller than that ", courseFees)));
                    }
                    return Json(new Response(false, "You Can't Edit This Discount !! Discount Time Over"));
                }
                return Json(new Response(false, "Invalid Discount Id Found"));

            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new Response(false, WebHelper.SetExceptionMessage(e)));
            }

        }

        #endregion

        #region Delete Operation

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                if (id == 0)
                {
                    return HttpNotFound();
                }
                var discountObj = _discountService.GetDiscount(id);
                _discountService.Delete(discountObj);
                return Json(new Response(true, "Discount Delete Successfully !"));
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return Json(new Response(false, WebHelper.SetExceptionMessage(e)));
            }
        }

        #endregion

        #region Details Operation

        public ActionResult Details(long id)
        {
            try
            {
                long cId;
                bool isNum = long.TryParse(id.ToString(), out cId);
                if (!isNum)
                {
                    return HttpNotFound();
                }
                var discount = _discountService.GetDiscount(id);
                if (discount != null)
                {
                    discount.CreateByText = _userService.GetUserNameByAspNetUserId(discount.CreateBy);
                    discount.ModifyByText = _userService.GetUserNameByAspNetUserId(discount.ModifyBy);
                }
                return View(discount);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(e);
                _logger.Error(e);
                return View();
            }
        }

        #endregion

        #endregion

        #region Helper Function

        [HttpPost]
        public ActionResult CourseListGenerate(string programId = "", string sessionId = "")
        {
            try
            {
                bool isAjaxRequest = !String.IsNullOrEmpty(programId) && !String.IsNullOrEmpty(sessionId);
                if (isAjaxRequest)
                {
                    IList<Course> courseList = _courseService.LoadCourse(Convert.ToInt64(programId), Convert.ToInt64(sessionId));
                    return PartialView("Partial/_CourseList", courseList);
                }
                return PartialView("Partial/_CourseList", new List<Course>());
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return PartialView("Partial/_CourseList", new List<Course>());
            }
        }

        private decimal GetTotalCourseFees(IEnumerable<CourseDiscountViewModel> discountCourseList)
        {
            try
            {
                decimal fees = 0;
                foreach (var courseDiscountViewModel in discountCourseList)
                {
                    IList<CourseSubject> courseSubjectList = _courseSubjectService.LoadCourseSubject(Convert.ToInt64(courseDiscountViewModel.courseId));
                    fees += (from courseSubject in courseSubjectList where courseSubject != null where true select (decimal)courseSubject.Payment).Sum();
                }
                return fees;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return -1;
            }
        }

        #endregion
    }
}