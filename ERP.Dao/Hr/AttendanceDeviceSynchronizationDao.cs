﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq.Visitors;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IAttendanceDeviceSynchronizationDao : IBaseDao<AttendanceDeviceRawData, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<AttendanceDeviceRawData> AttendanceDeviceSyncList(int pin, DateTime punchTime, Organization organization);
        IList<AttendanceDeviceRawData> LoadByPinAndDateRange(int pin, DateTime orgStartTime, DateTime orgEndTime, bool status = true);
        //List<AttendanceDeviceRawData> AttendanceReportByPinDate(int pin, DateTime attendanceDate, DateTime organizationStartTime);

        #endregion

        #region Others Function
        bool CheckDuplicate(int pin,DateTime punchTime);
        TeamMmberAttendanceAdjustmentInfo GetTeamMemberAttendanceAdjustmentInfo(DateTime attendanceDate, int pin);
        #endregion

        #region Helper Function

        #endregion

        
    }
    public class AttendanceDeviceSynchronizationDao : BaseDao<AttendanceDeviceRawData, long>, IAttendanceDeviceSynchronizationDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<AttendanceDeviceRawData> LoadByPinAndDateRange(int pin, DateTime orgStartTime, DateTime orgEndTime,bool status=true)
        {
            ICriteria criteria = Session.CreateCriteria<AttendanceDeviceRawData>();
            if (status)
                criteria.Add(Restrictions.Eq("Status", AttendanceDeviceRawData.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Pin", pin))
            .Add(Restrictions.Ge("PunchTime", orgStartTime))
            .Add(Restrictions.Le("PunchTime", orgEndTime));

            return criteria.List<AttendanceDeviceRawData>();
        }

        //public List<AttendanceDeviceRawData> AttendanceReportByPinDate(int pin, DateTime attendanceDate, DateTime organizationStartTime)
        //{
        //    throw new NotImplementedException();
        //}

        #endregion

        #region Others Function

        public IList<AttendanceDeviceRawData> AttendanceDeviceSyncList(int pin, DateTime punchTime, Organization organization)
        {
            var orgAttendanceStartTime = organization.AttendanceStartTime.ToString("H:mm");
            var date = punchTime.Date;
            var time = punchTime.ToString("H:mm");
            var pTime = DateTime.Parse("2001-01-01 " + " " + time);
            var after12AmCheck = DateTime.Parse("2000-01-01 00:00:00.000");
            if (pTime >= after12AmCheck && pTime < DateTime.Parse("2001-01-01 " + " " + orgAttendanceStartTime))
            {
                date = punchTime.Date.AddDays(-1);
            }

            var startTime = DateTime.Parse(date.ToString("yyyy-MM-dd") + " " + orgAttendanceStartTime);
            var endTime = startTime.AddHours(23).AddMinutes(59).AddSeconds(59);
           
            ICriteria criteria =
                Session.CreateCriteria<AttendanceDeviceRawData>()
                    .Add(Restrictions.Eq("Status", AttendanceDeviceRawData.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Pin", pin));
            criteria.Add(Restrictions.Ge("PunchTime", startTime))
                .Add(Restrictions.Le("PunchTime", endTime));
            //criteria.Add(Expression.Like(Projections.Cast(NHibernateUtil.String, Projections.Property("PunchTime")), Convert.ToString(punchTime), MatchMode.Anywhere));
            var results = criteria.List<AttendanceDeviceRawData>().ToList();
            return results.Count > 0 ? results : null;
            //return results;
        }

        public bool CheckDuplicate(int pin, DateTime punchTime)
        {
            var rowList = Session.QueryOver<AttendanceDeviceRawData>()
                    .Where(x => x.Pin == pin && x.PunchTime == punchTime && x.Status == AttendanceDeviceRawData.EntityStatus.Active)
                    .RowCount();
            if (rowList < 1)
                return false;
            return true;
        }

        public TeamMmberAttendanceAdjustmentInfo GetTeamMemberAttendanceAdjustmentInfo(DateTime attendanceDate, int pin)
        {
            string query = @"Select t.Id as TeamMemberId
	                            ,t.Pin as Pin
	                            ,hw.ApprovalType as HolyDayApprovalType
	                            ,nw.ApprovalType as NightWorkApprovalType
	                            ,(((DatePart(HOUR,ot.ApprovedTime) * 60) + DatePart(MINUTE, ot.ApprovedTime))) as OverTimeApproveTime
	                            ,da.DayOffStatus as DayOffStatus
	                            from [dbo].[HR_TeamMember] as t
                            left join [dbo].[HR_HolidayWork] as hw ON hw.TeamMemberId = t.Id 
			                            -- AND  hw.ApprovalType IS NOT NULL 
			                            AND hw.HolidayWorkDate= '" + attendanceDate + @"'
                            left join [dbo].[HR_NightWork] as nw ON nw.TeamMemberId = t.Id 
			                            -- AND  nw.ApprovalType IS NOT NULL 
			                            AND nw.NightWorkDate = '" + attendanceDate + @"'
                            left join [dbo].[HR_Overtime] as ot ON ot.TeamMemberId = t.Id 
			                            -- AND  nw.ApprovalType IS NOT NULL 
			                            AND ot.OverTimeDate = '" + attendanceDate + @"'
                            left join [dbo].[HR_DayOffAdjustment] as da ON da.TeamMemberId = t.Id 
			                            -- AND  nw.ApprovalType IS NOT NULL 
			                            AND da.InsteadOfDate = '" +attendanceDate+@"'
                            Where t.Pin = '"+pin.ToString()+@"' AND t.Status!="+TeamMember.EntityStatus.Delete;

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMmberAttendanceAdjustmentInfo>());
            iQuery.SetTimeout(5000);
           
            return iQuery.UniqueResult<TeamMmberAttendanceAdjustmentInfo>(); ;
        }

        #endregion

        #region Helper Function

        #endregion
        
    }

}
