﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.App_code;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("UInventory")]
    public class GoodsTransferController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IOrganizationService _organizationService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly IItemGroupService _itemGroupService;
        private readonly ItemService _itemService;
        private readonly ICurrentStockSummaryService _currentStockSummaryService;
        private readonly ICommonHelper _commonHelper;
        private readonly IBranchService _branchService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IGoodsTransferService _goodsTransferService;
        private List<UserMenu> _userMenus;

        public GoodsTransferController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _organizationService = new OrganizationService(session);
            _programBranchSessionService = new ProgramBranchSessionService(session);
            _itemService = new ItemService(session);
            _itemGroupService = new ItemGroupService(session);
            _currentStockSummaryService = new CurrentStockSummaryService(session);
            _commonHelper = new CommonHelper();
            _branchService = new BranchService(session);
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _goodsTransferService = new GoodsTransferService(session);
        }


        #endregion

        #region Operational Function

        public ActionResult Index()
        {
            try
            {
                InitIndexView();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        #region Render Data Table

        [HttpPost]
        public JsonResult GoodsTransferList(int draw, int start, int length, long? organizationFromId, long? branchFromId, long? organizationToId, long? branchToId, string itemName, string requiredDate)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            bool isSuccess = false;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "OrgFromShortName";
                            break;
                        case "1":
                            orderBy = "OrgToShortName";
                            break;
                        case "2":
                            orderBy = "BranchFromName";
                            break;
                        case "3":
                            orderBy = "BranchToName";
                            break;
                        case "4":
                            orderBy = "ItemName";
                            break;
                        case "5":
                            orderBy = "TransferDate";
                            break;
                        case "6":
                            orderBy = "TransferNo";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion


                _userMenus = (List<UserMenu>)ViewBag.UserMenu;

                recordsTotal = _goodsTransferService.GetGoodsTransferRowCount(_userMenus, organizationFromId, branchFromId, organizationToId, branchToId, itemName, requiredDate);
                recordsFiltered = recordsTotal;
                IList<GoodsTransferDto> goodsTransferList = _goodsTransferService.LoadGoodsTransfer(_userMenus, start, length, orderBy, orderDir, organizationFromId, branchFromId, organizationToId, branchToId, itemName, requiredDate).ToList();
                foreach (var goodsTransfer in goodsTransferList)
                {
                    var str = new List<string>
                    {
                        goodsTransfer.OrgFromShortName,
                        goodsTransfer.BranchFromName,
                        goodsTransfer.OrgToShortName,
                        goodsTransfer.BranchToName,
                        goodsTransfer.ItemName,
                        goodsTransfer.TransferDate.ToString("MMM dd, yyyy"),
                        goodsTransfer.TransferNo,
                        LinkGenerator.GetDetailsLink("Details", "GoodsTransfer", goodsTransfer.TransferId)
                    };
                    getData.Add(str);
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        #endregion

        public ActionResult GoodsTransfers()
        {
            InitCreateView(null);
            return View(new GoodsTransferViewModel());
        }

        [HttpPost]
        public ActionResult GoodsTransfers(GoodsTransferViewModel goodsTransferViewModel)
        {
            GoodsTransfer goodsTransfer = new GoodsTransfer();
            IList<GoodsTransferDetails> goodsTransferDetailList = new List<GoodsTransferDetails>();
            try
            {
                _userMenus = (List<UserMenu>)ViewBag.UserMenu;

                if (!goodsTransferViewModel.GoodsTransferDetailsViewModelList.Any())
                {
                    throw new InvalidDataException("One or more goods details required for transfer");
                }

                #region Get Authorize Branch

                long branchFromId = 0;
                long branchToId = 0;

                List<long> authBranchFrom = AuthHelper.LoadBranchIdList(_userMenus, null, null, _commonHelper.ConvertIdToList(goodsTransferViewModel.BranchFromId));
                List<long> authBranchTo = AuthHelper.LoadBranchIdList(_userMenus, null, null, _commonHelper.ConvertIdToList(goodsTransferViewModel.BranchToId));

                if (authBranchFrom.Any())
                    branchFromId = authBranchFrom[0];

                if (authBranchTo.Any())
                    branchToId = authBranchTo[0];

                Branch branchFrom = _branchService.GetBranch(branchFromId);
                Branch branchTo = _branchService.GetBranch(branchToId);

                //if (branchFrom == branchTo)
                //{
                //    throw new MessageException("You can't transfer goods in the same branch");
                //}


                //if (branchFrom.IsCorporate == false && branchTo.IsCorporate)
                //{
                //    throw new MessageException("You can't transfer goods from branch to corporate branch");
                //}


                #endregion

                #region Assign From View Model To Original Model

                goodsTransfer.BranchFrom = branchFrom;
                goodsTransfer.BranchTo = branchTo;


                goodsTransfer.Remarks = goodsTransferViewModel.Remarks;

                foreach (var viewGoodsTransferDetails in goodsTransferViewModel.GoodsTransferDetailsViewModelList)
                {
                    GoodsTransferDetails goodsTransferDetails = new GoodsTransferDetails();
                    goodsTransferDetails.GoodsTransfer = goodsTransfer;
                    goodsTransferDetails.Item = _itemService.LoadById(viewGoodsTransferDetails.ItemId);
                    goodsTransferDetails.TransferQuantity = viewGoodsTransferDetails.TransferQuantity;

                    if (!String.IsNullOrEmpty(viewGoodsTransferDetails.ProgramSessionId) && viewGoodsTransferDetails.ProgramSessionId != "0")
                    {
                        var spltValue = viewGoodsTransferDetails.ProgramSessionId.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                        goodsTransferDetails.Program = _programService.GetProgram(Convert.ToInt64(spltValue[0]));
                        goodsTransferDetails.Session = _sessionService.LoadById(Convert.ToInt64(spltValue[1]));
                    }
                    else
                    {
                        goodsTransferDetails.Program = null;
                        goodsTransferDetails.Session = null;
                    }

                    goodsTransferDetailList.Add(goodsTransferDetails);
                }

                goodsTransfer.GoodsTransferDetails = goodsTransferDetailList;

                #endregion

                _goodsTransferService.SaveOrUpdate(goodsTransfer);

                //return Json(new Response(true, "Success"));
                //return Json(new { IsSuccess = true, Message = "Goods successfully Transferred." });
            }
            catch (MessageException ex)
            {
                //ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, ex.Message));
            }

            catch (InvalidDataException ex)
            {
                //ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
            //InitCreateView(goodsTransferViewModel);

            //return RedirectToAction("Details", new { id = goodsTransfer.Id });
            string detailsId = goodsTransfer.Id.ToString();
            return Json(new Response(true, "Goods is successfully transfered", detailsId));
            //string Msg = "Goods is successfully transfered";
            //long Idx = goodsTransfer.Id;
            //return Json(new { Idx, Msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(long id)
        {
            List<GoodsTransferDto> goodsTransferDto;
            try
            {
                goodsTransferDto = _goodsTransferService.GetGoodsTransfer(id);

                if (goodsTransferDto == null)
                    goodsTransferDto = new List<GoodsTransferDto>();
                ViewBag.GoodsTransferDetails = goodsTransferDto;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
            return View();
        }

        #endregion

        #region Ajax Request

        [HttpGet]
        public ActionResult NewGoodsDetails(int index)
        {
            try
            {
                ViewBag.ItemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
                ViewBag.ItemList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.ProgramSessionList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.Index = index;
                return PartialView("_partial/_NewGoodsDetails");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public ActionResult LoadItemGroup()
        {
            if (Request.IsAjaxRequest())
            {

                try
                {
                    var itemGroup = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
                    ViewBag.ItemGroupList = itemGroup;
                    return Json(new { returnResult = itemGroup, IsSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new { returnResult = new List<SelectList>(), IsSuccess = false });
                }
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult LoadItems(long fromOrganizationId, long toOrganizationId, long[] itemGroupId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (itemGroupId.Count() <= 0)
                        return Json(new Response(false, "Invalid Item Group !"));
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var itemSelectList = new SelectList(_itemService.LoadItem(userMenu, fromOrganizationId, toOrganizationId, itemGroupId), "Id", "Name");
                    return Json(new { returnResult = itemSelectList, IsSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new { returnResult = new List<SelectList>(), IsSuccess = false });
                }
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult LoadProgramSession(long branchIdFrom, long branchIdTo, long itemId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                    var programSessionList = _programBranchSessionService.LoadProgramSession(_userMenus, branchIdFrom, branchIdTo, itemId).ToList();
                    var list = new MultiSelectList(programSessionList, "ProgramAndSessionId", "ProgramAndSessionName");
                    return Json(new { programSessionList = list, IsSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult GetStockQuantity(string programSession, long branchIdFrom, long itemId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                    long? programId;
                    long? sessionId;


                    if (programSession == "0" || programSession == "")
                    {
                        programId = null;
                        sessionId = null;

                    }
                    else
                    {
                        var spltValue = programSession.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                        programId = Convert.ToInt64(spltValue[0]);
                        sessionId = Convert.ToInt64(spltValue[1]);
                    }
                    var stockQty = _currentStockSummaryService.GetCurrentStockQuantity(_userMenus, itemId, branchIdFrom, programId, sessionId);

                    return Json(new { stockQty = stockQty, IsSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                }
            }
            else
            {
                return HttpNotFound();
            }
        }


        #endregion

        #region Helper

        private void InitIndexView()
        {
            _userMenus = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationFromList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenus), "Id", "ShortName");
            ViewBag.BranchFromList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.OrganizationToList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenus), "Id", "ShortName");

            ViewBag.BranchToList = new SelectList(string.Empty, "Value", "Text");
        }

        private void InitCreateView(GoodsTransferViewModel goodsTransferViewModel)
        {
            try
            {
                _userMenus = (List<UserMenu>)ViewBag.UserMenu;
                var organizationList = _organizationService.LoadAuthorizedOrganization(_userMenus);
                ViewBag.BranchFromList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.BranchToList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.ItemList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.ProgramSessionList = new SelectList(string.Empty, "Value", "Text");

                if (goodsTransferViewModel == null)
                {
                    ViewBag.OrganizationFromList = new SelectList(organizationList, "Id", "ShortName");
                    ViewBag.OrganizationToList = new SelectList(organizationList, "Id", "ShortName");
                    ViewBag.ItemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
                }
                else
                {
                    ViewBag.OrganizationFromList = new SelectList(organizationList, "Id", "ShortName", goodsTransferViewModel.OrganizationFromId);
                    ViewBag.OrganizationToList = new SelectList(organizationList, "Id", "ShortName", goodsTransferViewModel.OrganizationToId);

                    if (goodsTransferViewModel.OrganizationFromId > 0)
                    {
                        ViewBag.BranchFromList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenus, _commonHelper.ConvertIdToList(goodsTransferViewModel.OrganizationFromId)), "Id", "Name", goodsTransferViewModel.BranchFromId);
                    }

                    if (goodsTransferViewModel.OrganizationToId > 0)
                    {
                        ViewBag.BranchToList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenus, _commonHelper.ConvertIdToList(goodsTransferViewModel.OrganizationToId)), "Id", "Name", goodsTransferViewModel.BranchToId);
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

            }
        }

        #endregion

        #region Reports

        private void InitReportView()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var authorgList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            ViewBag.OrganizationFromList = authorgList;
            ViewBag.OrganizationToList = authorgList;
            ViewBag.BranchFromList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.BranchToList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.itemGroupList = new SelectList(_itemGroupService.LoadItemGroup(), "Id", "Name");
            ViewBag.itemList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.ReportType = new SelectList(_commonHelper.LoadEmumToDictionary<ReportType>(), "Key", "Value");

        }

        public ActionResult GoodsTransferReport()
        {
            InitReportView();

            return View();
        }

        [HttpPost]
        public ActionResult GoodsTransferReport(GoodsTransferReportFormViewModel reportViewModel, long[] itemId)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _userMenus = (List<UserMenu>)ViewBag.UserMenu;

                    if (reportViewModel.ReportType == (int)ReportType.BranchWise)
                    {
                       // List<long> authOrganizationFrom = AuthHelper.LoadOrganizationIdList(_userMenus,_commonHelper.ConvertIdToList(reportViewModel.OrganizationId));
                        List<long> authOrganizationTo = AuthHelper.LoadOrganizationIdList(_userMenus, _commonHelper.ConvertIdToList(reportViewModel.OrganizationToId));

                        //List<long> authBranchFrom = AuthHelper.LoadBranchIdList(_userMenus, authOrganizationFrom, null,(reportViewModel.BranchId).ToList());
                        List<long> authBranchTo = AuthHelper.LoadBranchIdList(_userMenus, authOrganizationTo, null, reportViewModel.BranchToId == null ? null : (reportViewModel.BranchToId).ToList());

                        IList<string> branchNameList = _branchService.LoadBranch(authOrganizationTo,null,null,authBranchTo,null,null).Select(x => x.Name).ToList();
                  
    
                        ViewBag.BranchNameList = branchNameList;

                        ViewBag.ReportTitle = " Branch Wise Goods Transfer Report";
                        return PartialView("_partial/_TransferReportByBranch", reportViewModel);
                    }
                    else
                    {

                        List<long> authOrganizationFrom = AuthHelper.LoadOrganizationIdList(_userMenus, _commonHelper.ConvertIdToList(reportViewModel.OrganizationId));
                        List<long> authOrganizationTo = AuthHelper.LoadOrganizationIdList(_userMenus, _commonHelper.ConvertIdToList(reportViewModel.OrganizationToId));

                        List<long> authBranchFrom = AuthHelper.LoadBranchIdList(_userMenus, authOrganizationFrom, null, (reportViewModel.BranchId).ToList());
                        List<long> authBranchTo = AuthHelper.LoadBranchIdList(_userMenus, authOrganizationTo, null, reportViewModel.BranchToId == null ? null : (reportViewModel.BranchToId).ToList());
                        
                        IList<string> programNameList=new List<string>();

                        var programSessionList = _programBranchSessionService.LoadProgramSession(_userMenus, authOrganizationTo, authBranchTo, itemId==null ? null : itemId.ToList());

                        foreach (var programSession in programSessionList)
                        {
                            string programSessionConcate = programSession.ProgramShortName + " " +programSession.SessionName;
                            programNameList.Add(programSessionConcate);
                        }
                        programNameList.Insert(0, "N/A");
                        ViewBag.ProgramNameList = programNameList;
                        ViewBag.ReportTitle = "Program Wise Goods Transfer Report";
                        return PartialView("_partial/_TransferReportByProgram", reportViewModel);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }

            return RedirectToAction("GoodsTransferReport");
        }

        [HttpPost]
        public ActionResult TransferReportByBranch(int draw, int start, int length, long organizationId, long branchId, long organizationToId, long[] branchToId, long[] itemGroupId, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            bool isSuccess = false;
            _userMenus = (List<UserMenu>)ViewBag.UserMenu;

            try
            {
                recordsTotal = _goodsTransferService.GetGoodsTransferByBranchRowCount(_userMenus, organizationId, branchId, organizationToId, branchToId, itemGroupId, itemId, dateFrom, dateTo);
                recordsFiltered = recordsTotal;

                var returnObjects = _goodsTransferService.LoadGoodsTransferByBranch(_userMenus, start, length, organizationId, branchId, organizationToId, branchToId, itemGroupId, itemId, dateFrom, dateTo);

                int sl = start + 1;

                foreach (var x in returnObjects)
                {
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    foreach (var val in x)
                    {
                        if (val == null)
                            str.Add("N/A");
                        else
                            str.Add(val.ToString());
                    }
                    getData.Add(str);
                    sl++;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        [HttpPost]
        public ActionResult TransferReportByProgram(int draw, int start, int length, long organizationId, long branchId, long organizationToId, long[] branchToId, long[] itemGroupId, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            bool isSuccess = false;
            _userMenus = (List<UserMenu>)ViewBag.UserMenu;

            try
            {
                recordsTotal = _goodsTransferService.GetGoodsTransferByProgramRowCount(_userMenus, organizationId, branchId, organizationToId, branchToId, itemGroupId, itemId, dateFrom, dateTo);
                recordsFiltered = recordsTotal;

                var returnObjects = _goodsTransferService.LoadGoodsTransferByProgram(_userMenus, start, length, organizationId, branchId, organizationToId, branchToId, itemGroupId, itemId, dateFrom, dateTo);

                int sl = start + 1;

                foreach (var x in returnObjects)
                {
                    var str = new List<string>();
                    str.Add(sl.ToString());
                    foreach (var val in x)
                    {
                        str.Add(val.ToString());
                    }
                    getData.Add(str);
                    sl++;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        #endregion
    }
}