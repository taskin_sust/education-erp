﻿using System;
using System.Collections.Generic;

namespace UdvashERP.Areas.Student.Models
{
    public class AttendanceReports
    {
        public string ReportHeadProgramSession { get; set; }
        public string BranchName { get; set; }
        public string CampusName { get; set; }
        public string BatchDays { get; set; }
        public string BatchName { get; set; }
        public string CourseType { get; set; }
        public string CourseName { get; set; }
        public string DateRangeText { get { return StartDate.ToString("dd-mm-yyyy") + " to " + EndDate.ToString("dd-mm-yyyy"); } }
        public string Subject { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<String> ReportTableColumnNameList { get; set; }
        public IList<IList<string>> ReportTableRowsValuesList { get; set; }

        public AttendanceReports()
        {
            ReportHeadProgramSession = "";
            BranchName = "";
            CampusName = "";
            BatchDays = "";
            BatchName = "";
            CourseType = "";
            CourseName = "";
            Subject = "";
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
        }
    }

    public class StudentInfoDetails
    {
        public string ProgramRoll { get; set; }
        public string StudentName { get; set; }
        public string BranchName { get; set; }
        public string BatchName { get; set; }
        public string FatherName { get; set; }

        public string FatherOccupation { get; set; }
        public string MotherOccupation { get; set; }
        public string MotherName { get; set; }
        public string MobilePersonal { get; set; }
        public string MobileGuardian { get; set; }
        public string SchoolCollegeName { get; set; }
        public string RegistrationNo { get; set; }
        public string Email { get; set; }

        public int ClassHeld { get; set; }
        public int ClassAttend { get; set; }
        public int RegistratedPresent { get; set; }
        public int RegisteredAbscent { get; set; }
        public int UnregisteredPresent { get; set; }
        public int UnregisteredAbsent { get; set; }

    }
}