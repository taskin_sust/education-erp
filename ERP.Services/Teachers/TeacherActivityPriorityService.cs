﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherActivityPriorityService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<TeacherActivityPriority> LoadTeacherActivityPriority(long id);
        #endregion

        #region Others Function

        #endregion
    }
    public class TeacherActivityPriorityService : BaseService, ITeacherActivityPriorityService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly TeacherActivityPriorityDao _teacherActivityPriorityDao;
        public TeacherActivityPriorityService(ISession session)
        {
            Session = session;
            _teacherActivityPriorityDao = new TeacherActivityPriorityDao() { Session = session };

        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<TeacherActivityPriority> LoadTeacherActivityPriority(long id)
        {
            try
            {
               return _teacherActivityPriorityDao.LoadTeacherActivityPriority(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function

        #endregion
    }
}
