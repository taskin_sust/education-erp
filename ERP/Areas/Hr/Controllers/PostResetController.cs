﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Hr.AttendanceSummaryServices;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class PostResetController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICampusService _campusService;
        private readonly IBranchService _branchService;
        private readonly ITeamMemberService _hrMemberService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IDepartmentService _departmentService;
        private readonly IDesignationService _designationService;
        private readonly IAttendanceSummaryService _attendanceSummaryService;
        private readonly IShiftService _shiftService;
        private readonly IShiftWeekendHistoryService _shiftWeekendHistoryService;
        private readonly ISalaryHistoryService _hrSalaryHistoryService;
        private readonly IAllowanceSheetService _allowanceSheetService;
        private List<UserMenu> _userMenu;
        public PostResetController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _campusService = new CampusService(session);
            _branchService = new BranchService(session);
            _userService = new UserService(session);
            _hrMemberService = new TeamMemberService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _departmentService = new DepartmentService(session);
            _designationService = new DesignationService(session);
            _attendanceSummaryService = new AttendanceSummaryService(session);
            _shiftService = new ShiftService(session);
            _shiftWeekendHistoryService = new ShiftWeekendHistoryService(session);
            _hrSalaryHistoryService = new SalaryHistoryService(session);
            _allowanceSheetService = new AllowanceSheetService(session);
        }
        #endregion

        #region Attendance Reset

        public ActionResult AttendanceReset()
        {
           // ViewBag.IsPost = false;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.DateFrom = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
            ViewBag.OrganizationId = new List<long>();
            ViewBag.BranchId = new List<long>();
            ViewBag.CampusId = new List<long>();
            ViewBag.departmentId = new List<long>();
            ViewBag.Pin = "";
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new MultiSelectList(new List<Campus>(), "Id", "Name");
                ViewBag.DepartmentList = new MultiSelectList(new List<Department>(), "Id", "Name");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public JsonResult PostAttendanceReset(string dateFrom, string dateTo, List<long> organizationId = null, List<long> branchId = null, List<long> campusIds = null, List<long> departmentIds = null, string pin = "")
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime convertDateFrom = Convert.ToDateTime(dateFrom);
                DateTime convertDateto = Convert.ToDateTime(dateTo);
                if (convertDateFrom > convertDateto)
                    return Json(new { Message = "From Date must be smaller or equal than To Date", IsSuccess = false });

                _attendanceSummaryService.PostAttendanceReset(_userMenu, convertDateFrom, convertDateto, organizationId, branchId, campusIds, departmentIds, pin);
                return Json(new { Message = "Successfully Attendance Reset",IsSuccess = true });
                
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Shift Batch Assign

        public ActionResult BatchAssignShift()
        {
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            //ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.BranchList = new MultiSelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new MultiSelectList(new List<Campus>(), "Id", "Name");
                ViewBag.DepartmentList = new MultiSelectList(new List<Department>(), "Id", "Name");
                ViewBag.DesignationList = new MultiSelectList(new List<Designation>(), "Id", "Name");
                ViewBag.ShiftList = new SelectList(new List<Shift>(), "Id", "Name");
                var memberEmploymentStatusList = new SelectList(_commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int> { (int)MemberEmploymentStatus.Retired }), "Key", "Value");
                List<SelectListItem> esList = memberEmploymentStatusList.ToList();
                esList.Insert(0, new SelectListItem() { Value = "0", Text = "All EmployMentList" });
                ViewBag.EmploymentStatusList = new MultiSelectList(esList, "Value", "Text");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public PartialViewResult TeamMemberBatchAssignShift(long organizationId, List<long> branchIds = null, List<long> campusIds = null, List<long> departmentIds = null, List<long> designationIds = null, List<int> employmentStatus = null, List<long> searchingShiftIds = null, string searchingEffectiveDate = "")
        {
            List<TeamMemberBatchShiftDto> teamMemberBatchShiftDtoList = new List<TeamMemberBatchShiftDto>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime? searchingEffectiveDateConverted = (!String.IsNullOrEmpty(searchingEffectiveDate))
                    ? Convert.ToDateTime(searchingEffectiveDate)
                    : (DateTime?) null;

                teamMemberBatchShiftDtoList = _hrMemberService.LoadTeamMemberBatchShiftDto(_userMenu, organizationId, branchIds, campusIds, employmentStatus, departmentIds, designationIds, searchingShiftIds, searchingEffectiveDateConverted).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return PartialView("Partial/_TeamMemberBatchShift", teamMemberBatchShiftDtoList);
        }

        [HttpPost]
        public JsonResult AssignBatchShiftToTeamMember(List<int> teamMemberPins, long shiftId, string effectiveDate)
        {
            string message = "";
            bool successState = false;
            int count = 0;
            try
            {
                _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                if (!teamMemberPins.Any())
                {
                    throw new InvalidDataException("No TeamMember Selected!");
                }
                Shift shift = _shiftService.LoadById(shiftId);
                if (shift == null)
                {
                    throw new InvalidDataException("No Shift Selected!");
                }
                DateTime shiftEffectiveDateTime = Convert.ToDateTime(effectiveDate);
                List<long> authorizedTeamMemberId = _hrMemberService.LoadHrAuthorizedTeamMemberId(_userMenu, null, null, null, null, null, null, teamMemberPins);
                if (!authorizedTeamMemberId.Any())
                {
                    throw new InvalidDataException("No Authorized TeamMember Found!");
                }
                List<ShiftWeekendHistory> teamMemberShiftWeekendHistoryList = new List<ShiftWeekendHistory>();
                foreach (long teamMemberId in authorizedTeamMemberId)
                {
                    TeamMember teamMember = _hrMemberService.LoadById(teamMemberId);
                    ShiftWeekendHistory currentShiftWeekendHistory = teamMember.ShiftWeekendHistory.Where(x =>
                                x.Status == ShiftWeekendHistory.EntityStatus.Active && x.EffectiveDate != null &&
                                x.EffectiveDate.Value.Date <= DateTime.Today.Date)
                            .OrderByDescending(x => x.EffectiveDate)
                            .ThenByDescending(x => x.Id)
                            .FirstOrDefault();

                    ShiftWeekendHistory teamShiftWeekendHistory = new ShiftWeekendHistory
                    {
                        Organization = shift.Organization,
                        TeamMember = teamMember,
                        Shift = shift,
                        Weekend = (currentShiftWeekendHistory != null) ? currentShiftWeekendHistory.Weekend : null,
                        EffectiveDate = shiftEffectiveDateTime,
                        Status = ShiftWeekendHistory.EntityStatus.Active,
                        Rank = 10,
                    };
                    teamMemberShiftWeekendHistoryList.Add(teamShiftWeekendHistory);
                    count++;
                }
                _shiftWeekendHistoryService.SaveBatchShiftWeekendHistory(teamMemberShiftWeekendHistoryList);
                message = "Total " + count + " Team Member Shift Weekend Successfully Assign.";
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        #endregion

        #region Salary Batch Assign

        public ActionResult BatchAssignSalaryHistory()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
            try
            {
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.BranchList = new MultiSelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusList = new MultiSelectList(new List<Campus>(), "Id", "Name");
                ViewBag.DepartmentList = new MultiSelectList(new List<Department>(), "Id", "Name");
                ViewBag.DesignationList = new MultiSelectList(new List<Designation>(), "Id", "Name");
                var memberEmploymentStatusList = new SelectList(_commonHelper.LoadEmumToDictionary<MemberEmploymentStatus>(new List<int> { (int)MemberEmploymentStatus.Retired }), "Key", "Value");
                List<SelectListItem> esList = memberEmploymentStatusList.ToList();
                esList.Insert(0, new SelectListItem() { Value = "0", Text = "All EmployMentList" });
                ViewBag.EmploymentStatusList = new MultiSelectList(esList, "Value", "Text");

                List<SelectListItem> searchList = new List<SelectListItem>();
                searchList.Insert(0, new SelectListItem() { Value = true.ToString(), Text = "Search On Employmnet History" });
                searchList.Insert(0, new SelectListItem() { Value = false.ToString(), Text = "Search On Salary History" });
                ViewBag.IsSearchOnEmploymetHistorySelectList = new SelectList(searchList, "Value", "text", true);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public PartialViewResult TeamMemberBatchAssignSalaryHistory(long organizationId, List<long> branchIds = null, List<long> campusIds = null, List<long> departmentIds = null, List<long> designationIds = null, List<int> employmentStatus = null, bool isSearchOnEmploymetHistory = true)
        {
            List<TeamMemberBatchSalaryHistoryDto> teamMemberBatchSalaryHistoryDtoList = new List<TeamMemberBatchSalaryHistoryDto>();
            List<TeamMemberBatchSalaryHistoryDto> teamMemberBatchSalaryHistoryDtoList2 = new List<TeamMemberBatchSalaryHistoryDto>();
            try
            {
                _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                teamMemberBatchSalaryHistoryDtoList =
                    _hrMemberService.LoadTeamMemberBatchSalaryHistoryDto(_userMenu, organizationId, branchIds, campusIds,
                        employmentStatus, departmentIds, designationIds, isSearchOnEmploymetHistory).ToList();
                if (teamMemberBatchSalaryHistoryDtoList.Any())
                {
                    foreach (TeamMemberBatchSalaryHistoryDto salaryHistoryDto in teamMemberBatchSalaryHistoryDtoList)
                    {
                        long selectOrganizationId = salaryHistoryDto.LastSalaryHistoryOrganizationId ??
                                                    salaryHistoryDto.OrganizationId;
                        long selectBranchId = (salaryHistoryDto.LastSalaryHistoryBranchId != null)
                            ? (long) salaryHistoryDto.LastSalaryHistoryBranchId
                            : salaryHistoryDto.BranchId;

                        long selectDepartmentId = (salaryHistoryDto.LastSalaryHistoryDepartmentId != null)
                            ? (long) salaryHistoryDto.LastSalaryHistoryDepartmentId
                            : salaryHistoryDto.DepartmentId;

                        long selectDesignationId = (salaryHistoryDto.LastSalaryHistoryDesignationId != null)
                           ? (long)salaryHistoryDto.LastSalaryHistoryDesignationId
                           : salaryHistoryDto.DesignationId;

                        long selectCampusId = (salaryHistoryDto.LastSalaryHistoryCampusId != null)
                            ? (long) salaryHistoryDto.LastSalaryHistoryCampusId
                            : salaryHistoryDto.CampusId;
                        int selectSalaryPurposeId = (salaryHistoryDto.LastSalaryHistorySalaryPurpose != null)
                            ? (int) salaryHistoryDto.LastSalaryHistorySalaryPurpose
                            : 0;

                        salaryHistoryDto.OrganizationList =
                            new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName",
                                selectOrganizationId);
                        salaryHistoryDto.BranchList =
                            new SelectList(
                                _branchService.LoadAuthorizedBranch(_userMenu,
                                    _commonHelper.ConvertIdToList(selectOrganizationId), null, null, false), "Id",
                                "Name", selectBranchId);
                        salaryHistoryDto.DepartmentList =
                            new SelectList(
                                _departmentService.LoadAuthorizedDepartment(_userMenu,
                                    _commonHelper.ConvertIdToList(selectOrganizationId)), "Id", "Name",
                                selectDepartmentId);

                        salaryHistoryDto.DesignationList = new SelectList(_designationService.LoadDesignation(_commonHelper.ConvertIdToList(selectOrganizationId)), "Id", "Name",
                                selectDesignationId);

                        salaryHistoryDto.CampusList =
                            new SelectList(
                                _campusService.LoadAuthorizeCampus(_userMenu,
                                    _commonHelper.ConvertIdToList(selectOrganizationId), null,
                                    _commonHelper.ConvertIdToList(selectBranchId), null, false), "Id", "Name",
                                selectCampusId);
                        salaryHistoryDto.SalaryPurposeList =
                            new SelectList(_commonHelper.LoadEmumToDictionary<SalaryPurpose>(), "Key", "Value",
                                selectSalaryPurposeId);

                        salaryHistoryDto.LastSalaryHistorySalaryPurposeName =
                            (salaryHistoryDto.LastSalaryHistorySalaryPurpose != null)
                                ? _commonHelper.GetEmumIdToValue<SalaryPurpose>(
                                    (int) salaryHistoryDto.LastSalaryHistorySalaryPurpose)
                                : "";
                        salaryHistoryDto.NextSalaryHistorySalaryPurposeName =
                            (salaryHistoryDto.NextSalaryHistorySalaryPurpose != null)
                                ? _commonHelper.GetEmumIdToValue<SalaryPurpose>(
                                    (int) salaryHistoryDto.NextSalaryHistorySalaryPurpose)
                                : "";
                    }
                }
            }
            catch (InvalidDataException ex)
            {
                ViewBag.Errormessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return PartialView("Partial/_TeamMemberBatchSalaryHistory", teamMemberBatchSalaryHistoryDtoList);
        }

        [HttpPost]
        public JsonResult AddBatchSalaryHistoryToTeamMember(List<SalaryHistoryViewModel> salaryHistoryViewModelList)
        {
            string message = "";
            bool successState = false;
            int count = 0;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (!salaryHistoryViewModelList.Any())
                {
                    throw new InvalidDataException("No TeamMember Selected!");
                }

                List<int> teamMemberPins = salaryHistoryViewModelList.Select(x => x.MemberPin).ToList();

                List<long> authorizedTeamMemberId = _hrMemberService.LoadHrAuthorizedTeamMemberId(_userMenu, null, null, null, null, null, null, teamMemberPins);
                if (!authorizedTeamMemberId.Any())
                {
                    throw new InvalidDataException("No Authorized TeamMember Found!");
                }

                List<SalaryHistory> teamMemberSalaryHistoryList = new List<SalaryHistory>();

                foreach (SalaryHistoryViewModel salaryHistoryViewModel in salaryHistoryViewModelList)
                {
                    SalaryHistory salaryHistory = new SalaryHistory();
                    salaryHistoryViewModel.GetModelFromViewModel(salaryHistory);
                    salaryHistory.SalaryOrganization = _organizationService.LoadById(salaryHistoryViewModel.OrganizationId);
                    salaryHistory.SalaryDepartment = _departmentService.LoadById(salaryHistoryViewModel.DepartmentId);
                    salaryHistory.SalaryDesignation = _designationService.LoadById(salaryHistoryViewModel.DesignationId);
                    salaryHistory.SalaryCampus = _campusService.GetCampus(salaryHistoryViewModel.CampusId);
                    salaryHistory.SalaryPurpose = (SalaryPurpose)salaryHistoryViewModel.SalaryPurpose;
                    salaryHistory.TeamMember = _hrMemberService.LoadById(salaryHistoryViewModel.MemberId);
                    salaryHistory.Rank = 10;
                    salaryHistory.Status = SalaryHistory.EntityStatus.Active;
                    teamMemberSalaryHistoryList.Add(salaryHistory);
                    count++;
                }
                _hrSalaryHistoryService.SaveBatchSalaryHistory(teamMemberSalaryHistoryList);
                message = "Total " + count + " Team Member Salary History Successfully Added.";
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.SetExceptionMessage(ex);
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        #endregion

        //#region Allowance Sheet

        //public ActionResult AllowanceSheet()
        //{
        //    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //    ViewBag.OrganizationList = new SelectList(new List<Organization>(), "Id", "Name");
        //    try
        //    {
        //        ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
        //        ViewBag.BranchList = new MultiSelectList(new List<Branch>(), "Id", "Name");
        //        ViewBag.CampusList = new MultiSelectList(new List<Campus>(), "Id", "Name");
        //        ViewBag.DepartmentList = new MultiSelectList(new List<Department>(), "Id", "Name");
        //        var t = new SelectList(_commonHelper.LoadEmumToDictionary<AllowanceSheetTeamMemberSearchType>(), "Key", "Value");
        //        ViewBag.EmploymentSearchOnList = t;
        //        ViewBag.AllowanceSheetMonthList = new SelectList(_commonHelper.LoadEmumToDictionary<MonthsOfYear>(), "Key", "Value");

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //    }
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult AllowanceSheet(AllowanceSheetFormViewModel allowanceSheetFormViewModel)
        //{
        //    bool isSuccess = false;
        //    string message = "";
        //    List<AllowanceSheet> allowanceSheetList = new List<AllowanceSheet>();
        //    ViewBag.OrganizationFullName = "";
        //    ViewBag.BranchName = "";
        //    ViewBag.CampusName = "";
        //    ViewBag.DepartmentName = "";
        //    ViewBag.SheetName = "";
        //    try
        //    {
        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(_userMenu, allowanceSheetFormViewModel);

        //        ViewBag.OrganizationFullName = (allowanceSheetFormViewModel.OrganizationId != SelectionType.SelelectAll) ? _organizationService.LoadById(allowanceSheetFormViewModel.OrganizationId).Name : "All Organization";
        //        ViewBag.BranchName = (allowanceSheetFormViewModel.BranchId != SelectionType.SelelectAll) ? _branchService.GetBranch(allowanceSheetFormViewModel.BranchId).Name : "All Branch ";
        //        ViewBag.CampusName = (allowanceSheetFormViewModel.CampusId != SelectionType.SelelectAll) ? _campusService.GetCampus(allowanceSheetFormViewModel.CampusId).Name : "All Campus ";
        //        ViewBag.DepartmentName = (allowanceSheetFormViewModel.DepartmentId != SelectionType.SelelectAll) ? _departmentService.LoadById(allowanceSheetFormViewModel.DepartmentId).Name : "All Department ";
        //        ViewBag.SheetName = "Allowance Sheet - " + _commonHelper.GetEmumIdToValue<MonthsOfYear>(allowanceSheetFormViewModel.Month) + " " + allowanceSheetFormViewModel.Year.ToString();
        //        isSuccess = true;
        //        return PartialView("Partial/_TeamMemberAllowanceSheetList", allowanceSheetList);
        //    }
        //    catch (InvalidDataException ex)
        //    {
        //        message = ex.Message;
        //        ViewBag.ErrorMessage = ex.Message;
        //    }
        //    catch (Exception ex)
        //    {
        //        message = WebHelper.SetExceptionMessage(ex);
        //        _logger.Error(ex);
        //    }
        //    return Json(new { IsSuccess = isSuccess, Message = message });
        //}

        //public ActionResult ExportAllowanceSheet(string allowanceSheetFormViewModelString)
        //{
        //    AllowanceSheetFormViewModel allowanceSheetViewModel = new JavaScriptSerializer().Deserialize<AllowanceSheetFormViewModel>(allowanceSheetFormViewModelString);

        //    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //    var allowanceSheetExcelList = new List<List<object>>();
        //    try
        //    {
        //        var headerList = new List<string>();
        //        if (allowanceSheetViewModel.OrganizationId >= 0)
        //        {
        //            string organizationFullName = (allowanceSheetViewModel.OrganizationId != SelectionType.SelelectAll) ? _organizationService.LoadById(allowanceSheetViewModel.OrganizationId).Name : "All Organization";
        //            string branchName = (allowanceSheetViewModel.BranchId != SelectionType.SelelectAll) ? _branchService.GetBranch(allowanceSheetViewModel.BranchId).Name : "All Branch ";
        //            string campusName = (allowanceSheetViewModel.CampusId != SelectionType.SelelectAll) ? _campusService.GetCampus(allowanceSheetViewModel.CampusId).Name : "All Campus ";
        //            string departmentName = (allowanceSheetViewModel.DepartmentId != SelectionType.SelelectAll) ? _departmentService.LoadById(allowanceSheetViewModel.DepartmentId).Name : "All Department ";

        //            string sheetName = "Allowance Sheet - " + _commonHelper.GetEmumIdToValue<MonthsOfYear>(allowanceSheetViewModel.Month) + " " + allowanceSheetViewModel.Year.ToString();

        //            headerList.Add(organizationFullName);
        //            headerList.Add(branchName);
        //            headerList.Add(campusName);
        //            headerList.Add(departmentName);
        //            headerList.Add("");
        //            headerList.Add("");
        //            headerList.Add("");
        //            headerList.Add(sheetName);
        //        }
        //        var columnList = new List<string> {
        //                            "PIN"
        //                            , "Name"
        //                            , "Designation"
        //                            , "Daily Support Allowance"
        //                            , "Holiday Allowance"
        //                            , "Overtime Allowance"
        //                            , "NightWork Allowance"
        //                            , "Children Education Allowance"
        //                            , "Special Allowance"
        //                            , "Zakat"
        //                            , "CSR Allowance"
        //                            , "Total Allowance"
        //                        };

        //        int start = 0;

        //        #region datatable

        //        _userMenu = (List<UserMenu>)ViewBag.UserMenu;
        //        List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(_userMenu, allowanceSheetViewModel);

        //        var footerList = new List<string>();
        //        footerList.Add("");
        //        decimal dailySupportAllowanceTotalAmount = 0;
        //        decimal hodidayAllowanceTotalAmount = 0;
        //        decimal overtimeAllowanceTotalAmount = 0;
        //        decimal nightWorkAllowanceTotalAmount = 0;
        //        decimal childrenEducationAllowanceTotalAmount = 0;
        //        decimal specialAllowanceTotalAmount = 0;
        //        decimal zakatTotalAmount = 0;
        //        decimal csrAllowanceTotalAmount = 0;
        //        decimal grandTotalAllowanceAmount = 0;

        //        foreach (AllowanceSheet allowanceSheet in allowanceSheetList)
        //        {
        //            var xlsRow = new List<object>();
        //            dailySupportAllowanceTotalAmount += allowanceSheet.DailySupportAllowanceAmount;
        //            hodidayAllowanceTotalAmount += allowanceSheet.ExtradayAllowanceAmount;
        //            overtimeAllowanceTotalAmount += allowanceSheet.OvertimeAllowanceAmount;
        //            nightWorkAllowanceTotalAmount += allowanceSheet.NightWorkAllowanceAmount;
        //            childrenEducationAllowanceTotalAmount += allowanceSheet.ChildrenEducationAllowanceAmount;
        //            specialAllowanceTotalAmount += allowanceSheet.SpecialAllowanceAmount;
        //            zakatTotalAmount += allowanceSheet.ZakatAllowanceAmount;
        //            csrAllowanceTotalAmount += allowanceSheet.CsrAllowanceAmount;
        //            grandTotalAllowanceAmount += allowanceSheet.TotalAllowanceAmount;

        //            #region datatable
        //            xlsRow.Add(allowanceSheet.TeamMember.Pin.ToString());
        //            xlsRow.Add(allowanceSheet.TeamMember.Name);
        //            xlsRow.Add(allowanceSheet.TeamMemberDesignation.Name);
        //            xlsRow.Add(allowanceSheet.DailySupportAllowanceAmount);
        //            xlsRow.Add(allowanceSheet.ExtradayAllowanceAmount);
        //            xlsRow.Add(allowanceSheet.OvertimeAllowanceAmount);
        //            xlsRow.Add(allowanceSheet.NightWorkAllowanceAmount);
        //            xlsRow.Add(allowanceSheet.ChildrenEducationAllowanceAmount);
        //            xlsRow.Add(allowanceSheet.SpecialAllowanceAmount);
        //            xlsRow.Add(allowanceSheet.ZakatAllowanceAmount);
        //            xlsRow.Add(allowanceSheet.CsrAllowanceAmount);
        //            xlsRow.Add(allowanceSheet.TotalAllowanceAmount);
        //            allowanceSheetExcelList.Add(xlsRow);
        //            #endregion
        //        }
        //        if (allowanceSheetList.Any())
        //        {
        //            var xlsRow2 = new List<object>();
        //            xlsRow2.Add("");
        //            xlsRow2.Add("Sub Total");
        //            xlsRow2.Add("");
        //            xlsRow2.Add(dailySupportAllowanceTotalAmount.ToString());
        //            xlsRow2.Add(hodidayAllowanceTotalAmount.ToString());
        //            xlsRow2.Add(overtimeAllowanceTotalAmount.ToString());
        //            xlsRow2.Add(nightWorkAllowanceTotalAmount.ToString());
        //            xlsRow2.Add(childrenEducationAllowanceTotalAmount.ToString());
        //            xlsRow2.Add(specialAllowanceTotalAmount.ToString());
        //            xlsRow2.Add(zakatTotalAmount.ToString());
        //            xlsRow2.Add(csrAllowanceTotalAmount.ToString());
        //            xlsRow2.Add(grandTotalAllowanceAmount.ToString());
        //            allowanceSheetExcelList.Add(xlsRow2);
        //        }
        //        ExcelGenerator.GenerateExcel(headerList, columnList, allowanceSheetExcelList, footerList, "Allowance-sheet__" + _commonHelper.GetEmumIdToValue<MonthsOfYear>(allowanceSheetViewModel.Month) + "_" + allowanceSheetViewModel.Year.ToString());
        //        return View("AutoClose");

        //        #endregion
        //    }
        //    catch (Exception e)
        //    {
        //        _logger.Error(e.Message);
        //        throw e;
        //    }
        //}

        //[HttpPost]
        //public JsonResult SaveAllowanceSheet(AllowanceSheetFormViewModel allowanceSheetFormViewModel)
        //{
        //    bool isSuccess = false;
        //    string message = "";
        //    try
        //    {
        //        _userMenu = (List<UserMenu>) ViewBag.UserMenu;
        //        List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(_userMenu, allowanceSheetFormViewModel, true);
        //        int count = allowanceSheetList.Count(x => x.IsFinalSubmit = true && x.Id != 0);
        //        _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList);
        //        isSuccess = true;
        //        message = "Total " + (allowanceSheetList.Count - count) + " Allowance Sheet(s) Saved Successfully ";
        //    }
        //    catch (InvalidDataException ex)
        //    {
        //        message = ex.Message;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        message = WebHelper.SetExceptionMessage(ex);
        //    }
        //    return Json(new { IsSuccess = isSuccess, Message = message });
        //}

        //#endregion

    }
}