﻿using System;
using System.Collections;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.EmployeeBenefitFundSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "EmployeeBenefitsFundSetting")]
    public class EmployeeBenefitFundSettingSaveTest : EmployeeBenefitFundSettingBaseTest
    {
        [Fact]
        public void Save_Should_Be_Successfully()
        {
           var organization =_organizationFactory.Create().Persist();
           var branch = branchFactory.Create().WithOrganization(organization.Object).Persist();
           var userMenu = BuildUserMenu(organization.Object, (Program)null, branch.Object);

            const string name = "Permanent";
            DateTime effectiveDate = DateTime.Parse("2016-09-01");
            DateTime closingDate = DateTime.Parse("2016-09-10");
            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);
            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);

            _employeeBenefitsFundSettingFactory.Create(name)
                .WithOrganization(organization.Object)
                .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                .WithCalculationOn(CalculationOn.Basic)
                .WithEmployeeEmployerContribution(10, 25).WithEffectiveClosingDate(effectiveDate, closingDate).WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList);

            _employeeBenefitsFundSettingService.Save(_employeeBenefitsFundSettingFactory.Object, userMenu);

            EmployeeBenefitsFundSetting employeeBenefitsFundSetting = _employeeBenefitsFundSettingService.GetEmployeeBenefitFundSetting(_employeeBenefitsFundSettingFactory.Object.Id);
            Assert.NotNull(employeeBenefitsFundSetting);
            Assert.True(employeeBenefitsFundSetting.Id > 0);
        }


         [Fact]
        public void Save_Should_Be_Fail_For_Null_Organization()
        {
            var organization = _organizationFactory.Create().Persist();
            var branch = branchFactory.Create().WithOrganization(organization.Object).Persist();
            var userMenu = BuildUserMenu(organization.Object, (Program)null, branch.Object);

            string name = "Permanent";
            DateTime effectiveDate = DateTime.Parse("2016-09-01");
            DateTime closingDate = DateTime.Parse("2016-09-10");
            _employeeBenefitsFundSettingEntitlementFactory.Create().WithServicePeriod(3).WithEmployerContribution(40);
            IList<EmployeeBenefitsFundSettingEntitlement> employeeBenefitsFundSettingEntitlementList = new List<EmployeeBenefitsFundSettingEntitlement>();
            employeeBenefitsFundSettingEntitlementList.Add(_employeeBenefitsFundSettingEntitlementFactory.Object);
           
             _employeeBenefitsFundSettingFactory.Create(name)
                 //.WithOrganization(null)
                 .WithEmploymentStatus(MemberEmploymentStatus.Permanent)
                 .WithCalculationOn(CalculationOn.Basic)
                 .WithEmployeeEmployerContribution(10, 25)
                 .WithEffectiveClosingDate(effectiveDate, closingDate)
                 .WithEmployeeBenefitFundSettingEntitlement(employeeBenefitsFundSettingEntitlementList);
             Assert.Throws<InvalidDataException>(() => _employeeBenefitsFundSettingService.Save(_employeeBenefitsFundSettingFactory.Object,userMenu));
        }

    }
}
