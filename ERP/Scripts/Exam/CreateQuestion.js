﻿function getCkeditor(id, height, question) {

   CKEDITOR.replace(id, {
        filebrowserImageBrowseUrl: '/Exam/Question/UploadPartial',
        filebrowserImageUploadUrl: '/Exam/Question/UploadNow',
        height: height
   });
  
}

function ckeditorGenerate() {

    if ($("#QuestionBangla").length > 0) {
        getCkeditor("QuestionBangla", 200,"q");
        getCkeditor("OptionBanglaA", 100,"");
        getCkeditor("OptionBanglaB", 100, "");
        getCkeditor("OptionBanglaC", 100, "");
        getCkeditor("OptionBanglaD", 100, "");
        //getCkeditor("OptionBanglaE", 100);
    }
    if ($("#QuestionEnglish").length > 0) {
        getCkeditor("QuestionEnglish", 200, "q");
        getCkeditor("OptionEnglishA", 100, "");
        getCkeditor("OptionEnglishB", 100, "");
        getCkeditor("OptionEnglishC", 100, "");
        getCkeditor("OptionEnglishD", 100, "");
        //getCkeditor("OptionEnglishE", 100);
    }
}

$(document).ready(function () {
    $("#QuestionEntryForm").submit(function () {
        return false;
    });

    $('body').keydown(function (e) {
        if (e.keyCode == 13) {
            responseReturn();
        }
    });


});

$(document).on("change", "#Organization", function (event) {

    $("#Program option").remove();
    $('#Program').append("<option value=''>--Select Program--</option>");

    $("#Session option").remove();
    $('#Session').append("<option value=''>--Select Session--</option>");

    $("#Course option").remove();
    $('#Course').append("<option value=''>--Select Course--</option>");

    $("#SubjectId option").remove();
    $('#SubjectId').append("<option value=''>--Select Subject--</option>");

    $("#UniqueSet option").remove();
    $('#UniqueSet').append("<option value=''>--Select Unique Set--</option>");

    $("#ExamId").val("");
    $("#ExamCode").val("");
    $("#ExamName").val("");

    $("#questionEntryDiv").empty();
    $("#questionEntryDiv").append("<div>Please Fillup Question details criteria.</div>");

    if ($(this).val() != "") {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
            cache: false,
            async: true,
            data: { "organizationIds": $(this).val(), isAuthorized: true },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                $.unblockUI();
                if (response.IsSuccess) {
                    $.each(response.returnProgramList, function (i, v) {
                        $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
});

$(document).on("change", "#Program", function (event) {

    $("#Session option").remove();
    $('#Session').append("<option value=''>--Select Session--</option>");

    $("#Course option").remove();
    $('#Course').append("<option value=''>--Select Course--</option>");

    $("#SubjectId option").remove();
    $('#SubjectId').append("<option value=''>--Select Subject--</option>");

    $("#UniqueSet option").remove();
    $('#UniqueSet').append("<option value=''>--Select Unique Set--</option>");

    $("#ExamId").val("");
    $("#ExamCode").val("");
    $("#ExamName").val("");

    $("#questionEntryDiv").empty();
    $("#questionEntryDiv").append("<div>Please Fillup Question details criteria.</div>");

    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();

    if (organizationId != "" && programId != "") {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
            cache: false,
            async: true,
            data: { organizationIds: organizationId, programIds: programId, isAuthorized: true },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                $.unblockUI();
                if (response.IsSuccess) {
                    $.each(response.returnSessionList, function (i, v) {
                        $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
            }
        });
    }
});

$(document).on("change", "#Session", function (event) {

    $("#Course option").remove();
    $('#Course').append("<option value=''>--Select Course--</option>");

    $("#SubjectId option").remove();
    $('#SubjectId').append("<option value=''>--Select Subject--</option>");

    $("#UniqueSet option").remove();
    $('#UniqueSet').append("<option value=''>--Select Unique Set--</option>");

    $("#ExamId").val("");
    $("#ExamCode").val("");
    $("#ExamName").val("");

    $("#questionEntryDiv").empty();
    $("#questionEntryDiv").append("<div>Please Fillup Question details criteria.</div>");

    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    if (organizationId != "" && organizationId != null && programId != "" && programId != null && sessionId != "" && sessionId != null) {
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
            cache: false,
            async: true,
            data: { organizationIds: organizationId, programIds: programId, sessionIds: sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {

                $.unblockUI();
                if (response.IsSuccess) {
                    $.each(response.returnCourse, function (i, v) {
                        $('#Course').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }

});

$(document).on("change", "#Course", function (event) {
    $("#SubjectId option").remove();
    $('#SubjectId').append("<option value=''>--Select Subject--</option>");

    $("#UniqueSet option").remove();
    $('#UniqueSet').append("<option value=''>--Select Unique Set--</option>");

    $("#ExamId").val("");
    $("#ExamCode").val("");
    $("#ExamName").val("");

    $("#questionEntryDiv").empty();
    $("#questionEntryDiv").append("<div>Please Fillup Question details criteria.</div>");
});


function autoCompleteFunc() {
    $('.autoComplete').each(function () {
        var $this = $(this);
        $this.typeahead({
            source: function (query, process) {
                var programId = $("#Program").val();
                var sessionId = $("#Session").val();
                var courseId = $("#Course").val();
                var data = { query: query, programId: programId, sessionId: sessionId, courseId: courseId }
                $.ajax({
                    type: "POST",
                    url: $("body").attr("data-project-root") + "Exam/Question/AjaxRequestForExamCode",
                    cache: false,
                    async: true,
                    data: data,

                    success: function (result) {
                        if (result.IsSuccess) {
                            process(result.returnList);
                        }
                    },
                    complete: function () {
                    },
                    error: function (result) {
                    }
                });
            },
            updater: function (item) {
                $("#SubjectId option").remove();
                $('#SubjectId').append("<option value=''>--Select Subject--</option>");

                $("#UniqueSet option").remove();
                $('#UniqueSet').append("<option value=''>--Select Unique Set--</option>");

                $("#ExamId").val(item.Value);
                $("#ExamCode").val("");
                $("#ExamName").val("");

                $("#questionEntryDiv").empty();
                $("#questionEntryDiv").append("<div>Please Fillup Question details criteria.</div>");

                $.ajax({
                    type: "post",
                    url: $("body").attr("data-project-root") + "Exam/Question/AjaxRequestForExamInformation",
                    cache: false,
                    async: true,
                    data: { "id": item.Value },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (result) {
                        $.unblockUI();
                        if (result.IsSuccess) {
                            console.log(result);
                            if (result.returnList.hasOwnProperty("Name")) {
                                $("#ExamName").val(result.returnList.Name);
                            }
                            if (result.returnList.hasOwnProperty("TotalUniqueSet")) {
                                var uniqueSet = parseInt(result.returnList.TotalUniqueSet);

                                for (var i = 1; i <= uniqueSet; i++) {
                                    $('#UniqueSet').append($('<option>').text(i).attr('value', i));
                                }
                            }

                            if (result.hasOwnProperty("subjectList")) {
                                $.each(result.subjectList, function (i, v) {
                                    $('#SubjectId').append($('<option>').text(v.Text).attr('value', v.Value));
                                });
                            }

                        }
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (result) {
                        $.unblockUI();
                        console.log("Failed -----");
                    }
                });
                return item.Text;
            },
            matcher: function (item) {
                if (item.Text.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                    return true;
                }
                return false;
            },
            sorter: function (items) {
                return items.sort();
            },
            highlighter: function (item) {
                var regex = new RegExp('(' + this.query + ')', 'gi');
                return item.Text.replace(regex, "<strong>$1</strong>");
            },
            onselect: function (obj) {
                alert('Selected ' + obj);
            }
        });
    });
}

function responseReturn() {

    var check = true;
    $("#questionEntryDiv").empty();
    $("#questionEntryDiv").append("<div>Please Fillup Question details criteria.</div>");


    var organizationId = $("#Organization").val();
    var programId = $("#Program").val();
    var sessionId = $("#Session").val();
    var course = $("#Course").val();
    var examCode = $("#ExamCode").val();
    var subjectId = $("#SubjectId").val();
    var uniqueSet = $("#UniqueSet").val();


    if (organizationId == "" || organizationId == null) {
        showErrorMessageBelowCtrl("Organization", "", false);
        showErrorMessageBelowCtrl("Organization", "The Organization field is required.", true);
        check = false;
    }

    if (programId == "" || programId == null) {
        showErrorMessageBelowCtrl("Program", "", false);
        showErrorMessageBelowCtrl("Program", "The Program field is required.", true);
        check = false;
    }

    if (sessionId == "" || sessionId == null) {
        showErrorMessageBelowCtrl("Session", "", false);
        showErrorMessageBelowCtrl("Session", "The Session field is required.", true);
        check = false;
    }

    if (course == "" || course == null) {
        showErrorMessageBelowCtrl("Course", "", false);
        showErrorMessageBelowCtrl("Course", "The Course field is required.", true);
        check = false;
    }


    if (examCode == "" || examCode == null) {
        showErrorMessageBelowCtrl("ExamCode", "", false);
        showErrorMessageBelowCtrl("ExamCode", "The Exam Code field is required.", true);
        check = false;
    }

    if (subjectId == "" || subjectId == null) {
        showErrorMessageBelowCtrl("SubjectId", "", false);
        showErrorMessageBelowCtrl("SubjectId", "The Subject field is required.", true);
        check = false;
    }

    if (uniqueSet == "" || uniqueSet == null) {
        showErrorMessageBelowCtrl("UniqueSet", "", false);
        showErrorMessageBelowCtrl("UniqueSet", "The Unique Set field is required.", true);
        check = false;
    }

    if (check) {
        var examId = $("#ExamId").val();
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Exam/Question/AjaxRequestForDrawingQuestionEntryDiv",
            cache: false,
            async: true,
            data: { "id": examId, "uniqueset": uniqueSet },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (!result.hasOwnProperty("IsSuccess")) {
                    $("#questionEntryDiv").html(result);
                    $('#questionPanel').keydown(function (e) {
                        if (e.keyCode == 13) {
                            return false;
                        }
                    });

                    window.validateTextField("QuestionBangla", "The Question field is required.");
                    window.validateTextField("OptionBanglaA", "The Option A field is required.");
                    window.validateTextField("OptionBanglaB", "The Option B field is required.");

                    window.validateTextField("QuestionEnglish", "The Question field is required.");
                    window.validateTextField("OptionEnglishA", "The Option A field is required.");
                    window.validateTextField("OptionEnglishB", "The Option B field is required.");

                    window.validateTextField("CorrectAnswer", "The Correct Answer field is required.");

                    ckeditorGenerate();
                    checkValidationForOptionCDE();
                }
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }
}

$(document).on("input propertychange", ".autoComplete", function (event) {

    autoCompleteFunc();
});

$(document).on("click", "#next", function (event) {
    responseReturn();
});

function checkValidationForOptionCDE() {
    for (var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].on('change', function () {
            var optionBanglaC = CKEDITOR.instances['OptionBanglaC'].getData();
            var optionEnglishC = CKEDITOR.instances['OptionEnglishC'].getData();
            var optionBanglaD = CKEDITOR.instances['OptionBanglaD'].getData();
            var optionEnglishD = CKEDITOR.instances['OptionEnglishD'].getData();
            var optionBanglaE = "";//CKEDITOR.instances['OptionBanglaE'].getData();
            var optionEnglishE = "";//CKEDITOR.instances['OptionEnglishE'].getData();


            if ($("#OptionEnglishC").length > 0 && $("#OptionBanglaC").length > 0) {
                if (optionEnglishC == "" && optionBanglaC == "") {
                    showErrorMessageBelowCtrl("OptionBanglaC", "", false);
                    showErrorMessageBelowCtrl("OptionEnglishC", "", false);
                }
                else if (optionEnglishC != "" && optionBanglaC != "") {
                    showErrorMessageBelowCtrl("OptionBanglaC", "", false);
                    showErrorMessageBelowCtrl("OptionEnglishC", "", false);
                }
                else if (optionEnglishC != "" && optionBanglaC == "") {
                    showErrorMessageBelowCtrl("OptionBanglaC", "", false);
                    showErrorMessageBelowCtrl("OptionBanglaC", "The Option C field is required.", true);
                }
                else if (optionEnglishC == "" && optionBanglaC != "") {
                    showErrorMessageBelowCtrl("OptionEnglishC", "", false);
                    showErrorMessageBelowCtrl("OptionEnglishC", "The Option C field is required.", true);
                }
            }

            if ($("#OptionEnglishD").length > 0 && $("#OptionBanglaD").length > 0) {
                if (optionEnglishD == "" && optionBanglaD == "") {
                    showErrorMessageBelowCtrl("OptionBanglaD", "", false);
                    showErrorMessageBelowCtrl("OptionEnglishD", "", false);
                }
                else if (optionEnglishD != "" && optionBanglaD != "") {
                    showErrorMessageBelowCtrl("OptionBanglaD", "", false);
                    showErrorMessageBelowCtrl("OptionEnglishD", "", false);
                }
                else if (optionEnglishD != "" && optionBanglaD == "") {
                    showErrorMessageBelowCtrl("OptionBanglaD", "", false);
                    showErrorMessageBelowCtrl("OptionBanglaD", "The Option D field is required.", true);
                }
                else if (optionEnglishD == "" && optionBanglaD != "") {
                    showErrorMessageBelowCtrl("OptionEnglishD", "", false);
                    showErrorMessageBelowCtrl("OptionEnglishD", "The Option D field is required.", true);
                }
                if (optionEnglishD != "" || optionBanglaD != "") {
                    if (optionBanglaC == "") {
                        showErrorMessageBelowCtrl("OptionBanglaC", "", false);
                        showErrorMessageBelowCtrl("OptionBanglaC", "The Option C field is required.", true);
                    }
                    if (optionEnglishC == "") {
                        showErrorMessageBelowCtrl("OptionEnglishC", "", false);
                        showErrorMessageBelowCtrl("OptionEnglishC", "The Option C field is required.", true);
                    }
                }
            }
            if ($("#OptionEnglishE").length > 0 && $("#OptionBanglaE").length > 0) {
                if (optionEnglishE == "" && optionBanglaE == "") {
                    showErrorMessageBelowCtrl("OptionBanglaE", "", false);
                    showErrorMessageBelowCtrl("OptionEnglishE", "", false);
                }
                else if (optionEnglishE != "" && optionBanglaE != "") {
                    showErrorMessageBelowCtrl("OptionBanglaE", "", false);
                    showErrorMessageBelowCtrl("OptionEnglishE", "", false);
                }
                else if (optionEnglishE != "" && optionBanglaE == "") {
                    showErrorMessageBelowCtrl("OptionBanglaE", "", false);
                    showErrorMessageBelowCtrl("OptionBanglaE", "The Option E field is required.", true);
                }
                else if (optionEnglishE == "" && optionBanglaE != "") {
                    showErrorMessageBelowCtrl("OptionEnglishE", "", false);
                    showErrorMessageBelowCtrl("OptionEnglishE", "The Option E field is required.", true);
                }
                if (optionEnglishE != "" || optionBanglaE != "") {
                    if (optionBanglaD == "") {
                        showErrorMessageBelowCtrl("OptionBanglaD", "", false);
                        showErrorMessageBelowCtrl("OptionBanglaD", "The Option D field is required.", true);
                    }
                    if (optionEnglishD == "") {
                        showErrorMessageBelowCtrl("OptionEnglishD", "", false);
                        showErrorMessageBelowCtrl("OptionEnglishD", "The Option D field is required.", true);
                    }
                }
            }
            // CKEDITOR.instances[i].updateElement()
        });

    }
}

$(document).on("input propertychange", "#OptionBanglaC,#OptionEnglishC,#OptionBanglaD,#OptionEnglishD,#OptionBanglaE,#OptionEnglishE", function (event) {
    var optionBanglaC = CKEDITOR.instances['OptionBanglaC'].getData();
    var optionEnglishC = CKEDITOR.instances['OptionEnglishC'].getData();
    var optionBanglaD = CKEDITOR.instances['OptionBanglaD'].getData();
    var optionEnglishD = CKEDITOR.instances['OptionEnglishD'].getData();
    //var optionBanglaE = CKEDITOR.instances['OptionBanglaE'].getData();
    //var optionEnglishE = CKEDITOR.instances['OptionEnglishE'].getData();

    if ($("#OptionEnglishC").length > 0 && $("#OptionBanglaC").length > 0) {
        if (optionEnglishC == "" && optionBanglaC == "") {
            showErrorMessageBelowCtrl("OptionBanglaC", "", false);
            showErrorMessageBelowCtrl("OptionEnglishC", "", false);
        }
        else if (optionEnglishC != "" && optionBanglaC != "") {
            showErrorMessageBelowCtrl("OptionBanglaC", "", false);
            showErrorMessageBelowCtrl("OptionEnglishC", "", false);
        }
        else if (optionEnglishC != "" && optionBanglaC == "") {
            showErrorMessageBelowCtrl("OptionBanglaC", "", false);
            showErrorMessageBelowCtrl("OptionBanglaC", "The Option C field is required.", true);
        }
        else if (optionEnglishC == "" && optionBanglaC != "") {
            showErrorMessageBelowCtrl("OptionEnglishC", "", false);
            showErrorMessageBelowCtrl("OptionEnglishC", "The Option C field is required.", true);
        }
    }

    if ($("#OptionEnglishD").length > 0 && $("#OptionBanglaD").length > 0) {
        if (optionEnglishD == "" && optionBanglaD == "") {
            showErrorMessageBelowCtrl("OptionBanglaD", "", false);
            showErrorMessageBelowCtrl("OptionEnglishD", "", false);
        }
        else if (optionEnglishD != "" && optionBanglaD != "") {
            showErrorMessageBelowCtrl("OptionBanglaD", "", false);
            showErrorMessageBelowCtrl("OptionEnglishD", "", false);
        }
        else if (optionEnglishD != "" && optionBanglaD == "") {
            showErrorMessageBelowCtrl("OptionBanglaD", "", false);
            showErrorMessageBelowCtrl("OptionBanglaD", "The Option D field is required.", true);
        }
        else if (optionEnglishD == "" && optionBanglaD != "") {
            showErrorMessageBelowCtrl("OptionEnglishD", "", false);
            showErrorMessageBelowCtrl("OptionEnglishD", "The Option D field is required.", true);
        }
    }
    if ($("#OptionEnglishE").length > 0 && $("#OptionBanglaE").length > 0) {
        if (optionEnglishE == "" && optionBanglaE == "") {
            showErrorMessageBelowCtrl("OptionBanglaE", "", false);
            showErrorMessageBelowCtrl("OptionEnglishE", "", false);
        }
        else if (optionEnglishE != "" && optionBanglaE != "") {
            showErrorMessageBelowCtrl("OptionBanglaE", "", false);
            showErrorMessageBelowCtrl("OptionEnglishE", "", false);
        }
        else if (optionEnglishE != "" && optionBanglaE == "") {
            showErrorMessageBelowCtrl("OptionBanglaE", "", false);
            showErrorMessageBelowCtrl("OptionBanglaE", "The Option E field is required.", true);
        }
        else if (optionEnglishE == "" && optionBanglaE != "") {
            showErrorMessageBelowCtrl("OptionEnglishE", "", false);
            showErrorMessageBelowCtrl("OptionEnglishE", "The Option E field is required.", true);
        }
    }
});

$(document).on('input propertychange', '#CorrectAnswer',
       function () {
           var node = $(this);
           node.val(node.val().replace(/[^a-dA-D]/g, ''));
           $(this).val($(this).val().toUpperCase());
           $(this).val(removeDuplicateCharacters($(this).val()));
       });
function removeDuplicateCharacters(string) {
    return string
       .split('')
       .filter(function (item, pos, self) {
           return self.indexOf(item) == pos;
       })
       .join('');

}
$(document).on("click", "#saveNew,#saveExist", function (event) {
    var btnId = $(this).attr('id');
    var check = true;
    mcqQuestionFilterViewModel = {};

    var organizationId = mcqQuestionFilterViewModel["Organization"] = $("#Organization").val();
    var programId = mcqQuestionFilterViewModel["Program"] = $("#Program").val();
    var sessionId = mcqQuestionFilterViewModel["Session"] = $("#Session").val();
    var course = mcqQuestionFilterViewModel["Course"] = $("#Course").val();
    var examCode = mcqQuestionFilterViewModel["ExamCode"] = $("#ExamCode").val();
    var examId = mcqQuestionFilterViewModel["ExamId"] = $("#ExamId").val();
    var examName = mcqQuestionFilterViewModel["ExamName"] = $("#ExamName").val();
    var subjectId = mcqQuestionFilterViewModel["SubjectId"] = $("#SubjectId").val();
    var uniqueSet = mcqQuestionFilterViewModel["UniqueSet"] = $("#UniqueSet").val();
    var correctAnswer = $("#CorrectAnswer").val();


    if (organizationId == "" || organizationId == null) {
        showErrorMessageBelowCtrl("Organization", "", false);
        showErrorMessageBelowCtrl("Organization", "The Organization field is required.", true);
        check = false;
    }

    if (programId == "" || programId == null) {
        showErrorMessageBelowCtrl("Program", "", false);
        showErrorMessageBelowCtrl("Program", "The Program field is required.", true);
        check = false;
    }

    if (sessionId == "" || sessionId == null) {
        showErrorMessageBelowCtrl("Session", "", false);
        showErrorMessageBelowCtrl("Session", "The Session field is required.", true);
        check = false;
    }

    if (course == "" || course == null) {
        showErrorMessageBelowCtrl("Course", "", false);
        showErrorMessageBelowCtrl("Course", "The Course field is required.", true);
        check = false;
    }


    if (examCode == "" || examCode == null) {
        showErrorMessageBelowCtrl("ExamCode", "", false);
        showErrorMessageBelowCtrl("ExamCode", "The Exam Code field is required.", true);
        check = false;
    }

    if (subjectId == "" || subjectId == null) {
        showErrorMessageBelowCtrl("SubjectId", "", false);
        showErrorMessageBelowCtrl("SubjectId", "The Subject field is required.", true);
        check = false;
    }

    if (uniqueSet == "" || uniqueSet == null) {
        showErrorMessageBelowCtrl("UniqueSet", "", false);
        showErrorMessageBelowCtrl("UniqueSet", "The Unique Set field is required.", true);
        check = false;
    }

    if (correctAnswer == "" || correctAnswer == null) {
        showErrorMessageBelowCtrl("CorrectAnswer", "", false);
        showErrorMessageBelowCtrl("CorrectAnswer", "The Correct Answer field is required.", true);
        check = false;
    }
    var suffle = 0;
    if ($("#Shuffle").is(":checked")) {
        suffle = 1;
    }


    var mcqQuestionList = [];

    var isBangla = false;
    var isEnglish = false;
    if ($("#banglaDiv").length > 0) {
        isBangla = true;
    }
    if ($("#englishDiv").length > 0) {
        isEnglish = true;
    }
    //var value = CKEDITOR.instances['DOM-ID-HERE'].getData()
    if (isBangla && isEnglish) {
        mcqQuestionBangla = {}
        mcqQuestionBangla["Version"] = $("#banglaDiv").attr("data-version");
        mcqQuestionBangla["CorrectAnswer"] = correctAnswer;
        mcqQuestionBangla["IsShuffle"] = suffle;

        var QuestionBangla = mcqQuestionBangla["QuestionText"] = CKEDITOR.instances['QuestionBangla'].getData(); //$("#QuestionBangla").val();
        if (QuestionBangla == "" || QuestionBangla == null) {
            showErrorMessageBelowCtrl("QuestionBangla", "", false);
            showErrorMessageBelowCtrl("QuestionBangla", "The Question field is required.", true);
            check = false;
        }

        var OptionBanglaA = mcqQuestionBangla["OptionA"] = CKEDITOR.instances['OptionBanglaA'].getData(); //$("#OptionBanglaA").val();
        if (OptionBanglaA == "" || OptionBanglaA == null) {
            showErrorMessageBelowCtrl("OptionBanglaA", "", false);
            showErrorMessageBelowCtrl("OptionBanglaA", "The Option A field is required.", true);
            check = false;
        }

        var OptionBanglaB = mcqQuestionBangla["OptionB"] = CKEDITOR.instances['OptionBanglaB'].getData();  //$("#OptionBanglaB").val();
        if (OptionBanglaB == "" || OptionBanglaB == null) {
            showErrorMessageBelowCtrl("OptionBanglaB", "", false);
            showErrorMessageBelowCtrl("OptionBanglaB", "The Option B field is required.", true);
            check = false;
        }

        var OptionBanglaC = mcqQuestionBangla["OptionC"] = CKEDITOR.instances['OptionBanglaC'].getData();  //$("#OptionBanglaC").val();
        var OptionBanglaD = mcqQuestionBangla["OptionD"] = CKEDITOR.instances['OptionBanglaD'].getData();  //$("#OptionBanglaD").val();
        //var OptionBanglaE = mcqQuestionBangla["OptionE"] = CKEDITOR.instances['OptionBanglaE'].getData();  //$("#OptionBanglaD").val();
        mcqQuestionBangla["OptionE"] = "";

        mcqQuestionEnglish = {}
        mcqQuestionEnglish["CorrectAnswer"] = correctAnswer;
        mcqQuestionEnglish["IsShuffle"] = suffle;
        mcqQuestionEnglish["Version"] = $("#englishDiv").attr("data-version");

        var QuestionEnglish = mcqQuestionEnglish["QuestionText"] = CKEDITOR.instances['QuestionEnglish'].getData();  //$("#QuestionEnglish").val();
        if (QuestionEnglish == "" || QuestionEnglish == null) {
            showErrorMessageBelowCtrl("QuestionEnglish", "", false);
            showErrorMessageBelowCtrl("QuestionEnglish", "The Question field is required.", true);
            check = false;
        }

        var OptionEnglishA = mcqQuestionEnglish["OptionA"] = CKEDITOR.instances['OptionEnglishA'].getData();  //$("#OptionEnglishA").val();
        if (OptionEnglishA == "" || OptionEnglishA == null) {
            showErrorMessageBelowCtrl("OptionEnglishA", "", false);
            showErrorMessageBelowCtrl("OptionEnglishA", "The Option A field is required.", true);
            check = false;
        }

        var OptionEnglishB = mcqQuestionEnglish["OptionB"] = CKEDITOR.instances['OptionEnglishB'].getData();  //$("#OptionEnglishB").val();
        if (OptionEnglishB == "" || OptionEnglishB == null) {
            showErrorMessageBelowCtrl("OptionEnglishB", "", false);
            showErrorMessageBelowCtrl("OptionEnglishB", "The Option B field is required.", true);
            check = false;
        }

        var OptionEnglishC = mcqQuestionEnglish["OptionC"] = CKEDITOR.instances['OptionEnglishC'].getData(); //$("#OptionEnglishC").val();
        var OptionEnglishD = mcqQuestionEnglish["OptionD"] = CKEDITOR.instances['OptionEnglishD'].getData();  //$("#OptionEnglishD").val();
        //var OptionEnglishE = mcqQuestionEnglish["OptionE"] = CKEDITOR.instances['OptionEnglishE'].getData();  //$("#OptionEnglishD").val();
        mcqQuestionEnglish["OptionE"] = "";


        if (OptionBanglaC != "" && OptionEnglishC == "") {
            showErrorMessageBelowCtrl("OptionEnglishC", "", false);
            showErrorMessageBelowCtrl("OptionEnglishC", "The Option C field is required.", true);
            check = false;
        }
        if (OptionBanglaC == "" && OptionEnglishC != "") {
            showErrorMessageBelowCtrl("OptionBanglaC", "", false);
            showErrorMessageBelowCtrl("OptionBanglaC", "The Option C field is required.", true);
            check = false;
        }

        if (OptionBanglaD != "" && OptionEnglishD == "") {
            showErrorMessageBelowCtrl("OptionEnglishD", "", false);
            showErrorMessageBelowCtrl("OptionEnglishD", "The Option D field is required.", true);
            check = false;
        }
        if (OptionBanglaD == "" && OptionEnglishD != "") {
            showErrorMessageBelowCtrl("OptionBanglaD", "", false);
            showErrorMessageBelowCtrl("OptionBanglaD", "The Option D field is required.", true);
            check = false;
        }
        //if (OptionBanglaE != "" && OptionEnglishE == "") {
        //    showErrorMessageBelowCtrl("OptionEnglishE", "", false);
        //    showErrorMessageBelowCtrl("OptionEnglishE", "The Option E field is required.", true);
        //    check = false;
        //}
        //if (OptionBanglaE == "" && OptionEnglishE != "") {
        //    showErrorMessageBelowCtrl("OptionBanglaE", "", false);
        //    showErrorMessageBelowCtrl("OptionBanglaE", "The Option E field is required.", true);
        //    check = false;
        //}
        mcqQuestionList.push(mcqQuestionBangla);
        mcqQuestionList.push(mcqQuestionEnglish);

    }
    else if (isBangla && !isEnglish) {
        mcqQuestionBangla = {}
        mcqQuestionBangla["Version"] = $("#banglaDiv").attr("data-version");
        mcqQuestionBangla["CorrectAnswer"] = correctAnswer;
        mcqQuestionBangla["IsShuffle"] = suffle;

        var QuestionBangla = mcqQuestionBangla["QuestionText"] = CKEDITOR.instances['QuestionBangla'].getData();
        if (QuestionBangla == "" || QuestionBangla == null) {
            showErrorMessageBelowCtrl("QuestionBangla", "", false);
            showErrorMessageBelowCtrl("QuestionBangla", "The Question field is required.", true);
            check = false;
        }

        var OptionBanglaA = mcqQuestionBangla["OptionA"] = CKEDITOR.instances['OptionBanglaA'].getData();  //$("#OptionBanglaA").val();
        if (OptionBanglaA == "" || OptionBanglaA == null) {
            showErrorMessageBelowCtrl("OptionBanglaA", "", false);
            showErrorMessageBelowCtrl("OptionBanglaA", "The Option A field is required.", true);
            check = false;
        }

        var OptionBanglaB = mcqQuestionBangla["OptionB"] = CKEDITOR.instances['OptionBanglaB'].getData();  //$("#OptionBanglaB").val();
        if (OptionBanglaB == "" || OptionBanglaB == null) {
            showErrorMessageBelowCtrl("OptionBanglaB", "", false);
            showErrorMessageBelowCtrl("OptionBanglaB", "The Option B field is required.", true);
            check = false;
        }

        var OptionBanglaC = mcqQuestionBangla["OptionC"] = CKEDITOR.instances['OptionBanglaC'].getData();  //$("#OptionBanglaC").val();
        var OptionBanglaD = mcqQuestionBangla["OptionD"] = CKEDITOR.instances['OptionBanglaD'].getData();  //$("#OptionBanglaD").val();
        //var OptionBanglaE = mcqQuestionBangla["OptionE"] = CKEDITOR.instances['OptionBanglaE'].getData();  //$("#OptionBanglaD").val();
        mcqQuestionBangla["OptionE"] = "";
        mcqQuestionList.push(mcqQuestionBangla);

    }
    else if (!isBangla && isEnglish) {
        mcqQuestionEnglish = {}
        mcqQuestionEnglish["CorrectAnswer"] = correctAnswer;
        mcqQuestionEnglish["IsShuffle"] = suffle;
        mcqQuestionEnglish["Version"] = $("#englishDiv").attr("data-version");

        var QuestionEnglish = mcqQuestionEnglish["QuestionText"] = CKEDITOR.instances['QuestionBangla'].getData();  //$("#QuestionEnglish").val();
        if (QuestionEnglish == "" || QuestionEnglish == null) {
            showErrorMessageBelowCtrl("QuestionEnglish", "", false);
            showErrorMessageBelowCtrl("QuestionEnglish", "The Question field is required.", true);
            check = false;
        }

        var OptionEnglishA = mcqQuestionEnglish["OptionA"] = CKEDITOR.instances['QuestionBangla'].getData(); // $("#OptionEnglishA").val();
        if (OptionEnglishA == "" || OptionEnglishA == null) {
            showErrorMessageBelowCtrl("OptionEnglishA", "", false);
            showErrorMessageBelowCtrl("OptionEnglishA", "The Option A field is required.", true);
            check = false;
        }

        var OptionEnglishB = mcqQuestionEnglish["OptionB"] = CKEDITOR.instances['QuestionBangla'].getData();  //$("#OptionEnglishB").val();
        if (OptionEnglishB == "" || OptionEnglishB == null) {
            showErrorMessageBelowCtrl("OptionEnglishB", "", false);
            showErrorMessageBelowCtrl("OptionEnglishB", "The Option B field is required.", true);
            check = false;
        }

        var OptionEnglishC = mcqQuestionEnglish["OptionC"] = CKEDITOR.instances['OptionEnglishC'].getData(); //$("#OptionEnglishC").val();
        var OptionEnglishD = mcqQuestionEnglish["OptionD"] = CKEDITOR.instances['OptionEnglishD'].getData();  //$("#OptionEnglishD").val();
        //var OptionEnglishE = mcqQuestionEnglish["OptionE"] = CKEDITOR.instances['OptionEnglishE'].getData();  //$("#OptionEnglishD").val();
        mcqQuestionEnglish["OptionE"] = "";
        
        mcqQuestionList.push(mcqQuestionEnglish);
    }
    else {
        check = false;
    }


    mcqQuestionFilterViewModel["McqQuestionList"] = mcqQuestionList;

    if (check) {
        var data = JSON.stringify(mcqQuestionFilterViewModel);
        $.ajax({
            type: "post",
            url: $("body").attr("data-project-root") + "Exam/Question/SubmitQuestion",
            cache: false,
            async: true,
            dataType: "json",
            contentType: "application/json",
            data: data,
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (result) {
                $.unblockUI();
                if (result.IsSuccess) {
                    if (btnId == "saveExist") {
                        var link = "/Exam/Question/Index?editSuccess=" + "Question Saved.";
                        console.log(link);
                        window.location.href = link;
                    }

                    $.fn.customMessage({
                        displayMessage: "Success",
                        displayMessageType: "s",
                    });

                    $("#questionEntryDiv").empty();
                    $("#questionEntryDiv").append("<div>Please Fillup Question details criteria.</div>");
                    responseReturn();
                }
                else {
                    $.fn.customMessage({
                        displayMessage: result.Message,
                        displayMessageType: "error",
                    });
                }
                $("body, html").animate({
                    scrollTop: $("#bodyContent").offset().top
                }, 600);
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.unblockUI();
                console.log("Failed -----");
            }
        });
    }

});
