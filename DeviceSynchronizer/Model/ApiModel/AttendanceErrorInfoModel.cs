﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceSynchronizer.Model.ApiModel
{
    public class AttendanceErrorInfoModel
    {
        public int EnrollNo { get; set; }
        public long DeviceId { get; set; }
        public int Pin { get; set; }
        public DateTime LogTime { get; set; }
    }
}
