﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services.Description;
using log4net;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using NHibernate.Dialect.Function;
using NHibernate.Util;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.Areas.UInventory.Models;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;
using Constants = UdvashERP.BusinessRules.Constants;
using ReportType = UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel.ReportType;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("UInventory")]
    public class GoodsReturnController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ISupplierService _supplierService;
        private readonly IBankService _bankService;
        private readonly IBankBranchService _bankBranchService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IItemGroupService _itemGroupService;
        private readonly IItemService _itemService;
        private readonly ISupplierItemService _supplierItemService;
        private readonly ISupplierBankDetailService _supplierBankBranchDetails;
        private readonly IGoodsReceiveService _goodsReceiveService;
        private readonly IGoodsReturnService _goodsReturnService;
        private readonly IGoodsIssueDetailsService _goodsReturnDetailsService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IBranchService _branchService;
        private readonly ICurrentStockSummaryService _currentStockSummaryService;
        private List<UserMenu> _userMenu;
        public GoodsReturnController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _supplierService = new SupplierService(session);
                _bankService = new BankService(session);
                _bankBranchService = new BankBranchService(session);
                _organizationService = new OrganizationService(session);
                _itemGroupService = new ItemGroupService(session);
                _itemService = new ItemService(session);
                _supplierItemService = new SupplierItemService(session);
                _supplierBankBranchDetails = new SupplierBankDetailsService(session);
                _goodsReceiveService = new GoodsReceiveService(session);
                _goodsReturnService = new GoodsReturnService(session);
                _goodsReturnDetailsService = new GoodsIssueDetailsService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _branchService = new BranchService(session);
                _currentStockSummaryService=new CurrentStockSummaryService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion

        #region Details

        public ActionResult Details(long id)
        {
            try
            {
                var goodsReturnDetails = _goodsReturnDetailsService.GetGoodsReturnDetails(id);
                var goodsReceive = goodsReturnDetails.GoodsReturn.GoodsReceive;
                var firstGoodsRecDetails = goodsReceive.GoodsReceiveDetails[0];
                var goodReturnObj = new GoodsReturnViewModel
                {
                    OrganizationShortName = goodsReceive.Branch.Organization.ShortName,
                    OrganizationFullName = goodsReceive.Branch.Organization.Name,
                    Branch = goodsReceive.Branch.Name,
                    GoodsReturnDate = DateTime.Now.ToString("MMM dd, yyyy"),
                    GoodsReturnNo = goodsReturnDetails.GoodsReturn.GoodsReturnNo,
                    Purpose =
                        firstGoodsRecDetails.Purpose != null
                            ? _commonHelper.GetEmumIdToValue<Purpose>(firstGoodsRecDetails.Purpose.Value)
                            : "N/A",
                    SupplierName = goodsReceive.Supplier.Name,
                    ItemGroup = firstGoodsRecDetails.Item.ItemGroup.Name,
                    ItemName = firstGoodsRecDetails.Item.Name,
                    ProgramSession =
                        firstGoodsRecDetails.Program != null
                            ? firstGoodsRecDetails.Program.Name + " " + firstGoodsRecDetails.Session.Name
                            : "N/A",
                    Remarks = goodsReceive.Remarks,
                    GoodsReceivedId = goodsReceive.Id
                };
                goodReturnObj.QuantityRateDetails = new List<QuantityRateDetailsViewModel>();
                QuantityRateDetailsViewModel detailsViewModel = new QuantityRateDetailsViewModel();
                int sum = 0;
                foreach (var x in goodsReceive.GoodsReceiveDetails)
                {
                    if (x.Item.Id == goodsReturnDetails.Item.Id)
                        sum += x.ReceivedQuantity;
                }
                detailsViewModel.ReceivedQuantity = sum.ToString();
                int sum1 = 0;
                foreach (var x in goodsReceive.GoodsReturnList)
                {
                    foreach (var goodsReturnDetail in x.GoodsReturnDetails)
                    {
                        if (goodsReturnDetail.Id < goodsReturnDetails.Id)
                        {
                            sum1 += goodsReturnDetail.ReturnedQuantity;
                        }
                    }
                }
                detailsViewModel.PreviouslyReturnedQuantity = sum1.ToString();
                detailsViewModel.CurrentlyReturnedQuantity = goodsReturnDetails.ReturnedQuantity.ToString();
                goodReturnObj.QuantityRateDetails.Add(detailsViewModel);
                ViewBag.UserName = HttpContext.User.Identity.GetUserName();
                return View("PrintGoodsReturn", goodReturnObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Index/Manage

        public ActionResult Index()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                GetStatusText();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                GetStatusText();
                return View();
            }
        }

        #region Render Data Table

        public JsonResult GoodsReturnList(int draw, int start, int length, long? organizationId = null, long? branchId = null, string item = "", string goodsReturnDate = "", int? status = null)
        {
            var getData = new List<object>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                #region OrderBy and Direction

                string orderBy = "";
                string orderDir = "";

                #endregion

                DateTime? goodsReturnDateValue = !string.IsNullOrEmpty(goodsReturnDate) ? Convert.ToDateTime(goodsReturnDate) : (DateTime?)null;
                int recordsTotal = _goodsReturnService.GetGoodsReturnCount(_userMenu, organizationId, branchId, item, goodsReturnDateValue, status);
                long recordsFiltered = recordsTotal;
                List<GoodsReturnDto> goodsReturnList = _goodsReturnService.LoadGoodsReturn(start, length, orderBy, orderDir.ToUpper(), _userMenu, organizationId, branchId, item, goodsReturnDateValue, status).ToList();
                getData.AddRange(goodsReturnList.Select(goodsReturnDto => new List<string>
                {
                    goodsReturnDto.OrganizationShortName,
                    goodsReturnDto.Branch,
                    goodsReturnDto.Item,
                    goodsReturnDto.GoodsReceiveDate.ToString("MMM dd, yyyy"),
                    goodsReturnDto.GoodsReceiveNo,
                    goodsReturnDto.GoodsReturnDate.ToString("MMM dd, yyyy"),
                    goodsReturnDto.GoodsReturnNo.ToString(CultureInfo.InvariantCulture),
                    goodsReturnDto.Status,
                    LinkGenerator.GetDetailsLink("Details", "GoodsReturn", goodsReturnDto.GoodsReturnDetailsId)
                }));
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData,
                    isSuccess = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }

        #endregion

        #endregion

        #region reports

        public ActionResult GoodsReturnReport()
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var authorgList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.organizationList = authorgList;
                ViewBag.branchIdList = new SelectList(string.Empty, "Value", "Text");
                var itemGroupList = _itemGroupService.LoadItemGroup();
                itemGroupList.Insert(0, new ItemGroup() { Id = 0, Name = "All selected" });
                ViewBag.itemGroupList = new SelectList(itemGroupList, "Id", "Name");
                ViewBag.itemList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.programSessionList = new SelectList(string.Empty, "Value", "Text");
                ViewBag.SupplierList = new SelectList(string.Empty, "Value", "Text");
                var purposeDictionary = _commonHelper.LoadEmumToDictionary<Purpose>();
                purposeDictionary =
                    new Dictionary<int, string> { { 0, "All selected" }, { -1, "N/A" } }.Concat(purposeDictionary)
                        .ToDictionary(k => k.Key, v => v.Value);
                ViewBag.perposeList = new SelectList(purposeDictionary, "Key", "Value");
                var reportTypeDictionary = _commonHelper.LoadEmumToDictionary<ReportType>();
                ViewBag.ReportTypeList = new SelectList(reportTypeDictionary, "Key", "Value");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        public ActionResult GoodsReturnReportPost(GoodsReturnReportFormViewModel goodsReturnReport)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    var programSession = "";
                    if (goodsReturnReport.ProgramSession == null)
                    {
                        programSession = "All Program & Session";
                    }
                    else if (goodsReturnReport.ProgramSession.Contains("0"))
                    {
                        programSession = "All Program & Session";
                    }
                    var purpose = "";
                    if (goodsReturnReport.Purpose == null)
                    {
                        purpose = "All Purposes";
                    }
                    else if (goodsReturnReport.Purpose.Where(x => (int)x == SelectionType.SelelectAll).Any())
                    {
                        purpose = "All Purposes";
                    }
                    ViewBag.organizationName = _organizationService.LoadById(goodsReturnReport.OrganizationId).Name;
                    var branchIdList = new List<long>();
                    if (goodsReturnReport.BranchId != null)
                    {
                        branchIdList = goodsReturnReport.BranchId.ToList();
                        if (!goodsReturnReport.BranchId.Contains(SelectionType.SelelectAll))
                        {
                            ViewBag.branchName = string.Join(", ",
                                _branchService.LoadBranch(goodsReturnReport.BranchId).Select(x => x.Name).ToList());
                        }
                        else
                            ViewBag.branchName = "All Branch";
                    }
                    else
                        ViewBag.branchName = "All Branch";
                    ViewBag.itemGroup = goodsReturnReport.ItemGroupId != null && !goodsReturnReport.ItemGroupId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _itemGroupService.LoadItemGroup(goodsReturnReport.ItemGroupId.ToList()).Select(x => x.Name).ToList()) : "All Item Groups";
                    ViewBag.item = goodsReturnReport.ItemId != null && !goodsReturnReport.ItemId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _itemService.LoadItemList(goodsReturnReport.ItemId).Select(x => x.Name).ToList()) : "All Items";
                    ViewBag.SupplierName = goodsReturnReport.SupplierId != null && !goodsReturnReport.SupplierId.Contains(SelectionType.SelelectAll) ? string.Join(", ", _supplierService.LoadSupplierByIds(goodsReturnReport.SupplierId).Select(x => x.Name).ToList()) : "All Supplier";
                    ViewBag.dateFrom = goodsReturnReport.DateFrom.ToString("MMM dd, yyyy");
                    ViewBag.dateTo = goodsReturnReport.DateTo.ToString("MMM dd, yyyy");
                    List<long> programIdList = new List<long>();
                    List<long> sessionIdList = new List<long>();
                    if (goodsReturnReport.ProgramSession != null)
                    {
                        foreach (var programsession in goodsReturnReport.ProgramSession.Where(x => x != "0" && x != "-1"))
                        {
                            string[] programSessionArray = programsession.Split(new[] { "::" }, StringSplitOptions.None);
                            Program program = _programService.GetProgram(Convert.ToInt64(programSessionArray[0]));
                            Session session = _sessionService.LoadById(Convert.ToInt64(programSessionArray[1]));
                            programIdList.Add(program.Id);
                            sessionIdList.Add(session.Id);
                            if (!goodsReturnReport.ProgramSession.Contains("0"))
                            {
                                string programName = program.Name;
                                string sessionName = session.Name;
                                string programSessionName = programName + "&" + sessionName + ", ";
                                programSession += programSessionName;

                            }
                        }
                        if (goodsReturnReport.ProgramSession.Contains("-1"))
                        {
                            programIdList.Add(-1);
                            sessionIdList.Add(-1);
                            if (!goodsReturnReport.ProgramSession.Contains("0"))
                            {
                                programSession += ", N/A";
                            }
                        }

                    }
                    programSession = programSession.Trim(' ', ',');
                    ViewBag.ProgramIdList = programIdList;
                    ViewBag.SessionIdList = sessionIdList;
                    if (goodsReturnReport.Purpose != null)
                    {
                        if (!goodsReturnReport.Purpose.Where(x => (int)x == SelectionType.SelelectAll).Any())
                        {
                            foreach (var purposeValue in goodsReturnReport.Purpose)
                            {
                                purpose += _commonHelper.GetEmumIdToValue<Purpose>((int)purposeValue) + ", ";
                            }
                            if (goodsReturnReport.Purpose.Where(x => (int)x == -1).Any())
                            {
                                purpose += " N/A";
                            }
                        }

                        purpose = purpose.Trim(' ', ',');
                    }
                    ViewBag.programSession = programSession;
                    ViewBag.purpose = purpose;
                    IList<string> columnList = new List<string>();
                    if (goodsReturnReport.ReportType == 1)
                    {
                        columnList = _branchService.LoadAuthorizedBranchNameList(_userMenu, _commonHelper.ConvertIdToList(goodsReturnReport.OrganizationId), branchIdList);
                    }
                    else if (goodsReturnReport.ReportType == 2)
                    {
                        columnList = _goodsReturnService.LoadGoodsReturnProgramSessionNameList(_userMenu, goodsReturnReport.OrganizationId, branchIdList, programIdList, sessionIdList);
                    }
                    ViewBag.ColumnList = columnList;
                    return PartialView("Partial/_GoodsReturnReportPartialView", goodsReturnReport);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View("GoodsReturnReport");
        }

        #region Render Data Table

        [HttpPost]
        public JsonResult GoodsReturnReportAjaxRequest(int draw, int start, int length, long organizationId,
            List<long> branchIdList
            , List<long> purposeList, List<long> itemGroupIdList, List<long> itemIdList,
            List<long> programIdList, List<long> sessionIdList, List<long> supplierIdList, string dateFrom, string dateTo,
            List<string> columnList, int reportType, List<string> programsession)
        {
            var getData = new List<object>();
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                DateTime fromDate = Convert.ToDateTime(dateFrom);
                DateTime toDate = Convert.ToDateTime(dateTo);
                IList<dynamic> goodsReturnList =
                    _goodsReturnService.LoadGoodsReturnReport(start, length, userMenu, organizationId, branchIdList
                        , purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, supplierIdList,
                        fromDate, toDate, columnList, reportType, programsession).ToList();
                long recordsTotal = _goodsReturnService.GetGoodsReturnReportCount(userMenu, organizationId,
                    branchIdList
                    , purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList, supplierIdList,
                    fromDate, toDate, columnList, reportType, programsession);
                long recordsFiltered = recordsTotal;
                long i = start + 1;

                foreach (var obj in goodsReturnList)
                {
                    var str = new List<string>();
                    IDictionary<string, object> dic;
                    str.Add(i.ToString());
                    foreach (KeyValuePair<string, object> entry in dic = obj)
                    {
                        str.Add(entry.Value != null ? entry.Value.ToString() : "0");
                    }
                    var sum = 0;
                    if (reportType == 1)
                    {
                        sum = str.Skip(4).Sum(x => Convert.ToInt32(x));
                        str.Add(sum.ToString());
                    }
                    else if (reportType == 2)
                    {
                        sum = str.Skip(3).ToList().Sum(x => Convert.ToInt32(x));
                        if (programsession.Contains("") || programsession.Contains("-1"))
                        {
                            str.Add(sum.ToString());
                        }
                        else
                        {
                            str[str.Count - 1] = sum.ToString();
                        }
                    }
                    getData.Add(str);
                    i++;
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = getData
                });
            }
        }

        #endregion

        #region Export

        //public ActionResult ExportGoodsReturnReport(string organizationString, string dateString, string branchString
        //    , string purposeString, string itemGroupString, string itemNameString,
        //    string programSessionString
        //    , string supplierString, long organizationId, List<long> branchIdList, List<long> purposeList
        //    , List<long> itemGroupIdList, List<long> itemIdList,
        //    List<long> programIdList, List<long> sessionIdList
        //    , List<long> supplierIdList, string dateFrom, string dateTo, List<string> columnList, int reportType,
        //    List<string> programsession)
        //{
        //    var excelList = new List<List<object>>();
        //    try
        //    {
        //        var userMenu = (List<UserMenu>) ViewBag.UserMenu;
        //        DateTime fromDate = Convert.ToDateTime(dateFrom);
        //        DateTime toDate = Convert.ToDateTime(dateTo);
        //        List<string> headerList = new List<string>();
        //        headerList.Add(organizationString);
        //        headerList.Add("Work Order Report");
        //        List<string> footerList = new List<string>();
        //        footerList.Add("");
        //        columnList.Add("Branch");
        //        columnList.Add("Work Order Type");
        //        columnList.Add("Item Group");
        //        columnList.Add("Item Name");
        //        columnList.Add("Program & Session");
        //        columnList.Add("Work Order Date");
        //        columnList.Add("Work Order Ref. No");
        //        columnList.Add("Status");
        //        int recordsTotal = _workOrderService.GetWorkOrderReportCount(userMenu, organizationId, branchIdList,
        //            workOrderType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList,
        //            workOrderstatus,
        //            fromDate, toDate);
        //        IList<WorkOrderReportDto> workOrderList =
        //            _workOrderService.LoadWorkOrderReport(0, recordsTotal, userMenu, organizationId, branchIdList,
        //                workOrderType, purposeList, itemGroupIdList, itemIdList, programIdList, sessionIdList,
        //                workOrderstatus,
        //                fromDate, toDate).ToList();
        //        foreach (var wol in workOrderList)
        //        {
        //            var xlsRow = new List<object>();
        //            xlsRow.Add(wol.Branch);
        //            xlsRow.Add(wol.WorkOrderType);
        //            xlsRow.Add(wol.ItemGroup);
        //            xlsRow.Add(wol.ItemName);
        //            xlsRow.Add(wol.ProgramSession);
        //            xlsRow.Add(wol.WorkOrderDate.ToString("yyyy-MM-dd"));
        //            xlsRow.Add(wol.WorkOrderRefNo);
        //            xlsRow.Add(wol.WorkOrderStatus);
        //            excelList.Add(xlsRow);
        //        }
        //        ExcelGenerator.GenerateExcel(headerList, columnList, excelList, footerList, "Work_Order_Report__"
        //                                                                                    +
        //                                                                                    DateTime.Now.ToString(
        //                                                                                        "yyyyMMdd-HHmmss"));
        //        return View("AutoClose");
        //    }
        //    catch (Exception ex)
        //    {
        //        //_logger.Error(ex.Message);
        //        return View("AutoClose");
        //    }
        //}

        #endregion

        #endregion

        #region Operational Function

        #region Save Operation

        public ActionResult GoodsReturn(string message = "", string goodsReceiveNo = "")
        {
            try
            {
                if (!String.IsNullOrEmpty(message))
                {
                    ViewBag.ErrorMessage = message;
                    ViewBag.goodsReceiveNo = goodsReceiveNo;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult GoodsReturn(string goodsReceiveNo)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (!string.IsNullOrEmpty(goodsReceiveNo))
                {
                    var goodReturnObj = new GoodsReturnViewModel();
                    var goodsReceive = _goodsReceiveService.GetGoodsReceive(goodsReceiveNo);
                    if (goodsReceive == null)
                    {
                        //return RedirectToAction("GoodsReturn",new { message = "Goods receive not found", goodsReceiveNo = goodsReceiveNo });
                        goodReturnObj.ErrorMessage = "Goods receive not found";
                        return PartialView("GoodsReturnPartialView", null);
                    }
                    var goodsReceiveDetails = goodsReceive.GoodsReceiveDetails[0];
                    long? programId = null;
                    long? sessionId = null;
                    if (goodsReceiveDetails.Program != null)
                    {
                        programId = goodsReceiveDetails.Program.Id;
                    }
                    if (goodsReceiveDetails.Session != null)
                    {
                        sessionId = goodsReceiveDetails.Session.Id;
                    }
                    ViewBag.CurrentStockQuantity = _currentStockSummaryService.GetCurrentStockQuantity(_userMenu,
                        goodsReceiveDetails.Item.Id, goodsReceive.Branch.Id, programId, sessionId);
                    var firstGoodsRecDetails = goodsReceive.GoodsReceiveDetails[0];
                    goodReturnObj.OrganizationShortName = goodsReceive.Branch.Organization.ShortName;
                    goodReturnObj.OrganizationFullName = goodsReceive.Branch.Organization.Name;
                    goodReturnObj.Branch = goodsReceive.Branch.Name;
                    goodReturnObj.GoodsReturnDate = DateTime.Now.ToString("MMM dd, yyyy");
                    //goodReturnObj.GoodsReturnNo = goodsReceive.GoodsReceivedNo;
                    goodReturnObj.Purpose = firstGoodsRecDetails.Purpose != null ? _commonHelper.GetEmumIdToValue<Purpose>(firstGoodsRecDetails.Purpose.Value) : "N/A";
                    goodReturnObj.SupplierName = goodsReceive.Supplier.Name;
                    goodReturnObj.ItemGroup = firstGoodsRecDetails.Item.ItemGroup.Name;
                    goodReturnObj.ItemName = firstGoodsRecDetails.Item.Name;
                    goodReturnObj.ProgramSession = firstGoodsRecDetails.Program != null ? firstGoodsRecDetails.Program.Name + " " + firstGoodsRecDetails.Session.Name : "N/A";
                    goodReturnObj.Remarks = goodsReceive.Remarks;
                    goodReturnObj.GoodsReceivedId = goodsReceive.Id;
                    goodReturnObj.QuantityRateDetails = new List<QuantityRateDetailsViewModel>();
                    foreach (var goodsReceiveDetailse in goodsReceive.GoodsReceiveDetails)
                    {
                        QuantityRateDetailsViewModel detailsViewModel = new QuantityRateDetailsViewModel();
                        detailsViewModel.ReceivedQuantity = goodsReceiveDetailse.ReceivedQuantity.ToString();
                        int sum = 0;
                        foreach (var goodsReturn in goodsReceive.GoodsReturnList)
                        {
                            foreach (var goodsReturnDetailse in goodsReturn.GoodsReturnDetails)
                            {
                                if (goodsReturnDetailse.Item.Id == goodsReceiveDetailse.Item.Id)
                                {
                                    sum += goodsReturnDetailse.ReturnedQuantity;
                                }
                            }
                        }
                        detailsViewModel.PreviouslyReturnedQuantity = sum.ToString();
                        detailsViewModel.CurrentlyReturnedQuantity = "";
                        detailsViewModel.ItemId = goodsReceiveDetailse.Item.Id;
                        goodReturnObj.QuantityRateDetails.Add(detailsViewModel);
                    }
                    return PartialView("GoodsReturnPartialView", goodReturnObj);
                }
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        [HttpPost]
        public ActionResult SaveGoodsReturn(GoodsReturnViewModel goodsReturnViewModel)
        {
            try
            {
                var goodsReturn = new GoodsReturn
                {
                    Remarks = goodsReturnViewModel.Remarks,
                    GoodsReceive = _goodsReceiveService.GetGoodsReceive(goodsReturnViewModel.GoodsReceivedId),
                    ReturnedQuantity = goodsReturnViewModel.QuantityRateDetails.Sum(x => Convert.ToInt32(x.CurrentlyReturnedQuantity))
                };
                foreach (var goodsReturnDetailse in goodsReturnViewModel.QuantityRateDetails)
                {
                    GoodsReturnDetails goodsReturnDetails = new GoodsReturnDetails();
                    goodsReturnDetails.Item = _itemService.LoadById(goodsReturnDetailse.ItemId);
                    goodsReturnDetails.ReturnedQuantity = Convert.ToInt32(goodsReturnDetailse.CurrentlyReturnedQuantity);
                    goodsReturnDetails.GoodsReturn = goodsReturn;
                    goodsReturn.GoodsReturnDetails.Add(goodsReturnDetails);
                }

                bool success = _goodsReturnService.SaveOrUpdate(goodsReturn);
                goodsReturnViewModel.GoodsReturnNo = goodsReturn.GoodsReturnNo;
                foreach (var qrd in goodsReturnViewModel.QuantityRateDetails)
                {
                    var previouslyReturnedQuantity = Convert.ToInt32(qrd.PreviouslyReturnedQuantity);
                    //                    if (previouslyReturnedQuantity == 0)
                    //                    {
                    //                        qrd.PreviouslyReturnedQuantity = "N/A";
                    //                    }
                    qrd.PreviouslyReturnedQuantity = previouslyReturnedQuantity.ToString();
                }
                ViewBag.UserName = HttpContext.User.Identity.GetUserName();

            }
            catch (OverflowException ex)
            {
                return RedirectToAction("GoodsReturn", new { message = ex.Message, goodsReceiveNo = ex.InnerException.Message });
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (NullObjectException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View("PrintGoodsReturn", goodsReturnViewModel);
        }

        #endregion

        #endregion

        #region Ajax functions

        public ActionResult LoadSupplierList(string[] itemIdList)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var supplierList = _supplierService.LoadAll();
                    if (supplierList != null && supplierList.Any())
                    {
                        supplierList.Insert(0, new Supplier { Id = 0, Name = "All Supplier" });
                        var list = new MultiSelectList(supplierList, "Id", "Name");
                        return Json(new { SupplierList = list, isSuccess = true });
                    }

                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
                }
            }
            return HttpNotFound();
        }

        #endregion

        #region Helper funtions

        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.LoadEmumToDictionary<GoodsReturnStatus>();
        }

        #endregion

    }
}
