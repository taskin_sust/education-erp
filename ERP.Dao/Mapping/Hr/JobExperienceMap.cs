using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class JobExperienceMap : BaseClassMap<JobExperience,long> {
        
        public JobExperienceMap() {
			Table("HR_JobExperience");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.CompanyName).Column("CompanyName");
			Map(x => x.CompanyType).Column("CompanyType");
			Map(x => x.CompanyLocation).Column("CompanyLocation");
			Map(x => x.Position).Column("Position");
			Map(x => x.ExperienceArea).Column("ExperienceArea");
			Map(x => x.Duration).Column("Duration");
			Map(x => x.DepartmentName).Column("DepartmentName");
            Map(x => x.FromDate).Column("FromDate");
            Map(x => x.ToDate).Column("ToDate");
			HasMany(x => x.Certificate).KeyColumn("HRJobExperienceId");
        }
    }
}
