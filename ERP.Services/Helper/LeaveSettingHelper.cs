﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace UdvashERP.Services.Helper
{
    public class LeaveSettingHelper
    {
        public static IEnumerable<SelectListItem> GetPublicOrPrivateLeaveType()
        {
            IList<SelectListItem> items = new List<SelectListItem>()
            {
                new SelectListItem(){Text = "Yes",Value = "1"},
                new SelectListItem(){Text = "No",Value = "0"}
            };
            return items;
        }
        public static IEnumerable<SelectListItem> GetPayType()
        {
            IList<SelectListItem> items = new List<SelectListItem>()
            {
                new SelectListItem(){Text = "With pay",Value = "1"},
                new SelectListItem(){Text = "Without pay",Value = "0"}
            };
            return items;
        }
        public static IEnumerable<SelectListItem> RepeatType()
        {
            IList<SelectListItem> items = new List<SelectListItem>()
            {
                new SelectListItem(){Text = "Every year",Value = "1"},
                new SelectListItem(){Text = "Once",Value = "2"},
                new SelectListItem(){Text = "Max-two",Value = "3"}
            };
            return items;
        }
        public static IEnumerable<SelectListItem> StartingFrom()
        {
            IList<SelectListItem> items = new List<SelectListItem>()
            {
                new SelectListItem(){Text = "Joining date",Value = "1"},
                new SelectListItem(){Text = "Permanent date",Value = "2"},
                new SelectListItem(){Text = "After one year of permanent",Value = "3"}
            };
            return items;
        }

        //public static IEnumerable<SelectListItem> GetGender()
        //{
        //    IList<SelectListItem> items = new List<SelectListItem>()
        //    {
        //        new SelectListItem() {Text = "Male", Value = "1"},
        //        new SelectListItem() {Text = "Female", Value = "2"},
        //        new SelectListItem() {Text = "Both", Value = "3"}
        //    };
        //    return items;
        //}
        public static IEnumerable<SelectListItem> GetEmployeementType()
        {
            IList<SelectListItem> items = new List<SelectListItem>()
            {
                new SelectListItem(){Text = "Probation",Value = "1"},
                new SelectListItem(){Text = "Permanent",Value = "2"},
                new SelectListItem(){Text = "Part-time",Value = "3"},
                new SelectListItem(){Text = "Contractual",Value = "4"},
                new SelectListItem(){Text = "Intern",Value = "5"},
                new SelectListItem(){Text = "Retired",Value = "6"}

            };
            return items;
        }
        public static IEnumerable<SelectListItem> GetMemberMaritalStatus()
        {
            IList<SelectListItem> items = new List<SelectListItem>()
            {
                new SelectListItem(){Text = "Single",Value = "1"},
                new SelectListItem(){Text = "Married",Value = "2"},
                new SelectListItem(){Text = "Widow",Value = "3"},
                new SelectListItem(){Text = "Widower",Value = "3"},
                new SelectListItem(){Text = "Divorced",Value = "3"}
                };
            return items;
        }

    }
}
