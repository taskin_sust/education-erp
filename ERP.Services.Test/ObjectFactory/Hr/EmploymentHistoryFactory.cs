﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class EmploymentHistoryFactory : ObjectFactoryBase<EmploymentHistory>
    {
        private CampusFactory _campusFactory;
        private CampusRoomFactory _campusRoomFactory;
        private DesignationFactory _designationFactory;
        private DepartmentFactory _departmentFactory;
        private TeamMemberFactory _teamMemberFactory;
        private OrganizationFactory _organizationFactory;
        private BranchFactory _branchFactory;

        private readonly ICampusService _campusService;
        private readonly IDesignationService _designationService;
        private readonly IDepartmentService _departmentService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IEmploymentHistoryService _employmentHistoryService;
        private readonly TestBaseService<EmploymentHistoryLog> _employmentHistoryLogService;
        public EmploymentHistoryFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _campusFactory = new CampusFactory(this, session);
            _campusRoomFactory = new CampusRoomFactory(this, session);
            _designationFactory = new DesignationFactory(this, session);
            _departmentFactory = new DepartmentFactory(this, session);
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _organizationFactory = new OrganizationFactory(this, session);
            _branchFactory = new BranchFactory(this, session);

            _campusService = new CampusService(session);
            _designationService = new DesignationService(session);
            _departmentService = new DepartmentService(session);
            _teamMemberService = new TeamMemberService(session);
            _employmentHistoryService = new EmploymentHistoryService(session);
            _employmentHistoryLogService = new TestBaseService<EmploymentHistoryLog>(session);
        }
        #region Create

        public EmploymentHistoryFactory Create()
        {
            Object = new EmploymentHistory()
            {
                CreateBy = 1,
                EmploymentStatus = (int)MemberEmploymentStatus.Probation,
                LeaveDate = DateTime.Now,
                ModifyBy = 1,
                Name = "EmploymentHistory_",
                EffectiveDate = DateTime.Now,
                Rank = 1,
                Status = 1,
                BusinessId = GetPcUserName(),
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public EmploymentHistoryFactory CreateMore(int numberOfEmployment)
        {
            _organizationFactory.Create();
            CreateEmploymentHistryList(numberOfEmployment, new List<Organization>() { _organizationFactory.Object });
            return this;
        }

        public EmploymentHistoryFactory CreateMore(int numberOfEmployment, Organization organization)
        {
            CreateEmploymentHistryList(numberOfEmployment, new List<Organization>() { organization });
            return this;
        }

        public EmploymentHistoryFactory CreateMore(int num, List<Organization> organizations)
        {
            CreateEmploymentHistryList(num, organizations);
            return this;
        }

        public EmploymentHistoryFactory CreateMore(int numberOfEmployment, MemberEmploymentStatus memberEmploymentStatus, DateTime? leaveDate, DateTime effectiveDate, Campus campus, Department department, Designation designation)
        {
            CreateEmploymentHistryList(numberOfEmployment, memberEmploymentStatus, leaveDate, effectiveDate, campus, department, designation);
            return this;
        }

        private IList<EmploymentHistory> CreateEmploymentHistryList(int numberOfEmployment, MemberEmploymentStatus memberEmploymentStatus, DateTime? leaveDate, DateTime effectiveDate, Campus campus, Department department, Designation designation)
        {
            for (int i = 0; i < numberOfEmployment; i++)
            {
                var obj = new EmploymentHistory()
                {
                    CreateBy = 1,
                    EmploymentStatus = (int)memberEmploymentStatus,
                    LeaveDate = leaveDate,
                    ModifyBy = 1,
                    Name = "EmploymentHistory_" + i,
                    Rank = 1,
                    Status = 1,
                    EffectiveDate = effectiveDate,
                    Department = _departmentService.LoadById(department.Id),
                    Campus = _campusService.GetCampus(campus.Id),
                    Designation = _designationService.LoadById(designation.Id),
                    BusinessId = GetPcUserName(),
                };
                ObjectList.Add(obj);
            }
            return ObjectList;
        }

        private IList<EmploymentHistory> CreateEmploymentHistryList(int numberOfEmployment, IEnumerable<Organization> organizations)
        {
            int j = -15;
            foreach (var organization in organizations)
            {
                _branchFactory.Create().WithOrganization(organization).Persist();

                for (int i = 0; i < numberOfEmployment; i++)
                {
                    _designationFactory.Create().WithRank(i + 1).WithOrganization(organization).Persist();
                    _campusRoomFactory.CreateMore(1, null);
                    _campusFactory.Create().WithCampusRoomList(_campusRoomFactory.ObjectList).WithBranch(_branchFactory.Object).Persist();
                    _departmentFactory.Create().WithOrganization(organization).Persist();

                    var desig = _designationService.LoadById(_designationFactory.Object.Id);

                    var _object = new EmploymentHistory()
                    {
                        EmploymentStatus = (int)MemberEmploymentStatus.Permanent,
                        LeaveDate = DateTime.Now,
                        CreateBy = 1,
                        ModifyBy = 1,
                        Name = "EmploymentHistory_" + Guid.NewGuid() + "_" + i,
                        Rank = 1,
                        Status = 1,
                        EffectiveDate = DateTime.Now.AddDays(j),
                        Department = _departmentFactory.Object,
                        Campus = _campusFactory.Object,
                        Designation = desig,
                        BusinessId = GetPcUserName(),
                    };
                    ObjectList.Add(_object);
                }
                j = j + 5;
            }
            return ObjectList;
        }

        public EmploymentHistoryFactory WithNameAndRank()
        {
            Object.Name = "EmploymentHistory" + Guid.NewGuid();
            Object.Rank = new Random().Next(10000);
            return this;
        }

        public EmploymentHistoryFactory WithCampus()
        {
            _campusFactory.Create().WithBranch().Persist();
            Object.Campus = _campusFactory.Object;
            return this;
        }
        public EmploymentHistoryFactory WithCampus(Campus campus)
        {
            Object.Campus = campus;
            return this;
        }

        public EmploymentHistoryFactory WithDesignation()
        {
            _designationFactory.Create().Persist();
            Object.Designation = _designationFactory.Object;
            return this;
        }
        public EmploymentHistoryFactory WithDesignation(Designation designation)
        {
            Object.Designation = designation;
            return this;
        }

        public EmploymentHistoryFactory WithDepartment()
        {
            _departmentFactory.Create().Persist();
            Object.Department = _departmentFactory.Object;
            return this;
        }
        public EmploymentHistoryFactory WithDepartment(Department department)
        {
            Object.Department = department;
            return this;
        }
        public EmploymentHistoryFactory WithTeamMember()
        {
            _teamMemberFactory.Create().Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }

        public EmploymentHistoryFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public EmploymentHistoryFactory Persist()
        {
            SingleObjectList.ForEach(x => { _employmentHistoryService.Save(x); });
            ObjectList.ForEach(x => { _employmentHistoryService.Save(x); });
            return this;
        }
        #endregion

        #region Other
        #endregion

        #region CleanUp
        public void CleanUp()
        {
            DeleteAll();
            _campusRoomFactory.CleanUp();
            _campusFactory.Cleanup();
            _branchFactory.Cleanup();
            _departmentFactory.Cleanup();
            _designationFactory.Cleanup();
            // _teamMemberFactory.Cleanup();
        }

        public void CleanUpParentFactory()
        {
            _campusRoomFactory.CleanUp();
            _campusFactory.Cleanup();
            _branchFactory.Cleanup();
            _departmentFactory.OnlyDepartmentCleanUp();
            //_branchFactory.Cleanup();
            _designationFactory.OnlyDesignationCleanUp();
            //_departmentFactory.OnlyDepartmentCleanUp();
            //_designationFactory.OnlyDesignationCleanUp();
        }

        //public void LogCleanUp(TeamMember teamMember)
        //{
        //    _employmentHistoryLogService.DeleteEmploymentHistoryLogByEmploymentHistoryIdList(teamMember);
        //}




        public void LogCleanUp(EmploymentHistoryFactory employmentHistoryFactory)
        {
            if (employmentHistoryFactory.ObjectList.Any())
            {
                DeleteLog(employmentHistoryFactory.ObjectList);
            }
            if (employmentHistoryFactory.SingleObjectList.Any())
            {
                DeleteLog(employmentHistoryFactory.SingleObjectList);
            }
        }

        private void DeleteLog(IEnumerable<EmploymentHistory> list)
        {
            foreach (var employmentHisotry in list)
            {
                var tableName = GetEntityTableName(typeof(EmploymentHistoryLog));
                string swData = "DELETE FROM " + tableName + " WHERE [EmploymentHistoryId] = " + employmentHisotry.Id + ";";
                _session.CreateSQLQuery(swData).ExecuteUpdate();
            }
        }




        //public void LogCleanUp()
        //{
        //    var emHistoryIdList =
        //        _session.QueryOver<EmploymentHistory>()
        //            .Select(x => x.Id)
        //            .Where(x => x.BusinessId == GetPcUserName().ToString(CultureInfo.InvariantCulture))
        //            .List<long>();
            
        //    var ids = new List<long>();
        //    foreach (long id in emHistoryIdList)
        //    {
        //        long id1 = id;
        //        EmploymentHistoryLog entity = _session.QueryOver<EmploymentHistoryLog>().Where(x => x.EmploymentHistoryId == id1).SingleOrDefault();
        //        ids.Add(entity.Id);
        //    }
        //    if (ids.Count > 0)
        //        _employmentHistoryLogService.DeleteByIdList(ids);
        //}

        #endregion

    }

}
