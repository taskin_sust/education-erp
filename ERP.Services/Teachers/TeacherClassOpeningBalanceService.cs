﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherClassOpeningBalanceService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdateTeacherClassOpeningBalance(List<TeacherClassOpeningBalance> teacherClassOpeningBalanceList);

        #endregion

        #region Single Instances Loading Function

        TeacherClassOpeningBalance GetTeacherClassOpeningBalance(long id);

        #endregion

        #region List Loading Function

        IList<TeacherClassOpeningBalanceDto> LoadTeacherClassOpeningBalance(List<UserMenu> userMenu, List<long> organizationIdList, string pinList);

        #endregion

        #region Others Function
        #endregion
        
    }

    public class TeacherClassOpeningBalanceService : BaseService, ITeacherClassOpeningBalanceService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("TeacherService");

        #endregion

        #region Propertise & Object Initialization

        private readonly ITeacherDao _teacherDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly ITeacherClassOpeningBalanceDao _teacherClassOpeningBalanceDao;

        public TeacherClassOpeningBalanceService(ISession session)
        {
            Session = session;
            _teacherDao = new TeacherDao { Session = session };
            _organizationDao = new OrganizationDao() { Session = session };
            _teacherClassOpeningBalanceDao = new TeacherClassOpeningBalanceDao { Session = session };
        }

        #endregion

        #region Operational Function

        public void SaveOrUpdateTeacherClassOpeningBalance(List<TeacherClassOpeningBalance> teacherClassOpeningBalanceList)
        {
            ITransaction trans = null;
            try
            {
                List<TeacherClassOpeningBalance> validOpeningBalanceList = new List<TeacherClassOpeningBalance>();
                foreach (TeacherClassOpeningBalance openingBalance in teacherClassOpeningBalanceList)
                {
                    CheckBeforeSave(openingBalance);
                    validOpeningBalanceList.Add(openingBalance);
                }
                using (trans = Session.BeginTransaction())
                {
                    if (validOpeningBalanceList.Any())
                    {
                        foreach (TeacherClassOpeningBalance teacherClassOpeningBalance in validOpeningBalanceList)
                        {
                            _teacherClassOpeningBalanceDao.SaveOrUpdate(teacherClassOpeningBalance);
                        }
                        trans.Commit();
                    }
                }

            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public TeacherClassOpeningBalance GetTeacherClassOpeningBalance(long id)
        {
            try
            {
                return _teacherClassOpeningBalanceDao.GetTeacherClassOpeningBalance(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<TeacherClassOpeningBalanceDto> LoadTeacherClassOpeningBalance(List<UserMenu> userMenu, List<long> organizationIdList, string pinList)
        {
            try
            {
                List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (!organizationIdList.Contains(SelectionType.SelelectAll)) ? organizationIdList : null);
                if (!authOrganizationIdList.Any())
                {
                    throw new InvalidDataException("You have no authorization Organization. Please contact to Administration person.");
                }

                IList<TeacherClassOpeningBalanceDto> teacherClassOpeningBalanceDtoList = _teacherClassOpeningBalanceDao.LoadTeacherClassOpeningBalance(authOrganizationIdList, pinList);

                return teacherClassOpeningBalanceDtoList;

            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        private void CheckBeforeSave(TeacherClassOpeningBalance openingBalance)
        {
            int classNumber;
            DateTime openingDate;
            if (openingBalance == null)
                throw new InvalidDataException("No Information found!");
            if (openingBalance.Teacher == null)
                throw new InvalidDataException("No Teacher found!");
            Teacher teacher = openingBalance.Teacher;
            if (openingBalance.Organization == null)
                throw new InvalidDataException("No Organization found" + " for TPIN: " + teacher.Tpin + "!");
            if (openingBalance.TotalClass < 0 || int.TryParse(openingBalance.TotalClass.ToString(), out classNumber) == false || classNumber < 0)
                throw new InvalidDataException("Invalid Total Class Number" + " for TPIN: " + teacher.Tpin + "!");
            if (DateTime.TryParse(openingBalance.OpeningDate.ToString(), out openingDate) == false)
                throw new InvalidDataException("Invalid Opening Date" + " for TPIN: " + teacher.Tpin + "!");

           
            if (!teacher.Organizations.Select(x => x.Id).ToList().Contains(openingBalance.Organization.Id))
            {
                throw new InvalidDataException("This Teacher is not as a teacher to This organization" + " for TPIN: " + teacher.Tpin + "!");
            }

            TeacherClassOpeningBalance teacherOpeningBalance = _teacherClassOpeningBalanceDao.GetTeacherClassOpeningBalanceByTeacherIdOrganizationId(openingBalance.Teacher.Id, openingBalance.Organization.Id);
            if (teacherOpeningBalance != null)
            {
                if (openingBalance.Id == 0)
                {
                    if (teacherOpeningBalance.Organization.Id == openingBalance.Organization.Id)
                    {
                        throw new InvalidDataException("This Teacher has already Opening Balance for this Organization" + " for TPIN: " + teacher.Tpin + "!");
                    }
                }
                else if (openingBalance.Id != 0)
                {
                    if (teacherOpeningBalance.Id != openingBalance.Id)
                    {
                        throw new InvalidDataException("This Teacher has already Opening Balance for this Organization" + " for TPIN: " + teacher.Tpin + "!");
                    }
                    if (teacherOpeningBalance.Organization.Id != openingBalance.Organization.Id)
                    {
                        throw new InvalidDataException("Can't Change Organization for Teacher Opening Balance Update" + " for TPIN: " + teacher.Tpin + "!");
                    }
                }
            }
        }

        #endregion

       
    }
}
