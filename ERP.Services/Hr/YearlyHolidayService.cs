﻿using System;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{
    public interface IYearlyHolidayService : IBaseService
    {

        #region Operational Function

        void Save(YearlyHoliday yearlyHoliday);
        void Delete(long id);

        #endregion

        #region Single Instances Loading Function

        YearlyHoliday GetYearlyHoliday(string name, long organizationId,bool loadActive=true);

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
        
    }
    public class YearlyHolidayService : BaseService, IYearlyHolidayService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IYearlyHolidayDao _yearlyHolidayDao;
        public YearlyHolidayService(ISession session)
        {
            Session = session;
            _yearlyHolidayDao = new YearlyHolidayDao()
            {
                Session = session
            };
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Functions

        public void Save(YearlyHoliday yearlyHoliday)
        {
            try
            {
                _yearlyHolidayDao.Save(yearlyHoliday);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public void Delete(long id)
        {
            try
            {
                _yearlyHolidayDao.Delete(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public YearlyHoliday GetYearlyHoliday(string name, long organizationId, bool loadActive = true)
        {
            return _yearlyHolidayDao.GetYearlyHoliday(name, organizationId, loadActive);
        }

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function
        
        #endregion

        #region Helper function
        
        #endregion

        
    }
}
