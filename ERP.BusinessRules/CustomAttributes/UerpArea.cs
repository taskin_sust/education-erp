﻿using System;

namespace UdvashERP.BusinessRules.CustomAttributes
{   
    public class UerpAreaAttribute : Attribute
    {
        public UerpAreaAttribute(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
    }
}
