﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Axzkemkeeper;
using DeviceSynchronizer.Model;
using DeviceSynchronizer.Model.ApiModel;
using DeviceSynchronizer.Model.BusinessModel;

namespace DeviceSynchronizer.DeviceHelper
{
    public interface IAttendanceDeviceDriver
    {
        bool SetTime(GetStandardTime getStandardTime);
        void Clean();
        bool SaveToDevice(TeamMemberModel memberViewModel,  bool useInternalConnection = false, int machineNum = 0);
        List<AttendanceLog> LoadLogList();
        bool IsAttendanceLogExits();
        bool ClearLog();
        bool OpenConnection(bool isDeviceLock = true);
        void CloseConnection();
        void RefreshData();
    }
}
