﻿using System;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using System.Collections.Generic;
using UdvashERP.Services.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.HolidaySettingTest
{
    public class HolidaySettingBaseTest : TestBase,IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal HolidaySettingService _holidaySettingService;
        internal OrganizationService _organizationService;
        internal List<long> IdList = new List<long>();
        private ISession _session;
        internal HolidaySettingFactory _holidaySettingFactory;
        internal YearlyHolidayFactory _yearlyHolidayFactory;
        internal IYearlyHolidayService _YearlyHolidayService;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal const long OrgId = 1;

        #endregion

        public HolidaySettingBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();   
         
            _holidaySettingFactory = new HolidaySettingFactory(null,_session);
            _holidaySettingService = new HolidaySettingService(_session);
            _yearlyHolidayFactory=new YearlyHolidayFactory(null,_session);
            _YearlyHolidayService = new YearlyHolidayService(_session);
            _organizationService = new OrganizationService(_session);
        }
               
        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public void Dispose()
        {
            _holidaySettingFactory.Cleanup();
            _yearlyHolidayFactory.Cleanup();
            base.Dispose();
           
        }                
    }   
}
