using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.Administration
{
    public interface IBankDao : IBaseDao<Bank, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<Bank> LoadBankList(int start, int length, string orderBy = "", string orderDir = "", string name = "", string shortname = "", string rank = "", string status = "");
        IList<Bank> LoadBankList();
        #endregion

        #region Others Function
        bool CheckDuplicateFields(long id, string name = "", string shortname = "");
        int GetBankRowCount(string name, string shortname, string rank, string status);

        #endregion

        #region Helper Function

        #endregion

    }

    public class BankDao : BaseDao<Bank, long>, IBankDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public IList<Bank> LoadBankList(int start, int length, string orderBy = "", string orderDir = "",
            string name = "", string shortname = "", string rank = "", string status = "")
        {
            var bankListQueryOver = GetBankListQueryOver(orderBy, orderDir, name, shortname, rank, status);
            var bankList = length > 0 ? bankListQueryOver.Skip(start).Take(length).List<Bank>() : bankListQueryOver.List<Bank>();
            return bankList;
        }


        public IList<Bank> LoadBankList()
        {
            return Session.QueryOver<Bank>().Where(x => x.Status == Bank.EntityStatus.Active).List<Bank>();

        }
        #endregion

        #region Others Function
        public int GetBankRowCount(string name, string shortname, string rank, string status)
        {
            var bankListQueryOver = GetBankListQueryOver("", "", name, shortname, rank, status);
            var rowCount = bankListQueryOver.RowCount();
            return rowCount;
        }

        #endregion

        #region Helper Function
        private IQueryOver<Bank, Bank> GetBankListQueryOver(string orderBy, string orderDir, string name, string shortname, string rank, string status)
        {
            var bankListQueryOver = Session.QueryOver<Bank>();
            if (!string.IsNullOrEmpty(name))
            {
                bankListQueryOver = bankListQueryOver.WhereRestrictionOn(x => x.Name).IsInsensitiveLike(name.Trim(), MatchMode.Anywhere);
            }
            if (!string.IsNullOrEmpty(shortname))
            {
                bankListQueryOver = bankListQueryOver.WhereRestrictionOn(x => x.ShortName).IsInsensitiveLike(shortname.Trim(), MatchMode.Anywhere);
            }
            if (!string.IsNullOrEmpty(rank))
            {
                bankListQueryOver = bankListQueryOver.Where(x => x.Rank == Convert.ToInt32(rank));
            }
            bankListQueryOver = !string.IsNullOrEmpty(status)
                ? bankListQueryOver.Where(x => x.Status == Convert.ToInt32(status))
                : bankListQueryOver.Where(x => x.Status != Bank.EntityStatus.Delete);
            if (!string.IsNullOrEmpty(orderBy.Trim()))
            {
                if (!string.IsNullOrEmpty(orderDir))
                {
                    orderDir = orderDir.Trim().ToUpper();
                    if (orderDir == "ASC")
                    {
                        bankListQueryOver = bankListQueryOver.OrderBy(Projections.Property(orderBy)).Asc;
                    }
                    else if (orderDir == "DESC")
                    {
                        bankListQueryOver = bankListQueryOver.OrderBy(Projections.Property(orderBy)).Desc;
                    }
                }
            }
            return bankListQueryOver;
        }
        public bool CheckDuplicateFields(long id, string name = "", string shortname = "")
        {
            var bankQueryOver =
                Session.QueryOver<Bank>().Where(x => x.Status == Bank.EntityStatus.Active);
            if (id > 0)
            {
                bankQueryOver = bankQueryOver.Where(x => x.Id != id);
            }
            if (!string.IsNullOrEmpty(name.Trim()))
            {
                bankQueryOver = bankQueryOver.Where(x => x.Name == name.ToString());
            }
            if (!string.IsNullOrEmpty(shortname.Trim()))
            {
                bankQueryOver = bankQueryOver.Where(x => x.ShortName == shortname.ToString());
            }
            var rowcount = bankQueryOver.RowCount();
            if (rowcount > 0)
            {
                return true;
            }
            return false;
        }

        #endregion

    }
}
