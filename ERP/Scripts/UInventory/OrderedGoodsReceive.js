﻿$(document).ready(function () {

    window.validateTextField("worderNo", "Please enter valid work order no.");

    $(document).on("click", "#searchByworderNo", function () {
        window.showErrorMessageBelowCtrl("worderNo", "", false);
        var wOrderNo = $("#worderNo").val();
        if (wOrderNo.length > 13) {
            window.showErrorMessageBelowCtrl("worderNo", "Please enter valid work order no.", true);
        };
        if (wOrderNo.length <= 0) {
            window.showErrorMessageBelowCtrl("worderNo", "Please enter valid work order no.", true);
            return;
        };
        var url = $("body").attr("data-project-root") + "UInventory/OrderedGoodsReceive/GoodsReceiveInformation";
        var reqType = "Post";
        var data = { workOrderNo: wOrderNo };

        $.ajax({
            type: reqType,
            url: url,
            data: data,
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (response) {
                if (response.IsSuccess == false) {
                    $.fn.customMessage({
                        displayMessage: response.Message,
                        displayMessageType: "e",
                        displayTime: 120000,
                    });
                } else {
                    $("#order-Info").html(response);
                    console.log(response);
                }
                $.unblockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (result) {
                $.fn.customMessage({
                    displayMessage: window.AJAX_ERROR,
                    displayMessageType: "error",
                    displayTime: 5000
                });
            }
        });

    });
});