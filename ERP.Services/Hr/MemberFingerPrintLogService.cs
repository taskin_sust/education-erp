﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Hr
{
    public interface IMemberFingerPrintLogService : IBaseService
    {

    }

    public class MemberFingerPrintLogService : BaseService, IMemberFingerPrintLogService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Properties & Object Initialization

        public MemberFingerPrintLogService(ISession session)
        {
            Session = session;

        }
        
        #endregion

    }
}
