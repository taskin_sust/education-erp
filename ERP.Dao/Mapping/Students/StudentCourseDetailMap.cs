using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students {


    public partial class StudentCourseDetailMap : BaseClassMap<StudentCourseDetail, long>
    {
        
        public StudentCourseDetailMap() {
			Table("StudentCourseDetails");
			LazyLoad();
            Map(x => x.CourseId).Column("CourseId");
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            //References(x => x.Student).Column("StudentId");
            //References(x => x.Course).Column("CourseId");
			References(x => x.CourseSubject).Column("CourseSubjectId");
			References(x => x.StudentProgram).Column("StudentProgramId");
           
        }
    }
}
