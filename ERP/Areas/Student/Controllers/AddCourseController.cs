﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using NHibernate.Criterion;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using UdvashERP.App_code;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services;
using UdvashERP.Services.Administration;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.Student;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using WebGrease.Css.Extensions;

namespace UdvashERP.Areas.Student.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Student")]
    public class AddCourseController : Controller
    {
        #region Objects/Propertise/Services/Dao & Initialization
        private ILog logger = LogManager.GetLogger("StudentArea");
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly ICourseService _courseService;
        private readonly IStudentService _studentService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IStudentPaymentService _studentPaymentService;
        private readonly IStudentCourseDetailsService _studentCourseDetailsService;
        private readonly IDiscountService _discountService;
        private readonly ICommonHelper _commonHelper;
        private readonly IReferenceService _referenceService;
        private readonly ICourseSubjectService _courseSubjectService;
        private readonly IComplementaryCourseService _complementaryCourseService;
        private IDiscountDetailService _discountDetailService;
        private List<UserMenu> _userMenu;
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        #region Operational Function
        [HttpGet]
        public ActionResult AddCourseOrSubject(string message = "")
        {
            if (!String.IsNullOrEmpty(message))
            {
                ViewBag.ErrorMessage = message;
            }
            return View();
        }

        [HttpGet]
        public ActionResult CourseOrSubjectAdd(string stdProRoll, string optionalErrorMessage = "")
        {
            ViewData["stdPId"] = stdProRoll;
            try
            {
                if (!String.IsNullOrEmpty(stdProRoll))
                {
                    List<long> takenCourseIds = new List<long>();
                    SelectList paymentM;
                    var paymentdetail = new PaymentDetails();
                    stdProRoll = stdProRoll.Trim();
                    var studentProgram = _studentProgramService.GetStudentProgram(stdProRoll, true);
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    //check authorization
                    var message = _studentProgramService.CheckAuthorizationForStudentProgram(_userMenu, studentProgram);
                    if (message != "")
                        return RedirectToAction("AddCourseOrSubject", new { message = message });
                    if (studentProgram != null)
                    {
                        if (studentProgram.DueAmount == null)
                        {
                            ViewBag.StudentRoll = stdProRoll;
                            ViewBag.ErrorMessage = "No payment found. Please pay first then add more course.";
                            return View("AddCourseOrSubject");
                        }
                        var studentViewModel = new StudentViewModel();
                        var student = _studentService.LoadById(studentProgram.Student.Id);
                        studentViewModel.Name = student.NickName;
                        studentViewModel.MobNumber = student.Mobile;
                        studentViewModel.Program = studentProgram.Program.Name;
                        studentViewModel.BatchDays = studentProgram.Batch.Days;
                        studentViewModel.BatchTime = studentProgram.Batch.FormatedBatchTime;
                        studentViewModel.Batch = studentProgram.Batch.Name;
                        studentViewModel.Id = studentProgram.Id;
                        studentViewModel.VersionOfStudy = studentProgram.VersionOfStudy;
                        studentViewModel.Gender = student.Gender;
                        studentViewModel.Religion = student.Religion;

                        var branch = _branchService.GetBranch(studentProgram.Batch.Branch.Id);
                        studentViewModel.Branch = branch.Name;
                        studentViewModel.Session = studentProgram.Batch.Session.Name;
                        studentViewModel.Campus = studentProgram.Batch.Campus.Name;
                        /*Student Course Assigned View */
                        var studentCourseDetailsList =
                            _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id);
                        var query = studentCourseDetailsList.GroupBy(x => x.CourseSubject.Course,
                            (key, g) => new { Course = key, Subjects = g.OrderBy(x => x.CourseSubject.Subject.Rank).ToList() });
                        var courseList = query.ToList();
                        IList<Course> courses =
                            _courseService.LoadCourse(studentProgram.Program.Id,
                                studentProgram.Batch.Session.Id);

                        var countTotalAssignedCourse = courseList.Count;
                        // student does not pay anything
                        if (countTotalAssignedCourse == 0)
                        {
                            foreach (var courseObj in courses)
                            {
                                var courseViewModelObj = new CourseViewModel()
                                {
                                    Id = courseObj.Id,
                                    Name = courseObj.Name,
                                    IsTaken = false,
                                    ComplementaryCourses = courseObj.ComplementaryCourses
                                };

                                foreach (
                                    var totalCourseSubject in courseObj.CourseSubjects.OrderBy(x => x.Subject.Rank))
                                {
                                    var subjectViewModel = new SubjectViewModel();
                                    var subject = totalCourseSubject.Subject;
                                    subjectViewModel.Name = totalCourseSubject.Subject.Name;
                                    subjectViewModel.Payment = totalCourseSubject.Payment;
                                    subjectViewModel.Id = totalCourseSubject.Subject.Id;
                                    subjectViewModel.IsTaken = false;
                                    courseViewModelObj.SubjectViewModels.Add(subjectViewModel);
                                }
                                studentViewModel.CourseViewModels.Add(courseViewModelObj);

                            }
                            return View(studentViewModel);
                        }

                        #region Total Course List
                        /*Total Courses*/
                        foreach (var courseObj in courses)
                        {
                            /* Took Courses by Student */
                            var jj = 0;
                            var isCourseAssign = false;
                            var flag1 = 0;

                            //else
                            //{
                            foreach (var courseAndCorrespondingSubjectList in courseList)//Student course list
                            {
                                /*if students have already taken this course */
                                jj++;
                                if (courseObj.Id == courseAndCorrespondingSubjectList.Course.Id)
                                {
                                    takenCourseIds.Add(courseObj.Id);
                                    var courseViewModel = new CourseViewModel
                                    {
                                        Id = courseAndCorrespondingSubjectList.Course.Id,
                                        Name = courseAndCorrespondingSubjectList.Course.Name,
                                        IsTaken = true,
                                        ComplementaryCourses = courseObj.ComplementaryCourses
                                    };
                                    /*total Subject List*/
                                    foreach (
                                        var totalCourseSubject in
                                            courseObj.CourseSubjects.OrderBy(x => x.Subject.Rank))
                                    {
                                        /* Took subjects by Student */
                                        var totalSub = courseObj.CourseSubjects.Count;
                                        var ii = 0;
                                        var isSubjctAssign = false;
                                        var flag = 0;
                                        var countTotalAssignedSub = courseAndCorrespondingSubjectList.Subjects.Count;
                                        foreach (var takenSubject in courseAndCorrespondingSubjectList.Subjects)
                                        {
                                            /*if student have already taken this subject */
                                            ii++;
                                            var isAssign = false;
                                            if (totalCourseSubject.Subject == takenSubject.CourseSubject.Subject)
                                            {
                                                isAssign = true;
                                                isSubjctAssign = true;
                                                var subjectViewModel = new SubjectViewModel();
                                                var subject = totalCourseSubject.Subject;
                                                subjectViewModel.Name = totalCourseSubject.Subject.Name;
                                                subjectViewModel.Payment = totalCourseSubject.Payment;
                                                subjectViewModel.Id = totalCourseSubject.Subject.Id;
                                                subjectViewModel.IsTaken = true;
                                                courseViewModel.SubjectViewModels.Add(subjectViewModel);
                                                break;
                                            }
                                            if (ii == countTotalAssignedSub && isSubjctAssign == false)
                                            {
                                                flag = 1;
                                            }
                                            if (flag == 1)
                                            {
                                                var subjectViewModel = new SubjectViewModel();
                                                var subject = totalCourseSubject.Subject;
                                                subjectViewModel.Name = totalCourseSubject.Subject.Name;
                                                subjectViewModel.Payment = totalCourseSubject.Payment;
                                                subjectViewModel.Id = subject.Id;
                                                subjectViewModel.IsTaken = false;
                                                courseViewModel.SubjectViewModels.Add(subjectViewModel);
                                                flag = 0;
                                                break;
                                            }
                                        }
                                    }
                                    studentViewModel.CourseViewModels.Add(courseViewModel);
                                    isCourseAssign = true;
                                    break;
                                }

                                if (jj == countTotalAssignedCourse && isCourseAssign == false)
                                {
                                    flag1 = 1;
                                }

                                if (flag1 == 1)
                                {
                                    var courseViewModel = new CourseViewModel
                                    {
                                        Id = courseObj.Id,
                                        Name = courseObj.Name,
                                        IsTaken = false,
                                        ComplementaryCourses = courseObj.ComplementaryCourses

                                    };

                                    foreach (var courseSubObj in courseObj.CourseSubjects.OrderBy(x => x.Subject.Rank))
                                    {
                                        var subjectViewModel = new SubjectViewModel();
                                        var subject = courseSubObj.Subject;
                                        subjectViewModel.Name = courseSubObj.Subject.Name;
                                        subjectViewModel.Payment = courseSubObj.Payment;
                                        subjectViewModel.Id = subject.Id;
                                        subjectViewModel.IsTaken = false;
                                        courseViewModel.SubjectViewModels.Add(subjectViewModel);
                                    }

                                    //bool isComplementary = _complementaryCourseService.IsComplemented(courseViewModel.Id);
                                    //if (isComplementary)
                                    //{
                                    //    courseViewModel.IsComplementaryCourse = true;
                                    //}
                                    //else
                                    //{
                                    //    courseViewModel.IsComplementaryCourse = false;
                                    //}
                                    /* Check these nontaken course belongs to any other taken course if yes then set IsComplementary true*/

                                    studentViewModel.CourseViewModels.Add(courseViewModel);
                                    break;
                                }
                            }
                            //}
                        }


                        List<CourseViewModel> nonTakenlist =
                            studentViewModel.CourseViewModels.Where(x => !x.IsTaken).ToList();
                        List<CourseViewModel> newNonTakenlist = new List<CourseViewModel>();
                        List<CourseViewModel> newNonTakenlistTemp = new List<CourseViewModel>();

                        foreach (var courseViewModel in nonTakenlist)
                        {
                            var newNonTakenObj = new CourseViewModel();
                            newNonTakenObj = courseViewModel;
                            newNonTakenlist.Add(newNonTakenObj);
                        }
                        foreach (var nonTakenCourseObj in nonTakenlist)
                        {
                            foreach (var fcourseViewModel in studentViewModel.CourseViewModels)
                            {
                                if (fcourseViewModel.IsTaken)
                                {
                                    var fComIds = fcourseViewModel.ComplementaryCourses.Select(x => x.CompCourse.Id);
                                    if (fComIds.Contains(nonTakenCourseObj.Id))//
                                    {
                                        //nonTakenCourseObj = fcourseViewModel;
                                        nonTakenCourseObj.IsComplementaryCourse = true;
                                        newNonTakenlistTemp.Add(nonTakenCourseObj);
                                    }
                                }
                            }
                        }
                        var fstudentViewModel = new StudentViewModel();
                        foreach (var courseViewModel in studentViewModel.CourseViewModels)
                        {
                            var nCourseViewModel = new CourseViewModel();
                            if (courseViewModel.IsTaken)
                            {
                                nCourseViewModel = courseViewModel;
                            }
                            else
                            {
                                foreach (var nNonTaken in newNonTakenlistTemp)
                                {
                                    if (courseViewModel.Id == nNonTaken.Id)
                                    {
                                        nCourseViewModel = nNonTaken;
                                        break;
                                    }
                                }
                            }
                            fstudentViewModel.CourseViewModels.Add(nCourseViewModel);
                        }

                        //var nontakenCourseIds = studentViewModel.CourseViewModels.Select(x => x.Id).Except(takenCourseIds);//

                        #endregion

                        if (!String.IsNullOrEmpty(optionalErrorMessage)) { ViewBag.ErrorMessage = optionalErrorMessage; }
                        ViewData["stdPId"] = stdProRoll;
                        return View(studentViewModel);
                    }
                    ViewBag.StudentRoll = stdProRoll;
                    ViewBag.ErrorMessage = "No Program admission information found for this roll";
                    return View("AddCourseOrSubject");
                }
                ViewBag.ErrorMessage = "Invalid Program Roll";
                return View("AddCourseOrSubject");
            }
            catch (Exception)
            {
                return RedirectToAction("AddCourseOrSubject", new { message = "Invalid Program Roll" });
            }
        }
        private Discount GetDiscount(IList<Discount> existingDiscountList, StudentViewModel studentViewModel)
        {
            IList<Discount> discountApplicableListForStd = (from discountObj in existingDiscountList
                                                            let discountDetailsList =
                       _discountDetailService.GetAllDiscountDetailByDiscountId(discountObj.Id)
                                                            where discountDetailsList.Count <= studentViewModel.CourseViewModels.Count
                                                            let countdiscountDetailsList = discountDetailsList.Count
                                                            let jj = (from discountDetail in discountDetailsList
                                                                      let dCourse = discountDetail.Course
                                                                      let dMinSub = discountDetail.MinSubject
                                                                      let ii = 0
                                                                      where (from stdAssignedCourse in studentViewModel.CourseViewModels
                                                                             let stdCourse = _courseService.GetCourse(stdAssignedCourse.Id)
                                                                             let ss = stdAssignedCourse.SubjectViewModels.Count(x => x.IsNewlyTakenSubject)
                                                                             where stdCourse == dCourse && dMinSub <= stdAssignedCourse.SubjectViewModels.Count(x => x.IsNewlyTakenSubject == true)
                                                                             select stdAssignedCourse).Any()
                                                                      select dCourse).Count()
                                                            where countdiscountDetailsList == jj
                                                            select discountObj).ToList();
            var appliedDiscountObj = new Discount();
            if (discountApplicableListForStd.Count > 1)
            {
                appliedDiscountObj = _discountService.GetMostApplicableDiscountObj(discountApplicableListForStd);
            }
            if (discountApplicableListForStd.Count == 1)
            {
                appliedDiscountObj = discountApplicableListForStd[0];
            }
            return appliedDiscountObj;
        }



        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function


        #endregion

        #region Others Function

        #endregion


        public AddCourseController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _branchService = new BranchService(session);
                _campusService = new CampusService(session);
                _courseService = new CourseService(session);
                _studentService = new StudentService(session);
                _studentProgramService = new StudentProgramService(session);
                _studentPaymentService = new StudentPaymentService(session);
                _studentCourseDetailsService = new StudentCourseDetailsService(session);
                _discountService = new DiscountService(session);
                _commonHelper = new CommonHelper();
                _referenceService = new ReferenceService(session);
                _courseSubjectService = new Services.Students.CourseSubjectService(session);
                _discountDetailService = new DiscountDetailService(session);
                _complementaryCourseService = new ComplementaryCourseService(session);
            }
            catch (Exception e)
            {

                if (e.InnerException != null)
                {
                    logger.Error("CourseAdd Constructor can't be initialized Due to " + e.InnerException);
                }
                else
                {
                    logger.Error("CourseAdd Constructor can't be initialized Due to " + e.Message);
                }
                Console.WriteLine("{0}", e.Message);
            }
        }

        private static SubjectViewModel ToSubjectViewModel(CourseSubject takenCs, bool isTaken)
        {
            var courseSubjectVm = new SubjectViewModel()
            {
                Id = takenCs.Subject.Id,
                IsTaken = isTaken,
                Name = takenCs.Subject.Name,
                Payment = takenCs.Payment
            };
            return courseSubjectVm;
        }

        private static CourseViewModel ToCourseViewModel(Course course, bool isTaken)
        {
            var courseViewModel = new CourseViewModel()
            {
                Id = course.Id,
                IsTaken = isTaken,
                Name = course.Name,
                ProgramId = course.Program.Id + "",
                SessionId = course.RefSession.Id + ""
            };
            return courseViewModel;
        }
        private List<ResponseMessage> GetTotalCourseFee(StudentViewModel studentViewModel, out List<ResponseMessage> responseMessages)
        {

            responseMessages = new List<ResponseMessage>();

            foreach (var course in studentViewModel.CourseViewModels)
            {
                int courseSubjectTaken = course.SubjectViewModels.Count();

                var dbCourse = _courseService.GetCourse(course.Id);
                if (dbCourse != null)
                {
                    if (courseSubjectTaken > dbCourse.MaxSubject)
                    {
                        responseMessages.Add(new ResponseMessage()
                        {
                            ErrorMessage = string.Format("You cannot take more than {0} subjects in {1} course", dbCourse.MaxSubject, dbCourse.Name),
                        });
                    }
                    else
                    {
                        responseMessages.Add(new ResponseMessage()
                        {
                            ErrorMessage = ""
                        });
                    }
                }
            }

            return responseMessages;
        }

        [HttpPost]
        public ActionResult NewCourseAssigned(string studentObj, string stdProRoll)
        {
            try
            {
                decimal receivableAmountWithoutDiscount = 0;
                if (!String.IsNullOrEmpty(stdProRoll))
                {
                    List<ResponseMessage> responseMessages;
                    stdProRoll = stdProRoll.Trim();
                    var studentCourseDetailList = new List<StudentCourseDetail>();
                    IList<StudentCourseDetail> removalList = new List<StudentCourseDetail>();
                    var jjScriptSerializer = new JavaScriptSerializer();
                    var studentViewModel = jjScriptSerializer.Deserialize<StudentViewModel>(studentObj);

                    responseMessages = GetTotalCourseFee(studentViewModel, out responseMessages);
                    foreach (var responseMessageObj in responseMessages)
                    {
                        if (responseMessageObj.ErrorMessage != "") { return Json(new Response(false, responseMessageObj.ErrorMessage)); }
                    }
                    var studentProgram = _studentProgramService.GetStudentProgram(stdProRoll, true);
                    var comCourseFee = 0;
                    foreach (var course in studentViewModel.CourseViewModels)
                    {
                        foreach (var courseSubject in course.SubjectViewModels)
                        {
                            if (courseSubject.IsNewlyTakenSubject)
                            {
                                var studentCourseDetailObj = new StudentCourseDetail();
                                var courseSubjectObj = _courseSubjectService.LoadCourseSubject(course.Id, courseSubject.Id);
                                //var courseSubjectObj = _courseService.GetCourseSubjectByCourseSubjectId(Convert.ToInt64(courseSubject.Id));

                                studentCourseDetailObj.CourseSubject = courseSubjectObj;
                                studentCourseDetailObj.StudentProgram = studentProgram;
                                studentCourseDetailObj.CourseId = courseSubjectObj.Course.Id;//Add Course Id
                                receivableAmountWithoutDiscount += Convert.ToDecimal(courseSubject.Payment);
                                studentCourseDetailList.Add(studentCourseDetailObj);

                            }
                        }
                        //check if existing course is assigned to other course as complementary
                        Course courseObj = _courseService.GetCourse(course.Id);//Newly taken course

                        if (courseObj.ComplementaryCourses.Count > 0) //if it contains complementary course 
                        {
                            foreach (var complementaryCourse in courseObj.ComplementaryCourses)
                            {
                                IList<CourseSubject> courseSubjectList = _courseSubjectService.LoadCourseSubject(complementaryCourse.CompCourse.Id);
                                foreach (var cSub in courseSubjectList)
                                {
                                    StudentCourseDetail stdCourseDetailObj =
                                        _studentCourseDetailsService
                                            .LoadStudentCourseDetailByCourseSubjectAndStudentProgram(cSub,
                                                studentProgram);
                                    if (stdCourseDetailObj != null)//if student had already taken this course then add it to removal list 
                                    {
                                        removalList.Add(stdCourseDetailObj);
                                    }
                                }
                                //bool isAlreadyTaken = _studentCourseDetailsService.
                            }
                        }


                    }
                    /* Discount Calculation */
                    var existingDiscountList =
                             _discountService.LoadDiscount(studentProgram.Program,
                                 studentProgram.Batch.Session, studentProgram.CreationDate);
                    //var courseCount = studentViewModel.CourseViewModels.Count;
                    Discount appliedDiscountObj = GetDiscount(existingDiscountList, studentViewModel);

                    //TODO
                    //programatically discount : if this student has a course which has been assigned as a free or complemenatry course of any current course.
                    //studentProgram.StudentCourseDetails

                    #region oldCode Discount calculation for Newly added Course
                    //IList<Discount> discountApplicableListForStd = new List<Discount>();
                    //foreach (var discountObj in existingDiscountList)
                    //{
                    //    IList<DiscountDetail> discountDetailsList =
                    //        _discountService.GetAllDiscountDetailByDiscountId(discountObj.Id);
                    //    /*Check : does student deserve discount or not */
                    //    if (discountDetailsList.Count <= studentViewModel.CourseViewModels.Count)
                    //    {
                    //        var countdiscountDetailsList = discountDetailsList.Count;
                    //        int jj = 0;
                    //        foreach (var discountDetail in discountDetailsList)
                    //        {
                    //            var dCourse = discountDetail.Course;
                    //            var dMinSub = discountDetail.MinSubject;
                    //            int ii = 0;
                    //            foreach (var stdAssignedCourse in studentViewModel.CourseViewModels)
                    //            {
                    //                var stdCourse = _courseService.LoadByCourselId(stdAssignedCourse.Id);
                    //                if (stdCourse == dCourse && dMinSub <= stdAssignedCourse.SubjectViewModels.Count)
                    //                {
                    //                    /*TODO*/
                    //                    jj++;
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //        /*Contain each and Every Course and their minSubject which allows discount */
                    //        if (countdiscountDetailsList == jj)
                    //        {
                    //            discountApplicableListForStd.Add(discountObj);
                    //        }
                    //    }
                    //}
                    //var appliedDiscountObj = new Discount();
                    //if (discountApplicableListForStd.Count > 1)
                    //{
                    //    appliedDiscountObj = _discountService.GetMostApplicableDiscountObj(discountApplicableListForStd);
                    //}
                    //if (discountApplicableListForStd.Count == 1)
                    //{
                    //    appliedDiscountObj = discountApplicableListForStd[0];
                    //}
                    /*Discount Calculation Finished*/
                    #endregion

                    var paymentdetail = new PaymentDetails();
                    paymentdetail.PrevDueAmount = studentProgram.DueAmount;
                    paymentdetail.CourseFee = receivableAmountWithoutDiscount;

                    if (appliedDiscountObj.Amount > 0 && appliedDiscountObj.Amount < receivableAmountWithoutDiscount)
                    {
                        /*after calculating discount student have to pay following amount*/
                        if (appliedDiscountObj.Amount != null)
                        {
                            paymentdetail.OfferedDiscount = (decimal)appliedDiscountObj.Amount;
                            paymentdetail.PayableAmount = (decimal)(paymentdetail.PrevDueAmount +
                                                                        (paymentdetail.CourseFee - paymentdetail.OfferedDiscount));
                        }
                    }
                    else
                    {
                        paymentdetail.OfferedDiscount = 0;
                        paymentdetail.PayableAmount = (decimal)(paymentdetail.CourseFee + studentProgram.DueAmount);
                    }
                    paymentdetail.DueAmount = paymentdetail.PayableAmount;
                    //var paymentM = new SelectList(_commonHelper.GetPaymentMethods(), "Key", "Value");
                    //var paymentMethodList = (from KeyValuePair<string, int> statusKeyValuePair in paymentM.Items
                    //                         select new SelectListItem()
                    //                         {
                    //                             Text = statusKeyValuePair.Key,
                    //                             Value = statusKeyValuePair.Value.ToString()
                    //                         }).ToList();
                    var refList = new SelectList(_referenceService.GetAllRefferer(), "Id", "Name");
                    ViewBag.RefererList = refList;
                    ViewBag.PaymentMethods = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentMethod>(new List<int> { (int)PaymentMethod.BKash, (int)PaymentMethod.Cheque }), "Key", "Value");//paymentMethodList;
                    return PartialView("Partial/_PaymentDetailsForCourseAdd", paymentdetail);
                    //}
                    //return Json(new Response(false, "Course Add Failed(Possible Cause: Slow Internet Speed) "));

                }
                return Json(new Response(false, "Course Add Failed(Possible Cause: Student Roll Number Missing or Invalid) "));
            }
            catch (Exception exception)
            {
                return Json(new Response(false, "Course Add Failed(Possible Cause: Slow Internet Connection) "));
                //throw;
            }
        }
        [HttpPost]
        public ActionResult NewCourseAddPaymentAndMoneyReceiptGeneration(String studentObj, String stdPId, decimal recavailAmount, decimal nRec, decimal sDis, decimal recvdAmount, string paymentMethod, string referrerenceNote, string nextRecDate = "", string referrer = "")
        {
            try
            {
                decimal receivableAmountWithoutDiscount = 0;
                if (!String.IsNullOrEmpty(stdPId))
                {
                    var stdProRoll = stdPId.Trim();
                    SelectList paymentM;
                    var studentCourseDetailList = new List<StudentCourseDetail>();
                    IList<StudentCourseDetail> removalList = new List<StudentCourseDetail>();
                    var jjScriptSerializer = new JavaScriptSerializer();
                    var studentViewModel = jjScriptSerializer.Deserialize<StudentViewModel>(studentObj);

                    var studentProgram = _studentProgramService.GetStudentProgram(stdProRoll, true);
                    if (studentProgram == null) { return Json(new Response(false, "Transaction Process Failed(Possible Cause : Invalid Program Roll )")); }
                    foreach (var course in studentViewModel.CourseViewModels)
                    {
                        var courseProxy = _courseService.GetCourse(course.Id);
                        
                        foreach (var courseSubject in course.SubjectViewModels)
                        {
                            var studentCourseDetailObj = new StudentCourseDetail();
                            var courseSubjectObj = _courseSubjectService.LoadCourseSubject(course.Id, courseSubject.Id);

                            studentCourseDetailObj.CourseSubject = courseSubjectObj;
                            studentCourseDetailObj.StudentProgram = studentProgram;
                            studentCourseDetailObj.CourseId = courseSubjectObj.Course.Id;//Add Course Id
                            receivableAmountWithoutDiscount += Convert.ToDecimal(courseSubject.Payment);
                            studentCourseDetailList.Add(studentCourseDetailObj);
                        }
                        //check if existing course is assigned to other course as complementary
                        //Course courseObj = _courseService.GetCourse(course.Id);//Newly taken course

                        if (courseProxy.ComplementaryCourses.Count > 0) //if this course contains complementary course 
                        {
                            foreach (var complementaryCourse in courseProxy.ComplementaryCourses)
                            {
                                IList<CourseSubject> courseSubjectList = _courseSubjectService.LoadCourseSubject(complementaryCourse.CompCourse.Id);
                                foreach (var cSub in courseSubjectList)
                                {
                                    StudentCourseDetail stdCourseDetailObj =
                                        _studentCourseDetailsService
                                            .LoadStudentCourseDetailByCourseSubjectAndStudentProgram(cSub,
                                                studentProgram);
                                    if (stdCourseDetailObj != null)//if student had already taken this course then add it to removal list 
                                    {
                                        removalList.Add(stdCourseDetailObj);
                                    }
                                }
                            }
                        }
                    }

                    /* Discount Calculation Start */
                    var existingDiscountList =
                        _discountService.LoadDiscount(studentProgram.Program, studentProgram.Batch.Session, studentProgram.CreationDate);
                    Discount appliedDiscountObj = GetDiscount(existingDiscountList, studentViewModel);
                    var paymentdetail = new PaymentDetails();
                    paymentdetail.PrevDueAmount = studentProgram.DueAmount;
                    paymentdetail.CourseFee = receivableAmountWithoutDiscount;

                    if (appliedDiscountObj.Amount > 0 && appliedDiscountObj.Amount < receivableAmountWithoutDiscount)
                    {
                        /*after calculating discount student have to pay below amount*/
                        if (appliedDiscountObj.Amount != null)
                        {
                            paymentdetail.OfferedDiscount = (decimal)appliedDiscountObj.Amount;
                            paymentdetail.PayableAmount = (decimal)(paymentdetail.PrevDueAmount + (paymentdetail.CourseFee - paymentdetail.OfferedDiscount));
                        }
                    }
                    else
                    {
                        paymentdetail.OfferedDiscount = 0;
                        paymentdetail.PayableAmount = (decimal)(paymentdetail.CourseFee + studentProgram.DueAmount);
                    }
                    if (paymentdetail.PayableAmount == recavailAmount)
                    {
                        try
                        {
                            Referrer referrerObj = null;
                            /*If Special Discount Applicable */
                            if (sDis > 0)
                            {
                                referrerObj = _referenceService.LoadById(Convert.ToInt64(referrer.Trim()));
                            }
                            if (recavailAmount > 0 && nRec >= 0 && recvdAmount >= 0)
                            {
                                if ((sDis + recvdAmount) > 0)
                                {
                                    if (recvdAmount == (recavailAmount - sDis) && recavailAmount >= recvdAmount)
                                    {
                                        /*Payment Clear*/
                                        /*does not need nextPayment date*/
                                        string[] recAmountDisAmount =
                                        {
                                            paymentdetail.PayableAmount.ToString(),
                                            appliedDiscountObj.Amount.ToString()
                                        };
                                        StudentPayment studentPayment = _studentPaymentService.OrganizeObjectUsedAddCourse(
                                            recavailAmount,
                                            recvdAmount,
                                            sDis,
                                            nRec,
                                            referrerenceNote,
                                            paymentMethod,
                                            referrerObj,
                                            recAmountDisAmount,
                                            stdPId.Trim(),
                                            studentCourseDetailList
                                            );

                                        studentPayment.NextReceivedDate = UdvashERP.BusinessRules.Constants.MaxDateTime;
                                        bool isPaymentProcessSuccess = _studentPaymentService.IsPaymentProcessSuccessForAddingCourse(studentCourseDetailList, studentPayment, stdProRoll, removalList);
                                        if (isPaymentProcessSuccess)
                                        {
                                            //var sPayment = _studentPaymentService.LoadByStudentProgram(studentProgram);
                                            studentProgram.Status = StudentProgram.EntityStatus.Active;
                                            _studentProgramService.Update(studentProgram);
                                            bool flag = SendSmsApi.GenarateInstantSmsAndNumber(3, studentProgram.Program.Organization.Id, studentProgram.Program.Id, studentProgram.Batch.Session.Id, studentProgram.Student.Id, studentProgram.Id);
                                            return Json(new Response(true, "Payment success", studentPayment.Id.ToString()));
                                        }
                                        return Json(new Response(false, "Transaction Process Failed(Possible cause: DB operation)"));
                                    }

                                    if (recvdAmount < (recavailAmount - sDis) && recavailAmount >= recvdAmount)
                                    {
                                        /*Payment Not clear*/
                                        /*Next payment date requiured*/
                                        if (!String.IsNullOrEmpty(nextRecDate))
                                        {
                                            var nextRDate = Convert.ToDateTime(nextRecDate);
                                            if (nextRDate >= DateTime.Now.Date)
                                            {
                                                string[] recAmountDisAmount =
                                            {
                                                paymentdetail.PayableAmount.ToString(),
                                                appliedDiscountObj.Amount.ToString()
                                            };
                                                StudentPayment studentPayment =
                                                    _studentPaymentService.OrganizeObjectUsedAddCourse(recavailAmount,
                                                        recvdAmount, sDis, nRec,
                                                        referrerenceNote, paymentMethod, referrerObj, recAmountDisAmount,
                                                        stdPId.Trim(), studentCourseDetailList);
                                                studentPayment.NextReceivedDate = nextRDate;

                                                bool isPaymentProcessSuccess =
                                                    _studentPaymentService.IsPaymentProcessSuccessForAddingCourse(
                                                        studentCourseDetailList, studentPayment, stdProRoll, removalList);
                                                if (isPaymentProcessSuccess)
                                                {
                                                    //var sPayment = _studentPaymentService.LoadByStudentProgram(studentProgram);
                                                    studentProgram.Status = StudentProgram.EntityStatus.Active;
                                                    _studentProgramService.Update(studentProgram);
                                                    bool flag = SendSmsApi.GenarateInstantSmsAndNumber(3, studentProgram.Program.Organization.Id, studentProgram.Program.Id, studentProgram.Batch.Session.Id, studentProgram.Student.Id, studentProgram.Id);
                                                    //MoneyReceipt moneyReceipt = GetMoneyReceipt(studentPayment);
                                                    return Json(new Response(true, "Payment success", studentPayment.Id.ToString()));
                                                }
                                            }
                                            return
                                                Json(new Response(false, "Transaction Process Failed(Next Receivable date must be greater than Today's Date)"));
                                        }
                                        return
                                            Json(new Response(false, "Transaction Process Failed(Next Receivable date Missing)"));
                                    }
                                    return Json(new Response(false, "Transaction Process Failed(Invalid Value Insert)"));
                                }
                                return Json(new Response(false, "Transaction Process Failed(Possible Cause : Addition of Special discount and received amount must be greater than 0(Empty Transaction not Excepted))"));
                            }
                            return Json(new Response(false, "Transaction Process Failed(Possible Cause : Invalid Program Roll )"));
                        }
                        catch (Exception)
                        {
                            return Json(new Response(false, "Transaction Process Failed"));
                        }
                    }
                    return Json(new Response(false, "Transaction Process Failed(Possible Cause : Data Tempering Found)"));
                }
                return Json(new Response(false, "Transaction Process Failed(Possible Cause : Invalid Program Roll )"));

            }
            catch (Exception)
            {
                return Json(new Response(false, "Transaction Process Failed"));
            }
        }



        /*Not Used */
        private string GetReceivableAmountFromStudent(string stdProId)
        {
            try
            {
                decimal rreceivableAmountWithoutDiscount = 0;
                decimal recavailableAmount = 0;
                var studentProgram = _studentProgramService.GetStudentProgram(stdProId.Trim());
                var studentCourseDetailsList =
                      _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id);
                var query = studentCourseDetailsList.GroupBy(x => x.CourseSubject.Course,
                    (key, g) => new { Course = key, Subjects = g.ToList() });

                var courseList = query.ToList();
                var studentViewModel = new StudentViewModel();
                foreach (var courseAndCorrespondingSubjectList in courseList)
                {
                    var courseViewModel = new CourseViewModel
                    {
                        Id = courseAndCorrespondingSubjectList.Course.Id,
                        Name = courseAndCorrespondingSubjectList.Course.Name
                    };
                    var courseSubjectList = courseAndCorrespondingSubjectList.Subjects;
                    foreach (var courseSubject in courseSubjectList)
                    {
                        var subjectViewModel = new SubjectViewModel();
                        var subject = courseSubject.CourseSubject;
                        subjectViewModel.Name = courseSubject.CourseSubject.Subject.Name;
                        subjectViewModel.Payment = subject.Payment;
                        subjectViewModel.Id = subject.Id;
                        rreceivableAmountWithoutDiscount += Convert.ToDecimal(subject.Payment.ToString().Trim());
                        courseViewModel.SubjectViewModels.Add(subjectViewModel);
                    }
                    studentViewModel.CourseViewModels.Add(courseViewModel);
                }
                StudentPayment lastStudentPaymentTransication = _studentPaymentService.LoadByStudentProgram(studentProgram);
                if (lastStudentPaymentTransication == null)
                {
                    /*First Transication in this Program*/
                    /*Calculate Discount*/
                    var existingDiscountList =
                            _discountService.LoadDiscount(studentProgram.Program,
                                studentProgram.Batch.Session, studentProgram.CreationDate);
                    /*Discount Calculation Start */
                    Discount appliedDiscountObj = GetDiscount(existingDiscountList, studentViewModel);
                    var courseCount = studentViewModel.CourseViewModels.Count;

                    if (appliedDiscountObj.Amount > 0)
                    {
                        recavailableAmount = (decimal)(rreceivableAmountWithoutDiscount - appliedDiscountObj.Amount);
                        return recavailableAmount.ToString() + '#' + appliedDiscountObj.Amount;
                    }
                    recavailableAmount = rreceivableAmountWithoutDiscount - 0;
                    return recavailableAmount.ToString() + '#' + "0";
                }
                /*Already paid some amount */
                /*Pick Discount amount from db for this Program*/
                if (lastStudentPaymentTransication.DueAmount != null)
                    recavailableAmount = (decimal)lastStudentPaymentTransication.DueAmount;
                return recavailableAmount.ToString() + '#' + lastStudentPaymentTransication.DiscountAmount.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
        private MoneyReceipt GetMoneyReceipt(StudentPayment studentPayment)
        {
            var moneyReceipt = new MoneyReceipt();
            moneyReceipt.MrNo = studentPayment.ReceiptNo;
            moneyReceipt.Session = studentPayment.StudentProgram.Batch.Session.Name;
            moneyReceipt.Program = studentPayment.StudentProgram.Program.Name;
            moneyReceipt.RegNo = studentPayment.StudentProgram.Student.RegistrationNo;
            moneyReceipt.DateOFSubmission = studentPayment.CreationDate;
            moneyReceipt.PrRollNo = studentPayment.StudentProgram.PrnNo;
            moneyReceipt.StudentName = studentPayment.StudentProgram.Student.NickName;
            moneyReceipt.Branch = studentPayment.StudentProgram.Batch.Branch.Name;
            moneyReceipt.BatchDays = studentPayment.StudentProgram.Batch.Days;
            moneyReceipt.BatchName = studentPayment.StudentProgram.Batch.Name;
            moneyReceipt.BatchTime = studentPayment.StudentProgram.Batch.FormatedBatchTime;
            moneyReceipt.CampusAddress = (studentPayment.DueAmount).ToString();
            moneyReceipt.PaidAmount = studentPayment.ReceivedAmount.ToString();
            var studentCList =
                _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(
                    studentPayment.StudentProgram.Id);
            moneyReceipt.CourseDetails = studentCList;
            moneyReceipt.DueAmount = studentPayment.DueAmount.ToString();
            if (studentPayment.NextReceivedDate != null)
                moneyReceipt.NextPaymentDate = (DateTime)studentPayment.NextReceivedDate;
            return moneyReceipt;
        }

        #region OLD IS GOLD



        #endregion

    }

}