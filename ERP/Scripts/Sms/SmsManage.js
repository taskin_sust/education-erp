

    function dataTableRender() {
        var pageSize = 10;
        var organizationId = $("#Organization").val();
        if ($('#pageSize').val() != "" && /^\d+$/.test($('#pageSize').val())) {
            pageSize = parseInt($('#pageSize').val());
        }
        else {
            $('#pageSize').val(pageSize);
        }
        $('#DataGrid').dataTable({
            destroy: true,
            "processing": true,
            searching: false,
            serverSide: true,
            "scrollX": true,
            "bLengthChange": false,
            "iDisplayLength": pageSize,
            //stateSave: true,
            order: [[0, "desc"]],
            _pageSetter: function (val) {
                return isFinite(+val) ? +val : null;
            },

            page: {
                value: 1,
                setter: '_pageSetter'
            },
            ajax: {
                url: 'SmsSettingsListResult',
                type: 'POST',
                data: function (d) {
                    d.programId = $('#Program').val();
                    d.smsTypeId = $('#SmsType').val();                   
                    d.status = $('#status').val();
                    d.organizationId = organizationId;
                }
            }
        });
    }
    dataTableRender();
    $(document).on("click", "#search", function () {
        dataTableRender();
    });

    $(document).on("click", ".glyphicon-th-list", function () {
        console.log("Hi");
        var id = $(this).attr("id");
        if (id != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/Sms/Details",
                cache: false,
                async: true,
                data: { "id": id },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.hasOwnProperty('IsSuccess')) {
                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    else {
                        bootbox.alert(result).css('margin-top', (($(window).height() / 4)));
                        $(".bootbox-close-button").addClass("glyphicon").addClass("glyphicon-remove").text("");
                        $(".modal-footer").css("margin-top","0px").css("padding","10px").css("padding-right","20px");
                       
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.unblockUI();
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
        return false;
    });

    $(document).on("click", ".glyphicon-trash", function () {
        var id = $(this).attr("id");
        var name = $(this).attr("data-name");
        if (id != "") {
            bootbox.deleteConfirm("<h3>Are you sure you want to delete this <span class='confirm-message'>" + name + "</span> Menu Group?</h3>", function (result) {
                if (result) {
                    $.ajax({
                        type: "post",
                        url: $("body").attr("data-project-root") + "Administration/Sms/Delete",
                        cache: false,
                        async: true,
                        data: { "id": id },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (result) {
                            $.unblockUI();

                            if (result.IsSuccess) {
                                $.fn.customMessage({
                                    displayMessage: "SMS Settings Delete Successfully",
                                    displayMessageType: "success",
                                });
                                dataTableRender();
                            }
                            else {
                                $.fn.customMessage({
                                    displayMessage: result.Message,
                                    displayMessageType: "e",
                                });
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (result) {
                            $.fn.customMessage({
                                displayMessage: "SMS Settings Delete Failed",
                                displayMessageType: "e",
                            });
                        }
                    });

                }
            }).css({ 'margin-top': (($(window).height() / 4)) });
        }
        return false;
    });

$(document).ready(function () {
    $("#Organization").change(function () {
        $("#Program option").remove();
        $('#Program').append("<option>All</option>");
        var organizationId = $(this).val();
        if (organizationId != "" && organizationId != null) {
           
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Administration/Sms/GetProgramByOrganization",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                cache: false,
                async: true,
                data: { organizationIds: organizationId, isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        var i = 0;
                        $.each(result.returnProgramList, function (i, v) {
                            //if (i == 0) {
                            //    $('#Program').append($('<option>').text(v.Text).attr('value', ""));
                            //}
                            //else {
                                $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                            //}
                            //i++;
                        });
                        //$("#Program option:first").value() = "";
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
        
    });
});

    