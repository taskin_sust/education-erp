﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Administration.ProgramSettingTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "ProgramSetting")]
    public class ProgramSettingUpdateTest : ProgramSettingBaseTest
    {
        #region Basic Test
        [Fact]
        public void ProgramSetting_Update_Should_Return_True()
        {
            _programFactory.CreateMore(3).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);
            var thirdProgram = _programService.GetProgram(_programFactory.ObjectList[2].Id);

            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, nextProgramSession).Persist();
            var programSettingObj = _programSettingService.GetProgramSetting(Convert.ToInt32(_programSettingFactory.Object.Id));

            bool isSuccess = _programSettingService.Update(programSettingObj, program, programSession, thirdProgram, nextProgramSession);
            Assert.True(isSuccess);
        }
        #endregion
        #region Business Logic Test
        [Fact]
        public void ProgramSetting_Save_EmptyFieldException_For_Null_ProgramSettingObject()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);

            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);
            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, nextProgramSession).Persist();
            Assert.Throws<NullObjectException>(() => _programSettingService.Update(null, program, programSession, nextProgram, nextProgramSession));
        }
        [Theory]
        [InlineData("program")]
        [InlineData("nextProgram")]
        [InlineData("programSession")]
        [InlineData("nextProgramSession")]
        public void ProgramSetting_Update_EmptyFieldException_For_Null_Program(string fact)
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);

            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);
            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, nextProgramSession).Persist();
            var programSettingObj = _programSettingService.GetProgramSetting(Convert.ToInt32(_programSettingFactory.Object.Id));
            if (fact.Equals("program"))
            {
                program = new Program();
            }
            else if (fact.Equals("nextProgram"))
            {
                nextProgram = new Program();
            }
            else if (fact.Equals("programSession"))
            {
                programSession = new Session();
            }
            else if (fact.Equals("nextProgramSession"))
            {
                nextProgramSession = new Session();
            }
            Assert.Throws<EmptyFieldException>(() => _programSettingService.Update(programSettingObj, program, programSession, nextProgram, nextProgramSession));
        }
        [Fact]
        public void ProgramSetting_Update_Validation_Exception_For_Reverse_ProgramAndSession()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);

            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, nextProgramSession).Persist();
            var programSettingObj = _programSettingService.GetProgramSetting(Convert.ToInt32(_programSettingFactory.Object.Id));
            Assert.Throws<DuplicateEntryException>(() => _programSettingService.Update(programSettingObj, nextProgram, nextProgramSession, program, programSession));
        }
        [Fact]
        public void ProgramSetting_Update_Validation_Exception_For_Same_Program_Entry()
        {
            _programFactory.CreateMore(2).Persist();
            var program = _programService.GetProgram(_programFactory.ObjectList[0].Id);
            var nextProgram = _programService.GetProgram(_programFactory.ObjectList[1].Id);

            _sessionFactory.CreateMore(2).Persist();
            var programSession = _sessionService.LoadById(_sessionFactory.ObjectList[0].Id);
            var nextProgramSession = _sessionService.LoadById(_sessionFactory.ObjectList[1].Id);

            _programSettingFactory.Create().WithProperties(program, nextProgram, programSession, nextProgramSession).Persist();
            var programSettingObj = _programSettingService.GetProgramSetting(Convert.ToInt32(_programSettingFactory.Object.Id));
            Assert.Throws<DuplicateEntryException>(() => _programSettingService.Update(programSettingObj, program, programSession, program, nextProgramSession));
        }
        #endregion
    }
}
