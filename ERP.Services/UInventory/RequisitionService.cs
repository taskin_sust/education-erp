using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using log4net;
using NHibernate;
using NHibernate.Linq.Functions;
using NHibernate.SqlCommand;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
    public interface IRequisitionService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(Requisition requisition);
        void CancelRequisition(Requisition requisitionObj);
        bool SaveRequisition(Requisition model, List<UserMenu> userMenu);

        #endregion

        #region Single Instances Loading Function

        Requisition LoadById(long id);
        string GetLastRequisitionSlipNo(long branchId);

        string GetRequisitionSlipName(long organizationId, Branch branch);


        int CountRequisitionSlip(string orderBy, string orderDir, List<UserMenu> list, long organization, long branch
                                    , string itemName, string requiredDate, int rqStatus, bool isListForIssue);

        #endregion

        #region List Loading Function

        IList<Requisition> LoadRequisitionSlipList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenus
                                                    , long organization, long branch, string itemName, string requiredDate, int rqStatus, bool isListForIssue);

        IList<RequisitionListReportDto> RequisitionReportByList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenus, long organizationId,
                                                                    long[] branchIds, long[] itemGroupIds, long[] itemIds, string[] programSession,
                                                                    int[] purposeIds, int[] status, DateTime dateFrom, DateTime dateTo);

        IList<dynamic> RequisitionReportByBranch(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu
                                                , long organizationId, long[] branchIds, int[] purposeIds, string[] programSession
                                                , long[] itemGroupIds, long[] itemIds, int[] status, DateTime dateFrom, DateTime dateTo);

        IList<dynamic> RequisitionReportByProgramSession(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu
                                                , long organizationId, long[] branchIds, int[] purposeIds, long[] itemIds, long[] itemGroupIds
                                                , string[] programSession, int[] status, DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Others Function

        int GetRequisitionReportByListCount(List<UserMenu> userMenus, long organizationId, long[] branchId
                                            , long[] itemGroupIds, long[] itemIds, string[] programSession
                                            , int[] purposeIds, int[] status, DateTime dateFrom, DateTime dateTo);

        int RequisitionReportByBranchCount(List<UserMenu> userMenu, long organizationId, long[] branchIds
                                            , int[] purposeIds, string[] programSession, long[] itemGroupIds
                                            , long[] itemIds, int[] status, DateTime dateFrom, DateTime dateTo);
        int RequisitionReportByProgramSessionCount(List<UserMenu> userMenus, long organizationId, long[] branchIds
                                                    , int[] purposeIds, string[] programSession, long[] itemGroupIds
                                                    , long[] itemIds, int[] status, DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Helper Function

        string[] FilterProgramSession(List<UserMenu> userMenus, List<long> authOrgList, long[] bIds, long[] itemIds, string[] programSession);

        #endregion

    }
    public class RequisitionService : BaseService, IRequisitionService
    {

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("InventoryService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IRequisitionDao _requisitionDao;
        private readonly IRequisitionDetailDao _requisitionDetailDao;
        private readonly IBranchDao _branchDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly IItemDao _itemDao;
        private readonly IProgramSessionItemDao _programSessionItemDao;
        private readonly IProgramBranchSessionDao _programBranchSessionDao;
        public RequisitionService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _requisitionDao = new RequisitionDao { Session = session };
            _requisitionDetailDao = new RequisitionDetailsDao { Session = session };
            _branchDao = new BranchDao { Session = session };
            _programDao = new ProgramDao { Session = session };
            _sessionDao = new SessionDao { Session = session };
            _organizationDao = new OrganizationDao { Session = session };
            _itemDao = new ItemDao { Session = session };
            _programSessionItemDao = new ProgramSessionItemDao { Session = session };
            _programBranchSessionDao = new ProgramBranchSessionDao() { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool SaveRequisition(Requisition model, List<UserMenu> userMenu)
        {
            if (model == null)
            {
                throw new NullObjectException("Requsition form not found!");
            }
            if (userMenu == null)
            {
                throw new InvalidDataException("User Menu form not found!");
            }
            bool result;
            var authBranchIds = AuthHelper.LoadBranchIdList(userMenu, null, null, _commonHelper.ConvertIdToList(model.Branch.Id));

            var programIds = model.RequisitionDetails.Where(x => x.Program != null).Select(x => x.Program.Id).ToList();
            var authProgramList = AuthHelper.LoadProgramIdList(userMenu, null, null, programIds);


            if (authBranchIds.Count > 0 && authBranchIds[0] == model.Branch.Id)
            {
                if (authProgramList != null && authProgramList.Count > 0)
                {
                    var inListButNotInProgramIds = authProgramList.Except(programIds).ToList();
                    if (programIds.Count > 0 && inListButNotInProgramIds.Count > 0)
                    {
                        throw new InvalidDataException("Unauthorize program selected!");
                    }
                    result = SaveOrUpdate(model);
                }
                else
                {
                    throw new InvalidDataException("Unauthorize program selected!");
                }
            }
            else
            {
                throw new InvalidDataException("Unauthorize branch selected!");
            }
            return result;
        }

        public bool SaveOrUpdate(Requisition requisition)
        {
            bool returnText = false;
            ITransaction trans = null;
            try
            {
                if (requisition == null)
                {
                    throw new NullObjectException("Requsition form not found!");
                }
                if (requisition.RequisitionDetails == null || requisition.RequisitionDetails.Count <= 0)
                {
                    throw new NullObjectException("Requsition form should contain minimum one item details!");
                }
                if (requisition.RequisitionDetails.Any(rd => rd.Item == null || rd.Purpose <= 0 || rd.RequiredQuantity <= 0))
                {
                    throw new InvalidDataException("Requsition form contain invalid item details row!");
                }
                /*
                if (requisition.RequisitionDetails.Where(requisitionDetailse => requisitionDetailse.Item.ItemType == (int)ItemType.ProgramItem).
                    Any(requisitionDetailse => requisitionDetailse.Program == null && requisitionDetailse.Session == null))
                    throw new InvalidDataException("Requsition form contain invalid item details row!");
                */

                if (requisition.RequisitionDetails.Any(requisitionDetailse =>
                    requisitionDetailse.Program == null || requisitionDetailse.Session == null ||
                    requisitionDetailse.PurposeProgram == null || requisitionDetailse.PurposeSession == null)
                    )
                {
                    throw new InvalidDataException("Requsition form contain invalid item details row!");
                }

                var distincList = new HashSet<RequisitionDetails>(requisition.RequisitionDetails);
                if (requisition.RequisitionDetails.Count != distincList.Count)
                {
                    throw new InvalidDataException("Requsition form contain item details row with same property!");
                }
                CheckValidation(requisition);
                UniquePropertyCheck(requisition);
                //CheckItemsValidationWithProgramSession(requisition);
                using (trans = Session.BeginTransaction())
                {
                    _requisitionDao.SaveOrUpdate(requisition);
                    trans.Commit();
                    returnText = true;
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                {
                    trans.Rollback();
                }
            }
            return returnText;
        }

        private void CheckItemsValidationWithProgramSession(Requisition requisition)
        {
            foreach (var row in requisition.RequisitionDetails)
            {
                if (row.Item.ItemType == (int)ItemType.ProgramItem)
                {
                    if (row.Program == null || row.Session == null)
                    {
                        throw new InvalidDataException("Invalid item selected! Please try again!");
                    }
                    var entity = _programSessionItemDao.GetProgramSessionItem(row.Item, row.Program, row.Session);
                    if (entity == null)
                    {
                        throw new InvalidDataException("Invalid item selected! Please try again!");
                    }
                }
                else if (row.Item.ItemType == (int)ItemType.CommonItem)
                {
                    var entity = _programSessionItemDao.GetProgramSessionItem(row.Item);
                    if (entity == null)
                    {
                        throw new InvalidDataException("Invalid item selected! Please try again!");
                    }
                    if (row.Purpose > 0 && row.Purpose <= (int)Purpose.GiftAndPromotionPurpose)
                    {
                        if (row.Program == null || row.Session == null)
                        {
                            throw new InvalidDataException("Invalid item selected! Please try again!");
                        }
                    }
                }
            }
        }

        public void CancelRequisition(Requisition requisitionObj)
        {
            ITransaction trans = null;
            try
            {
                if (requisitionObj == null)
                {
                    throw new NullObjectException("Requsition not found! Please try again!");
                }
                requisitionObj.RequisionStatus = (int)RequisitionStatus.Cancelled;
                using (trans = Session.BeginTransaction())
                {
                    _requisitionDao.SaveOrUpdate(requisitionObj);
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                {
                    trans.Rollback();
                }
            }
        }

        #endregion

        #region Single Instances Loading Function

        public Requisition LoadById(long id)
        {
            try
            {
                return _requisitionDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<Requisition> LoadRequisitionSlipList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, long organizationId
                                                            , long branchId, string itemName, string requiredDate, int status, bool isListForIssue)
        {
            try
            {
                if (userMenu == null || userMenu.Count <= 0)
                {
                    return new List<Requisition>();
                }
                IList<long> authBranchIds;
                if (organizationId <= 0 && branchId <= 0)
                {
                    authBranchIds = AuthHelper.LoadBranchIdList(userMenu);
                }
                else
                {
                    authBranchIds = branchId <= 0
                                    ? AuthHelper.LoadBranchIdList(userMenu, _commonHelper.ConvertIdToList(organizationId))
                                    : AuthHelper.LoadBranchIdList(userMenu, null, null, _commonHelper.ConvertIdToList(branchId));
                }
                return _requisitionDao.LoadRequisition(start, length, orderBy, orderDir, authBranchIds.ToList(), itemName, requiredDate, status, isListForIssue);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<RequisitionListReportDto> RequisitionReportByList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenus, long organizationId,
            long[] branchIds, long[] itemGroupIds, long[] itemIds, string[] programSession, int[] purposeIds, int[] status, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }
                branchIds = branchIds.Where(val => val != 0).ToArray();
                long[] bIds = { };
                if (branchIds.Count() > 0 && branchIds[0] != 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, authOrgList, null, branchIds.ToList()).ToArray();
                }
                else if (branchIds.Length <= 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, _commonHelper.ConvertIdToList(organizationId)).ToArray();
                }
                if (bIds.Length <= 0)
                {
                    throw new InvalidDataException("Authorize branch not found");
                }
                itemIds = itemIds.Where(val => val != 0).ToArray();
                if (itemIds.Length <= 0 && itemGroupIds.Length > 0)
                {
                    itemGroupIds = itemGroupIds.Where(val => val != 0).ToArray();
                    itemIds = _itemDao.LoadItem(itemGroupIds, organizationId).Select(x => x.Id).ToArray();
                }
                programSession = FilterProgramSession(userMenus, authOrgList, bIds, itemIds, programSession);

                IList<RequisitionListReportDto> list = _requisitionDao.RequisitionReportByList(start, length, orderBy,
                                                        orderDir, bIds, itemGroupIds, itemIds, programSession, purposeIds, status, dateFrom, dateTo);
                return list;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public IList<dynamic> RequisitionReportByBranch(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenus,
            long organizationId, long[] branchIds, int[] purposeIds, string[] programSession, long[] itemGroupIds,
            long[] itemIds, int[] status, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }
                branchIds = branchIds.Where(val => val != 0).ToArray();
                long[] bIds = { };
                if (branchIds.Count() > 0 && branchIds[0] != 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, authOrgList, null, branchIds.ToList()).ToArray();
                }
                else if (branchIds.Length <= 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, _commonHelper.ConvertIdToList(organizationId)).ToArray();
                }
                if (bIds.Length <= 0)
                {
                    throw new InvalidDataException("Authorize branch not found");
                }
                itemIds = itemIds.Where(val => val != 0).ToArray();
                if (itemIds.Length <= 0 && itemGroupIds.Length > 0)
                {
                    itemGroupIds = itemGroupIds.Where(val => val != 0).ToArray();
                    itemIds = _itemDao.LoadItem(itemGroupIds, organizationId).Select(x => x.Id).ToArray();
                }
                programSession = FilterProgramSession(userMenus, authOrgList, bIds, itemIds, programSession);

                IList<string> branchNameList = _branchDao.LoadBranch(bIds).Select(x => x.Name).ToList();
                IList<dynamic> list = _requisitionDao.RequisitionReportByBranch(start, length, orderBy, orderDir, branchNameList
                                                                                , bIds, itemGroupIds, itemIds, purposeIds
                                                                                , programSession, status, dateFrom, dateTo);
                return list;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public IList<dynamic> RequisitionReportByProgramSession(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenus,
            long organizationId, long[] branchIds, int[] purposeIds, long[] itemIds, long[] itemGroupIds, string[] programSession, int[] status,
            DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }
                branchIds = branchIds.Where(val => val != 0).ToArray();
                long[] bIds = { };
                if (branchIds.Count() > 0 && branchIds[0] != 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, authOrgList, null, branchIds.ToList()).ToArray();
                }
                else if (branchIds.Length <= 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, _commonHelper.ConvertIdToList(organizationId)).ToArray();
                }
                if (bIds.Length <= 0)
                {
                    throw new InvalidDataException("Authorize branch not found");
                }
                itemIds = itemIds.Where(val => val != 0).ToArray();
                if (itemIds.Length <= 0 && itemGroupIds.Length > 0)
                {
                    itemGroupIds = itemGroupIds.Where(val => val != 0).ToArray();
                    itemIds = _itemDao.LoadItem(itemGroupIds, organizationId).Select(x => x.Id).ToArray();
                }
                programSession = FilterProgramSession(userMenus, authOrgList, bIds, itemIds, programSession);

                var programSessionNameList = GetProgramSessionNameList(programSession);
                IList<dynamic> list = _requisitionDao.RequisitionReportByProgramSession(start, length, orderBy, orderDir, bIds
                                                        , itemGroupIds, itemIds, purposeIds, programSessionNameList.ToArray(), programSession, status, dateFrom, dateTo);
                return list;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #endregion

        #region Others Function

        public string GetLastRequisitionSlipNo(long branchId)
        {
            try
            {
                var obj = Session.QueryOver<Requisition>().Where(x => x.Branch.Id == branchId)
                                    .OrderBy(x => x.Id).Desc.List<Requisition>().FirstOrDefault();
                return obj == null ? "" : obj.RequisitionNo;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int CountRequisitionSlip(string orderBy, string orderDir, List<UserMenu> userMenu, long organizationId, long branchId, string itemName,
            string requiredDate, int rqStatus, bool isListForIssue)
        {
            try
            {
                if (userMenu == null || userMenu.Count <= 0)
                {
                    return 0;
                }
                IList<long> authBranchIds;
                if (organizationId <= 0 && branchId <= 0)
                {
                    authBranchIds = AuthHelper.LoadBranchIdList(userMenu);
                }
                else
                {
                    authBranchIds = branchId <= 0
                                    ? AuthHelper.LoadBranchIdList(userMenu, _commonHelper.ConvertIdToList(organizationId))
                                    : AuthHelper.LoadBranchIdList(userMenu, null, null, _commonHelper.ConvertIdToList(branchId));
                }
                int count = _requisitionDao.CountRequisitionSlip(authBranchIds, itemName, requiredDate, rqStatus, isListForIssue);
                return count;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetRequisitionReportByListCount(List<UserMenu> userMenus, long organizationId, long[] branchIds, long[] itemGroupIds
                                                    , long[] itemIds, string[] programSession, int[] purposeIds
                                                    , int[] status, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }
                branchIds = branchIds.Where(val => val != 0).ToArray();
                long[] bIds = { };
                if (branchIds.Count() > 0 && branchIds[0] != 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, authOrgList, null, branchIds.ToList()).ToArray();
                }
                else if (branchIds.Length <= 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, _commonHelper.ConvertIdToList(organizationId)).ToArray();
                }
                if (bIds.Length <= 0)
                {
                    throw new InvalidDataException("Authorize branch not found");
                }
                itemIds = itemIds.Where(val => val != 0).ToArray();
                if (itemIds.Length <= 0 && itemGroupIds.Length > 0)
                {
                    itemGroupIds = itemGroupIds.Where(val => val != 0).ToArray();
                    itemIds = _itemDao.LoadItem(itemGroupIds, organizationId).Select(x => x.Id).ToArray();
                }
                programSession = FilterProgramSession(userMenus, authOrgList, bIds, itemIds, programSession);
                return _requisitionDao.RequisitionReportByListCount(bIds, itemGroupIds, itemIds, programSession, purposeIds, status, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int RequisitionReportByBranchCount(List<UserMenu> userMenus, long organizationId, long[] branchIds, int[] purposeIds,
            string[] programSession, long[] itemGroupIds, long[] itemIds, int[] status, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }
                branchIds = branchIds.Where(val => val != 0).ToArray();
                long[] bIds = { };
                if (branchIds.Count() > 0 && branchIds[0] != 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, authOrgList, null, branchIds.ToList()).ToArray();
                }
                else if (branchIds.Length <= 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, _commonHelper.ConvertIdToList(organizationId)).ToArray();
                }
                if (bIds.Length <= 0)
                {
                    throw new InvalidDataException("Authorize branch not found");
                }
                itemIds = itemIds.Where(val => val != 0).ToArray();
                if (itemIds.Length <= 0 && itemGroupIds.Length > 0)
                {
                    itemGroupIds = itemGroupIds.Where(val => val != 0).ToArray();
                    itemIds = _itemDao.LoadItem(itemGroupIds, organizationId).Select(x => x.Id).ToArray();
                }
                programSession = FilterProgramSession(userMenus, authOrgList, bIds, itemIds, programSession);

                IList<string> branchNameList = _branchDao.LoadBranch(bIds).Select(x => x.Name).ToList();

                return _requisitionDao.RequisitionReportByBranchCount(branchNameList, bIds, itemGroupIds, itemIds, programSession, purposeIds, status, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int RequisitionReportByProgramSessionCount(List<UserMenu> userMenus, long organizationId, long[] branchIds, int[] purposeIds,
            string[] programSession, long[] itemGroupIds, long[] itemIds, int[] status, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                if (organizationId <= 0)
                {
                    throw new InvalidDataException("Organization is not defined");
                }
                List<long> authOrgList = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(organizationId));
                if (authOrgList.Count <= 0)
                {
                    throw new InvalidDataException("Organization is not authorized");
                }
                branchIds = branchIds.Where(val => val != 0).ToArray();
                long[] bIds = { };
                if (branchIds.Count() > 0 && branchIds[0] != 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, authOrgList, null, branchIds.ToList()).ToArray();
                }
                else if (branchIds.Length <= 0)
                {
                    bIds = AuthHelper.LoadBranchIdList(userMenus, _commonHelper.ConvertIdToList(organizationId)).ToArray();
                }
                if (bIds.Length <= 0)
                {
                    throw new InvalidDataException("Authorize branch not found");
                }
                itemIds = itemIds.Where(val => val != 0).ToArray();
                if (itemIds.Length <= 0 && itemGroupIds.Length > 0)
                {
                    itemGroupIds = itemGroupIds.Where(val => val != 0).ToArray();
                    itemIds = _itemDao.LoadItem(itemGroupIds, organizationId).Select(x => x.Id).ToArray();
                }
                programSession = FilterProgramSession(userMenus, authOrgList, bIds, itemIds, programSession);

                var programSessionNameList = GetProgramSessionNameList(programSession);

                return _requisitionDao.RequisitionReportByProgramSessionCount(programSessionNameList.ToArray(), bIds, itemGroupIds, itemIds, programSession, purposeIds, status, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Helper function

        private static void CheckValidation(Requisition requisition)
        {
            var validationResult = ValidationHelper.ValidateEntity<Requisition, Requisition.RequisitionMetaData>(requisition);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private void UniquePropertyCheck(Requisition requisition)
        {
            string fieldName = "";
            bool isExistsByName = _requisitionDao.CheckDuplicateRequisitionName(out fieldName, requisition.RequisitionNo, requisition.Branch.Id, requisition.Id);
            if (isExistsByName)
            {
                throw new DuplicateEntryException("Duplicate requisition found for" + fieldName + "! Please try for another " + fieldName + "!");
            }
        }

        private IEnumerable<string> GetProgramSessionNameList(IEnumerable<string> programSession)
        {
            IList<string> programSessionNameList = new List<string>();

            foreach (var ps in programSession)
            {
                string[] psIds = ps.Split(new string[] { "::" }, StringSplitOptions.None);
                if (psIds.Length == 3)
                {
                    string programName = _programDao.LoadById(Convert.ToInt64(psIds[0])).ShortName;
                    string sessionName = _sessionDao.LoadById(Convert.ToInt64(psIds[1])).Name;
                    if (!string.IsNullOrWhiteSpace(programName) && !string.IsNullOrWhiteSpace(sessionName))
                    {
                        programSessionNameList.Add(programName + ":" + sessionName);
                    }
                }
                else
                {
                    programSessionNameList.Add("N/A");
                }
            }
            return programSessionNameList.Distinct().ToList();
        }

        public string GetRequisitionSlipName(long organizationId, Branch branch)
        {
            string name = "";
            try
            {
                var organizationName = _organizationDao.LoadById(organizationId).ShortName.Substring(0, 3);
                var branchCode = branch.Code.ToString(CultureInfo.CurrentCulture);

                var lastRequisitionNo = GetLastRequisitionSlipNo(branch.Id);

                string requisitionNo = "000001";

                if (lastRequisitionNo != "")
                {
                    string newSl = (Convert.ToInt32(lastRequisitionNo.Substring(lastRequisitionNo.Length - 6, 6)) + 1).ToString(CultureInfo.CurrentCulture);
                    int lengthDiff = 6 - newSl.Length;
                    for (int i = 0; i < lengthDiff; i++)
                    {
                        newSl = "0" + newSl;
                    }
                    requisitionNo = newSl;
                }
                name = organizationName + branchCode + "RS" + requisitionNo;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return name;
        }

        public string[] FilterProgramSession(List<UserMenu> userMenus, List<long> authOrgList, long[] bIds, long[] itemIds, string[] programSession)
        {
            if (programSession == null || programSession[0].Equals(string.Empty))
            {
                var authProIdList = AuthHelper.LoadProgramIdList(userMenus, authOrgList, bIds.ToList());
                var psList = _programBranchSessionDao.LoadProgramSession(authOrgList, bIds.ToList<long>(), authProIdList, itemIds.ToList(), false);
                IList<string> li = psList.Select(programSessionDto => programSessionDto.ProgramAndSessionId).ToList();
                li.Add("N/A");
                programSession = li.ToArray();
            }
            else if (programSession[0] == "0")
            {
                var ps = programSession.ToList();
                ps.RemoveAt(0);
                programSession = ps.ToArray();
            }
            return programSession;
        }

        #endregion

    }
}

