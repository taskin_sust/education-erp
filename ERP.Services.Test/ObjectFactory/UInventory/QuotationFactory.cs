using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class QuotationFactory : ObjectFactoryBase<Quotation>
    {

        private readonly IQuotationService _quotationService;
        internal readonly ProgramFactory _programFactory;
        internal readonly SessionFactory _sessionFactory;
        internal readonly OrganizationFactory _organizationFactory;

        public QuotationFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _quotationService = new QuotationService(session);
            _programFactory = new ProgramFactory(null, session);
            _sessionFactory = new SessionFactory(this, session);
            _organizationFactory = new OrganizationFactory(this, session);

        }

        #region Create

        public QuotationFactory Create()
        {
            Object = new Quotation()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = 1,
                QuotationStatus = QuotationStatus.NotPublished,
                QuotationQuantity = 100,
                Purpose = (int)Purpose.GiftAndPromotionPurpose,
                SubmissionDeadLine = DateTime.Now.AddDays(2),
                DeliveryDate = DateTime.Now.AddDays(30),
                PublishedDate = DateTime.Now,
                Remarks = "Remarks"
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public QuotationFactory CreateWith(long id, int quantity, Purpose? purpose, QuotationStatus quotationStatus, DateTime deliveryDate, DateTime submissionDate, DateTime? publishedDate)
        {

            Object = new Quotation()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = 1,
                QuotationStatus = quotationStatus,
                QuotationQuantity = quantity,
                //Purpose = (int)purpose,
                SubmissionDeadLine = submissionDate,
                DeliveryDate = deliveryDate,
                PublishedDate = publishedDate,
                Remarks = "Remarks"
            };
            if (purpose != null)
                Object.Purpose = (int) purpose;
            else
                Object.Purpose = null;

            SingleObjectList.Add(Object);
            return this;
        }

        public QuotationFactory CreateMore(int num, Organization organization, Branch branch, Item item)
        {
            _programFactory.Create().WithOrganization(organization).Persist();
            _sessionFactory.Create().Persist();
            for (int i = 1; i <= num; i++)
            {
                var obj = new Quotation()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = 1,
                    QuotationStatus = QuotationStatus.NotPublished,
                    QuotationQuantity = 1000+i,
                    Purpose = (int)Purpose.GiftAndPromotionPurpose,
                    PurposeProgram = _programFactory.Object,
                    PurposeSession = _sessionFactory.Object,
                    SubmissionDeadLine = DateTime.Now.AddDays(2),
                    DeliveryDate = DateTime.Now.AddDays(30),
                    PublishedDate = DateTime.Now,
                    Remarks = "Remarks",
                    QuotationNo = GetQuotationNo(organization, branch,i),
                    Program = _programFactory.Object,
                    Session = _sessionFactory.Object,
                    Item = item,
                    Branch = branch
                };
                obj.QuotationCriteriaList = GetCriteriaList(item, obj);
                ObjectList.Add(obj);
            }
            return this;
        }

        public QuotationFactory CreateMore(int num , List<Organization> orgList,List<Branch> brList,List<Item> itemList )
        {
            return this;
        }


        public QuotationFactory WithQuotationNo(Organization organization, Branch branch)
        {
            Object.QuotationNo = GetQuotationNo(organization, branch);
            return this;
        }

        public QuotationFactory WithQuotationNo(string quotationNo)
        {
            Object.QuotationNo = quotationNo;
            return this;
        }

        public QuotationFactory WithProgramAndSession(Program program, Session session)
        {
            Object.Program = program;
            Object.Session = session;
            return this;
        }

        public QuotationFactory WithPurposeprogramAndPurposeSession(Program program, Session session)
        {
            Object.PurposeProgram = program;
            Object.PurposeSession = session;
            return this;
        }

        public QuotationFactory WithPurposeprogramAndPurposeSession(Organization organization)
        {
            _programFactory.Create().WithOrganization(organization).Persist();
            _sessionFactory.Create().Persist();
            Object.PurposeProgram = _programFactory.Object;
            Object.PurposeSession = _sessionFactory.Object;
            return this;
        }

        public QuotationFactory WithProgramAndSession(Organization organization)
        {
            _programFactory.Create().WithOrganization(organization).Persist();
            _sessionFactory.Create().Persist();
            Object.Program = _programFactory.Object;
            Object.Session = _sessionFactory.Object;
            return this;
        }

        public QuotationFactory WithItem(Item item)
        {
            Object.Item = item;
            return this;
        }

        public QuotationFactory WithBranch(Branch branch)
        {
            Object.Branch = branch;
            return this;
        }

        public QuotationFactory WithQuotationCriteriaList(Item item)
        {
            Object.QuotationCriteriaList = GetCriteriaList(item, Object);
            return this;
        }

        private IList<QuotationCriteria> GetCriteriaList(Item item,Quotation quotation)
        {
            IList<QuotationCriteria> quotationCriteriaList = new List<QuotationCriteria>();
            foreach (var criteria in item.SpecificationTemplate.SpecificationCriteriaList)
            {
                var quotationCriteria = new QuotationCriteria
                {
                    Quotation = quotation,
                    Criteria = criteria.Name,
                    CriteriaValue = criteria.Controltype == FieldType.Dropdown
                        ? criteria.SpecificationCriteriaOptions.Select(x => x.Name).FirstOrDefault()
                        : Guid.NewGuid().ToString()
                };
                quotationCriteriaList.Add(quotationCriteria);
            }
            return quotationCriteriaList;
        }

        public QuotationFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x => { if (x.Id == 0) _quotationService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x => { if (x.Id == 0)_quotationService.SaveOrUpdate(x); });
            }
            return this;
        }

        #endregion

        #region other

        private string GetQuotationNo(Organization organization, Branch branch,int qn = 1)
        {
            string quotationNo = "";
            int quotationLast6Digit = 000000;

            if (organization != null && branch != null)
            {
                var lastQuotationNo = _quotationService.GetLastQuotationNo(branch.Id);
                quotationLast6Digit = lastQuotationNo != "" ? Convert.ToInt32(lastQuotationNo.Substring(lastQuotationNo.Length - 6, 6)) + 1 : (000000+qn);
                quotationNo = organization.ShortName.Substring(0, 2) + "" + branch.Code + "QTN";
            }
            quotationNo = quotationNo + quotationLast6Digit.ToString("000000");
            return quotationNo;

        }

        #endregion

        #region CleanUp

        #region Quotation Criteria

        internal void QuotationCriteraiCleanUp(QuotationFactory quotationFactory)
        {
            if (quotationFactory.ObjectList != null)
            {
                foreach (var quotation in quotationFactory.ObjectList)
                {
                    string swData = "DELETE FROM [UINV_QuotationCriteria] WHERE [QuotationId] = " + quotation.Id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
            if (quotationFactory.SingleObjectList != null)
            {
                foreach (var quotation in quotationFactory.SingleObjectList)
                {
                    string swData = "DELETE FROM [UINV_QuotationCriteria] WHERE [QuotationId] = " + quotation.Id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
        }

        #endregion

        #region ProgramSessionItem

        internal void ProgramSessionItemCleanUp(ItemFactory _itemFactory)
        {
            if (_itemFactory.ObjectList != null)
            {
                foreach (var item in _itemFactory.ObjectList)
                {
                    string swData = "DELETE FROM [UINV_ProgramSessionItem] WHERE [ItemId] = " + item.Id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
            if (_itemFactory.SingleObjectList != null)
            {
                foreach (var item in _itemFactory.SingleObjectList)
                {
                    string swData = "DELETE FROM [UINV_ProgramSessionItem] WHERE [ItemId] = " + item.Id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
        }

        #endregion

        #region Specification Template

        internal void SpecificationTemplateCleanUp(ItemFactory itemFactory)
        {
            if (itemFactory.ObjectList != null)
            {
                foreach (var item in itemFactory.ObjectList)
                {
                    string swData = "DELETE FROM [UINV_SpecificationTemplate] WHERE [Id] = " + item.SpecificationTemplate.Id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
            if (itemFactory.SingleObjectList != null)
            {
                foreach (var item in itemFactory.SingleObjectList)
                {
                    string swData = "DELETE FROM [UINV_SpecificationTemplate] WHERE [Id] = " + item.SpecificationTemplate.Id + ";";
                    _session.CreateSQLQuery(swData).ExecuteUpdate();
                }
            }
        }

        #endregion

        public QuotationFactory CleanUp()
        {
            DeleteAll();
            _programFactory.Cleanup();
            _sessionFactory.Cleanup();
            return this;
        }

        #endregion


    }
}