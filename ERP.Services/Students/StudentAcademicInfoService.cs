﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface IStudentAcademicInfoService : IBaseService
    {
        #region Operational Function

        bool Update(StudentAcademicInfo studentAcademicInfoObj);
        void SaveOrUpdateStudentAcademicInfoFromList(List<StudentAcademicInfo> studentAcademicInfoList);
        void SaveOrUpdateStudentAcademicInfo(Student student, List<BoardInfoView> boardInfoList);
        bool SaveSmsSenderAndUpdateStudentAcademicInfo(IList<SmsSender> smsSenders, List<StudentAcademicInfo> stdAcademicInfos, List<StudentAcademicInfo> prevStudentAcademicInfo, List<StudentAcademicInfo> savableAcademicInfos);

        #endregion

        #region Save & Update
        void Save(StudentAcademicInfo studentAcaInfo);
        #endregion

        #region Single Instances Loading Function
        StudentAcademicInfo LoadByStudentId(long studentId, long studentExmId);
        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Clean
        //StudentAcademicInfo LoadByStudentId(long id);
        #endregion
       
    }

    public class StudentAcademicInfoService : BaseService, IStudentAcademicInfoService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentAcademicInfoDao _studentAcademicInfoDao;
        private readonly IStudentDao _studentDao;
        private readonly IStudentExamDao _studentExam;
        private readonly IStudentBoardDao _studentBoard;
        private readonly ISmsSenderService _smsSenderService;
        private readonly IStudentInfoHistoryService _studentInfoHistoryService;
        public StudentAcademicInfoService(ISession session)
        {
            Session = session;
            _studentAcademicInfoDao = new StudentAcademicInfoDao() { Session = Session };
            _studentDao = new StudentDao() { Session = Session };
            _studentExam = new StudentExamDao() { Session = Session };
            _studentBoard = new StudentBoardDao() { Session = Session };
            _smsSenderService = new SmsSenderService();
            _studentInfoHistoryService = new StudentInfoHistoryService();
        }
        #endregion

        #region Operational Function
        public bool Update(StudentAcademicInfo studentAcademicInfoObj)
        {
            try
            {
                if (studentAcademicInfoObj != null)
                {
                    Student studentObj = _studentDao.LoadById(studentAcademicInfoObj.Student.Id);
                    StudentExam studentExamObj = _studentExam.LoadById(studentAcademicInfoObj.StudentExam.Id);
                    StudentBoard studentBoardObj = _studentBoard.LoadById(studentAcademicInfoObj.StudentBoard.Id);
                    if (studentObj != null && studentExamObj != null && studentBoardObj != null)
                    {
                        studentAcademicInfoObj.Student = studentObj;
                        studentAcademicInfoObj.StudentExam = studentExamObj;
                        studentAcademicInfoObj.StudentBoard = studentBoardObj;
                        _studentAcademicInfoDao.Save(studentAcademicInfoObj);
                        
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

        }

        public void SaveOrUpdateStudentAcademicInfo(Student student, List<BoardInfoView> boardInfoList)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    foreach (BoardInfoView bi in boardInfoList)
                    {
                        int year;
                        int.TryParse(bi.Year, out year);
                        if (bi.StudentExamId > 0 && year > 1900 && bi.BoardId > 0 && !string.IsNullOrEmpty(bi.BoardRoll) && !string.IsNullOrEmpty(bi.RegistrationNumber))
                        {
                            StudentAcademicInfo existingInfo = _studentAcademicInfoDao.LoadByStudentId(student.Id, bi.StudentExamId);

                            if (existingInfo == null)
                            {
                                var acaInfo = new StudentAcademicInfo
                                {
                                    Student = student,
                                    Year = bi.Year,
                                    BoradRoll = bi.BoardRoll,
                                    RegistrationNumber = bi.RegistrationNumber,
                                    StudentBoard = _studentBoard.LoadById(bi.BoardId),
                                    StudentExam = _studentExam.LoadById(bi.StudentExamId)
                                };
                                Save(acaInfo);
                            }
                            else
                            {
                                existingInfo.Year = bi.Year;
                                existingInfo.BoradRoll = bi.BoardRoll;
                                existingInfo.RegistrationNumber = bi.RegistrationNumber;
                                existingInfo.StudentBoard = _studentBoard.LoadById(bi.BoardId);
                                Update(existingInfo);
                            }
                        }
                    }
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public void SaveOrUpdateStudentAcademicInfoFromList(List<StudentAcademicInfo> studentAcademicInfoList)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    foreach (StudentAcademicInfo academicInfo in studentAcademicInfoList)
                    {
                        int year;
                        int.TryParse(academicInfo.Year, out year);
                        if (academicInfo.StudentExam.Id > 0 && year > 1900 && academicInfo.StudentBoard.Id > 0 && !string.IsNullOrEmpty(academicInfo.BoradRoll) && !string.IsNullOrEmpty(academicInfo.RegistrationNumber))
                        {
                            StudentAcademicInfo existingInfo = _studentAcademicInfoDao.LoadByStudentId(academicInfo.Student.Id, academicInfo.StudentExam.Id);

                            if (existingInfo == null)
                            {
                                var acaInfo = new StudentAcademicInfo
                                {
                                    Student = academicInfo.Student,
                                    Year = academicInfo.Year,
                                    BoradRoll = academicInfo.BoradRoll,
                                    RegistrationNumber = academicInfo.RegistrationNumber,
                                    StudentBoard = academicInfo.StudentBoard,
                                    StudentExam = academicInfo.StudentExam
                                };
                                Save(acaInfo);
                            }
                            else
                            {
                                existingInfo.Year = academicInfo.Year;
                                existingInfo.BoradRoll = academicInfo.BoradRoll;
                                existingInfo.RegistrationNumber = academicInfo.RegistrationNumber;
                                existingInfo.StudentBoard = academicInfo.StudentBoard;
                                Update(existingInfo);
                            }
                        }
                    }
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public bool SaveSmsSenderAndUpdateStudentAcademicInfo(IList<SmsSender> smsSenders, List<StudentAcademicInfo> updateStudentAcademicInfos, List<StudentAcademicInfo> prevStudentAcademicInfo, List<StudentAcademicInfo> savableAcademicInfos)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    bool? jscFlag = null;
                    bool? sscFlag = null;
                    bool? hscFlag = null;
                    List<Student> studentListObject = new List<Student>();
                    var updateTime = DateTime.Now;

                    var infoHistories = new List<StudentInfoHistory>();
                    foreach (var studentAcademicInfo in prevStudentAcademicInfo)
                    {
                        if (studentAcademicInfo != null)
                        {
                            var infoHistory = new StudentInfoHistory();
                            if (studentAcademicInfo.StudentBoard != null)
                                infoHistory.Board = studentAcademicInfo.StudentBoard.Name;
                            infoHistory.BoardRoll = studentAcademicInfo.BoradRoll;
                            if (studentAcademicInfo.StudentExam != null)
                                infoHistory.ExamName = studentAcademicInfo.StudentExam.Name;
                            infoHistory.Student = studentAcademicInfo.Student;
                            infoHistory.Year = studentAcademicInfo.Year;
                            infoHistories.Add(infoHistory);
                        }
                    }
                    if (updateStudentAcademicInfos.Any())
                    {
                        foreach (var studentAcademicInfo in updateStudentAcademicInfos)
                        {
                            Student studentTempObj = null;
                            studentAcademicInfo.Status = StudentAcademicInfo.EntityStatus.Active;
                            studentAcademicInfo.ModificationDate = updateTime;
                            studentTempObj = studentListObject.FirstOrDefault(x => x.Id == studentAcademicInfo.Student.Id);
                            if (studentTempObj == null)
                            {
                                studentTempObj = studentAcademicInfo.Student;
                                studentListObject.Add(studentTempObj);
                            }

                            switch (studentAcademicInfo.StudentExam.Id)
                            {
                                case 2:
                                    jscFlag = true;
                                    break;
                                case 3:
                                    sscFlag = true;
                                    break;
                                case 4:
                                    hscFlag = true;
                                    break;
                            }
                            if (studentTempObj != null)
                            {
                                if (jscFlag != null)
                                    studentTempObj.IsJsc = Convert.ToBoolean(jscFlag);
                                if (sscFlag != null)
                                    studentTempObj.IsSsc = Convert.ToBoolean(sscFlag);
                                if (hscFlag != null)
                                    studentTempObj.IsHsc = Convert.ToBoolean(hscFlag);
                            }
                        }
                    }
                    if (savableAcademicInfos.Any())
                    {
                        foreach (var savableAcademicInfo in savableAcademicInfos)
                        {
                            Student studentTempObj = null;
                            savableAcademicInfo.Status = StudentAcademicInfo.EntityStatus.Active;
                            studentTempObj = studentListObject.FirstOrDefault(x => x.Id == savableAcademicInfo.Student.Id);
                            if (studentTempObj == null)
                            {
                                studentTempObj = savableAcademicInfo.Student;
                                studentTempObj.StudentAcademicInfos.Add(savableAcademicInfo);
                                studentListObject.Add(studentTempObj);
                            }

                            switch (savableAcademicInfo.StudentExam.Id)
                            {
                                case 2:
                                    jscFlag = true;
                                    break;
                                case 3:
                                    sscFlag = true;
                                    break;
                                case 4:
                                    hscFlag = true;
                                    break;
                            }
                            if (studentTempObj != null)
                            {
                                if (jscFlag != null)
                                    studentTempObj.IsJsc = Convert.ToBoolean(jscFlag);
                                if (sscFlag != null)
                                    studentTempObj.IsSsc = Convert.ToBoolean(sscFlag);
                                if (hscFlag != null)
                                    studentTempObj.IsHsc = Convert.ToBoolean(hscFlag);
                            }
                        }
                    }

                    foreach (var student in studentListObject)
                    {
                        _studentDao.Update(student);
                    }
                    _studentInfoHistoryService.Save(infoHistories);
                    trans.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;

            }
        }
        #endregion

        #region Save & Update
        public void Save(StudentAcademicInfo studentAcaInfo)
        {
            _studentAcademicInfoDao.Save(studentAcaInfo);
        }
        #endregion

        #region Single Instances Loading Function
        public StudentAcademicInfo LoadByStudentId(long studentId, long studentExmId)
        {
            try
            {
                return _studentAcademicInfoDao.LoadByStudentId(studentId, studentExmId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

        #region Clean
        //public StudentAcademicInfo LoadByStudentId(long id)
        //{
        //    return _studentAcademicInfoDao.LoadByStudentId(id);
        //}
        #endregion
    }
}
