﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.MediaDao;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.MediaServices
{
    public interface IEmployeeImageMediaService
    {

        EmployeeMediaImage GetEmployeeImageMediaByRefId(long refId);
    }
    public class EmployeeImageMediaService:IEmployeeImageMediaService
    {
        public readonly IEmployeeImageMediaDao _employeeImageMediaDao;
        public EmployeeImageMediaService()
        {
            var sessionMedia = NHibernateMediaSessionFactory.OpenSession();
            _employeeImageMediaDao = new EmployeeImageMediaDao(sessionMedia);
        }

        public EmployeeMediaImage GetEmployeeImageMediaByRefId(long refId)
        {
            return _employeeImageMediaDao.GetEmployeeImageMediaByRefId(refId);
        }
    }
}
