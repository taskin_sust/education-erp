﻿using System.Security.Cryptography.X509Certificates;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BankTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Bank")]
    public class BankCountTest : BankBaseTest
    {
        [Fact]
        public void Bank_Count_Greater_Than_Zero_Check()
        {
            _bankFactory.CreateMore(100).Persist();
            var bankCount = _bankService.BankRowCount("", "", "", Bank.EntityStatus.Active.ToString());
            Assert.True(bankCount > 0);
        }
        
    }
}
