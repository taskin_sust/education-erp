using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class SpecialAllowanceSettingFactory : ObjectFactoryBase<SpecialAllowanceSetting>
    {

        private readonly ISpecialAllowanceSettingService _specialAllowanceSettingService;

        public SpecialAllowanceSettingFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            _specialAllowanceSettingService = new SpecialAllowanceSettingService(session);

        }

        #region Create

        public SpecialAllowanceSettingFactory Create(int responsibleMemberPin)
        {

            Object = new SpecialAllowanceSetting()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = SpecialAllowanceSetting.EntityStatus.Active,
                ResponsibleMemberPin = responsibleMemberPin,
            };
            SingleObjectList.Add(Object);
            return this;
        }
        
        public SpecialAllowanceSettingFactory WithPaymentType(PaymentType paymentType)
        {
            Object.PaymentType = (int)paymentType;
            SingleObjectList.Add(Object);
            return this;
        }

        public SpecialAllowanceSettingFactory WithAmount(decimal amount)
        {
            Object.Amount =amount;
            SingleObjectList.Add(Object);
            return this;
        }

        public SpecialAllowanceSettingFactory WithEffectiveClosingDate(string effectiveDate, string closingDate="")
        {
            Object.EffectiveDateString = effectiveDate;
            Object.ClosingDateString = closingDate;

            DateTime eDate;
            DateTime.TryParse(effectiveDate, out eDate);
            Object.EffectiveDate = eDate;
            if (!string.IsNullOrEmpty(closingDate) && !string.IsNullOrWhiteSpace(closingDate))
            {
                DateTime cDate;
                DateTime.TryParse(closingDate, out cDate);
                var endOfMonth = new DateTime(cDate.Year, cDate.Month, DateTime.DaysInMonth(cDate.Year, cDate.Month));
                Object.ClosingDate = endOfMonth;
            }
            else
            {
                Object.ClosingDate = null;
            }
            SingleObjectList.Add(Object);
            return this;
        }

        public SpecialAllowanceSettingFactory WithRemarks(string remarks)
        {
            Object.Remarks = remarks;
            SingleObjectList.Add(Object);
            return this;
        }

        public SpecialAllowanceSettingFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public SpecialAllowanceSettingFactory Persist(List<UserMenu> userMenus,bool isUpdate=false)
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _specialAllowanceSettingService.SaveOrUpdate(userMenus, x, isUpdate); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_specialAllowanceSettingService.SaveOrUpdate(userMenus, x, isUpdate); });

            }
            return this;
        }

        public SpecialAllowanceSettingFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new SpecialAllowanceSetting() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public SpecialAllowanceSettingFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion

    }
}
