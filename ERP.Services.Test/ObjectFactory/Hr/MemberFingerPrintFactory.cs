﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class MemberFingerPrintFactory : ObjectFactoryBase<MemberFingerPrint>
    {
        private readonly IAttendanceDeviceTypeService _attendanceDeviceTypeService;
        private readonly IMemberFingerPrintService _memberFingerPrintService;
        public MemberFingerPrintFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _attendanceDeviceTypeService = new AttendanceDeviceTypeService(session);
            _memberFingerPrintService = new MemberFingerPrintService(session);

        }

        public MemberFingerPrintFactory Create()
        {
            Object = new MemberFingerPrint()
            {
                Index0 = "654D6546D6464S45sd54d646ert6ert46131gh313fgh654d314g3df4g5d3g1d3f4gd65gf43f1d3g4df3g1df3gh4d3fg1df3g4dg3d3g1d3g4d3g13dg4dg3dh3fg4h3fdgh1d3t4er3rtr3y4rt",
                Index1 = "654D6546D6464S45sd54d646ert6ert46131gh313fgh654d314g3df4g5d3g1d3f4gd65gf43f1d3g4df3g1df3gh4d3fg1df3g4dg3d3g1d3g4d3g13dg4dg3dh3fg4h3fdgh1d3t4er3rtr3y4rt",
                Index2 = "654D6546D6464S45sd54d646ert6ert46131gh313fgh654d314g3df4g5d3g1d3f4gd65gf43f1d3g4df3g1df3gh4d3fg1df3g4dg3d3g1d3g4d3g13dg4dg3dh3fg4h3fdgh1d3t4er3rtr3y4rt",
                Index3 = "654D6546D6464S45sd54d646ert6ert46131gh313fgh654d314g3df4g5d3g1d3f4gd65gf43f1d3g4df3g1df3gh4d3fg1df3g4dg3d3g1d3g4d3g13dg4dg3dh3fg4h3fdgh1d3t4er3rtr3y4rt",
                Index4 = "654D6546D6464S45sd54d646ert6ert46131gh313fgh654d314g3df4g5d3g1d3f4gd65gf43f1d3g4df3g1df3gh4d3fg1df3g4dg3d3g1d3g4d3g13dg4dg3dh3fg4h3fdgh1d3t4er3rtr3y4rt",
                Index5 = "654D6546D6464S45sd54d646ert6ert46131gh313fgh654d314g3df4g5d3g1d3f4gd65gf43f1d3g4df3g1df3gh4d3fg1df3g4dg3d3g1d3g4d3g13dg4dg3dh3fg4h3fdgh1d3t4er3rtr3y4rt",
                Index6 = "654D6546D6464S45sd54d646ert6ert46131gh313fgh654d314g3df4g5d3g1d3f4gd65gf43f1d3g4df3g1df3gh4d3fg1df3g4dg3d3g1d3g4d3g13dg4dg3dh3fg4h3fdgh1d3t4er3rtr3y4rt",
                Index7 = "654D6546D6464S45sd54d646ert6ert46131gh313fgh654d314g3df4g5d3g1d3f4gd65gf43f1d3g4df3g1df3gh4d3fg1df3g4dg3d3g1d3g4d3g13dg4dg3dh3fg4h3fdgh1d3t4er3rtr3y4rt",
                Index8 = "654D6546D6464S45sd54d646ert6ert46131gh313fgh654d314g3df4g5d3g1d3f4gd65gf43f1d3g4df3g1df3gh4d3fg1df3g4dg3d3g1d3g4d3g13dg4dg3dh3fg4h3fdgh1d3t4er3rtr3y4rt",
                Index9 = "654D6546D6464S45sd54d646ert6ert46131gh313fgh654d314g3df4g5d3g1d3f4gd65gf43f1d3g4df3g1df3gh4d3fg1df3g4dg3d3g1d3g4d3g13dg4dg3dh3fg4h3fdgh1d3t4er3rtr3y4rt",

            };
            SingleObjectList.Add(Object);
            return this;
        }

        //public MemberFingerPrintFactory Persist()
        //{
        //    SingleObjectList.ForEach(item =>
        //    {
        //        if (item.Id == 0)
        //        {
        //            _memberFingerPrintService.SaveOrUpdate(item);
        //        }
        //    });
        //    ObjectList.ForEach(item =>
        //    {
        //        if (item.Id == 0)
        //        {
        //            _attendanceDeviceTypeService.SaveOrUpdate(item);
        //        }
        //    });
        //    return this;
        //}

        public void CleanUp()
        {
            DeleteAll();
        }
    }
}
