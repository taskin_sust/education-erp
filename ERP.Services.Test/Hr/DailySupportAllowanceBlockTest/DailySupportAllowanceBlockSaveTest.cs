﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DailySupportAllowanceBlockTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "DailySupportAllowanceBlock")]
    public class DailySupportAllowanceBlockSaveTest : DailySupportAllowanceBlockBaseTest
    {
        #region Basic Test

        [Fact]
        public void AllowanceBlock_Should_Saved_Successfully_ByMentor()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";
            var teamMember = PersistTeamMember(MentorId);
            TeamMember mentor = teamMember.MentorHistory.Select(x => x.Mentor).LastOrDefault();

            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }
                , null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason)
                                        .WithTeamMember(teamMember);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });
            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu
                , new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, mentor, true);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>()
                .Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId)
                .List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;
            Assert.NotNull(entity);
        }

        [Fact]
        public void AllowanceBlock_Should_Saved_Successfully()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";

            var teamMember = PersistTeamMember(MentorId);
            TeamMember mentor = teamMember.MentorHistory.Select(x => x.Mentor).LastOrDefault();
            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }
                , null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMemberFactory.Object);
            attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            _dailyAllowanceBlockService.SaveOrUpdate(userMenu, new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, null);
            var entity = Session.QueryOver<DailySupportAllowanceBlock>().Where(x => x.BusinessId == _dailySupportAllowanceBlockFactory.Object.BusinessId)
                .List<DailySupportAllowanceBlock>().LastOrDefault();
            _dailySupportAllowanceBlockFactory.Object.Id = entity.Id;
            Assert.NotNull(entity);
        }

        #endregion

        #region Business Logic Test

        [Fact]
        public void InvalidDataException_For_Reason_Empty()
        {
            organizationFactory.Create().Persist();
            var userMenu = BuildUserMenu(new List<Organization>() { organizationFactory.Object });
            _dailySupportAllowanceBlockFactory.Create();
            _dailySupportAllowanceBlockFactory.Object.Reason = string.Empty;
            Assert.Throws<InvalidDataException>(() => _dailyAllowanceBlockService.SaveOrUpdate(userMenu
                , new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, null));
        }

        [Fact]
        public void InvalidDataException_For_TeamMember_Null()
        {
            organizationFactory.Create().Persist();
            var userMenu = BuildUserMenu(new List<Organization>() { organizationFactory.Object });
            _dailySupportAllowanceBlockFactory.Create();
            Assert.Throws<InvalidDataException>(() => _dailyAllowanceBlockService.SaveOrUpdate(userMenu
                , new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, null));
        }

        [Fact]
        public void InvalidDataException_For_UserMenu_Null()
        {
            organizationFactory.Create().Persist();
            _dailySupportAllowanceBlockFactory.Create();
            Assert.Throws<ArgumentNullException>(() => _dailyAllowanceBlockService.SaveOrUpdate(null
                , new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, null));
        }

        [Fact]
        public void InvalidDataException_For_UserMenu_Empty()
        {
            organizationFactory.Create().Persist();
            _dailySupportAllowanceBlockFactory.Create();
            Assert.Throws<ArgumentNullException>(() => _dailyAllowanceBlockService.SaveOrUpdate(new List<UserMenu>()
                , new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, null));
        }

        [Fact]
        public void DailySupportAllowanceBlock_Save_Failed_For_Invalid_Mentor()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";

            var teamMember = PersistTeamMember(MentorId);
            TeamMember mentor = teamMember.MentorHistory.Select(x => x.Mentor).LastOrDefault();
            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }
                , null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            Assert.Throws<InvalidDataException>(() => _dailyAllowanceBlockService.SaveOrUpdate(userMenu
                , new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, null, true));
        }

        [Fact]
        public void DailySupportAllowanceBlock_Save_Failed_For_Unauthorized_Mentor()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";

            var teamMember = PersistTeamMember(MentorId);
            TeamMember mentor = teamMember.MentorHistory.Select(x => x.Mentor).LastOrDefault();
            var employment = teamMember.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { employment.Campus.Branch.Organization }
                , null, new List<Branch>() { employment.Campus.Branch });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;
            using (ITransaction trans = Session.BeginTransaction())
            {
                for (int i = 0; i < teamMemberFactory.Object.MentorHistory.Count; i++)
                {
                    teamMemberFactory.Object.MentorHistory.RemoveAt(i);
                }
                trans.Commit();
            }
            Assert.Throws<InvalidDataException>(() => _dailyAllowanceBlockService.SaveOrUpdate(userMenu
                , new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, mentor, true));
        }

        [Fact]
        public void DailySupportAllowanceBlock_Save_Failed_For_Unauthorized_Branch()
        {
            DateTime date = DateTime.Now.AddDays(1);
            var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
            var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);
            const string reason = "Testing Cause";
            var teamMember = PersistTeamMember(MentorId);
            TeamMember mentor = teamMember.MentorHistory.Select(x => x.Mentor).LastOrDefault();
            branchFactory.Create().WithOrganization().Persist();
            var userMenu = BuildUserMenu(new List<Organization>() { branchFactory.Object.Organization },
                                            null, new List<Branch>() { branchFactory.Object });

            attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
            _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { attendanceAdjustmentFactory.Object });

            _dailySupportAllowanceBlockFactory.Create().WithTeamMember(teamMember);
            _dailySupportAllowanceBlockFactory.Object.BlockingDate = date;

            Assert.Throws<InvalidDataException>(() => _dailyAllowanceBlockService.SaveOrUpdate(userMenu
                , new List<DailySupportAllowanceBlock>() { _dailySupportAllowanceBlockFactory.Object }, mentor));

        }

        //TODO: Write more test
        //1. User who has not permission
        //2. User who has permission but not mentor
        //3. Hr users should perform only for branch users allownce block

        #endregion
    }
}
