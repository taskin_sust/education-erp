﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class LeaveApplicationFactory : ObjectFactoryBase<LeaveApplication>
    {
        private OrganizationFactory _organizationFactory;
        internal LeaveFactory _leaveFactory;
        private TeamMemberFactory _teamMemberFactory;
        private readonly IOrganizationService _organizationService;
        private readonly ILeaveService _leaveService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly ILeaveApplicationService _leaveApplicationService;
        private readonly TestBaseService<LeaveApplicationFactory> _leaveTestBaseService; 
        public LeaveApplicationFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _organizationFactory = new OrganizationFactory(this, session);
            _leaveFactory = new LeaveFactory(this, session);
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _organizationService = new OrganizationService(session);
            _leaveService = new LeaveService(session);
            _teamMemberService = new TeamMemberService(session);
            _leaveApplicationService = new LeaveApplicationService(session);
            _leaveTestBaseService = new TestBaseService<LeaveApplicationFactory>(session);
        }

        #region Create
        public LeaveApplicationFactory Create()
        {
            Object = new LeaveApplication()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                DateFrom = DateTime.Now,
                DateTo = DateTime.Now.AddMinutes(60),
                LeaveNote = "Testing Note",
                LeaveStatus = (int)LeaveStatus.Pending,
                Remarks = "NIL",
                Status = LeaveApplication.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public LeaveApplicationFactory CreateWith(long id,DateTime dateFrom, DateTime dateTo, string leaveNote, int leaveStatus, string remarks)
        {
            Object = new LeaveApplication()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                DateFrom = dateFrom,
                DateTo = dateTo,
                LeaveNote = leaveNote,
                LeaveStatus = leaveStatus,
                Remarks = remarks
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public LeaveApplicationFactory WithLeave()
        {
            _leaveFactory.Create().WithNameAndIsPublic().Persist();
            Object.Leave = _leaveFactory.Object;
            return this;
        }

        public LeaveApplicationFactory WithLeave(Leave leave)
        {
            Object.Leave = leave;
            return this;
        }

        public LeaveApplicationFactory WithTeamMember()
        {
            _teamMemberFactory.Create().Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }

        public LeaveApplicationFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public LeaveApplicationFactory WithResposibleTeammember()
        {
            _teamMemberFactory.Create().Persist();
            Object.ResponsiblePerson = _teamMemberFactory.Object;
            return this;
        }
        public LeaveApplicationFactory WithResposibleTeamMember(TeamMember teamMember)
        {
            Object.ResponsiblePerson = teamMember;
            return this;
        }
        public LeaveApplicationFactory Persist()
        {
            if (SingleObjectList.Count > 0)
            {
                _leaveApplicationService.SaveOrUpdate(SingleObjectList);
            }
            if (ObjectList.Count > 0)
            {
                _leaveApplicationService.SaveOrUpdate(ObjectList);
            }
            return this;
        }
        #endregion

        #region Others

        #endregion

        #region CleanUp
        public void LogCleanUp(List<LeaveApplication> leaveApplicationList)
        {
            _leaveTestBaseService.DeleteLeaveApplicationLogByLeaveApplicationIdList(leaveApplicationList);
        }
        public void DeleteLeaveApplicationDetailsByLeaveApplicationIdList(List<LeaveApplication> leaveApplicationList)
        {
            _leaveTestBaseService.DeleteLeaveApplicationDetailsByLeaveApplicationIdList(leaveApplicationList);
        }
        public LeaveApplicationFactory CleanUp()
        {
            if (SingleObjectList.Count > 0)
            {
                LogCleanUp(SingleObjectList);
                DeleteLeaveApplicationDetailsByLeaveApplicationIdList(SingleObjectList);
            }
            if (ObjectList.Count > 0)
            {
                LogCleanUp(ObjectList);
                DeleteLeaveApplicationDetailsByLeaveApplicationIdList(ObjectList);
            }
            DeleteAll();
            _leaveFactory.Cleanup();
            _teamMemberFactory.Cleanup();

            return this;
        }

        #endregion

    }
}
