﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DayOffAdjustmentTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "DayOffAdjustment")]
    public class DayOffAdjustmentLoadTest:DayOffAdjustmentBaseTest
    {

        #region LoadDayOffRecentAdjustmentForMentor
        
        [Fact]
        public void Should_Return_LoadDayOffRecentAdjustmentForMentor_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 12),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object });

            var mentor = tm.MentorHistory.Select(x => x.Mentor).FirstOrDefault();

            var list = _dayOffAdjustmentService.LoadDayOffRecentAdjustmentForMentor(mentor, 0, 10, "", "", tm.Pin.ToString(),
                new DateTime(2016, 12, 1), new DateTime(2016, 12, 31));

            Assert.NotEqual(0, list.Count());
        }

        [Fact]
        public void Should_Return_LoadDayOffRecentAdjustmentForMentor_InvalidException()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 12),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object });


            Assert.Throws<InvalidDataException>(() => _dayOffAdjustmentService.LoadDayOffRecentAdjustmentForMentor(null, 0, 10,
                "", "", tm.Pin.ToString(), new DateTime(2016, 12, 1), new DateTime(2016, 12, 31)));
        }
        
        #endregion

        #region LoadAllHrDayOffAdjustmentHistory

        [Fact]
        public void Should_Return_LoadAllHrDayOffAdjustmentHistory_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 12),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object });

            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var userMenu = BuildUserMenu(org, (Program) null, branch);
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();

            var list = _dayOffAdjustmentService.LoadAllHrDayOffAdjustmentHistory(userMenu,0,10,"","",tm.Pin.ToString(),
                new DateTime(2016, 12, 1), new DateTime(2016, 12, 31), org.Id, branch.Id, campus.Id, department.Id);

            Assert.NotEqual(0, list.Count());
        }

        [Fact]
        public void Should_Return_LoadAllHrDayOffAdjustmentHistory_InvalidException()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 12),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object });

            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();

            Assert.Throws<InvalidDataException>(() => _dayOffAdjustmentService.LoadAllHrDayOffAdjustmentHistory(null, 0, 10, "", "", tm.Pin.ToString(),
                new DateTime(2016, 12, 1), new DateTime(2016, 12, 31), org.Id, branch.Id, campus.Id, department.Id));
        }
        
        #endregion

        #region CountDayOffRecentAdjustmentForMentor

        [Fact]
        public void Should_Return_CountDayOffRecentAdjustmentForMentor_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 12),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object });
            
            var listCount = _dayOffAdjustmentService.CountDayOffRecentAdjustmentForMentor(tm.MentorHistory.Select(x => x.Mentor).FirstOrDefault(),
                tm.Pin.ToString(), new DateTime(2016, 12, 1), new DateTime(2016, 12, 31));

            Assert.NotEqual(0, listCount);
        }

        [Fact]
        public void Should_Return_CountDayOffRecentAdjustmentForMentor_InvalidException()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 12),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object });

            Assert.Throws<InvalidDataException>(() => _dayOffAdjustmentService.CountDayOffRecentAdjustmentForMentor(null, tm.Pin.ToString(), 
                new DateTime(2016, 12, 1), new DateTime(2016, 12, 31)));
        }

        #endregion

        #region CountHrDayOffAdjustmentHistory

        [Fact]
        public void Should_Return_CountHrDayOffAdjustmentHistory_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);

            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var userMenu = BuildUserMenu(org, (Program)null, branch);
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();

            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 12),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object });

            var listCount = _dayOffAdjustmentService.CountHrDayOffAdjustmentHistory(userMenu, tm.Pin.ToString(), new DateTime(2016, 12, 12),
                new DateTime(2016, 12, 12), org.Id, branch.Id, campus.Id, department.Id);

            Assert.NotEqual(0, listCount);
        }

        [Fact]
        public void Should_Return_CountHrDayOffAdjustmentHistory_InvalidException_For_Usemenu()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);

            var branch = tm.EmploymentHistory.Select(x => x.Campus.Branch).FirstOrDefault();
            var userMenu = BuildUserMenu(org, (Program)null, branch);
            var campus = tm.EmploymentHistory.Select(x => x.Campus).FirstOrDefault();
            var department = tm.EmploymentHistory.Select(x => x.Department).FirstOrDefault();

            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 12),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object });
            Assert.Throws<InvalidDataException>(
                () =>
                    _dayOffAdjustmentService.CountHrDayOffAdjustmentHistory(null, tm.Pin.ToString(),
                        new DateTime(2016, 12, 12),
                        new DateTime(2016, 12, 12), org.Id, branch.Id, campus.Id, department.Id));
        }

        #endregion

        #region GetTeamMemberAttendanceAdjustmentPostDateTime

        [Fact]
        public void Should_Return_GetTeamMemberAttendanceAdjustmentPostDateTime_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);

            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 12),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved,1,0,true).WithTeamMember(tm);

            _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object });

            Assert.NotEqual(null, _dayOffAdjustmentService.GetTeamMemberAttendanceAdjustmentPostDateTime(tm.Id));
        }

        [Fact]
        public void Should_Return_GetTeamMemberAttendanceAdjustmentPostDateTime_InvalidException_For_TeamMember()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var org = _organizationService.LoadById(3);
            var tm = PersistTeamMember(joiningDate, dateTo, org);
            attendanceAdjustmentFactory.CreateMore(new DateTime(2016, 12, 1), tm);
            _attendanceAdjustmentService.SaveOrUpdate(attendanceAdjustmentFactory.ObjectList);

            _dayOffAdjustmentFactory.CreateWith(new DateTime(2016, 12, 12), new DateTime(2016, 12, 12),
                "Test Purpose", (int)DayOffAdjustmentStatus.Approved).WithTeamMember(tm);

            _dayOffAdjustmentService.Save(new List<DayOffAdjustment>() { _dayOffAdjustmentFactory.Object });

            Assert.Equal(null, _dayOffAdjustmentService.GetTeamMemberAttendanceAdjustmentPostDateTime(null));
        }

        #endregion

    }
}
