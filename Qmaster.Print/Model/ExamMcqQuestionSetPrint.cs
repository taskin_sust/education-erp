﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qmaster.Print.Model
{

    public class ExamQuestionSetPrintApiDto
    {
        public virtual string QuestionSetZipPath { get; set; }
        public virtual IList<ExamMcqQuestionSetPrint> ExamQuestionSetPrintApiObjects { get; set; }
    }

    public class ExamMcqQuestionSetPrint
    {
        public virtual long Id { get; set; }
        public virtual long ExamId { get; set; }
        public virtual int UniqueSet { get; set; }
        public virtual int SetId { get; set; }
        public virtual string SetNo { get; set; }
        public virtual int PrintStatus { get; set; }
        public virtual string CodeNo { get; set; }
        public virtual string EanCheckSumCodeNo { get; set; }

        /// <summary>
        /// for thread
        /// </summary>
        public virtual string Message { get; set; }
        public virtual int Progress { get; set; }
        public virtual string ProgressString { get; set; }
        public virtual  bool IsAllUploaded { get; set; }

    }
}
