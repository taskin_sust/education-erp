using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UserAuth;
using UdvashERP.MessageExceptions;

namespace UdvashERP.Services.Hr
{
    public interface IEmployeeBenefitsFundSettingService : IBaseService
    {
        #region Operational Function

        void Save(EmployeeBenefitsFundSetting employeeBenefitsFund, List<UserMenu> userMenus);

        void Update(List<UserMenu> userMenus, long id, EmployeeBenefitsFundSetting employeeBenefitsFundSetting);

        bool Delete(List<UserMenu> userMenus, long id);

        #endregion

        #region Single Instances Loading Function

        EmployeeBenefitsFundSetting GetEmployeeBenefitFundSetting(long employeeBenefitFundSettingId);

        #endregion

        #region List Loading Function

        IList<EmployeeBenefitsFundSetting> LoadEmployeeBenefitFundSetting(List<UserMenu> userMenus, int start, int length, string orderBy, string orderDir, long? organizationId, long? employeeStatus, long? calculationOn);
        IList<EmployeeBenefitFundBalanceView> LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(List<UserMenu> userMenu, long organizationId, List<long> branchIds, IList<long> campusIds, IList<long> departmentIds, IList<long> designationIds, int employmentStatus, DateTime effectiveDate);

        #endregion

        #region Others Function


        int LoadEmployeeBenefitFundSettingCount(List<UserMenu> userMenus, string orderBy, string orderDir, long? organizationId, long? employeeStatus, long? calculationOn);

        string GetUserNameByAspNetUserId(long id);

        #endregion

        #region Helper Function

        #endregion

    }

    public class EmployeeBenefitsFundSettingService : BaseService, IEmployeeBenefitsFundSettingService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IEmployeeBenefitsFundSettingDao _employeeBenefitsFundDao;
        private IEmployeeBenefitsFundSettingEntitlementDao _employeeBenefitsFundSettingEntitlementDao;
        private IOrganizationDao _organizationDao;
        ISalarySheetDao _salarySheetDao;
        private IAspNetUserDao _aspNetUserDao;
        ICampusDao _campusDao;
        IProgramDao _programDao;
        IDepartmentDao _departmentDao;
        IDesignationDao _designationDao;
        public EmployeeBenefitsFundSettingService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _employeeBenefitsFundDao = new EmployeeBenefitsFundSettingDao { Session = session };
            _employeeBenefitsFundSettingEntitlementDao = new EmployeeBenefitsFundSettingEntitlementDao { Session = session };
            _organizationDao = new OrganizationDao { Session = session };
            _salarySheetDao = new SalarySheetDao { Session = session };
            _aspNetUserDao = new AspNetUserDao() { Session = session };
            _campusDao = new CampusDao() { Session = session };
            _programDao = new ProgramDao() { Session = session };
            _departmentDao = new DepartmentDao() { Session = session };
            _designationDao = new DesignationDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public void Save(EmployeeBenefitsFundSetting employeeBenefitsFund, List<UserMenu> userMenus)
        {
            ITransaction trans = null;
            try
            {
                DoBeforeSaveOrUpdate(employeeBenefitsFund, false);

                using (trans = Session.BeginTransaction())
                {
                    //authorization checking
                    var authorizedOrganization = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(employeeBenefitsFund.OrganizationId));
                    if (authorizedOrganization.Count > 0)
                    {
                        var organization = _organizationDao.LoadById(authorizedOrganization[0]);
                        employeeBenefitsFund.Organization = organization;
                        foreach (var empBenefitFundSetting in employeeBenefitsFund.EmployeeBenefitsFundSettingEntitlement)
                        {
                            empBenefitFundSetting.CreateBy = GetCurrentUserId();
                            empBenefitFundSetting.ModifyBy = GetCurrentUserId();
                            empBenefitFundSetting.EmployeeBenefitsFundSetting = employeeBenefitsFund;
                        }
                        _employeeBenefitsFundDao.Save(employeeBenefitsFund);
                        trans.Commit();
                    }
                    else
                    {
                        throw new InvalidDataException("Permission Denied");
                    }

                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (trans != null && trans.IsActive)
                {
                    trans.Rollback();
                }
                throw;
            }

        }

        public void Update(List<UserMenu> userMenus, long id, EmployeeBenefitsFundSetting employeeBenefitsFundSetting)
        {

            ITransaction trans = null;
            try
            {
                DoBeforeSaveOrUpdate(employeeBenefitsFundSetting, true);

                using (trans = Session.BeginTransaction())
                {
                    //authorization checking
                    var authorizedOrganization = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(employeeBenefitsFundSetting.Organization.Id));
                    if (authorizedOrganization.Count > 0)
                    {
                        var organization = _organizationDao.LoadById(authorizedOrganization[0]);
                        //employeeBenefitsFundSetting.Organization = organization;

                        EmployeeBenefitsFundSetting existingBenefitsFundSetting = _employeeBenefitsFundDao.LoadById(id);
                        if (existingBenefitsFundSetting != null)
                        {
                            existingBenefitsFundSetting.Organization = organization;
                            existingBenefitsFundSetting.Name = employeeBenefitsFundSetting.Name;
                            existingBenefitsFundSetting.EmploymentStatus = employeeBenefitsFundSetting.EmploymentStatus;
                            existingBenefitsFundSetting.CalculationOn = employeeBenefitsFundSetting.CalculationOn;
                            existingBenefitsFundSetting.EmployeeContribution = employeeBenefitsFundSetting.EmployeeContribution;
                            existingBenefitsFundSetting.EmployerContribution = employeeBenefitsFundSetting.EmployerContribution;
                            existingBenefitsFundSetting.EffectiveDate = employeeBenefitsFundSetting.EffectiveDate;
                            existingBenefitsFundSetting.ClosingDate = employeeBenefitsFundSetting.ClosingDate;


                            //var x = from e in employeeBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement
                            //        where !existingBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement.Contains(e)
                            //        select e;

                            //var y = from e in existingBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement
                            //        where !employeeBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement.Contains(e)
                            //        select e;

                            // if(existingBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement.Excep(employeeBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement))
                            #region new entitlement

                            foreach (var newFundSettingEntitlement in employeeBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement)
                            {
                                if (newFundSettingEntitlement.Id == 0)
                                {
                                    newFundSettingEntitlement.CreateBy = GetCurrentUserId();
                                    newFundSettingEntitlement.ModifyBy = GetCurrentUserId();
                                    existingBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement.Add(newFundSettingEntitlement);
                                    newFundSettingEntitlement.EmployeeBenefitsFundSetting = existingBenefitsFundSetting;
                                    _employeeBenefitsFundDao.Save(existingBenefitsFundSetting);
                                }

                            }
                            #endregion

                            #region Deleted Benefit Entitle

                            //var deletedIds = existingBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement.Where(l1 => !employeeBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement.Any(l2 => l1.Id == l2.Id));

                            var deletedIds = existingBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement.Where(l1 => employeeBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement.All(l2 => l1.Id != l2.Id));

                            foreach (var entitlement in deletedIds)
                            {
                                var item = _employeeBenefitsFundSettingEntitlementDao.LoadById(entitlement.Id);
                                item.ModifyBy = GetCurrentUserId();
                                item.Status = EmployeeBenefitsFundSetting.EntityStatus.Delete;
                                _employeeBenefitsFundSettingEntitlementDao.Update(item);
                            }

                            #endregion

                            #region operation for update entitlement

                            var updateList = from e in employeeBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement
                                             where !existingBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement.Contains(e)
                                             select e;

                            foreach (var x in updateList)
                            {
                                var item = _employeeBenefitsFundSettingEntitlementDao.LoadById(x.Id);
                                item.ModifyBy = GetCurrentUserId();
                                item.ServicePeriod = x.ServicePeriod;
                                item.EmployerContribution = x.EmployerContribution;
                                _employeeBenefitsFundSettingEntitlementDao.Update(item);

                            }


                            //foreach (var existEmpBenefitFundSettingEntitlement in existingBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement)
                            //{
                            //    foreach (var newFundSettingEntitlement in employeeBenefitsFundSetting.EmployeeBenefitsFundSettingEntitlement)
                            //    {
                            //        if (existEmpBenefitFundSettingEntitlement.Id == newFundSettingEntitlement.Id)
                            //        {
                            //            existEmpBenefitFundSettingEntitlement.ServicePeriod = newFundSettingEntitlement.ServicePeriod;
                            //            existEmpBenefitFundSettingEntitlement.EmployerContribution = newFundSettingEntitlement.EmployerContribution;
                            //            existEmpBenefitFundSettingEntitlement.ModifyBy = GetCurrentUserId();
                            //            existEmpBenefitFundSettingEntitlement.EmployeeBenefitsFundSetting = existingBenefitsFundSetting;
                            //        }

                            //    }

                            //}
                            //_employeeBenefitsFundDao.Update(existingBenefitsFundSetting);
                            #endregion

                            trans.Commit();
                        }

                    }
                    else
                    {
                        throw new MessageException("Permission Denied");
                    }

                }
            }
            catch (NullObjectException ex)
            {
                throw ex;
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (MessageException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        public bool Delete(List<UserMenu> userMenus, long id)
        {
            ITransaction trans = null;
            try
            {
                EmployeeBenefitsFundSetting employeeBenefitsFund = new EmployeeBenefitsFundSetting();
                employeeBenefitsFund = _employeeBenefitsFundDao.LoadById(id);
                DoBeforeDelete(employeeBenefitsFund);

                using (trans = Session.BeginTransaction())
                {
                    //authorization checking
                    var authorizedOrganization = AuthHelper.LoadOrganizationIdList(userMenus, _commonHelper.ConvertIdToList(employeeBenefitsFund.Organization.Id));
                    if (authorizedOrganization.Count > 0)
                    {
                        var organization = _organizationDao.LoadById(authorizedOrganization[0]);
                        employeeBenefitsFund.Organization = organization;
                        employeeBenefitsFund.Status = EmployeeBenefitsFundSetting.EntityStatus.Delete;
                        foreach (var empBenefitFundSetting in employeeBenefitsFund.EmployeeBenefitsFundSettingEntitlement)
                        {
                            //empBenefitFundSetting.CreateBy = GetCurrentUserId();
                            empBenefitFundSetting.ModifyBy = GetCurrentUserId();
                            empBenefitFundSetting.Status = EmployeeBenefitsFundSettingEntitlement.EntityStatus.Delete;
                            empBenefitFundSetting.EmployeeBenefitsFundSetting = employeeBenefitsFund;
                        }
                        _employeeBenefitsFundDao.Update(employeeBenefitsFund);
                        trans.Commit();
                        return true;
                    }
                    else
                    {
                        throw new InvalidDataException("Permission Denied");
                    }

                }
            }
            catch (MessageException ex)
            {
                throw ex;
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (trans != null && trans.IsActive)
                {
                    trans.Rollback();
                }
                throw;
            }
            return false;
        }

        private void DoBeforeDelete(EmployeeBenefitsFundSetting employeeBenefitsFund)
        {
            //var currentDate = DateTime.Now.AddHours(-DateTime.Now.Hour).AddMinutes(-DateTime.Now.Minute).AddSeconds(-DateTime.Now.Second);
            ////var currentDateTime = currentDate.AddHours(23).AddMinutes(59).AddSeconds(59);
            //if (employeeBenefitsFund.EffectiveDate <= currentDate)
            //    throw new DependencyException("You can't delete this employee benefit fund settings for effective date");
            if (_employeeBenefitsFundDao.HasSalarySheet(employeeBenefitsFund))
            {
                throw new MessageException("You can't delete this Employee benefit fund setting");
            }
        }

        private void DoBeforeSaveOrUpdate(EmployeeBenefitsFundSetting employeeBenefitsFund, bool isUpdate)
        {
            ValidationChecking(employeeBenefitsFund);

            //duplicate name checking
            string name;
            if (_employeeBenefitsFundDao.HasDuplicate(employeeBenefitsFund, out name, isUpdate))
                throw new MessageException("Duplicate " + name + " found");

            if (_employeeBenefitsFundDao.HasDuplicateCombination(employeeBenefitsFund, isUpdate))
            {
                throw new MessageException("Already exist employee benefit fund setting");
            }

            if (isUpdate == false)
            {
                var salarySheet = _salarySheetDao.GetOrganizationLastSalarySheet(employeeBenefitsFund.OrganizationId);
                if (salarySheet != null)
                {
                    if (employeeBenefitsFund.EffectiveDate > salarySheet.EndDate)
                        throw new MessageException("You can't create EBF in this Effective Date");
                }
            }
            else
            {
                var salarySheet = _salarySheetDao.GetOrganizationLastSalarySheet(employeeBenefitsFund.Organization.Id);
                if (salarySheet != null)
                {
                    //checking
                    var tmpDat = _employeeBenefitsFundDao.LoadById(employeeBenefitsFund.Id);

                    if (salarySheet.EndDate <= employeeBenefitsFund.EffectiveDate)
                    {
                        if (employeeBenefitsFund.OrganizationId != tmpDat.Organization.Id)
                        {
                            throw new MessageException("You can't update this EBF settings");
                        }

                        if (employeeBenefitsFund.EmploymentStatus != tmpDat.EmploymentStatus)
                        {
                            throw new MessageException("You can't update this EBF settings");
                        }

                        if (employeeBenefitsFund.CalculationOn != tmpDat.CalculationOn)
                        {
                            throw new MessageException("You can't update this EBF settings");
                        }

                        if (employeeBenefitsFund.EffectiveDate != tmpDat.EffectiveDate)
                        {
                            throw new MessageException("You can't update this EBF settings");
                        }

                        if (salarySheet.EndDate <= employeeBenefitsFund.EffectiveDate)
                        {
                            throw new MessageException("You can't change the effective date in this date range");
                        }
                    }

                    if(employeeBenefitsFund.ClosingDate!=null)
                    {
                        if (salarySheet.EndDate >= employeeBenefitsFund.ClosingDate)
                            throw new MessageException("You can't create EBF in this Date Range");
                    }

                    //if (employeeBenefitsFund.EffectiveDate < salarySheet.EndDate && employeeBenefitsFund.ClosingDate > salarySheet.EndDate)
                    //    throw new MessageException("You can't create EBF in this Date Range");
                }

            }
        }

        //validation check ussign data annotation
        private void ValidationChecking(EmployeeBenefitsFundSetting employeeBenefitsFund)
        {
            var validationResult = ValidationHelper.ValidateEntity<EmployeeBenefitsFundSetting, EmployeeBenefitsFundSetting>(employeeBenefitsFund);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        #endregion

        #region Single Instances Loading Function


        public EmployeeBenefitsFundSetting GetEmployeeBenefitFundSetting(long employeeBenefitFundSettingId)
        {
            try
            {
                return _employeeBenefitsFundDao.LoadById(employeeBenefitFundSettingId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<EmployeeBenefitsFundSetting> LoadEmployeeBenefitFundSetting(List<UserMenu> userMenus, int start, int length, string orderBy, string orderDir, long? organizationId, long? employeeStatus, long? calculationOn)
        {
            IList<EmployeeBenefitsFundSetting> employeeBenefitsFundSettings;
            try
            {
                List<long> authorizedOrganization = AuthHelper.LoadOrganizationIdList(userMenus, (organizationId == null) ? null : _commonHelper.ConvertIdToList((long)organizationId));

                if (authorizedOrganization == null || !EnumerableExtensions.Any(authorizedOrganization))
                    throw new InvalidDataException("Invalid Organization");

                employeeBenefitsFundSettings = _employeeBenefitsFundDao.LoadEmployeeBenefitFundSetting(start, length, orderBy, orderDir, authorizedOrganization, employeeStatus, calculationOn);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return employeeBenefitsFundSettings;
        }


        public IList<EmployeeBenefitFundBalanceView> LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(List<UserMenu> userMenu, long organizationId, List<long> branchIds, IList<long> campusIds, IList<long> departmentIds, IList<long> designationIds, int employmentStatus, DateTime effectiveDate)
        {
            try
            {
                List<long> authOrganizationIds = AuthHelper.LoadOrganizationIdList(userMenu, _commonHelper.ConvertIdToList(organizationId));
                List<long> authBranchIds = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIds, null, branchIds);

                //List<long> organizationIdList = null;
                //List<long> branchIdList = null;
                //if (!String.IsNullOrEmpty(organization))
                //    organizationIdList = _commonHelper.ConvertIdToList(Convert.ToInt64(organization));
                //if (!String.IsNullOrEmpty(branch))
                //    branchIdList = _commonHelper.ConvertIdToList(Convert.ToInt64(branch));

                //List<long> authorizedOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, organizationIdList);
                //List<long> authorizedBranchIdList = AuthHelper.LoadBranchIdList(userMenu, authorizedOrganizationIdList, null, branchIdList);
                if (campusIds == null)
                {
                    List<long> authProgram = AuthHelper.LoadProgramIdList(userMenu, authOrganizationIds, authBranchIds);
                    campusIds = _campusDao.LoadAuthorizeCampus(authProgram, authBranchIds, null, true).Select(x => x.Id).ToList<long>();
                }

                if (departmentIds == null)
                {
                    departmentIds = _departmentDao.LoadHrDepartmentList(authOrganizationIds).Select(x => x.Id).ToList<long>();
                }

                if (designationIds == null)
                {
                    designationIds = _designationDao.LoadHrDesignationList(authOrganizationIds).Select(x => x.Id).ToList<long>();
                }


                return _employeeBenefitsFundDao.LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(authOrganizationIds, authBranchIds, campusIds, departmentIds, designationIds, employmentStatus, effectiveDate);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }



        #endregion

        #region Others Function


        public int LoadEmployeeBenefitFundSettingCount(List<UserMenu> userMenus, string orderBy, string orderDir, long? organizationId, long? employeeStatus, long? calculationOn)
        {
            int returnValue = 0;
            try
            {
                List<long> authorizedOrganization = AuthHelper.LoadOrganizationIdList(userMenus, (organizationId == null) ? null : _commonHelper.ConvertIdToList((long)organizationId));

                if (authorizedOrganization == null || !EnumerableExtensions.Any(authorizedOrganization))
                    throw new InvalidDataException("Invalid Organization");

                ICriteria criteria = _employeeBenefitsFundDao.LoadEmployeeBenefitFundSettingCount(orderBy, orderDir, authorizedOrganization, employeeStatus, calculationOn);
                criteria.SetProjection(Projections.RowCount());
                returnValue = Convert.ToInt32(criteria.UniqueResult());
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return returnValue;
        }


        public string GetUserNameByAspNetUserId(long id)
        {
            try
            {
                var aspNetUser = _aspNetUserDao.LoadById(id);
                return aspNetUser != null ? aspNetUser.UserName : "";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function



        #endregion
    }
}

