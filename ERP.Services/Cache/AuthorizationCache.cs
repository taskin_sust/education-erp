﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;
using UdvashERP.BusinessModel.Dto;

namespace UdvashERP.Services.Cache
{
    public static class AuthorizationCache
    {
        #region User Data

        private const string GlobalAuthKey = "GlobalAuthTime";

        public static UserProfile GetCurrentUserProfile()
        {
            const string userProfileKey = "USER_PROFILE";
            UserProfile profile = null;

            //userId from context
            string userIdText = HttpContext.Current.User.Identity.GetUserId();
            long aspnetUserId = -1;
            if (long.TryParse(userIdText, out aspnetUserId) == false || aspnetUserId <= 0)
                return null;

            //try load from session
            if (HttpContext.Current.Session[userProfileKey] != null)
            {
                profile = HttpContext.Current.Session[userProfileKey] as UserProfile;
            }

            //load from db & save to session
            if (profile == null)
            {
                using (var session = NHibernateSessionFactory.OpenSession())
                {
                    var userService = new UserService(session);
                    profile = userService.GetByAspNetUser(aspnetUserId);
                    HttpContext.Current.Session[userProfileKey] = profile;
                }
            }
            return profile;
        }

        public static List<UserMenu> LoadUserMenuByUserProfile(UserProfile userProfile, AuthInfo authInfo = null)
        {
            const string key = "UserMenuList";
            const string loadTimeKey = "UserMenuLoadTime";
            List<UserMenu> userMenuList = null;

            //check reload from db needed 
            long loadFromDbTime = HttpContext.Current.Session[loadTimeKey] != null
                ? (long)HttpContext.Current.Session[loadTimeKey]
                : 0;
            if(authInfo == null)
                authInfo = GetUserAuthInfo(userProfile.AspNetUser);
            bool loadFromDb = (loadFromDbTime <= authInfo.PermissionChangeTime);

            //try load from session
            if (loadFromDb == false && HttpContext.Current.Session[key] != null)
            {
                userMenuList = HttpContext.Current.Session[key] as List<UserMenu>;
            }

            //load from db & save to session
            if (userMenuList == null)
            {
                //load all menu & create a virtual UserMenuList for SuperAdmin
                if (userProfile.AspNetUser.UserName == ApplicationUsers.SuperAdmin)
                {
                    userMenuList = new List<UserMenu>();
                    var menuList = LoadMenuList();
                    foreach (var menu in menuList)
                    {
                        userMenuList.Add(new UserMenu(userProfile, menu));
                    }
                }
                else
                {
                    //load UserMenu from DB
                    using (var session = NHibernateSessionFactory.OpenSession())
                    {
                        var userMenuService = new UserMenuService(session);
                        userMenuList = userMenuService.LoadByProfile(userProfile) ?? new List<UserMenu>();
                    }

                    //set Menu
                    var vUserMenuList = new List<UserMenu>();
                    foreach (var userMenu in userMenuList)
                    {
                        var menu = GetMenu(userMenu.Menu.Id);
                        userMenu.Menu = menu;

                        //create virtual userMenu for parent
                        if (menu != null && menu.MenuSelfMenu != null)
                        {
                            var found = vUserMenuList.Count(x => x.Menu != null && x.Menu.Id == menu.MenuSelfMenu.Id) > 0;
                            if (found == false)
                            {
                                var parentMenu = GetMenu(menu.MenuSelfMenu.Id);
                                var pum = new UserMenu() {Menu = parentMenu};
                                vUserMenuList.Add(pum);
                            }
                        }
                    }
                    userMenuList.AddRange(vUserMenuList);

                    //remove null menu
                    userMenuList = userMenuList.Where(x => x.Menu != null).ToList();
                }

                //set menu list to session
                HttpContext.Current.Session[key] = userMenuList;
                HttpContext.Current.Session[loadTimeKey] = DateTime.Now.Ticks;
            }

            return userMenuList;
        }

        public static List<AuthInfo> LoadAuthInfoList()
        {
            return HttpRuntime.Cache.Get(GlobalAuthKey) as List<AuthInfo> ?? new List<AuthInfo>();
        }

        public static void SetUserPermissionChangeTime(AspNetUser aspnetUser, bool allUser = false)
        {
            long ticks = DateTime.Now.Ticks;
            if (allUser)
            {
                var authList = LoadAuthInfoList();
                authList.ForEach(x => x.PermissionChangeTime = ticks);
            }
            else
            {
                GetUserAuthInfo(aspnetUser).PermissionChangeTime = ticks;
            }
        }

        public static void SetUserForceLogoff(AspNetUser aspnetUser, bool forceLogoff, bool allUser = false)
        {
            if (allUser)
            {
                var authList = LoadAuthInfoList();
                authList.ForEach(x => x.ForceLogoff = forceLogoff);
            }
            else
            {
                GetUserAuthInfo(aspnetUser).ForceLogoff = forceLogoff;
            }
        }

        public static void SetUserPermissionDenied(AspNetUser aspnetUser, bool permissionDenied, bool allUser)
        {
            if (allUser)
            {
                var authList = LoadAuthInfoList();
                authList.ForEach(x => x.PermissionDenied = permissionDenied);
            }
            else
            {
                GetUserAuthInfo(aspnetUser).PermissionDenied = permissionDenied;
            }
        }

        public static void SetUserAuthInfo(AuthInfo authInfo)
        {
            if (authInfo == null || authInfo.UserId <= 0)
                return;

            //found authInfo
            var authList = LoadAuthInfoList();
            var authInfoFound = (from auth in authList
                                 where auth.UserId == authInfo.UserId
                                 select auth).FirstOrDefault();

            //set authInfo
            if (authInfoFound == null)
            {
                authList.Add(authInfo);
            }
            else
            {
                authInfoFound.PermissionChangeTime = authInfo.PermissionChangeTime;
                authInfoFound.ForceLogoff = authInfo.ForceLogoff;
                authInfoFound.PermissionDenied = authInfo.PermissionDenied;
            }

            //set value to cache
            HttpRuntime.Cache.Insert(GlobalAuthKey, authList);
        }

        public static AuthInfo GetUserAuthInfo(AspNetUser aspnetUser)
        {
            var defaultAuthInfo = new AuthInfo(0, new DateTime(2010, 01, 01).Ticks);
            if (aspnetUser == null || aspnetUser.Id <= 0)
                return defaultAuthInfo;

            //get from global cache
            var authList = LoadAuthInfoList();
            var authInfoFound = (from auth in authList
                        where auth.UserId == aspnetUser.Id
                        select auth).FirstOrDefault();
            if (authInfoFound == null)
            {
                authInfoFound = new AuthInfo(aspnetUser.Id, DateTime.Now.Ticks, false, false);
                SetUserAuthInfo(authInfoFound);
            }

            return authInfoFound;
        }
        #endregion


        #region Controller-Action

        private static List<Actions> LoadActionList(bool forceFormDb = false)
        {
            const string key = "GlobalActionList";
            List<Actions> actionList = null;


            //try load from cache
            if (forceFormDb == false && HttpRuntime.Cache[key] != null)
            {
                actionList = HttpRuntime.Cache.Get(key) as List<Actions>;
            }

            //load from db & save to cache
            if (actionList == null)
            {
                using (var session = NHibernateSessionFactory.OpenSession())
                {
                    var actionService = new ActionsService(session);
                    actionList = actionService.LoadActive(true).ToList();

                    //set menu list to session
                    HttpRuntime.Cache.Insert(key, actionList);
                }
            }
            return actionList;
        }

        private static Actions GetAction(string actionName, string controllerName)
        {
            var actionList = LoadActionList();
            var query = from a in actionList
                where
                    a.Name == actionName
                    && a.AreaControllers.Name == controllerName
                select a;

            var actionObj = query.FirstOrDefault();
            return actionObj;
        }

        private static Actions GetAction(long actionId)
        {
            var actionList = LoadActionList();
            var query = from a in actionList
                where
                    a.Id == actionId
                select a;

            var actionObj = query.FirstOrDefault();
            return actionObj;
        }

        public static long GetActionId(string actionName, string controllerName)
        {
            var actionList = LoadActionList();
            var query = from a in actionList
                where
                    a.Name == actionName
                    && a.AreaControllers.Name == controllerName
                select a.Id;

            long actionId = query.FirstOrDefault();
            return actionId;
        }

        #endregion Controller-Action


        #region Menu-MenuAccess

        private static List<Menu> LoadMenuList(bool forceFormDb = false)
        {
            const string key = "GlobalMenuList";
            List<Menu> menuList = null;


            //try load from cache
            if (forceFormDb == false && HttpRuntime.Cache[key] != null)
            {
                menuList = HttpRuntime.Cache.Get(key) as List<Menu>;
            }

            //load from db & save to cache
            if (menuList == null)
            {
                using (var session = NHibernateSessionFactory.OpenSession())
                {
                    var menuService = new MenuService(session);
                    menuList = menuService.LoadActive(true).ToList();

                }

                //create link
                foreach (var menu in menuList)
                {
                    if (menu.DefaultAction != null)
                    {
                        var action = GetAction(menu.DefaultAction.Id);
                        if (action != null)
                        {
                            menu.Link = "";
                            if (action.AreaControllers.Area != "NoArea")
                                menu.Link += action.AreaControllers.Area + "/";

                            menu.Link += action.AreaControllers.Name + "/" + action.Name;
                            menu.BusinessId = action.AreaControllers.Name + "-" + action.Name+"|";
                        }
                        // FOR SOLVING MENU SELECTION PROBLEM
                        foreach (var i in menu.MenuAccess.Where(ma=>ma.HasReferrer==false).ToList())
                        {
                            var refererAction = GetAction(i.Actions.Id);
                            if (refererAction != null)
                            {
                                if (menu.BusinessId.Contains(refererAction.AreaControllers.Name + "-" + refererAction.Name + "|") == false)
                                {
                                    menu.BusinessId += refererAction.AreaControllers.Name + "-" + refererAction.Name + "|";
                                }
                            }
                        }
                        // FOR SOLVING MENU SELECTION PROBLEM
                    }
                }

                //set menu list to session
                HttpRuntime.Cache.Insert(key, menuList);
            }

            return menuList;
        }

        private static Menu GetMenu(long menuId)
        {
            var menuList = LoadMenuList();
            var query = from a in menuList
                where
                    a.Id == menuId
                select a;

            var obj = query.FirstOrDefault();
            return obj;
        }

        #endregion Menu-MenuAccess


        #region General

        public static void ResetCache(bool action = true, bool menu = true, bool users = false)
        {
            if (action)
                LoadActionList(true);
            if (menu)
                LoadMenuList(true);
            if (users)
            {
                var authList = HttpRuntime.Cache.Get(GlobalAuthKey) as List<AuthInfo> ?? new List<AuthInfo>();
                authList.ForEach(x => { x.PermissionChangeTime = DateTime.Now.Ticks; x.ForceLogoff = false; x.PermissionDenied = false; });
            }
        }

        public static void ResetOrganicationCache()
        {
            LoadOrganizationList(true);
        }

        public static void ResetProgramCache()
        {
            LoadProgramList(true);
        }

        public static void ResetBranchCache()
        {
            LoadBranchList(true);
        }

        public static void ResetProgramBranchPairCache()
        {
            LoadProgramBranchPairList(true);
        }
        #endregion


        #region Organization

        private static List<Organization> LoadOrganizationList(bool forceFormDb = false)
        {
            const string key = "GlobalOrganizationList";
            List<Organization> orgList = null;


            //try load from cache
            if (forceFormDb == false && HttpRuntime.Cache[key] != null)
            {
                orgList = HttpRuntime.Cache.Get(key) as List<Organization>;
            }

            //load from db & save to cache
            if (orgList == null)
            {
                using (var session = NHibernateSessionFactory.OpenSession())
                {
                    var orgService = new OrganizationService(session);
                    orgList = orgService.LoadOrganization().ToList();

                    //set list to session
                    HttpRuntime.Cache.Insert(key, orgList);
                }
            }
            return orgList;
        }

        public static List<long> LoadOrganizationIdList()
        {
            var orgList = LoadOrganizationList();
            var query = from a in orgList
                        select a.Id;

            var idList = query.ToList();
            return idList;
        }

        #endregion Organization


        #region Program

        private static List<Program> LoadProgramList(bool forceFormDb = false)
        {
            const string key = "GlobalProgramList";
            List<Program> proList = null;


            //try load from cache
            if (forceFormDb == false && HttpRuntime.Cache[key] != null)
            {
                proList = HttpRuntime.Cache.Get(key) as List<Program>;
            }

            //load from db & save to cache
            if (proList == null)
            {
                using (var session = NHibernateSessionFactory.OpenSession())
                {
                    var proService = new ProgramService(session);
                    proList = proService.LoadProgram().ToList();

                    //set list to session
                    HttpRuntime.Cache.Insert(key, proList);
                }
            }
            return proList;
        }

        public static List<long> LoadProgramIdList(List<long> orgIdList = null)
        {
            var proList = LoadProgramList();
            var query = from a in proList
                where orgIdList == null
                      || orgIdList.Contains(a.Organization.Id)
                select a.Id;

            var idList = query.ToList();
            return idList;
        }

        #endregion Program


        #region Branch

        private static List<Branch> LoadBranchList(bool forceFormDb = false)
        {
            const string key = "GlobalBranchList";
            List<Branch> branchList = null;


            //try load from cache
            if (forceFormDb == false && HttpRuntime.Cache[key] != null)
            {
                branchList = HttpRuntime.Cache.Get(key) as List<Branch>;
            }

            //load from db & save to cache
            if (branchList == null)
            {
                using (var session = NHibernateSessionFactory.OpenSession())
                {
                    var branchService = new BranchService(session);
                    branchList = branchService.LoadBranch().ToList();

                    //set list to session
                    HttpRuntime.Cache.Insert(key, branchList);
                }
            }
            return branchList;
        }

        public static List<long> LoadBranchIdList(List<long> orgIdList = null)
        {
            var branchList = LoadBranchList();
            var query = from a in branchList
                where orgIdList == null
                      || orgIdList.Contains(a.Organization.Id)
                select a.Id;

            var idList = query.ToList();
            return idList;
        }

        #endregion Branch


        #region ProgramBranchPair

        private static List<ProgramBranchPair> LoadProgramBranchPairList(bool forceFormDb = false)
        {
            const string key = "GlobalProgramBranchPairList";
            List<ProgramBranchPair> dataList = null;


            //try load from cache
            if (forceFormDb == false && HttpRuntime.Cache[key] != null)
            {
                dataList = HttpRuntime.Cache.Get(key) as List<ProgramBranchPair>;
            }

            //load from db & save to cache
            if (dataList == null)
            {
                dataList = new List<ProgramBranchPair>();
                var programList = LoadProgramList(forceFormDb);
                var branchList = LoadBranchList(forceFormDb);
                foreach (var pro in programList)
                {
                    var filterBranchList = branchList.Where(x => x.Organization.Id == pro.Organization.Id);
                    foreach (var bra in filterBranchList)
                    {
                        dataList.Add(new ProgramBranchPair(pro.Organization.Id, pro.Id, bra.Id));
                    }
                }

                //set list to session
                HttpRuntime.Cache.Insert(key, dataList);
            }
            return dataList;
        }

        public static List<ProgramBranchPair> LoadProgramBranchPairList(List<long> orgIdList, List<long> programIdList, List<long> branchIdList)
        {
            var pairList = LoadProgramBranchPairList();
            if (orgIdList != null)
                pairList = pairList.Where(x => orgIdList.Contains(x.OrganizationId)).ToList();
            if (programIdList != null)
                pairList = pairList.Where(x => programIdList.Contains(x.ProgramId)).ToList();
            if (branchIdList != null)
                pairList = pairList.Where(x => branchIdList.Contains(x.BranchId)).ToList();

            return pairList;
        }

        #endregion ProgramBranchPair

    }

    public class AuthInfo
    {
        public long UserId { get; set; }
        public long PermissionChangeTime { get; set; }
        public bool ForceLogoff { get; set; }
        public bool PermissionDenied { get; set; }

        public AuthInfo(long userId, long permissionChangeTime, bool forceLogoff = false, bool permissionDenied = false)
        {
            this.UserId = userId;
            this.PermissionChangeTime = permissionChangeTime;
            this.ForceLogoff = forceLogoff;
            this.PermissionDenied = permissionDenied;
        }
    }
}