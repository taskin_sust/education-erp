﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UdvashERP.Dao.Administration;
using UdvashERP.Test.SessionFactory;

namespace UdvashERP.Test.Administration
{
    [TestClass]
    public class TestBranchDao
    {  
        private readonly IBranchDao _branchDao;

        public TestBranchDao()
        {
            _branchDao=new BranchDao(){Session = NHibernateSessionFactory.OpenSession()};
        }
     
        #region LoadAuthorizedBranch 

        [TestMethod]
        public void TestLoadAuthorizedBranch()
        {
            var branches = _branchDao.LoadAuthorizedBranch(null, null, null, null);
            int rowCount = branches.Count;
            const int actualBranch = 27;
            Assert.IsTrue(rowCount>0,"");
            Assert.AreEqual(rowCount,actualBranch, "");
        }

        [TestMethod]
        public void TestLoadAuthorizedBranchByProgramId()
        {
            const int programId = 68;
            var branches = _branchDao.LoadAuthorizedBranch(null, null, programId, null);
            int rowCount = branches.Count;
            const int actualBranch = 17;
            //Assert.IsTrue(rowCount > 0, "");
            Assert.AreEqual(rowCount, actualBranch, "");

        }


	    #endregion
    }
    
 
    
    

}
