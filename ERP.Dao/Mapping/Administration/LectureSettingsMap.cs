﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
   public class LectureSettingsMap : BaseClassMap<LectureSettings, long>
    {
        public LectureSettingsMap()
        {
            Table("LectureSettings");
            LazyLoad();
            Map(x => x.Rank);
            Map(x => x.NumberOfClasses);
            Map(x => x.ClassNamePrefix);
            References(x => x.CourseSubject).Column("CourseSubjectId");
            References(x => x.Course).Column("CourseId");  
            HasMany(x => x.Lectures).KeyColumn("LectureSettingsId").Cascade.SaveUpdate();
        }
    }
}
