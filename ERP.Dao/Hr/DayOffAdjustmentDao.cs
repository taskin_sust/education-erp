﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IDayOffAdjustmentDao : IBaseDao<DayOffAdjustment, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        DayOffAdjustment GetApprovedDayOffAdjustmentByPinAndDate(long memberId, DateTime attendenceDate);
        DayOffAdjustment GetDayOffAdjustmentByInsteadOfDateAndMemberId(long memberId, DateTime insteadOfDate);
        DayOffAdjustment GetTeamMemberLastIsPostDayOffAdjustment(long? teamMemberId, int? pin, string orderby = "DayOffDate");
        
        #endregion

        #region List Loading Function
        IList<DayOffAdjustmentMentorDto> LoadDayOffRecentAdjustmentForMentor(List<long> mentorTeamMemberIdList, int start, int length, string orderBy, string toUpper, string pin, DateTime dateFrom, DateTime dateTo);
        IList<DayOffAdjustmentHrDto> LoadAllHrDayOffAdjustmentHistory(int start, int length, string orderBy, string toUpper, int convertedPin, DateTime dateFrom, DateTime dateTo, List<long> authBranchIdList, List<long> campusIdList, List<long> departmentIdList);
        #endregion

        #region Others Function

        bool CheckHasDuplicate(DayOffAdjustment dayOffAdjustment);
        int CountDayOffRecentAdjustmentForMentor(List<long> mentorTeamMemberIdList, string pin, DateTime dateFrom, DateTime dateTo);
        int CountHrDayOffAdjustmentHistory(int pin, DateTime dateFrom, DateTime dateTo, List<long> authBranchIdList, List<long> campusIdList, List<long> departmentIdList);
        bool CheckHasDayOffAdjustmentOnADate(long teamMemberId, DateTime dateTime);
        DateTime? GetTeamMemberLastAppliedDayOffAdjustment(long teamMemberId);

        #endregion

        #region Helper Function

        #endregion



    }
    public class DayOffAdjustmentDao : BaseDao<DayOffAdjustment, long>, IDayOffAdjustmentDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public DayOffAdjustment GetApprovedDayOffAdjustmentByPinAndDate(long memberId, DateTime attendenceDate)
        {
            ICriteria criteria = Session.CreateCriteria<DayOffAdjustment>()
                               .Add(Restrictions.Eq("DayOffDate", attendenceDate))
                               .Add(Restrictions.Eq("Status", DayOffAdjustment.EntityStatus.Active))
                               .Add(Restrictions.Eq("DayOffStatus", (int)DayOffAdjustmentStatus.Approved))
                               ;
            criteria.CreateAlias("TeamMember", "member")
                .Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
                .Add(Restrictions.Eq("member.Id", memberId));
            return criteria.List<DayOffAdjustment>().FirstOrDefault();
        }
        public DayOffAdjustment GetDayOffAdjustmentByInsteadOfDateAndMemberId(long memberId, DateTime insteadOfDate)
        {
            ICriteria criteria = Session.CreateCriteria<DayOffAdjustment>()
                               .Add(Restrictions.Eq("InsteadOfDate", insteadOfDate))
                               .Add(Restrictions.Eq("Status", DayOffAdjustment.EntityStatus.Active))
                               .Add(Restrictions.Eq("DayOffStatus", (int)DayOffAdjustmentStatus.Approved))
                               ;
            criteria.CreateAlias("TeamMember", "member")
                .Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
                .Add(Restrictions.Eq("member.Id", memberId));
            return criteria.List<DayOffAdjustment>().FirstOrDefault();
        }

        public DayOffAdjustment GetTeamMemberLastIsPostDayOffAdjustment(long? teamMemberId, int? pin, string orderby = "DayOffDate")
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<DayOffAdjustment>();
            criteria.CreateAlias("TeamMember", "member");
            criteria.Add(Restrictions.Eq("IsPost", true));
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));

            criteria.AddOrder(Order.Desc(orderby));

            criteria.SetTimeout(3000);
            return criteria.List<DayOffAdjustment>().Take(1).FirstOrDefault();
        }

        #endregion

        #region List Loading Function

        public IList<DayOffAdjustmentMentorDto> LoadDayOffRecentAdjustmentForMentor(List<long> mentorTeamMemberIdList, int start, int length, string orderBy, string toUpper, string pin, DateTime dateFrom, DateTime dateTo)
        {
            string orderByQuery = "";
            string query1 = GetDayOffRecentAdjustmentForMentorQuery(mentorTeamMemberIdList, pin, dateFrom, dateTo);
            var query = @"Select * FROM( 
                            " + query1 + @"
                              
                        ) AS a";

            if (!String.IsNullOrEmpty(orderBy))
            {
                orderByQuery = " Order By a." + orderBy + " " + toUpper + "  ";
            }
            else
            {
                orderByQuery = " Order By a.TeamMemberPin ASC ";
            }
            query += orderByQuery;
            if (length > 0)
            {
                query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            iQuery.SetResultTransformer(Transformers.AliasToBean<DayOffAdjustmentMentorDto>());
            var list = iQuery.List<DayOffAdjustmentMentorDto>().ToList();
            return list;
        }
        
        public string GetAllHrDayOffAdjustmentHistory(int pin, DateTime dateFrom, DateTime dateTo, List<long> authBranchIdList, List<long> campusIdList, List<long> departmentIdList)
        {
            string effectiveDateFilter = " AND eh.EffectiveDate <= '" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "' ";
            string dayoffDateFilter = " AND doff.DayOffDate >= '" + dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "'  AND doff.DayOffDate <='" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "'";

            string query = @"Select doff.Id
                            , tm.Name as Name
                            , tm.pin as Pin
                            , cam.Name as Campus
                            , desg.Name as Designation
                            , dep.Name as Department
                            , a.TeamMemberId as TeamMemberId
                            , doff.DayOffDate as DayOffDate,doff.InsteadOfDate as InsteadOfDate,doff.DayOffStatus as DayOffStatus
                            , doff.Reason as Reason
                            , CASE When how.Id > 0 then 0 ELSE 1 END AS CanEdit
                            , doff.ModifyBy AS ModifiedBy
                            , doff.ModificationDate AS LastModificationDate
                            , org.ShortName as Organization,br.Name as Branch from [HR_TeamMember] as tm
                            inner join (
	                            Select * from(
		                            Select eh.CampusId,eh.DesignationId,eh.DepartmentId,eh.TeamMemberId,eh.EffectiveDate,eh.EmploymentStatus, 
			                            RANK() OVER(PARTITION BY eh.TeamMemberId order by eh.EffectiveDate DESC) as rank
			                            from [HR_EmploymentHistory] as eh                
			                            where eh.Status='" + EmploymentHistory.EntityStatus.Active + "'";
            //if (!String.IsNullOrEmpty(date))
            //{
                //query += @" AND eh.EffectiveDate<='" + date + "'";
                query += effectiveDateFilter;
            //}
            query += @") as emplyHistory where emplyHistory.rank=1
                            ) as a on tm.Id=a.TeamMemberId
                            left join [Campus] as cam on cam.id = a.CampusId and cam.Status=1
                            left join [HR_Designation] as desg on desg.id=a.DesignationId and desg.Status=1
                            left join [HR_Department] as dep on dep.Id = a.DepartmentId and dep.Status=1
                            left join [Branch] as br on br.id= cam.BranchId and br.Status=1
                            left join [Organization] as org on org.id=br.OrganizationId and org.Status=1
                            inner join [HR_DayOffAdjustment] as doff on doff.TeamMemberId=a.TeamMemberId and doff.Status=1
                            left join HR_HolidayWork as how on how.TeamMemberId = tm.Id and how.HolidayWorkDate = doff.InsteadOfDate and how.Status = 1 and how.ApprovalType = 1
                            where tm.Status=1 ";
            if (authBranchIdList != null && !authBranchIdList.Contains(SelectionType.All))
            {
                query += @" AND br.Id in(" + string.Join(",", authBranchIdList) + ")";
            }
            if (campusIdList != null && !campusIdList.Contains(SelectionType.All))
            {
                query += @" AND cam.Id in(" + string.Join(",", campusIdList) + ")";
            }
            if (departmentIdList != null && !departmentIdList.Contains(SelectionType.All))
            {
                query += @" AND dep.id in(" + string.Join(",", departmentIdList) + ")";
            }
            if (pin != 0)
            {
                query += @" AND tm.Pin in(" + string.Join(",", pin) + ")";
            }
            //if (!String.IsNullOrEmpty(date))
            //{
            //    query += " AND doff.DayOffDate='" + date + "'";
            //}
            query += dayoffDateFilter;
           

            return query;
        }

        public IList<DayOffAdjustmentHrDto> LoadAllHrDayOffAdjustmentHistory(int start, int length, string orderBy, string toUpper, int pin, DateTime dateFrom, DateTime dateTo, List<long> authBranchIdList, List<long> campusIdList, List<long> departmentIdList)
        {
            string query1 = GetAllHrDayOffAdjustmentHistory(pin, dateFrom, dateTo, authBranchIdList, campusIdList, departmentIdList);
            string query = @"Select * FROM(" + query1;
            query += ") as a order by a.Id";
            if (length > 0)
            {
                query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            iQuery.SetResultTransformer(Transformers.AliasToBean<DayOffAdjustmentHrDto>());
            var list = iQuery.List<DayOffAdjustmentHrDto>().ToList();
            return list;
        }

        #endregion

        #region Others Function

        public bool CheckHasDuplicate(DayOffAdjustment dayOffAdjustment)
        {
            var rowList = Session.QueryOver<DayOffAdjustment>().Where(x => (x.DayOffDate == dayOffAdjustment.DayOffDate || x.InsteadOfDate == dayOffAdjustment.InsteadOfDate) && x.TeamMember == dayOffAdjustment.TeamMember && x.Status == DayOffAdjustment.EntityStatus.Active && x.DayOffStatus == (int)DayOffAdjustmentStatus.Approved && x.Id!=dayOffAdjustment.Id).RowCount();
            if (rowList < 1)
                return false;
            return true;
        }

        public int CountDayOffRecentAdjustmentForMentor(List<long> mentorTeamMemberIdList, string pin, DateTime dateFrom, DateTime dateTo)
        {
            string query1 = GetDayOffRecentAdjustmentForMentorQuery(mentorTeamMemberIdList, pin, dateFrom, dateTo);
            var query = @"Select COUNT(*) as totalRow FROM( 
                            " + query1 + @"
                              
                        ) AS a";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var totalRow = iQuery.UniqueResult<int>();
            return totalRow;
        }

        public int CountHrDayOffAdjustmentHistory(int pin, DateTime dateFrom, DateTime dateTo, List<long> authBranchIdList, List<long> campusIdList, List<long> departmentIdList)
        {
            string query1 = GetAllHrDayOffAdjustmentHistory(pin, dateFrom, dateTo, authBranchIdList, campusIdList, departmentIdList);
            var query = @"Select COUNT(*) as totalRow FROM( 
                            " + query1 + @"
                              
                        ) AS a";
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var totalRow = iQuery.UniqueResult<int>();
            return totalRow;
        }

        public bool CheckHasDayOffAdjustmentOnADate(long teamMemberId, DateTime dateTime)
        {
            int dayOffAdjustmentCount = Session.QueryOver<DayOffAdjustment>().Where(x => x.InsteadOfDate.Value.Date == dateTime.Date && x.Status == DayOffAdjustment.EntityStatus.Active && x.DayOffStatus == (int)DayOffAdjustmentStatus.Approved && x.TeamMember.Id == teamMemberId).RowCount();

            if (dayOffAdjustmentCount > 0)
                return true;
            return false;
        }

        public DateTime? GetTeamMemberLastAppliedDayOffAdjustment(long teamMemberId)
        {
            DateTime? dateTime = null;

            string sql = @"select 
case when max(dayoffDate)> max(insteadofDate) then max(dayoffDate) else max(insteadofDate) end MaxDate 
from HR_DayOffAdjustment 
where status = " + DayOffAdjustment.EntityStatus.Active + @"
and  TeamMemberId = " + teamMemberId + @"
and ( DayOffStatus = " + (int)DayOffAdjustmentStatus.Approved + @" Or DayOffStatus = " + (int)DayOffAdjustmentStatus.Pending + @")";
            IQuery iQuery = Session.CreateSQLQuery(sql);
            iQuery.SetTimeout(2700);
            dateTime = iQuery.UniqueResult<DateTime?>();
            return dateTime;
        }

        #endregion

        #region Helper Function

        public string GetDayOffRecentAdjustmentForMentorQuery(List<long> mentorTeamMemberIdList, string pin, DateTime dateFrom, DateTime dateTo)
        {
            string dateFilter = "";
            string pinFilter = "";
            string mentorTeamMemberIdFilter = "";
            //if (!String.IsNullOrEmpty(date))
            //{
            dateFilter = @" AND doa.DayOffDate >='" + dateFrom.Date.AddHours(0).AddMinutes(0).AddSeconds(0) + "' AND doa.DayOffDate <='" + dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59) + "' ";
            //}
            if (!String.IsNullOrEmpty(pin))
            {
                pinFilter = @" AND tm.Pin =" + Convert.ToInt32(pin) + " ";
            }
            if (mentorTeamMemberIdList != null)
            {
                mentorTeamMemberIdFilter = @" AND tm.Id in(" + string.Join(",", mentorTeamMemberIdList) + ")";
            }
            string query = @"select 
                             tm.Id as TeamMemberId
                             , tm.Name as TeamMemberName
                             , tm.pin as TeamMemberPin
                             , doa.DayOffDate as DayOffDate
                             , doa.InsteadOfDate as InsteadOfDate
                             , doa.Reason as Reason
                             , doa.Id As Id
                             , doa.DayOffStatus as DayOffStatus
                             , CASE When how.Id > 0 OR doa.DayOffStatus <> 1 then 0 ELSE 1 END AS CanEdit
                            from HR_DayOffAdjustment as doa
                            inner join HR_TeamMember as tm on doa.TeamMemberId = tm.Id and tm.Status = 1
                            left join HR_HolidayWork as how on how.TeamMemberId = tm.Id and how.HolidayWorkDate = doa.InsteadOfDate and how.Status = 1 and how.ApprovalType = 1
                            Where 1=1     
                            " + dateFilter + @"
                            " + pinFilter + @"
                            " + mentorTeamMemberIdFilter + @"
                            ";
            return query;
        }

        #endregion
    }
}
