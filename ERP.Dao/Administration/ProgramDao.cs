﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Administration
{
    public interface IProgramDao : IBaseDao<Program, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        Program GetProgram(string shortName, bool caseSensitive = true);

        #endregion

        #region List Loading Function

        IList<Program> LoaProgram(List<long> organizationIds = null, List<long> sessionIds = null);
        IList<Program> LoadAuthorizedProgram(List<long> programIdList);
        IList<Program> LoadAuthorizedProgram(List<long> authorizeProgramIdList, List<long> sessionIdList, bool? isOffice = null, bool? isPublic = null, List<long> branchIdList = null);
        IList<Program> LoadProgram(int start, int length, string orderBy, string direction, string organization, string programName, string code, string shortName, string type, string status, string rank);
        IList<Program> LoadProgram(DateTime date, long teacherId);

        IList<string> LoadProgramCode();
        #endregion

        #region Others Function
        int GetProgramCount(string orderBy, string direction, string organization, string programName, string code, string shortName, string type, string status, string rank);
        bool IsDuplicateProgram(string programName, string programCode, string programShortName, int programType, long? id = null);
        bool CheckDuplicateShortName(Program programObj);

        string HasDuplicateProgram(Program program);
        #endregion

        #region Public API
        IList<Program> LoadPublicProgram(string orgBusinessId);
        #endregion

    }
    public class ProgramDao : BaseDao<Program, long>, IProgramDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public Program GetProgram(string shortName, bool caseSensitive = true)
        {
            ICriteria criteria = Session.CreateCriteria<Program>().Add(Restrictions.Eq("Status", Batch.EntityStatus.Active));
            if (!string.IsNullOrEmpty(shortName))
            {
                criteria.Add(caseSensitive
                    ? Restrictions.Eq("ShortName", shortName)
                    : Restrictions.Eq("ShortName", shortName).IgnoreCase());
            }
            return criteria.UniqueResult<Program>();
        }

        #endregion

        #region List Loading Function
        public IList<Program> LoadAuthorizedProgram(List<long> programIdList)
        {
            ICriteria criteria;
            if (programIdList == null)
            {
                criteria = Session.CreateCriteria<Program>();
                criteria.Add(Restrictions.Eq("Status", Program.EntityStatus.Active));
                criteria.AddOrder(Order.Asc("Rank"));
                return criteria.List<Program>();
            }

            criteria = Session.CreateCriteria<ProgramBranchSession>();
            criteria.Add(Restrictions.In("Program.Id", programIdList));
            criteria.Add(Restrictions.Eq("Status", Program.EntityStatus.Active));
            criteria.SetProjection(Projections.Distinct(Projections.Property("Program")));
            //criteria.SetProjection(Projections.Property("Program"));
            return criteria.List<Program>().OrderBy(x => x.Rank).ToList();

        }

        public IList<Program> LoaProgram(List<long> organizationIds = null, List<long> sessionIds = null)
        {
            ICriteria criteria = Session.CreateCriteria<Program>().Add(Restrictions.Eq("Status", Program.EntityStatus.Active));
            //DetachedCriteria programInProgramBranchSession = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Program.Id")));
            if (organizationIds != null)
            {
                criteria.CreateAlias("Organization", "o");
                if (!organizationIds.Contains(SelectionType.SelelectAll))
                    criteria.Add(Restrictions.In("o.Id", organizationIds));
            }

            if (sessionIds != null)
            {
                DetachedCriteria programInProgramBranchSession = DetachedCriteria.For<ProgramBranchSession>().SetProjection(Projections.Distinct(Projections.Property("Program.Id")));
                programInProgramBranchSession.CreateAlias("Session", "s").Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
                if (!sessionIds.Contains(SelectionType.SelelectAll))
                    programInProgramBranchSession.Add(Restrictions.In("s.Id", sessionIds));
                criteria.Add(Subqueries.PropertyIn("Id", programInProgramBranchSession));
            }

            criteria.AddOrder(Order.Asc("Rank"));
            return criteria.List<Program>().ToList();
        }

        public IList<Program> LoadAuthorizedProgram(List<long> authorizeProgramIdList, List<long> sessionIdList, bool? isOffice = null, bool? isPublic = null, List<long> branchIdList = null)
        {
            ICriteria criteria = Session.CreateCriteria<Program>()
                .Add(Restrictions.Eq("Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Organization", "or")
                .Add(Restrictions.Eq("or.Status", Organization.EntityStatus.Active));

            DetachedCriteria programSessionBranch = DetachedCriteria.For<ProgramBranchSession>()
                .SetProjection(Projections.Distinct(Projections.Property("Program.Id")));
            programSessionBranch.CreateAlias("Session", "s")
                .Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));

            programSessionBranch.CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
            if (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll))
            {
                programSessionBranch.Add(Restrictions.In("br.Id", branchIdList));
            }


            if (authorizeProgramIdList != null)
            {
                criteria.Add(Restrictions.In("Id", authorizeProgramIdList));
            }
            if (isPublic != null)
            {
                programSessionBranch.Add(Restrictions.Eq("IsPublic", isPublic));
            }
            if (isOffice != null)
            {
                programSessionBranch.Add(Restrictions.Eq("IsOffice", isOffice));
            }
            if (sessionIdList != null)
            {
                programSessionBranch.Add(Restrictions.In("s.Id", sessionIdList));
            }
            criteria.Add(Subqueries.PropertyIn("Id", programSessionBranch));

            return criteria.List<Program>().OrderBy(x => x.Rank).ToList();
        }

        //public IList<Program> LoadAuthorizedProgramBasedOnBranchs(List<long> programIdList, List<long> branchIdList, List<long> sessionIdList, bool? isOffice = null, bool? isPublic = null)
        //{
        //    ICriteria criteria = Session.CreateCriteria<Program>()
        //        .Add(Restrictions.Eq("Status", Program.EntityStatus.Active));
        //    criteria.CreateAlias("Organization", "or")
        //        .Add(Restrictions.Eq("or.Status", Organization.EntityStatus.Active));

        //    DetachedCriteria programSessionBranch = DetachedCriteria.For<ProgramBranchSession>()
        //        .SetProjection(Projections.Distinct(Projections.Property("Program.Id")));
        //    programSessionBranch.CreateAlias("Session", "s")
        //        .Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
        //    programSessionBranch.CreateAlias("Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));

        //    if (branchIdList != null && !branchIdList.Contains(SelectionType.SelelectAll))
        //    {
        //        programSessionBranch.Add(Restrictions.In("br.Id", branchIdList));
        //    }

        //    if (programIdList != null)
        //    {
        //        criteria.Add(Restrictions.In("Id", programIdList));
        //    }
        //    if (isPublic != null)
        //    {
        //        programSessionBranch.Add(Restrictions.Eq("IsPublic", isPublic));
        //    }
        //    if (isOffice != null)
        //    {
        //        programSessionBranch.Add(Restrictions.Eq("IsOffice", isOffice));
        //    }
        //    if (sessionIdList != null)
        //    {
        //        programSessionBranch.Add(Restrictions.In("s.Id", sessionIdList));
        //    }
        //    criteria.Add(Subqueries.PropertyIn("Id", programSessionBranch));

        //    return criteria.List<Program>().OrderBy(x => x.Rank).ToList();
        //}

        public IList<Program> LoadProgram(int start, int length, string orderBy, string direction, string organization, string programName, string code, string shortName, string type, string status, string rank)
        {
            ICriteria criteria = GetLoadProgramCriteria(orderBy, direction, organization, programName, code, shortName, type, status, rank);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Program>();
        }

        public IList<Program> LoadProgram(DateTime date, long teacherId)
        {
            var programList = Session.Query<TeacherClassEntryDetails>().Where(x => x.Status == Teacher.EntityStatus.Active && x.TeacherClassEntry.Status == Teacher.EntityStatus.Active && x.TeacherClassEntry.HeldDate == date.Date && x.Teacher.Id == teacherId).Select(x => x.TeacherClassEntry.Program);
            return programList.Where(x => x.Id != null).Distinct().ToList();
        }

        public IList<string> LoadProgramCode()
        {
            ICriteria criteria = Session.CreateCriteria<Program>();
            criteria.SetProjection(Projections.Distinct(Projections.Property("Code")));
            return criteria.List<string>().ToList();
        }

        #endregion

        #region Others Function
        public bool IsDuplicateProgram(string programName, string programCode, string programShortName, int programType, long? id = null)
        {
            if (!String.IsNullOrEmpty(programName))
            {
                int cc = 0;
                var count = Session.QueryOver<Program>().Where(x => x.Name == programName.Trim() || x.Code == programCode || x.ShortName == programShortName).Where(x => x.Status != Program.EntityStatus.Delete).List<Program>();
                if (id != null && id != 0)
                {
                    count = count.Where(x => x.Id != id).ToList();
                }
                cc = count.Count();
                if (cc > 0)
                    return true;
                return false;
            }
            return true;
        }

        public string HasDuplicateProgram(Program program)
        {
            string returnValue = "";
            if (program != null)
            {
                var countProgramName = Session.QueryOver<Program>().Where(x => x.Name == program.Name.Trim()).Where(x => x.Status != Program.EntityStatus.Delete).Where(x => x.Id != program.Id).List<Program>();
                if (countProgramName.Count > 0)
                {
                    returnValue = "Program name";
                    return returnValue;
                }

                var countShortName = Session.QueryOver<Program>().Where(x => x.ShortName == program.ShortName.Trim()).Where(x => x.Status != Program.EntityStatus.Delete).Where(x => x.Id != program.Id).List<Program>();
                if (countShortName.Count > 0)
                {
                    returnValue = "Short name";
                    return returnValue;
                }
                var countCode = Session.QueryOver<Program>().Where(x => x.Code == program.Code.Trim()).Where(x => x.Status != Program.EntityStatus.Delete).Where(x => x.Id != program.Id).List<Program>();
                if (countCode.Count > 0)
                {
                    returnValue = "Program code";
                    return returnValue;
                }
            }
            return returnValue;
        }

        public int GetProgramCount(string orderBy, string direction, string organization, string programName, string code, string shortName, string type, string status, string rank)
        {
            ICriteria criteria = GetLoadProgramCriteria(orderBy, direction, organization, programName, code, shortName, type, status, rank, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }
        public bool CheckDuplicateShortName(Program programObj)
        {
            ICriteria criteria = Session.CreateCriteria<Program>();
            criteria.Add(Restrictions.Eq("ShortName", programObj.ShortName));
            criteria.Add(Restrictions.Eq("Status", Organization.EntityStatus.Active));
            if (programObj.Id > 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", programObj.Id)));
            }
            var programList = criteria.List<Program>();
            if (programList != null && programList.Count > 0)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region Helper Function
        private ICriteria GetLoadProgramCriteria(string orderBy, string direction, string organization, string programName, string code, string shortName, string type, string status, string rank, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<Program>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Program.EntityStatus.Delete)));

            criteria.CreateAlias("Organization", "organization")
                .Add(Restrictions.Eq("organization.Status", Organization.EntityStatus.Active));
            if (!String.IsNullOrEmpty(organization))
            {
                criteria.Add(Restrictions.Eq("organization.Id", Convert.ToInt64(organization)));
            }
            if (!String.IsNullOrEmpty(programName))
            {
                criteria.Add(Restrictions.InsensitiveLike("Name", programName.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(shortName))
            {
                criteria.Add(Restrictions.InsensitiveLike("ShortName", shortName.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(code))
            {
                criteria.Add(Restrictions.InsensitiveLike("Code", code.Trim(), MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(type))
            {
                criteria.Add(Restrictions.Eq("Type", Convert.ToInt32(type.Trim())));
            }

            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }

            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status.Trim())));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(direction == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }

        #endregion

        #region Public API Functions
        public IList<Program> LoadPublicProgram(string orgBusinessId)
        {
            ICriteria criteria = Session.CreateCriteria<Program>().Add(Restrictions.Eq("Status", Program.EntityStatus.Active)).CreateAlias("Organization", "o")
                .Add(Restrictions.Eq("o.BusinessId", orgBusinessId)).CreateAlias("ProgramBranchSessions", "pbs").Add(Restrictions.Eq("pbs.IsPublic", true));

            return criteria.List<Program>().OrderBy(x => x.Rank).Distinct().ToList();
        }
        #endregion



    }
}