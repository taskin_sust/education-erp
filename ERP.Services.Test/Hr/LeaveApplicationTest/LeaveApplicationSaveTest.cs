﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.LeaveApplicationTest
{
    public interface ILeaveApplicationSaveTest
    {
        #region Common
        void Should_Return_InvalidDataException_ForEmptyList();
        void Should_Save_Successfully();

        #endregion

        #region Overlaping or duplicate leave application for same team member check
        void Should_Return_DuplicateDataException();

        #endregion

        #region New Leave Application

        void Should_Return_InvalidDataAccess_Exception_Mentor_Cant_AddNew_Application();
        void Should_return_InvalidDataException_OutOff_Leave_Amount();

        #endregion

        #region Update Leave Application

        void Should_Return_Invalid_TeamMember();
        void Should_Return_InvalidData_Manupulation_From_Public_To_Private_OR_WithPay_To_WithoutPay();
        void Should_Return_Invalid_Data_After_Applied_for_Leave();
        void Should_Return_Invalid_Leave_BecauseOf_Salary_Already_Taken();
        void Should_Return_Cant_Cancel_Leave_BecauseOf_Salary_Already_Taken();
        void Should_Return_Cant_Cancel_Leave_BecauseOf_Date_Passed();

        #endregion

        #region Leave Type and DateFrom DateTo Edit implementation Start

        void Should_return_Insufficient_Leave_Amount();
        void Should_Return_Invalid_Combination_Of_Leave_Application();

        #endregion

        #region DateFrom DateTo Edit for Leave Application Details

        void Should_Return_Is_Already_Posted_Leave();

        #endregion

        #region Leave Type Not Changed Summary Calculation
        void Should_Return_Insufficient_Leave_Amount();

        #region For Regular leave

        void Should_Return_Invalid_Regular_leave_Combination_Of_Leave_Application_05();
        void Should_Return_Invalid_Combination_Of_Leave_Application_06();
        void Should_Return_Invalid_SummaryCalculation();
        #endregion

        #endregion

        #region Validation Test

        void Should_Return_EmptyFieldException_For_Leave();
        void Should_Return_EmptyFieldException_For_TeamMember();
        void Should_Return_Date_From_Must_Be_Less_Then_Or_Equal_Date_To_Exception();
        void Should_Return_EmptyFieldException_For_LeaveApp_Reason();
        void Should_Return_InvalidDataException_For_Same_Responsible_Person();
        void Should_Return_InvalidDataException_For_Organization();
        void Should_Return_InvalidDataException_For_Not_Taking_Leave_From_Current_Organization();
        void Should_Return_InvalidDataException_For_Not_Taking_Leave_From_Current_Year_WithoutPay();
        void Should_Return_InvalidDataException_For_Not_Taking_Leave_From_Current_Year_WithtPay();

        #region Pre Post [Responsible Person Check]

        void Should_Return_EmptyFieldException_For_Empty_ResponsiblePerson();
        void Should_Return_EmptyFieldException_For_ResponsiblePerson_FutureLeave();


        #endregion

        #region Duplicate Leave Check

        void Should_Return_Duplicate_Entry_Exception();
        void Should_Return_Invalid_LeaveApplication_In_Holiday();

        #endregion

        #region Max Taking day Check

        void Should_Return_Invalid_Maximum_Taking_Limit();

        #endregion

        #region Pre Post [Working Days calculation]

        void Should_Return_Apply_Before_Working_Days_Exception();
        void Should_Return_Apply_After_Working_Days_Exception();

        #endregion

        #endregion

        //#region SaveSmsHistory

        //void Should_Return_Invalid_Designation();
        //void Should_Return_Invalid_Department();
        //void Should_Return_Invalid_Oranization();
        //void Should_Return_Invalid_Responsible_MemDesignation();
        //void Should_Return_Invalid_Responsible_MemDepartment();
        //void Should_Return_Invalid_Responsible_Organization();
        //void Should_Return_Invalid_MentorHistory();
        //void Should_Return_Invalid_Mentor_MemDesignation();
        //void Should_Return_Invalid_Mentor_Department();
        //void Should_Return_Invalid_Mentor_Orgqanization();







    }
    [Trait("Area", "Hr")]
    [Trait("Service", "LeaveApplication")]
    public class LeaveApplicationSaveTest : LeaveApplicationBaseTest, ILeaveApplicationSaveTest
    {
        #region Common

        [Fact]
        public void Should_Return_InvalidDataException_ForEmptyList()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);
            Assert.Throws<InvalidDataException>(() => _leaveApplicationService.SaveOrUpdate(null, false, false));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);
            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            Assert.True(_leaveApplicationFactory.Object.Id > 0);
        }

        #endregion

        #region Overlaping or duplicate leave application for same team member check

        [Fact]
        public void Should_Return_DuplicateDataException()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);

            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);

            Assert.Throws<DuplicateEntryException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.SingleObjectList[1] }, false, true));
        }
        #endregion

        #region New Leave Application

        [Fact]
        public void Should_Return_InvalidDataAccess_Exception_Mentor_Cant_AddNew_Application()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.Leave.IsPublic = false;
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, false));
        }

        [Fact]
        public void Should_return_InvalidDataException_OutOff_Leave_Amount()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.TotalLeaveDay = 50;
            _leaveApplicationFactory.Object.DateFrom = DateTime.Now;
            _leaveApplicationFactory.Object.DateTo = DateTime.Now.AddDays(50);
            _leaveApplicationFactory.Object.Leave.IsPublic = true;
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }
        #endregion

        #region Update Leave Application

        [Fact]
        public void Should_Return_Invalid_TeamMember()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            _leaveApplicationFactory.Object.TeamMember = PersistNewTeamMemberForLeaveApplication(teamMember.EmploymentHistory, teamMember.ShiftWeekendHistory,
                DateTime.Now.AddDays(-365), DateTime.Now.AddDays(365), teamMember.MaritalInfo, teamMember.MentorHistory);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_InvalidData_Manupulation_From_Public_To_Private_OR_WithPay_To_WithoutPay()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);

            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            Session.Evict(_leaveApplicationFactory.Object);
            Session.Evict(_leaveApplicationFactory.Object.Leave);

            _leaveApplicationFactory.Object.Leave.IsPublic = false;
            _leaveApplicationFactory.Object.Leave.PayType = (int)PayType.WithoutPay;

            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_Invalid_Data_After_Applied_for_Leave()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            // session.Evict(_leaveApplicationFactory.Object);
            //session.Evict(_leaveApplicationFactory.Object.Leave);

            _leaveApplicationFactory.Object.LeaveStatus = (int)LeaveStatus.Rejected;
            _leaveApplicationFactory.Object.LeaveApplicationDetail[0].IsPost = true;
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_Invalid_Leave_BecauseOf_Salary_Already_Taken()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            Session.Evict(_leaveApplicationFactory.Object);
            Session.Evict(_leaveApplicationFactory.Object.Leave);

            _leaveApplicationFactory.Object.LeaveApplicationDetail[0].IsPost = true;
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_Cant_Cancel_Leave_BecauseOf_Salary_Already_Taken()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            Session.Evict(_leaveApplicationFactory.Object);
            Session.Evict(_leaveApplicationFactory.Object.Leave);

            _leaveApplicationFactory.Object.LeaveStatus = (int)LeaveStatus.Cancelled;
            _leaveApplicationFactory.Object.LeaveApplicationDetail[0].IsPost = true;
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_Cant_Cancel_Leave_BecauseOf_Date_Passed()
        {
            var teamMember = PersistTeamMemberForHistory();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            Session.Evict(_leaveApplicationFactory.Object);
            Session.Evict(_leaveApplicationFactory.Object.Leave);
            var la = _leaveApplicationService.GetById(_leaveApplicationFactory.Object.Id);
            la.DateFrom = DateTime.Now.AddDays(-5);
            la.DateTo = DateTime.Now.AddDays(-4);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { la }, false, false));
        }

        #endregion

        #region Leave Type and DateFrom DateTo Edit implementation Start

        [Fact]
        public void Should_return_Insufficient_Leave_Amount()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            Session.Evict(_leaveApplicationFactory.Object);
            Session.Evict(_leaveApplicationFactory.Object.Leave);

            _leaveApplicationFactory.Object.Leave = _leaveFactory.Create().Persist().Object;
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_Invalid_Combination_Of_Leave_Application()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.LeaveStatus = (int)LeaveStatus.Cancelled;
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            Session.Evict(_leaveApplicationFactory.Object);
            Session.Evict(_leaveApplicationFactory.Object.Leave);

            _leaveApplicationFactory.Object.Leave = _leaveFactory.Create().Persist().Object;

            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        #endregion

        #region DateFrom DateTo Edit for Leave Application Details

        [Fact]
        public void Should_Return_Is_Already_Posted_Leave()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            Session.Evict(_leaveApplicationFactory.Object);
            Session.Evict(_leaveApplicationFactory.Object.Leave);

            _leaveApplicationFactory.Object.DateFrom = DateTime.Now.AddDays(10);
            _leaveApplicationFactory.Object.DateTo = DateTime.Now.AddDays(12);

            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }
        #endregion

        #region Leave Type Not Changed Summary Calculation

        [Fact]
        public void Should_Return_Insufficient_Leave_Amount()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            Session.Evict(_leaveApplicationFactory.Object);
            Session.Evict(_leaveApplicationFactory.Object.Leave);

            _leaveApplicationFactory.Object.DateFrom = DateTime.Now;
            _leaveApplicationFactory.Object.DateTo = DateTime.Now.AddDays(20);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        #region For Regular leave

        [Fact]
        public void Should_Return_Invalid_Regular_leave_Combination_Of_Leave_Application_05()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);
            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            Session.Evict(_leaveApplicationFactory.Object);
            Session.Evict(_leaveApplicationFactory.Object.Leave);

            _leaveApplicationFactory.Object.LeaveStatus = 5;
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_Invalid_Combination_Of_Leave_Application_06()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);
            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            Session.Evict(_leaveApplicationFactory.Object);
            Session.Evict(_leaveApplicationFactory.Object.Leave);

            _leaveApplicationFactory.Object.LeaveStatus = 6;
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_Invalid_SummaryCalculation()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
                _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true);
            Session.Evict(_leaveApplicationFactory.Object);
            Session.Evict(_leaveApplicationFactory.Object.Leave);

            _leaveApplicationFactory.Object.DateFrom = DateTime.Now;
            _leaveApplicationFactory.Object.DateTo = DateTime.Now.AddDays(20);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        #endregion

        #endregion

        #region Validation Test

        [Fact]
        public void Should_Return_EmptyFieldException_For_Leave()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.Leave = null;

            Assert.Throws<EmptyFieldException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_EmptyFieldException_For_TeamMember()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.TeamMember = null;

            Assert.Throws<EmptyFieldException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));

        }

        [Fact]
        public void Should_Return_Date_From_Must_Be_Less_Then_Or_Equal_Date_To_Exception()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.DateFrom = DateTime.Now.AddDays(2);
            _leaveApplicationFactory.Object.DateTo = DateTime.Now.AddDays(1);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_EmptyFieldException_For_LeaveApp_Reason()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.LeaveNote = "";

            Assert.Throws<EmptyFieldException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_Same_Responsible_Person()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_Organization()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _employmentHistoryFactory.Create().WithTeamMember(teamMember).WithCampus(teamMember.EmploymentHistory[0].Campus)
                .WithDepartment(teamMember.EmploymentHistory[0].Department).WithDesignation(teamMember.EmploymentHistory[0].Designation).Persist();

            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.DateFrom = DateTime.Now.AddDays(-60);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_Not_Taking_Leave_From_Current_Organization()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _employmentHistoryFactory.Create().WithTeamMember(teamMember).WithCampus(teamMember.EmploymentHistory[0].Campus)
                .WithDepartment(teamMember.EmploymentHistory[0].Department).WithDesignation(teamMember.EmploymentHistory[0].Designation).Persist();
            teamMember.EmploymentHistory[0].Campus.Branch.Organization = organizationFactory.Create().Persist().Object;
            _leaveApplicationFactory.Create()
                 .WithLeave(leaveList[0])
                 .WithTeamMember(teamMember)
                 .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.CreationDate = DateTime.Now.AddDays(-60);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_Not_Taking_Leave_From_Current_Year_WithoutPay()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                  .WithLeave(leaveList[0])
                  .WithTeamMember(teamMember)
                  .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.Leave.PayType = (int)PayType.WithoutPay;
            _leaveApplicationFactory.Object.DateFrom = new DateTime(2017, 1, 10);
            _leaveApplicationFactory.Object.DateTo = new DateTime(2017, 1, 11);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_Not_Taking_Leave_From_Current_Year_WithtPay()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                  .WithLeave(leaveList[0])
                  .WithTeamMember(teamMember)
                  .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.Leave.PayType = (int)PayType.WithPay;
            _leaveApplicationFactory.Object.DateFrom = new DateTime(2017, 1, 10);
            _leaveApplicationFactory.Object.DateTo = new DateTime(2017, 1, 11);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        #region Pre Post [Responsible Person Check]

        [Fact]
        public void Should_Return_EmptyFieldException_For_Empty_ResponsiblePerson()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                  .WithLeave(leaveList[0])
                  .WithTeamMember(teamMember)
                  .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.Leave.Applicationtype = 1;
            _leaveApplicationFactory.Object.ResponsiblePerson = null;
            Assert.Throws<EmptyFieldException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }

        [Fact]
        public void Should_Return_EmptyFieldException_For_ResponsiblePerson_FutureLeave()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                  .WithLeave(leaveList[0])
                  .WithTeamMember(teamMember)
                  .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.DateFrom = DateTime.Now.AddDays(2);
            _leaveApplicationFactory.Object.Leave.Applicationtype = 2;
            _leaveApplicationFactory.Object.ResponsiblePerson = null;
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        }


        #endregion

        #region Duplicate Leave Check

        [Fact]
        public void Should_Return_Duplicate_Entry_Exception()
        {
            var teamMember = PersistTeamMember();
            var leaveList =
               _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                   teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                   teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationDetailFactory.Create().WithLeaveApplication(_leaveApplicationFactory.Object);
            _leaveApplicationFactory.Object.LeaveApplicationDetail.Add(_leaveApplicationDetailFactory.Object);

            _leaveApplicationService.SaveOrUpdate(new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, true);
            _leaveApplicationFactory.Create()
                .WithLeave(leaveList[0])
                .WithTeamMember(teamMember)
                .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);

            Assert.Throws<DuplicateEntryException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.SingleObjectList[1] }, false, true));
        }

        [Fact]
        public void Should_Return_Invalid_LeaveApplication_In_Holiday()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                  .WithLeave(leaveList[0])
                  .WithTeamMember(teamMember)
                  .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.DateFrom = new DateTime(2016, 12, 1);
            _leaveApplicationFactory.Object.DateTo = new DateTime(2016, 12, 3);
            Assert.Throws<InvalidDataException>(
                  () =>
                      _leaveApplicationService.SaveOrUpdate(
                          new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, false));

        }

        #endregion

        #region Max Taking day Check

        [Fact]
        public void Should_Return_Invalid_Maximum_Taking_Limit()
        {
            var teamMember = PersistTeamMemberForHistory();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                  .WithLeave(leaveList[0])
                  .WithTeamMember(teamMember)
                  .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.DateFrom = DateTime.Now.AddDays(-10);
            _leaveApplicationFactory.Object.DateTo = DateTime.Now.AddDays(10);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, false));
        }

        #endregion

        #region Pre Post [Working Days calculation]

        [Fact]
        public void Should_Return_Apply_Before_Working_Days_Exception()
        {
            var teamMember = PersistTeamMemberForHistory();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                  .WithLeave(leaveList[0])
                  .WithTeamMember(teamMember)
                  .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.DateFrom = DateTime.Now.AddDays(-1);
            _leaveApplicationFactory.Object.DateTo = DateTime.Now;
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, false));
        }

        [Fact]
        public void Should_Return_Apply_After_Working_Days_Exception()
        {
            var teamMember = PersistTeamMember();
            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
                    teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
            _leaveApplicationFactory.Create()
                  .WithLeave(leaveList[0])
                  .WithTeamMember(teamMember)
                  .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
            _leaveApplicationFactory.Object.DateFrom = DateTime.Now.AddDays(1);
            _leaveApplicationFactory.Object.DateTo = DateTime.Now.AddDays(1);
            Assert.Throws<InvalidDataException>(
                () =>
                    _leaveApplicationService.SaveOrUpdate(
                        new List<LeaveApplication>() { _leaveApplicationFactory.Object }, false, false));
        }

        #endregion

        #endregion

        //#region SaveSmsHistory

        //[Fact]
        //public void Should_Return_Invalid_Designation()
        //{
        //    var teamMember = PersistTeamMember();
        //    var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
        //            teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
        //            teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
        //    _leaveApplicationFactory.Create()
        //         .WithLeave(leaveList[0])
        //         .WithTeamMember(teamMember)
        //         .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
        //    teamMember.EmploymentHistory[0].Designation = null;

        //    Assert.Throws<InvalidDataException>(
        //        () =>
        //            _leaveApplicationService.SaveOrUpdate(
        //                new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        //}

        //[Fact]
        //public void Should_Return_Invalid_Department()
        //{
        //    var teamMember = PersistTeamMemberForHistory();
        //    var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
        //            teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
        //            teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
        //    _leaveApplicationFactory.Create()
        //         .WithLeave(leaveList[0])
        //         .WithTeamMember(teamMember)
        //         .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
        //    _leaveApplicationFactory.Object.TeamMember.EmploymentHistory[0].Department = null;

        //    Assert.Throws<InvalidDataException>(
        //        () =>
        //            _leaveApplicationService.SaveOrUpdate(
        //                new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));

        //}

        //[Fact]
        //public void Should_Return_Invalid_Oranization()
        //{
        //    var teamMember = PersistTeamMember();
        //    var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
        //            teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
        //            teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
        //    _leaveApplicationFactory.Create()
        //         .WithLeave(leaveList[0])
        //         .WithTeamMember(teamMember)
        //         .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
        //    _leaveApplicationFactory.Object.TeamMember.EmploymentHistory[0].Campus.Branch.Organization = null;

        //    Assert.Throws<InvalidDataException>(
        //        () =>
        //            _leaveApplicationService.SaveOrUpdate(
        //                new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        //}

        //[Fact]
        //public void Should_Return_Invalid_Responsible_MemDesignation()
        //{
        //    var teamMember = PersistTeamMember();
        //    var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
        //            teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
        //            teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
        //    _leaveApplicationFactory.Create()
        //         .WithLeave(leaveList[0])
        //         .WithTeamMember(teamMember)
        //         .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
        //    _leaveApplicationFactory.Object.ResponsiblePerson.EmploymentHistory[0].Designation = null;

        //    Assert.Throws<InvalidDataException>(
        //        () =>
        //            _leaveApplicationService.SaveOrUpdate(
        //                new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        //}

        //[Fact]
        //public void Should_Return_Invalid_Responsible_MemDepartment()
        //{
        //    var teamMember = PersistTeamMember();
        //    var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
        //            teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
        //            teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
        //    _leaveApplicationFactory.Create()
        //         .WithLeave(leaveList[0])
        //         .WithTeamMember(teamMember)
        //         .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
        //    _leaveApplicationFactory.Object.ResponsiblePerson.EmploymentHistory[0].Department = null;

        //    Assert.Throws<InvalidDataException>(
        //        () =>
        //            _leaveApplicationService.SaveOrUpdate(
        //                new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        //}

        //[Fact]
        //public void Should_Return_Invalid_Responsible_Organization()
        //{
        //    var teamMember = PersistTeamMember();
        //    var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
        //            teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
        //            teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
        //    _leaveApplicationFactory.Create()
        //         .WithLeave(leaveList[0])
        //         .WithTeamMember(teamMember)
        //         .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
        //    _leaveApplicationFactory.Object.ResponsiblePerson.EmploymentHistory[0].Campus.Branch.Organization = null;

        //    Assert.Throws<InvalidDataException>(
        //        () =>
        //            _leaveApplicationService.SaveOrUpdate(
        //                new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        //}

        //[Fact(Skip = "As object pull from db so cwill not arise this case")]
        //public void Should_Return_Invalid_MentorHistory()
        //{

        //}

        //[Fact(Skip = "As object pull from db so cwill not arise this case")]
        //public void Should_Return_Invalid_Mentor_MemDesignation()
        //{
        //    var teamMember = PersistTeamMember();
        //    var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
        //            teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
        //            teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
        //    _leaveApplicationFactory.Create()
        //         .WithLeave(leaveList[0])
        //         .WithTeamMember(teamMember)
        //         .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
        //    _leaveApplicationFactory.Object.TeamMember.MentorHistory[0].Designation = null;

        //    Assert.Throws<InvalidDataException>(
        //        () =>
        //            _leaveApplicationService.SaveOrUpdate(
        //                new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));

        //}

        //[Fact(Skip = "As object pull from db so cwill not arise this case")]
        //public void Should_Return_Invalid_Mentor_Department()
        //{
        //    var teamMember = PersistTeamMember();
        //    var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
        //            teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
        //            teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
        //    _leaveApplicationFactory.Create()
        //         .WithLeave(leaveList[0])
        //         .WithTeamMember(teamMember)
        //         .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
        //    _leaveApplicationFactory.Object.TeamMember.MentorHistory[0].Department = null;

        //    Assert.Throws<InvalidDataException>(
        //        () =>
        //            _leaveApplicationService.SaveOrUpdate(
        //                new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));

        //}

        //[Fact(Skip = "As object pull from db so cwill not arise this case")]
        //public void Should_Return_Invalid_Mentor_Orgqanization()
        //{
        //    var teamMember = PersistTeamMember();
        //    var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
        //            teamMember.EmploymentHistory[0].Campus.Branch.Organization.Id, teamMember.Gender,
        //            teamMember.EmploymentHistory[0].EmploymentStatus, (int)teamMember.MaritalInfo[0].MaritalStatus);
        //    _leaveApplicationFactory.Create()
        //         .WithLeave(leaveList[0])
        //         .WithTeamMember(teamMember)
        //         .WithResposibleTeamMember(teamMember.MentorHistory[0].Mentor);
        //    _leaveApplicationFactory.Object.TeamMember.MentorHistory[0].Organization = null;

        //    Assert.Throws<InvalidDataException>(
        //        () =>
        //            _leaveApplicationService.SaveOrUpdate(
        //                new List<LeaveApplication>() { _leaveApplicationFactory.Object }, true, true));
        //}

        //#endregion
    }


}
