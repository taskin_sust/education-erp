﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public partial class ProgramBranchSessionMap: BaseClassMap<ProgramBranchSession,long>
    {
        public ProgramBranchSessionMap()
        {
            Table("ProgramBranchSession");
            LazyLoad();
            //Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.IsOffice);
            Map(x => x.IsPublic);
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.Branch).Column("BranchId");
        }
    }
}
