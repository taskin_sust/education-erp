using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class AttendanceDeviceErrorPinMap : BaseClassMap<AttendanceDeviceErrorPin,long> {

        public AttendanceDeviceErrorPinMap()
        {
			Table("HR_AttendanceDeviceErrorPin");
			LazyLoad();
			Map(x => x.Pin).Column("Pin");
            Map(x => x.EnrollNo).Column("EnrollNo");
            Map(x => x.LogTime).Column("LogTime");
            Map(x => x.RequestIp).Column("RequestIp");
                        
            References(x => x.AttendanceDevice).Column("AttendanceDeviceId");
        }
    }
}
