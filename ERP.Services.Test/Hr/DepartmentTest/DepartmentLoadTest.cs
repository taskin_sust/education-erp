﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DepartmentTest
{
    public interface IDepartmentLoadTest
    {
        #region LoadById

        void GetDepartment_Successfull();
        void GetDepartment_Null_Value_Check();

        #endregion

        #region LoadActive

        void LoadActive_Should_Return_List();

        #endregion
    }


    [Trait("Area", "Hr")]
    [Trait("Service", "Department")]
    public class DepartmentLoadTest : DepartmentBaseTest, IDepartmentLoadTest
    {
        #region LoadById

        [Fact]
        public void GetDepartment_Successfull()
        {
            _departmentFactory.Create().WithOrganization();
            _departmentService.Save(_departmentFactory.Object);
            var returnDepartment = _departmentService.LoadById(_departmentFactory.Object.Id);
            Assert.NotNull(returnDepartment);
        }

        [Fact]
        public void GetDepartment_Null_Value_Check()
        {
            var returnDepartment = _departmentService.LoadById(-1);
            Assert.Null(returnDepartment);
        }

        #endregion

        #region LoadActive

        [Fact]
        public void LoadActive_Should_Return_List()
        {
            _departmentFactory.CreateMore(10).Persist();
            var dept = _departmentFactory.GetLastCreatedObjectList()[0];
            var organization = dept.Organization;
            var usermenu = BuildUserMenu(organization, program: null, branch: null);
            IList<Department> departmentList = _departmentService.LoadActive(usermenu, 0, 10, "", "", "", "");
            Assert.Equal(10, departmentList.Count);   
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_Usermenu()
        {
            Assert.Throws<InvalidDataException>(() => _departmentService.LoadActive(null, 0, 10, "", "", "", ""));
        }

        #endregion

        #region LoadAuthorizedDepartment

        [Fact]
        public void Should_Return_LoadAuthorizedDepartment_InvalidException_For_Usermenu()
        {
            Assert.Throws<InvalidDataException>(() => _departmentService.LoadAuthorizedDepartment(null,new List<long>() {}));
        }

        [Fact]
        public void Should_Return_LoadAuthorizedDepartment_Successfully()
        {
            _departmentFactory.CreateMore(10).Persist();
            var dept = _departmentFactory.GetLastCreatedObjectList()[0];
            var organization = dept.Organization;
            var usermenu = BuildUserMenu(organization, program: null, branch: null);
            IList<Department> departmentList = _departmentService.LoadAuthorizedDepartment(usermenu,new List<long>(){organization.Id});
            Assert.Equal(10, departmentList.Count);   
        }

        #endregion

        #region TotalRowCountOfDepartmentSettings

        [Fact]
        public void Should_Return_TotalRowCountOfDepartmentSettings_InvalidException_For_Usermenu()
        {
            Assert.Throws<InvalidDataException>(() => _departmentService.TotalRowCountOfDepartmentSettings(null, "",""));
        }

        [Fact]
        public void Should_Return_TotalRowCountOfDepartmentSettings_Successfully()
        {
            _departmentFactory.CreateMore(10).Persist();
            var dept = _departmentFactory.GetLastCreatedObjectList()[0];
            var organization = dept.Organization;
            var usermenu = BuildUserMenu(organization, program: null, branch: null);
            var departmentListCount = _departmentService.TotalRowCountOfDepartmentSettings(usermenu, organization.Id.ToString(), "");
            Assert.Equal(10, departmentListCount);   
        }

        #endregion

    }
}
