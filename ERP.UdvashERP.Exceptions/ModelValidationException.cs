﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.MessageExceptions
{
    public class ModelValidationException : Exception
    {
        public ModelValidationException()
            : base("Message Data")
        {

        }

        public ModelValidationException(string message)
            : base(message)
        {

        }

    }

}
