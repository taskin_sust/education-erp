﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.JobExperienceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "JobExperience")]
    public class JobExperienceUpdateTest : JobExperienceBaseTest
    {
        #region Basic Test

        [Fact]
        public void JobExperience_NewAndOld_Object_UpdateReturnTrue()
        {
            var teamMemberObj = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            var ids = jobExperienceService.UpdateJobExperienceInfo(teamMemberObj, objFactory.SingleObjectList);
            IList<JobExperience> oldObject = jobExperienceService.LoadMemberJobExperienceInfo(teamMemberObj.Id);
            foreach (var jobExperience in oldObject)
            {
                jobExperience.Name = "New TM Name " + jobExperience.Id;
            }
            var oldObjIds = jobExperienceService.UpdateJobExperienceInfo(teamMemberObj, oldObject.ToList());

            Assert.NotNull(objFactory.SingleObjectList[0].Id);
            Assert.NotEmpty(ids);
            Assert.NotEqual(ids.Count, 0);
            Assert.NotEmpty(oldObjIds);
            Assert.NotEqual(oldObjIds.Count, 0);
        }

        #endregion

        #region Validation check

        [Fact]
        public void JobExperience_Update_CompanyName_Empty()
        {
            var teamMember = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            objFactory.Object.CompanyName = "";
            Exception ex = Assert.Throws<NullObjectException>(() => jobExperienceService.UpdateJobExperienceInfo(teamMember, objFactory.SingleObjectList));
            Assert.Equal(ex.Message, CompanyNameNullError);
        }

        [Fact]
        public void JobExperience_Update_CompanyType_Null()
        {
            var teamMember = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            objFactory.Object.CompanyType = null;
            Exception ex = Assert.Throws<NullObjectException>(() => jobExperienceService.UpdateJobExperienceInfo(teamMember, objFactory.SingleObjectList));
            Assert.Equal(ex.Message, CompanyTypeNullError);
        }

        [Fact]
        public void JobExperience_Update_CompanyType_Invalid()
        {
            var teamMember = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            objFactory.Object.CompanyType = 0;
            Exception ex = Assert.Throws<NullObjectException>(() => jobExperienceService.UpdateJobExperienceInfo(teamMember, objFactory.SingleObjectList));
            Assert.Equal(ex.Message, CompanyTypeNullError);
        }

        [Fact]
        public void JobExperience_Update_CompanyLocation_Empty()
        {
            var teamMember = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            objFactory.Object.CompanyLocation = "";
            Exception ex = Assert.Throws<NullObjectException>(() => jobExperienceService.UpdateJobExperienceInfo(teamMember, objFactory.SingleObjectList));
            Assert.Equal(ex.Message, CompanyLocationNullError);
        }

        [Fact]
        public void JobExperience_Update_DepartmentName_Empty()
        {
            var teamMember = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            objFactory.Object.DepartmentName = "";
            Exception ex = Assert.Throws<NullObjectException>(() => jobExperienceService.UpdateJobExperienceInfo(teamMember, objFactory.SingleObjectList));
            Assert.Equal(ex.Message, DepartmentNameNullError);
        }

        [Fact]
        public void JobExperience_Update_ExperienceArea_Empty()
        {
            var teamMember = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            objFactory.Object.ExperienceArea = "";
            Exception ex = Assert.Throws<NullObjectException>(() => jobExperienceService.UpdateJobExperienceInfo(teamMember, objFactory.SingleObjectList));
            Assert.Equal(ex.Message, ExperienceAreaNullError);
        }

        [Fact]
        public void JobExperience_Update_Position_Empty()
        {
            var teamMember = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            objFactory.Object.Position = "";
            Exception ex = Assert.Throws<NullObjectException>(() => jobExperienceService.UpdateJobExperienceInfo(teamMember, objFactory.SingleObjectList));
            Assert.Equal(ex.Message, JobPositionNullError);
        }

        [Fact]
        public void JobExperience_Update_Member_Null()
        {
            var objFactory = jobExperienceFactory.Create();
            Exception ex = Assert.Throws<NullObjectException>(() => jobExperienceService.UpdateJobExperienceInfo(null, objFactory.SingleObjectList));
            Assert.Equal(ex.Message, MemberEntityNotFound);
        }

        [Fact]
        public void JobExperience_Update_Object_Null()
        {
            var teamMember = PersistTeamMember();
            Exception ex = Assert.Throws<NullObjectException>(() => jobExperienceService.UpdateJobExperienceInfo(teamMember, null));
            Assert.Equal(ex.Message, NullObjectError);
        }

        [Fact]
        public void JobExperience_Update_Duration_Empty()
        {
            var teamMember = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            objFactory.Object.Duration = "";
            Exception ex = Assert.Throws<NullObjectException>(() => jobExperienceService.UpdateJobExperienceInfo(teamMember, objFactory.SingleObjectList));
            Assert.Equal(ex.Message, JobDurationNullError);
        }

        #endregion

    }
}
