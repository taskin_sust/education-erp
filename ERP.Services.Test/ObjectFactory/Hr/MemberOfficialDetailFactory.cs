﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class MemberOfficialDetailFactory : ObjectFactoryBase<MemberOfficialDetail>
    {
        private TeamMemberFactory _teamMemberFactory;
        private MemberOfficialDetailService _memberOfficialDetailService;
        private readonly TestBaseService<MemberOfficialDetailLog> _memberOfficialDetailLogService;
        public MemberOfficialDetailFactory(IObjectFactoryBase caller, ISession session)
            : base(caller,session)
        {
            _teamMemberFactory = new TeamMemberFactory(this,session);
            _memberOfficialDetailService = new MemberOfficialDetailService(session);
            _memberOfficialDetailLogService = new TestBaseService<MemberOfficialDetailLog>(session);
        }

        internal string GetNewCardNumber()
        {
            return _memberOfficialDetailService.GetNewCardNumber();
        }
        internal string GetRandomNumber()
        {
            var r = new Random();
            var x = r.Next(0, 1000000);
            var s = x.ToString("000000");
            return s;
        }
        public MemberOfficialDetailFactory Create()
        {
            Object = new MemberOfficialDetail()
            {
                CardNo = GetNewCardNumber(),
                OfficialContact = "8801841" + GetRandomNumber(),
                OfficialEmail = "a@b.com"
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public MemberOfficialDetailFactory CreateMore(int num)
        {
            for (int i = 0; i < num; i++)
            {
                var _object = new MemberOfficialDetail()
                {
                    CardNo = GetNewCardNumber(),
                    OfficialContact = "8801841" + GetRandomNumber(),
                    OfficialEmail = "a@b.com"
                };
                ObjectList.Add(_object);
            }
            return this;
        }

        public MemberOfficialDetailFactory CreateWith(string cardNo,string officialContact,string officialEmail)
        {
            Object = new MemberOfficialDetail()
            {
                CardNo = cardNo,
                OfficialContact = officialContact,
                OfficialEmail = officialEmail
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberOfficialDetailFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }

        public void LogCleanUp(TeamMember member)
        {
            //_memberOfficialDetailLogService.DeleteMemberOfficialDetailsLogByMemberOfficialDetailIdList(member);
            var list = member.MemberOfficialDetails.Select(x => x.Id).ToList();
            var logHistoryIdList =
               _session.QueryOver<MemberOfficialDetailLog>()
                // .Select(x => x.Id)
                   .Where(x => x.MemberOfficialDetailId.IsIn(list.ToArray()) || x.MemberOfficialDetailId == null)
                   .List<MemberOfficialDetailLog>();
            if (logHistoryIdList.Count > 0)
            {
                foreach (var modl in logHistoryIdList)
                {
                    string a = "DELETE FROM [HR_MemberOfficialDetailLog] WHERE [Id] = " + modl.Id + ";";
                    _session.CreateSQLQuery(a).ExecuteUpdate();
                }
                //dao.Session.Flush();
                //string a = "DELETE FROM [HR_MemberOfficialDetailLog] WHERE [Id] IN (" + string.Join(",", logHistoryIdList) + "); ";
                //dao.Session.CreateSQLQuery(a).ExecuteUpdate();
                //dao.Session.Flush();
            }
        }
    }
}
