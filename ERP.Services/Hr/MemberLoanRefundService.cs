using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
using UdvashERP.MessageExceptions;
using NHibernate.Criterion;
using NHibernate.Transform;

namespace UdvashERP.Services.Hr
{
    public interface IMemberLoanRefundService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate( List<UserMenu> userMenus,MemberLoanRefund memberLoanRefund);

        #endregion

        #region Single Instances Loading Function

        MemberLoanAmountForRefundView GetLoanBalance(long teamMemberId);
        MemberLoanRefund GetMemberLoanRefund(long id);

        #endregion

        #region List Loading Function

        IList<MemberLoanRefund> LoadMemberLoanRefund(List<UserMenu> userMenus, int start, int length, string orderBy, string orderDir, int? pin, string name, string receiptNo);

        #endregion

        #region Others Function

        int LoadMemberLoanRefundCount(List<UserMenu> userMenus, int start, int length, string orderBy, string orderDir, int? pin, string name, string receiptNo);

        #endregion

        #region Helper Function

        #endregion     
    }
    public class MemberLoanRefundService : BaseService, IMemberLoanRefundService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("PayrollService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IMemberLoanRefundDao _memberLoanRefundDao;
        private ITeamMemberDao _teamMemberDao;
        public MemberLoanRefundService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _memberLoanRefundDao = new MemberLoanRefundDao { Session = session };
            _teamMemberDao = new TeamMemberDao() {Session = session};
        }
        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(List<UserMenu> userMenus, MemberLoanRefund memberLoanRefund)
        {
            bool returnVal=false;
            ITransaction trans = null;
            try
            {

                List<long> authOrganizationIds = AuthHelper.LoadOrganizationIdList(userMenus);
                List<long> authBranchIds = AuthHelper.LoadBranchIdList(userMenus, authOrganizationIds);

                var authorizeTeamMember = _teamMemberDao.LoadHrAuthorizedTeamMember(null, authBranchIds,_commonHelper.ConvertIdToList(memberLoanRefund.SalaryCampus.Id), null, null,_commonHelper.ConvertIdToList(memberLoanRefund.TeamMember.Pin), false);

                if (authorizeTeamMember == null)
                {
                    throw new NullObjectException("Invalid Team Member");
                }

                var previousLoan = _memberLoanRefundDao.GetLoanBalance(authorizeTeamMember[0].Id);
                if (memberLoanRefund.RefundAmount == 0)
                {
                    throw new InvalidDataException("Return amount can't be zero");
                }

                if (memberLoanRefund.RefundAmount > previousLoan.LoanBalance)
                {
                    throw new InvalidDataException("Return amount can't be greater with loan balance");
                }

                #region salary history

                var organization = _teamMemberDao.GetTeamMemberSalaryOrganization(DateTime.Now,authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);
                var branch = _teamMemberDao.GetTeamMemberSalaryBranch(DateTime.Now, authorizeTeamMember[0].Id,authorizeTeamMember[0].Pin);
                var campus = _teamMemberDao.GetTeamMemberSalaryCampus(DateTime.Now, authorizeTeamMember[0].Id,authorizeTeamMember[0].Pin);
               // var department = _teamMemberDao.GetTeamMemberSalaryDepartment(DateTime.Now, authorizeTeamMember[0].Id,authorizeTeamMember[0].Pin);
                #endregion

                using (trans = Session.BeginTransaction())
                {
                    var latestReceiptNo = _memberLoanRefundDao.GetMaxReceiptNo(organization.Id).ToString();
                    memberLoanRefund.ReceiptNo =latestReceiptNo.ToString().PadLeft(10,'0') ;
                    memberLoanRefund.TeamMember = authorizeTeamMember[0];
                    memberLoanRefund.SalaryOrganization = organization;
                    memberLoanRefund.SalaryBranch = branch;
                    memberLoanRefund.SalaryCampus = campus;
                    memberLoanRefund.RefundDate = DateTime.Now;
                    memberLoanRefund.LoanAmount = previousLoan.LoanApprovedAmount;
                    memberLoanRefund.DueAmount = previousLoan.LoanBalance - memberLoanRefund.RefundAmount;

                   _memberLoanRefundDao.Save(memberLoanRefund);
                    trans.Commit();
                    returnVal = true;
                }
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return returnVal;
        }

        #endregion

        #region Single Instances Loading Function

        public MemberLoanAmountForRefundView GetLoanBalance(long teamMemberId)
        {
            try
            {
                return _memberLoanRefundDao.GetLoanBalance(teamMemberId);
            }
            catch (Exception)
            {
                
                throw;
            }
        }


        public MemberLoanRefund GetMemberLoanRefund(long id)
        {
            try
            {
                return _memberLoanRefundDao.LoadById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }


        #endregion

        #region List Loading Function

        public IList<MemberLoanRefund> LoadMemberLoanRefund(List<UserMenu> userMenus, int start, int length, string orderBy, string orderDir, int? pin, string name, string receiptNo)
        {
            return _memberLoanRefundDao.LoadMemberLoanRefund(start,length,orderBy, orderDir, pin, name, receiptNo);
           
        }

        #endregion

        #region Others Function


        #endregion

        #region Helper function
        public int LoadMemberLoanRefundCount(List<UserMenu> userMenus, int start, int length, string orderBy, string orderDir, int? pin, string name, string receiptNo)
        {
            return _memberLoanRefundDao.LoadMemberLoanRefundCount(start,length,orderBy, orderDir, pin, name, receiptNo);
        }
       
        #endregion
    }
}

