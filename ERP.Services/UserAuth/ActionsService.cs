﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.UserAuth
{
    public interface IActionsService : IBaseService
    {
        #region Operational Function
        void Save(Actions erpActions);
        bool Update(long id, Actions erpActions);
        bool Delete(long id, Actions actionsObj);
        #endregion

        #region Single Instances Loading Function
        Actions LoadByActionsNameAndController(string action, AreaControllers areaController);
        Actions LoadById(long id);
        #endregion

        #region List Loading Function
        IList<Actions> LoadActiveByControllers(long controllersId);
        IList<Actions> LoadActive(int start, int length, string orderBy, string orderDir, string name, string controllersId, string status, string rank);
        IList<Actions> LoadActive(bool eagerLoadController = false);
        #endregion

        #region Others Function
        int GetMaximumRank(Actions actionsObj);
        int ActionsRowCount(string name, string controllersId, string status, string rank);
        #endregion
        
    }
    public class ActionsService : BaseService, IActionsService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("UserAuthService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IActionsDao _actionsDao;
        private readonly IAreaControllersDao _areaControllersDao;

        public ActionsService(ISession session)
        {
            Session = session;
            _actionsDao = new ActionsDao() { Session = Session };
        }
        #endregion

        #region Operational Function
        private void CheckBeforeSave(Actions erpActions)
        {
            //check duplicate name
            CheckDuplicateName(erpActions);
        }

        private void CheckDuplicateName(Actions erpActions)
        {
            if (_actionsDao.HasDuplicateByName(erpActions.Name, erpActions.AreaControllers) == true)
                throw new DuplicateEntryException("Duplicate Actions found");
        }

        public void Save(Actions erpActions)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {

                    CheckEmptyField(erpActions);
                    CheckBeforeSave(erpActions);
                    erpActions.Rank = _actionsDao.GetMaximumRank(erpActions) + 1;
                    _actionsDao.Save(erpActions);
                    transaction.Commit();
                }

            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            catch (ArgumentNullException ex)
            {
                throw;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        private void CheckBeforeUpdate(Actions erpActions)
        {
            //check duplicate
            if (_actionsDao.HasDuplicateByName(erpActions.Name, erpActions.AreaControllers, erpActions.Id))
                throw new DuplicateEntryException("Duplicate Actions found");
        }

        public bool Update(long id, Actions actionsObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    if (actionsObj == null)
                    {
                        throw new NullObjectException("action can not be null");
                    }
                    var tempActions = _actionsDao.LoadById(id);
                    tempActions.Id = id;
                    tempActions.Name = actionsObj.Name;
                    tempActions.AreaControllers = actionsObj.AreaControllers;
                    CheckEmptyField(tempActions);
                    CheckBeforeUpdate(tempActions);
                    _actionsDao.Update(tempActions);
                    transaction.Commit();
                    return true;
                }
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();

            }
            return false;
        }
        public bool Delete(long id, Actions actionsObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    var tempActions = _actionsDao.LoadById(id);
                    //var tempControllers = _areaControllersDao.LoadById(tempActions.AreaControllers.Id);
                    //actionsObj.Id = tempActions.Id;
                    //actionsObj.Name = tempActions.Name;
                    //actionsObj.AreaControllers = actionsObj.AreaControllers;
                    //tempActions.Status = Actions.EntityStatus.Delete;
                    _actionsDao.Delete(actionsObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();

            }
            return false;
        }
        #endregion

        #region Single Instances Loading Function
        public Actions LoadByRank(int rank)
        {
            try
            {
                return _actionsDao.LoadByRank(rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ;
            }
            
        }
        public Actions LoadByActionsNameAndController(string action, AreaControllers areaController)
        {
            try
            {
                return _actionsDao.LoadByActionsNameAndController(action, areaController);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        public Actions LoadById(long id)
        {
            try
            {
                return _actionsDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        #endregion

        #region List Loading Function
        public IList<Actions> LoadActive(bool eagerLoadController = false)
        {
            try
            {
                return _actionsDao.LoadActive(eagerLoadController);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public IList<Actions> LoadActive(int start, int length, string orderBy, string orderDir, string name, string controllersId, string status, string rank)
        {
            try
            {
                return _actionsDao.LoadActive(start, length, orderBy, orderDir, name, controllersId, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public IList<Actions> LoadActiveByControllers(long controllersId)
        {
            try
            {
                return _actionsDao.LoadActiveByControllers(controllersId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        #endregion

        #region Others Function
        private void CheckEmptyField(Actions erpActions)
        {
            if (erpActions == null) throw new ArgumentNullException("erpActions");
            if (string.IsNullOrEmpty(erpActions.Name.Trim()))
                throw new EmptyFieldException("Actions name in empty");
            if (erpActions.AreaControllers==null)
                throw new EmptyFieldException("Actions name in empty");

        }
        public int ActionsRowCount(string name, string controllers, string status, string rank)
        {
            try
            {
                return _actionsDao.ActionsRowCount(name, controllers, status, rank);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        public int GetMaximumRank(Actions actionsObj)
        {
            try
            {
                return _actionsDao.GetMaximumRank(actionsObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        #endregion
        
    }
}
