﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.JobExperienceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "JobExperience")]
    public class JobExperienceLoadTest : JobExperienceBaseTest
    {
        [Fact]
        public void LoadById_Not_Null_Check()
        {
            var teamMemberObj = PersistTeamMember();
            var objFactory = jobExperienceFactory.Create();
            jobExperienceService.UpdateJobExperienceInfo(teamMemberObj, objFactory.SingleObjectList);
            IList<JobExperience> jobExperiencesObj = jobExperienceService.LoadMemberJobExperienceInfo(teamMemberObj.Id);

            Assert.NotNull(jobExperiencesObj);
            Assert.NotEqual(jobExperiencesObj.Count, 0);
        }

        [Fact]
        public void LoadById_Null_Check()
        {
            var teamMemberObj = PersistTeamMember();
            IList<JobExperience> jobExperiencesObj = jobExperienceService.LoadMemberJobExperienceInfo(teamMemberObj.Id);
            Assert.Null(jobExperiencesObj);
        }
        
    }
}
