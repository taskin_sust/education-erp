﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernate.Criterion;
using UdvashERP.Dao.Administration;
using UdvashERP.Model.Entity.Administration;
using UdvashERP.Test.SessionFactory;

namespace UdvashERP.Test.Administration
{
    [TestClass]
    public class TestSessionDao
    {
        #region Propertise & Object Initialization
        private readonly ISessionDao _sessionDao;
        public TestSessionDao()
        {
            _sessionDao = new SessionDao() { Session = NHibernateSessionFactory.OpenSession() };
        }
        #endregion

        #region LoadActiveTest
        /// <summary>
        /// Test Case: Check Session Lists Object
        /// </summary>
        [TestMethod]
        public void LoadActiveTestForSessionObject()
        {
            var sessionLists = _sessionDao.LoadSession();
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>), "Returning object is must be a instance of IList Session.");
        }
        /// <summary>
        /// Test Case : Checking Any Result
        /// </summary>
        [TestMethod]
        public void LoadActiveTestForAnyResult()
        {
            IList<Session> sessions = _sessionDao.LoadSession();
            int noOfRow = sessions.Count;
            Assert.IsTrue(noOfRow > 0, "session  must be contained one or more data");
        }

        /// <summary>
        /// Test Case : Checking Actual Result Based On Database : 9
        /// </summary>
        [TestMethod]
        public void LoadActiveTestForActualResult()
        {
            const int actualResultFromDataBase = 9;
            IList<Session> sessions = _sessionDao.LoadSession();
            int noOfRow = sessions.Count;
            Assert.AreEqual(noOfRow, actualResultFromDataBase, "session  must be contained 9 data");
        }
        #endregion

        #region LoadAuthorizedSession
        /// <summary>
        /// Test Case: branchIdList = null, programIdList = null, programId = null, Expected Result 8 from Database
        /// </summary>
        [TestMethod]
        public void LoadAuthorizedSessionTestForAllNullparams()
        {
            var authorizedSession = _sessionDao.LoadAuthorizedSession(null, null, null);
            int numberOfRow = authorizedSession.Count;
            const int actualAuthorizedSessionRow = 8;
            Assert.IsInstanceOfType(authorizedSession,typeof(IList<Session>));
            Assert.IsTrue(numberOfRow > 0, "Must Return some session rows");
            Assert.AreEqual(numberOfRow, actualAuthorizedSessionRow, "Must Return some session rows");
        }
        /// <summary>
        /// Test Case: branchIdList = null, programIdList = null, programId = 68, Expected Result 3 from Database
        /// </summary>
        [TestMethod]
        public void LoadAuthorizedSessionTestForSpecificProgram()
        {
            const int programId = 68;
            const int actualAuthorizedSessionRow = 3;
            var authorizedSession = _sessionDao.LoadAuthorizedSession(null, null, programId);
            int numberOfRow = authorizedSession.Count;
            Assert.IsInstanceOfType(authorizedSession, typeof(IList<Session>));
            Assert.IsTrue(numberOfRow > 0, "Must Return some session rows");
            Assert.AreEqual(numberOfRow, actualAuthorizedSessionRow, "Must Return some session rows");
         }
        /// <summary>
        /// Test Case: branchIdList = null, programIdList = { 67, 69 }, programId = null
        /// </summary>
        [TestMethod]
        public void LoadAuthorizedSessionTestForAllBranchsSomePrograms()
        {
            var programIdList = new List<long>() { 67, 69 };
            const int actualAuthorizedSessionRow = 0;
            var authorizedSession = _sessionDao.LoadAuthorizedSession(null, programIdList,null);
            int numberOfRow = authorizedSession.Count;
            Assert.IsInstanceOfType(authorizedSession, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualAuthorizedSessionRow);
        }
        /// <summary>
        /// Test Case: branchIdList = {78,82}, programIdList = null, programId = null
        /// </summary>
        [TestMethod]
        public void LoadAuthorizedSessionTestForSomeBranchsAllPrograms()
        {
            var branchIdList = new List<long>() { 78, 82 };
            const int actualAuthorizedSessionRow = 3;
            var authorizedSession = _sessionDao.LoadAuthorizedSession(branchIdList,null,null);
            int numberOfRow = authorizedSession.Count;
            Assert.IsInstanceOfType(authorizedSession, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualAuthorizedSessionRow, "Must Return some session rows");
        }
        /// <summary>
        /// Test Case: branchIdList = {78,82}, programIdList = { 67, 69 }, programId = null
        /// </summary>
        [TestMethod]
        public void LoadAuthorizedSessionTestForSomeBranchsSomePrograms()
        {
            var branchIdList = new List<long>() { 78, 82 };
            var programIdList = new List<long>() { 67, 69 };
            const int actualAuthorizedSessionRow = 0;
            var authorizedSession = _sessionDao.LoadAuthorizedSession(branchIdList, programIdList, null);
            int numberOfRow = authorizedSession.Count;
            Assert.IsInstanceOfType(authorizedSession, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualAuthorizedSessionRow);
        }

        /// <summary>
        /// Test Case: branchIdList = {78}, programIdList = { 67 }, programId = null
        /// </summary>
        [TestMethod]
        public void LoadAuthorizedSessionTestForSingleBranchSingleProgram()
        {
            var branchIdList = new List<long>() { 78};
            var programIdList = new List<long>() { 67};
            const int actualAuthorizedSessionRow = 0;
            var authorizedSession = _sessionDao.LoadAuthorizedSession(branchIdList, programIdList, null);
            int numberOfRow = authorizedSession.Count;
            Assert.IsInstanceOfType(authorizedSession, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualAuthorizedSessionRow);
        }
        /// <summary>
        /// Test Case: branchIdList = null, programIdList = { 67, 69 }, programId = 68
        /// </summary>
        [TestMethod]
        public void LoadAuthorizedSessionTestForAllBranchSomeProgramSpecificProgram()
        {
            const int programId = 68;
            var programIdList = new List<long>() { 67, 69 };
            const int actualAuthorizedSessionRow = 0;
            var authorizedSession = _sessionDao.LoadAuthorizedSession(null, programIdList, programId);
            int numberOfRow = authorizedSession.Count;
            Assert.IsInstanceOfType(authorizedSession, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualAuthorizedSessionRow);
        }
        /// <summary>
        /// Test Case: branchIdList = {78,82}, programIdList = null, programId = 68
        /// </summary>
        [TestMethod]
        public void LoadAuthorizedSessionTestForSomeBranchsAllProgramsSpecificProgram()
        {
            const int programId = 68;
            var branchIdList = new List<long>() { 78, 82 };
            const int actualAuthorizedSessionRow = 3;
            var authorizedSession = _sessionDao.LoadAuthorizedSession(branchIdList, null, programId);
            int numberOfRow = authorizedSession.Count;
            Assert.IsInstanceOfType(authorizedSession, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualAuthorizedSessionRow);
        }
        /// <summary>
        /// Test Case: branchIdList = {78,82}, programIdList = { 67, 69 }, programId = 68
        /// </summary>
        [TestMethod]
        public void LoadAuthorizedSessionTestForSomeBranchsSomeProgramsSpecificProgram()
        {
            const int programId = 68;
            var branchIdList = new List<long>() { 78, 82 };
            var programIdList = new List<long>() { 67, 69 };
            const int actualAuthorizedSessionRow = 0;
            var authorizedSession = _sessionDao.LoadAuthorizedSession(branchIdList, programIdList, programId);
            int numberOfRow = authorizedSession.Count;
            Assert.IsInstanceOfType(authorizedSession, typeof(IList<Session>));
            Assert.IsTrue(numberOfRow == 0) ;
            Assert.AreEqual(numberOfRow, actualAuthorizedSessionRow);
        }
        /// <summary>
        /// Test Case: branchIdList = {78,82}, programIdList = { 67, 69 }, programId = 67
        /// </summary>
        [TestMethod]
        public void LoadAuthorizedSessionTestForSingleBranchSingleProgramSpecific()
        {
            const int programId = 67;
            var branchIdList = new List<long>() { 78 };
            var programIdList = new List<long>() { 67 };
            const int actualAuthorizedSessionRow = 0;
            var authorizedSession = _sessionDao.LoadAuthorizedSession(branchIdList, programIdList, programId);
            int numberOfRow = authorizedSession.Count;
            Assert.IsInstanceOfType(authorizedSession, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualAuthorizedSessionRow);
        }
        #endregion

        #region LoadSessionForList
        /// <summary>
        /// Test Case: int start = 0, int length = 1000, string orderBy = "", string orderDir="", string name="", string code="", string rank="", string status=""
        /// </summary>
        [TestMethod]
        public void LoadSessionForListTestForAllDefualtParams()
        {
            const int start = 0;
            const int length = 1000;
            const string orderBy = "";
            const string orderDir = "";
            const string name = "";
            const string code = "";
            const string rank = "";
            const string status = "";
            var sessionLists = _sessionDao.LoadSession(start, length , orderBy, orderDir, name, code, rank, status);
            var numberOfRow = sessionLists.Count;
            const int actualSession = 10;
            Assert.IsInstanceOfType(sessionLists,typeof(IList<Session>));
            Assert.AreEqual(numberOfRow,actualSession);
        }
        /// <summary>
        /// Test Case: int start = 0, int length = 5, string orderBy = "", string orderDir="", string name="", string code="", string rank="", string status=""
        /// </summary>
        [TestMethod]
        public void LoadSessionForListTestForOnlyFiveRow()
        {
            const int start = 0;
            const int length = 5;
            const string orderBy = "";
            const string orderDir = "";
            const string name = "";
            const string code = "";
            const string rank = "";
            const string status = "";
            var sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            var numberOfRow = sessionLists.Count;
            const int actualSession = 5;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);
        }
        /// <summary>
        /// Test Case: int start = 0, int length = 1000, string orderBy = "", string orderDir="", string name="", string code="", string rank="", string status=""
        /// </summary>
        [TestMethod]
        public void LoadSessionForListTestForOrderByAscDesc()
        {
            const int start = 0;
            const int length = 1000;
            string orderBy = "Rank";
            string orderDir = "ASC";
            const string name = "";
            const string code = "";
            const string rank = "";
            const string status = "";
            var sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            var numberOfRow = sessionLists.Count;
            const int actualSession = 10;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);
            //Rank DESC
            orderDir = "DESC";
            sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            numberOfRow = sessionLists.Count;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);
            //Name Asc
            orderBy = "Name";
            orderDir = "ASC";
            sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            numberOfRow = sessionLists.Count;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);
            //DESC
            orderDir = "DESC";
            sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            numberOfRow = sessionLists.Count;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);
            
            //Code Asc
            orderBy = "Code";
            orderDir = "ASC";
            sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            numberOfRow = sessionLists.Count;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);
            //DESC
            orderDir = "DESC";
            sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            numberOfRow = sessionLists.Count;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);

            //Status Asc
            orderBy = "Status";
            orderDir = "ASC";
            sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            numberOfRow = sessionLists.Count;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);
            //DESC
            orderDir = "DESC";
            sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            numberOfRow = sessionLists.Count;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);
        }
        /// <summary>
        /// Test Case: int start = 0, int length = 1000, string orderBy = "", string orderDir="", string name="", string code="", string rank="", string status=""
        /// </summary>
        [TestMethod]
        public void LoadSessionForListTestForNameSearch()
        {
            const int start = 0;
            const int length = 1000;
            const string orderBy = "Rank";
            const string orderDir = "ASC";
            string name = "   ";
            const string code = "";
            const string rank = "";
            const string status = "";
            var sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            var numberOfRow = sessionLists.Count;
            int actualSession = 0;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);

            name = "1";
            sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            numberOfRow = sessionLists.Count;
            actualSession = 7;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);

            name = "12";
            sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            numberOfRow = sessionLists.Count;
            actualSession = 0;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);
        }
        /// <summary>
        /// Test Case: int start = 0, int length = 1000, string orderBy = "", string orderDir="", string name="", string code="", string rank="", string status=""
        /// </summary>
        [TestMethod]
        public void LoadSessionForListTestForNameCode()
        {
            const int start = 0;
            const int length = 1000;
            const string orderBy = "Rank";
            const string orderDir = "ASC";
            const string name = "";
            string code = "  ";
            const string rank = "";
            const string status = "";
            var sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            var numberOfRow = sessionLists.Count;
            int actualSession = 0;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);

            code = "1";
            sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            numberOfRow = sessionLists.Count;
            actualSession = 8;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);

            code = "12";
            sessionLists = _sessionDao.LoadSession(start, length, orderBy, orderDir, name, code, rank, status);
            numberOfRow = sessionLists.Count;
            actualSession = 0;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualSession);
        }
        #endregion

        #region LoadPublicSessionByProgram
        /// <summary>
        /// Test Case: string orgBusinessId = null, long programId=68
        /// </summary>
        [TestMethod]
        public void LoadPublicSessionByProgramTestForOrganizationBussinessIdNull()
        {
            const long programId = 68;
            var sessionLists = _sessionDao.LoadPublicSessionByProgram(null,programId);
            const int actualNumberOfRow = 0;
            int numberOfRow = sessionLists.Count;
            Assert.IsInstanceOfType(sessionLists,typeof(IList<Session>));
            Assert.AreEqual(numberOfRow,actualNumberOfRow);
        }
        /// <summary>
        /// Test Case: string orgBusinessId = udvash, long programId=68
        /// </summary>
        [TestMethod]
        public void LoadPublicSessionByProgramTestForOrganizationBussinessId()
        {
            const string orgBusinessId = "udvash";
            const long programId = 68;
            var sessionLists = _sessionDao.LoadPublicSessionByProgram(orgBusinessId, programId);
            const int actualNumberOfRow = 2;
            int numberOfRow = sessionLists.Count;
            Assert.IsInstanceOfType(sessionLists, typeof(IList<Session>));
            Assert.AreEqual(numberOfRow, actualNumberOfRow);
        }

        #endregion

        #region HasDuplicateByName
        /// <summary>
        /// Test Case string name = ""
        /// </summary>
        [TestMethod]
        public void HasDuplicateByNameTestForEmptyName()
        {
            const string name = " ";
            var result = _sessionDao.HasDuplicateByName(name);
            const bool actualResult = false;
            Assert.IsInstanceOfType(result,typeof(bool));
            Assert.AreEqual(result,actualResult);
        }
        /// <summary>
        /// Test Case string name = "2099"
        /// </summary>
        [TestMethod]
        public void HasDuplicateByNameTestForSomeName()
        {
            const string name = "2099";
            var result = _sessionDao.HasDuplicateByName(name);
            const bool actualResult = false;
            Assert.IsInstanceOfType(result, typeof(bool));
            Assert.AreEqual(result, actualResult);
        }
        /// <summary>
        /// Test Case string name = "2015"
        /// </summary>
        [TestMethod]
        public void HasDuplicateByNameTestForExistsName()
        {
            const string name = "2015";
            var result = _sessionDao.HasDuplicateByName(name);
            const bool actualResult = true;
            Assert.IsInstanceOfType(result, typeof(bool));
            Assert.AreEqual(result, actualResult);
        }
        #endregion

        #region HasDuplicateByName(string name, long id)

        public void HasDuplicateByNameTestForNameId()
        {
            string name = " ";
            long id = 2;
            var result = _sessionDao.HasDuplicateByName(name,id);
            bool actualResult = false;

        }
        #endregion
    }
}
