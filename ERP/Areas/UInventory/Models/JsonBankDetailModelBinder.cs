﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdvashERP.BusinessModel.ViewModel.UInventory;

namespace UdvashERP.Areas.UInventory.Models
{
    public class JsonBankDetailModelBinder : IModelBinder
    {   
        public object BindModel(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext)
        {
            var model = new BankDetail();

            foreach (string key in HttpContext.Current.Request.Form.AllKeys)
            {
                if (key.Contains("AcNumber"))
                {
                    model.AcNumber = HttpContext.Current.Request.Form[key];
                }
                else if (key.Contains("BankBranch"))
                {
                    model.BankBranch = string.IsNullOrWhiteSpace(HttpContext.Current.Request.Form[key]) ? 0 : Convert.ToInt64(HttpContext.Current.Request.Form[key]);                    
                }
                else if (key.Contains("AcTitle"))
                {
                    model.AcTitle = HttpContext.Current.Request.Form[key];
                }
                else if (key.Contains("Id"))
                {
                    model.Id = string.IsNullOrWhiteSpace(HttpContext.Current.Request.Form[key]) ? 0 : Convert.ToInt64(HttpContext.Current.Request.Form[key]);                    
                }
            }
            return model;
        }
    }

    
}