﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.SqlCommand;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.Dao.Teachers;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.MediaDao;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.Teachers;
using UdvashERP.Dao.Sms;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherService : IBaseService
    {
        #region Operational Function
        void SaveTeacher(Teacher teacher, HttpPostedFileBase imageUpload = null, TeacherPublicRegistration teacherPublicRegistration = null);
        void UpdateTeacher(Teacher teacher, HttpPostedFileBase httpPostedFileBase);
        #endregion

        #region Single Instances Loading Function
        Teacher GetTeacher(long teacherId);
        Teacher GetAuthorizedTeacherByTpin(List<UserMenu> userMenu, int tpin, List<long> organizationIds = null);
        Teacher GetTeacherByTpin(int tpin, List<long> organizationIds = null);
        Teacher GetTeacher(string mobileNumber);
        TeacherImages GetTeacherImageByTeacherId(long teacherId);
        #endregion

        #region List Loading Function
        IList<Teacher> LoadTeacher(long organizationId, List<long> subjectIdList = null);
        IList<Teacher> LoadTeacher();
        IList<Teacher> LoadTeacher(long[] lectureIdList, long[] batchIdList, DateTime dateFrom, DateTime dateTo, bool? clearClassAttendance = null);
        IList<Teacher> LoadTeacher(long[] organizations, long[] teacherActivities, long[] subjects, int[] religion, int[] versionOfPriority, int[] gender, string nickName = "", string instutute = "", string personalM = "", string fatherM = "",string tpin = "", int? start = null, int? length = null, string orderBy = "NickName", string direction = "ASC");
        IList<Teacher> LoadTeacher(long[] teacherIds);
        IList<Teacher> LoadTeacher(List<BusinessModel.Entity.UserAuth.UserMenu> _userMenu, DateTime date);
        IList<Teacher> LoadTeacherAutoComplete(string query, long organizationId, long? activityTypeId);
        IList<Teacher> LoadTeacher(long[] lectureIdList, DateTime dateFrom, DateTime dateTo);
        #endregion

        #region Others Function
        int GetTeacherCount(long[] organizations, long[] teacherActivities, long[] subjects, int[] religion, int[] versionOfPriority, int[] gender, string nickName = "", string instutute = "", string personalM = "", string fatherM = "",string tpin="");
        bool IsDuplicateMobileNumber(string personalMobile, long id = 0);

        int GetTeacherByKeyWordSearchCount(List<UserMenu> userMenu, List<long> organizationIdList, string keyword, List<string> informationViewList, string tpin, string nickName, string mobileNumber1, string subjectPrority);
        #endregion



        IList<Teacher> LoadTeacherByKeyWordSearch(List<UserMenu> userMenu, int start, int length, string orderBy, string direction, List<long> organizationIdList, string keyword, List<string> informationViewList, string tpin, string nickName, string mobileNumber1, string subjectPrority);
    }

    public class TeacherService : BaseService, ITeacherService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ITeacherDao _teacherDao;
        private readonly ICommonHelper _commonHelper;
        private readonly ITeacherImageMediaDao _teacherImageMediaDao;
        private readonly ISession _mediaSession;
        private readonly ITeacherPublicRegistrationDao _teacherPublicRegistrationDao;
        private readonly ISmsHistoryDao _hrHistoryDao;
        private readonly ISmsSettingsDao _smsSettingsDao;
        public TeacherService(ISession session)
        {
            Session = session;
            _teacherDao = new TeacherDao() { Session = session };
            _commonHelper = new CommonHelper();

            _mediaSession = NHibernateMediaSessionFactory.OpenSession();
            _teacherImageMediaDao = new TeacherImageMediaDao { Session = _mediaSession };
            _teacherPublicRegistrationDao = new TeacherPublicRegistrationDao { Session = session };
            _hrHistoryDao = new SmsHistoryDao { Session = session };
            _smsSettingsDao = new SmsSettingsDao { Session = session };
        }
        #endregion

        #region Operational Function

        public void SaveTeacher(Teacher teacher, HttpPostedFileBase imageUpload = null, TeacherPublicRegistration teacherPublicRegistration = null)
        {
            ITransaction transaction = null;
            try
            {
                #region check teacher

                CheckTeacher(teacher);

                #endregion

                #region check Image

                CheckTeacherImage(imageUpload);

                #endregion

                using (transaction = Session.BeginTransaction())
                {
                    int tpin = _teacherDao.GetNewTpin();
                    teacher.Tpin = tpin;
                    DateTime dateTime = DateTime.Now;
                    long createdBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                    long modifiedBy = createdBy;
                    byte[] imageFile = null;
                    string currentImageHash = "";

                    TeacherImages teacherImages = new TeacherImages();
                    if (imageUpload != null)
                    {
                        imageFile = ConvertFileInByteArray(imageUpload.InputStream, imageUpload.ContentLength);
                        currentImageHash = ComputeHash(imageFile);
                        teacherImages.Teacher = teacher;
                        teacherImages.Hash = currentImageHash;
                        teacherImages.CreateBy = createdBy;
                        teacherImages.ModifyBy = modifiedBy;
                        teacherImages.CreationDate = dateTime;
                        teacherImages.ModificationDate = dateTime;
                        teacherImages.Status = TeacherImages.EntityStatus.Active;
                        teacher.TeacherImageses = new List<TeacherImages> { teacherImages };
                    }
                    _teacherDao.SaveOrUpdate(teacher);

                    //update Teacher Public Register to new teacher's Tpin
                    if (teacherPublicRegistration != null)
                    {
                        teacherPublicRegistration.ModifyBy = modifiedBy;
                        teacherPublicRegistration.ModificationDate = dateTime;
                        teacherPublicRegistration.Tpin = teacher.Tpin;
                        _teacherPublicRegistrationDao.SaveOrUpdate(teacherPublicRegistration);
                        SaveSmsHistory(teacherPublicRegistration,teacher, new List<string>() { "Teacher Assigned" });
                    }

                    if (imageUpload != null)
                    {
                        TeacherMediaImage mediaImage = new TeacherMediaImage();
                        mediaImage.Hash = teacherImages.Hash;
                        mediaImage.ImageRef = teacherImages.Id;
                        mediaImage.ImageGuid = Guid.NewGuid();
                        mediaImage.Images = imageFile;
                        mediaImage.CreateBy = createdBy;
                        mediaImage.ModifyBy = modifiedBy;
                        mediaImage.Status = TeacherMediaImage.EntityStatus.Active;
                        _teacherImageMediaDao.Save(mediaImage);
                    }
                    

                    transaction.Commit();
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                {
                    transaction.Rollback();
                }
            }
        }

        public void UpdateTeacher(Teacher teacher, HttpPostedFileBase imageUpload)
        {
            ITransaction transaction = null;
            ITransaction mediaTransaction = null;
            try
            {
                if (teacher.Id > 0)
                {
                    DateTime dateTime = DateTime.Now;
                    long createdBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                    long modifiedBy = createdBy;
                    byte[] imageFile = null;
                    string currentImageHash = "";

                    Teacher savingTeacher = _teacherDao.LoadById(teacher.Id);

                    #region basic test 

                    if (savingTeacher == null)
                    {
                        throw new InvalidDataException("Invalid Teacher found!");
                    }
                    CheckTeacher(teacher);
                    #endregion

                    #region quick information

                    savingTeacher.NickName = teacher.NickName;
                    savingTeacher.PersonalMobile = teacher.PersonalMobile;
                    savingTeacher.AlternativeMobile = teacher.AlternativeMobile;
                    savingTeacher.Institute = teacher.Institute;
                    savingTeacher.Department = teacher.Department;
                    savingTeacher.HscPassingYear = teacher.HscPassingYear;
                    savingTeacher.Religion = teacher.Religion;
                    savingTeacher.Gender = teacher.Gender;

                    #region Teacher Organization
                    savingTeacher.Organizations.Clear();
                    if (teacher.Organizations.Any())
                    {
                        savingTeacher.Organizations = teacher.Organizations;
                    }
                    #endregion

                    #region Teacher Activity
                    List<long> newTeacherActivityIdList = new List<long>();
                    if (teacher.TeacherActivityPriorities.Any())
                    {
                        newTeacherActivityIdList = teacher.TeacherActivityPriorities.Select(teacherActivityPriority => teacherActivityPriority.TeacherActivity.Id).ToList();
                    }

                    List<TeacherActivityPriority> updateTeacherActivityPriorityList = new List<TeacherActivityPriority>();
                    List<TeacherActivityPriority> deleteTeacherActivityPriorityList = new List<TeacherActivityPriority>();
                    if (savingTeacher.TeacherActivityPriorities.Any())
                    {
                        foreach (TeacherActivityPriority teacherActivityPriority in savingTeacher.TeacherActivityPriorities)
                        {
                            //update current Teacher Activity with new one
                            if (newTeacherActivityIdList.Contains(teacherActivityPriority.TeacherActivity.Id))
                            {
                                TeacherActivityPriority tempTeacherActivityPriority = teacher.TeacherActivityPriorities.SingleOrDefault(x => x.TeacherActivity.Id == teacherActivityPriority.TeacherActivity.Id);
                                updateTeacherActivityPriorityList.Add(tempTeacherActivityPriority);
                                newTeacherActivityIdList.Remove(teacherActivityPriority.TeacherActivity.Id);
                                //after update teacher activity priority remove it from the new list
                            }
                            else
                            {
                                deleteTeacherActivityPriorityList.Add(teacherActivityPriority);
                                //remove the teacherActivityPriority 
                            }
                        }
                    }
                    //newly added 
                    if (newTeacherActivityIdList.Any())
                    {
                        foreach (long teacherActivityId in newTeacherActivityIdList)
                        {
                            TeacherActivityPriority tempTeacherActivityPriority = teacher.TeacherActivityPriorities.SingleOrDefault(x => x.TeacherActivity.Id == teacherActivityId);
                            tempTeacherActivityPriority.Teacher = savingTeacher;
                            savingTeacher.TeacherActivityPriorities.Add(tempTeacherActivityPriority);
                        }
                    }
                    //update list
                    if (updateTeacherActivityPriorityList.Any())
                    {
                        foreach (TeacherActivityPriority teacherActivityPriority in updateTeacherActivityPriorityList)
                        {
                            TeacherActivityPriority tempTeacherActivityPriority = savingTeacher.TeacherActivityPriorities.SingleOrDefault(x => x.TeacherActivity.Id == teacherActivityPriority.TeacherActivity.Id);
                            tempTeacherActivityPriority.Priority = teacherActivityPriority.Priority;
                            tempTeacherActivityPriority.ModifyBy = teacherActivityPriority.ModifyBy;
                            tempTeacherActivityPriority.ModificationDate = teacherActivityPriority.ModificationDate;
                        }
                    }
                    //Delete list
                    if (deleteTeacherActivityPriorityList.Any())
                    {
                        foreach (TeacherActivityPriority teacherActivityPriority in deleteTeacherActivityPriorityList)
                        {
                            savingTeacher.TeacherActivityPriorities.Remove(teacherActivityPriority);
                        }
                    }
                    #endregion

                    #region Teacher Subject priority

                    List<long> newSubjectIdList = new List<long>();
                    if (teacher.TeacherSubjectPriorities.Any())
                    {
                        newSubjectIdList = teacher.TeacherSubjectPriorities.Select(teacherSubjectPriority => teacherSubjectPriority.Subject.Id).ToList();
                    }

                    List<TeacherSubjectPriority> updateTeacherSubjectPriorityList = new List<TeacherSubjectPriority>();
                    List<TeacherSubjectPriority> deleteTeacherSubjectPriorityList = new List<TeacherSubjectPriority>();

                    if (savingTeacher.TeacherSubjectPriorities.Any())
                    {
                        foreach (TeacherSubjectPriority teacherSubjectPriority in savingTeacher.TeacherSubjectPriorities)
                        {
                            //update current Teacher Subject with new one
                            if (newSubjectIdList.Contains(teacherSubjectPriority.Subject.Id))
                            {
                                TeacherSubjectPriority tempTeacherSubjectPriority = teacher.TeacherSubjectPriorities.SingleOrDefault(x => x.Subject.Id == teacherSubjectPriority.Subject.Id);
                                updateTeacherSubjectPriorityList.Add(tempTeacherSubjectPriority);
                                newSubjectIdList.Remove(teacherSubjectPriority.Subject.Id);
                                //after update teacher subject priority remove it from the new list
                            }
                            else
                            {
                                deleteTeacherSubjectPriorityList.Add(teacherSubjectPriority);
                                //remove the teacherActivityPriority 
                            }
                        }
                    }
                    //newly added 
                    if (newSubjectIdList.Any())
                    {
                        foreach (long subjectId in newSubjectIdList)
                        {
                            TeacherSubjectPriority tempTeacherSubjectPriority = teacher.TeacherSubjectPriorities.SingleOrDefault(x => x.Subject.Id == subjectId);
                            tempTeacherSubjectPriority.Teacher = savingTeacher;
                            savingTeacher.TeacherSubjectPriorities.Add(tempTeacherSubjectPriority);
                        }
                    }
                    //update list
                    if (updateTeacherSubjectPriorityList.Any())
                    {
                        foreach (TeacherSubjectPriority teacherSubjectPriority in updateTeacherSubjectPriorityList)
                        {
                            TeacherSubjectPriority tempTeacherSubjectPriority = savingTeacher.TeacherSubjectPriorities.SingleOrDefault(x => x.Subject.Id == teacherSubjectPriority.Subject.Id);
                            tempTeacherSubjectPriority.Priority = teacherSubjectPriority.Priority;
                            tempTeacherSubjectPriority.ModifyBy = teacherSubjectPriority.ModifyBy;
                            tempTeacherSubjectPriority.ModificationDate = teacherSubjectPriority.ModificationDate;
                        }
                    }
                    //Delete list
                    if (deleteTeacherSubjectPriorityList.Any())
                    {
                        foreach (TeacherSubjectPriority teacherSubjectPriority in deleteTeacherSubjectPriorityList)
                        {
                            savingTeacher.TeacherSubjectPriorities.Remove(teacherSubjectPriority);
                        }
                    }
                    #endregion

                    #region Teacher Version priority

                    List<int> newTeacherVersionIdList = new List<int>();
                    if (teacher.TeacherVersionOfStudyPriorities.Any())
                    {
                        newTeacherVersionIdList = teacher.TeacherVersionOfStudyPriorities.Select(teacherVersionOfStudyPriority => teacherVersionOfStudyPriority.VersionOfStudy).ToList();
                    }

                    //update list will contain object list from view
                    List<TeacherVersionOfStudyPriority> updateTeacherVersionOfStudyPriorityList = new List<TeacherVersionOfStudyPriority>();
                    //delete list will contain real object list which going to saving
                    List<TeacherVersionOfStudyPriority> deleteTeacherVersionOfStudyPriorityList = new List<TeacherVersionOfStudyPriority>();

                    if (savingTeacher.TeacherVersionOfStudyPriorities.Any())
                    {
                        foreach (TeacherVersionOfStudyPriority teacherVersionOfStudyPriority in savingTeacher.TeacherVersionOfStudyPriorities)
                        {
                            if (newTeacherVersionIdList.Contains(teacherVersionOfStudyPriority.VersionOfStudy))
                            {
                                TeacherVersionOfStudyPriority tempTeacherVersionOfStudyPriority = teacher.TeacherVersionOfStudyPriorities.SingleOrDefault(x => x.VersionOfStudy == teacherVersionOfStudyPriority.VersionOfStudy);
                                updateTeacherVersionOfStudyPriorityList.Add(tempTeacherVersionOfStudyPriority);
                                newTeacherVersionIdList.Remove(teacherVersionOfStudyPriority.VersionOfStudy);
                            }
                            else
                            {
                                deleteTeacherVersionOfStudyPriorityList.Add(teacherVersionOfStudyPriority);
                            }
                        }
                    }

                    //newly Version of study add 
                    if (newTeacherVersionIdList.Any())
                    {
                        foreach (int versionOfStudy in newTeacherVersionIdList)
                        {
                            TeacherVersionOfStudyPriority tempTeacherVersionOfStudyPriority = teacher.TeacherVersionOfStudyPriorities.SingleOrDefault(x => x.VersionOfStudy == versionOfStudy);
                            tempTeacherVersionOfStudyPriority.Teacher = savingTeacher;
                            savingTeacher.TeacherVersionOfStudyPriorities.Add(tempTeacherVersionOfStudyPriority);
                        }
                    }
                    //update vsersion of study 
                    if (updateTeacherVersionOfStudyPriorityList.Any())
                    {
                        foreach (TeacherVersionOfStudyPriority teacherVersionOfStudyPriority in updateTeacherVersionOfStudyPriorityList)
                        {
                            TeacherVersionOfStudyPriority tempTeacherVersionOfStudyPriority = savingTeacher.TeacherVersionOfStudyPriorities.SingleOrDefault(x => x.VersionOfStudy == teacherVersionOfStudyPriority.VersionOfStudy);
                            tempTeacherVersionOfStudyPriority.Priority = teacherVersionOfStudyPriority.Priority;
                            tempTeacherVersionOfStudyPriority.ModificationDate = teacherVersionOfStudyPriority.ModificationDate;
                            tempTeacherVersionOfStudyPriority.ModifyBy = teacherVersionOfStudyPriority.ModifyBy;

                        }
                    }
                    //delete vsersion of study 
                    if (deleteTeacherVersionOfStudyPriorityList.Any())
                    {
                        foreach (TeacherVersionOfStudyPriority teacherVersionOfStudyPriority in deleteTeacherVersionOfStudyPriorityList)
                        {
                            savingTeacher.TeacherVersionOfStudyPriorities.Remove(teacherVersionOfStudyPriority);

                        }
                    }
                    #endregion

                    #endregion

                    #region details Information

                    savingTeacher.FullName = teacher.FullName;
                    savingTeacher.FatherName = teacher.FatherName;
                    savingTeacher.DateOfBirth = teacher.DateOfBirth;
                    savingTeacher.BloodGroup = teacher.BloodGroup;
                    savingTeacher.RoomMatesMobile = teacher.RoomMatesMobile;
                    savingTeacher.FathersMobile = teacher.FathersMobile;
                    savingTeacher.MothersMobile = teacher.MothersMobile;
                    savingTeacher.Email = teacher.Email;
                    savingTeacher.FbId = teacher.FbId;
                    savingTeacher.PresentAddress = teacher.PresentAddress;
                    savingTeacher.Area = teacher.Area;
                    savingTeacher.PermanentAddress = teacher.PermanentAddress;


                    #region Teacher Admission Info

                    List<int> newTeacherUniversityIdList = new List<int>();
                    if (teacher.TeacherAdmissionInfos != null && teacher.TeacherAdmissionInfos.Any())
                    {
                        newTeacherUniversityIdList = teacher.TeacherAdmissionInfos.Select(teacherAdmissionInfo => teacherAdmissionInfo.UniversityName).ToList();
                    }


                    //update list will contain object list from view
                    List<TeacherAdmissionInfo> updateTeacherAdmissionInfoList = new List<TeacherAdmissionInfo>();
                    //delete list will contain real object list which going to saving
                    List<TeacherAdmissionInfo> deleteTeacherAdmissionInfoList = new List<TeacherAdmissionInfo>();

                    if (savingTeacher.TeacherAdmissionInfos.Any())
                    {
                        foreach (TeacherAdmissionInfo teacherAdmissionInfo in savingTeacher.TeacherAdmissionInfos)
                        {
                            if (newTeacherUniversityIdList.Contains(teacherAdmissionInfo.UniversityName))
                            {
                                TeacherAdmissionInfo tempTeacherAdmissionInfo = teacher.TeacherAdmissionInfos.SingleOrDefault(x => x.UniversityName == teacherAdmissionInfo.UniversityName);
                                updateTeacherAdmissionInfoList.Add(tempTeacherAdmissionInfo);
                                newTeacherUniversityIdList.Remove(teacherAdmissionInfo.UniversityName);
                            }
                            else
                            {
                                deleteTeacherAdmissionInfoList.Add(teacherAdmissionInfo);
                            }
                        }
                    }

                    //newly Admission info 
                    if (newTeacherUniversityIdList.Any())
                    {
                        foreach (int universitryName in newTeacherUniversityIdList)
                        {
                            TeacherAdmissionInfo teampTeacherAdmissionInfo = teacher.TeacherAdmissionInfos.SingleOrDefault(x => x.UniversityName == universitryName);
                            teampTeacherAdmissionInfo.Teacher = savingTeacher;
                            savingTeacher.TeacherAdmissionInfos.Add(teampTeacherAdmissionInfo);
                        }
                    }
                    //update Admission Info
                    if (updateTeacherAdmissionInfoList.Any())
                    {
                        foreach (TeacherAdmissionInfo teacherAdmissionInfo in updateTeacherAdmissionInfoList)
                        {
                            TeacherAdmissionInfo tempTeacherAdmissionInfo = savingTeacher.TeacherAdmissionInfos.SingleOrDefault(x => x.UniversityName == teacherAdmissionInfo.UniversityName);
                            tempTeacherAdmissionInfo.AtPosition = teacherAdmissionInfo.AtPosition;
                            tempTeacherAdmissionInfo.AtSession = teacherAdmissionInfo.AtSession;
                            tempTeacherAdmissionInfo.GotSubject = teacherAdmissionInfo.GotSubject;
                            tempTeacherAdmissionInfo.ModificationDate = teacherAdmissionInfo.ModificationDate;
                            tempTeacherAdmissionInfo.ModifyBy = teacherAdmissionInfo.ModifyBy;

                        }
                    }
                    //delete vsersion of study 
                    if (deleteTeacherAdmissionInfoList.Any())
                    {
                        foreach (TeacherAdmissionInfo teacherAdmissionInfo in deleteTeacherAdmissionInfoList)
                        {
                            savingTeacher.TeacherAdmissionInfos.Remove(teacherAdmissionInfo);

                        }
                    }
                    #endregion

                    #region Teacher Extra Curricular Activities

                    List<long> newExtraCurricularActivityIdList = new List<long>();
                    if (teacher.TeacherExtraCurricularActivities != null && teacher.TeacherExtraCurricularActivities.Any())
                    {
                        newExtraCurricularActivityIdList = teacher.TeacherExtraCurricularActivities.Select(teacherExtraCurricularActivity => teacherExtraCurricularActivity.ExtraCurricularActivity.Id).ToList();
                    }


                    //update list will contain object list from view
                    List<TeacherExtraCurricularActivity> updateTeacherExtraCurricularActivityList = new List<TeacherExtraCurricularActivity>();
                    //delete list will contain real object list which going to saving
                    List<TeacherExtraCurricularActivity> deleteTeacherExtraCurricularActivityList = new List<TeacherExtraCurricularActivity>();

                    if (savingTeacher.TeacherExtraCurricularActivities.Any())
                    {
                        foreach (TeacherExtraCurricularActivity teacherExtraCurricularActivity in savingTeacher.TeacherExtraCurricularActivities)
                        {
                            if (newExtraCurricularActivityIdList.Contains(teacherExtraCurricularActivity.ExtraCurricularActivity.Id))
                            {
                                TeacherExtraCurricularActivity tempTeacherExtraCurricularActivity = teacher.TeacherExtraCurricularActivities.SingleOrDefault(x => x.ExtraCurricularActivity.Id == teacherExtraCurricularActivity.ExtraCurricularActivity.Id);
                                updateTeacherExtraCurricularActivityList.Add(tempTeacherExtraCurricularActivity);
                                newExtraCurricularActivityIdList.Remove(teacherExtraCurricularActivity.ExtraCurricularActivity.Id);
                            }
                            else
                            {
                                deleteTeacherExtraCurricularActivityList.Add(teacherExtraCurricularActivity);
                            }
                        }
                    }

                    //newly Extra Curricular Activity
                    if (newExtraCurricularActivityIdList.Any())
                    {
                        foreach (long extraCurricularActivityId in newExtraCurricularActivityIdList)
                        {
                            TeacherExtraCurricularActivity teampTeacherExtraCurricularActivity = teacher.TeacherExtraCurricularActivities.SingleOrDefault(x => x.ExtraCurricularActivity.Id == extraCurricularActivityId);
                            teampTeacherExtraCurricularActivity.Teacher = savingTeacher;
                            savingTeacher.TeacherExtraCurricularActivities.Add(teampTeacherExtraCurricularActivity);
                        }
                    }
                    //update Extra Curricular Activity
                    if (updateTeacherExtraCurricularActivityList.Any())
                    {
                        foreach (TeacherExtraCurricularActivity teacherExtraCurricularActivity in updateTeacherExtraCurricularActivityList)
                        {
                            TeacherExtraCurricularActivity tempTeacherExtraCurricularActivity = savingTeacher.TeacherExtraCurricularActivities.SingleOrDefault(x => x.ExtraCurricularActivity.Id == teacherExtraCurricularActivity.ExtraCurricularActivity.Id);
                            tempTeacherExtraCurricularActivity.ModificationDate = teacherExtraCurricularActivity.ModificationDate;
                            tempTeacherExtraCurricularActivity.ModifyBy = teacherExtraCurricularActivity.ModifyBy;
                        }
                    }
                    //delete Extra Curricular Activity
                    if (deleteTeacherExtraCurricularActivityList.Any())
                    {
                        foreach (TeacherExtraCurricularActivity teacherExtraCurricularActivity in deleteTeacherExtraCurricularActivityList)
                        {
                            savingTeacher.TeacherExtraCurricularActivities.Remove(teacherExtraCurricularActivity);

                        }
                    }
                    #endregion

                    #endregion


                    #region Update Teacher Info

                    #region check teacher
                    //after populate check again
                    CheckTeacher(teacher);
                    #endregion
                    #region check Image
                    CheckTeacherImage(imageUpload);
                    #endregion

                    #region Teacher Image

                    bool hasImage = true;
                    TeacherImages teacherImages = savingTeacher.TeacherImageses.OrderByDescending(x => x.Id).Take(1).SingleOrDefault();
                    if (imageUpload != null)
                    {
                        if (teacherImages == null)
                        {
                            hasImage = false;
                            teacherImages = new TeacherImages();
                            teacherImages.CreationDate = dateTime;
                            teacherImages.CreateBy = createdBy;
                            teacherImages.Teacher = savingTeacher;
                            teacherImages.Status = TeacherImages.EntityStatus.Active;
                        }
                        imageFile = ConvertFileInByteArray(imageUpload.InputStream, imageUpload.ContentLength);
                        currentImageHash = ComputeHash(imageFile);
                        teacherImages.Hash = currentImageHash;
                        teacherImages.ModifyBy = modifiedBy;
                        teacherImages.ModificationDate = dateTime;
                        if (hasImage == false)
                        {
                            savingTeacher.TeacherImageses = new List<TeacherImages> { teacherImages };
                        }
                    }

                    #endregion

                    using (transaction = Session.BeginTransaction())
                    {
                        _teacherDao.SaveOrUpdate(savingTeacher);
                        transaction.Commit();
                    }
                    using (mediaTransaction = _mediaSession.BeginTransaction())
                    {
                        if (imageUpload != null)
                        {
                            TeacherMediaImage mediaImage = new TeacherMediaImage();
                            if (hasImage == true)
                            {
                                mediaImage = _teacherImageMediaDao.GetByRefId(teacherImages.Id);
                                if (mediaImage == null)
                                {
                                    mediaImage = new TeacherMediaImage();
                                    mediaImage.CreationDate = dateTime;
                                    mediaImage.CreateBy = createdBy;
                                    mediaImage.Status = TeacherMediaImage.EntityStatus.Active;
                                }
                            }
                            mediaImage.ImageRef = teacherImages.Id;
                            mediaImage.Hash = currentImageHash;
                            mediaImage.ImageGuid = Guid.NewGuid();
                            mediaImage.Images = imageFile;
                            mediaImage.ModificationDate = dateTime;
                            mediaImage.ModifyBy = modifiedBy;
                            _teacherImageMediaDao.SaveOrUpdate(mediaImage);

                            mediaTransaction.Commit();
                        }
                    }


                    #endregion
                }
                else
                {
                    throw new InvalidDataException("Your are trying to update information of an Invalid Teacher.");
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                {
                    transaction.Rollback();
                }
                if (mediaTransaction != null && mediaTransaction.IsActive)
                {
                    mediaTransaction.Rollback();
                }
            }
        }

        #endregion

        #region Single Instances Loading Function

        public TeacherImages GetTeacherImageByTeacherId(long teacherId)
        {
            try
            {
                return _teacherDao.GetTeacherImageByTeacherId(teacherId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Teacher GetTeacherByTpin(int tpin, List<long> organizationIds = null)
        {
            try
            {
                return _teacherDao.GetTeacherByTpin(tpin, organizationIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public Teacher GetAuthorizedTeacherByTpin(List<UserMenu> userMenu, int tpin, List<long> organizationIds = null)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationIds != null && organizationIds.Any()) ? organizationIds : null);
                return _teacherDao.GetTeacherByTpin(tpin, organizationIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public Teacher GetTeacher(long teacherId)
        {
            try
            {
                return _teacherDao.LoadById(teacherId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public Teacher GetTeacher(string mobileNumber)
        {
            try
            {
                return _teacherDao.GetTeacher(mobileNumber);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<Teacher> LoadTeacher()
        {
            try
            {
                return _teacherDao.LoadAllOk();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Teacher> LoadTeacher(long[] teacherIds)
        {
            try
            {
                return _teacherDao.LoadByIds(teacherIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Teacher> LoadTeacherAutoComplete(string query, long organizationId, long? activityTypeId)
        {
            try
            {
                return _teacherDao.LoadTeacherAutoComplete(query, organizationId, activityTypeId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Teacher> LoadTeacher(long organizationId, List<long> subjectIdList = null)
        {
            try
            {
                return _teacherDao.LoadTeacher(organizationId, subjectIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Teacher> LoadTeacher(long[] organizations, long[] teacherActivities, long[] subjects, int[] religion, int[] versionOfPriority, int[] gender, string nickName = "", string instutute = "", string personalM = "", string fatherM = "", string tpin="", int? start = null, int? length = null, string orderBy = "NickName", string direction = "ASC")
        {
            try
            {
                if (String.IsNullOrEmpty(tpin))
                    tpin = "0";
                return _teacherDao.LoadTeacher(organizations.ToList(), teacherActivities.ToList(), subjects.ToList(), religion.ToList(), versionOfPriority.ToList(), gender.ToList(), nickName, instutute, personalM, fatherM,Convert.ToInt32(tpin), start, length, orderBy, direction);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Teacher> LoadTeacher(long[] lectureIdList, long[] batchIdList, DateTime dateFrom, DateTime dateTo, bool? clearClassAttendance = null)
        {

            try
            {
                return _teacherDao.LoadTeacher(lectureIdList, batchIdList, dateFrom, dateTo, clearClassAttendance);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public IList<Teacher> LoadTeacher(List<BusinessModel.Entity.UserAuth.UserMenu> _userMenu, DateTime date)
        {
            List<long> programIdList = AuthHelper.LoadProgramIdList(_userMenu);
            List<long> branchIdList = AuthHelper.LoadBranchIdList(_userMenu);
            return _teacherDao.LoadTeacher(programIdList, branchIdList, date);
        }

        public IList<Teacher> LoadTeacher(long[] lectureIdList, DateTime dateFrom, DateTime dateTo)
        {
            return _teacherDao.LoadTeacher(lectureIdList, dateFrom, dateTo);
        }
        
        public IList<Teacher> LoadTeacherByKeyWordSearch(List<UserMenu> userMenu, int start, int length, string orderBy, string direction, List<long> organizationIdList, string keyword, List<string> informationViewList, string tpin, string nickName, string mobileNumber1, string subjectPrority)
        {
            try
            {
                List<long> authorizedOrganizationList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationIdList.Contains(SelectionType.SelelectAll)) ? null : organizationIdList);
                if (String.IsNullOrEmpty(tpin))
                    tpin = "0";

                List<int> religionIdList = new List<int>();
                List<int> genderIdList = new List<int>();
                List<int> bloodGroupIdList = new List<int>();
                List<int> versionPriorityIdList = new List<int>();
                
                //search keyword 
                if (informationViewList.Any())
                {
                    foreach (string information in informationViewList)
                    {
                        switch (information)
                        {
                            case TeacherSearchConstants.Religion:
                                religionIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Religion>(keyword);
                                break;
                            case TeacherSearchConstants.Gender:
                                genderIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Gender>(keyword);
                                break;
                            case TeacherSearchConstants.BloodGroup:
                                bloodGroupIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<BloodGroup>(keyword);
                                break;
                            case TeacherSearchConstants.VersionPriority:
                                versionPriorityIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<VersionOfStudy>(keyword);
                                break;
                        }
                    }
                }
                
                List<Teacher> teacherList = _teacherDao.LoadTeacherByKeyWordSearch(start, length, orderBy, direction, authorizedOrganizationList, keyword, informationViewList, Convert.ToInt32(tpin), nickName, mobileNumber1, subjectPrority, religionIdList, genderIdList, bloodGroupIdList, versionPriorityIdList);
                return teacherList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int GetTeacherCount(long[] organizations, long[] teacherActivities, long[] subjects, int[] religion, int[] versionOfPriority, int[] gender, string nickName = "", string instutute = "", string personalM = "", string fatherM = "", string tpin = "")
        {
            int teacherCount;
            try
            {
                if (String.IsNullOrEmpty(tpin))
                    tpin = "0";
                teacherCount = _teacherDao.GetTeacherCount(organizations.ToList(), teacherActivities.ToList(), subjects.ToList(), religion.ToList(), versionOfPriority.ToList(), gender.ToList(), nickName, instutute, personalM, fatherM, Convert.ToInt32(tpin));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return teacherCount;
        }
        public int GetTeacherByKeyWordSearchCount(List<UserMenu> userMenu, List<long> organizationIdList, string keyword, List<string> informationViewList, string tpin, string nickName, string mobileNumber1, string subjectPrority)
        {
            int teacherCount;
            try
            {
                List<long> authorizedOrganizationList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationIdList.Contains(SelectionType.SelelectAll)) ? null : organizationIdList);
                if (String.IsNullOrEmpty(tpin))
                    tpin = "0";

                List<int> religionIdList = new List<int>();
                List<int> genderIdList = new List<int>();
                List<int> bloodGroupIdList = new List<int>();
                List<int> versionPriorityIdList = new List<int>();

                //search keyword 
                if (informationViewList.Any())
                {
                    foreach (string information in informationViewList)
                    {
                        switch (information)
                        {
                            case TeacherSearchConstants.Religion:
                                religionIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Religion>(keyword);
                                break;
                            case TeacherSearchConstants.Gender:
                                genderIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<Gender>(keyword);
                                break;
                            case TeacherSearchConstants.BloodGroup:
                                bloodGroupIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<BloodGroup>(keyword);
                                break;
                            case TeacherSearchConstants.VersionPriority:
                                versionPriorityIdList = _commonHelper.LoadSearchResultEmumToDictionaryForKeyWord<VersionOfStudy>(keyword);
                                break;
                        }
                    }
                }

                teacherCount = _teacherDao.GetTeacherByKeyWordSearchCount(authorizedOrganizationList, keyword, informationViewList, Convert.ToInt32(tpin), nickName, mobileNumber1, subjectPrority, religionIdList, genderIdList, bloodGroupIdList, versionPriorityIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return teacherCount;
        }

        public bool IsDuplicateMobileNumber(string personalMobile, long id = 0)
        {
            bool isDuplicate;
            try
            {
                //isDuplicate = _teacherDao.IsDuplicateMobileNumber(personalMobile, isEdit, id);
                isDuplicate = _teacherDao.IsDuplicateMobileNumber(personalMobile, id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return isDuplicate;
        }

        #endregion

        #region Helper Function
        private void SaveSmsHistory(TeacherPublicRegistration teacherPublicRegistration, Teacher teacher, List<string> smsTypes)
        {
            var smsSettingses = _smsSettingsDao.SmsSettingsList(smsTypes);
            if (smsSettingses.Any())
            {
                long smsUserId = 1;

                var smsSettings = smsSettingses.FirstOrDefault();
                if (smsSettings != null)
                {
                    var smsHistoryList = new List<SmsHistory>();
                    var options = smsSettings.SmsType.DynamicOptions;
                    var message = smsSettings.Template;
                    var mask = smsSettings.MaskName;
                    var teacherMobile = teacherPublicRegistration.MobileNumber1;

                    #region Generate SMS from Template
                    if (options.Any())
                    {
                        foreach (var o in options)
                        {
                            string optionField = "[[{{" + o.Name + "}}]]";
                            string optionValue = "";

                            switch (o.Name)
                            {
                                case "Teacher Full Name":
                                    optionValue = teacherPublicRegistration.FullName;
                                    break;
                                case "Teacher Nick Name":
                                    optionValue = teacherPublicRegistration.NickName;
                                    break;
                                case "Teacher Pin":
                                    optionValue = teacherPublicRegistration.Tpin.ToString();
                                    break;

                                default:
                                    optionValue = "";
                                    break;
                            }
                            message = message.Replace(optionField, optionValue);
                        }
                    }

                    #endregion

                    #region Listing Receivers

                    var smsToBeSend = new List<SendTeacherSmsViewModel>();
                    foreach (var sr in smsSettings.SmsReceivers)
                    {
                        var svm = new SendTeacherSmsViewModel();
                        switch (sr.Name)
                        {
                            case "Mobile Number (Teacher)":
                                teacherMobile = _commonHelper.CheckMobileNumber(teacherMobile);
                                // teacherMobile = CheckMobileNumber(teacherMobile);
                                if (teacherMobile != null && teacherMobile != null && teacherMobile != null)
                                {
                                    svm.SmsReceiverId = sr.Id;
                                    svm.ReceiverNumber = "88" + teacherMobile;
                                    svm.TeacherId = teacher.Id;
                                    svm.OrganizationId = teacherPublicRegistration.Organization.Id;
                                    svm.Organization = teacherPublicRegistration.Organization;
                                }
                                break;

                            default:
                                break;
                        }

                        if (svm.ReceiverNumber != null && svm.SmsReceiverId != 0)
                        {
                            var alreadyExist = smsToBeSend.Where(x => x.ReceiverNumber == svm.ReceiverNumber).ToList();
                            if (!alreadyExist.Any())
                            {
                                smsToBeSend.Add(svm);
                            }
                        }
                    }
                    #endregion

                    #region Generate Sms History List
                    if (smsToBeSend.Any())
                    {
                        if (String.IsNullOrEmpty(mask))
                        {
                            mask = "";
                        }
                        foreach (var stbs in smsToBeSend)
                        {

                            var smsHistory = new SmsHistory();
                            var currentNumber = stbs;
                            smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                            smsHistory.SmsReceiver = smsSettings.SmsReceivers.FirstOrDefault(x => x.Id == currentNumber.SmsReceiverId);
                            smsHistory.Sms = message;
                            smsHistory.Organization = stbs.Organization;
                            smsHistory.SmsSettings = smsSettings;
                            smsHistory.Mask = mask;
                            smsHistory.Type = Convert.ToInt32(smsSettings.SmsType.Id);
                            smsHistory.Status = SmsHistory.EntityStatus.Pending;
                            smsHistory.CreateBy = smsUserId;
                            smsHistory.ModifyBy = smsUserId;
                            smsHistory.Priority = 4;
                            smsHistoryList.Add(smsHistory);
                        }
                    }

                    #endregion

                    if (smsHistoryList.Any())
                    {
                        foreach (var smsHistory in smsHistoryList)
                        {
                            _hrHistoryDao.Save(smsHistory);
                        }
                    }
                }
            }
            //var smsHistoryList = GenerateSmsHistoryTemplate(teacherPublicRegistration, smsSettingses);
        }

        private string CheckFileSettingsForTypeAndSize(string name, string contentType, int size)
        {
            try
            {
                if (name == null || contentType == null || size < 1)
                    return "This is not valid file. Check the file extension.";

                if (contentType.ToUpper() == "JPG" || contentType.ToUpper() == "JPEG" || contentType.ToUpper() == "IMAGE/JPEG")
                {
                    string message = "";
                    var imageSizeKB = StudentImageSize.CalculateSize(Convert.ToDouble(size));

                    if (imageSizeKB < StudentImageSize.MinimumSizeKB)
                        return "The image size is too small.";

                    if (imageSizeKB > StudentImageSize.MaximumSizeKB)
                        return "The image size is too large. Max valid size is up to " + StudentImageSize.MaximumSizeKB + "KB";

                    return message;
                }

                return "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension";
            }
        }

        private string ComputeHash(byte[] file)
        {
            MD5 md5 = MD5.Create();

            byte[] hashAraay = md5.ComputeHash(file);

            var builder = new StringBuilder();

            foreach (byte b in hashAraay)
            {
                builder.AppendFormat("{0:x2}", b);
            }

            return builder.ToString();
        }

        private byte[] ConvertFileInByteArray(Stream inputStream, int contentLength)
        {
            try
            {
                byte[] file = null;

                using (var binaryReader = new BinaryReader(inputStream))
                {
                    file = binaryReader.ReadBytes(contentLength);
                }

                return file;
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
                throw;
            }
        }

        private void CheckTeacherImage(HttpPostedFileBase imageUpload)
        {
            if (imageUpload != null)
            {
                string sizeCheckerMessage = CheckFileSettingsForTypeAndSize(imageUpload.FileName, imageUpload.ContentType, imageUpload.ContentLength);
                if (!String.IsNullOrEmpty(sizeCheckerMessage))
                {
                    throw new InvalidDataException(sizeCheckerMessage);
                }
            }
        }

        private void CheckTeacher(Teacher teacher)
        {

            #region required filed validation

            if (String.IsNullOrEmpty(teacher.NickName.Trim()))
            {
                throw new InvalidDataException("Nick Name Is Empty.");
            }
            if (String.IsNullOrEmpty(teacher.PersonalMobile.Trim()))
            {
                throw new InvalidDataException("Personal Mobile Number Is Empty.");
            }
            else
            {
                if (IsDuplicateMobileNumber(teacher.PersonalMobile, teacher.Id))
                {
                    throw new InvalidDataException("This Personal Mobile (" + teacher.PersonalMobile + ") Number already has for other teacher.");
                }
            }
            //if (String.IsNullOrEmpty(teacher.AlternativeMobile.Trim()))
            //{
            //    throw new InvalidDataException("Alternative Mobile Number Is Empty.");
            //}
            if (!String.IsNullOrEmpty(teacher.AlternativeMobile) && (teacher.PersonalMobile.Trim() == teacher.AlternativeMobile.Trim()))
            {
                throw new InvalidDataException("Alternative Mobile Number and Personal Mobile Number can't be same.");
            }
            if (String.IsNullOrEmpty(teacher.Institute.Trim()))
            {
                throw new InvalidDataException("Institute Is Empty.");
            }
            if (String.IsNullOrEmpty(teacher.Department.Trim()))
            {
                throw new InvalidDataException("Department Is Empty.");
            }
            if (teacher.HscPassingYear == null || teacher.HscPassingYear <= 0)
            {
                throw new InvalidDataException("Invalid HSC Passing Year.");
            }
            else
            {
                Regex rgx = new Regex(@"^(\d{4})$");
                if (!rgx.IsMatch(teacher.HscPassingYear.ToString()))
                {
                    throw new InvalidDataException("Invalid HSC Passing Year.");
                }
            }
            if (teacher.Religion <= 0)
            {
                throw new InvalidDataException("Invalid Religion.");
            }
            else
            {
                var religionValue = new List<int>(Enum.GetValues(typeof(Religion)).Cast<int>());
                if (!religionValue.Contains(Convert.ToInt32(teacher.Religion)))
                {
                    throw new InvalidDataException("Invalid Regilion Selected");
                }
            }
            if (teacher.Gender <= 0)
            {
                throw new InvalidDataException("Invalid Gender.");
            }
            else
            {
                var genderValue = new List<int>(Enum.GetValues(typeof(Gender)).Cast<int>());
                if (!genderValue.Contains(Convert.ToInt32(teacher.Gender)))
                {
                    throw new InvalidDataException("Invalid Gender Selected");
                }
            }

            if (!teacher.Organizations.Any())
            {
                throw new InvalidDataException("No Organization Selected");
            }

            #region Activity Priority
            if (!teacher.TeacherActivityPriorities.Any())
            {
                throw new InvalidDataException("No Activity Selected");
            }
            else
            {
                //dublicate activity check
                int countDuplicateActivity = teacher.TeacherActivityPriorities.GroupBy(x => x.TeacherActivity.Id).Select(x => new
                {
                    Id = x.Key,
                    Count = x.Count()
                }).Where(x => x.Count > 1).ToList().Count;
                if (countDuplicateActivity > 0)
                {
                    throw new InvalidDataException("One or more Activity Priority selected one or more time(s).");
                }

                //check if first activity is selected or not
                TeacherActivityPriority firstPriorityActivity = teacher.TeacherActivityPriorities.SingleOrDefault(x => x.Priority == 1);
                if (firstPriorityActivity == null)
                {
                    throw new InvalidDataException("You have to select first Activity Priority.");
                }
                //todo check number of activity can submit a teacher

                //check missing subject priority
                int maxActivityPriority = teacher.TeacherActivityPriorities.Max(x => x.Priority);
                if (maxActivityPriority != teacher.TeacherActivityPriorities.Count)
                {
                    throw new InvalidDataException("One or more Activity Priority missing in serial.");
                }

            }
            #endregion

            #region Subject Priority
            if (!teacher.TeacherSubjectPriorities.Any())
            {
                throw new InvalidDataException("No Subject Priority selected.");
            }
            else
            {
                //dublicate Subject priority check
                int countDuplicateSubjectPriority = teacher.TeacherSubjectPriorities.GroupBy(x => x.Subject.Id).Select(x => new
                {
                    Id = x.Key,
                    Count = x.Count()
                }).Where(x => x.Count > 1).ToList().Count;
                if (countDuplicateSubjectPriority > 0)
                {
                    throw new InvalidDataException("One or more Subject Priority selected one or more time(s).");
                }

                //check if first activity is selected or not
                TeacherSubjectPriority firstPrioritySubject = teacher.TeacherSubjectPriorities.SingleOrDefault(x => x.Priority == 1);
                if (firstPrioritySubject == null)
                {
                    throw new InvalidDataException("You have to select first Subject Priority.");
                }

                //check missing subject priority
                int maxSubjectPriority = teacher.TeacherSubjectPriorities.Max(x=>x.Priority);
                if (maxSubjectPriority != teacher.TeacherSubjectPriorities.Count)
                {
                    throw new InvalidDataException("One or more Subject Priority missing in serial.");
                }
            }
            #endregion

            #region Version Priority
            if (!teacher.TeacherVersionOfStudyPriorities.Any())
            {
                throw new InvalidDataException("No Version Priority selected.");
            }
            else
            {
                //duplicate Version priority check
                int countDuplicateVersionPriority = teacher.TeacherVersionOfStudyPriorities.GroupBy(x => x.VersionOfStudy).Select(x => new
                {
                    Id = x.Key,
                    Count = x.Count()
                }).Where(x => x.Count > 1).ToList().Count;
                if (countDuplicateVersionPriority > 0)
                {
                    throw new InvalidDataException("One or more Version Priority selected one or more time(s).");
                }

                //check if first activity is selected or not
                TeacherVersionOfStudyPriority firstPriorityVersion = teacher.TeacherVersionOfStudyPriorities.SingleOrDefault(x => x.Priority == 1);
                if (firstPriorityVersion == null)
                {
                    throw new InvalidDataException("You have to select first Version Priority.");
                }
            }
            #endregion

            #region room mate, father , mother , personal && alternative mobile semilarity check
            List<long> mobileNumberList = new List<long>
            {
                Convert.ToInt64(teacher.PersonalMobile)
            };
            if (!String.IsNullOrEmpty(teacher.AlternativeMobile))
            {
                mobileNumberList.Add(Convert.ToInt64(teacher.AlternativeMobile));
            }
            if (!String.IsNullOrEmpty(teacher.RoomMatesMobile))
            {
                mobileNumberList.Add(Convert.ToInt64(teacher.RoomMatesMobile));
            }
            if (!String.IsNullOrEmpty(teacher.MothersMobile))
            {
                mobileNumberList.Add(Convert.ToInt64(teacher.MothersMobile));
            }
            if (!String.IsNullOrEmpty(teacher.FathersMobile))
            {
                mobileNumberList.Add(Convert.ToInt64(teacher.FathersMobile));
            }
            int countDuplicateMobileNumber = mobileNumberList.GroupBy(x => x).Select(x => new
            {
                Id = x.Key,
                Count = x.Count()
            }).Where(x => x.Count > 1).ToList().Count;
            if (countDuplicateMobileNumber > 0)
            {
                throw new InvalidDataException("One or More duplicate mobile number found.");
            }
            #endregion

            #endregion

        }

        #endregion
    }
}
