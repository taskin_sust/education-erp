﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Authentication;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class ChildrenAllowanceBlockController : Controller
    {
        #region Logger

        private ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> authorizeMenu;
        private ISession _session;

        private readonly IOrganizationService _organizationService;
        private readonly IChildrenAllowanceSettingService _childrenAllowanceSettingService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IChildrenAllowanceBlockService _allowanceBlockService;
        private readonly IUserService _userService;

        public ChildrenAllowanceBlockController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(_session);
            _childrenAllowanceSettingService = new ChildrenAllowanceSettingService(_session);
            _teamMemberService = new TeamMemberService(_session);
            _allowanceBlockService = new ChildrenAllowanceBlockService(_session);
            _userService = new UserService(_session);
        }

        #endregion

        #region Operational

        #region Save

        [HttpGet]
        public ActionResult Create(string message = "", int? type = 0)
        {
            try
            {
                switch (type)
                {
                    case 1:
                        ViewBag.SuccessMessage = message;
                        break;
                    case 2:
                        ViewBag.ErrorMessage = message;
                        break;
                }
                Initialize();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ChildrenAllowanceBlockViewModel allowanceBlockViewModel)
        {
            var messsage = "";
            try
            {
                if (ModelState.IsValid)
                {
                    authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    ChildrenAllowanceBlock allowanceBlock = FormModelFromVm(allowanceBlockViewModel);
                    _allowanceBlockService.SaveOrUpdate(authorizeMenu, allowanceBlock);
                    messsage = "Operation completed successfully";
                    return RedirectToAction("Create", new { message = messsage, type = 1 /*success*/ });
                }
                IEnumerable<ModelError> modelStateErrors = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
                messsage = modelStateErrors.Aggregate(messsage, (current, modelStateError) => current + (modelStateError.ErrorMessage + " "));
                return RedirectToAction("Create", new { message = messsage, type = 2 /*failure*/ });
            }
            catch (DuplicateEntryException dex)
            {
                _logger.Error(dex);
                messsage = dex.Message;
            }
            catch (InvalidDataException ide)
            {
                _logger.Error(ide);
                messsage = ide.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                messsage = ex.Message;
            }
            return RedirectToAction("Create", new { message = messsage, type = 2 /*fail*/});
        }

        #endregion

        #region Update

        [HttpGet]
        public ActionResult Edit(long id, string message = "", int? type = 0)
        {
            try
            {
                var editType = 0;
                ChildrenAllowanceBlockViewModel allowanceBlockViewModel = null;

                switch (type)
                {
                    case 1:
                        ViewBag.SuccessMessage = "Updated Successfully";
                        break;
                    case 2:
                        ViewBag.ErrorMessage = message;
                        break;
                }

                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                ChildrenAllowanceBlock allowanceBlock = _allowanceBlockService.GetById(id);
                if (allowanceBlock == null)
                {
                    return HttpNotFound("Invalid address/request for allowance block!");
                }

                var salaryHistory =
                    allowanceBlock.TeamMember.SalaryHistory.LastOrDefault(
                        x => x.Status == SalaryHistory.EntityStatus.Active);
                if (salaryHistory == null)
                {
                    throw new Exception("Salary information not found for this team member");
                }
                List<long> authorizedOrgIds = AuthHelper.LoadOrganizationIdList(authorizeMenu,
                    _commonHelper.ConvertIdToList(salaryHistory.SalaryOrganization.Id));
                if (authorizedOrgIds.Count <= 0)
                {
                    throw new InvalidDataException("You are not authorize to perform this oparation!");
                }
                IList<ChildrenAllowanceSetting> childrenAllowanceSettings = _childrenAllowanceSettingService.LoadChilderenAllowanceSetting(0,
                    0, int.MaxValue, authorizeMenu, authorizedOrgIds[0]);
                ViewBag.ckAllowance = new SelectList(childrenAllowanceSettings, "Id", "Name",
                    allowanceBlock.ChildrenAllowanceSetting.Id);
                allowanceBlockViewModel = FormVmFromModel(allowanceBlock);
                editType = CheckAllowanceBlockState(allowanceBlock);
                if (editType == 2)
                {
                    ViewBag.CEName = allowanceBlock.ChildrenAllowanceSetting.Name;
                    allowanceBlockViewModel.ChildrenAllowanceSetting = allowanceBlock.ChildrenAllowanceSetting.Id;
                    return View("Update", allowanceBlockViewModel);
                }
                return View(allowanceBlockViewModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return Index();
            }
        }

        [HttpPost]
        public ActionResult Edit(ChildrenAllowanceBlockViewModel allowanceBlockViewModel)
        {
            var proxyObj = new ChildrenAllowanceBlock();
            var messsage = "";
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                proxyObj = _allowanceBlockService.GetById(allowanceBlockViewModel.Id);
                proxyObj.ChildrenAllowanceSetting = _childrenAllowanceSettingService.GetChildrenAllowanceSetting(allowanceBlockViewModel.ChildrenAllowanceSetting);
                DateTime effectiveDate;
                DateTime.TryParse(allowanceBlockViewModel.EffectiveDate, out effectiveDate);
                if (allowanceBlockViewModel.ClosingDate != null)
                {
                    DateTime closingDate;
                    DateTime.TryParse(allowanceBlockViewModel.ClosingDate, out closingDate);
                    var endOfMonth = new DateTime(closingDate.Year, closingDate.Month, DateTime.DaysInMonth(closingDate.Year, closingDate.Month));

                    proxyObj.ClosingDate = endOfMonth;
                }
                else
                {
                    proxyObj.ClosingDate = null;
                }
                proxyObj.EffectiveDate = effectiveDate;
                _allowanceBlockService.SaveOrUpdate(authorizeMenu, proxyObj, true);
                messsage = "Operation completed successfully";
                return RedirectToAction("Edit", new { id = proxyObj.Id, message = messsage, type = 1 /*success*/ });
            }
            catch (NullObjectException nullObjectException)
            {
                _logger.Error(nullObjectException);
                messsage = nullObjectException.Message;
            }
            catch (InvalidDataException invalidDataException)
            {
                _logger.Error(invalidDataException);
                messsage = invalidDataException.Message;
            }
            catch (DuplicateEntryException duplicateEntryException)
            {
                _logger.Error(duplicateEntryException);
                messsage = duplicateEntryException.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                messsage = ex.Message;
            }
            return RedirectToAction("Edit", new { id = proxyObj.Id, message = messsage, type = 2 /*fail*/});
        }

        #endregion

        #region Details

        public ActionResult Detail(long id)
        {
            try
            {
                ChildrenAllowanceBlock allowanceBlock = _allowanceBlockService.GetById(id);
                var allowanceBlockVm = FormVmFromModel(allowanceBlock);
                return View(allowanceBlockVm);
            }
            catch (InvalidDataException ide)
            {
                _logger.Error(ide);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View(new ChildrenAllowanceBlock());
        }

        #endregion

        #region Delete

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                ChildrenAllowanceBlock allowanceBlock = _allowanceBlockService.GetById(id);
                string endDate = allowanceBlock.ClosingDate != null
                    ? allowanceBlock.ClosingDate.Value.ToShortDateString()
                    : "Infinity";
                string message = "Children Allowance Block \"" + allowanceBlock.ChildrenAllowanceSetting.Name
                                    + "\" from " + allowanceBlock.EffectiveDate.ToShortDateString() + " to " + endDate
                                    + " for Team Member  \"" + allowanceBlock.TeamMember.UserProfile.NickName + "\" Deleted Successfully!";
                _allowanceBlockService.Delete(authorizeMenu, allowanceBlock);

                return Json(new Response(true, message));
            }
            catch (AuthenticationException ae)
            {
                _logger.Error(ae);
                return Json(new Response(false, ae.Message));
            }
            catch (InvalidDataException ide)
            {
                _logger.Error(ide);
                return Json(new Response(false, ide.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
        }

        #endregion

        #endregion

        #region Manage DataTable

        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(authorizeMenu), "Id", "ShortName");
                IList<ChildrenAllowanceSetting> childrenAllowanceSettings = _childrenAllowanceSettingService.LoadChilderenAllowanceSetting(0, 0, int.MaxValue, authorizeMenu);
                ViewBag.ckAllowance = new SelectList(string.Empty, "Value", "Text");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(int draw, int start, int length, string pinNo = "", string blockName = "", string organizationIdStr = "", long? allowanceType = null)
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                long organizationId = !string.IsNullOrEmpty(organizationIdStr) ? Convert.ToInt64(organizationIdStr) : 0;
                IList<ChildrenAllowanceBlockViewModel> allowanceBlocks = _allowanceBlockService.LoadAll(draw, start, length, authorizeMenu, pinNo,
                                                                                                        blockName, organizationId, allowanceType);
                int count = _allowanceBlockService.CountBlockAllowance(authorizeMenu, pinNo, blockName, organizationId, allowanceType);
                var data = new List<object>();
                foreach (var allowance in allowanceBlocks)
                {
                    var str = new List<string>();
                    str.Add(allowance.Pin.ToString(CultureInfo.InvariantCulture));
                    str.Add(allowance.Name);
                    str.Add(allowance.DesignationName);
                    str.Add(allowance.DepartmentName);
                    str.Add(allowance.OrganizationName);
                    str.Add(allowance.AllowanceType);
                    str.Add(allowance.EffectiveDate ?? "");
                    str.Add(allowance.ClosingDate ?? "");
                    str.Add(LinkGenerator.GetDetailsLink("Detail", "ChildrenAllowanceBlock", allowance.Id) +
                            LinkGenerator.GetEditLink("Edit", "ChildrenAllowanceBlock", allowance.Id) +
                            "<a href='#' id='" + allowance.Id + "' class='glyphicon glyphicon-trash'></a>&nbsp ");
                    data.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = count,
                    recordsFiltered = count,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                var data = new List<object>();
                _logger.Error(ex);
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    start = start,
                    length = length,
                    data = data
                });
            }
        }

        public JsonResult OrganizationsAllowance(long id)
        {
            try
            {
                authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                var ids = AuthHelper.LoadOrganizationIdList(authorizeMenu, _commonHelper.ConvertIdToList(id));
                var childrenAllowanceSettings = _childrenAllowanceSettingService.LoadChilderenAllowanceSetting(Convert.ToInt64(ids[0]));
                var selectList = new SelectList(childrenAllowanceSettings.ToList(), "Id", "Name");
                return Json(new { returnList = selectList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred During Load Allowance(s)";
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #region Helper

        private void Initialize()
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.ckAllowance = new SelectList(string.Empty, "Value", "Text");
        }

        private ChildrenAllowanceBlock FormModelFromVm(ChildrenAllowanceBlockViewModel allowanceBlockViewModel)
        {
            var model = new ChildrenAllowanceBlock();
            DateTime effectiveDate;
            DateTime.TryParse(allowanceBlockViewModel.EffectiveDate, out effectiveDate);
            if (allowanceBlockViewModel.ClosingDate != null)
            {
                DateTime closingDate;
                DateTime.TryParse(allowanceBlockViewModel.ClosingDate, out closingDate);
                var endOfMonth = new DateTime(closingDate.Year, closingDate.Month, DateTime.DaysInMonth(closingDate.Year, closingDate.Month));

                model.ClosingDate = endOfMonth;
            }
            else
            {
                model.ClosingDate = null;
            }
            model.EffectiveDate = effectiveDate;
            model.TeamMember = _teamMemberService.GetByPin(allowanceBlockViewModel.Pin);
            model.ChildrenAllowanceSetting = _childrenAllowanceSettingService.GetChildrenAllowanceSetting(allowanceBlockViewModel.ChildrenAllowanceSetting);

            return model;
        }

        private ChildrenAllowanceBlockViewModel FormVmFromModel(ChildrenAllowanceBlock allowanceBlock)
        {

            return new ChildrenAllowanceBlockViewModel()
            {
                Pin = allowanceBlock.TeamMember.Pin,
                Name = allowanceBlock.TeamMember.Name,
                AllowanceType = _childrenAllowanceSettingService.GetChildrenAllowanceSetting(allowanceBlock.ChildrenAllowanceSetting.Id).Name,
                EffectiveDate = String.Format("{0:y}", allowanceBlock.EffectiveDate),
                ClosingDate = allowanceBlock.ClosingDate != null ? String.Format("{0:y}", allowanceBlock.ClosingDate.Value) : string.Empty,
                CreatedBy = _userService.GetByAspNetUser(allowanceBlock.CreateBy).AspNetUser.UserName,
                ModifiedBy = _userService.GetByAspNetUser(allowanceBlock.ModifyBy).AspNetUser.UserName,
                CreationDate = allowanceBlock.CreationDate,
                ModificationDate = allowanceBlock.ModificationDate
            };
        }

        private int CheckAllowanceBlockState(ChildrenAllowanceBlock allowanceBlock)
        {
            int editType = 1;
            var allowanceSheet = _allowanceBlockService.GetTeamMemberLastIsSubmittedAllowanceSheet(allowanceBlock);
            if (allowanceSheet != null && allowanceSheet.DateTo >= allowanceBlock.EffectiveDate)
            {
                editType = 2;
            }
            return editType;
        }

        #endregion

        #region Ajax

        public ActionResult GetCeAllowance(int pin)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    TeamMember teamMember = _teamMemberService.GetMember(pin);
                    if (teamMember != null)
                    {
                        var ehistory = teamMember.EmploymentHistory.Where(x => x.EffectiveDate < DateTime.Now).OrderByDescending(x => x.EffectiveDate).Take(1).SingleOrDefault();
                        IList<ChildrenAllowanceSetting> childrenAllowanceSettings = _childrenAllowanceSettingService.LoadChilderenAllowanceSetting(ehistory.Designation.Organization.Id, ehistory.EmploymentStatus);
                        var childrenAllowanceSettingsList = new SelectList(childrenAllowanceSettings.ToList(), "Id", "Name");
                        return Json(new { returnResult = childrenAllowanceSettingsList, IsSuccess = true });
                    }
                }
                return Json(new { returnResult = "Invalid request", IsSuccess = false });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public ActionResult IsDeletable(long id)
        {
            try
            {
                string message = "Invalid request";
                bool isSuccess = false;
                if (Request.IsAjaxRequest())
                {
                    ChildrenAllowanceBlock allowanceBlock = _allowanceBlockService.GetById(id);
                    if (allowanceBlock == null)
                    {
                        message = "Entity not found!";
                    }
                    else
                    {
                        var allowanceSheet = _allowanceBlockService.GetTeamMemberLastIsSubmittedAllowanceSheet(allowanceBlock);

                        if (allowanceSheet != null && allowanceSheet.DateTo <= allowanceBlock.EffectiveDate)
                        {
                            message = "This entry is not deletable! Member salary sheet already generated with this entry!";
                        }
                        else
                        {
                            message = "Entity Deletable";
                            isSuccess = true;
                        }
                    }
                }
                return Json(new { Text = message, IsSuccess = isSuccess });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

    }
}