using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {


    public partial class DiscountMap : BaseClassMap<Discount, long>
    {
        
        public DiscountMap() {
			Table("Discount");
			LazyLoad();
			//Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            
            Map(x => x.Name);
            Map(x => x.Amount).Column("Amount");
			Map(x => x.StartDate).Column("StartDate");
			Map(x => x.EndDate).Column("EndDate");

            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");

            HasMany(x => x.DiscountDetails).KeyColumn("DiscountId").Cascade.All();
        }
    }
}
