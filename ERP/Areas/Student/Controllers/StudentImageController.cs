﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using log4net;
using Microsoft.Ajax.Utilities;
using NHibernate;
using Org.BouncyCastle.Apache.Bzip2;
using UdvashERP.App_code;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.Student;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class StudentImageController : Controller
    {
        #region Objects/Propertise/Services/Dao & Initialization

        private ILog logger = LogManager.GetLogger("StudentArea");
        private readonly IStudentImageService _studentImageService;
        private readonly IProgramService _programService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly ISessionService _sessionService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IBatchService _batchService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IStudentImagesMediaService _studentImagesMediaService;
        private readonly IUserService _userService;
        public StudentImageController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _studentImageService = new StudentImageService(session);
            _programService = new ProgramService(session);
            _studentProgramService = new StudentProgramService(session);
            _sessionService = new SessionService(session);
            _branchService = new BranchService(session);
            _campusService = new CampusService(session);
            _batchService = new BatchService(session);
            _commonHelper = new CommonHelper();
            _userService = new UserService(session);
            _organizationService = new OrganizationService(session);
            _studentImagesMediaService = new StudentImagesMediaService();
        }

        #endregion

        #region Image Upload

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ContentResult CheckFile(string name, string type, int size)
        {
            try
            {
                var contentTypeArray = name.Split('.');
                var extension = contentTypeArray[contentTypeArray.Length - 1];
                var message = _studentImageService.CheckFileSettingsForTypeAndSize(name, extension, size);
                return Content(message);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                logger.Error(ex);
                return Content(WebHelper.SetExceptionMessage(ex));
            }
        }

        [HttpPost]
        public JsonResult FileUpload(HttpPostedFileBase upl, bool overwrite)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var rsp = _studentImageService.FileUpload(upl, overwrite, userMenu);
                return Json(rsp);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                logger.Error(ex);
                return Json(WebHelper.SetExceptionMessage(ex));
            }
        }

        #endregion

        #region New Image Status Report

        public ActionResult ImageStatus()
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.BranchId = new SelectList((new List<Branch>()), "Value", "Text");
                ViewBag.CampusId = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.ImageStatus = new SelectList(_commonHelper.LoadEmumToDictionary<StudentImageStatus>(), "Key", "Value");
                //ViewBag.ImageStatus = _commonHelper.GetImageStatus();
                return View();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return null;
            }
        }

        [HttpPost]
        public ActionResult ImageStatus(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays = null, string[] batchTime = null, int? isMissing = 1, string[] informationViewList = null)
        {

            ViewBag.programId = programId;
            ViewBag.sessionId = sessionId;
            ViewBag.branchId = branchId;
            ViewBag.campusId = campusId;
            ViewBag.batchName = batchName;
            ViewBag.batchDays = batchDays;
            ViewBag.batchTime = batchTime;
            ViewBag.isMissing = isMissing;
            ViewBag.informationViewList = informationViewList;
            ViewBag.statusVal = statusVal;
            var program = _programService.GetProgram(programId);
            var session = _sessionService.LoadById(sessionId);
            var batchsName = "ALL";
            var branchsName = "ALL";
            var batchsDays = "ALL";
            var campussName = "ALL";

            if (!batchDays.Contains("0"))
            {

                batchsDays = string.Join("; ", batchDays);
            }

            if (!batchName.Contains(0))
            {
                IList<Batch> batches = _batchService.LoadBatch(null, null, null, null, null, batchName.ToList());
                batchsName = string.Join(", ", batches.Select(x => x.Name.Trim()).Distinct().ToList());

            }

            if (!branchId.Contains(0))
            {
                IList<Branch> branches = _branchService.LoadBranch(null, null, null, branchId.ToList());
                branchsName = string.Join(", ", branches.Select(x => x.Name.Trim()).Distinct().ToList());
            }


            if (!campusId.Contains(0))
            {
                IList<Campus> campuses = _campusService.LoadCampus(null, null, null, null, campusId.ToList());
                campussName = string.Join(", ", campuses.Select(x => x.Name.Trim()).Distinct().ToList());
            }
            ViewBag.ProgramSession = program.Name + "-" + session.Name;
            ViewBag.Branchs = branchsName;
            ViewBag.BatchsName = batchsName;
            ViewBag.BatchsDays = batchsDays;
            ViewBag.CampussName = campussName;
            return View("ImageStatusPost");
        }

        [HttpPost]
        public ActionResult ImageStatusReport(int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays = null, string[] batchTime = null, int? isMissing = null, string[] informationViewList = null, string programRoll = "", string nickName = "", string mobileNumber = "")
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<dynamic> imageStatusList = _studentProgramService.LoadStudentImageByVariousCriteria(userMenu, start, length, programId, sessionId, branchId, campusId, batchName, statusVal, batchDays, batchTime, programRoll, nickName, mobileNumber);

                int recordsTotal = _studentProgramService.CountStudentImageByVariousCriteria(userMenu, programId, sessionId, branchId, campusId, batchName, statusVal, batchDays, batchTime, programRoll, nickName, mobileNumber);

                IList<ImageStatusViewModel> imageStatusViewModels = new List<ImageStatusViewModel>();
                imageStatusViewModels = GenerateImageStatusReport(imageStatusList);
                long recordsFiltered = recordsTotal;
                var data = new List<object>();
                int sl = start + 1;
                foreach (var imageStatusObj in imageStatusViewModels)
                {
                    //Batch b = _batchService.GetBatch(Convert.ToInt64(imageStatusObj.BatchId));
                    string createdBy = "-";
                    string modifiedBy = "-";
                    string image = "-";
                    if (!String.IsNullOrEmpty(imageStatusObj.StudentImageId))
                    {
                        StudentProgramImage studentProgramImage = _studentImageService.GetStudentProgramImage(Convert.ToInt64(imageStatusObj.StudentImageId));
                        if (studentProgramImage != null)
                        {
                            createdBy = _userService.GetUserNameByAspNetUserId(studentProgramImage.CreateBy);
                            modifiedBy = _userService.GetUserNameByAspNetUserId(studentProgramImage.ModifyBy);
                            StudentMediaImage mediaStudentImages = _studentImagesMediaService.GetStudentImageMediaByRefId(Convert.ToInt64(imageStatusObj.StudentImageId));
                            string imageBase64 = mediaStudentImages != null ? Convert.ToBase64String(mediaStudentImages.Images) : null;
                            string imageSrc = imageBase64 != null ? string.Format("data:image/gif;base64,{0}", imageBase64) : "";
                            if (!String.IsNullOrEmpty(imageSrc))
                            {
                                image = "<img src='" + imageSrc + "' height='100' />";
                            }
                        }
                    }
                    var str = new List<object>();
                    str.Add(sl.ToString());
                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case "Program Roll":
                                    str.Add(imageStatusObj.ProgramRoll);
                                    break;
                                case "Nick Name":
                                    str.Add(imageStatusObj.NickName);
                                    break;
                                case "Full Name":
                                    str.Add(imageStatusObj.FullName);
                                    break;
                                case "Institute":
                                    str.Add(imageStatusObj.Institute);
                                    break;
                                case "Mobile Number(Personal)":
                                    str.Add(imageStatusObj.PersonalMobile);
                                    break;
                                case "Mobile Number(Father)":
                                    str.Add(imageStatusObj.FatherMobile);
                                    break;
                                case "Mobile Number(Mother)":
                                    str.Add(imageStatusObj.MotherMobile);
                                    break;
                                case "Branch":
                                    str.Add(imageStatusObj.Branch);
                                    break;
                                case "Campus":
                                    str.Add(imageStatusObj.Campus);
                                    break;
                                case "BatchDays":
                                    str.Add(imageStatusObj.BatchDays);
                                    break;
                                case "BatchTime":
                                    str.Add(imageStatusObj.BatchTime);
                                    break;
                                case "BatchName":
                                    str.Add(imageStatusObj.BatchName);
                                    break;
                                case "Image":
                                    str.Add(image);
                                    break;
                                case "Upload By":
                                    str.Add(createdBy);
                                    break;
                                case "Updated By":
                                    str.Add(modifiedBy);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    sl++;
                    data.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
        }

        private IList<ImageStatusViewModel> GenerateImageStatusReport(IList<dynamic> imageStatusList)
        {
            IList<ImageStatusViewModel> imageStatusViewModels = new List<ImageStatusViewModel>();
            foreach (var obj in imageStatusList)
            {
                IDictionary<string, object> dic;
                var imageStatus = new ImageStatusViewModel();
                foreach (KeyValuePair<string, object> entry in dic = obj)
                {
                    string defaultValue = entry.Key;
                    switch (defaultValue)
                    {
                        case "PROGRAM ROLL":
                            {
                                imageStatus.ProgramRoll = entry.Value != null ? entry.Value.ToString() : "";
                            }
                            break;
                        case "NICK NAME":
                            {
                                imageStatus.NickName = entry.Value != null ? entry.Value.ToString() : "";
                            }
                            break;
                        case "FULL NAME":
                            {
                                imageStatus.FullName = entry.Value != null ? entry.Value.ToString() : "";
                            }
                            break;
                        case "MOBILE NUMBER(PERSONAL)":
                            {
                                imageStatus.PersonalMobile = entry.Value != null ? entry.Value.ToString() : "";
                            }
                            break;
                        case "MOBILE NUMBER(FATHER)":
                            {
                                imageStatus.FatherMobile = entry.Value != null ? entry.Value.ToString() : "";
                                break;
                            }

                        case "MOBILE NUMBER(MOTHER)":
                            {
                                imageStatus.MotherMobile = entry.Value != null ? entry.Value.ToString() : "";
                                break;
                            }
                        case "BRANCH":
                            {
                                imageStatus.Branch = entry.Value != null ? entry.Value.ToString() : "";
                                break;
                            }
                        case "CAMPUS NAME":
                            {
                                imageStatus.Campus = entry.Value != null ? entry.Value.ToString() : "";
                                break;
                            }
                        case "BATCH DAYS":
                            {
                                imageStatus.BatchDays = entry.Value != null ? entry.Value.ToString() : "";
                                break;
                            }
                        //case "BATCH TIME":
                        //    {
                        //        imageStatus.BatchTime = entry.Value != null ? entry.Value.ToString() : "";
                        //        break;
                        //    }
                        case "BatchTime":
                            {
                                imageStatus.BatchTime = entry.Value != null ? entry.Value.ToString() : "";
                                break;
                            }
                        case "BATCH NAME":
                            {
                                imageStatus.BatchName = entry.Value != null ? entry.Value.ToString() : "";
                                break;
                            }
                        //case "BatchName":
                        //    {
                        //        imageStatus.BatchName = entry.Value != null ? entry.Value.ToString() : "";
                        //        break;
                        //    }
                        case "INSTITUTE":
                            {
                                imageStatus.Institute = entry.Value != null ? entry.Value.ToString() : "";
                                break;
                            }
                        //case "BATCH ID":
                        case "BatchId":
                            {
                                imageStatus.BatchId = entry.Value != null ? entry.Value.ToString() : "";
                                break;

                            }
                        case "Image":
                            {
                                imageStatus.StudentImageId = entry.Value != null ? entry.Value.ToString() : "";
                                break;

                            }
                        case "Upload By":
                            {
                                imageStatus.StudentImageModifiedBy = entry.Value != null ? entry.Value.ToString() : "";
                                break;

                            }
                        case "Updated By":
                            {
                                imageStatus.StudentImageCreatedBy = entry.Value != null ? entry.Value.ToString() : "";
                                break;

                            }

                        default: { break; }

                    }
                }
                imageStatusViewModels.Add(imageStatus);
            }
            return imageStatusViewModels;
        }

        public JsonResult CountResult(long programId, long sessionId, string statusVal, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            int count = _studentProgramService.CountStudentImageByVariousCriteria(userMenu, programId, sessionId, branchId, campusId, batchId, statusVal, batchDays, batchTime); //batchDays, batchTime, batchId);
            return Json(count);
        }

        public ActionResult ImageStatusReportExcel(string programSessionHeader, string branchNameHeader, string campusHeader, string batchDaysHeader, string batchHeader, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string statusVal, string[] batchDays = null, string[] batchTime = null, int? isMissing = null, string[] informationViewList = null, string programRoll = "", string nickName = "", string mobileNumber = "")
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //IList<dynamic> imageStatusList = _studentProgramService.LoadStudentImageByVariousCriteria(userMenu, 0, 0,
                //    Int32.MaxValue, programId, sessionId, branchId, campusId,
                //    batchName, statusVal, batchDays, batchTime, isMissing, programRoll, nickName, mobileNumber, "Excel");
                IList<dynamic> imageStatusList = _studentProgramService.LoadStudentImageByVariousCriteria(userMenu, 0, Int32.MaxValue, programId, sessionId, branchId, campusId,
                   batchName, statusVal, batchDays, batchTime, programRoll, nickName, mobileNumber, "Excel");
                IList<ImageStatusViewModel> imageStatusViewModels = new List<ImageStatusViewModel>();
                imageStatusViewModels = GenerateImageStatusReport(imageStatusList);

                List<string> headerList = new List<string>();
                headerList.Add(programSessionHeader);
                headerList.Add("Student's Image Report");
                headerList.Add("Branch: " + branchNameHeader);
                headerList.Add("Campus: " + campusHeader);
                headerList.Add("Batch Days: " + batchDaysHeader);
                headerList.Add("Batch Name: " + batchHeader);
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();

                if (informationViewList != null)
                {
                    foreach (var information in informationViewList)
                    {
                        switch (information)
                        {
                            case "Image":
                                break;
                            case "Upload By":
                                break;
                            case "Updated By":
                                break;
                            default:
                                columnList.Add(information);
                                break;
                        }
                    }
                }
                var transactionXlsList = new List<List<object>>();
                foreach (var datareportMeritList in imageStatusViewModels)
                {
                    var str = new List<object>();
                    #region InfomationViewList

                    if (informationViewList != null)
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case "Program Roll":
                                    if (!string.IsNullOrEmpty(datareportMeritList.ProgramRoll))
                                    {
                                        str.Add(datareportMeritList.ProgramRoll);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Nick Name":
                                    if (!string.IsNullOrEmpty(datareportMeritList.NickName))
                                    {
                                        str.Add(datareportMeritList.NickName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Full Name":
                                    if (!string.IsNullOrEmpty(datareportMeritList.FullName))
                                    {
                                        str.Add(datareportMeritList.FullName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Institute":
                                    if (!string.IsNullOrEmpty(datareportMeritList.Institute))
                                    {
                                        str.Add(datareportMeritList.Institute);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number(Personal)":
                                    if (!string.IsNullOrEmpty(datareportMeritList.PersonalMobile))
                                    {
                                        str.Add(datareportMeritList.PersonalMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number(Father)":
                                    if (!string.IsNullOrEmpty(datareportMeritList.FatherMobile))
                                    {
                                        str.Add(datareportMeritList.FatherMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number(Mother)":
                                    if (!string.IsNullOrEmpty(datareportMeritList.MotherMobile))
                                    {
                                        str.Add(datareportMeritList.MotherMobile);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Branch":
                                    if (!string.IsNullOrEmpty(datareportMeritList.Branch))
                                    {
                                        str.Add(datareportMeritList.Branch);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Campus":
                                    if (!string.IsNullOrEmpty(datareportMeritList.Campus))
                                    {
                                        str.Add(datareportMeritList.Campus);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "BatchDays":
                                    if (!string.IsNullOrEmpty(datareportMeritList.BatchDays))
                                    {
                                        str.Add(datareportMeritList.BatchDays);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "BatchTime":
                                    //if (!string.IsNullOrEmpty(datareportMeritList.BatchId))
                                    //{
                                    //    Batch b = _batchService.GetBatch(Convert.ToInt64(datareportMeritList.BatchId));
                                    //    str.Add(b.FormatedBatchTime);
                                    //    break;
                                    //}
                                    //str.Add("");
                                    str.Add(datareportMeritList.BatchTime);
                                    break;
                                case "BatchName":
                                    if (!string.IsNullOrEmpty(datareportMeritList.BatchName))
                                    {
                                        str.Add(datareportMeritList.BatchName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                //case "Image":
                                //    break;
                                //case "Upload By":
                                //    break;
                                //case "Updated By":
                                //    break;
                                default:
                                    break;
                            }
                        }
                    }
                    transactionXlsList.Add(str);
                    #endregion
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "ImageStatusReport__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("Autoclose");
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Missing & Existing Image Counter Reports

        [HttpGet]
        public ActionResult MissingImage()
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.BranchId = new SelectList((new List<Branch>()), "Value", "Text");
                ViewBag.CampusId = new SelectList(new List<Campus>(), "Id", "Name");
                //ViewBag.ImageStatus = new SelectList(_commonHelper.GetImageStatus());
                ViewBag.ImageStatus = new SelectList(_commonHelper.LoadEmumToDictionary<StudentImageStatus>(), "Key", "Value");
                return View();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return null;
            }
        }

        [HttpPost]
        public JsonResult AjaxRequestForImageMissingCount(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                int imageMissingCount = _studentImageService.ImageCount(userMenu, programId, sessionId, branchId, campusId, batchId, batchDays, batchTime, true);
                int imageExistingCount = _studentImageService.ImageCount(userMenu, programId, sessionId, branchId, campusId, batchId, batchDays, batchTime, false);
                return Json(new { missingImageCount = imageMissingCount, existingImageCount = imageExistingCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpPost]
        public JsonResult AjaxRequestForImageExistingCount(long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //set Authorize branch. when all branch is seleced 
                if ((Array.Exists(branchId, item => item == 0)))
                {

                    IList<Branch> authorizedBranchList = _branchService.LoadAuthorizedBranch(userMenu);
                    if (authorizedBranchList != null && authorizedBranchList.Count > 0)
                        branchId = (from x in authorizedBranchList select x.Id).ToArray();
                    else
                        branchId = new[] { -1l };
                }
                int existingImageCount = _studentImageService.ImageCount(userMenu, programId, sessionId, branchId, campusId, batchId, batchDays, batchTime, false);

                return Json(new { existingImageCount = existingImageCount, IsSuccess = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        [HttpGet]
        public ActionResult MissingImageReport()
        {
            return RedirectToAction("MissingImage");
        }

        [HttpGet]
        public ActionResult ExistingImageReport()
        {
            return RedirectToAction("MissingImage");
        }

        #region missing image report

        [HttpGet]
        public ActionResult GenerateMissingImageReport()
        {
            return RedirectToAction("MissingImage");
        }

        [HttpPost]
        public ActionResult GenerateMissingImageReport(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string[] batchDays = null, string[] batchTime = null, int? isMissing = null)
        {
            try
            {
                ViewBag.programId = programId;
                ViewBag.sessionId = sessionId;
                ViewBag.branchId = branchId;
                ViewBag.campusId = campusId;
                ViewBag.batchName = batchName;
                ViewBag.batchDays = batchDays;
                ViewBag.batchTime = batchTime;
                ViewBag.isMissing = isMissing;
                var program = _programService.GetProgram(programId);
                var session = _sessionService.LoadById(sessionId);
                var batchsName = "ALL";
                var branchsName = "ALL";
                var batchsDays = "ALL";
                var campussName = "ALL";

                if (!batchDays.Contains("0"))
                {

                    batchsDays = string.Join("; ", batchDays);
                }

                if (!batchName.Contains(0))
                {
                    IList<Batch> batches = _batchService.LoadBatch(null, null, null, null, null, batchName.ToList());
                    batchsName = string.Join(", ", batches.Select(x => x.Name.Trim()).Distinct().ToList());

                }

                if (!branchId.Contains(0))
                {
                    IList<Branch> branches = _branchService.LoadBranch(null, null, null, branchId.ToList());
                    branchsName = string.Join(", ", branches.Select(x => x.Name.Trim()).Distinct().ToList());
                }


                if (!campusId.Contains(0))
                {
                    IList<Campus> campuses = _campusService.LoadCampus(null, null, null, null, campusId.ToList());
                    campussName = string.Join(", ", campuses.Select(x => x.Name.Trim()).Distinct().ToList());
                }
                ViewBag.ProgramSession = program.Name + "-" + session.Name;
                ViewBag.Branchs = branchsName;
                ViewBag.BatchsName = batchsName;
                ViewBag.BatchsDays = batchsDays;
                ViewBag.CampussName = campussName;
            }
            catch (Exception ex)
            {

                // _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult MissingImageReport(int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string[] batchDays = null, string[] batchTime = null, int? isMissing = null)
        {

            if (Request.IsAjaxRequest())
            {
                try
                {
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    IList<MissingExistingImageDto> misssingImageReports = new List<MissingExistingImageDto>();
                    int recordsTotal = 0;

                    misssingImageReports = _studentImageService.LoadImageReports(start, length, userMenu, programId,
                        sessionId, branchId, campusId, batchName, batchDays, batchTime, Convert.ToBoolean(isMissing));
                    recordsTotal = _studentImageService.LoadImageReportsRowCount(userMenu, programId, sessionId,
                        branchId, campusId, batchName, batchDays, batchTime, Convert.ToBoolean(isMissing));

                    long recordsFiltered = recordsTotal;
                    var data = new List<object>();
                    int sl = start + 1;
                    foreach (var c in misssingImageReports)
                    {
                        var batch = _batchService.GetBatch(c.BatchId);
                        var str = new List<string>();
                        str.Add(sl.ToString());
                        str.Add(batch.Branch.Name);
                        str.Add(batch.Name);
                        str.Add(batch.FormatedBatchTime);
                        str.Add(c.PrnNo);
                        sl++;
                        data.Add(str);
                    }

                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }


                catch (Exception ex)
                {
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }

            }
            else { return Json(HttpNotFound()); }


        }

        [HttpGet]
        public ActionResult MissingImageReportToExcel(string programSessionHeader, string branchNameHeader, string campusHeader, string batchDaysHeader, string batchHeader, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string[] batchDays = null, string[] batchTime = null, int? isMissing = null)
        {
            try
            {
                var headerList = new List<string>();
                var columnList = new List<string>();
                var footerList = new List<string>();
                headerList.Add(programSessionHeader);
                headerList.Add("Image Report");
                headerList.Add(branchNameHeader);
                headerList.Add(campusHeader);
                headerList.Add(batchDaysHeader);
                headerList.Add(batchHeader);
                columnList.Add("Branch");
                columnList.Add("Batch");
                columnList.Add("Batch Time");
                columnList.Add("Program Roll");
                footerList.Add("");
                var imageReportXlsList = new List<List<object>>();

                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<MissingExistingImageDto> misssingImageReports = new List<MissingExistingImageDto>();
                misssingImageReports = _studentImageService.LoadImageReports(0, 0, userMenu, programId,
                        sessionId, branchId, campusId, batchName, batchDays, batchTime, Convert.ToBoolean(isMissing));
                foreach (var c in misssingImageReports)
                {
                    var xlsRow = new List<object>();
                    var batch = _batchService.GetBatch(c.BatchId);
                    xlsRow.Add(batch.Branch.Name);
                    xlsRow.Add(batch.Name);
                    xlsRow.Add(batch.FormatedBatchTime);
                    xlsRow.Add(c.PrnNo);
                    imageReportXlsList.Add(xlsRow);
                }

                ExcelGenerator.GenerateExcel(headerList, columnList, imageReportXlsList, footerList, "image-report-list__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");

            }

            catch (Exception ex)
            {
                return View("AutoClose");
            }
        }

        #endregion

        [HttpPost]
        public ActionResult ExistingImageReport(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, string[] batchDays = null, string[] batchTime = null)
        {
            try
            {
                //set Authorize branch. when all branch is seleced 
                //if ((Array.Exists(branchId, item => item == 0)))
                //{
                //    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                //    IList<Branch> authorizedBranchList = _branchService.LoadAuthorizedBranch(userMenu);
                //    if (authorizedBranchList != null && authorizedBranchList.Count > 0)
                //        branchId = (from x in authorizedBranchList select x.Id).ToArray();
                //    else
                //        branchId = new[] { -1l };
                //}
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<StudentImageMissing> existingImageReports = new List<StudentImageMissing>();
                //_studentImageService.LoadImageReports(userMenu, programId, sessionId, branchId, campusId, batchName, batchDays, batchTime, false);
                // List<StudentImageMissing> existingImageReports = _studentImageService.LoadExistingImageReports(programId, sessionId, branchId, campusId, batchName, batchDays, batchTime);

                ViewBag.ReportInfo = existingImageReports;
                return View();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return null;
            }
        }

        #endregion

        #region Image Status

        [HttpGet]
        public ActionResult StuedentImageStatusSearch()
        {
            return View();
        }

        [HttpPost]
        public ActionResult StuedentImageStatusSearch(string ProgramRoll, HttpPostedFileBase file)
        {
            try
            {
                if (ProgramRoll == null)
                {
                    ViewBag.ErrorMessage = "Program Roll is not assigned.";
                    return View();
                }

                ProgramRoll = ProgramRoll.Trim();

                if (ProgramRoll.Length < 11 || ProgramRoll.Length > 11)
                {
                    ViewBag.ErrorMessage = "Program Roll length is invalid. It will be exact 11 digit";
                    return View();
                }

                long insertedProNo = -1;
                long.TryParse(ProgramRoll, out insertedProNo);
                if (insertedProNo <= 0)
                {
                    ViewBag.ErrorMessage = "Program Roll is not valid. It will be number.";
                    return View();
                }

                //Check if this student is under authorizied branch to this AppUser
                StudentProgram spDatabase = _studentProgramService.GetStudentProgram(ProgramRoll);

                if (spDatabase == null || spDatabase.Id <= 0)
                {
                    ViewBag.InfoMessage = "Student not found.";
                    return View();
                }


                //check authorization
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var authrizationMessage = _studentProgramService.CheckAuthorizationForStudentProgram(userMenu, spDatabase);
                if (authrizationMessage != "")
                {
                    ViewBag.ErrorMessage = authrizationMessage;
                    return View();
                }


                //Upload new image IF selected
                string message = "";
                if (file != null && file.ContentLength > 0)
                {
                    message = _studentImageService.FileUpload(ProgramRoll, file, true, userMenu);
                }


                spDatabase = _studentProgramService.GetStudentProgram(ProgramRoll);
                if (spDatabase != null && spDatabase.Id > 0)
                {
                    StudentSingleImageView ssiv = new StudentSingleImageView();
                    ssiv.StudentId = spDatabase.Student.Id;
                    ssiv.ProgramRoll = spDatabase.PrnNo;
                    ssiv.StudentName = spDatabase.Student.NickName;
                    ssiv.FatherName = spDatabase.Student.FatherName;
                    ssiv.MobileNo = spDatabase.Student.Mobile;
                    ssiv.BranchName = spDatabase.Batch.Branch.Name;
                    ssiv.BatchName = spDatabase.Batch.Name;

                    ssiv.SchoolCollege = "";
                    if (spDatabase.Institute != null)
                        ssiv.SchoolCollege = spDatabase.Institute.Name;
                    //if (spDatabase.Student.StudentAcademicInfos != null && spDatabase.Student.StudentAcademicInfos.Count > 0)
                    //    ssiv.SchoolCollege = spDatabase.Student.StudentAcademicInfos[spDatabase.Student.StudentAcademicInfos.Count - 1].Name;

                    if (spDatabase.StudentImage != null && spDatabase.StudentImage.Count > 0)
                    {
                        //calling ums_erp_media db for image viewing 
                        {
                            StudentMediaImage mediaStudentImages = _studentImagesMediaService.GetStudentImageMediaByRefId(spDatabase.StudentImage[0].Id);
                            ssiv.StuentImage = mediaStudentImages != null ? mediaStudentImages.Images : null;
                        }
                    }
                    else
                    {
                        Image imageIn = Image.FromFile(Server.MapPath("~/Content/Image/No_Image.png"));
                        MemoryStream ms = new MemoryStream();
                        imageIn.Save(ms, imageIn.RawFormat);
                        ssiv.StuentImage = ms.ToArray();
                    }


                    if (file != null && message != null && message.Length <= 10)
                        ViewBag.SuccessMessage = "Student image successfully uploaded";
                    else if (file != null && message != null && message.Length > 10)
                        ViewBag.ErrorMessage = message;

                    return View("StuedentImageUpload", ssiv);
                }
                else
                {
                    ViewBag.InfoMessage = "Student not found.";
                    return View();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return View();
            }
        }

        #endregion

        #region Image Delete

        [HttpPost]
        public ActionResult StuedentImageDelete(string programRoll)
        {
            try
            {
                if (String.IsNullOrEmpty(programRoll))
                {
                    throw new MessageException("Invalid Program Roll.");
                }
                // var studentProgram = _studentProgramService.GetStudentProgram(programRoll);
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;

                _studentImageService.Delete(programRoll, userMenu);
                return Json(new Response(true, "Image Delete Successful"));
            }
            catch (MessageException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Image Download

        public ActionResult ImageDownload()
        {
            ViewBag.OrganizationId = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.BranchId = new SelectList((new List<Branch>()), "Value", "Text");
            ViewBag.CampusId = new SelectList(new List<Campus>(), "Id", "Name");
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                return View();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return View();
            }
        }

        [HttpPost]
        public ActionResult ImageDownload(long[] programId, long[] sessionId, long[] branchId, long[] campusId, long[] batchName, string[] batchDay = null, string[] batchTime = null)
        {
            ViewBag.OrganizationId = new SelectList(new List<Organization>(), "Id", "Name");
            ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.BranchId = new SelectList((new List<Branch>()), "Value", "Text");
            ViewBag.CampusId = new SelectList(new List<Campus>(), "Id", "Name");
            try
            {
                var zipFileName = "student-images__"
                                  + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".zip";
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var studentProgramIdList =
                    _studentProgramService.LoadStudentProgramWithImage(userMenu, programId, sessionId,
                        branchId, campusId, batchName, batchDay, batchTime).Distinct().ToList();
                MemoryStream outputMemStream = new MemoryStream();
                ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);
                zipStream.SetLevel(3); //0-9, 9 being the highest level of compression
                foreach (var spId in studentProgramIdList)
                {
                    var studentProgram = _studentProgramService.GetStudentProgram(spId);
                    if (studentProgram.StudentImage != null && studentProgram.StudentImage.Count > 0)
                    {
                        StudentMediaImage mediaStudentImages =
                                        _studentImagesMediaService.GetStudentImageMediaByRefId(studentProgram.StudentImage[0].Id);
                        if (mediaStudentImages != null)
                        {
                            var newEntry = new ZipEntry(studentProgram.PrnNo + ".png");
                            newEntry.DateTime = DateTime.Now;
                            zipStream.PutNextEntry(newEntry);
                            byte[] bytes = mediaStudentImages.Images;
                            MemoryStream inStream = new MemoryStream(bytes);
                            StreamUtils.Copy(inStream, zipStream, new byte[bytes.Length]);
                            inStream.Close();
                            zipStream.CloseEntry();
                        }
                    }
                }
                zipStream.IsStreamOwner = false; // False stops the Close also Closing the underlying stream.
                zipStream.Close(); // Must finish the ZipOutputStream before using outputMemStream.
                outputMemStream.Position = 0;
                return File(outputMemStream.ToArray(), "application/octet-stream", zipFileName);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return View();
            }
        }
        
        #endregion

        #region Helper Function

        #endregion
    }
}