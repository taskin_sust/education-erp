using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;
namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class MemberEbfBalanceFactory : ObjectFactoryBase<MemberEbfBalance>
    {

        private readonly IMemberEbfBalanceService memberEbfBalanceService;

        public MemberEbfBalanceFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {

            memberEbfBalanceService = new MemberEbfBalanceService(session);

        }

        #region Create

        public MemberEbfBalanceFactory Create()
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberEbfBalanceFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public MemberEbfBalanceFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

               // SingleObjectList.ForEach(x => { if (x.Id == 0) memberEbfBalanceService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

               // ObjectList.ForEach(x => { if (x.Id == 0)memberEbfBalanceService.SaveOrUpdate(x); });

            }
            return this;
        }

        public MemberEbfBalanceFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new MemberEbfBalance() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public MemberEbfBalanceFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion

    }
}
