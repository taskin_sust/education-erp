﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Hr")]
    public class HolidayWorkController : Controller
    {

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion
        
        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _userMenu;
        private readonly IHolidayWorkService _holidayWorkService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IOrganizationService _organizationService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IDepartmentService _departmentService;
        private readonly IUserService _userService;
        public HolidayWorkController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _holidayWorkService = new HolidayWorkService(session);
                _teamMemberService = new TeamMemberService(session);
                _organizationService = new OrganizationService(session);
                _branchService = new BranchService(session);
                _campusService = new CampusService(session);
                _departmentService = new DepartmentService(session);
                _userService = new UserService(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        #endregion
        
        public ActionResult Index()
        {
            return View();
        }

        #region Holidday Work Approval By Mentor

        public ActionResult MentorHolidayWorkApproval()
        {
            ViewBag.ShowNodata = false;
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-01");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            return View();
        }

        [HttpPost]
        public ActionResult MentorHolidayWorkApproval(HolidayWorkFormViewModel data)
        {
            ViewBag.ShowNodata = true;
            ViewBag.holidayWorkApprovalList = new List<HolidayApprovalDto>();
            ViewBag.DateFrom = data.FromDate.ToString("yyyy-MM-dd");
            ViewBag.DateTo = data.ToDate.ToString("yyyy-MM-dd");
            try
            {
                TeamMember mentor = _teamMemberService.GetCurrentMember();
                if (mentor == null)
                    throw new InvalidDataException("This User has no Team member.");
                ViewBag.DateFrom = data.FromDate.ToString("yyyy-MM-dd");
                ViewBag.DateTo = data.ToDate.ToString("yyyy-MM-dd");
                if (data.FromDate > data.ToDate)
                    throw new InvalidDataException("From Date must be smaller then the To date");
                if (ModelState.IsValid)
                {
                    string pinList = (!String.IsNullOrEmpty(data.PinList)) ? data.PinList.Trim() : "";
                    Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    pinList = regex.Replace(pinList, @",");
                    List<HolidayApprovalDto> holidayWorkApprovalList = _holidayWorkService.LoadMentorHolidayWorkApproval(mentor, data.FromDate, data.ToDate, pinList).ToList();
                    ViewBag.holidayWorkApprovalList = holidayWorkApprovalList;
                }
                else
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                }
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }
        
        #endregion

        #region Holiday Approval Common Function

        [HttpPost]
        public ActionResult HolidayWorkApproval(List<HolidayApprovalDto> holidayWorkApprovalViewModelList, bool isHr = false, bool isMentor = false)
        {
            bool successState = false;
            string message = "";
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                TeamMember member = _teamMemberService.GetCurrentMember();
                List<HolidayWork> holoWorkList = new List<HolidayWork>();
                if (member == null)
                {
                    throw new InvalidDataException("This TeamMmber Has No User");
                }
                if (holidayWorkApprovalViewModelList.Any())
                {
                    foreach (var holidayWorkViewModel in holidayWorkApprovalViewModelList)
                    {
                        HolidayWork holidayWorkObj = new HolidayWork();
                        holidayWorkObj.TeamMember = _teamMemberService.GetByPin(holidayWorkViewModel.Pin);
                        holidayWorkObj.HolidayWorkDate = Convert.ToDateTime(holidayWorkViewModel.HolidayWorkDate).Date;
                        if (holidayWorkViewModel.HolidayWorkId != null && holidayWorkViewModel.HolidayWorkId > 0)
                        {
                            holidayWorkObj.Id = Convert.ToInt64(holidayWorkViewModel.HolidayWorkId);
                        }
                        else
                        {
                            holidayWorkObj.Rank = 10;
                            holidayWorkObj.Status = HolidayWork.EntityStatus.Active;
                        }
                        if (Convert.ToInt32(HolidayWorkApprovalStatus.Full) == Convert.ToInt32(holidayWorkViewModel.ApprovalType))
                            holidayWorkObj.ApprovalType = Convert.ToInt32(HolidayWorkApprovalStatus.Full);
                        else if (Convert.ToInt32(HolidayWorkApprovalStatus.Half) == Convert.ToInt32(holidayWorkViewModel.ApprovalType))
                            holidayWorkObj.ApprovalType = Convert.ToInt32(HolidayWorkApprovalStatus.Half);
                        else
                            holidayWorkObj.ApprovalType = null;
                        holidayWorkObj.Reason = holidayWorkViewModel.Reason;
                        holoWorkList.Add(holidayWorkObj);
                    }
                }

                if (holoWorkList.Any())
                {
                    if (_holidayWorkService.SaveHolidayWorkApproval(holoWorkList, member, isMentor, isHr, _userMenu))
                    {
                        successState = true;
                        message = holoWorkList.Count() + " Holiday Work(s) Successfully Saved.";
                    }
                }
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        #endregion
        
        #region Holiday work Approval by HR

        public ActionResult HrHolidayWorkApproval()
        {
            ViewBag.ShowNodata = false;
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.organizationlist = new SelectList(new List<Organization>(), "Id", "ShortName");
            ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.CampusList = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.DepartmentList = new SelectList(new List<Department>(), "Id", "Name"); 
            ViewBag.DateFrom = DateTime.Today.ToString("yyyy-MM-01");
            ViewBag.DateTo = DateTime.Today.ToString("yyyy-MM-dd");
            try
            {
                List<SelectListItem> organizationSelectList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName").ToList();
                organizationSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Organization" });
                ViewBag.organizationlist = new SelectList(organizationSelectList, "Value", "Text");

                List<SelectListItem> branchSelectList = new SelectList(new List<Branch>(), "Id", "Name").ToList();
                branchSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Branch" });
                ViewBag.BranchList = new SelectList(branchSelectList, "Value", "Text");

                List<SelectListItem> campusSelectList = new SelectList(new List<Campus>(), "Id", "Name").ToList();
                campusSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Campus" });
                ViewBag.CampusList = new SelectList(campusSelectList, "Value", "Text");

                List<SelectListItem> departmentSelectList = new SelectList(new List<Department>(), "Id", "Name").ToList();
                departmentSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Department" });
                ViewBag.DepartmentList = new SelectList(departmentSelectList, "Value", "Text");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult HrHolidayWorkApproval(HolidayWorkHrFormViewModel data)
        {
            ViewBag.ShowNodata = true;
            ViewBag.organizationlist = new SelectList(new List<Organization>(), "Id", "ShortName");
            ViewBag.BranchList = new SelectList(new List<Branch>(), "Id", "Name");
            ViewBag.CampusList = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.DepartmentList = new SelectList(new List<Department>(), "Id", "Name"); 
            try
            {
                if (ModelState.IsValid)
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    
                    ViewBag.DateFrom = data.FromDate.ToString("yyyy-MM-dd");
                    ViewBag.DateTo = data.ToDate.ToString("yyyy-MM-dd");

                    string pinList = (!String.IsNullOrEmpty(data.PinList)) ? data.PinList.Trim() : "";
                    Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    pinList = regex.Replace(pinList, @",");

                    long organizationId = data.OrganizationId;
                    long branchId = data.BranchId ?? 0;
                    long campusId = data.CampusId ?? 0;
                    long departmentId = data.DepartmentId ?? 0;
                    pinList = regex.Replace(pinList, @",");
                    
                    List<SelectListItem> organizationSelectList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", organizationId).ToList();
                    ViewBag.organizationlist = new SelectList(organizationSelectList, "Value", "Text", organizationId);

                    List<SelectListItem> branchSelectList = new SelectList(_branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(organizationId), null, null, false), "Id", "Name", branchId).ToList();
                    branchSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All" });
                    ViewBag.BranchList = new SelectList(branchSelectList, "Value", "Text", branchId);

                    List<SelectListItem> campusSelectList = new SelectList(_campusService.LoadAuthorizeCampus(_userMenu, _commonHelper.ConvertIdToList(organizationId), null, (branchId > 0) ? _commonHelper.ConvertIdToList(branchId) : null, null, false), "Id", "Name", campusId).ToList();
                    campusSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All" });
                    ViewBag.CampusList = new SelectList(campusSelectList, "Value", "Text", campusId);

                    List<SelectListItem> departmentSelectList = new SelectList(_departmentService.LoadDepartment(_commonHelper.ConvertIdToList(organizationId)), "Id", "Name", departmentId).ToList();
                    departmentSelectList.Insert(0, new SelectListItem() { Value = "0", Text = "All" });
                    ViewBag.DepartmentList = new SelectList(departmentSelectList, "Value", "Text", departmentId);


                    if (data.FromDate > data.ToDate)
                    {
                        throw new InvalidDataException("From Date must be smaller then the To date");
                    }

                    List<HolidayApprovalDto> holidayWorkApprovalList = _holidayWorkService.LoadHrHolidayWorkApproval(_userMenu, data.FromDate, data.ToDate, pinList, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(branchId), _commonHelper.ConvertIdToList(campusId), _commonHelper.ConvertIdToList(departmentId)).ToList();
                    ViewBag.holidayWorkApprovalList = holidayWorkApprovalList;
                }
                else
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                }
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }

            return View();
        }

        #region Holiday Work History

        public ActionResult HrHolidayWorkHistory()
        {
            try
            {
                SelectList branch = null, organization = null, campus = null;
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.organizationlist = organization;
                ViewBag.BranchId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.CampusId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.DepartmentId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.dateFromField = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
                ViewBag.dateToField = DateTime.Now.ToString("yyyy-MM-dd");

                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;

                return PartialView("_partial/_HolidayWorkApprovalHistory");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult LoadHolidayWorkHistory(int draw, int start, int length, long? organizationId, long? departmentId, long? branchId, long? campusId, string pin, string dateFrom, string dateTo)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";


                    DateTime dateFromDateTime = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).Date;
                    DateTime dateToDateTime = DateTime.Today.Date;
                    if (!String.IsNullOrEmpty(dateFrom))
                    {
                        dateFromDateTime = Convert.ToDateTime(dateFrom).Date;
                    }
                    if (!String.IsNullOrEmpty(dateTo))
                    {
                        dateToDateTime = Convert.ToDateTime(dateTo).Date;
                    }

                    if (dateFromDateTime > dateToDateTime)
                    {
                        DateTime temp = dateToDateTime;
                        dateToDateTime = dateFromDateTime;
                        dateFromDateTime = temp;
                    }

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                                orderBy = "Pin";
                                break;
                            case "1":
                                orderBy = "Name";
                                break;
                            case "2":
                                orderBy = "DepartmentName";
                                break;
                            case "3":
                                orderBy = "OrganizationShortName";
                                break;
                            case "4":
                                orderBy = "BranchName";
                                break;
                            case "5":
                                orderBy = "CampusName";
                                break;
                            case "6":
                                orderBy = "HolidayWorkDate";
                                break;
                            case "7":
                                orderBy = "InTime";
                                break;
                            case "8":
                                orderBy = "OutTime";
                                break;
                            case "9":
                                orderBy = "ApprovalType";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }

                    }
                    #endregion

                    int recordsTotal = _holidayWorkService.GetHrHolidayWorkApprovalHistoryRowCount(userMenu, dateFromDateTime, dateToDateTime, pin, organizationId, branchId, campusId, departmentId);
                    long recordsFiltered = recordsTotal;
                    IList<HolidayApprovalDto> dataList = _holidayWorkService.LoadHrHolidayWorkApprovalHistory(start, length, orderBy, orderDir, userMenu, dateFromDateTime, dateToDateTime, pin, organizationId, branchId, campusId, departmentId);
                    var data = new List<object>();
                    foreach (var c in dataList)
                    {
                        var str = new List<string>();
                        str.Add(c.Pin.ToString());
                        str.Add(c.Name);
                        str.Add(c.DepartmentName);
                        str.Add(c.OrganizationShortName);
                        str.Add(c.BranchName);
                        str.Add(c.CampusName);
                        str.Add(Convert.ToDateTime(c.HolidayWorkDate).ToString("yyyy-MM-dd"));
                        str.Add(c.InTime.ToString("hh:mm tt"));
                        str.Add(c.OutTime.ToString("hh:mm tt"));
                        str.Add((c.ApprovalType!=null)?(c.ApprovalType==Convert.ToInt32(HolidayWorkApprovalStatus.Full))?"Full":"Half":"No");
                        str.Add(c.ModifyBy != null ? _userService.GetUserNameByAspNetUserId((long)c.ModifyBy) : "");
                        str.Add(CommonHelper.GetReportCommonDateTimeFormat(c.LastModificationDate));
                        str.Add(c.Reason);
                        data.Add(str);
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });

                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return HttpNotFound();
        }

        #endregion

        #endregion

    }
}