﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.DirectGoodsReceive
{
    public interface IDirectGoodsReceiveSaveOrUpdateTest
    {
        #region Basic Test

        void Should_Return_Branch_Null_Exception();
        void Should_Return_Supplier_Null_Exception();
        void Should_Return_Item_Null_Exception();
        void Should_Return_Branch_Null_Exception_For_GetCurrentGoodsReceivedNoByBranchAndOrg();
        void Should_Return_Save_Successfully();


        #endregion

        #region Business Logic

        void Should_Return_Invalid_Data_Exception_For_TotalCost();
        void Should_Return_Receive_Quantity_Exception();
        void Should_Return_Receive_Rate_Exception();

        #region Skip

        //void Should_Return_Invalid_PurposeProgram();
        //void Should_Return_Invalid_PurposeSession();
        //void Should_Return_Invalid_PurposeProgram_For_NotApplicablePurpose();
        //void Should_Return_Invalid_PurposeSession_For_NotApplicablePurpose();

        #endregion

        void Should_Successfully_Check_ProgramItem_With_AnyPurpose();
        void Should_Successfully_Check_ProgramItem_With_NotApplicablePurpose();
        void Should_Successfully_Check_CommonItem_With_NotApplicablePurpose();
        void Should_Successfully_Check_CommonItem_With_FirstThreePurpose();
        void Should_Successfully_Check_CommonItem_With_LastTwoPurpose();

        #endregion

    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "GoodsReceive")]
    public class DirectGoodsReceiveSaveTest : DirectGoodsReceiveBaseTest, IDirectGoodsReceiveSaveOrUpdateTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Branch_Null_Exception()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(organizationFactory.Object, commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();

            //receive
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Object.Branch = null;
            //            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object);

            Assert.Throws<NullObjectException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        [Fact]
        public void Should_Return_Supplier_Null_Exception()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(organizationFactory.Object, commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();
            //receive
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Object.Supplier = null;

            Assert.Throws<NullObjectException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        [Fact]
        public void Should_Return_Item_Null_Exception()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object).Object);
            Assert.Throws<NullObjectException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        [Fact]
        public void Should_Return_Save_Successfully()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithPurpose().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);
            Assert.Equal(_goodsReceiveFactory.Object.Id, _goodsReceiveService.GetGoodsReceive(_goodsReceiveFactory.Object.Id).Id);
        }

        [Fact]
        public void Should_Return_Branch_Null_Exception_For_GetCurrentGoodsReceivedNoByBranchAndOrg()
        {
            Assert.Throws<NullObjectException>(() => _goodsReceiveService.GetCurrentGoodsReceivedNoByBranchAndOrg(null));
        }

        #endregion

        #region Business Logic

        [Fact]
        public void Should_Return_Invalid_Data_Exception_For_TotalCost()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(organizationFactory.Object, commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), branchFactory.Object);

            // direct workorder
            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create().CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList).Persist();
            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _directWorkOrderFactory.Create().WithBranch(branchFactory.Object)
                  .WithSupplier(_supplierFactory.Object)
                  .WithWorkOrderDetails(_itemFactory.Object, _programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session, _directWorkOrderFactory.Object)
                  .Persist();
            //receive
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier(_supplierFactory.Object).WithWorkOrder(_directWorkOrderFactory.Object);
            _goodsReceiveDetailsFactory.Create().WithWorkOrderDetails(_directWorkOrderFactory.Object.WorkOrderDetails[0]).WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveFactory.Object.TotalCost = -5;

            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        [Fact]
        public void Should_Return_Receive_Quantity_Exception()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveDetailsFactory.Object.ReceivedQuantity = 0;
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        [Fact]
        public void Should_Return_Receive_Rate_Exception()
        {
            _goodsReceiveFactory.Create().WithBranch().WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession().WithItem().WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveDetailsFactory.Object.Rate = 0;
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            Assert.Throws<InvalidDataException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        }

        #region Old And Skip

        //[Fact]
        //public void Should_Return_Invalid_PurposeProgram()
        //{
        //    _organizationFactory.Create().Persist();
        //    _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
        //    _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object)
        //        .WithSpecifationTemplate().Persist();
        //    _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();
        //    _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
        //    _goodsReceiveDetailsFactory.Create().WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
        //        .WithItem(_itemFactory.Object).WithPurpose(Purpose.AdministrativePurpose).WithGoodsReceive(false, _goodsReceiveFactory.Object);
        //    _goodsReceiveDetailsFactory.Object.PurposeProgramId = null;
        //    _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);

        //    Assert.Throws<InvalidDataException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        //}

        //[Fact]
        //public void Should_Return_Invalid_PurposeSession()
        //{
        //    _organizationFactory.Create().Persist();
        //    _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
        //    _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object)
        //        .WithSpecifationTemplate().Persist();
        //    _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();
        //    _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
        //    _goodsReceiveDetailsFactory.Create().WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
        //        .WithItem(_itemFactory.Object).WithPurpose(Purpose.AdministrativePurpose).WithGoodsReceive(false, _goodsReceiveFactory.Object);
        //    _goodsReceiveDetailsFactory.Object.PurposeSessionId = null;
        //    _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);

        //    Assert.Throws<InvalidDataException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        //}

        //[Fact]
        //public void Should_Return_Invalid_PurposeProgram_For_NotApplicablePurpose()
        //{
        //    _organizationFactory.Create().Persist();
        //    _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
        //    _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object)
        //        .WithSpecifationTemplate().Persist();
        //    _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();
        //    _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
        //    _goodsReceiveDetailsFactory.Create().WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
        //        .WithItem(_itemFactory.Object).WithPurpose(Purpose.AdministrativePurpose).WithGoodsReceive(false, _goodsReceiveFactory.Object);
        //    _goodsReceiveDetailsFactory.Object.PurposeProgramId = _programSessionItemFactory.Object.Program;
        //    _goodsReceiveDetailsFactory.Object.Purpose = null;
        //    _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);

        //    Assert.Throws<InvalidDataException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        //}

        //[Fact]
        //public void Should_Return_Invalid_PurposeSession_For_NotApplicablePurpose()
        //{
        //    _organizationFactory.Create().Persist();
        //    _branchFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
        //    _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(_organizationFactory.Object)
        //        .WithSpecifationTemplate().Persist();
        //    _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(_organizationFactory.Object).Persist();
        //    _goodsReceiveFactory.Create().WithBranch(_branchFactory.Object).WithSupplier();
        //    _goodsReceiveDetailsFactory.Create().WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
        //        .WithItem(_itemFactory.Object).WithPurpose(Purpose.AdministrativePurpose).WithGoodsReceive(false, _goodsReceiveFactory.Object);
        //    _goodsReceiveDetailsFactory.Object.PurposeSessionId = _programSessionItemFactory.Object.Session;
        //    _goodsReceiveDetailsFactory.Object.Purpose = null;
        //    _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);

        //    Assert.Throws<InvalidDataException>(() => _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false));
        //}

        #endregion

        [Fact]
        public void Should_Successfully_Check_ProgramItem_With_AnyPurpose()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object)
                .WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.AdministrativePurpose).WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Equal(_goodsReceiveDetailsFactory.Object.PurposeProgramId.Id, _goodsReceiveDetailsFactory.Object.Program.Id);
            Assert.Equal(_goodsReceiveDetailsFactory.Object.PurposeSessionId.Id, _goodsReceiveDetailsFactory.Object.Session.Id);
        }

        [Fact]
        public void Should_Successfully_Check_ProgramItem_With_NotApplicablePurpose()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object)
                .WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(SelectionType.NotApplicable).WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Equal(_goodsReceiveDetailsFactory.Object.PurposeProgramId, null);
            Assert.Equal(_goodsReceiveDetailsFactory.Object.PurposeSessionId, null);
        }

        [Fact]
        public void Should_Successfully_Check_CommonItem_With_NotApplicablePurpose()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.CommonItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object)
                .WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).Persist();
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object).WithPurpose(SelectionType.NotApplicable).WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Equal(_goodsReceiveDetailsFactory.Object.PurposeProgramId, null);
            Assert.Equal(_goodsReceiveDetailsFactory.Object.PurposeSessionId, null);
        }

        [Fact]
        public void Should_Successfully_Check_CommonItem_With_FirstThreePurpose()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.CommonItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object)
                .WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).Persist();
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession()
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose).WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Equal(_goodsReceiveDetailsFactory.Object.PurposeProgramId, _goodsReceiveDetailsFactory.Object.PurposeProgramId);
            Assert.Equal(_goodsReceiveDetailsFactory.Object.PurposeSessionId, _goodsReceiveDetailsFactory.Object.PurposeSessionId);
        }

        [Fact]
        public void Should_Successfully_Check_CommonItem_With_LastTwoPurpose()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.CommonItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object)
                .WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).Persist();
            _goodsReceiveFactory.Create().WithBranch(branchFactory.Object).WithSupplier();
            _goodsReceiveDetailsFactory.Create().WithProgram().WithSession()
                .WithItem(_itemFactory.Object).WithPurpose(Purpose.ProgramPurpose).WithGoodsReceive(false, _goodsReceiveFactory.Object);
            _goodsReceiveFactory.Object.GoodsReceiveDetails.Add(_goodsReceiveDetailsFactory.Object);
            _goodsReceiveService.SaveOrUpdate(_goodsReceiveFactory.Object, false);

            Assert.Equal(_goodsReceiveDetailsFactory.Object.Program, null);
            Assert.Equal(_goodsReceiveDetailsFactory.Object.Session, null);
        }

        #endregion
    }
}
