﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
   public class SurveyQuestionAnswerFactory : ObjectFactoryBase<SurveyQuestionAnswer>
   {
       #region Object Initialization

       public SurveyQuestionAnswerFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)       
       {
           
       }
       #endregion

       #region Create
       public SurveyQuestionAnswerFactory CreateMore(int numberOfanswer, SurveyQuestion surveyQuestion)
       {
           CreateAnswerList(numberOfanswer, surveyQuestion);
           return this;
       }

       private IList<SurveyQuestionAnswer> CreateAnswerList(int numberOfanswer, SurveyQuestion surveyQuestion) 
       {
           ListStartIndex = ObjectList.Count;
           var list = new List<SurveyQuestionAnswer>();

           for (int i = 0; i < numberOfanswer; i++)
           {
               var surveyQuestionAnswer = new SurveyQuestionAnswer()
               {
                   Id = 0,
                   BusinessId = GetPcUserName(),
                   Name = Name,
                   CreateBy = 1,
                   ModifyBy = 1,
                   Answer = "Answer_" + (i + 1),
                   Status = CampusRoom.EntityStatus.Active,
                   SurveyQuestion = surveyQuestion
               };
               list.Add(surveyQuestionAnswer);
               
           }
           ObjectList.AddRange(list);
           return list;
       }

       #endregion

       #region Cleanup
           
        public void Cleanup()
           {
               DeleteAll();
           }
       #endregion
   }
}
