using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface ISpecificationCriteriaDao : IBaseDao<SpecificationCriteria, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion
    }

    public class SpecificationCriteriaDao : BaseDao<SpecificationCriteria, long>, ISpecificationCriteriaDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        #endregion

        #region Helper Function

        #endregion
    }
}
