﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Base;
using UdvashERP.Dao.LogBaseDao;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface ILeaveApplicationLogDao : ILogBaseDao<LeaveApplicationLog, long>
    {

    }
    public class LeaveApplicationLogDao : LogBaseDao<LeaveApplicationLog, long>, ILeaveApplicationLogDao
    {

    }
}
