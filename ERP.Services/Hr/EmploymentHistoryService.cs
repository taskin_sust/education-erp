﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
//using InvalidDataException = System.IO.InvalidDataException;

namespace UdvashERP.Services.Hr
{
    public interface IEmploymentHistoryService : IBaseService
    {
        #region Operational Function

        bool Save(EmploymentHistory hrEmploymentHistory);
        bool AddEmploymentHistory(TeamMember member, long organizationId, long branchId, long campusId, long departmentId, long designationId, int memberType, string effectiveDate);
        bool UpdateEmploymentHistory(EmploymentHistory employmentHistory, long campusId, long departmentId, long designationId, int memberType, string effectiveDate);
        bool DeleteEmploymentHistory(EmploymentHistory employmentHistory);

        #endregion

        #region Single Instances Loading Function

        int GetCurrentEmployementStatus(long memId);
        EmploymentHistory GetEmploymentHistory(long id);
        EmploymentHistory GetTeamMemberEmploymentHistory(DateTime? searchingDateTime = null, long? teamMemberId = null, int? teamMemberPin = null);
        EmploymentHistory GetTeamMemberFirstMonth(long teamMemberId, int currentMonth, int currentYear);

        #endregion

        #region List Loading Function

        List<EmploymentHistory> LoadEmploymentHistories(TeamMember teamMember, DateTime? searchingDate = null);

        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion
        
    }
    public class EmploymentHistoryService : BaseService, IEmploymentHistoryService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IEmploymentHistoryDao _employmentHistoryDao;
        private readonly ICampusDao _campusDao;
        private readonly IDepartmentDao _departmentDao;
        private readonly IDesignationDao _designationDao;
        private readonly ITeamMemberDao _teamMemberDao;
        public EmploymentHistoryService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _employmentHistoryDao = new EmploymentHistoryDao(){Session = session};
            _campusDao = new CampusDao { Session = session };
            _departmentDao = new DepartmentDao { Session = session };
            _designationDao = new DesignationDao { Session = session };
            _teamMemberDao = new TeamMemberDao { Session = session };
        }

        #endregion

        #region Operational Functions

        public bool Save(EmploymentHistory hrEmploymentHistory)
        {
            ITransaction trans = null;
            try
            {
                if (hrEmploymentHistory.Campus == null || hrEmploymentHistory.Department == null
                    || hrEmploymentHistory.Designation == null || hrEmploymentHistory.TeamMember == null)
                {
                    throw new NullReferenceException("Null Ref found!");
                }
                if (hrEmploymentHistory.EmploymentStatus <= 0)
                    throw new InvalidDataException("Invalid Member Type found!");
                if (hrEmploymentHistory.EffectiveDate == null)
                    throw new InvalidDataException("Invalid Effective Date");

                using (trans = Session.BeginTransaction())
                {
                    _employmentHistoryDao.Save(hrEmploymentHistory);
                    var hrEmploymentHistoryLog = new EmploymentHistoryLog();
                    hrEmploymentHistoryLog.EmploymentStatus = hrEmploymentHistory.EmploymentStatus;
                    hrEmploymentHistoryLog.EffectiveDate = hrEmploymentHistory.EffectiveDate;
                    //hrEmploymentHistoryLog.LeaveDate = Convert.ToDateTime(dateTo);
                    hrEmploymentHistoryLog.CampusId = hrEmploymentHistory.Campus.Id;
                    hrEmploymentHistoryLog.DesignationId = hrEmploymentHistory.Designation.Id;
                    hrEmploymentHistoryLog.DepartmentId = hrEmploymentHistory.Department.Id;
                    hrEmploymentHistoryLog.TeamMemberId = hrEmploymentHistory.TeamMember.Id;
                    hrEmploymentHistoryLog.EmploymentHistoryId = hrEmploymentHistory.Id;
                    //save hrmember history log
                    hrEmploymentHistoryLog.CreationDate = DateTime.Now;
                    hrEmploymentHistoryLog.CreateBy = GetCurrentUserId();
                    Session.Save(hrEmploymentHistoryLog);
                    //save hrmember history log
                    trans.Commit();
                    return true;
                }
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (NullReferenceException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if(trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        public bool AddEmploymentHistory(TeamMember member, long organizationId, long branchId, long campusId, long departmentId,
            long designationId, int memberType, string effectiveDate)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    CheckBeforeAdd(member, campusId, departmentId, designationId, memberType, effectiveDate);
                    Campus campus = _campusDao.LoadById(campusId);
                    Department department = _departmentDao.LoadById(departmentId);
                    Designation designation = _designationDao.LoadById(designationId);

                    EmploymentHistory empHistory = new EmploymentHistory();
                    empHistory.TeamMember = member;
                    empHistory.Campus = campus;
                    empHistory.Department = department;
                    empHistory.Designation = designation;
                    empHistory.EmploymentStatus = Convert.ToInt32(memberType);
                    empHistory.EffectiveDate = Convert.ToDateTime(effectiveDate);
                    empHistory.Status = EmploymentHistory.EntityStatus.Active;
                    empHistory.Rank = 10;
                    _employmentHistoryDao.SaveOrUpdate(empHistory);
                    SaveEmploymentHistoryLog(empHistory);
                    trans.Commit();
                    return true;
                }
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (NullReferenceException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool UpdateEmploymentHistory(EmploymentHistory employmentHistory, long campusId, long departmentId, long designationId,
            int memberType, string effectiveDate)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    var canLogAdd = false;
                    CheckBeforeAdd(employmentHistory.TeamMember, campusId, departmentId, designationId, memberType, effectiveDate, employmentHistory.Id);
                    Campus campus = _campusDao.LoadById(campusId);
                    Department department = _departmentDao.LoadById(departmentId);
                    Designation designation = _designationDao.LoadById(designationId);
                    if (employmentHistory.Campus.Id != campusId || employmentHistory.Department.Id != departmentId ||
                        employmentHistory.Designation.Id != designationId ||
                        employmentHistory.EmploymentStatus != memberType ||
                        employmentHistory.EffectiveDate.Date != Convert.ToDateTime(effectiveDate).Date)
                    {
                        canLogAdd = true;
                    }

                    employmentHistory.Campus = campus;
                    employmentHistory.Department = department;
                    employmentHistory.Designation = designation;
                    employmentHistory.EmploymentStatus = Convert.ToInt32(memberType);
                    employmentHistory.EffectiveDate = Convert.ToDateTime(effectiveDate);
                    _employmentHistoryDao.SaveOrUpdate(employmentHistory);
                    if (canLogAdd)
                    {
                        SaveEmploymentHistoryLog(employmentHistory);    
                    }
                    
                    trans.Commit();
                    return true;
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool DeleteEmploymentHistory(EmploymentHistory employmentHistory)
        {
            ITransaction trans = null;
            try
            {
                CheckBeforeDelete(employmentHistory.TeamMember);
                using (trans = Session.BeginTransaction())
                {
                    employmentHistory.Status = -404;
                    _employmentHistoryDao.SaveOrUpdate(employmentHistory);
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        private void CheckBeforeDelete(TeamMember member)
        {
            int numberOfEmploymentHistory =
                member.EmploymentHistory.Where(x => x.Status != EmploymentHistory.EntityStatus.Delete).ToList().Count;
            if (numberOfEmploymentHistory <= 1)
            {
                throw new DependencyException("Team member must have atleast one emplyment history!!");
            }
        }

        private void CheckBeforeAdd(TeamMember member, long campusId, long departmentId, long designationId, int memberType, string effectiveDate, long id = 0)
        {
            Campus campus = _campusDao.LoadById(campusId);
            Department department = _departmentDao.LoadById(departmentId);
            Designation designation = _designationDao.LoadById(designationId);
            if (member == null)
                throw new InvalidDataException("No Member found!");
            if (campus == null)
                throw new InvalidDataException("No Campus found!");
            if (department == null)
                throw new InvalidDataException("No Department found!");
            if (designation == null)
                throw new InvalidDataException("No Designation found!");
            if (memberType <= 0)
                throw new InvalidDataException("Invalid Member Type found!");
            if (Convert.ToDateTime(effectiveDate) == null)
                throw new InvalidDataException("Invalid Effective Date");
            int dublicateDataForSameEffectiveDate = member.EmploymentHistory.Where(x => x.EffectiveDate.Date == Convert.ToDateTime(effectiveDate).Date && x.Id != id && x.Status != EmploymentHistory.EntityStatus.Delete).ToList().Count();
            if (dublicateDataForSameEffectiveDate >= 1)
            {
                throw new InvalidDataException("Already there is an Employment history data for this effective day");
            }

            Organization currentOrganization = _teamMemberDao.GetTeamMemberOrganization(Convert.ToDateTime(effectiveDate), member.Id, null);
            if (currentOrganization != null && currentOrganization.Id != department.Organization.Id)
            {
                throw new InvalidDataException("This is a transfered record, you can't chnage it!");
            }
        }

        private void SaveEmploymentHistoryLog(EmploymentHistory empHistory)
        {
            try
            {
                var hrEmploymentHistoryLog = new EmploymentHistoryLog();
                hrEmploymentHistoryLog.EmploymentStatus = empHistory.EmploymentStatus;
                hrEmploymentHistoryLog.EffectiveDate = empHistory.EffectiveDate;
                hrEmploymentHistoryLog.CampusId = empHistory.Campus.Id;
                hrEmploymentHistoryLog.DesignationId = empHistory.Designation.Id;
                hrEmploymentHistoryLog.DepartmentId = empHistory.Department.Id;
                hrEmploymentHistoryLog.TeamMemberId = empHistory.TeamMember.Id;
                hrEmploymentHistoryLog.EmploymentHistoryId = empHistory.Id;
                hrEmploymentHistoryLog.CreationDate = DateTime.Now;
                hrEmploymentHistoryLog.CreateBy = hrEmploymentHistoryLog.CreateBy != 0 ? hrEmploymentHistoryLog.CreateBy : GetCurrentUserId();
                Session.Save(hrEmploymentHistoryLog);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        
        #endregion

        #region Single Instances Loading Function

        public EmploymentHistory GetTeamMemberFirstMonth(long tmId, int currentMonth, int currentYear)
        {
            try
            {
                return _employmentHistoryDao.GetTeamMemberFirstMonth(tmId, currentMonth, currentYear);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public EmploymentHistory GetEmploymentHistory(long id)
        {
            return
                Session.QueryOver<EmploymentHistory>().Where(x => x.Id == id).SingleOrDefault<EmploymentHistory>();
        }

        public EmploymentHistory GetTeamMemberEmploymentHistory(DateTime? searchingDateTime = null, long? teamMemberId = null,
            int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId != null || teamMemberPin != null)
                {                                 
                    DateTime? date = DateTime.Now;

                    if (searchingDateTime != null)
                        date = searchingDateTime;
                    return _employmentHistoryDao.GetTeamMemberEmploymentHistory(date, teamMemberId, teamMemberPin);
                }
                else
                {
                    throw new MessageException("Team Member Id and Team Member Pin is empty");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
           
        }

        public int GetCurrentEmployementStatus(long memId)
        {
            IList<EmploymentHistory> hrEmploymentHistories = _employmentHistoryDao.LoadByMemberId(memId,DateTime.Now);

            if (hrEmploymentHistories.Count > 0) {
                return hrEmploymentHistories.OrderByDescending(h => h.EffectiveDate).FirstOrDefault().EmploymentStatus;
            }

            //foreach (var hrEmploymentHistory in hrEmploymentHistories.OrderByDescending(x=> x.Id))
            //{
            //    if (hrEmploymentHistory.EffectiveDate <= DateTime.Now)
            //    {
            //        if (hrEmploymentHistory.EmploymentStatus != null) return (int) hrEmploymentHistory.EmploymentStatus;
            //    }
            //    continue;
            //}
            return -404;
        }

        #endregion

        #region List Loading Function

        public List<EmploymentHistory> LoadEmploymentHistories(TeamMember teamMember, DateTime? searchingDate = null)
        {
            try
            {
                if (searchingDate != null)
                    searchingDate = searchingDate.Value.Date;
                return _employmentHistoryDao.LoadByMemberId(teamMember.Id, searchingDate).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper function



        #endregion

      
    }
}
