﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Hr;
using System.Collections;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class ShiftFactory : ObjectFactoryBase<Shift>
    {

        #region Object Initialization

        protected readonly IShiftService _shiftService;
        private OrganizationFactory _organizationFactory;
        private ISession _session;
        public ShiftFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _session = session;
            _shiftService = new ShiftService(_session);
            _organizationFactory = new OrganizationFactory(this, _session);
        }
        #endregion

        #region Create
        
        public ShiftFactory Create()
        {
            DateTime convertStartTime = Convert.ToDateTime("2001-01-01 09:00:00");
            DateTime endTime = convertStartTime.AddHours(9).AddMinutes(1);
            Object = new Shift
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid().ToString(),
                Rank = 10,
                StartTime = convertStartTime,
                EndTime = endTime,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Shift.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public ShiftFactory CreateMore(int numberOfShift)
        {
            CreateShiftList(numberOfShift);
            return this;
        }
        public ShiftFactory CreateMore(int numberOfShift, Organization organization)
        {
            CreateShiftList(numberOfShift, organization);
            return this;
        }

        public ShiftFactory CreateMore(int numberOfShift, List<Organization> organizations)
        {
            CreateShiftList(numberOfShift, organizations);
            return this;
        }

        private List<Shift> CreateShiftList(int numberOfShift, List<Organization> organizations)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Shift>();
            foreach (var organization in organizations)
            {
                for (int i = 0; i < numberOfShift; i++)
                {
                    DateTime convertStartTime = Convert.ToDateTime("2001-01-01 09:00:00");
                    DateTime endTime = convertStartTime.AddHours(9).AddMinutes(i + 1);
                    var shift = new Shift
                    {
                        Id = 0,
                        Name = Guid.NewGuid().ToString() + "_" + i,
                        Rank = i,
                        StartTime = convertStartTime,
                        EndTime = endTime,
                        CreateBy = 1,
                        ModifyBy = 1,
                        Status = Shift.EntityStatus.Active
                    };
                    shift.Organization = organization;
                    list.Add(shift);
                }
                ObjectList.AddRange(list);
            }
            
            return list;
        }

        private List<Shift> CreateShiftList(int numberOfShift, Organization organization)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Shift>();
            var orgNamePrefix = Guid.NewGuid().ToString();
            for (int i = 0; i < numberOfShift; i++)
            {
                DateTime convertStartTime = Convert.ToDateTime("2001-01-01 09:00:00");
                DateTime endTime = convertStartTime.AddHours(9).AddMinutes(i + 1);
                var shift = new Shift
                {
                    Id = 0,
                    Name = Guid.NewGuid().ToString() + "_" + i,
                    Rank = i,
                    StartTime = convertStartTime,
                    EndTime = endTime,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Shift.EntityStatus.Active
                };
                shift.Organization = organization;
                list.Add(shift);
            }
            ObjectList.AddRange(list);
            return list;
        }

        private List<Shift> CreateShiftList(int numberOfShift)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Shift>();
            var orgNamePrefix = Guid.NewGuid().ToString();
            _organizationFactory.Create()
                .WithNameAndRank(orgNamePrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"), 1)
                .Persist();

            for (int i = 0; i < numberOfShift; i++)
            {
                DateTime convertStartTime = Convert.ToDateTime("2001-01-01 09:00:00");
                DateTime endTime = convertStartTime.AddHours(9).AddMinutes(i + 1);
                var shift = new Shift
                {
                    Id = 0,
                    Name = Guid.NewGuid().ToString() + "_" + i,
                    Rank = i,
                    StartTime = convertStartTime,
                    EndTime = endTime,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Shift.EntityStatus.Active
                };
                shift.Organization = _organizationFactory.Object;
                list.Add(shift);
            }
            ObjectList.AddRange(list);
            return list;
        }

        public ShiftFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }

        public ShiftFactory WithStartTimeEndTime(DateTime startTime, DateTime endtime)
        {
            Object.StartTime = startTime;
            Object.EndTime = endtime;
            return this;
        }
        public ShiftFactory WithStartTimeEndTime()
        {
            Object.StartTime = Convert.ToDateTime("2001-01-01 07:00:00");
            var endTime = Object.StartTime.Value.AddHours(new Random().Next(8, 12));
            bool isContinue = true;
            bool isStart = false;
            while (isContinue)
            {
                foreach (Shift shift in SingleObjectList)
                {
                    if (shift.EndTime == endTime)
                    {
                        endTime = endTime.AddHours(1);
                        isStart = true;
                        break;
                    }
                }
                if (!isStart)
                    isContinue = false;
            }
            Object.EndTime = endTime;
            return this;
        }
        public ShiftFactory WithOrganization()
        {
            _organizationFactory.Create().Persist();
            Object.Organization = _organizationFactory.Object;
            return this;
        }

        public ShiftFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public ShiftFactory WithOrganization(string orgName, int rank, string shortName, string contact, decimal basicSalary, decimal houseRent, decimal medicalAllowance, decimal convenyance, int monthlyWorkingDay, decimal deductionPerAbsent)
        {
            _organizationFactory.CreateWith(orgName, rank, shortName, contact, basicSalary, houseRent, medicalAllowance, convenyance, monthlyWorkingDay, deductionPerAbsent);
            Object.Organization = _organizationFactory.Object;
            return this;
        }

        public ShiftFactory Persist()
        {
            SingleObjectList.ForEach(x =>
            {
                if (x.Id == 0)
                {
                    _shiftService.Save(x);
                }
            });           
             
            foreach (var item in ObjectList)
            {
                if (item.Id == 0)
                {
                    _shiftService.Save(item);
                }
            }
            
            return this;
        }

        #endregion 

        #region Others

        #endregion
        
        #region Cleanup

        public void Cleanup()
        {
            DeleteAll();
            _organizationFactory.Cleanup();
        }

        #endregion

        public void Cleanup(Organization organization)
        {
            IList<Shift> list = _shiftService.LoadShift(new List<long>() { organization.Id });
            foreach (var shift in list)
            {
                string a = "DELETE FROM [HR_Shift] WHERE [Id] = " + shift.Id + ";";
                _session.CreateSQLQuery(a).ExecuteUpdate();
            }
        }
    }
}
