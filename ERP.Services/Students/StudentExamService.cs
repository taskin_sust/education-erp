﻿using System;
using System.Collections;
using System.Collections.Generic;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface IStudentExamService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        StudentExam LoadById(long examId);
        StudentExam GetByExamName(string examName);
        #endregion

        #region List Loading Function
        IList<StudentExam> LoadActive();
        IList<StudentExam> LoadBoardExam(bool isBoard);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion        
        
    }

    public class StudentExamService : BaseService, IStudentExamService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly StudentExamDao _studentExamDao;

        public StudentExamService(ISession session)
        {
            Session = session;
            _studentExamDao = new StudentExamDao() { Session = Session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public StudentExam LoadById(long examId)
        {
            try
            {
                return _studentExamDao.LoadById(examId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }

        public StudentExam GetByExamName(string examName)
        {
            return _studentExamDao.GetByExamName(examName);
        }

        #endregion

        #region List Loading Function
        public IList<StudentExam> LoadActive()
        {
            try
            {
                return _studentExamDao.LoadAllOk();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }            
        }

        public IList<StudentExam> LoadBoardExam(bool isBoard)
        {
            try
            {
                return _studentExamDao.LoadStudentExam(isBoard); 
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }        
           
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
