﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherVersionService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<TeacherVersionOfStudyPriority> LoadTeacherVersionByTacherId(long id);
        #endregion

        #region Others Function

        #endregion
    }
    public class TeacherVersionService : BaseService, ITeacherVersionService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ITeacherVersionOfStudyPriorityDao _teacherVersionOfStudyPriorityDao;
        public TeacherVersionService(ISession session)
        {
            Session = session;
            _teacherVersionOfStudyPriorityDao = new TeacherVersionOfStudyPriorityDao() { Session = session };

        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<TeacherVersionOfStudyPriority> LoadTeacherVersionByTacherId(long id)
        {
            try
            {
                return _teacherVersionOfStudyPriorityDao.LoadTeacher(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Others Function

        #endregion
    }
}
