﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.MemberLeaveSummary
{
    [Trait("Area", "Hr")]
    [Trait("Service", "MemberLeaveSummary")]
    public class MemberLeaveSummaryUpdateTest:MemberLeaveSummaryBaseTest
    {
        [Fact]
        public void Should_Return_Successfully()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();

            var memberLeaveSummary = _membersLeaveSummaryService.LoadByMemberAndYear(tm.Id, lvStartDate.Year).FirstOrDefault();
            memberLeaveSummary.CarryForwardLeave = 2;
            memberLeaveSummary.CurrentYearLeave = 8;
            memberLeaveSummary.TotalLeaveBalance = 10;
            memberLeaveSummary.ApprovedLeave = 5;
            memberLeaveSummary.PendingLeave = 1;
            memberLeaveSummary.AvailableBalance = 4;
            var errorList = _membersLeaveSummaryService.SaveAndUpdateIndividualLeave(null,
                new List<BusinessModel.Entity.Hr.MemberLeaveSummary>() { memberLeaveSummary });
            Assert.Contains("0", errorList.Count().ToString());

        }

        [Fact]
        public void Should_Return_InvalidDataException_TeamMember()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();

            var memberLeaveSummary = _membersLeaveSummaryService.LoadByMemberAndYear(tm.Id, lvStartDate.Year).FirstOrDefault();
            memberLeaveSummary.CarryForwardLeave = 2;
            memberLeaveSummary.CurrentYearLeave = 8;
            memberLeaveSummary.TotalLeaveBalance = 10;
            memberLeaveSummary.ApprovedLeave = 5;
            memberLeaveSummary.PendingLeave = 1;
            memberLeaveSummary.AvailableBalance = 4;
            memberLeaveSummary.TeamMember = null;

            Assert.Throws<InvalidDataException>(() => _membersLeaveSummaryService.SaveAndUpdateIndividualLeave(null,
                new List<BusinessModel.Entity.Hr.MemberLeaveSummary>() {memberLeaveSummary}));
        }

        [Fact]
        public void Should_Return_InvalidDataException_Leave()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();

            var memberLeaveSummary = _membersLeaveSummaryService.LoadByMemberAndYear(tm.Id, lvStartDate.Year).FirstOrDefault();
            memberLeaveSummary.CarryForwardLeave = 2;
            memberLeaveSummary.CurrentYearLeave = 8;
            memberLeaveSummary.TotalLeaveBalance = 10;
            memberLeaveSummary.ApprovedLeave = 5;
            memberLeaveSummary.PendingLeave = 1;
            memberLeaveSummary.AvailableBalance = 4;
            memberLeaveSummary.Leave = null;
            Assert.Throws<InvalidDataException>(() => _membersLeaveSummaryService.SaveAndUpdateIndividualLeave(null,
               new List<BusinessModel.Entity.Hr.MemberLeaveSummary>() { memberLeaveSummary }));

        }
        
        [Fact]
        public void Should_Return_Exception_For_AvailableBalance_Is_Grater_than_NoOfDaysLeave()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();

            var memberLeaveSummary = _membersLeaveSummaryService.LoadByMemberAndYear(tm.Id, lvStartDate.Year).FirstOrDefault();
            memberLeaveSummary.CarryForwardLeave = 2;
            memberLeaveSummary.CurrentYearLeave = 8;
            memberLeaveSummary.TotalLeaveBalance = 10;
            memberLeaveSummary.ApprovedLeave = 5;
            memberLeaveSummary.PendingLeave = 1;
            memberLeaveSummary.AvailableBalance = 30;

            var errorList = _membersLeaveSummaryService.SaveAndUpdateIndividualLeave(null,
                new List<BusinessModel.Entity.Hr.MemberLeaveSummary>() { memberLeaveSummary });
            Assert.NotEqual(0, errorList.Count());
        }

        [Fact]
        public void Should_Return_TotalLeave_LessThan_ApprovedAndPending_Leave()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();

            var memberLeaveSummary = _membersLeaveSummaryService.LoadByMemberAndYear(tm.Id, lvStartDate.Year).FirstOrDefault();
            memberLeaveSummary.CarryForwardLeave = 2;
            memberLeaveSummary.CurrentYearLeave = 8;
            memberLeaveSummary.TotalLeaveBalance = 10;
            memberLeaveSummary.ApprovedLeave = 5;
            memberLeaveSummary.PendingLeave = 10;
            memberLeaveSummary.AvailableBalance = 4;
            var errorList = _membersLeaveSummaryService.SaveAndUpdateIndividualLeave(null,
                new List<BusinessModel.Entity.Hr.MemberLeaveSummary>() { memberLeaveSummary });
            Assert.NotEqual(0, errorList.Count());
        }

        [Fact]
        public void Should_Return_Leave_IsEncash_False_But_Total_Encash_Leave_MoreThan_Zero()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();

            var memberLeaveSummary = _membersLeaveSummaryService.LoadByMemberAndYear(tm.Id, lvStartDate.Year).FirstOrDefault();

            memberLeaveSummary.Leave.IsEncash = false;
            memberLeaveSummary.EncashLeave = 5;

            memberLeaveSummary.CarryForwardLeave = 2;
            memberLeaveSummary.CurrentYearLeave = 8;
            memberLeaveSummary.TotalLeaveBalance = 10;
            memberLeaveSummary.ApprovedLeave = 3;
            memberLeaveSummary.PendingLeave = 1;
            memberLeaveSummary.AvailableBalance = 4;

            var errorList = _membersLeaveSummaryService.SaveAndUpdateIndividualLeave(null,
                new List<BusinessModel.Entity.Hr.MemberLeaveSummary>() { memberLeaveSummary });
            Assert.NotEqual(0, errorList.Count());
        }

        [Fact]
        public void Should_Return_Encash_or_Void_Calculation_is_Not_Correct()
        {
            var joiningDate = new DateTime(2016, 6, 1);
            var dateTo = new DateTime(2016, 6, 1);
            var lvStartDate = DateTime.Now.AddDays(3).Date;
            var lvEndDate = DateTime.Now.AddDays(3).Date;

            const long orgId = 3;

            var org = _organizationService.LoadById(orgId);
            var holidaySettingsList = _holidaySettingService.LoadHoliday(0, 100, "", "", commonHelper.ConvertIdToList(org.Id), null, null);


            var tm = PersistTeamMember(joiningDate, dateTo, org);
            var memberWeekend = tm.ShiftWeekendHistory[0].Weekend;

            var leaveList = _leaveService.LoadLeaveTypeByMembersGenderAndEmployeementStatusAndMeritalStatus(
                    tm.EmploymentHistory[0].Campus.Branch.Organization.Id, tm.Gender,
                    tm.EmploymentHistory[0].EmploymentStatus, (int)tm.MaritalInfo[0].MaritalStatus);

            if (memberWeekend == (int)Enum.Parse(typeof(WeekDay), lvStartDate.DayOfWeek.ToString()))
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            var holidays = holidaySettingsList.Where(x => x.DateFrom >= lvStartDate && x.DateTo <= lvStartDate).ToList();
            if (holidays.Any())
            {
                lvStartDate = lvStartDate.AddDays(1);
                lvEndDate = lvEndDate.AddDays(1);
            }

            leaveApplicationFactory.CreateWith(0, lvStartDate, lvEndDate, "test", (int)LeaveStatus.Approved, "test")
                  .WithLeave(leaveList[0])
                  .WithTeamMember(tm)
                  .WithResposibleTeamMember(tm.MentorHistory[0].Mentor).Persist();

            var memberLeaveSummary = _membersLeaveSummaryService.LoadByMemberAndYear(tm.Id, lvStartDate.Year).FirstOrDefault();

            memberLeaveSummary.Leave.IsEncash = true;
            memberLeaveSummary.EncashLeave = 5;
            memberLeaveSummary.Leave.MinEncashReserveDays = 2;

            memberLeaveSummary.CarryForwardLeave = 2;
            memberLeaveSummary.CurrentYearLeave = 8;
            memberLeaveSummary.TotalLeaveBalance = 10;
            memberLeaveSummary.ApprovedLeave = 3;
            memberLeaveSummary.PendingLeave = 1;
            memberLeaveSummary.AvailableBalance = 4;
            memberLeaveSummary.VoidLeave = 3;

            var errorList = _membersLeaveSummaryService.SaveAndUpdateIndividualLeave(null,
                new List<BusinessModel.Entity.Hr.MemberLeaveSummary>() { memberLeaveSummary });
            Assert.NotEqual(0, errorList.Count());
        }

    }
}
