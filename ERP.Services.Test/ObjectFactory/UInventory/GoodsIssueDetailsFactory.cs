using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Testing.Values;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class GoodsIssueDetailsFactory : ObjectFactoryBase<GoodsIssueDetails>
    {

        #region Object Initalization

        private readonly IGoodsIssueDetailsService _goodsIssueDetailService;
        private ItemFactory _itemFactory;
        private ProgramFactory _programFactory;
        private SessionFactory _sessionFactory;
       

        #endregion

        public GoodsIssueDetailsFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _goodsIssueDetailService = new GoodsIssueDetailsService(session);
            _itemFactory = new ItemFactory(null, _session);
            _programFactory = new ProgramFactory(this, _session);
            _sessionFactory = new SessionFactory(this, _session);
           

        }

        #region Create

        public GoodsIssueDetailsFactory Create()
        {

            Object = new GoodsIssueDetails()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Purpose = (int)Purpose.ProgramPurpose,
                IssuedQuantity = 15
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsIssueDetailsFactory Create(Program program,Session session, Item item, GoodsIssue goodsIssue)
        {
            Object = new GoodsIssueDetails()
            {
                BusinessId = GetPcUserName(),
                Item = item,
                Program = program,
                Session = session,
                PurposeProgramId = _programFactory.Object,
                PurposeSessionId = _sessionFactory.Object,
                GoodsIssue = goodsIssue,
                Purpose = (int)Purpose.ProgramPurpose,
                IssuedQuantity = 15
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public GoodsIssueDetailsFactory CreateMore(int num)
        {
            CreateGoodsIssueList(num);
            return this;
        }

        private GoodsIssueDetailsFactory CreateGoodsIssueList(int num)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<GoodsIssueDetails>();
            var item = _itemFactory.Create().WithItemGroup().WithOrganization().Persist().Object;
            var program = _programFactory.Create().WithOrganization().Persist().Object;
            var session = _sessionFactory.Create().Persist().Object;
            for (int i = 0; i < num; i++)
            {
                Object = new GoodsIssueDetails
                {
                    BusinessId = GetPcUserName(),
                    Item = item,
                    Program = program,
                    Session = session,
                    PurposeProgramId = program,
                    PurposeSessionId = session,
                    Purpose = (int)Purpose.ProgramPurpose,
                    IssuedQuantity = 15,
                };
                list.Add(Object);
            }
            ObjectList.AddRange(list);
            return this;
        }
        public GoodsIssueDetailsFactory WithProperty(int? qty)
        {
            Object.IssuedQuantity = qty ?? 0;
            return this;
        }

        public GoodsIssueDetailsFactory WithItem()
        {
            Object.Item = _itemFactory.Create().WithItemGroup().WithOrganization().Persist().Object;
            return this;
        }

        public GoodsIssueDetailsFactory WithItem(Item item)
        {
            Object.Item = item;
            return this;
        }

        public GoodsIssueDetailsFactory WithProgram()
        {
            Object.Program = _programFactory.Create().WithOrganization().Persist().Object;
            return this;
        }

        public GoodsIssueDetailsFactory WithProgram(Organization organization)
        {
            Object.Program = _programFactory.Create().WithOrganization(organization).Persist().Object;
            return this;
        }

        public GoodsIssueDetailsFactory WithProgram(Program program)
        {
            Object.Program = program;
            return this;
        }

        public GoodsIssueDetailsFactory WithSession()
        {
            Object.Session = _sessionFactory.Create().Persist().Object;
            return this;
        }

        public GoodsIssueDetailsFactory WithSession(Session session)
        {
            Object.Session = session;
            return this;
        }

        public GoodsIssueDetailsFactory WithPurpose()
        {
            Object.Purpose = (int)Purpose.ProgramPurpose;
            return this;
        }
        public GoodsIssueDetailsFactory WithPurpose(Purpose purpose)
        {
            Object.Purpose = (int)purpose;
            return this;
        }

        public GoodsIssueDetailsFactory WithPurpose(int purpose)
        {
            Object.Purpose = purpose;
            return this;
        }
        public GoodsIssueDetailsFactory WithProgramAndSession(Program program, Session session)
        {
            Object.Program = program;
            Object.Session = session;
            return this;
        }

        public GoodsIssueDetailsFactory WithGoodsIssue(GoodsIssue goodsIssue)
        {
            Object.GoodsIssue = goodsIssue;
            return this;
        }


        public GoodsIssueDetailsFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x => { if (x.Id == 0) _goodsIssueDetailService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                foreach (var goodsIssueDetails in ObjectList)
                {
                    goodsIssueDetails.Item = WithItem().Object.Item;
                    goodsIssueDetails.Program = WithProgram().Object.Program;
                    goodsIssueDetails.Session = WithSession().Object.Session;
                }
                ObjectList.ForEach(x => { if (x.Id == 0)_goodsIssueDetailService.SaveOrUpdate(x); });

            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public GoodsIssueDetailsFactory CleanUp()
        {
           DeleteAll();
            return this;
        }

        #endregion

    }
}
