﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Hr;
namespace UdvashERP.Services.Hr
{
    public interface IDepartmentService : IBaseService
    {
        #region Operational Function

        bool Save(Department hrDepartment);
        bool Update(Department hrDepartment);
        bool Delete(Department hrDepartment);

        #endregion

        #region Single Instances Loading Function

        Department LoadById(long id);

        #endregion

        #region List Loading Function
        IList<Department> LoadActive(List<UserMenu> userMenu ,int start, int length, string orderBy, string toUpper, string organizationId, string status);
        IList<Department> LoadDepartment(List<long> organizationIds);
        IList<Department> LoadAuthorizedDepartment(List<UserMenu> userMenu, List<long> organizationIdList = null);
        #endregion

        #region Others Function
        int TotalRowCountOfDepartmentSettings(List<UserMenu> userMenu ,string organizationId, string status);
        bool HasDuplicateByDepartmentName(string name, long organizationId, long? id);
        #endregion

        #region Helper Function

        #endregion
    }
    public class DepartmentService : BaseService, IDepartmentService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IDepartmentDao _hrDepartmentDao;
        public DepartmentService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrDepartmentDao = new DepartmentDao(){Session = session};
        }
        #endregion

        #region Operational Functions

        public bool Save(Department hrDepartment)
        {
            ITransaction transaction = null;
            
            try
            {
                if (hrDepartment == null)
                    throw new NullObjectException("Null Object!!");

                if (hrDepartment.Organization == null)
                    throw new NullObjectException("Organization can't empty");

                bool duplicationCheck = HasDuplicateByDepartmentName(hrDepartment.Name, hrDepartment.Organization.Id, null);
                if (!duplicationCheck)
                    throw new DuplicateEntryException("Duplicate result found");

                if (string.IsNullOrEmpty(hrDepartment.Name))
                {
                    throw new ServiceException("Department name can not be empty.");
                }

                using (transaction = Session.BeginTransaction())
                {
                    hrDepartment.Rank = _hrDepartmentDao.GetMaximumRank(hrDepartment) + 1;
                    _hrDepartmentDao.Save(hrDepartment);
                    transaction.Commit();
                    return true;
                }
            }
            catch (DuplicateEntryException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (ServiceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                return false;
            }
        }
        public bool Update(Department hrDepartment)
        {
            ITransaction transaction = null;
            try
            {
                if (hrDepartment == null)
                    throw new EmptyFieldException("Field must be required");

                if (hrDepartment.Organization == null)
                    throw new NullObjectException("Organization invalid");

                bool duplicationCheck = HasDuplicateByDepartmentName(hrDepartment.Name, hrDepartment.Organization.Id, hrDepartment.Id);
                if (!duplicationCheck)
                    throw new DuplicateEntryException("Duplicate result found");

                Department hrD = _hrDepartmentDao.LoadById(hrDepartment.Id);
                if (hrD == null)
                    throw new NullObjectException("Department id invalid");

                using (transaction = Session.BeginTransaction())
                {
                    hrD.Organization = hrDepartment.Organization;
                    hrD.Name = hrDepartment.Name;
                    hrD.Status = hrDepartment.Status;
                    _hrDepartmentDao.Update(hrD);
                    transaction.Commit();
                    return true;
                }
            }
            catch (DuplicateEntryException)
            {
                throw;
            }
            catch (NullObjectException)
            {
                throw;
            }
            catch (ServiceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
            return false;
        }

        public bool Delete(Department hrDepartment)
        {
            ITransaction transaction = null;
            try
            {
                if (hrDepartment == null)
                    throw new EmptyFieldException("Field must be required");

                //check dependency
                int totalEmploymentHistory =
                    hrDepartment.EmploymentHistory.Where(x => x.Status != EmploymentHistory.EntityStatus.Delete)
                        .ToList()
                        .Count();
                if (totalEmploymentHistory > 0)
                {
                    throw new DependencyException("This Department has total " + totalEmploymentHistory.ToString() +
                                                  " record(s). You can't delete this department now.");
                }

                using (transaction = Session.BeginTransaction())
                {
                    hrDepartment.Status = Department.EntityStatus.Delete;
                    _hrDepartmentDao.Update(hrDepartment);
                    transaction.Commit();
                    return true;
                }
            }
            catch (EmptyFieldException)
            {
                throw;
            }
            catch (DependencyException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function
        public Department LoadById(long id)
        {
            try
            {
                return _hrDepartmentDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function
        public IList<Department> LoadActive(List<UserMenu> userMenu ,int start, int length, string orderBy, string toUpper, string organizationId, string status)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");
                long? convertedOrganizationId = organizationId == "" ? (long?)null : Convert.ToInt32(organizationId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu,
                    (convertedOrganizationId != null)
                        ? _commonHelper.ConvertIdToList((long)convertedOrganizationId)
                        : null);
                return _hrDepartmentDao.LoadActive(start, length, orderBy, toUpper, organizationIdList, status);
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Department> LoadDepartment(List<long> organizationIds)
        {
            try
            {
                var departmentList = _hrDepartmentDao.LoadHrDepartmentList(organizationIds);
                return departmentList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public IList<Department> LoadAuthorizedDepartment(List<UserMenu> userMenu, List<long> organizationIdList = null)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");
                List<long> authorizedOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (organizationIdList != null && !organizationIdList.Contains(SelectionType.SelelectAll) && !organizationIdList.Contains(-1)) ? organizationIdList : null);
                return LoadDepartment(authorizedOrganizationIdList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function
        public int TotalRowCountOfDepartmentSettings(List<UserMenu> userMenu, string organizationId, string status)
        {
            try
            {
                if (userMenu == null)
                    throw new InvalidDataException("Invalid menu");
                long? convertedOrganizationId = organizationId == "" ? (long?)null : Convert.ToInt32(organizationId);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                return _hrDepartmentDao.TotalRowCountOfDepartmentSettings(organizationIdList, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public bool HasDuplicateByDepartmentName(string name, long organizationId, long? id)
        {
            try
            {
                return _hrDepartmentDao.HasDuplicateByDepartmentName(name, organizationId, id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function



        #endregion

        
    }
}
