﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.AdminisTration.MenuGroupTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.MenuGroupTest
{
    interface IMenuGroupDeleteTest
    {
        #region Basic Test
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "MenuGroup")]
    public class MenuGroupDeleteTest : MenuGroupBaseTest, IMenuGroupDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            MenuGroupFactory menugroupFactory =
                    _menugroupFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithDisplayText(_displaytext)
                    .Persist();
            MenuGroup menuGroup = _menuService.LoadById(menugroupFactory.Object.Id);
            var success = _menuService.Delete(menuGroup);
            Assert.True(success);
            menuGroup = _menuService.LoadById(menugroupFactory.Object.Id);
            Assert.Null(menuGroup);

        }
        
        #endregion

    }
}
