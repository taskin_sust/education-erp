﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.SpecificationTemplateTest
{
    public interface ISpecificationTemplateUpdateTest
    {
        #region basic test
        void Update_Should_Return_Null_Exception();
        void Should_Update_Successfully();
        #endregion

        #region business logic test
        void Update_Should_Check_Duplicate_Entry();
        void Should_Not_Update_Empty_Name();
        void Should_Not_Update_Empty_Criteria_Name();
        void Should_Not_Update_Empty_Criteria_Option_Name();
        void Should_Not_Update_Without_Criteria();
        void Should_Not_Update_Without_Criteria_Option();
        #endregion
    }
    [Trait("Area", "UInventory")]
    [Trait("Service", "SpecificationCriteria")]
    public class SpecificationTemplateUpdateTest : SpecificationTemplateBaseTest, ISpecificationTemplateUpdateTest
    {
        #region Basic Test

        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(Guid.NewGuid().ToString().Substring(0, 5)).Persist();
            Assert.Throws<NullObjectException>(() => _specificationTemplateService.SaveOrUpdate(null));
            var criteriaList = _specificationTemplateFactory.Object.SpecificationCriteriaList;
            CleanUpSpecificationCriteria(criteriaList);
        }

        [Fact]
        public void Should_Update_Successfully()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 5);
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(name).Persist();

            var specificationTemplate = new SpecificationTemplate();
            var specificationCriterias = new List<SpecificationCriteria>();
            specificationTemplate.Name = Guid.NewGuid().ToString().Substring(0, 5);
            var spc = new SpecificationCriteria();
            spc.Name = Guid.NewGuid().ToString().Substring(0, 5);
            spc.Controltype = FieldType.Text;
            spc.SpecificationTemplate = specificationTemplate;
            specificationCriterias.Add(spc);
            specificationTemplate.SpecificationCriteriaList = specificationCriterias;
            specificationTemplate = _specificationTemplateFactory.Object;
            var success = _specificationTemplateService.SaveOrUpdate(specificationTemplate);
            Assert.True(success);
            var criteriaList = _specificationTemplateFactory.Object.SpecificationCriteriaList;
            CleanUpSpecificationCriteria(criteriaList);
        }

        #endregion

        #region Business Logic Test
        [Fact]
        public void Update_Should_Check_Duplicate_Entry()
        {
            _specificationTemplateFactory.CreateMore(2).Persist();
            var st1 = _specificationTemplateFactory.GetLastCreatedObjectList()[0];
            var st2 = _specificationTemplateFactory.GetLastCreatedObjectList()[1];
            st2.Name = st1.Name;
            Assert.Throws<DuplicateEntryException>(() => _specificationTemplateService.SaveOrUpdate(st2));
            var templatelist = _specificationTemplateFactory.GetLastCreatedObjectList();
            var criteriaList = new List<SpecificationCriteria>();
            foreach (var template in templatelist)
            {
                criteriaList.AddRange(template.SpecificationCriteriaList);
            }
            CleanUpSpecificationCriteria(criteriaList);
        }

        [Fact]
        public void Should_Not_Update_Empty_Name()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 5);
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(name).Persist();
            var st = _specificationTemplateFactory.Object;
            st.Name = "";
            Assert.Throws<InvalidDataException>(() => _specificationTemplateService.SaveOrUpdate(st));
            var criteriaList = _specificationTemplateFactory.Object.SpecificationCriteriaList;
            CleanUpSpecificationCriteria(criteriaList);
        }

        [Fact]
        public void Should_Not_Update_Empty_Criteria_Name()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 5);
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(name).Persist();
            var st = _specificationTemplateFactory.Object;
            st.SpecificationCriteriaList[0].Name = "";
            Assert.Throws<InvalidDataException>(() => _specificationTemplateService.SaveOrUpdate(st));
            var criteriaList = _specificationTemplateFactory.Object.SpecificationCriteriaList;
            CleanUpSpecificationCriteria(criteriaList);
        }

        [Fact]
        public void Should_Not_Update_Empty_Criteria_Option_Name()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 5);
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(name).Persist();
            var st = _specificationTemplateFactory.Object;
            st.SpecificationCriteriaList[0].SpecificationCriteriaOptions[0].Name = "";
            Assert.Throws<InvalidDataException>(() => _specificationTemplateService.SaveOrUpdate(st));
            var criteriaList = new List<SpecificationCriteria>(_specificationTemplateFactory.Object.SpecificationCriteriaList);
            CleanUpSpecificationCriteria(criteriaList);
        }
        [Fact]
        public void Should_Not_Update_Without_Criteria()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 5);
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(name).Persist();
            var st = _specificationTemplateFactory.Object;
            var criteriaList = new List<SpecificationCriteria>(_specificationTemplateFactory.Object.SpecificationCriteriaList);
            st.SpecificationCriteriaList.Clear();
            Assert.Throws<InvalidDataException>(() => _specificationTemplateService.SaveOrUpdate(st));
            CleanUpSpecificationCriteria(criteriaList);
        }
        [Fact]
        public void Should_Not_Update_Without_Criteria_Option()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 5);
            _specificationTemplateFactory.Create().WithSpecificationCriteria().WithName(name).Persist();
            var st = _specificationTemplateFactory.Object;
            var criteriaList = new List<SpecificationCriteria>(_specificationTemplateFactory.Object.SpecificationCriteriaList);
            st.SpecificationCriteriaList[0].SpecificationCriteriaOptions.Clear();
            Assert.Throws<InvalidDataException>(() => _specificationTemplateService.SaveOrUpdate(st));
            CleanUpSpecificationCriteria(criteriaList);
        }

        #endregion
    }
}
