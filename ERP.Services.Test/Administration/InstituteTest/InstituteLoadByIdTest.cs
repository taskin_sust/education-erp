﻿using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.InstituteTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Institute")]
    public class InstituteLoadByIdTest : InstituteBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _InstituteService.LoadById(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            InstituteFactory instituteFactory =
                _InstituteFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithInstituteCategory().Persist();
            var institute = _InstituteService.LoadById(instituteFactory.Object.Id);
            Assert.NotNull(institute);
            
        }

    }
}
