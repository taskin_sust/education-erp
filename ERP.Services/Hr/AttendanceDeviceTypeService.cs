﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface IAttendanceDeviceTypeService : IBaseService
    {

        #region Operational Function

        void SaveOrUpdate(AttendanceDeviceType deviceType);
        void Delete(AttendanceDeviceType entity);

        #endregion

        #region Single Instances Loading Function
        AttendanceDeviceType GetAttendanceDeviceType(long id);

        #endregion

        #region List Loading Function

        IList<AttendanceDeviceType> LoadAttendanceDeviceType();
        IList<AttendanceDeviceType> LoadAttendanceDeviceType(int draw, int start, int length, string name, string code, string finger);
        #endregion

        #region Others Function
        int AttendanceDevicetypeCount(string name, string code, string finger);
        #endregion

        #region Helper Function

        #endregion


    }
    public class AttendanceDeviceTypeService : BaseService, IAttendanceDeviceTypeService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Properties & Object Initialization

        private readonly IAttendanceDeviceTypeDao _attendanceDeviceTypeDao;
        private readonly IMemberFingerPrintDao _memberFingerPrintDao;
        public AttendanceDeviceTypeService(ISession session)
        {
            Session = session;
            _attendanceDeviceTypeDao = new AttendanceDeviceTypeDao() { Session = session };
            _memberFingerPrintDao = new MemberFingerPrintDao() { Session = session };
        }

        #endregion

        #region Operational Function

        public void SaveOrUpdate(AttendanceDeviceType deviceType)
        {
            ITransaction transaction = null;
            try
            {
                CheckBeforeSave(deviceType);
                CheckValidation(deviceType);
                using (transaction = Session.BeginTransaction())
                {
                    if (deviceType.Id == 0)
                        _attendanceDeviceTypeDao.Save(deviceType);
                    else
                        _attendanceDeviceTypeDao.Update(deviceType);
                    transaction.Commit();
                }

            }
            catch (InvalidDataException ide)
            {
                throw;
            }
            catch (DuplicateEntryException dee)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void Delete(AttendanceDeviceType entity)
        {
            ITransaction transaction = null;
            try
            {
                EligibleForDelete(entity);
                using (transaction = Session.BeginTransaction())
                {
                    entity.Status = DailySupportAllowanceSetting.EntityStatus.Delete;
                    _attendanceDeviceTypeDao.Update(entity);
                    transaction.Commit();
                }
            }
            catch (NullObjectException nullObjectException)
            {
                throw;
            }
            catch (InvalidDataException invalidDataException)
            {
                throw;
            }
            catch (DependencyException e)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        private void EligibleForDelete(AttendanceDeviceType entity)
        {
            if (entity == null) throw new NullObjectException("Invalid Object");
            if (entity.Id <= 0) throw new NullObjectException("Invalid Object");
            MemberFingerPrint memberFingerPrint = _memberFingerPrintDao.GetByTeamMemberAndModelId(0, entity.Id);
            if (memberFingerPrint != null) throw new DependencyException("Can't delete because Member's fingerprint are registered using this type ");
        }

        #endregion

        #region Single Instances Loading Function

        public AttendanceDeviceType GetAttendanceDeviceType(long id)
        {
            try
            {
                if (id <= 0) throw new InvalidDataException("Invalid Id");
                return _attendanceDeviceTypeDao.LoadById(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<AttendanceDeviceType> LoadAttendanceDeviceType()
        {
            try
            {
                return _attendanceDeviceTypeDao.LoadAttendanceDeviceType();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<AttendanceDeviceType> LoadAttendanceDeviceType(int draw, int start, int length, string name, string code, string finger)
        {
            try
            {
                return _attendanceDeviceTypeDao.LoadAttendanceDeviceType(draw, start, length, name, code, finger);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }


        #endregion

        #region Others Function

        public int AttendanceDevicetypeCount(string name, string code, string finger)
        {
            try
            {
                return _attendanceDeviceTypeDao.AttendanceDevicetypeCount(name, code, finger);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper Function

        private void CheckValidation(AttendanceDeviceType deviceType)
        {
            var validationResult = ValidationHelper.ValidateEntity<AttendanceDeviceType, AttendanceDeviceType>(deviceType);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private void CheckBeforeSave(AttendanceDeviceType deviceType)
        {
            if (deviceType == null) throw new NullReferenceException("Invalid Object");
            IList<AttendanceDeviceType> list = _attendanceDeviceTypeDao.LoadAttendanceDeviceType(deviceType.Code, deviceType.Id, deviceType.Name);
            if (list != null && list.Count > 0) throw new DuplicateEntryException("There is already an entry with this Code! Please try something else");
        }

        #endregion

    }
}
