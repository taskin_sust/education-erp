﻿using System;
using System.Linq;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.SubjectTest
{
    interface ISubjectDeleteTest
    {
        #region Basic Test

        [Fact]
        void Should_Delete_Successfully();

        #endregion

        #region Business Logic Test
        [Fact]
        void Delete_Should_Throw_Dependency_Exception(); 
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Subject")]
    public class SubjectDeleteTest : SubjectBaseTest, ISubjectDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            SubjectFactory subjectFactory =
                    _subjectFactory.Create()
                    .WithNameAndRank(_name, 1)
                    .WithProperties(_shortName)
                    .Persist();
            Subject subject = _subjectService.GetSubject(subjectFactory.Object.Id);
            var success = _subjectService.Delete(subject.Id);
            Assert.True(success);
            subject = _subjectService.GetSubject(subjectFactory.Object.Id);
            Assert.Null(subject);

        }
        #endregion
        
        #region Business Logic Test
        [Fact]
        public void Delete_Should_Throw_Dependency_Exception()
        {
            const int noOfSubject = 1;
            var organzationFactory = _organizationFactory.Create().WithRequiredProperties().Persist();
            var programCode = _programFactory.GetRandomNumber();
            var programFactory =
                _programFactory.Create().WithOrganization(organzationFactory.Object).WithNameAndRank(_name, 1).WithProperties(_name, programCode, 1)
                    .Persist();
            var sessionCode = _sessionFactory.GetRandomNumber();
            var sessionFactory = _sessionFactory.Create().WithNameAndRank(Guid.NewGuid() + "test session name", 1).WithCode(sessionCode).Persist();
            var subjectFactory =
                _subjectFactory.CreateMore(noOfSubject)
                    .Persist();
            var subjectAssign = new SubjectAssign
            {
                SelectedSubjects = subjectFactory.GetLastCreatedObjectList().Select(x => Convert.ToInt32(x.Id)).ToArray()
            };
            var programId = programFactory.Object.Id;
            var sessionId = sessionFactory.Object.Id;
            var subjectIdList = subjectFactory.GetLastCreatedObjectList().Select(x => x.Id).ToList();
            _programSessionSubjectService.SaveUpdateSubject(subjectAssign, programId,
                sessionId);
            var subject = subjectFactory.GetLastCreatedObjectList()[0];
            Assert.Throws<DependencyException>(() => _subjectService.Delete(subject.Id));
            CleanUpProgramSessionSubject(programId, sessionId, subjectIdList);

        } 
        #endregion
    }
}
