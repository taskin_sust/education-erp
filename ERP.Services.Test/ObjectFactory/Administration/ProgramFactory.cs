﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class ProgramFactory : ObjectFactoryBase<Program>
    {
        #region Object Initialization

        protected readonly IProgramService _programService;
        private OrganizationFactory _organizationFactory;
        //internal readonly string _code;
        public readonly List<string> assignCodeList;

        public ProgramFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _programService = new ProgramService(_session);
            _organizationFactory = new OrganizationFactory(this, _session);
            //_code = GetRandomNumber();
            assignCodeList = new List<string>();
        }

        #endregion

        #region Create

        public ProgramFactory Create()
        {
            Object = new Program()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                Status = Branch.EntityStatus.Active,
                Name = "P-N" + Guid.NewGuid() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"),
                ShortName = "S-N" + Guid.NewGuid() + DateTime.Now.ToString("yyyyMMddHHmmss"),
                Code = GetRandomNumber(),
                Type = Program.ProgramTypeStatus.Paid,
                Rank = 1
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public ProgramFactory CreateMore(int numberOfProgram, IList<Organization> organizationList)
        {
            CreateProgramList(numberOfProgram, organizationList);
            return this;
        }

        private List<Program> CreateProgramList(int numberOfProgram, IList<Organization> organizationList)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Program>();
            foreach (var org in organizationList)
            {
                for (int i = 0; i < numberOfProgram; i++)
                {
                    string cd = GetRandomNumber();
                    var program = new Program()
                    {
                        Id = 0,
                        BusinessId = GetPcUserName(),
                        CreateBy = 1,
                        ModifyBy = 1,
                        Status = Program.EntityStatus.Active,
                        Name = "P-N" + Guid.NewGuid() + "_" + i,
                        ShortName = "S-N" + Guid.NewGuid() + "_" + i,
                        Code = cd,
                        Type = Program.ProgramTypeStatus.Paid,
                        Rank = i
                    };
                    program.Organization = org;
                    list.Add(program);
                    assignCodeList.Add(cd);
                }
            }
            ObjectList.AddRange(list);
            return list;
        }

        public ProgramFactory CreateMore(int numberOfProgram)
        {
            CreateProgramList(numberOfProgram);
            return this;
        }

        private List<Program> CreateProgramList(int numberOfProgram)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Program>();
            var orgNamePrefix = Guid.NewGuid().ToString();
            _organizationFactory.Create().WithNameAndRank(orgNamePrefix + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"), 1).Persist();
            for (int i = 0; i < numberOfProgram; i++)
            {
                string cd = GetRandomNumber();
                var program = new Program()
                {
                    Id = 0,
                    BusinessId = GetPcUserName(),
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Program.EntityStatus.Active,
                    Name = "P-N" + Guid.NewGuid() + "_" + i,
                    ShortName = "S-N" + Guid.NewGuid() + "_" + i,
                    Code = cd,
                    Type = Program.ProgramTypeStatus.Paid,
                    Rank = i,
                };
                program.Organization = _organizationFactory.Object;
                list.Add(program);
                assignCodeList.Add(cd);
            }
            ObjectList.AddRange(list);
            return list;
        }

        public ProgramFactory WithOrganization()
        {
            _organizationFactory.Create().Persist();
            Object.Organization = _organizationFactory.Object;
            return this;
        }

        public ProgramFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }

        public ProgramFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }

        public ProgramFactory WithProperties(string name, string shortName, string code, int programType)
        {
            Object.Name = name;
            Object.ShortName = shortName;
            Object.Code = code;
            Object.Type = programType;
            return this;
        }

        public ProgramFactory WithProperties(string shortName, string code, int programType)
        {
            Object.ShortName = shortName;
            Object.Code = code;
            Object.Type = programType;
            return this;
        }

        public ProgramFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _programService.ProgramSaveOrUpdate(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _programService.ProgramSaveOrUpdate(item);
                    }
                }
            }
            return this;
        }


        //public ProgramFactory WithOrganization(string orgName, int rank, string shortName, string contact, decimal basicSalary, decimal houseRent, decimal medicalAllowance, decimal convenyance, int monthlyWorkingDay, decimal deductionPerAbsent)
        //{
        //    _organizationFactory.CreateWith(orgName, rank, shortName, contact, basicSalary, houseRent, medicalAllowance, convenyance, monthlyWorkingDay, deductionPerAbsent);
        //    Object.Organization = _organizationFactory.Object;
        //    return this;
        //}

        #endregion

        #region Others
        public string GetRandomNumber()
        {
            int min = 1;
            int max = 99;
            var codeList = _programService.LoadProgramCode();
            if (assignCodeList != null && assignCodeList.Count > 0)
            {
                foreach (var assignCode in assignCodeList)
                {
                    codeList.Add(assignCode);
                }
            }
            do
            {
                Random r = new Random();
                var x = r.Next(min, max);
                string s = x.ToString("00");
                //string s = "10";
                if (codeList != null && !codeList.Contains(s))
                {
                    return s;
                }
                else if (codeList == null)
                {
                    return s;
                }
                min++;
            } while (min <= max);
            return "Error code found";
        }

        #endregion

        #region Cleanup

        public void CleanupProgram(List<long> programIdList)
        {
            _session.CreateSQLQuery("Delete from program where id in(" + string.Join(", ", programIdList) + ")").ExecuteUpdate();
        }
        public void CleanupProgramBranchSession(List<long> pbsIdList)
        {
            _session.CreateSQLQuery("Delete from ProgramBranchSession where id in(" + string.Join(", ", pbsIdList) + ")").ExecuteUpdate();
        }

        public void Cleanup()
        {
            DeleteAll();
            _organizationFactory.Cleanup();
        }

        public void CleanupWithoutOrganization()
        {
            DeleteAll();
        }

        #endregion

        public ProgramFactory WithCode(string code)
        {
            Object.Code = code;
            return this;
        }
    }
}
