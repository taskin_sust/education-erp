﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.SupplierPriceQuoteServiceTest
{
    interface ISupplierPriceQuoteIsCancelTest
    {
        void Should_Return_True();
        void Should_Return_NullObjectException();
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "SupplierPriceQuote")]
    public class SupplierPriceQuoteIsCancelTest : SupplierPriceQuoteBaseTest, ISupplierPriceQuoteIsCancelTest
    {
        [Fact]
        public void Should_Return_True()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create()
                .CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList)
                .Persist();

            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();
            Assert.True(_supplierPriceQuoteService.IsCancel(_supplierPriceQuoteFactory.Object.Id));
        }
        [Fact]
        public void Should_Return_NullObjectException()
        {
            organizationFactory.Create().Persist();
            _branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();

            _quotationFactory.Create().WithBranch(_branchFactory.Object).WithItem(_itemFactory.Object)
                .WithProgramAndSession(_programSessionItemFactory.Object.Program, _programSessionItemFactory.Object.Session)
                .WithQuotationCriteriaList(_itemFactory.Object)
                .WithQuotationNo(organizationFactory.Object, _branchFactory.Object).Persist();

            _supplierBankDetailsFactory.Create().WithBankBranch();
            _supplierFactory.Create()
                .CreateWithSupplierBankDetails(_supplierBankDetailsFactory.SingleObjectList)
                .Persist();

            _supplierItemFactory.Create().WithItem(_itemFactory.Object).WithSupplier(_supplierFactory.Object).Persist();

            _supplierPriceQuoteFactory.Create().WithQuotation(_quotationFactory.Object).WithSupplier(_supplierFactory.Object).Persist();
            Assert.Throws<NullObjectException>(
                () => _supplierPriceQuoteService.IsCancel((_supplierPriceQuoteFactory.Object.Id)+1));
        }
    }
}
