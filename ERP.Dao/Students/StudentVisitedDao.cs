﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentVisitedDao : IBaseDao<StudentVisited, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<StudentVisited> LoadStudentVisited(int start, int length, string orderBy, string orderDir, string classes, string year, string name, string mobile, string institute, string StudentVisitType, string UserId, string InterestedOrganizationId, long[] InterestedBranchIds);
        IList<StudentVisited> LoadStudentAdmissionVisited(int start, int length, string orderBy, string orderDir, string classes, string year, long ProgramId, long SessionId, string name, string mobile, string institute, string StudentVisitType, int StudentType, long[] UserIds, long[] BranchIds);
        IList<StudentVisited> LoadByMobile(string mobileNumber);      
        #endregion

        #region Others Function
        IList<string> LoadYear(string Class);
        IList<long> LoadUniqueUser(string Class, string Year, string StudentVisitType);
        bool CheckDuplicate(StudentVisited studentVisited);
        int GetStudentVisitedCount(string Class, string Year, string Name, string Mobile, string Institute, string StudentVisitType, string UserId, string InterestedOrganizationId, long[] InterestedBranchIds);
        int GetStudentAdmissionVisitedCount(string Class, string Year, long ProgramId, long SessionId, string Name, string Mobile, string Institute, string StudentVisitType, int StudentType, long[] UserIds, long[] BranchIds);
        #endregion
    }

    public class StudentVisitedDao : BaseDao<StudentVisited, long>, IStudentVisitedDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function


        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<StudentVisited> LoadStudentVisited(int start, int length, string orderBy, string orderDir, string classes, string year, string name, string mobile, string institute, string StudentVisitType, string UserId, string InterestedOrganizationId, long[] InterestedBranchIds)
        {
            ICriteria criteria = GetStudentVisitedCriteria(classes, year, name, mobile, institute, StudentVisitType, UserId, InterestedOrganizationId, InterestedBranchIds);
            /* Order should be add carefully
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }
            */
            if (start == 0 && length == 0)
                return criteria.List<StudentVisited>();
            else
                return criteria.SetFirstResult(start).SetMaxResults(length).List<StudentVisited>();
        }
        public IList<StudentVisited> LoadStudentAdmissionVisited(int start, int length, string orderBy, string orderDir, string classes, string year, long ProgramId, long SessionId, string name, string mobile, string institute, string StudentVisitType, int StudentType, long[] UserIds, long[] BranchIds)
        {
            ICriteria criteria = GetStudentAdmissionVisitedCriteria(classes, year, ProgramId, SessionId, name, mobile, institute, StudentVisitType, StudentType, UserIds, BranchIds);

            /* Order should be add carefully
            if (!String.IsNullOrEmpty(orderBy))
            {
                criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
            }
            */

            if (start == 0 && length == 0)
                return criteria.List<StudentVisited>();
            else
                return criteria.SetFirstResult(start).SetMaxResults(length).List<StudentVisited>();
        }
        public IList<StudentVisited> LoadByMobile(string mobileNumber)
        {
            ICriteria criteria = Session.CreateCriteria<StudentVisited>();
            criteria.Add(Restrictions.InsensitiveLike(Projections.Property<StudentVisited>(x => x.Mobile), mobileNumber, MatchMode.End));
            return criteria.List<StudentVisited>();
        }        
        #endregion

        #region Others Function
        public IList<string> LoadYear(string Class)
        {
            ICriteria criteria = Session.CreateCriteria<StudentVisited>();
            criteria.Add(Restrictions.Eq("Class", Class));
            return criteria.SetProjection(Projections.Distinct(
                    Projections.ProjectionList()
                    .Add(Projections.Property("Year").As("Year"))
                    )).List<string>();
        }
        public IList<long> LoadUniqueUser(string Class, string Year, string StudentVisitType)
        {
            ICriteria criteria = Session.CreateCriteria<StudentVisited>();
            criteria.Add(Restrictions.Eq("Class", Class));
            criteria.Add(Restrictions.Eq("Year", Year));
            if (StudentVisitType != null)
            {
                criteria.Add(Restrictions.Eq("StudentVisitType", Convert.ToInt32(StudentVisitType)));
            }
            return criteria.SetProjection(Projections.Distinct(
                    Projections.ProjectionList()
                    .Add(Projections.Property("CreateBy").As("CreateBy"))
                    )).List<long>();
        }
        public int GetStudentVisitedCount(string Class, string Year, string Name, string Mobile, string Institute, string StudentVisitType, string UserId, string InterestedOrganizationId, long[] InterestedBranchIds)
        {
            ICriteria criteria = GetStudentVisitedCriteria(Class, Year, Name, Mobile, Institute, StudentVisitType, UserId, InterestedOrganizationId, InterestedBranchIds);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public int GetStudentAdmissionVisitedCount(string Class, string Year, long ProgramId, long SessionId, string Name, string Mobile, string Institute, string StudentVisitType, int StudentType, long[] UserIds, long[] BranchIds)
        {
            ICriteria criteria = GetStudentAdmissionVisitedCriteria(Class, Year, ProgramId, SessionId, Name, Mobile, Institute, StudentVisitType, StudentType, UserIds, BranchIds);
           
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public bool CheckDuplicate(StudentVisited studentVisited)
        {
            ICriteria criteria = Session.CreateCriteria<StudentVisited>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", StudentVisited.EntityStatus.Delete)));
            if (studentVisited.Id != null)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", studentVisited.Id)));
            }
            criteria.Add(Restrictions.Eq("NickName", studentVisited.NickName));
            criteria.Add(Restrictions.Eq("Mobile", studentVisited.Mobile));
            criteria.Add(Restrictions.Eq("Class", studentVisited.Class));
            criteria.Add(Restrictions.Eq("Year", studentVisited.Year));
            criteria.SetProjection(Projections.RowCount());

            if (Convert.ToInt32(criteria.UniqueResult()) > 0)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region Helper Function
        private ICriteria GetStudentVisitedCriteria(string Class, string Year, string Name, string Mobile, string Institute, string StudentVisitType, string UserId, string InterestedOrganizationId, long[] InterestedBranchIds)
        {
            ICriteria criteria = Session.CreateCriteria<StudentVisited>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", StudentVisited.EntityStatus.Delete)));
            if (!String.IsNullOrWhiteSpace(Class))
                criteria.Add(Restrictions.Eq("Class", Class));
            if (!String.IsNullOrWhiteSpace(Year))
                criteria.Add(Restrictions.Eq("Year", Year));
            if (!String.IsNullOrWhiteSpace(StudentVisitType))
                criteria.Add(Restrictions.Eq("StudentVisitType", Convert.ToInt32(StudentVisitType)));
            if (!String.IsNullOrWhiteSpace(UserId))
                criteria.Add(Restrictions.Eq("CreateBy", Convert.ToInt64(UserId)));
            if (!String.IsNullOrWhiteSpace(InterestedOrganizationId))
                criteria.Add(Restrictions.Eq("InterestedOrganization.Id", Convert.ToInt64(InterestedOrganizationId)));
            if (InterestedBranchIds != null && InterestedBranchIds.Length > 0)
                criteria.Add(Restrictions.In("InterestedBranch.Id", InterestedBranchIds));

            if (!String.IsNullOrWhiteSpace(Name))
                criteria.Add(Restrictions.Like("NickName", Name, MatchMode.Anywhere));
            if (!String.IsNullOrWhiteSpace(Mobile))
                criteria.Add(Restrictions.Like("Mobile", Mobile, MatchMode.Anywhere));
            if (!String.IsNullOrWhiteSpace(Institute))
                criteria.Add(Restrictions.Like("Institute", Institute, MatchMode.Anywhere));
            return criteria;
        }
        private ICriteria GetStudentAdmissionVisitedCriteria(string Class, string Year, long ProgramId, long SessionId, string Name, string Mobile, string Institute, string StudentVisitType, int StudentType, long[] UserIds, long[] BranchIds)
        {
            ICriteria criteria = GetStudentVisitedCriteria(Class, Year, Name, Mobile, Institute, StudentVisitType, null, null, null);
            if (UserIds != null && UserIds.Length > 0)
                criteria.Add(Restrictions.In("CreateBy", UserIds));

            if (StudentType == 1)
            {
                criteria.CreateAlias("AdmittedStudentProgram", "asp");
                criteria.CreateAlias("asp.Program", "p");
                criteria.Add(Restrictions.Eq("p.Id", ProgramId));
                criteria.CreateAlias("asp.Batch", "b");
                criteria.CreateAlias("b.Session", "s");
                criteria.Add(Restrictions.Eq("s.Id", SessionId));
                if (BranchIds != null && BranchIds.Length > 0)
                {
                    criteria.CreateAlias("b.Branch", "br");
                    criteria.Add(Restrictions.In("br.Id", BranchIds));
                }
            }
            else if (StudentType == 2)
            {
                criteria.CreateAlias("AdmittedStudentProgram", "asp", JoinType.LeftOuterJoin);
                criteria.CreateAlias("asp.Program", "p", JoinType.LeftOuterJoin);
                criteria.CreateAlias("asp.Batch", "b", JoinType.LeftOuterJoin);
                criteria.CreateAlias("b.Session", "s", JoinType.LeftOuterJoin);
                criteria.Add(Restrictions.Or(
                    Restrictions.IsNull("AdmittedStudentProgram"),
                    Restrictions.And(
                        Restrictions.Not(Restrictions.Eq("p.Id", ProgramId)),
                        Restrictions.Not(Restrictions.Eq("s.Id", SessionId))
                    )
                ));
                if (BranchIds != null && BranchIds.Length > 0)
                {
                    criteria.CreateAlias("b.Branch", "br", JoinType.LeftOuterJoin);
                    criteria.Add(Restrictions.Or(
                        Restrictions.IsNull("AdmittedStudentProgram"),
                        Restrictions.Not(Restrictions.In("br.Id", BranchIds))
                    ));
                }
            }
            return criteria;
        }
        #endregion

    }
}