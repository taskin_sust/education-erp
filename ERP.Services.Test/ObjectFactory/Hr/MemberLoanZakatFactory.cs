using System;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Services.Hr;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class MemberLoanZakatFactory : ObjectFactoryBase<MemberLoanZakat>
    {

        private readonly IMemberLoanZakatService _prMemberLoanZakatService;

        public MemberLoanZakatFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _prMemberLoanZakatService = new MemberLoanZakatService(session);
        }

        #region Create

        public MemberLoanZakatFactory Create()
        {
            Object = new MemberLoanZakat()
            {
                Amount = 500,
                ApprovedDate = DateTime.Now,
                PaymentType = (int)PaymentType.Monthly,
                EffectiveDate = DateTime.Now,
                ClosingDate = DateTime.Now.AddMonths(5),
                FundedBy = (int)FundedBy.ManagingDirector
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public MemberLoanZakatFactory CreateWith(long id)
        {

            Object = null;
            SingleObjectList.Add(Object);
            return this;

        }

        public MemberLoanZakatFactory WithMemberLoanApplication(MemberLoanApplication memberLoanApplication)
        {
            Object.MemberLoanApplication = memberLoanApplication;
            return this;
        }

        public MemberLoanZakatFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {

                SingleObjectList.ForEach(x => { if (x.Id == 0) _prMemberLoanZakatService.SaveOrUpdate(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {

                ObjectList.ForEach(x => { if (x.Id == 0)_prMemberLoanZakatService.SaveOrUpdate(x); });

            }
            return this;
        }

        public MemberLoanZakatFactory CreateMore(int num)
        {

            for (int i = 0; i < num; i++)
            {
                var obj = new MemberLoanZakat() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public MemberLoanZakatFactory CleanUp()
        {

            DeleteAll();
            return this;
        }

        #endregion


    }
}
