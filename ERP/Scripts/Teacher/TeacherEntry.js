﻿$(document).ready(function() {
    window.validateTextField("NickName", "Please enter Nick Name.");
    window.validateTextField("Institute", "Please enter Institute.");
    window.validateTextField("Department", "Please enter Department.");
    window.validateTextField("HscPassingYear", "Please enter HSC Passing Year.");
    window.validateDropDownField("Religion", "Please Select Religion.");
    window.validateDropDownField("Gender", "Please Select Gender.");

    $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    $(document).on("input propertychange", "#DateOfBirth", function(e) {
        var dateOfBirthOfStudent = $("#DateOfBirth").val();
        if (dateOfBirthOfStudent) {
            if (dateOfBirthOfStudent.length > 0) {
                console.log(dateOfBirthOfStudent);
                dateOfBirthOfStudent = BirthDayCheck(dateOfBirthOfStudent);
                console.log(dateOfBirthOfStudent);
                if (dateOfBirthOfStudent == null) {
                    window.showErrorMessageBelowCtrl("DateOfBirth", "", false);
                    window.showErrorMessageBelowCtrl("DateOfBirth", "Please enter valid Date", true);
                } else {
                    var todayForCheck = new Date();
                    var selectedDate = new Date(dateOfBirthOfStudent);
                    if (parseInt(todayForCheck.getFullYear()) <= parseInt(selectedDate.getFullYear())) {
                        window.showErrorMessageBelowCtrl("DateOfBirth", "", false);
                        window.showErrorMessageBelowCtrl("DateOfBirth", "Please enter valid Date", true);
                    } else if (selectedDate.getFullYear() == '0000') {
                        window.showErrorMessageBelowCtrl("DateOfBirth", "", false);
                        window.showErrorMessageBelowCtrl("DateOfBirth", "Please enter valid Date", true);
                    } else if (dateOfBirthOfStudent.length != 10) {
                        window.showErrorMessageBelowCtrl("DateOfBirth", "", false);
                        window.showErrorMessageBelowCtrl("DateOfBirth", "Please enter valid Date", true);
                    } else {
                        $("#DateOfBirth").val(dateOfBirthOfStudent);
                        window.showErrorMessageBelowCtrl("DateOfBirth", "", false);
                    }
                }
            }
        } else {
            window.showErrorMessageBelowCtrl("DateOfBirth", "", false);
        }
    });

    $(document).on("input propertychange", "#HscPassingYear", function (e) {
        var value = $(this).val();
        checkPassingYear(value);
    });

    $(document).on("change", "#ActivityPriority1", function(event) {
        window.showErrorMessageBelowCtrl("ActivityPriority1", "", false);
        window.showErrorMessageBelowCtrl("ActivityPriority2", "", false);
        window.showErrorMessageBelowCtrl("ActivityPriority3", "", false);
        var val = $(this).val();
        if (val.length === 0) {
            window.showErrorMessageBelowCtrl("ActivityPriority1", "Please Select Activity 1", true);
        } else {
            window.showErrorMessageBelowCtrl("ActivityPriority1", "", false);
            window.showErrorMessageBelowCtrl("ActivityPriority2", "", false);
            window.showErrorMessageBelowCtrl("ActivityPriority3", "", false);
        }
    });

    $(document).on("change", "#ActivityPriority2", function(event) {
        window.showErrorMessageBelowCtrl("ActivityPriority1", "", false);
        window.showErrorMessageBelowCtrl("ActivityPriority2", "", false);
        window.showErrorMessageBelowCtrl("ActivityPriority3", "", false);
        var prevTa_1Value = $("#ActivityPriority1").val();
        if (prevTa_1Value.length <= 0) {
            window.showErrorMessageBelowCtrl("ActivityPriority2", "Please Select first priority", true);
            window.showErrorMessageBelowCtrl("ActivityPriority3", "", false);
        } else {
            var val = $(this).val();
            if (val.length === 0) {
                var prevTa_3Value = $("#ActivityPriority3").val();
                if (prevTa_3Value.length > 0) {
                    window.showErrorMessageBelowCtrl("ActivityPriority2", "Please Select Activity 2", true);
                }
            } else {
                window.showErrorMessageBelowCtrl("ActivityPriority2", "", false);
                window.showErrorMessageBelowCtrl("ActivityPriority3", "", false);
            }
        }
    });

    $(document).on("change", "#ActivityPriority3", function(event) {
        window.showErrorMessageBelowCtrl("ActivityPriority1", "", false);
        window.showErrorMessageBelowCtrl("ActivityPriority2", "", false);
        window.showErrorMessageBelowCtrl("ActivityPriority3", "", false);
        var prevTa_2Value = $("#ActivityPriority2").val();
        var prevTa_3Value = $(this).val();
        var prevTa_1Value = $("#ActivityPriority1").val();
        if (prevTa_3Value.length <= 0) {
            window.showErrorMessageBelowCtrl("ActivityPriority2", "", false);
            window.showErrorMessageBelowCtrl("ActivityPriority3", "", false);
            console.log(isProcessContinue);
        } else {
            if (prevTa_2Value.length <= 0) {
                window.showErrorMessageBelowCtrl("ActivityPriority2", "Please Select Second priority", true);
            }
        }

    });

    $(document).on("change", "#Subject_1", function(event) {
        window.showErrorMessageBelowCtrl("Subject_1", "", false);
        var val = $(this).val();
        if (val.length === 0) {
            window.showErrorMessageBelowCtrl("Subject_1", "Please Select Subject", true);
        } else {
            window.showErrorMessageBelowCtrl("Subject_1", "", false);
            window.showErrorMessageBelowCtrl("Subject_2", "", false);
            window.showErrorMessageBelowCtrl("Subject_3", "", false);
        }
    });

    $(document).on("change", "#Subject_2", function(event) {
        window.showErrorMessageBelowCtrl("Subject_2", "", false);
        var prevTa_1Value = $("#Subject_1").val();
        if (prevTa_1Value.length <= 0) {
            window.showErrorMessageBelowCtrl("Subject_3", "", false);
        } else {
            var val = $(this).val();
            if (val.length === 0) {
                var prevTa_3Value = $("#Subject_3").val();
                if (prevTa_3Value.length > 0) {
                    window.showErrorMessageBelowCtrl("Subject_2", "Please Select Activity", true);
                }
            } else {
                window.showErrorMessageBelowCtrl("Subject_2", "", false);
                window.showErrorMessageBelowCtrl("Subject_3", "", false);
            }
        }
    });

    $(document).on("change", "#Subject_3", function(event) {
        window.showErrorMessageBelowCtrl("Subject_3", "", false);
        var prevTa_1Value = $("#Subject_1").val();
        var prevTa_2Value = $("#Subject_2").val();
        var prevTa_3Value = $(this).val();
        if (prevTa_3Value.length <= 0) {
            window.showErrorMessageBelowCtrl("Subject_2", "", false);
            window.showErrorMessageBelowCtrl("Subject_3", "", false);
        }
    });

    $(document).on("click", ".dynamicButton", function (event) {
        var divId = $(".SubjectPriority:visible").length + 1;
        console.log(divId);
        var button = "<div id='testId' class='dynamicButton btn btn-primary'> + </div>";
        var cal = $(".dynamicSubject").html();
        var newD = cal.split("hidden").join('');
        newD = newD.replace('SubjectDynamic', 'Subject_' + divId);
        newD = newD.replace('data_priority_number', divId);
        var fDiv = newD.replace('col-md-offset-3', '');
        var spanDiv = "";
        if ($(".dynamicDiv").children().size() - 1 > 3) {
            console.log(">");
            $(".dynamicButton").remove();
            var count = $(".dynamicDiv").children().size() - 1;
            if (count % 3 == 0) {
                $(".dynamicDiv").append("<div class='col-md-offset-2 col-md-3 dynamicSubject' style='margin-top:15px' >" + newD + "</div>");
                $(".dynamicDiv").append(button);
            } else {
                $(".dynamicDiv").append("<div class='col-md-3 dynamicSubject' style='margin-top:15px' >" + newD + "</div>");
                $(".dynamicDiv").append(button);
            }
        } else {
            if ($("#testId").hasClass("testDiv")) {
                $(".dynamicButton").remove();
                console.log("First Call");
                $(".dynamicDiv").append("<div class='col-md-3 dynamicSubject col-md-offset-2'  >" + newD + "</div>");
                $(".dynamicDiv").append(button);
            } else {
                console.log("Second Call");
                $(".dynamicButton").remove();
                $(".dynamicDiv").append("<div class='col-md-3 dynamicSubject' id=''>" + fDiv + "</div>");
                $(".dynamicDiv").append(button);
            }
        }
        if ($("#Subject_" + divId).length) {
            $("#Subject_" + divId).prepend("<option value='' selected='selected'>Choice " + divId + "</option>");
        }
    });

    $(document).on("change", "#VersionPriority1", function (event) {
        window.showErrorMessageBelowCtrl("VersionPriority1", "", false);
        var val = $(this).val();
        if (val.length === 0) {
            window.showErrorMessageBelowCtrl("VersionPriority1", "Please Select Version", true);
        } else {
            window.showErrorMessageBelowCtrl("VersionPriority1", "", false);
            window.showErrorMessageBelowCtrl("VersionPriority2", "", false);
        }
    });

    $(document).on("change", "#VersionPriority2", function(event) {
        window.showErrorMessageBelowCtrl("VersionPriority2", "", false);
        var prevTa_1Value = $("#VersionPriority1").val();
        if (prevTa_1Value.length <= 0) {
            window.showErrorMessageBelowCtrl("VersionPriority2", "Please Select first priority", true);
        }
    });

    $(document).on("input propertychange", "#MobileNumber1", function (event) {
        mobileCheckWithId($("#MobileNumber1").val().trim(), "MobileNumber1");
    });

    $(document).on("input propertychange", "#MobileNumber2", function (event) {
        if ($("#MobileNumber2").val().trim().length > 0) {
            mobileCheckWithId($("#MobileNumber2").val().trim(), "MobileNumber2");
        } else {
            window.showErrorMessageBelowCtrl("#MobileNumber2", "", false);
        }
    });

    $(document).on("input propertychange", "#MobileNumberRoommate", function (event) {
        if ($("#MobileNumberRoommate").val().trim().length > 0) {
            mobileCheckWithId($("#MobileNumberRoommate").val().trim(), "MobileNumberRoommate");
        } else {
            window.showErrorMessageBelowCtrl("#MobileNumberRoommate", "", false);
        }
    });

    $(document).on("input propertychange", "#MobileNumberFather", function (event) {
        if ($("#MobileNumberFather").val().trim().length > 0) {
            mobileCheckWithId($("#MobileNumberFather").val().trim(), "MobileNumberFather");
        } else {
            window.showErrorMessageBelowCtrl("#MobileNumberFather", "", false);
        }
    });

    $(document).on("input propertychange", "#MobileNumberMother", function (event) {
        if ($("#MobileNumberMother").val().trim().length > 0) {
            mobileCheckWithId($("#MobileNumberMother").val().trim(), "MobileNumberMother");
        } else {
            window.showErrorMessageBelowCtrl("#MobileNumberMother", "", false);
        }
    });

    $(document).on('click', '.orgName', function () {
        var idvalue = "";
        $('.orgName').each(function (index, value) {

            if ($(this).is(':checked')) {
                console.log($(this).val());
                idvalue += $(this).val() + ",";
            }
        });
        if (idvalue.length <= 0) {
            window.showErrorMessageBelowCtrl("TeacherOrganization", "", false);
            window.showErrorMessageBelowCtrl("TeacherOrganization", "Please Select at least one organization.", true);
        } else {
            window.showErrorMessageBelowCtrl("TeacherOrganization", "", false);
        }
        console.log(idvalue);
        $.ajax({
            url: "/Teachers/Teacher/GetActivities",
            type: "POST",
            data: { idValue: idvalue },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $('.wishList').html(data);
            },
            complete: function () {
                $.unblockUI();
            },

            error: function (data) {
                bootbox.alert(data.Message).css('margin-top', (($(window).height() / 4)));
            }
        });

    });

    $(document).on('click', '#teacherEntry', function (event) {
        var isSuccess = true;
        var idvalue = "";
        var id = $("#Id").val().trim();
        var tpin = $("#Tpin").val().trim();
        var nickName = $("#NickName").val().trim();
        var mobileNumber1 = $("#MobileNumber1").val().trim();
        var mobileNumber2 = $("#MobileNumber2").val().trim();
        var institute = $("#Institute").val().trim();
        var department = $("#Department").val().trim();
        var hscPassingYear = $("#HscPassingYear").val().trim();
        var religion = $("#Religion").val();
        var gender = $("#Gender").val();
        var fullName = $("#FullName").val().trim();
        var fatherName = $("#FatherName").val().trim();
        var dateOfBirth = $("#DateOfBirth").val().trim();
        var bloodGroup = $("#BloodGroup").val();
        var mobileNumberRoommate = $("#MobileNumberRoommate").val().trim();
        var email = $("#Email").val().trim();
        var mobileNumberFather = $("#MobileNumberFather").val().trim();
        var mobileNumberMother = $("#MobileNumberMother").val().trim();
        var facebookId = $("#FacebookId").val().trim();
        var presentAddress = $("#PresentAddress").val().trim();
        var area = $("#Area").val().trim();
        var permanentAddress = $("#PermanentAddress").val().trim();
        var district = $("#District").val().trim();
        var teacherActivity1 = $("#ActivityPriority1").val();
        var teacherActivity2 = $("#ActivityPriority2").val();
        var teacherActivity3 = $("#ActivityPriority3").val();
        var subject1 = $("#Subject_1").val();
        var version1 = $("#VersionPriority1").val();
        var mobileNo1 = $("#MobileNumber1").val();
        var imageUploadVal = $("#ImageUpload").val();
        var teacherOrganizationViewModelArray = Array();
        var teacherActivityViewModelArray = Array();
        var teacherSubjectPriorityViewModelArray = Array();
        var teacherVersionOfStudyPriorityViewModelArray = Array();
        var teacherAdmissionViewModelArray = Array();
        var teacherPreviousExtraCuricularActivityViewModelArray = Array();
        var teacherWishExtraCuricularActivityViewModelArray = Array();

        var teacherOrganizationCount = $(".orgName:checked").length;

        var formData = new FormData();
        //var teacherViewModel = new TeacherViewModel();
        //validation
        if (nickName === "" || nickName.length <= 0) {
            window.showErrorMessageBelowCtrl("NickName", "", false);
            window.showErrorMessageBelowCtrl("NickName", "Please Enter Nick Name", true);
            isSuccess = false;
        } else {
            window.showErrorMessageBelowCtrl("NickName", "", false);
            //teacherViewModel.NickName = nickName;
            formData.append("NickName", nickName);
        }

        if (mobileNumber1 !== "" || mobileNumber1.length > 0) {
            if (mobileCheckWithId(mobileNumber1.trim(), "MobileNumber1") === true) {
                window.showErrorMessageBelowCtrl("MobileNumber1", "", false);
                //teacherViewModel.MobileNumber1 = mobileNumber1;
                formData.append("MobileNumber1", mobileNumber1);
            } else {
                isSuccess = false;
            }
        } else {
            window.showErrorMessageBelowCtrl("MobileNumber1", "", false);
            window.showErrorMessageBelowCtrl("MobileNumber1", "Please Enter Valid Mobile Number", true);
            isSuccess = false;
        }

        if (mobileNumber2 !== "" || mobileNumber2.length > 0) {
            if (mobileCheckWithId(mobileNumber2.trim(), "MobileNumber2") === true) {
                window.showErrorMessageBelowCtrl("MobileNumber2", "", false);
                //teacherViewModel.MobileNumber2 = mobileNumber2;
            } else {
                isSuccess = false;
            }
        } else {
            window.showErrorMessageBelowCtrl("MobileNumber2", "", false);
            //window.showErrorMessageBelowCtrl("MobileNumber2", "Please Enter Valid Mobile Number", true);
            //isSuccess = false;
        }

        if (institute === "" || institute.length <= 0) {
            window.showErrorMessageBelowCtrl("Institute", "", false);
            window.showErrorMessageBelowCtrl("Institute", "Please Enter Institute", true);
            isSuccess = false;
        } else {
            window.showErrorMessageBelowCtrl("Institute", "", false);
            //teacherViewModel.Institute = institute;
            formData.append("Institute", institute);
        }

        if (department === "" || department.length <= 0) {
            window.showErrorMessageBelowCtrl("Department", "", false);
            window.showErrorMessageBelowCtrl("Department", "Please Enter Department", true);
            isSuccess = false;
        } else {
            //window.showErrorMessageBelowCtrl("Department", "", false);
            formData.append("Department", department);
        }

        if (hscPassingYear === "" || hscPassingYear.length <= 0) {
            window.showErrorMessageBelowCtrl("HscPassingYear", "", false);
            window.showErrorMessageBelowCtrl("HscPassingYear", "Please Enter HscPassingYear", true);
            isSuccess = false;
        } else {
            if (checkPassingYear(hscPassingYear) === true) {
                window.showErrorMessageBelowCtrl("HscPassingYear", "", false);
                //teacherViewModel.HscPassingYear = hscPassingYear;
                formData.append("HscPassingYear", hscPassingYear);
            }
        }

        if (religion === "" || religion.length <= 0) {
            window.showErrorMessageBelowCtrl("Religion", "", false);
            window.showErrorMessageBelowCtrl("Religion", "Please select Religion", true);
            isSuccess = false;
        } else {
            window.showErrorMessageBelowCtrl("Religion", "", false);
            //teacherViewModel.Religion = religion;
            formData.append("Religion", religion);
        }

        if (gender === "" || gender.length <= 0) {
            window.showErrorMessageBelowCtrl("Gender", "", false);
            window.showErrorMessageBelowCtrl("Gender", "Please select Gender", true);
            isSuccess = false;
        } else {
            window.showErrorMessageBelowCtrl("Gender", "", false);
            //teacherViewModel.Gender = gender;
            formData.append("Gender", gender);
        }

        if (mobileNumberRoommate.length > 0) {
            if (mobileCheckWithId(mobileNumberRoommate.trim(), "MobileNumberRoommate") === true) {
                //teacherViewModel.MobileNumberRoommate = mobileNumberRoommate.trim();
                formData.append("MobileNumberRoommate", mobileNumberRoommate.trim());
            } else {
                isSuccess = false;
            }
        } else {
            window.showErrorMessageBelowCtrl("MobileNumberRoommate", "", false);
        }

        if (mobileNumberFather.length > 0) {
            if (mobileCheckWithId(mobileNumberFather.trim(), "MobileNumberFather") === true) {
                //teacherViewModel.MobileNumberFather = mobileNumberFather.trim();
                formData.append("MobileNumberFather", mobileNumberFather.trim());
            } else {
                isSuccess = false;
            }
        } else {
            window.showErrorMessageBelowCtrl("MobileNumberFather", "", false);
        }

        if (dateOfBirth.length > 0) {
            dateOfBirth = BirthDayCheck(dateOfBirth);
            if (dateOfBirth == null) {
                window.showErrorMessageBelowCtrl("DateOfBirth", "", false);
                window.showErrorMessageBelowCtrl("DateOfBirth", "Please enter valid Date", true);
                isSuccess = false;
            } else {
                window.showErrorMessageBelowCtrl("DateOfBirth", "", false);
            }
        } else {
            window.showErrorMessageBelowCtrl("DateOfBirth", "", false);
        }

        if (mobileNumberMother.length > 0) {
            if (mobileCheckWithId(mobileNumberMother.trim(), "MobileNumberMother") === true) {
                // teacherViewModel.MobileNumberMother = mobileNumberMother.trim();
                formData.append("MobileNumberMother", mobileNumberMother.trim());
            } else {
                isSuccess = false;
            }
        } else {
            window.showErrorMessageBelowCtrl("MobileNumberMother", "", false);
        }


        if (imageUploadVal.length > 0) {
            var ext = imageUploadVal.split('.').pop().toLowerCase();
            var fileSize = $("#ImageUpload")[0].files[0].size;

            if ($.inArray(ext, ['jpg', 'jpeg']) === -1) {
                window.showErrorMessageBelowCtrl("ImageUpload", "", false);
                window.showErrorMessageBelowCtrl("ImageUpload", "This file type is not valid. Only valid file type is image with *.jpg or *jpeg extension.", true);
                isSuccess = false;
            } else if (fileSize < 0) {
                window.showErrorMessageBelowCtrl("ImageUpload", "", false);
                window.showErrorMessageBelowCtrl("ImageUpload", "The image size is too small.", true);
                isSuccess = false;
            } else {
                formData.append("ImageUpload", document.getElementById("ImageUpload").files[0]);
            }
        } else {
            window.showErrorMessageBelowCtrl("ImageUpload", "", false);
        }

        if (teacherOrganizationCount <= 0) {
            isSuccess = false;
            window.showErrorMessageBelowCtrl("TeacherOrganization", "", false);
            window.showErrorMessageBelowCtrl("TeacherOrganization", "Please Select at least one organization.", true);
        } else {
            window.showErrorMessageBelowCtrl("TeacherOrganization", "", false);
            $('.orgName').each(function (index, value) {
                if ($(this).is(':checked')) {
                    var teacherOrganization = new TeacherOrganizationViewModel();
                    teacherOrganization.TeacherId = $(this).attr("data-teacherId");
                    teacherOrganization.OrganizationId = $(this).val();
                    teacherOrganizationViewModelArray.push(teacherOrganization);
                }
            });
        }

        if (teacherActivity1.length <= 0) {
            window.showErrorMessageBelowCtrl("ActivityPriority1", "", false);
            window.showErrorMessageBelowCtrl("ActivityPriority1", "Please Select first.", true);
            isSuccess = false;
        } else {
            window.showErrorMessageBelowCtrl("ActivityPriority1", "", false);
        }

        if (subject1.length <= 0) {
            window.showErrorMessageBelowCtrl("Subject_1", "", false);
            window.showErrorMessageBelowCtrl("Subject_1", "Please Select first.", true);
            isSuccess = false;
        } else {
            window.showErrorMessageBelowCtrl("Subject_1", "", false);
        }

        if (version1.length <= 0) {
            window.showErrorMessageBelowCtrl("VersionPriority1", "", false);
            window.showErrorMessageBelowCtrl("VersionPriority1", "Please Select first.", true);
            isSuccess = false;
        } else {
            window.showErrorMessageBelowCtrl("VersionPriority1", "", false);
        }

        $(".ActivityPriority").each(function () {
            var val = $(this).val();
            if (val !== "" && val.length > 0) {
                var teacherActivityViewModel = new TeacherActivityViewModel();
                teacherActivityViewModel.ActivityId = $(this).val();
                teacherActivityViewModel.Priority = $(this).attr("data-priority");
                teacherActivityViewModelArray.push(teacherActivityViewModel);
            }
        });

        $(".SubjectPriority:visible").each(function () {
            var val = $(this).val();
            if (val !== "" && val.length > 0) {
                var teacherSubjectPriorityViewModel = new TeacherSubjectPriorityViewModel();
                teacherSubjectPriorityViewModel.SubjectId = $(this).val();
                teacherSubjectPriorityViewModel.Priority = $(this).attr("data-priority");
                teacherSubjectPriorityViewModelArray.push(teacherSubjectPriorityViewModel);
            }
        });

        $(".VersionPriority").each(function () {
            var val = $(this).val();
            if (val !== "" && val.length > 0) {
                var teacherVersionOfStudyPriorityViewModel = new TeacherVersionOfStudyPriorityViewModel();
                teacherVersionOfStudyPriorityViewModel.VersionOfStudyId = $(this).val();
                teacherVersionOfStudyPriorityViewModel.Priority = $(this).attr("data-priority");
                teacherVersionOfStudyPriorityViewModelArray.push(teacherVersionOfStudyPriorityViewModel);
            }
        });

        $(".academicInfo").each(function () {
            var instituteName = $(this).find('.name').html();
            var atposition = $(this).find('.atposition').val();
            var atsession = $(this).find('.atsession').val();
            var gotsubject = $(this).find('.gotsubject').val();

            if (instituteName !== "" && atposition !== "" && atsession !== "" && gotsubject !== "" && instituteName.length > 0 && atposition.length > 0 && atsession.length > 0 && gotsubject.length) {
                var teacherAdmissionViewModel = new TeacherAdmissionViewModel();
                teacherAdmissionViewModel.InstituteName = instituteName;
                teacherAdmissionViewModel.AtPosition = atposition;
                teacherAdmissionViewModel.AtSession = atsession;
                teacherAdmissionViewModel.GotSubject = gotsubject;
                teacherAdmissionViewModelArray.push(teacherAdmissionViewModel);
            }

        });
        console.log(teacherAdmissionViewModelArray);


        $('.TeacherPreviousExtraCuricularActivity').each(function (index, value) {
            if ($(this).is(':checked')) {
                var teacherPreviousExtraCuricularActivityViewModel = new TeacherPreviousExtraCuricularActivityViewModel();
                teacherPreviousExtraCuricularActivityViewModel.TeacherId = $(this).attr("data-teacherId");
                teacherPreviousExtraCuricularActivityViewModel.ExtraCuricularActivityId = $(this).val();
                teacherPreviousExtraCuricularActivityViewModelArray.push(teacherPreviousExtraCuricularActivityViewModel);
            }
        });

        if ($('.TeacherWishExtraCuricularActivity').length > 0) {
            $('.TeacherWishExtraCuricularActivity').each(function (index, value) {
                if ($(this).is(':checked')) {
                    var teacherWishExtraCuricularActivityViewModel = new TeacherWishExtraCuricularActivityViewModel();
                    teacherWishExtraCuricularActivityViewModel.TeacherId = $(this).attr("data-teacherId");
                    teacherWishExtraCuricularActivityViewModel.ExtraCuricularActivityId = $(this).val();
                    teacherWishExtraCuricularActivityViewModelArray.push(teacherWishExtraCuricularActivityViewModel);
                }
            });
        }

        console.log(isSuccess);
        if (isSuccess === true) {
            //if (false) {
            event.preventDefault();
            formData.append("Id", id);
            formData.append("Tpin", Tpin);
            formData.append("MobileNumber2", mobileNumber2);
            formData.append("TeacherOrganizationString", JSON.stringify(teacherOrganizationViewModelArray));
            formData.append("TeacherActivityString", JSON.stringify(teacherActivityViewModelArray));
            formData.append("TeacherSubjectPriorityString", JSON.stringify(teacherSubjectPriorityViewModelArray));
            formData.append("TeacherVersionOfStudyPriorityString", JSON.stringify(teacherVersionOfStudyPriorityViewModelArray));
            formData.append("FullName", fullName);
            formData.append("FatherName", fatherName);
            formData.append("DateOfBirth", dateOfBirth);
            formData.append("BloodGroup", bloodGroup);
            formData.append("Email", email);
            formData.append("FacebookId", facebookId);
            formData.append("PresentAddress", presentAddress);
            formData.append("Area", area);
            formData.append("PermanentAddress", presentAddress);
            formData.append("District", district);
            formData.append("TeacherAdmissionString", JSON.stringify(teacherAdmissionViewModelArray));
            formData.append("TeacherPreviousExtraCuricularActivityString", JSON.stringify(teacherPreviousExtraCuricularActivityViewModelArray));
            formData.append("TeacherWishExtraCuricularActivityString", JSON.stringify(teacherWishExtraCuricularActivityViewModelArray));
            $.ajax({
                url: $("body").attr("data-project-root") + "Teacher/SaveTeacher",
                type: "POST",
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    if (response.IsSuccess) {
                        if (parseInt(id) === 0) {
                            $('#teacherEntryBtn')[0].reset();
                        }
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "s",
                            displayTime: 120000,
                        });
                        if (response.New === true && parseInt(id) === 0) {
                            // var url = $("body").attr("data-project-root") + "Teachers/Teacher/Edit?tpin=__tPin__";
                            // window.location.href = url.replace('__tPin__', response.tpin);
                        }
                        if (parseInt(id) > 0 && response.imageSrc !== "") {
                            $('#teacherImage').attr('src', response.imageSrc);
                            $('#ImageUpload').val('');
                        }
                    } else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "e"
                        });
                    }
                },

                complete: function () {
                    $.unblockUI();
                },
                error: function (response) {
                    $.unblockUI();
                    $.fn.customMessage({
                        displayMessage: "error occured during calculate teacher",
                        displayMessageType: "e",
                    });

                }
            });


        } else {
            event.preventDefault();
        }
    });

});

function TeacherViewModel() {
    this.Id = 0;
    this.Tpin = 0;
    this.NickName = "";
    this.MobileNumber1 = "";
    this.MobileNumber2 = "";
    this.Institute = "";
    this.Department = "";
    this.HscPassingYear = 0;
    this.Religion = 0;
    this.Gender = 0;
    this.TeacherOrganization = [];
    this.FullName = "";
    this.FatherName = "";
    this.DateOfBirth = "";
    this.BloodGroup = 0;
    this.MobileNumberRoommate = "";
    this.Email = "";
    this.MobileNumberFather = "";
    this.MobileNumberMother = "";
    this.FacebookId = "";
    this.PresentAddress = "";
    this.Area = "";
    this.PermanentAddress = "";
    this.District = "";
    this.TeacherActivity = [];
    this.TeacherSubjectPriority = [];
    this.TeacherVersionOfStudyPriority = [];
    this.TeacherAdmission = [];
    this.TeacherPreviousExtraCuricularActivity = [];
    this.TeacherWishExtraCuricularActivity = [];
    this.Images = [];
    this.ImageUpload = null;
    this.ImagesString = "";
    this.TeacherViewModel = function () { };
}

function TeacherOrganizationViewModel() {
    this.OrganizationId = 0;
    this.TeacherId = 0;
    this.TeacherOrganizationViewModel = function () { };
}

function TeacherActivityViewModel() {
    this.ActivityId = 0;
    this.TeacherId = 0;
    this.Priority = 0;
    this.TeacherActivityViewModel = function () { };
}

function TeacherSubjectPriorityViewModel() {
    this.SubjectId = 0;
    this.TeacherId = 0;
    this.Priority = 0;
    this.TeacherSubjectPriorityViewModel = function () { };
}

function TeacherVersionOfStudyPriorityViewModel() {
    this.VersionOfStudyId = 0;
    this.TeacherId = 0;
    this.Priority = 0;
    this.TeacherVersionOfStudyPriorityViewModel = function () { };
}

function TeacherAdmissionViewModel() {
    this.InstituteId = 0;
    this.TeacherId = 0;
    this.InstituteName = "";
    this.AtPosition = "";
    this.AtSession = "";
    this.GotSubject = "";
    this.TeacherAdmissionViewModel = function () { };
}

function TeacherPreviousExtraCuricularActivityViewModel() {
    this.ExtraCuricularActivityId = 0;
    this.TeacherId = 0;
    this.TeacherPreviousExtraCuricularActivityViewModel = function () { };
}

function TeacherWishExtraCuricularActivityViewModel() {
    this.ExtraCuricularActivityId = 0;
    this.TeacherId = 0;
    this.TeacherWishExtraCuricularActivityViewModel = function () { };
}

function mobileCheckWithId(mobileNumber, id) {
    var isSuccess = false;
    var mobile = $("#" + id).val().trim();
    mobile = mobileCheck(mobile);
    if (mobile == null) {
        window.showErrorMessageBelowCtrl(id, "", false);
        window.showErrorMessageBelowCtrl(id, "Please enter valid mobile number", true);
    } else {
        $("#" + id).val(mobile);
        window.showErrorMessageBelowCtrl(id, "", false);
        isSuccess = true;
    }
    return isSuccess;
}

function BirthDayCheck(val) {
    var birthDayPattern = /^[0-9]\d{1}\/[0-9]\d{1}\/19|20[0-9]{2}$/;
    if (val.match(birthDayPattern) != null) {
        var todayForCheck = new Date();
        var spiltDate = val.split('/');
        var day = parseInt(spiltDate[0]);
        var month = parseInt(spiltDate[1]);
        var year = parseInt(spiltDate[2]);
        var nowYear = todayForCheck.getFullYear();
       
        if (nowYear <= year) {
            return null;
        } else if (nowYear - year < 0) {
            return null;
        } else if (nowYear - year <= 10) {
            return null;
        } else if (nowYear - year >= 35) {
            return null;
        }
        console.log("day:" + day + " mo:" + month + " ye:" + year);
       // return val;
        if (month === 4 || month === 6 || month === 9 || month === 11) {
            if (day <= 30 && day > 0) {
                console.log("day:" + day + " mo:" + month + " ye4:" + year);
                return val;
            } else {
                return null;
            }
        } else if ((month === 2 && day <= 29 && year % 4 === 0 && day > 0) || (month === 2 && day <= 28 && year % 4 !== 0 && day > 0)) {
            return val;
        } else if ((month === 1 || month === 3 || month === 5 || month === 7 || month === 8 || month === 10 || month === 12) && day <= 31 && day > 0) {
            return val;
        } else
            return null;
    } else
        return null;
};

function checkPassingYear(value) {
    value = value.trim();
    var isSuccess = false;
    var passingYearPatteren = new RegExp(/^(\d{4})$/);
    if (value.length > 0) {
        console.log(value.match(passingYearPatteren));
        if (value.match(passingYearPatteren) === null) {
            window.showErrorMessageBelowCtrl("HscPassingYear", "", false);
            window.showErrorMessageBelowCtrl("HscPassingYear", "Digits Only", true);
            console.log(value);
        } else {
            window.showErrorMessageBelowCtrl("HscPassingYear", "", false);
            isSuccess = true;
        }
    } else {
        window.showErrorMessageBelowCtrl("HscPassingYear", "", false);
        window.showErrorMessageBelowCtrl("HscPassingYear", "Please enter passing Year", true);
    }
    return isSuccess;
}