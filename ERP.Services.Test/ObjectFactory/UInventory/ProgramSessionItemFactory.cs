using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

   public class ProgramSessionItemFactory : ObjectFactoryBase<ProgramSessionItem>
   {
       private readonly IProgramSessionItemService _programSessionItemService;
       private readonly ItemFactory _itemFactory;
       private readonly ProgramFactory _programFactory;
       private readonly SessionFactory _sessionFactory;
       private readonly OrganizationFactory _organizationFactory;

       public ProgramSessionItemFactory(IObjectFactoryBase caller, ISession session): base(caller, session)
       {
           _programSessionItemService = new ProgramSessionItemService(session);
            _organizationFactory = new OrganizationFactory(this, session);
            _sessionFactory = new SessionFactory(this, session);
           _itemFactory = new ItemFactory(this,session);
           _programFactory = new ProgramFactory(this,session);
           
           
       }

       #region Create 

       public ProgramSessionItemFactory Create()
       {
           Object = new ProgramSessionItem()
           {
               Id = 0,
               BusinessId = GetPcUserName(),
               CreateBy = 1,
               ModifyBy = 1,
               Status = ProgramSessionItem.EntityStatus.Active
           };
           SingleObjectList.Add(Object);
           return this;
       }
       
       public ProgramSessionItemFactory CreateMore(int num)
       {
           _organizationFactory.Create().Persist();
           _itemFactory.CreateWith(0,Guid.NewGuid().ToString(),(int)ItemType.ProgramItem,(int)ItemUnit.Pcs,(int)CostBearer.Corporate)
               .WithItemGroup().WithOrganization(_organizationFactory.Object).Persist();
           _programFactory.Create().WithOrganization(_organizationFactory.Object).Persist();
           _sessionFactory.Create().Persist();
           for (int i = 0; i < num; i++)
           {
               var obj = new ProgramSessionItem()
               {
                   Id = 0,
                   BusinessId = GetPcUserName(),
                   CreateBy = 1,
                   ModifyBy = 1,
                   Status = ProgramSessionItem.EntityStatus.Active
               };
               obj.Program = _programFactory.Object;
               obj.Session = _sessionFactory.Object;
               obj.Item = _itemFactory.Object;
               ObjectList.Add(obj);
           }
           return this;
       }

       public ProgramSessionItemFactory CreateMore(List<Organization> orgList,List<Item> itemList )
       {
           _programFactory.CreateMore(5, orgList).Persist();
           _sessionFactory.CreateMore(5).Persist();
           foreach (var item in itemList)
           {
               Random rnd = new Random();
               int r = rnd.Next(_programFactory.ObjectList.Count);
               var obj = new ProgramSessionItem()
               {
                   Id = 0,
                   BusinessId = GetPcUserName(),
                   CreateBy = 1,
                   ModifyBy = 1,
                   Status = ProgramSessionItem.EntityStatus.Active
               };
               if (item.ItemType == (int) ItemType.ProgramItem)
               {
                   obj.Program = _programFactory.ObjectList[r];
                   obj.Session = _sessionFactory.ObjectList.FirstOrDefault();
               }
               obj.Item = item;
               ObjectList.Add(obj);
           }
           return this;
       }


       public ProgramSessionItemFactory CreateWithProperties(long id,Item item,Program program,Session session)
       {
           Object = new ProgramSessionItem()
           {
               Id = id,
               BusinessId = GetPcUserName(),
               CreateBy = 1,
               ModifyBy = 1,
               Status = ProgramSessionItem.EntityStatus.Active,
               Item = item,
               Program = program,
               Session = session
           };
           SingleObjectList.Add(Object);
           return this;
       }

       public ProgramSessionItemFactory WithItem(Item item)
       {
           Object.Item = item;
           return this;
       }


       public ProgramSessionItemFactory WithItem(string name,int itemType,int itemUnit,int costBearer,Organization organization)
       {
           _itemFactory.CreateWith(0, name, itemType, itemUnit, costBearer).WithItemGroup().WithOrganization(organization).Persist();
           Object.Item = _itemFactory.Object;
           return this;
       }

       public ProgramSessionItemFactory WithItem(Organization organization)
       {
           _itemFactory.Create().WithItemGroup().WithOrganization(organization).Persist();
           Object.Item = _itemFactory.Object;
           return this;
       }

       public ProgramSessionItemFactory WithProgramAndSession(Program program,Session session)
       {
           Object.Program = program;
           Object.Session = session;
           return this;
       }

       public ProgramSessionItemFactory WithProgramAndSession()
       {
           _programFactory.Create().WithOrganization().Persist();
           _sessionFactory.Create().Persist();
           Object.Program = _programFactory.Object;
           Object.Session = _sessionFactory.Object;
           return this;
       }

       public ProgramSessionItemFactory WithProgramAndSession(Organization organization)
       {
           _programFactory.Create().WithOrganization(organization).Persist();
           _sessionFactory.Create().Persist();
           Object.Program = _programFactory.Object;
           Object.Session = _sessionFactory.Object;
           return this;
       }
       
       public ProgramSessionItemFactory Persist()
       {
           if (SingleObjectList != null && SingleObjectList.Count > 0)
           {
               var list = new List<ProgramSessionItem>();
               SingleObjectList.ForEach(x =>
               {
                   if (x.Id == 0) list.Add(x);
               });
               _programSessionItemService.SaveOrUpdate(list);
           }

           if (ObjectList != null && ObjectList.Count > 0)
           {
               var list = new List<ProgramSessionItem>();
               ObjectList.ForEach(x =>
               {
                   if (x.Id == 0) list.Add(x);
               });
               _programSessionItemService.SaveOrUpdate(list);
           }
           return this;
       }

       #endregion

       #region other
       #endregion

       #region CleanUp 

       public ProgramSessionItemFactory CleanUp()
       {
          DeleteAll();
          _itemFactory.CleanUp();
           _programFactory.Cleanup();
           _sessionFactory.Cleanup();
           _organizationFactory.Cleanup();
          return this;
       }
       public ProgramSessionItemFactory CleanUpForOpenningBalance()
       {
           DeleteAll();
           _itemFactory.CleanUp();
           return this;
       }
       #endregion

    }
}
