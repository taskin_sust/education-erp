using System;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Services.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
namespace UdvashERP.Services.Test.ObjectFactory.UInventory
{

    public class WorkOrderFactory : ObjectFactoryBase<WorkOrder>
    {

        private readonly IWorkOrderService _workOrderService;
        internal WorkOrderDetailsFactory _workOrderDetailsFactory;

        public WorkOrderFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _workOrderService = new WorkOrderService(session);
            _workOrderDetailsFactory = new WorkOrderDetailsFactory(null, session);
        }

        #region Create

        public WorkOrderFactory Create()
        {
            Object = new WorkOrder()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                DeliveryDate = DateTime.Now.AddDays(10),
                OrderedQuantity = 100,
                WorkOrderDate = DateTime.Now.AddDays(5),
                WorkOrderNo = "WOUDVCd123456",
                Remarks = "Test",
                TotalCost = 123,
                WorkOrderStatus = WorkOrderStatus.Issued
            };
            SingleObjectList.Add(Object);
            return this;
        }



        public WorkOrderFactory CreateWith(long id, DateTime deliveryDate, int orderQuantity, int totalCost, DateTime workOrderDate,WorkOrderStatus workOrderStatus)
        {
            Object = new WorkOrder()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                CreateBy = 1,
                ModifyBy = 1,
                DeliveryDate = deliveryDate,
                OrderedQuantity = orderQuantity,
                TotalCost = totalCost,
                WorkOrderDate = workOrderDate,
                WorkOrderNo = "WOUDVCd123456",
                Remarks = "Test",
                WorkOrderStatus = workOrderStatus
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public WorkOrderFactory WithSupplier(Supplier supplier)
        {
            Object.Supplier = supplier;
            return this;
        }
        public WorkOrderFactory WithQuotation(Quotation quotation)
        {
            Object.Quotation = quotation;
            return this;
        }
        public WorkOrderFactory WithBranch(Branch branch)
        {
            Object.Branch = branch;
            return this;
        }

        public WorkOrderFactory WithWorkOrderDetails(Item item, Program program, Session session, WorkOrder workOrder)
        {
            _workOrderDetailsFactory.Create()
                .WithItem(item)
                .WithProgram(program)
                .WithSession(session)
                .WithPurposeOfProgramAndSession(program,session)
                .WithWorkOrder(workOrder)
                .WithWorkOrderCriteria(_workOrderDetailsFactory.Object);
            Object.WorkOrderDetails = _workOrderDetailsFactory.SingleObjectList;
            return this;
        }

        public WorkOrderFactory Persist()
        {

            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x => { if (x.Id == 0) _workOrderService.WorkOrderSave(x); });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x => { if (x.Id == 0)  _workOrderService.WorkOrderSave(x); });
            }
            return this;
        }

        public WorkOrderFactory CreateMore(int num)
        {
            for (int i = 0; i < num; i++)
            {
                var obj = new WorkOrder() { };
                ObjectList.Add(obj);
            }
            return this;
        }

        #endregion

        #region other
        #endregion

        #region CleanUp

        public WorkOrderFactory CleanUp()
        {
            _workOrderDetailsFactory.CleanUp();
            DeleteAll();
            return this;
        }

        internal void CleanUpCurrentStockQuantity(Program program, Session session, Branch branch, Item item)
        {
            string str = "";
            if (branch != null && item != null)
            {
                if (program != null && session != null)
                {
                    str = " [ProgramId]=" + program.Id + " AND [SessionId]=" + session.Id + "";
                }
                else
                {
                    str = " [ProgramId] IS NULL AND [SessionId] IS NULL";
                }
                var tableName = GetEntityTableName(typeof(CurrentStockSummary));
                string swData = "delete from " + tableName + " where " + str + " AND [BranchId]=" + branch.Id + " AND [ItemId]=" + item.Id + ";";
                _session.CreateSQLQuery(swData).ExecuteUpdate();
            }
            
        }

        #endregion
    }
}
