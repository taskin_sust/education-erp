﻿using System;

namespace UdvashERP.MessageExceptions
{
    public class ValueNotAssignedException : Exception
    {
        public ValueNotAssignedException():base("Value Not Assigned")
        {

        }

        public ValueNotAssignedException(string message)
            : base(message)
        {

        } 
    }
}
