﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;
using SubjectAssign = UdvashERP.BusinessModel.Dto.SubjectAssign;

namespace UdvashERP.Services.Test.AdminisTration.SubjectTest
{
    public interface ISubjectSaveTest
    {
        #region Basic test
        [Fact]
        void Save_Should_Return_Null_Exception();

        [Fact]
        void Should_Save_Successfully();

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        void Should_Not_Save_Empty_Field(int index); 
        #endregion

        #region Business logic test
        [Fact]
        void Save_Should_Check_Duplicate_Name_Entry();

        [Fact]
        void Save_Should_Check_Duplicate_ShortName_Entry(); 
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Subject")]
    public class SubjectSaveTest : SubjectBaseTest, ISubjectSaveTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _subjectService.SubjectSaveOrUpdate(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var subjectFactory =
                _subjectFactory.Create().WithNameAndRank(_name, 1).WithProperties(_shortName)
                    .Persist();
            Subject subjectObj = _subjectService.GetSubject(subjectFactory.Object.Id);
            Assert.NotNull(subjectObj);
            Assert.True(subjectObj.Id > 0);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void Should_Not_Save_Empty_Field(int index)
        {
            if (index == 1)
            {
                _subjectFactory.Create().WithNameAndRank("", 1).WithProperties(_shortName);
            }
            else if (index == 2)
            {
                _subjectFactory.Create().WithProperties("");
            }
            Assert.Throws<InvalidDataException>(() => _subjectFactory.Persist());
        }

        #endregion

        #region Business Logic Test
        [Fact]
        public void Save_Should_Check_Duplicate_Name_Entry()
        {
            var subjectFactory =
                _subjectFactory.Create().WithNameAndRank(_name, 1).WithProperties(_shortName)
                    .Persist();
            var subjectFactory2 =
                _subjectFactory.Create().WithNameAndRank(_name, 1).WithProperties(_shortName + "fgfghfh");
            Assert.Throws<DuplicateEntryException>(() => subjectFactory2.Persist());
        }
        [Fact]
        public void Save_Should_Check_Duplicate_ShortName_Entry()
        {
            var subjectFactory =
               _subjectFactory.Create().WithNameAndRank(_name, 1).WithProperties(_shortName)
                   .Persist();
            var subjectFactory2 =
                _subjectFactory.Create().WithNameAndRank(_name + " DFGDD", 1).WithProperties(_shortName);
            Assert.Throws<DuplicateEntryException>(() => subjectFactory2.Persist());
        }

        #endregion
        
    }
}
