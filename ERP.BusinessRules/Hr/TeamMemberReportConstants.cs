﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.BusinessRules.Hr
{
    public class TeamMemberReportConstants
    {
        
        public const string Pin = "Pin";
        public const string NickName = "NickName";
        public const string FullName = "FullName";
        public const string Organization = "Organization";
        public const string Branch = "Branch";
        public const string Campus = "Campus";
        public const string Department = "Department";
        public const string Designation = "Designation";
        public const string EmploymentStatus = "Employment Status";
        public const string MentorNamePin = "Mentor Name and PIN";
        public const string JoiningDate = "Joining Date";
        public const string PermanentDate = "Permanent Date";
        public const string DiscontinuationDate = "Discontinuation Date";
        public const string OfficialContact = "Official Contact";
        public const string PersonalContact = "Personal Contact";
        public const string OfficialEmail = "Official Email";
        public const string PersonalEmail = "Personal Email";
        public const string OfficeShift = "Office Shift";
        public const string Gender = "Gender";
        public const string Religion = "Religion";
        public const string BloodGroup = "Blood Group";
        public const string DateOfBirthCertificate = "Date Of Birth(certificate)";
        public const string DateOfBirthActual = "Date Of Birth(Actual)";
        public const string HomeDistrict = "Home District";
        public const string Upozilla = "Upozilla";
        public const string MaritalStatus = "Marital Status";
        public const string MarriageDate = "Marriage Date";

        public static List<string> GetTeamMemberSearchMemberListConstants()
        {
            return new List<string>
            {
                Pin
                , NickName
                , FullName
                , Organization
                , Branch
                , Campus
                , Department
                , Designation
                , EmploymentStatus
                , MentorNamePin
                , JoiningDate
                , PermanentDate
                , DiscontinuationDate
                , OfficialContact
                , PersonalContact
                , OfficialEmail
                , PersonalEmail
                , OfficeShift
                , Gender
                , Religion
                , BloodGroup
                , DateOfBirthCertificate
                , DateOfBirthActual
                , HomeDistrict
                , Upozilla
                , MaritalStatus
                , MarriageDate
            };
        }
    }



}
