﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Sms
{
    /// <summary>
    /// Habib
    /// </summary>
    public interface ISmsHistoryDao : IBaseDao<SmsHistory, long>
    {
        #region Single Instances Loading Function
        SmsHistory LastHistory(long studentId, long smsSettingsId, long? programId = null);  
        #endregion
        #region List Loading Function
        IList<SmsHistory> LoadSmsHistory(int status, int top, int maxSendingTrySms); 
        #endregion

        IList<SmsHistory> LoadSmsHistory(int start, int length, string orderBy, string orderDir, DateTime dateFrom, DateTime dateTo, long? programId, long? smsTypeId, string mobileNumber, string sms, int? status, string userEmail);

        int CountSmsHistory(DateTime dateFrom, DateTime dateTo, long? programId, long? smsTypeId, string mobileNumber, string sms, int? status, string userEmail);
    }
    public class SmsHistoryDao:BaseDao<SmsHistory, long>,ISmsHistoryDao
    {
        #region Single Instances Loading Function

        public SmsHistory LastHistory(long studentId, long smsSettingsId, long? programId = null) 
        {
            ICriteria criteria = Session.CreateCriteria<SmsHistory>();
            criteria.Add(Restrictions.Eq("Student.Id", studentId));
            criteria.Add(Restrictions.Eq("SmsSettings.Id", smsSettingsId));
            //criteria.Add(Restrictions.Not(Restrictions.Eq("ErrorCode", "1900")));
            if (programId!=null)
            {
                criteria.Add(Restrictions.Eq("Program.Id", programId));
            }
            //criteria.Add(Restrictions.Eq("ErrorCode", "1900"));
            var disjunction = Restrictions.Disjunction();
            disjunction.Add(Restrictions.Eq("ErrorCode", "1900"));
            disjunction.Add(Restrictions.Eq("Status", SmsHistory.EntityStatus.Pending));
            criteria.Add(disjunction);
            criteria.AddOrder(Order.Desc("CreationDate"));
            return criteria.SetMaxResults(1).UniqueResult<SmsHistory>();
        }

        #endregion

        #region List Loading Function
        public IList<SmsHistory> LoadSmsHistory(int status, int top, int maxSendingTrySms) 
        {
            ICriteria criteria = Session.CreateCriteria<SmsHistory>();
            criteria.Add(Restrictions.Eq("Status", SmsHistory.EntityStatus.Pending));
            criteria.Add(Restrictions.Lt("SendingTryCount", maxSendingTrySms));
            criteria.AddOrder(Order.Asc("Priority"));
            criteria.AddOrder(Order.Asc("CreationDate"));
            criteria.SetFirstResult(0);
            criteria.SetMaxResults(top);
            return criteria.List<SmsHistory>();
        }

        public IList<SmsHistory> LoadSmsHistory(int start, int length, string orderBy, string orderDir, DateTime dateFrom, DateTime dateTo, long? programId, long? smsTypeId, string mobileNumber, string sms, int? status, string userEmail)
        {
            var criteria = GetSmsHistoryQuery(dateFrom, dateTo, programId, smsTypeId, mobileNumber, sms, status, userEmail, false, orderBy, orderDir);
            if(length>0)
            return criteria.SetFirstResult(start).SetMaxResults(length).List<SmsHistory>();
            else
            return criteria.List<SmsHistory>();
        }

        public int CountSmsHistory(DateTime dateFrom, DateTime dateTo, long? programId, long? smsTypeId, string mobileNumber, string sms, int? status, string userEmail)
        {
            var criteria = GetSmsHistoryQuery(dateFrom, dateTo, programId, smsTypeId, mobileNumber, sms, status, userEmail, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        private ICriteria GetSmsHistoryQuery(DateTime dateFrom, DateTime dateTo, long? programId, long? smsTypeId, string mobileNumber, string sms, int? status, string userEmail, bool countQuery = false, string orderBy = "CreationDate", string orderDir = "asc")
        {
            var criteria = Session.CreateCriteria<SmsHistory>();
            var user = "imran";
            //criteria.CreateAlias("SmsSettings", "smsSetting");
            //criteria.CreateAlias("smsSetting.SmsType", "smsType");
            if (programId != null)
            {
                if (programId != 0)
                    criteria.Add(Restrictions.Eq("Program.Id", programId));
            }
            if (smsTypeId != null)
            {
                if (smsTypeId != 0)
                    criteria.Add(Restrictions.Eq("Type", Convert.ToInt32(smsTypeId)));
            }

            if (!String.IsNullOrWhiteSpace(mobileNumber))
                criteria.Add(Restrictions.Like("ReceiverNumber", mobileNumber, MatchMode.Anywhere));

            if (!String.IsNullOrWhiteSpace(sms))
                criteria.Add(Restrictions.Like("Sms", sms, MatchMode.Anywhere));

            if (!String.IsNullOrWhiteSpace(userEmail))
            {
                var userIdList = Session.QueryOver<AspNetUser>()
                                .WhereRestrictionOn(u => u.Email).IsLike(userEmail,MatchMode.Anywhere)
                                .Select(u => u.Id)
                                .List<long>();
                //if (userIdList.Any())
                    criteria.Add(Restrictions.In("CreateBy", userIdList.ToList()));
            }
            
            if (status != null)
            {
                if (status != 0)
                    criteria.Add(Restrictions.Eq("Status", status));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }


            criteria.Add(Restrictions.Ge("ModificationDate", dateFrom));
            criteria.Add(Restrictions.Le("ModificationDate", dateTo));
            return criteria;
        }


        #endregion
    }
}
