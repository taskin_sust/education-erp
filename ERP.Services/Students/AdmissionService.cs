﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Students;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface IAdmissionService : IBaseService
    {
        #region Operational Function
        bool IsSuccessfullyProgramRegistered(Student alreadyRegisteredStudentObj, StudentProgram studentProgramObj, List<StudentCourseDetail> studentCourseDetailList, long studentVisitedId = 0);
        bool IsSuccessfullyRegisteredForNewAdmission(Student student, StudentProgram studentProgramObj, List<StudentCourseDetail> studentCourseDetailList, StudentPayment studentPayment);
        Student RegisteredStudent(Student student);
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion
    }

    public class AdmissionService : BaseService, IAdmissionService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentDao _studentDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private readonly IStudentCourseDetailDao _studentCourseDetailDao;
        private readonly IStudentPaymentDao _studentPaymentDao;
        private readonly IStudentVisitedDao _studentVisitedDao;
        public AdmissionService(ISession session)
        {
            Session = session;
            _studentDao = new StudentDao { Session = Session };
            _studentProgramDao = new StudentProgramDao { Session = Session };
            _studentCourseDetailDao = new StudentCourseDetailDao { Session = Session };
            _studentPaymentDao = new StudentPaymentDao { Session = Session };
            _studentVisitedDao = new StudentVisitedDao { Session = Session };
        }
        #endregion

        #region Operational Function
        public bool IsSuccessfullyRegisteredForNewAdmission(Student student, StudentProgram studentProgram, List<StudentCourseDetail> studentCourseDetailList, StudentPayment studentPayment)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    try
                    {
                        bool isDuplicateStudent =
                            _studentDao.HasDuplicateByNickNameAndMobileNumberAndProgramAndSession(student.NickName,
                                student.Mobile, studentProgram.Program.Id, studentProgram.Batch.Session.Id);
                        if (isDuplicateStudent)
                        {
                            throw new Exception(
                                "Duplicate Student Found !! Student already registered by using this Nick name and mobile number in this program and session");
                        }
                        var regNumber = GetRegistrationNumber();
                        var prNum = GetPrNumber(studentProgram.Batch.Branch.Code, studentProgram.Batch.Session.Code,
                            studentProgram.Program.Code);

                        if (regNumber != "" && prNum != "")
                        {
                            student.RegistrationNo = regNumber;
                            _studentDao.Save(student);

                            studentProgram.Student = student;
                            studentProgram.PrnNo = prNum;
                            _studentProgramDao.Save(studentProgram);
                            foreach (var studentCourseDetail in studentCourseDetailList)
                            {
                                try
                                {
                                    _studentCourseDetailDao.Save(studentCourseDetail);
                                }
                                catch (Exception)
                                {
                                    trans.Rollback();
                                    return false;
                                }
                            }
                            //var batch = stdProgram.Batch;
                            var mrNo = GetMrNumber(studentProgram.Batch.Branch.Code, studentProgram.Batch.Session.Code, studentPayment.PaymentType);
                            if (!String.IsNullOrEmpty(mrNo))
                            {
                                studentPayment.ReceiptNo = mrNo;
                                //studentPayment.StudentProgram = stdProgram;
                                //studentPayment.Batch = batch;
                            }
                            if (studentPayment.OnlineTransactionses.Count > 0)
                            {
                                foreach (var t in studentPayment.OnlineTransactionses)
                                {
                                    t.StudentPayment = studentPayment;
                                }
                            }
                            studentPayment.StudentProgram = studentProgram;
                            studentPayment.Batch = studentProgram.Batch;
                            _studentPaymentDao.Save(studentPayment);
                            trans.Commit();
                            Session.Flush();
                            return true;
                        }
                        return false;
                    }
                    catch (Exception ex)
                    {
                        if (trans != null && trans.IsActive)
                            trans.Rollback();
                        _logger.Error(ex);
                        throw ex;
                        //return false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        public bool IsSuccessfullyProgramRegistered(Student alreadyRegisteredStudentObj, StudentProgram studentProgramObj, List<StudentCourseDetail> studentCourseDetailList, long studentVisitedId = 0)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    try
                    {
                        bool isDuplicateStudent =
                               _studentDao.HasDuplicateByNickNameAndMobileNumberAndProgramAndSession(studentProgramObj.Student.NickName,
                                   studentProgramObj.Student.Mobile, studentProgramObj.Program.Id, studentProgramObj.Batch.Session.Id);
                        if (isDuplicateStudent)
                        {
                            throw new Exception(
                                "Duplicate Student Found !! Student already registered by using this Nick name and mobile number in this program and session");
                        }
                        var prNum = GetPrNumber(studentProgramObj.Batch.Branch.Code, studentProgramObj.Batch.Session.Code, studentProgramObj.Program.Code);
                        if (alreadyRegisteredStudentObj.RegistrationNo != "" && prNum != "")
                        {
                            //student.RollNo = regNumber;
                            _studentDao.Update(alreadyRegisteredStudentObj);
                            studentProgramObj.Student = alreadyRegisteredStudentObj;
                            studentProgramObj.PrnNo = prNum;
                            _studentProgramDao.Save(studentProgramObj);

                            foreach (var studentCourseDetail in studentCourseDetailList)
                            {
                                try
                                {
                                    _studentCourseDetailDao.Save(studentCourseDetail);
                                }
                                catch (Exception)
                                {
                                    trans.Rollback();
                                    return false;
                                }
                            }

                            if (studentVisitedId > 0)
                            {
                                var studentVisit = _studentVisitedDao.LoadById(studentVisitedId);
                                if (studentVisit != null)
                                {
                                    studentVisit.IsAdmitted = true;
                                    studentVisit.AdmittedStudentProgram = studentProgramObj;
                                    _studentVisitedDao.Update(studentVisit);
                                }
                            }
                            trans.Commit();
                            return true;
                        }
                        return false;
                    }
                    catch (Exception ex)
                    {
                        if (trans != null && trans.IsActive)
                            trans.Rollback();
                        _logger.Error(ex);
                        throw ex;
                        //return false;
                    }
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw ex;
                //return false;
            }
        }
        public Student RegisteredStudent(Student student)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {
                    //try
                    //{
                    //bool isDuplicateStudent = _studentDao.HasDuplicateByNickNameAndMobileNumber(student.NickName, student.Mobile);
                    //if (isDuplicateStudent)
                    //{
                    //    throw new ServiceException("Duplicate Student Found !! Student already registered by using this Nick name and mobile number.");
                    //}
                    var regNumber = GetRegistrationNumber();
                    if (!string.IsNullOrEmpty(regNumber))
                    {
                        student.RegistrationNo = regNumber;
                        _studentDao.Save(student);
                        trans.Commit();
                    }
                    else
                    {
                        throw new ServiceException("Student Save Failed. Please try again.");
                    }
                    //}
                    //catch (Exception e)
                    //{
                    //    trans.Rollback();
                    //    throw e;
                    //}
                }

                return student;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function


        #endregion

        #region Others Function
        private string GetMrNumber(string brCode, string sCode, int paymentType)
        {
            try
            {
                if (!String.IsNullOrEmpty(brCode) && !String.IsNullOrEmpty(sCode))
                {
                    var firstMrCode = "000001";
                    var lastMrCode = brCode + sCode;
                    StudentPayment studentPaymentObj = _studentPaymentDao.LoadBybrCodeAndsCode(lastMrCode, paymentType);
                    if (studentPaymentObj == null)
                    {
                        return lastMrCode + firstMrCode;
                    }

                    var preMrNo = studentPaymentObj.ReceiptNo;
                    var preMrNoLastPart = preMrNo.Substring(4);

                    return lastMrCode + (Convert.ToInt64(preMrNoLastPart) + 1).ToString().PadLeft(6, '0');
                }
                return "";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "";
                //throw;
            }

        }
        private string GetPrNumber(string bCode, string sCode, string pCode)
        {
            try
            {
                if (!String.IsNullOrEmpty(bCode) && !String.IsNullOrEmpty(sCode) && !String.IsNullOrEmpty(pCode))
                {
                    var firstPrCode = "00001";
                    var lastPrCode = bCode + sCode + pCode;
                    StudentProgram studentProgram = _studentProgramDao.GetLastStudentProgram(lastPrCode);
                    if (studentProgram == null)
                    {
                        return lastPrCode + firstPrCode;
                    }
                    var prePrNo = studentProgram.PrnNo;
                    var prePrNoLastPart = prePrNo.Substring(6);

                    long newFirstCode = Convert.ToInt64(prePrNoLastPart.Trim()) + 1;

                    string finalCode = lastPrCode + newFirstCode.ToString().PadLeft(5, '0');
                    return finalCode;
                }
                return "";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "";
            }

        }
        public string GetRegistrationNumber()
        {
            try
            {
                var studentObj = _studentDao.GetLastRegisteredStudent();
                if (studentObj == null) return "1000001";
                return (Convert.ToInt64(studentObj.RegistrationNo.Trim()) + 1).ToString();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "";
            }
        }
        #endregion

        #region Helper Function

        #endregion
    }
}