﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{
    public class AcademicInfoFactory : ObjectFactoryBase<AcademicInfo>
    {
        private readonly IHrBoardService _hrBoardService;
        private readonly IAcademicInfoService _academicInfoService;

        private CommonFactory _commonFactory;
        private InstituteFactory _instituteFactory;
        private TeamMemberFactory _teamMemberFactory;

        public AcademicInfoFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _hrBoardService = new HrBoardService(session);
            _academicInfoService = new AcademicInfoService(session);
            _commonFactory = new CommonFactory(this, session);
            _instituteFactory = new InstituteFactory(this, session);
            _teamMemberFactory = new TeamMemberFactory(this, session);
        }

        #region Create

        public AcademicInfoFactory Create()
        {
            Object = new AcademicInfo()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Guid.NewGuid().ToString(),
                DegreeTitle = "Degree",
                Major = "Science",
                ResultType = 1,
                Year = 2000,
                CurrentAcademicStatus = null,
                GradeScale = 5,
                Grade = 5
            };
            SingleObjectList.Add(Object);
            return this;
        }

        public AcademicInfoFactory CreateWith(long id, string businessId, string name,
           string degreeTitle, string major, int? resultType, int? gradeScale, int? grade,
            decimal? result, int? year, int currentAcademicStatus)
        {
            Object = CreateObject(id, name, degreeTitle, major, resultType, gradeScale, grade, result, year, currentAcademicStatus);
            SingleObjectList.Add(Object);
            return this;
        }

        private AcademicInfo CreateObject(long id, string name, string degreeTitle, string major, int? resultType, int? gradeScale, int? grade, decimal? result, int? year, int currentAcademicStatus)
        {
            var obj = new AcademicInfo()
            {
                Id = id,
                BusinessId = GetPcUserName(),
                Name = name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Status = AcademicInfo.EntityStatus.Active,
                DegreeTitle = degreeTitle,
                Major = major,
                ResultType = resultType,
                GradeScale = gradeScale,
                Grade = grade,
                Result = result,
                Year = year,
                CurrentAcademicStatus = currentAcademicStatus
            };
            return obj;
        }

        public AcademicInfoFactory WithExam()
        {
            var examObj = _commonFactory.WithAcademicExam();
            Object.Exam = examObj;
            return this;
        }
        public AcademicInfoFactory WithExam(AcademicExam examObj)
        {
            Object.Exam = examObj;
            return this;
        }
        public AcademicInfoFactory WithBoard()
        {
            // this.Create();
            var boardObj = _commonFactory.WithBoard();
            Object.Board = boardObj;
            return this;
        }

        public AcademicInfoFactory WithBoard(Board boardObj)
        {
            Object.Board = boardObj;
            return this;
        }
        public AcademicInfoFactory WithInstitute()
        {
            _instituteFactory.Create().Persist();
            Object.Institute = _instituteFactory.Object;
            return this;
        }
        public AcademicInfoFactory WithInstitute(Institute institute)
        {
            Object.Institute = institute;
            return this;
        }
        public AcademicInfoFactory WithTeamMember()
        {
            _teamMemberFactory.Create().Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }
        public AcademicInfoFactory WithTeamMember(TeamMember tm)
        {
            Object.TeamMember = tm;
            return this;
        }
        
        #endregion

        #region other

        #endregion

        #region CleanUp

        
        public AcademicInfoFactory CleanUp()
        {
            DeleteAll();
            _teamMemberFactory.Cleanup();
            _instituteFactory.Cleanup();
            return this;
        }
        #endregion
    }
}
