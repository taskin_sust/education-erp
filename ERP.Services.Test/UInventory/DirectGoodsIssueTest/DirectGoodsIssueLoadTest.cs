﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessRules.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.DirectGoodsIssueTest
{

    [Trait("Area", "UInventory")]
    [Trait("Service", "DirectGoodsIssue")]

    public class DirectGoodsIssueLoadTest : DirectGoodsIssueBaseTest
    {

        #region DiectGoodsIssueCount

        [Fact]
        public void GoodsIssue_Count_Should_Be_Greater_Than_Zero()
        {
            _goodsIssueFactory.CreateMore(5).Persist();
            var goodsIssue = _goodsIssueFactory.GetLastCreatedObjectList()[0];
            var branch = goodsIssue.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = goodsIssue.GoodsIssueDetails[0].Program;
            var issueDate = goodsIssue.CreationDate;
            var goodsIssueNo = goodsIssue.GoodsIssueNo;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            var giCount = _goodsIssueService.GetGoodsIssueCount(userMenu, organizationId, branchId, "", issueDate, goodsIssueNo,
                GoodsIssue.EntityStatus.Active);
            Assert.True(giCount >= 0);

        }

        #endregion

        #region DirectGoodsIssueLoadActive

        [Fact]
        public void Load_GoodsIssue_Should_Return_List()
        {
            _goodsIssueFactory.CreateMore(5).Persist();
            var goodsIssue = _goodsIssueFactory.GetLastCreatedObjectList()[0];
            var branch = goodsIssue.Branch;
            var organizationId = branch.Organization.Id;
            var branchId = branch.Id;
            var program = goodsIssue.GoodsIssueDetails[0].Program;
            var issueDate = goodsIssue.CreationDate;
            var goodsIssueNo = goodsIssue.GoodsIssueNo;
            var userMenu = BuildUserMenu(branch.Organization, program, branch);
            var gsList = _goodsIssueService.LoadGoodsIssuedList(0, 10, "", "", userMenu, organizationId, branchId, "",
                issueDate, goodsIssueNo, GoodsIssue.EntityStatus.Active);
            Assert.True(gsList.Count >= 0);
        }

        #endregion

        #region DirectGoodsIssueLoadById

        [Fact]
        public void LoadById_Null_Check()
        {
            Assert.Throws<InvalidDataException>(() => _goodsIssueService.GetGoodsIssue(-1));
        }

        [Fact]
        public void GetGoodsIssue_NotNull_Check()
        {
            _goodsIssueFactory.Create().WithBranch().WithGoodsIssueDetails(_goodsIssueFactory.Object.Branch.Organization).Persist();
            var goodsIssue = _goodsIssueService.GetGoodsIssue(_goodsIssueFactory.Object.Id);
            Assert.NotNull(goodsIssue);
            Assert.True(goodsIssue.Id > 0);

        }

        [Fact]
        public void Should_Return_Current_GoodsIssueNo_Successfully()
        {
            branchFactory.Create().WithOrganization().WithCode(new Random().Next(99).ToString());
            var recCode = _goodsIssueService.GetGoodsIssueNo(branchFactory.Object);
            Assert.StartsWith(branchFactory.Object.Organization.ShortName.Substring(0, 3) + branchFactory.Object.Code + "GI" + "00000" + 1, recCode);
        }

        [Fact]
        public void Should_Return_Invalid_IssueNo_Length_Exception()
        {
            Assert.Throws<InvalidDataException>(() => _goodsIssueService.GetGoodsIssue("1212121212"));
        }


        #endregion

        #region Count Test

        [Fact]
        public void Should_Return_Count_More_than_One_DirectGoodsIssue_SuccessFully()
        {
            organizationFactory.Create().Persist();
            branchFactory.Create().WithOrganization(organizationFactory.Object).Persist();
            _itemFactory.CreateWith(0, _name, (int)ItemType.ProgramItem, (int)ItemUnit.Feet, (int)CostBearer.Corporate).WithItemGroup().WithOrganization(organizationFactory.Object).WithSpecifationTemplate().Persist();
            _programSessionItemFactory.Create().WithItem(_itemFactory.Object).WithProgramAndSession(organizationFactory.Object).Persist();
            var userMenus = BuildUserMenu(organizationFactory.Object, commonHelper.ConvertIdToList(_programSessionItemFactory.Object.Program), branchFactory.Object);

            _goodsIssueFactory.Create().WithBranch(branchFactory.Object);
            _goodsIssueDetailsFactory.Create()
                .WithProgramAndSession(_programSessionItemFactory.Object.Program,
                    _programSessionItemFactory.Object.Session)
                .WithItem(_itemFactory.Object)
                .WithPurpose(Purpose.ProgramPurpose)
                .WithGoodsIssue(_goodsIssueFactory.Object);
            _goodsIssueFactory.Object.GoodsIssueDetails.Add(_goodsIssueDetailsFactory.Object);
            _goodsIssueService.SaveOrUpdate(_goodsIssueFactory.Object);

            int count = _goodsIssueService.GetGoodsIssueCount(userMenus, organizationFactory.Object.Id,
                branchFactory.Object.Id, _itemFactory.Object.Name, _goodsIssueFactory.Object.CreationDate,
                _goodsIssueFactory.Object.GoodsIssueNo);
            Assert.True(count > 0);

        }
        

        #endregion

    }
}