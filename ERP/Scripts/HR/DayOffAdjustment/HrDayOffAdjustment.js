﻿$(document).ready(function () {
    $("#hrDayOffAdjustmentTableList").tableHeadFixer({ "left": 3 });
    $(document).on("focus", "input.datepicker", function () {
        var today = new Date();
        var initialDate = today;//today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();
        var startDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + "01";
        var endDate = new Date(today.getFullYear(), (today.getMonth() + 1), 0);
        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: initialDate,
            startDate: startDate,
            endDate: endDate,
            startView: 2,
            minView: 2,
            maxView: 4
        }).on("changeDate", function (e) {
            var missing = $(this).val() === "";
            $(this).toggleClass('highlight', missing);

            $(this).parent().parent().find("input.pin").val("");
            $(this).parent().parent().find("input.name").val("");
            $(this).parent().parent().find("input.dept").val("");
            $(this).parent().parent().find("input.org").val("");
            $(this).parent().parent().find("input.br").val("");
            $(this).parent().parent().find("input.cam").val("");
            $(this).parent().parent().find("input.designation").val("");
            //$(this).parent().parent().find("input.insteadOfDate").val("");
        });
    });

    $(document).on("focus", "input.historyDate", function () {
        var today = new Date();
        var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: today,
            //startDate: startDate,
            endDate: lastDay,
            startView: 2,
            minView: 2,
            maxView: 4
        });
    });

    $(document).on("input propertychange", ".reasonForAdjustment,.pin", function (event) {
        var missing = $(this).val() === "";
        $(this).toggleClass('highlight', missing);
    });

    $('.hrDayOffHistoryTab').on('click', function () {
        $('.hrClickHide').hide();
    });

    $('.hrDayOffEntryListTab').on('click', function () {
        $('.hrClickHide').show();
    });

    $(document).on("click", ".hrDayOffHistoryTab, #showFormRecentHistory", function () {
        var dateFrom = $("#dateFrom").val();
        var dateTo = $("#dateTo").val();
        if (dateFrom > dateTo) {

            $.fn.customMessage({
                displayMessage: "From Datetime must be less than or equal to To Datetime",
                displayMessageType: "error",
            });
        }
        else {
            dataTableRender();
        }
    });

    $(document).on("click", "#submitHrDayOffAdjustmentForm", function (event) {
        var checkValidRow;
        var e = 0;
        var hrDayOffRowArray = [];

        $("#hrDayOffAdjustmentTableList tbody tr").each(function () {

            checkValidRow = checkIndividualRowValidity($(this));
            if (checkValidRow > 0) {
                e = e + 1;
            } else {
                var hrDayOffViewModel = {};
                hrDayOffViewModel["MemberId"] = $(this).find(".employeeId").val();
                hrDayOffViewModel["DayOffDate"] = $(this).find(".dayOffDate").val();
                hrDayOffViewModel["Pin"] = $(this).find(".pin").val();
                hrDayOffViewModel["InsteadOfDate"] = $(this).find(".insteadOfDate").val();
                hrDayOffViewModel["ReasonForAdjustment"] = $(this).find(".reasonForAdjustment").val();

                var checkObject = containsObject(hrDayOffViewModel, hrDayOffRowArray);
                if (checkObject == true) {
                    e = e + 1;
                    notValidRow($(this));
                } else {
                    hrDayOffRowArray.push(hrDayOffViewModel);
                }
            }
        });

        if (e == 0) {
            bootbox.confirm("<h3>Are you sure you want to approve all <span class='confirm-message'></span>?</h3>", function (result) {
                if (result) {
                    var dataObject = JSON.stringify(hrDayOffRowArray);
                    $.ajax({
                        type: "post",
                        url: $("body").attr("data-project-root") + "Hr/DayOffAdjustment/HrDayOffAdjustmentEntry",
                        data: { dataObject: dataObject },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (results) {
                            if (results.IsSuccess) {
                                $.fn.customMessage({
                                    displayMessage: results.returnSuccess,
                                    displayMessageType: "success",
                                });
                                $("#hrDayOffAdjustmentTableList tbody.hrDayOffAdjustmentTableNew").html("");
                             //   window.setTimeout(function () { location.reload(); }, 5000);
                            } else {
                                $.fn.customMessage({
                                    displayMessage: results.returnSuccess,
                                    displayMessageType: "error",
                                });
                            }
                            $.unblockUI();
                        },
                        complete: function () {

                            $.unblockUI();
                        },
                        error: function (results) {
                            $.unblockUI();
                            console.log("Failed -----");
                        }
                    });
                }
            }).css({ 'margin-top': (($(window).height() / 4)) });
        } else {

        }
    });

    var itemCount = $(".totalCount").val(); 
    $(document).on("click", ".dynamicRowGenerate", function (event) {
        var html = "";
        //console.log(event);
        // dynamically create rows in the table
        html = "<tr id='tr" + itemCount + "' >" +
            "<td><input type='text' name='dayOffDate" + itemCount + "' id='dayOffDate_" + itemCount + "'  value='' class='dayOffDate datepicker form-control text-box single-line' readonly='readonly' /></td>" +
            "<td><input type='hidden' name='employeeId" + itemCount + "' id='employeeId_" + itemCount + "' value='' class='employeeId'/>" +
            "<input type='text' name='pin" + itemCount + "' id='pin_" + itemCount + "'  value='' class='pin autoComplete memberPin form-control text-box single-line' /></td>" +
            "<td><input type='text' name='name" + itemCount + "' id='name_" + itemCount + "'  value='' class='name form-control text-box single-line' readonly='readonly' /></td>" +
            "<td><input type='text' name='dept" + itemCount + "' id='dept_" + itemCount + "'  value='' class='dept form-control text-box single-line' readonly='readonly' /></td>" +
            "<td><input type='text' name='org" + itemCount + "' id='org_" + itemCount + "'  value='' class='org form-control text-box single-line' readonly='readonly' /></td>" +
            "<td><input type='text' name='br" + itemCount + "' id='br_" + itemCount + "'  value='' class='br form-control text-box single-line' readonly='readonly' /></td>" +
            "<td><input type='text' name='cam" + itemCount + "' id='cam_" + itemCount + "'  value='' class='cam form-control text-box single-line' readonly='readonly' /></td>" +
            "<td><input type='text' name='designation" + itemCount + "' id='designation_" + itemCount + "'  value='' class='designation form-control text-box single-line' readonly='readonly' /></td>" +
            "<td><select name='insteadOfDate" + itemCount + "' id='insteadOfDate_" + itemCount + "' value='' class='insteadOfDate form-control'><option value=''>Select Date</option></select> </td>" +
            "<td><input type='text' name='reasonforad" + itemCount + "' id='reasonforad_" + itemCount + "' class='form-control text-box single-line reasonForAdjustment' placeholder='Reason for Adjustment'> </td>" +
            "<td><span  id='" + itemCount + "'  class='btn btn-primary' style='cursor: pointer'>-</span></td> " +
            "</tr>";
        $(".hrDayOffAdjustmentTableNew").append(html);
        $("#hrDayOffAdjustmentTableList").tableHeadFixer({ "left": 3, "z-index": 2 });
        $("#" + itemCount).click(function () {
            var buttonId = $(this).attr("id");
            $("#tr" + buttonId).remove();
        });
        itemCount++;
    });

    $(document).on("input propertychange", ".memberPin", function (event) {
        var $this = $(this);
        var memberPinPrevious = parseInt($(this).val().trim());
        var tr = $(this).parent().parent();
        var date = tr.find('input.datepicker').val();
        setTimeout(function () {
            var memberPin = parseInt($this.val().trim());
            if (memberPin !== "" && memberPin === memberPinPrevious) {
                if (memberPin > 0) {
                    tr.find('input.employeeId').val(0);
                    tr.find('input.name').val("");
                    tr.find('input.dept').val("");
                    tr.find('input.org').val("");
                    tr.find('input.br').val("");
                    tr.find('input.cam').val("");
                    tr.find('input.designation').val("");
                    tr.find('.insteadOfDate').empty();
                    $.ajax({
                        //url: "/Hr/LeaveApplication/GetTeamMemberInfo",
                        url: $("body").attr("data-project-root") + "Hr/CommonAjaxHr/GetTeamMemberInformation",
                        type: "POST",
                        data: { pin: memberPin, isHr: true, isOrganization: true, isBranch: true, isCampus: true, isDepartment: true, isDesignation: true, searchingDate: date, isInsteadDate: true },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (data) {
                            $.unblockUI();
                            if (data.IsSuccess) {
                                tr.find('input.employeeId').val(data.teamMemberId);
                                tr.find('input.name').val(data.teamMemberName);
                                tr.find('input.dept').val(data.departmentName);
                                tr.find('input.org').val(data.organizationName);
                                tr.find('input.br').val(data.branchName);
                                tr.find('input.cam').val(data.campusName);
                                tr.find('input.designation').val(data.designationName);
                                tr.find('select.insteadOfDate').empty("");
                                tr.find('select.insteadOfDate').append($('<option>').text("Select Date").attr('value', ""));
                                $.each(data.insteadDaylist, function (i, v) {
                                    tr.find('select.insteadOfDate').append($('<option>').text(v.Text).attr('value', v.Text));
                                });

                                //$this.prev('input.employeeId').val(result.returnMemberId);
                                //$this.parent().next().find('input.name').val(result.returnMemberName);
                                //$this.parent().parent().find('input.dept').val(result.memDept);
                                //$this.parent().parent().find('input.org').val(result.memOrg);
                                //$this.parent().parent().find('input.br').val(result.memBr);
                                //$this.parent().parent().find('input.cam').val(result.memCam);
                                //$this.parent().parent().find('input.designation').val(result.memDesignation);
                                //$.each(result.insdteadOfDateList, function (i, v) {
                                //    $this.parent().parent().find('select.insteadOfDate').append($('<option>').text(v.Text).attr('value', v.Text));
                                //});

                            } else {
                                tr.find('input.employeeId').val(0);
                                tr.find('input.name').val("");
                                tr.find('input.dept').val("");
                                tr.find('input.org').val("");
                                tr.find('input.br').val("");
                                tr.find('input.cam').val("");
                                tr.find('input.designation').val("");
                                tr.find('.insteadOfDate').empty();
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (data) {
                            $.unblockUI();
                            bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                        }
                    });
                }
            }
        }, 1000);
    });

    $(document).on('change', '#Organization', function () {
        $('#Branch').empty();
        $('#Branch').append("<option value=''>Select Branch </option>");
        $('#Campus').empty();
        $('#Campus').append("<option value=''>Select Campus</option>");
        $('#Department').empty();
        $('#Department').append("<option value=''>Select Department</option>");

        var organizationId = $('#Organization').val();

        if (organizationId.length > 0) {
            $.ajax({
                type: "POST",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                data: { organizationIds: organizationId, isAuthorized: true, isProgramBranSession: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnBranchList, function (i, v) {
                            $('#Branch').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.ajax({
                        type: "POST",
                        url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadDepartment",
                        data: { organizationIds: organizationId },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (response) {
                            $.unblockUI();
                            if (response.IsSuccess) {
                                $.each(response.returndepartmentList, function (i, v) {
                                    $('#Department').append($('<option>').text(v.Text).attr('value', v.Value));
                                });
                            } else {
                                $.fn.customMessage({
                                    displayMessage: response.Message,
                                    displayMessageType: "error",
                                });
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (data) {
                            $.fn.customMessage({
                                displayMessage: ajaxError,
                                displayMessageType: "error",
                                displayTime: 5000
                            });
                        }
                    });
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
    });

    $(document).on('change', '#Branch', function () {

        $('#Campus').empty();
        $('#Campus').append("<option value=''>Select Campus</option>");
        var organizationId = $('#Organization').val();
        var branchId = $('#Branch').val();

        if (branchId.length > 0) {
            $.ajax({
                type: "POST",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                data: { organizationIds: organizationId, isAuthorized : true, branchIds: branchId, isProgramBranSession: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnCampusList, function (i, v) {
                            $('#Campus').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
    });

});

function dataTableRender() {

    var displayNo = 100;
    if ($('#DisplayNo').val() != "" && /^\d+$/.test($('#DisplayNo').val())) {
        displayNo = parseInt($('#DisplayNo').val());
    } else {
        $('#DisplayNo').val(displayNo);
    }
    $("#hrDayOffAdjustmentRecentHistory").dataTable({
        destroy: true,
        "processing": true,
        searching: false,
        serverSide: true,
        "scrollX": true,
        "bLengthChange": false,
        "iDisplayLength": displayNo,
        "bSort": false,
        //stateSave: false,
        "scrollCollapse": true,
        "scrollY": "60vh",
        "aoColumnDefs": [{ "bSortable": false, "aTargets": ['_all'] }],
        fixedColumns: {
            leftColumns: 3
        },
        order: [[0, "Asc"]],
        ajax: {
            url: $("body").attr("data-project-root") + "Hr/DayOffAdjustment/HrDayOffAdjustmentHistory",
            //url: '@Url.Action("HrDayOffAdjustmentHistory", "DayOffAdjustment")',
            type: 'POST',
            data: function (d) {
                d.org = $("#Organization").val();
                d.branch = $("#Branch").val();
                d.campus = $("#Campus").val();
                d.department = $("#Department").val();
                d.pin = $("#pin").val();
                d.dateFrom = $("#dateFrom").val();
                d.dateTo = $("#dateTo").val();
            },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (response) {
                ShowErrorMessage("data load error. Please try again.");
            }
        }

    });
}

function checkIndividualRowValidity(row) {
    var error = 0;
    var date = row.find(".dayOffDate").val();
    var pin = row.find(".pin").val();
    var insteadOfDate = row.find(".insteadOfDate").val();
    var reasonForAdjustment = row.find(".reasonForAdjustment").val();

    if (date == "") {
        error = error + 1;
        row.find(".dayOffDate").addClass("highlight");
    } else {
        row.find(".dayOffDate").removeClass("highlight");
    }
    if (pin == "") {
        error = error + 1;
        row.find(".pin").addClass("highlight");
    } else {
        row.find(".pin").removeClass("highlight");
    }
    if (insteadOfDate == "") {
        error = error + 1;
        row.find(".insteadOfDate").addClass("highlight");
    } else {
        row.find(".insteadOfDate").removeClass("highlight");
    }
    if (reasonForAdjustment == "") {
        error = error + 1;
        row.find(".reasonForAdjustment").addClass("highlight");
    } else {
        row.find(".reasonForAdjustment").removeClass("highlight");
    }
    if (error == 0) {
        row.removeClass("danger");
        row.addClass("info");
    } else {
        row.addClass("danger");
        row.removeClass("info");
    }
    return error;
}

function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (JSON.stringify(list[i]) === JSON.stringify(obj)) {
            return true;
        } else if (list[i].DayOffDate == obj.DayOffDate) {
            return true;
        } else if (list[i].InsteadOfDate == obj.InsteadOfDate) {
            return true;
        }
    }
    return false;
}

function notValidRow(row) {
    try {
        row.find(".dayOffDate").addClass("highlight");
        row.find(".reasonForAdjustment").addClass("highlight");
        row.find(".pin").addClass("highlight");
        row.find(".insteadOfDate").addClass("highlight");
        return 1;
    } catch (e) {
        return 0;
    }
}