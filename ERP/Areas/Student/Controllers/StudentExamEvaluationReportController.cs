﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FluentNHibernate.Utils;
using log4net;
using Microsoft.Ajax.Utilities;
using NHibernate.Hql.Ast.ANTLR;
using NHibernate.Mapping;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Teachers;
using UdvashERP.Services.Teachers;
using Array = System.Array;

namespace UdvashERP.Areas.Student.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Student")]
    public class StudentExamEvaluationReportController : Controller
    {
        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ILog _logger = LogManager.GetLogger("StudentArea");
        private readonly IProgramService _programService;
        private readonly IBranchService _branchService;
        private readonly IBatchService _batchService;
        private readonly ISessionService _sessionService;
        private readonly ICourseService _courseService;
        private readonly ICampusService _campusService;
        private readonly IExamsService _examsService;
        private List<UserMenu> _userMenu;
        private readonly IOrganizationService _organizationService;
        private readonly IStudentExamAttendanceService _studentExamAttendanceService;
        private readonly ICommonHelper _commonHelper;
        public StudentExamEvaluationReportController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _batchService = new BatchService(session);
                _sessionService = new SessionService(session);
                _courseService = new CourseService(session);
                _campusService = new CampusService(session);
                _examsService = new ExamsService(session);
                _organizationService = new OrganizationService(session);
                _studentExamAttendanceService = new StudentExamAttendanceService(session);
                _commonHelper=new CommonHelper();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
            }
        }
        #endregion

        #region ExamEvulation
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ExamEvaluation()
        {
            ViewBag.Program = new SelectList(new List<Program>(), "Id", "Name");
            ViewBag.Session = new SelectList(new List<Session>(), "Id", "Name");
            ViewBag.SelectedCourse = new SelectList(new List<Course>(), "Id", "Name");
            ViewBag.SelectedExam = new SelectList(new List<Exams>(), "Id", "Name");
            ViewBag.SelectedBranch = new SelectList(new List<Branch>(), "Id", "Name", SelectionType.SelelectAll);
            ViewBag.SelectedCampus = new SelectList(new List<Campus>(), "Id", "Name");
            ViewBag.SelectedBatchDays = new SelectList(new List<Batch>(), "Name", "Id");
            ViewBag.SelectedBatchTime = new SelectList(new List<Batch>(), "Name", "Id");
            ViewBag.SelectedBatch = new SelectList(new List<Batch>(), "Id", "Name");
            ViewBag.SelectedSubject = new SelectList(new List<Subject>(), "Id", "Name");
            ViewBag.SelectedSubject = new SelectList(new List<Subject>(), "Id", "Name");
            ViewBag.orgList = new SelectList(new List<Organization>(), "Id", "ShortName");
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Organization> organizations = _organizationService.LoadAuthorizedOrganization(_userMenu);
                ViewBag.orgList = new SelectList(organizations, "Id", "ShortName");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
            }

            return View();
        }

        [HttpPost]
        public ActionResult ExamEvaluation(StudentExamSurveyReportViewModel studentExamSurveyReportViewModel)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var batchName = "";
            var branchName = "";
            var batchDays = "";
            var campusName = "";
            var batchTime = "";
            var examName = "";
            var courseName = "";
            try
            {
                var program = _programService.GetProgram(studentExamSurveyReportViewModel.ProgramId);
                var session = _sessionService.LoadById(studentExamSurveyReportViewModel.SessionId);
                batchDays = studentExamSurveyReportViewModel.SelectedBatchDays[0] == "0" ? "ALL" : string.Join(" ; ", studentExamSurveyReportViewModel.SelectedBatchDays.ToArray());
                batchTime = studentExamSurveyReportViewModel.SelectedBatchTime[0] == "0" ? "ALL" : string.Join("; ", studentExamSurveyReportViewModel.SelectedBatchTime.ToArray());
                if (studentExamSurveyReportViewModel.SelectedBatch.Contains(SelectionType.SelelectAll))
                {
                    batchName = "ALL";
                }
                else
                {
                    IList<Batch> batches = _batchService.LoadBatch(studentExamSurveyReportViewModel.SelectedBatch);
                    var batch = batches.GroupBy(x => x.Name);
                    batchName = batch.Aggregate(batchName, (current, batcNam) => current + (batcNam.Key + ","));
                    batchName = batchName.Remove(batchName.Length - 1);
                }
                if (studentExamSurveyReportViewModel.SelectedBranch.Contains(SelectionType.SelelectAll))
                {
                    branchName = "ALL";
                }
                else
                {
                    IList<Branch> branches = _branchService.LoadBranch(studentExamSurveyReportViewModel.SelectedBranch);
                    var batc = branches.GroupBy(x => x.Name);
                    branchName = batc.Aggregate(branchName, (current, brName) => current + (brName.Key + ","));
                    branchName = branchName.Remove(branchName.Length - 1);
                }
                if (studentExamSurveyReportViewModel.SelectedCampus.Contains(SelectionType.SelelectAll))
                {
                    campusName = "ALL";
                }
                else
                {
                    IList<Campus> campuses = _campusService.LoadCampus(studentExamSurveyReportViewModel.SelectedCampus);
                    var batc = campuses.GroupBy(x => x.Name);
                    campusName = batc.Aggregate(campusName, (current, caName) => current + (caName.Key + ","));
                    campusName = campusName.Remove(campusName.Length - 1);
                }
                if (studentExamSurveyReportViewModel.SelectedExam.Contains(SelectionType.SelelectAll))
                {
                    examName = "ALL";
                }
                else
                {
                    IList<Exams> exams = _examsService.LoadExams(studentExamSurveyReportViewModel.SelectedExam.ToList());
                    var exam = exams.GroupBy(x => x.Name);
                    examName = exam.Aggregate(examName, (current, eName) => current + (eName.Key + ", "));
                    examName = examName.Trim(new[] { ',', ' ' });
                }
                if (studentExamSurveyReportViewModel.SelectedCourse.Contains(SelectionType.SelelectAll))
                {
                    courseName = "ALL";
                }
                else
                {
                    IList<Course> courses = _courseService.LoadCourse(studentExamSurveyReportViewModel.SelectedCourse);
                    var course = courses.GroupBy(x => x.Name);
                    courseName = course.Aggregate(courseName, (current, cName) => current + (cName.Key + ","));
                    courseName = courseName.Remove(courseName.Length - 1);
                }

               
                
                #region generate exam and batch ids
                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(studentExamSurveyReportViewModel.OrganizationId), _commonHelper.ConvertIdToList(studentExamSurveyReportViewModel.ProgramId), studentExamSurveyReportViewModel.SelectedBranch.ToList(), _commonHelper.ConvertIdToList(studentExamSurveyReportViewModel.SessionId), studentExamSurveyReportViewModel.SelectedCampus.ToList(), studentExamSurveyReportViewModel.SelectedBatchDays, studentExamSurveyReportViewModel.SelectedBatchTime, null, null).ToList();
                if (studentExamSurveyReportViewModel.SelectedBatch[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(studentExamSurveyReportViewModel.SelectedBatch)).ToList();
                }
                List<Branch> branchList = _branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(studentExamSurveyReportViewModel.OrganizationId), _commonHelper.ConvertIdToList(studentExamSurveyReportViewModel.ProgramId), _commonHelper.ConvertIdToList(studentExamSurveyReportViewModel.SessionId)).ToList();
                if (studentExamSurveyReportViewModel.SelectedBranch[0] != 0)
                {
                    branchList = branchList.Where(b => b.Id.In(studentExamSurveyReportViewModel.SelectedBranch)).ToList();
                }
                var courseList = _courseService.LoadCourse(studentExamSurveyReportViewModel.OrganizationId, studentExamSurveyReportViewModel.ProgramId, studentExamSurveyReportViewModel.SessionId, studentExamSurveyReportViewModel.SelectedCourse);

                List<Subject> subjectList = new List<Subject>();
                if (courseList != null)
                {
                    foreach (var singleCourse in courseList)
                    {
                        var courseSubject = singleCourse.CourseSubjects.ToList();

                        if (courseSubject != null && courseSubject.Count > 0)
                        {
                            foreach (var subject in courseSubject)
                            {
                                subjectList.Add(subject.Subject);
                            }
                        }
                    }
                }
                if (!studentExamSurveyReportViewModel.SelectedSubject.Contains(0))
                {
                    subjectList = subjectList.Where(x => x.Id.In(studentExamSurveyReportViewModel.SelectedSubject)).ToList();
                }
                IList<Exams> examList = _examsService.LoadExams(studentExamSurveyReportViewModel.ProgramId, studentExamSurveyReportViewModel.SessionId, studentExamSurveyReportViewModel.SelectedCourse, batchList, branchList, null, null, true, subjectList.Select(x => x.Id).Distinct().ToArray(), studentExamSurveyReportViewModel.FromTime, studentExamSurveyReportViewModel.ToTime);
                if ((Array.Exists(studentExamSurveyReportViewModel.SelectedExam, item => item == 0)))
                {

                    studentExamSurveyReportViewModel.ExamIds = examList.Select(x => x.Id).ToArray();
                }
                else
                {
                    studentExamSurveyReportViewModel.ExamIds = examList.Where(x => x.Id.In(studentExamSurveyReportViewModel.SelectedExam)).Select(x => x.Id).ToArray();
                } 
                #endregion

                studentExamSurveyReportViewModel.BatchIds = batchList.Select(x => x.Id).ToArray();


                var json = new JavaScriptSerializer().Serialize(studentExamSurveyReportViewModel);
                ViewData["studentExamSurveyReportViewModel"] = json;
                ViewBag.ProgramSession = program.Name + "-" + session.Name;
                ViewBag.Branch = branchName;
                ViewBag.BatchName = batchName;
                ViewBag.BatchDays = batchDays;
                ViewBag.CampusName = campusName;
                ViewBag.BatchTime = batchTime;
                ViewBag.Exam = examName;
                ViewBag.FromDate = studentExamSurveyReportViewModel.FromTime.ToString("dd-MMM-yyyy");
                ViewBag.ToDate = studentExamSurveyReportViewModel.ToTime.ToString("dd-MMM-yyyy");
                ViewBag.Course = courseName;
                ViewBag.informationViewList = studentExamSurveyReportViewModel.InformationViewList;
                
            }
            catch (Exception e)
            {
                var json = new JavaScriptSerializer().Serialize(studentExamSurveyReportViewModel);
                ViewData["studentExamSurveyReportViewModel"] = json;
                ViewBag.ProgramSession = "";
                ViewBag.Branch = branchName;
                ViewBag.BatchName = batchName;
                ViewBag.BatchDays = batchDays;
                ViewBag.CampusName = campusName;
                ViewBag.BatchTime = batchTime;
                ViewBag.Exam = examName;
                ViewBag.FromDate = studentExamSurveyReportViewModel.FromTime.ToString("dd-MMM-yyyy");
                ViewBag.ToDate = studentExamSurveyReportViewModel.ToTime.ToString("dd-MMM-yyyy");
                ViewBag.Course = courseName;
                ViewBag.informationViewList = studentExamSurveyReportViewModel.InformationViewList;
                _logger.Error(e.Message);
                
            }
            return View("ExamEvaluationReportList");

        }
        #endregion

        #region Exam Evulation DataTable Report
        [HttpPost]
        public ActionResult ExamEvaluaionReport(int draw, int start, int length, string studentExamSurveyReportViewModelObj)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                NameValueCollection nvc = Request.Form;
                var orderBy = "";
                var orderDir = "";
                #region OrderBy and Direction
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "SubjectName";
                            break;
                        case "1":
                            orderBy = "ExamName";
                            break;
                        case "2":
                            orderBy = "StandardPercentage";
                            break;
                        case "3":
                            orderBy = "HardPercentage";
                            break;
                        case "4":
                            orderBy = "EasyPercentage";
                            break;
                        case "5":
                            orderBy = "Total";
                            break;
                        default:
                            orderBy = "StandardPercentage";
                            break;
                    }
                }
                #endregion
                if (!String.IsNullOrEmpty(studentExamSurveyReportViewModelObj))
                {
                    var studentExamSurveyReportViewModel = new JavaScriptSerializer().Deserialize<StudentExamSurveyReportViewModel>( studentExamSurveyReportViewModelObj );
                    long[] examIds = studentExamSurveyReportViewModel.SelectedExam;
                    int recordsTotal = 0;

                    var studentExamEvaluationViewModels = GenerateExamEvaluationReport(studentExamSurveyReportViewModel, studentExamSurveyReportViewModel.ExamIds, studentExamSurveyReportViewModel.BatchIds.Distinct().ToArray(), start, length, orderBy, orderDir.ToUpper());
                    recordsTotal = _studentExamAttendanceService.GetExamEvaluationCountDto(start, length, orderBy, orderDir, studentExamSurveyReportViewModel.ProgramId, studentExamSurveyReportViewModel.SessionId, studentExamSurveyReportViewModel.SelectedCourse.ToList(), studentExamSurveyReportViewModel.SelectedBranch.ToList(),
                        studentExamSurveyReportViewModel.SelectedCampus.ToList(), studentExamSurveyReportViewModel.SelectedBatch.ToList(), studentExamSurveyReportViewModel.SelectedSubject.ToList(), studentExamSurveyReportViewModel.FromTime.ToLocalTime(), studentExamSurveyReportViewModel.ToTime.ToLocalTime(), studentExamSurveyReportViewModel.ExamIds.ToList(), studentExamSurveyReportViewModel.BatchIds.ToList());
                    
                    long recordsFiltered = recordsTotal;
                    //var examIdString = string.Join(", ", examList);
                    var batchIdString = string.Join(", ", studentExamSurveyReportViewModel.BatchIds);
                    var data = new List<object>();
                    foreach (var evaluationReport in studentExamEvaluationViewModels)
                    {
                        var str = new List<string>();
                        str.Add(evaluationReport.SubjectName);
                        str.Add(evaluationReport.ExamName);
                        str.Add(evaluationReport.Standard+ " (" +evaluationReport.StandardPercentage.ToString() + "%) ");
                        str.Add(evaluationReport.Hard + " (" + evaluationReport.HardPercentage.ToString() + "%) ");
                        str.Add(evaluationReport.Easy + " (" + evaluationReport.EasyPercentage.ToString() + "%) ");
                        str.Add(evaluationReport.Total.ToString());
                        str.Add("<a href='" +
                                Url.Action("IndividualExamEvaluationReport", "StudentExamEvaluationReport") +
                                "?examId=" + evaluationReport.ExamId + "&batchIds=" + batchIdString + "&evaluationObj=" + studentExamSurveyReportViewModelObj + "&examName=" + evaluationReport.ExamName + "' data-id='" +
                                evaluationReport.ExamId + "' class='glyphicon glyphicon-th-list'></a>");
                        data.Add(str);
                    }
                    ViewData["studentExamSurveyReportViewModel"] = studentExamSurveyReportViewModel;
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = 1,
                    recordsFiltered = 1,
                    start = start,
                    length = length,
                    data = new List<string>()
                });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                return View();
            }
        }
        #endregion

        #region Helper
        private IList<ExamEvaluationReportDto> GenerateExamEvaluationReport(StudentExamSurveyReportViewModel studentExamSurveyReportViewModel, long[] examIds = null,long[]batchIds=null, int start = 0, int length = int.MaxValue, string orderBy = "StandardPercentage", string orderDir = "DESC")
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //if (examIds == null)
            //{
            //    IList<Exams> examList = _examsService.LoadExamByProgramAndSessionAndBranchsAndCampusesAndBatches(_userMenu, studentExamSurveyReportViewModel.ProgramId, studentExamSurveyReportViewModel.SessionId, studentExamSurveyReportViewModel.SelectedCourse, studentExamSurveyReportViewModel.SelectedBranch, studentExamSurveyReportViewModel.SelectedCampus, studentExamSurveyReportViewModel.SelectedBatch, studentExamSurveyReportViewModel.SelectedSubject, studentExamSurveyReportViewModel.FromTime.ToLocalTime(), studentExamSurveyReportViewModel.ToTime.ToLocalTime());
            //    examIds = examList.Select(x => x.Id).ToArray();
            //}
            var studentExamAttendances = _studentExamAttendanceService.LoadExamEvaluationDto(start, length,orderBy, orderDir, studentExamSurveyReportViewModel.ProgramId, studentExamSurveyReportViewModel.SessionId, studentExamSurveyReportViewModel.SelectedCourse.ToList(), studentExamSurveyReportViewModel.SelectedBranch.ToList(),
                        studentExamSurveyReportViewModel.SelectedCampus.ToList(), studentExamSurveyReportViewModel.SelectedBatch.ToList(), studentExamSurveyReportViewModel.SelectedSubject.ToList(), studentExamSurveyReportViewModel.FromTime.ToLocalTime(), studentExamSurveyReportViewModel.ToTime.ToLocalTime(), examIds.ToList(),batchIds.ToList());
            IList<ExamEvaluationReportDto> studentExamEvaluationDto = new List<ExamEvaluationReportDto>();
            foreach (var examAttendance in studentExamAttendances)
            {
                var studentExamEvaluation = new ExamEvaluationReportDto
                {
                    SubjectName = examAttendance.SubjectName,
                    ExamName = examAttendance.ExamName,
                    ExamId = examAttendance.ExamId,
                    Total = examAttendance.Total,
                    Easy = examAttendance.Easy,
                    Standard = examAttendance.Standard,
                    Hard = examAttendance.Hard,
                    EasyPercentage = examAttendance.EasyPercentage,
                    StandardPercentage = examAttendance.StandardPercentage,
                    HardPercentage = examAttendance.HardPercentage
                };
                studentExamEvaluationDto.Add(studentExamEvaluation);
            }
            return studentExamEvaluationDto;
        }
        #endregion

        #region Individual Exam Evaluation Report
        [HttpGet]
        public ActionResult IndividualExamEvaluationReport(long examId, string evaluationObj,string batchIds, string examName)
        {
            try
            {
                var studentExamSurveyReportViewModel = new JavaScriptSerializer().Deserialize<StudentExamSurveyReportViewModel>( evaluationObj );
                studentExamSurveyReportViewModel.FromTime = Convert.ToDateTime(studentExamSurveyReportViewModel.FromTime.AddDays(1).ToString("dd-MMM-yyyy"));
                studentExamSurveyReportViewModel.ToTime = Convert.ToDateTime(studentExamSurveyReportViewModel.ToTime.AddDays(1).ToString("dd-MMM-yyyy"));
                var program = _programService.GetProgram(studentExamSurveyReportViewModel.ProgramId);
                var session = _sessionService.LoadById(studentExamSurveyReportViewModel.SessionId);
                var batchName = "";
                var branchName = "";
                var batchDays = "";
                var campusName = "";
                var batchTime = "";
                var courseName = "";

                batchDays = studentExamSurveyReportViewModel.SelectedBatchDays[0] == "0" ? "ALL" : string.Join(" ; ", studentExamSurveyReportViewModel.SelectedBatchDays.ToArray());
                batchTime = studentExamSurveyReportViewModel.SelectedBatchTime[0] == "0" ? "ALL" : string.Join("; ", studentExamSurveyReportViewModel.SelectedBatchTime.ToArray());
                if (studentExamSurveyReportViewModel.SelectedBatch.Contains(SelectionType.SelelectAll))
                {
                    batchName = "ALL";
                }
                else
                {
                    IList<Batch> batches = _batchService.LoadBatch(studentExamSurveyReportViewModel.SelectedBatch);
                    var batch = batches.GroupBy(x => x.Name);
                    batchName = batch.Aggregate(batchName, (current, batcNam) => current + (batcNam.Key + ","));
                    batchName = batchName.Remove(batchName.Length - 1);
                }
                if (studentExamSurveyReportViewModel.SelectedBranch.Contains(SelectionType.SelelectAll))
                {
                    branchName = "ALL";
                }
                else
                {
                    IList<Branch> branches = _branchService.LoadBranch(studentExamSurveyReportViewModel.SelectedBranch);
                    var batc = branches.GroupBy(x => x.Name);
                    branchName = batc.Aggregate(branchName, (current, brName) => current + (brName.Key + ","));
                    branchName = branchName.Remove(branchName.Length - 1);
                }

                if (studentExamSurveyReportViewModel.SelectedCampus.Contains(SelectionType.SelelectAll))
                {
                    campusName = "ALL";
                }
                else
                {
                    IList<Campus> campuses = _campusService.LoadCampus(studentExamSurveyReportViewModel.SelectedCampus);
                    var batc = campuses.GroupBy(x => x.Name);
                    campusName = batc.Aggregate(campusName, (current, caName) => current + (caName.Key + ","));
                    campusName = campusName.Remove(campusName.Length - 1);
                }
                if (studentExamSurveyReportViewModel.SelectedCourse.Contains(SelectionType.SelelectAll))
                {
                    courseName = "ALL";
                }
                else
                {
                    IList<Course> courses = _courseService.LoadCourse(studentExamSurveyReportViewModel.SelectedCourse);
                    var course = courses.GroupBy(x => x.Name);
                    courseName = course.Aggregate(courseName, (current, cName) => current + (cName.Key + ","));
                    courseName = courseName.Remove(courseName.Length - 1);
                }
                var json = new JavaScriptSerializer().Serialize(studentExamSurveyReportViewModel);
                ViewData["studentExamSurveyReportViewModel"] = json;
                ViewData["examId"] = examId;
                ViewBag.ProgramSession = program.Name + "-" + session.Name;
                ViewBag.Branch = branchName;
                ViewBag.BatchName = batchName;
                ViewBag.BatchDays = batchDays;
                ViewBag.CampusName = campusName;
                ViewBag.BatchTime = batchTime;
                ViewBag.Exam = examName;
                ViewBag.FromDate = studentExamSurveyReportViewModel.FromTime.ToString("dd-MMM-yyyy");
                ViewBag.ToDate = studentExamSurveyReportViewModel.ToTime.ToString("dd-MMM-yyyy");
                ViewBag.Course = courseName;
                ViewBag.batchIds = batchIds;
                ViewBag.informationViewList = studentExamSurveyReportViewModel.InformationViewList;
                return View();
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return View();
            }

        }

        [HttpPost]
        public ActionResult ExamEvaluaionIndividualReport(int draw, int start, int length, long examId,string batchIds, string studentExamSurveyReportViewModel)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (examId == 0)
                {
                    return HttpNotFound();
                }
                long[] examArray = { examId };
                var studentExamSurvey = new JavaScriptSerializer().Deserialize<StudentExamSurveyReportViewModel>(studentExamSurveyReportViewModel);
                var studentExamAttendances = _studentExamAttendanceService.LoadExamEvaluationForIndividualReport(_userMenu, start, length, studentExamSurvey.ProgramId, studentExamSurvey.SessionId, studentExamSurvey.SelectedBranch.ToList(), studentExamSurvey.SelectedCampus.ToList(), studentExamSurvey.SelectedBatch.ToList(), studentExamSurvey.FromTime.ToLocalTime(), studentExamSurvey.ToTime.ToLocalTime(), examArray.ToList(), batchIds);
                var studentExamEvaluationViewModels = new List<IndividualStudentExamEvaluationReport>();
                foreach (var studentExamAttendance in studentExamAttendances)
                {
                    var studentExamEvaluation = new IndividualStudentExamEvaluationReport();
                    studentExamEvaluation.ExamName = studentExamAttendance.StudentExamAttendance.Exams.Name;
                    studentExamEvaluation.ProgramRoll = studentExamAttendance.StudentProgram.PrnNo;
                    studentExamEvaluation.NickName = studentExamAttendance.StudentProgram.Student.NickName;
                    studentExamEvaluation.MobileNumber = studentExamAttendance.StudentProgram.Student.Mobile;
                    studentExamEvaluation.Branch = studentExamAttendance.StudentProgram.Batch.Branch.Name;
                    if (studentExamAttendance.StudentProgram.Institute != null) { studentExamEvaluation.Institute = studentExamAttendance.StudentProgram.Institute.Name; }
                    studentExamEvaluation.StudentName = studentExamAttendance.StudentProgram.Student.NickName;
                    studentExamEvaluation.FatherName = studentExamAttendance.StudentProgram.Student.FatherName;
                    studentExamEvaluation.MobileFather = studentExamAttendance.StudentProgram.Student.GuardiansMobile1;
                    studentExamEvaluation.MobileMother = studentExamAttendance.StudentProgram.Student.GuardiansMobile2;
                    studentExamEvaluation.MobilePersonal = studentExamAttendance.StudentProgram.Student.Mobile;
                    studentExamEvaluation.Batch = studentExamAttendance.StudentProgram.Batch;
                    studentExamEvaluation.Campus = studentExamAttendance.StudentProgram.Batch.Campus;
                    studentExamEvaluation.Email = studentExamAttendance.StudentProgram.Student.Email;
                    if (studentExamAttendance.StudentFeedback != null)
                        if (Convert.ToInt32(studentExamAttendance.StudentFeedback) == (int)ExamFeedback.Standard)
                        {
                            studentExamEvaluation.Answer = "Standard";
                        }
                        else if (Convert.ToInt32(studentExamAttendance.StudentFeedback) == (int)ExamFeedback.Hard)
                        {
                            studentExamEvaluation.Answer = "Hard";
                        }
                        else if (Convert.ToInt32(studentExamAttendance.StudentFeedback) == (int)ExamFeedback.Easy)
                        {
                            studentExamEvaluation.Answer = "Easy";
                        }
                    studentExamEvaluationViewModels.Add(studentExamEvaluation);
                }
                int recordsTotal = _studentExamAttendanceService.CountRowCountForIndividualExamEvaluationReport(_userMenu, studentExamSurvey.ProgramId, studentExamSurvey.SessionId, studentExamSurvey.SelectedBranch.ToList(), studentExamSurvey.SelectedCampus.ToList(), studentExamSurvey.SelectedBatch.ToList(), studentExamSurvey.FromTime.ToLocalTime(), studentExamSurvey.ToTime.ToLocalTime(), examArray.ToList(),batchIds);
                long recordsFiltered = recordsTotal;
                var data = new List<object>();
                foreach (IndividualStudentExamEvaluationReport evaluationReport in studentExamEvaluationViewModels)
                {
                    var str = new List<string>();
                    str.Add(evaluationReport.ExamName);
                    str.Add(evaluationReport.ProgramRoll);
                    if (studentExamSurvey.InformationViewList != null)
                    {
                        foreach (string informationView in studentExamSurvey.InformationViewList)
                        {
                            switch (informationView)
                            {
                                case "Institute":
                                    if (!string.IsNullOrEmpty(evaluationReport.Institute))
                                    {
                                        str.Add(evaluationReport.Institute);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Student Name":
                                    if (!string.IsNullOrEmpty(evaluationReport.StudentName))
                                    {
                                        str.Add(evaluationReport.StudentName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Father Name":
                                    if (!string.IsNullOrEmpty(evaluationReport.FatherName))
                                    {
                                        str.Add(evaluationReport.FatherName);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number(Father)":
                                    if (!string.IsNullOrEmpty(evaluationReport.MobileFather))
                                    {
                                        str.Add(evaluationReport.MobileFather);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mobile Number(Mother)":
                                    if (!string.IsNullOrEmpty(evaluationReport.MobileMother))
                                    {
                                        str.Add(evaluationReport.MobileMother);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Mother Number(Personal)":
                                    if (!string.IsNullOrEmpty(evaluationReport.MobilePersonal))
                                    {
                                        str.Add(evaluationReport.MobilePersonal);
                                        break;
                                    }
                                    str.Add("");
                                    break;

                                case "Batch":
                                    if (evaluationReport.Batch != null &&
                                        !String.IsNullOrEmpty(evaluationReport.Batch.Name))
                                    {
                                        str.Add(evaluationReport.Batch.Name);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Campus":
                                    if (evaluationReport.Campus != null &&
                                        !String.IsNullOrEmpty(evaluationReport.Campus.Name))
                                    {
                                        str.Add(evaluationReport.Campus.Name);
                                        break;
                                    }
                                    str.Add("");
                                    break;
                                case "Email":
                                    if (!string.IsNullOrEmpty(evaluationReport.Email))
                                    {
                                        str.Add(evaluationReport.Email);
                                        break;
                                    }
                                    str.Add("");
                                    break;

                                default:
                                    break;
                            }
                        }

                    }
                    str.Add(evaluationReport.NickName);
                    str.Add(evaluationReport.Branch);
                    str.Add(evaluationReport.MobileNumber);
                    str.Add(evaluationReport.Answer);
                    data.Add(str);
                }
                ViewData["examId"] = examId;
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = data
                });

            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return View();
            }
        }
        #endregion

        #region Export Exam & Individual Report 
        
        public ActionResult ExportExamEvaluaionIndividualReport(long examId,string batchIds, string studentExamSurveyReportViewModel)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (examId == 0)
                {
                    return HttpNotFound();
                }
                var examArray = new[] { examId };
                var studentExamSurvey = new JavaScriptSerializer().Deserialize<StudentExamSurveyReportViewModel>( studentExamSurveyReportViewModel );
                var studentExamAttendances = _studentExamAttendanceService.LoadExamEvaluationForIndividualReport(_userMenu, 0, -1, studentExamSurvey.ProgramId, studentExamSurvey.SessionId, studentExamSurvey.SelectedBranch.ToList(), studentExamSurvey.SelectedCampus.ToList(), studentExamSurvey.SelectedBatch.ToList(), studentExamSurvey.FromTime.ToLocalTime(), studentExamSurvey.ToTime.ToLocalTime(), examArray.ToList(),batchIds);
                var studentExamEvaluationViewModels = new List<IndividualStudentExamEvaluationReport>();
                foreach (var studentExamAttendance in studentExamAttendances)
                {
                    var studentExamEvaluation = new IndividualStudentExamEvaluationReport
                    {
                        ExamName = studentExamAttendance.StudentExamAttendance.Exams.Name,
                        ProgramRoll = studentExamAttendance.StudentProgram.PrnNo,
                        NickName = studentExamAttendance.StudentProgram.Student.NickName,
                        MobileNumber = studentExamAttendance.StudentProgram.Student.Mobile,
                        Branch = studentExamAttendance.StudentProgram.Batch.Branch.Name
                    };
                    if (studentExamAttendance.StudentProgram.Institute != null)
                    {
                        studentExamEvaluation.Institute = studentExamAttendance.StudentProgram.Institute.Name;
                    }
                    studentExamEvaluation.StudentName = studentExamAttendance.StudentProgram.Student.NickName;
                    studentExamEvaluation.FatherName = studentExamAttendance.StudentProgram.Student.FatherName;
                    studentExamEvaluation.MobileFather = studentExamAttendance.StudentProgram.Student.GuardiansMobile1;
                    studentExamEvaluation.MobileMother = studentExamAttendance.StudentProgram.Student.GuardiansMobile2;
                    studentExamEvaluation.MobilePersonal = studentExamAttendance.StudentProgram.Student.Mobile;
                    studentExamEvaluation.Batch = studentExamAttendance.StudentProgram.Batch;
                    studentExamEvaluation.Campus = studentExamAttendance.StudentProgram.Batch.Campus;
                    studentExamEvaluation.Email = studentExamAttendance.StudentProgram.Student.Email;
                    if (studentExamAttendance.StudentFeedback != null)
                        if (Convert.ToInt32(studentExamAttendance.StudentFeedback) == (int)ExamFeedback.Standard)
                        {
                            studentExamEvaluation.Answer = "Standard";
                        }
                        else if (Convert.ToInt32(studentExamAttendance.StudentFeedback) == (int)ExamFeedback.Hard)
                        {
                            studentExamEvaluation.Answer = "Hard";
                        }
                        else if (Convert.ToInt32(studentExamAttendance.StudentFeedback) ==
                                 (int)ExamFeedback.Easy)
                        {
                            studentExamEvaluation.Answer = "Easy";
                        }
                    studentExamEvaluationViewModels.Add(studentExamEvaluation);

                }
                List<string> headerList = new List<string>();
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                columnList.Add("Exam Name");
                columnList.Add("Program Roll");
                if (studentExamSurvey.InformationViewList != null)
                {
                    foreach (string informationView in studentExamSurvey.InformationViewList)
                    {
                        columnList.Add(informationView);
                    }
                }
                else
                {
                    columnList.Add("Nick Name");
                    columnList.Add("Branch");
                    columnList.Add("Mobile No.");
                }
                columnList.Add("Answer");
                var xlsList = new List<List<object>>();
                foreach (IndividualStudentExamEvaluationReport evaluationReport in studentExamEvaluationViewModels)
                {
                    var xlsRow = new List<object>();
                    xlsRow.Add(evaluationReport.ExamName);
                    xlsRow.Add(evaluationReport.ProgramRoll);
                    if (studentExamSurvey.InformationViewList == null)
                    {
                        xlsRow.Add(evaluationReport.NickName);
                        xlsRow.Add(evaluationReport.Branch);
                        xlsRow.Add(evaluationReport.MobileNumber);
                    }
                    if (studentExamSurvey.InformationViewList != null)
                    {
                        foreach (string informationView in studentExamSurvey.InformationViewList)
                        {
                            switch (informationView)
                            {
                                case "Institute":
                                    if (!string.IsNullOrEmpty(evaluationReport.Institute))
                                    {
                                        xlsRow.Add(evaluationReport.Institute);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Student Name":
                                    if (!string.IsNullOrEmpty(evaluationReport.StudentName))
                                    {
                                        xlsRow.Add(evaluationReport.StudentName);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Father Name":
                                    if (!string.IsNullOrEmpty(evaluationReport.FatherName))
                                    {
                                        xlsRow.Add(evaluationReport.FatherName);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Mobile Number(Father)":
                                    if (!string.IsNullOrEmpty(evaluationReport.MobileFather))
                                    {
                                        xlsRow.Add(evaluationReport.MobileFather);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Mobile Number(Mother)":
                                    if (!string.IsNullOrEmpty(evaluationReport.MobileMother))
                                    {
                                        xlsRow.Add(evaluationReport.MobileMother);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Mother Number(Personal)":
                                    if (!string.IsNullOrEmpty(evaluationReport.MobilePersonal))
                                    {
                                        xlsRow.Add(evaluationReport.MobilePersonal);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;

                                case "Batch":
                                    if (evaluationReport.Batch != null &&
                                        !String.IsNullOrEmpty(evaluationReport.Batch.Name))
                                    {
                                        xlsRow.Add(evaluationReport.Batch.Name);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Campus":
                                    if (evaluationReport.Campus != null &&
                                        !String.IsNullOrEmpty(evaluationReport.Campus.Name))
                                    {
                                        xlsRow.Add(evaluationReport.Campus.Name);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;
                                case "Email":
                                    if (!string.IsNullOrEmpty(evaluationReport.Email))
                                    {
                                        xlsRow.Add(evaluationReport.Email);
                                        break;
                                    }
                                    xlsRow.Add("");
                                    break;

                                default:
                                    break;
                            }
                        }

                    }
                    xlsRow.Add(evaluationReport.Answer);
                    xlsList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, xlsList, footerList, "Individual_ExamEvaluation_Report__" + DateTime.Now.ToString("yyyyMMdd-HHmmss" ) );
                return View("Autoclose");

            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return View("Autoclose");
            }

        }
        public ActionResult ExcelExportExamEvaluation(string studentExamSurveyReportViewModelObj)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (!String.IsNullOrEmpty(studentExamSurveyReportViewModelObj))
                {
                    var studentExamSurveyReportViewModel = new JavaScriptSerializer().Deserialize<StudentExamSurveyReportViewModel>( studentExamSurveyReportViewModelObj );
                    List<string> headerList = new List<string>();
                    List<string> footerList = new List<string>();
                    footerList.Add("");
                    List<string> columnList = new List<string>();
                    columnList.Add("Subject");
                    columnList.Add("ExamName");
                    columnList.Add("Standard");
                    columnList.Add("Hard");
                    columnList.Add("Easy");
                    columnList.Add("Total");
                    var transactionXlsList = new List<List<object>>();
                    long[] examIds = studentExamSurveyReportViewModel.SelectedExam;
                    //if (examIds.Contains(SelectionType.SelelectAll))
                    //{
                    //    IList<Exams> examList = _examsService.LoadExamByProgramAndSessionAndBranchsAndCampusesAndBatches(_userMenu, studentExamSurveyReportViewModel.ProgramId,  studentExamSurveyReportViewModel.SessionId, studentExamSurveyReportViewModel.SelectedCourse, studentExamSurveyReportViewModel.SelectedBranch, studentExamSurveyReportViewModel.SelectedCampus, studentExamSurveyReportViewModel.SelectedBatch, studentExamSurveyReportViewModel.SelectedSubject, studentExamSurveyReportViewModel.FromTime.ToLocalTime(), studentExamSurveyReportViewModel.ToTime.ToLocalTime());
                    //    examIds = examList.Select(x => x.Id).ToArray();
                    //}
                    
                    var studentExamEvaluationViewModels = GenerateExamEvaluationReport(studentExamSurveyReportViewModel, studentExamSurveyReportViewModel.ExamIds, studentExamSurveyReportViewModel.BatchIds);
                    foreach (var evaluationReport in studentExamEvaluationViewModels)
                    {
                        var xlsRow = new List<object>();
                        if (columnList.Contains("Subject"))
                        {
                            xlsRow.Add(evaluationReport.SubjectName);
                        }
                        if (columnList.Contains("ExamName"))
                        {
                            xlsRow.Add(evaluationReport.ExamName);
                        }
                        if (columnList.Contains("Standard"))
                        {
                            xlsRow.Add(evaluationReport.Standard + " (" + evaluationReport.StandardPercentage + "%) ");
                        }
                        if (columnList.Contains("Hard"))
                        {
                            xlsRow.Add(evaluationReport.Hard + " (" + evaluationReport.HardPercentage + "%) ");
                        }
                        if (columnList.Contains("Easy"))
                        {
                            xlsRow.Add(evaluationReport.Easy + " (" + evaluationReport.EasyPercentage + "%) ");
                        }
                        if (columnList.Contains("Total"))
                        {
                            xlsRow.Add(evaluationReport.Total);
                        }
                        transactionXlsList.Add(xlsRow);
                    }
                    ExcelGenerator.GenerateExcel(headerList, columnList, transactionXlsList, footerList, "ExamEvaluatioReport__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                    return View("Autoclose");

                }

                return View("Autoclose");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return View("Autoclose");
            }
        }

        #endregion

        #region Ajax Functions

        [HttpPost]
        public JsonResult AjaxRequestForSubject(long organizatonId, long programId, long sessionId, long[] courseId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var courseList = _courseService.LoadCourse(organizatonId, programId, sessionId, courseId);
                    List<SelectListItem> subjectList = new List<SelectListItem>();
                    List<Subject> subjects = new List<Subject>();
                    if (courseList != null)
                    {
                        foreach (var singleCourse in courseList)
                        {
                            var courseSubject = singleCourse.CourseSubjects.ToList();

                            if (courseSubject != null && courseSubject.Count > 0)
                            {
                                foreach (var subject in courseSubject)
                                {
                                    subjects.Add(subject.Subject);
                                }
                            }
                        }
                        SelectList subjectSelectList = new SelectList(subjects.Where(sub => sub.Status == StudentProgram.EntityStatus.Active).ToList(), "Id", "Name");
                        subjectList = subjectSelectList.ToList();
                    }
                    subjectList.Insert(0, new SelectListItem() { Value = "0", Text = "All subject" });
                    return Json(new { subjectList = subjectList, IsSuccess = true });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public ActionResult AjaxRequestForExam(long programId, long sessionId, long[] courseId, long[] branchId, long[] campusId, long[] batchId, long[] subjectId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<Exams> examList = _examsService.LoadExamByProgramAndSessionAndBranchsAndCampusesAndBatches(_userMenu, programId, sessionId, courseId, branchId, campusId, batchId, subjectId, dateFrom, dateTo);
                List<SelectListItem> examSelecteList = (new SelectList(examList, "Id", "Name")).ToList();
                if (examList.Any())
                    examSelecteList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Exam" });

                return Json(new { examList = examSelecteList, IsSuccess = true });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        [HttpPost]

        public JsonResult GetTotalExamEvaluationRow(long organizationId, long programId, long sessionId, long[] courseId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] subjectId, DateTime dateFrom, DateTime dateTo, long[] examIds)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays, batchTime, null, null).ToList();
                if (batchId[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(batchId)).ToList();
                }
                List<Branch> branchList = _branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId)).ToList();
                if (branchId[0] != 0)
                {
                    branchList = branchList.Where(b => b.Id.In(branchId)).ToList();
                }
                var courseList = _courseService.LoadCourse(organizationId, programId, sessionId, courseId);

                List<Subject> subjectList = new List<Subject>();
                if (courseList != null)
                {
                    foreach (var singleCourse in courseList)
                    {
                        var courseSubject = singleCourse.CourseSubjects.ToList();

                        if (courseSubject != null && courseSubject.Count > 0)
                        {
                            foreach (var subject in courseSubject)
                            {
                                subjectList.Add(subject.Subject);
                            }
                        }
                    }
                }
                if (!subjectId.Contains(0))
                {
                    subjectList = subjectList.Where(x => x.Id.In(subjectId)).ToList();
                }
                IList<Exams> examList = _examsService.LoadExams(programId, sessionId, courseId, batchList, branchList, null, null, true, subjectList.Select(x => x.Id).Distinct().ToArray(), dateFrom, dateTo);
                if ((Array.Exists(examIds, item => item == 0)))
                {

                    examIds = examList.Select(x => x.Id).ToArray();
                }
                else
                {
                    examIds = examList.Where(x => x.Id.In(examIds)).Select(x => x.Id).ToArray();
                }
                
                
                int recordsTotal = 0;

                recordsTotal = _studentExamAttendanceService.GetTotalStudentCount(_userMenu, programId, sessionId, branchId.ToList(), campusId.ToList(), batchId.ToList(), dateFrom, dateTo, examIds.ToList(), string.Join(",", batchList.Select(x=>x.Id).ToList()));
                if (recordsTotal > 0)
                {
                    return Json(new { rowCount = recordsTotal, IsSuccess = true });
                }
                return Json(new { rowCount = recordsTotal, IsSuccess = false });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        public ActionResult LoadExams(long organizationId, long programId, long sessionId, long[] courseId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] subjectId, DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<Batch> batchList = _batchService.LoadAuthorizeBatch(_userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), branchId.ToList(), _commonHelper.ConvertIdToList(sessionId), campusId.ToList(), batchDays, batchTime, null, null).ToList();
                if (batchId[0] != 0)
                {
                    batchList = batchList.Where(b => b.Id.In(batchId)).ToList();
                }
                List<Branch> branchList = _branchService.LoadAuthorizedBranch(_userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId)).ToList();
                if (branchId[0] != 0)
                {
                    branchList = branchList.Where(b => b.Id.In(branchId)).ToList();
                }
                var courseList = _courseService.LoadCourse(organizationId, programId, sessionId, courseId);
                    
                    List<Subject> subjectList = new List<Subject>();
                    if (courseList != null)
                    {
                        foreach (var singleCourse in courseList)
                        {
                            var courseSubject = singleCourse.CourseSubjects.ToList();

                            if (courseSubject != null && courseSubject.Count > 0)
                            {
                                foreach (var subject in courseSubject)
                                {
                                    subjectList.Add(subject.Subject);
                                }
                            }
                        }
                    }
                if (!subjectId.Contains(0))
                {
                    subjectList = subjectList.Where(x => x.Id.In(subjectId)).ToList();
                }
                IList<Exams> examList = _examsService.LoadExams(programId, sessionId, courseId, batchList, branchList, null, null, true, subjectList.Select(x=>x.Id).Distinct().ToArray(), dateFrom, dateTo);
                List<SelectListItem> selectexamList = (new SelectList(examList, "Id", "Name")).ToList();
                if (examList.Any())
                {
                    selectexamList.Insert(0, new SelectListItem() { Value = "0", Text = "All Exam" });
                    return Json(new { examList = selectexamList, IsSuccess = true });
                }
                return Json(new { examList = new List<Exams>(), IsSuccess = true });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return Json(new { examList = new List<Exams>(), IsSuccess = false });

            }

        }
        #endregion
        
    }
}
