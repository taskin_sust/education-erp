﻿using System;
using System.Collections.Generic;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IChildrenInfoDao : IBaseDao<ChildrenInfo, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<ChildrenInfo> GetTeamMemberChildrenInfo(long teamMemberId);
        IList<ChildrenInfo> LoadTeamMemberChildrenInfo(List<long> teamMemberIdList, DateTime? searchingDate = null);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
    public class ChildrenInfoDao : BaseDao<ChildrenInfo, long>, IChildrenInfoDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<ChildrenInfo> GetTeamMemberChildrenInfo(long teamMemberId)
        {
            return Session.QueryOver<ChildrenInfo>().Where(x => x.TeamMember.Id == teamMemberId && x.Status != ChildrenInfo.EntityStatus.Delete).OrderBy(x => x.Dob).Asc.ThenBy(x => x.Status).Desc.ThenBy(x => x.Id).Asc.List<ChildrenInfo>();
        }

        public IList<ChildrenInfo> LoadTeamMemberChildrenInfo(List<long> teamMemberIdList, DateTime? searchingDate = null)
        {
            if (searchingDate != null)
                return Session.QueryOver<ChildrenInfo>().Where(x => x.TeamMember.Id.IsIn(teamMemberIdList) && x.Status != ChildrenInfo.EntityStatus.Delete && x.Dob.Value <= searchingDate.Value.Date).List<ChildrenInfo>();

            return Session.QueryOver<ChildrenInfo>().Where(x => x.TeamMember.Id.IsIn(teamMemberIdList) && x.Status != ChildrenInfo.EntityStatus.Delete).List<ChildrenInfo>();
        }

        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion

        
    }

}
