﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;
using WebGrease.Css.Extensions;

namespace UdvashERP.Controllers
{    
    [UerpArea("NoArea")]
    [Authorize]
    [AuthorizeAccess]
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index(String message = "")
        {

            if (User.Identity.IsAuthenticated == false)
                return RedirectToAction("Login", "Account");
            ViewBag.Message = message;
            return View();
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [AllowAnonymous]
        public void Test()
        {
            List<string> tStr = new List<string>();
            tStr.Add("A");
            tStr.Add("B");
            tStr.Add("C");
            tStr.Add("D");
            for (int i = 0; i < tStr.Count; i++)
            {
                tStr[i] = "EEE";
            }
            foreach (string str in tStr)
            {
                str.Replace("A", "EEE");
                Console.Write(str);
            }

        }

        [AllowAnonymous]
        public ActionResult p(string pageTitle="")
        {
            ViewBag.pageTitle = pageTitle;
            return View();
        }
        //[AuthorizeAccess]
        //public ActionResult AspNetUserTest()
        //{
        //    var model = new AspNetUser()
        //    {
        //        Email = "masumsss@gmail.com",
        //        LockoutEndDateUtc = DateTime.Now
        //    };
            
        //    var session = NHibernateSessionFactory.OpenSession();
        //    var aspNetUserDao = new AspNetUserDao(){Session = session};

        //    var list = aspNetUserDao.LoadAll();
        //    aspNetUserDao.Save(model);

        //    return RedirectToAction("Index","Home",new{Message="Done"});
        //}

        [AllowAnonymous]
        public ActionResult PermissionDenied()
        {
            return View();
        }


        [AllowAnonymous]
        public ActionResult Error()
        {
            return View();
        }

        //[AllowAnonymous]
        //public ActionResult Test()
        //{
        //    var session = NHibernateSessionFactory.OpenSession();
        //    var menuService = new MenuService(session);
        //    var menu = menuService.MenuLoadByIdEgarly(10142);
        //    var ma = menu.MenuAccess;

        //    return View();
        //}
    }
}