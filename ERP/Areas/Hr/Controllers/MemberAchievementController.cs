﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using iTextSharp.text.pdf.qrcode;
using log4net;
using Newtonsoft.Json.Linq;
using NHibernate.Criterion;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class MemberAchievementController : Controller
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IUserService _userService;
        private readonly IMemberAchievementService _memberAchievementService;
        private readonly ITeamMemberService _teamMemberService;
        private List<UserMenu> _userMenu;
        public MemberAchievementController()
        {
            var nHSession = NHibernateSessionFactory.OpenSession();
            _organizationService = new OrganizationService(nHSession);
            _commonHelper = new CommonHelper();
            _userService = new UserService(nHSession);
            _memberAchievementService = new MemberAchievementService(nHSession);
            _teamMemberService = new TeamMemberService(nHSession);
        }

        #endregion

        #region Report

        public ActionResult MonthlyAchievement(long id , bool hr = false)
        {
            MemberAchievement memberAchievement = new MemberAchievement();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                TeamMember currentTeamMember = null;
                if (!hr)
                {
                    _userMenu = null;
                    currentTeamMember = _teamMemberService.GetCurrentMember();
                    if (currentTeamMember == null)
                    {
                        RedirectToAction("Index");
                    }
                }

                memberAchievement = _memberAchievementService.GetMemberAchievement(id);
                if (memberAchievement == null)
                    throw new InvalidDataException("No Acievement entry found!");
                TeamMember teamMember = memberAchievement.TeamMember;
                if (teamMember == null)
                    throw new InvalidDataException("Invalid Acievement entry found!");

                if (hr == false && (currentTeamMember != null && memberAchievement.TeamMember.Id != currentTeamMember.Id))
                    throw new InvalidDataException("This is not your Acievement!");

                DateTime endDateTime = Convert.ToDateTime(memberAchievement.Year + "-" + memberAchievement.Month + "-1");
                var lastDayOfMonth = DateTime.DaysInMonth(endDateTime.Year, endDateTime.Month);
                string endDate = endDateTime.Year + "-" + endDateTime.Month + "-" + lastDayOfMonth;

                DateTime startDateTime = endDateTime.AddMonths(-11);
                string startDate = startDateTime.Year + "-" + startDateTime.Month + "-1";

                List<MyAchievementReport> reports = _memberAchievementService.LoadMyAchievementReports(_userMenu, memberAchievement.TeamMember.Id, startDate, endDate);
                
                var objForDesignation = reports.FirstOrDefault();
                string designation = "";
                if (objForDesignation != null)
                {
                    designation = objForDesignation.Designation;
                }
                string jsonreports = new JavaScriptSerializer().Serialize(reports);
                ViewBag.Reports = jsonreports;
                ViewBag.Designation = designation;

            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View(memberAchievement);

        }
        #endregion

        #region Index/Manage

        public ActionResult Index(string msg="")
        {
            try
            {
                if (TempData["SuccessMessageMA"] != null && !String.IsNullOrEmpty(msg) && msg == "show")
                {
                    ViewBag.SuccessMessage = TempData["SuccessMessageMA"];
                }
                SelectList monthSelect = new SelectList(_commonHelper.LoadEmumToDictionary<MonthsOfYear>(), "Key", "Value");
                List<SelectListItem> monthSelectList = monthSelect.ToList();
                monthSelectList.Insert(0, new SelectListItem { Value = "0", Text = "All Month" });
                ViewBag.MonthList = new MultiSelectList(monthSelectList, "Value", "Text");
                ViewBag.PageSize = Constants.PageSize;

                TeamMember currentTeamMember = _teamMemberService.GetCurrentMember();
                if (currentTeamMember == null)
                    throw new InvalidDataException("Sorry, you are not an employee of this Organization.");

            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        #region Render Data Table
        public JsonResult MemberAchievementDataTable(int draw, int start, int length, int year, int month)
        {
            string message = "";
            var getData = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            bool isSuccess = true;
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Year";
                            break;
                        case "1":
                            orderBy = "Month";
                            break;
                        case "2":
                            orderBy = "TotalWorkingHour";
                            break;
                        case "3":
                            orderBy = "TotalCompletePoint";
                            break;
                        case "4":
                            orderBy = "CompleteTaskCount";
                            break;
                        case "5":
                            orderBy = "RunningTaskCount";
                            break;
                        default:
                            orderBy = "Year";
                            break;
                    }
                }

                #endregion

                int pin = 0;
                TeamMember currentTeamMember = _teamMemberService.GetCurrentMember();
                if (currentTeamMember != null)
                    pin = currentTeamMember.Pin;
                else
                {
                    throw new InvalidDataException("Sorry, you are not an employee of this Organization.");
                }

                recordsTotal = _memberAchievementService.GetMemberAchievementCount(null, orderBy, orderDir, year, month, pin.ToString());
                recordsFiltered = recordsTotal;
                IList<MemberAchievement> memberAchievementList = _memberAchievementService.LoadMemberAchievementList(null, start, length, orderBy, orderDir.ToUpper(), year, month, pin.ToString()).ToList();
                decimal totalWorkingHour = 0;
                decimal totalCompletePoint = 0;
                decimal completeTaskCount = 0;
                decimal runningTaskCount = 0;
                foreach (MemberAchievement memberAchievement in memberAchievementList)
                {
                    string monthStr = _commonHelper.GetEmumIdToValue<MonthsOfYear>((int) memberAchievement.Month);
                    var str = new List<string>();
                    str.Add(memberAchievement.Year.ToString());
                    str.Add(monthStr);
                    str.Add(memberAchievement.TotalWorkingHour.ToString());
                    str.Add(memberAchievement.TotalCompletePoint.ToString());
                    str.Add(memberAchievement.CompleteTaskCount.ToString());
                    str.Add(memberAchievement.RunningTaskCount.ToString());
                    str.Add(
                            LinkGenerator.GetMemberAchievementGraphLink("MonthlyAchievement", "MemberAchievement", memberAchievement.Id) + 
                            LinkGenerator.GetEditLink("Edit", "MemberAchievement", memberAchievement.Id) + 
                            LinkGenerator.GetDeleteLinkForModal(memberAchievement.Id, memberAchievement.Year.ToString() + "-" + monthStr) 
                            
                            );
                    getData.Add(str);
                    totalWorkingHour += memberAchievement.TotalWorkingHour;
                    totalCompletePoint += memberAchievement.TotalCompletePoint;
                    completeTaskCount += memberAchievement.CompleteTaskCount;
                    runningTaskCount += memberAchievement.RunningTaskCount;
                }
                if (memberAchievementList.Any())
                {
                    var summary = new List<string>();
                    summary.Add("<strong>Total</strong>");
                    summary.Add("");
                    summary.Add("<strong>" + String.Format("{0:0.##}", totalWorkingHour) + "<strong>");
                    summary.Add("<strong>" + String.Format("{0:0.##}", totalCompletePoint) + "<strong>");
                    summary.Add("<strong>" + String.Format("{0:0.##}", completeTaskCount) + "<strong>");
                    summary.Add("<strong>" + String.Format("{0:0.##}", runningTaskCount) + "<strong>");
                    summary.Add("");
                    getData.Add(summary);
                }

            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
                isSuccess = false;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.SetExceptionMessage(ex);
                isSuccess = false;
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                IsSuccess = isSuccess,
                message = message
            });
        }

        #endregion

        public ActionResult AllMemberAchievement()
        {
            try
            {
                SelectList monthSelect = new SelectList(_commonHelper.LoadEmumToDictionary<MonthsOfYear>(), "Key", "Value");
                List<SelectListItem> monthSelectList = monthSelect.ToList();
                monthSelectList.Insert(0, new SelectListItem { Value = "0", Text = "All Month" });
                ViewBag.MonthList = new MultiSelectList(monthSelectList, "Value", "Text");
                ViewBag.PageSize = Constants.PageSize;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        #region Render Data Table
        public JsonResult AllMemberAchievementDataTable(int draw, int start, int length, int year, int month, string memberPin)
        {
            var getData = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Year";
                            break;
                        case "1":
                            orderBy = "Month";
                            break;
                        case "2":
                            orderBy = "teamMember.Pin";
                            break;
                        case "3":
                            orderBy = "TotalWorkingHour";
                            break;
                        case "4":
                            orderBy = "TotalCompletePoint";
                            break;
                        case "5":
                            orderBy = "CompleteTaskCount";
                            break;
                        case "6":
                            orderBy = "RunningTaskCount";
                            break;
                        default:
                            orderBy = "Year";
                            break;
                    }
                }

                #endregion

                recordsTotal = _memberAchievementService.GetMemberAchievementCount(_userMenu, orderBy, orderDir, year, month, memberPin);
                recordsFiltered = recordsTotal;
                IList<MemberAchievement> memberAchievementList = _memberAchievementService.LoadMemberAchievementList(_userMenu, start, length, orderBy, orderDir.ToUpper(), year, month, memberPin).ToList();
                decimal totalWorkingHour = 0;
                decimal totalCompletePoint = 0;
                decimal completeTaskCount = 0;
                decimal runningTaskCount = 0;
                foreach (MemberAchievement memberAchievement in memberAchievementList)
                {
                    string memberName = "";
                    if(memberAchievement.TeamMember!=null)
                        memberName = memberAchievement.TeamMember.Name + " (" + memberAchievement.TeamMember.Pin.ToString() + ")";
                    string monthStr = _commonHelper.GetEmumIdToValue<MonthsOfYear>((int)memberAchievement.Month);
                    var str = new List<string>();
                    str.Add(memberAchievement.Year.ToString());
                    str.Add(monthStr);
                    str.Add(memberName);
                    str.Add(memberAchievement.TotalWorkingHour.ToString());
                    str.Add(memberAchievement.TotalCompletePoint.ToString());
                    str.Add(memberAchievement.CompleteTaskCount.ToString());
                    str.Add(memberAchievement.RunningTaskCount.ToString());
                    str.Add(LinkGenerator.GetMemberAchievementGraphLink("MonthlyAchievement", "MemberAchievement", memberAchievement.Id, "&hr=true"));
                    getData.Add(str);
                    totalWorkingHour += memberAchievement.TotalWorkingHour;
                    totalCompletePoint += memberAchievement.TotalCompletePoint;
                    completeTaskCount += memberAchievement.CompleteTaskCount;
                    runningTaskCount += memberAchievement.RunningTaskCount;
                }
                if (memberAchievementList.Any())
                {
                    var summary = new List<string>();
                    summary.Add("<strong>Total</strong>");
                    summary.Add("");
                    summary.Add("");
                    summary.Add("<strong>" + String.Format("{0:0.##}", totalWorkingHour) + "<strong>");
                    summary.Add("<strong>" + String.Format("{0:0.##}", totalCompletePoint) + "<strong>");
                    summary.Add("<strong>" + String.Format("{0:0.##}", completeTaskCount) + "<strong>");
                    summary.Add("<strong>" + String.Format("{0:0.##}", runningTaskCount) + "<strong>");
                    summary.Add("");
                    getData.Add(summary);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = true
            });
        }

        #endregion

        #endregion

        #region Operational Function

        public ActionResult Create()
        {
            try
            {
                
                TeamMember currentTeamMember = _teamMemberService.GetCurrentMember();
                if (currentTeamMember == null)
                {
                    RedirectToAction("Index");
                }

                ViewBag.MonthList = new SelectList(_commonHelper.LoadEmumToDictionary<MonthsOfYear>(), "Key", "Value");


            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public JsonResult Create(MemberAchievementViewModel memberAchievementViewModel)
        {
            string message = "";
            bool successState = false;
            try
            {
                MemberAchievementViewModel mavm = memberAchievementViewModel;
                TeamMember currentTeamMember = _teamMemberService.GetCurrentMember();
                long currentUserId = _userService.GetCurrentUserId();
                if (currentTeamMember == null)
                {
                    throw new InvalidDataException("Sorry, you are not an employee of this Organization.");
                }
                MemberAchievement memberAchievement = new MemberAchievement();
                memberAchievement.TeamMember = currentTeamMember;
                memberAchievement.Year = mavm.Year;
                memberAchievement.Month = mavm.Month;
                memberAchievement.TotalWorkingHour = mavm.TotalWorkingHour;
                memberAchievement.TotalCompletePoint = mavm.TotalCompletePoint;
                memberAchievement.CompleteTaskCount = mavm.CompleteTaskCount;
                memberAchievement.RunningTaskCount = mavm.RunningTaskCount;
                if (mavm.MemberAchievementTaskTypeViewModel != null && mavm.MemberAchievementTaskTypeViewModel.Any())
                {
                    DateTime dateTime = DateTime.Now;
                    foreach (MemberAchievementTaskTypeViewModel taskTypeViewModel in mavm.MemberAchievementTaskTypeViewModel)
                    {
                        MemberAchievementDetails tempMemberAchievementDetails = new MemberAchievementDetails();
                        tempMemberAchievementDetails.MemberAchievement = memberAchievement;
                        tempMemberAchievementDetails.TaskType = taskTypeViewModel.TaskType;
                        tempMemberAchievementDetails.TaskName = taskTypeViewModel.Task;
                        tempMemberAchievementDetails.Status = MemberAchievementDetails.EntityStatus.Active;
                        tempMemberAchievementDetails.CreationDate = dateTime;
                        tempMemberAchievementDetails.ModificationDate = dateTime;
                        tempMemberAchievementDetails.CreateBy = currentUserId;
                        tempMemberAchievementDetails.ModifyBy = currentUserId;
                        memberAchievement.MemberAchievementDetails.Add(tempMemberAchievementDetails);
                    }
                }
                _memberAchievementService.SaveMemberAchievement(memberAchievement);
                message = "Successfully Save";
                successState = true;
                TempData["SuccessMessageMA"] = message;
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        public ActionResult Edit(long id)
        {
            MemberAchievementViewModel maViewModel = new MemberAchievementViewModel(); 
            ViewBag.MonthList = new SelectList(_commonHelper.LoadEmumToDictionary<MonthsOfYear>(), "Key", "Value");
            try
            {
                TeamMember currentTeamMember = _teamMemberService.GetCurrentMember();
                if (currentTeamMember == null)
                {
                    RedirectToAction("Index");
                }
                //long currentUserId = _userService.GetCurrentUserId();
                MemberAchievement memberAchievement = _memberAchievementService.GetMemberAchievement(id);

                if (memberAchievement == null)
                    throw new InvalidDataException("No Acievement entry found!");
                TeamMember teamMember = memberAchievement.TeamMember;
                if (teamMember == null)
                    throw new InvalidDataException("Invalid Acievement entry found!");
                if (memberAchievement.TeamMember.Id != currentTeamMember.Id)
                    throw new InvalidDataException("This is not your Acievement!");
                maViewModel = new MemberAchievementViewModel
                {
                    Id = memberAchievement.Id,
                    TeamMemberId = teamMember.Id,
                    Year = memberAchievement.Year,
                    Month = memberAchievement.Month,
                    TotalWorkingHour = memberAchievement.TotalWorkingHour,
                    TotalCompletePoint = memberAchievement.TotalCompletePoint,
                    CompleteTaskCount = memberAchievement.CompleteTaskCount,
                    RunningTaskCount = memberAchievement.RunningTaskCount,
                };
                List<MemberAchievementDetails> maDetailsList = new List<MemberAchievementDetails>();
                if (memberAchievement.MemberAchievementDetails != null)
                {
                    maDetailsList = memberAchievement.MemberAchievementDetails.Where(x => x.Status == MemberAchievementDetails.EntityStatus.Active).ToList();
                }
                if (maDetailsList.Any())
                {
                    foreach (MemberAchievementDetails details in maDetailsList)
                    {
                        MemberAchievementTaskTypeViewModel tempMaTaskTypeViewModel = new MemberAchievementTaskTypeViewModel();
                        tempMaTaskTypeViewModel.Id = details.Id;
                        tempMaTaskTypeViewModel.TaskType = details.TaskType;
                        tempMaTaskTypeViewModel.Task = details.TaskName;
                        maViewModel.MemberAchievementTaskTypeViewModel.Add(tempMaTaskTypeViewModel);
                    }
                }
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View(maViewModel);
        }

        [HttpPost]
        public JsonResult Edit(MemberAchievementViewModel memberAchievementViewModel)
        {
            string message = "";
            bool successState = false;
            try
            {
                MemberAchievementViewModel mavm = memberAchievementViewModel;
                TeamMember currentTeamMember = _teamMemberService.GetCurrentMember();
                long currentUserId = _userService.GetCurrentUserId();
                if (currentTeamMember == null)
                {
                    throw new InvalidDataException("Sorry, you are not an employee of this Organization.");
                }
                MemberAchievement memberAchievement = new MemberAchievement();
                memberAchievement.Id = memberAchievementViewModel.Id;
                memberAchievement.TeamMember = currentTeamMember;
                memberAchievement.Year = mavm.Year;
                memberAchievement.Month = mavm.Month;
                memberAchievement.TotalWorkingHour = mavm.TotalWorkingHour;
                memberAchievement.TotalCompletePoint = mavm.TotalCompletePoint;
                memberAchievement.CompleteTaskCount = mavm.CompleteTaskCount;
                memberAchievement.RunningTaskCount = mavm.RunningTaskCount;
                if (mavm.MemberAchievementTaskTypeViewModel != null && mavm.MemberAchievementTaskTypeViewModel.Any())
                {
                    DateTime dateTime = DateTime.Now;
                    foreach (MemberAchievementTaskTypeViewModel taskTypeViewModel in mavm.MemberAchievementTaskTypeViewModel)
                    {
                        MemberAchievementDetails tempMemberAchievementDetails = new MemberAchievementDetails();
                        tempMemberAchievementDetails.MemberAchievement = memberAchievement;
                        tempMemberAchievementDetails.Id = taskTypeViewModel.Id;
                        tempMemberAchievementDetails.TaskType = taskTypeViewModel.TaskType;
                        tempMemberAchievementDetails.TaskName = taskTypeViewModel.Task;
                        tempMemberAchievementDetails.Status = MemberAchievementDetails.EntityStatus.Active;
                        tempMemberAchievementDetails.CreationDate = dateTime;
                        tempMemberAchievementDetails.ModificationDate = dateTime;
                        tempMemberAchievementDetails.CreateBy = currentUserId;
                        tempMemberAchievementDetails.ModifyBy = currentUserId;
                        memberAchievement.MemberAchievementDetails.Add(tempMemberAchievementDetails);
                    }
                }
                _memberAchievementService.UpdateMemberAchievement(memberAchievement);
                message = "Successfully updated.";
                successState = true;
                TempData["SuccessMessageMA"] = message;
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            string message = "";
            bool successState = false;
            try
            {
                _memberAchievementService.DeleteMemberAchievement(id);
                message = "Successfully Deleted";
                successState = true;
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { IsSuccess = successState, Message = message });
        }

        #endregion

        #region Others function

        #endregion

        #region Helper function
        #endregion

    }
}
