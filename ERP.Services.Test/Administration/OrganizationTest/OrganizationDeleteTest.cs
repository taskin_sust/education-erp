﻿using System;
using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.OrganizationTest
{
    public interface IOrganizationDeleteTest
    {
        #region Basic Test
        [Fact]
        void Should_Delete_Successfully();

        [Fact]
        void Delete_Should_Return_Null_Exception();
        #endregion

        #region Business Logic Test
        [Theory]
        [MemberData("OrganizationIndex", MemberType = typeof(OrganizationDeleteTest))]
        void Delete_Should_Throw_Dependency_Exception(int i);
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Organization")]
    public class OrganizationDeleteTest : OrganizationBaseTest, IOrganizationDeleteTest
    {
        #region Test Related Properties
        public static IEnumerable<object[]> OrganizationIndex
        {
            get
            {
                var indexList = new List<object[]>();
                for (int i = 0; i < 7; i++)
                    indexList.Add(new object[] { i });
                return indexList;
            }
        }
        #endregion

        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            OrganizationFactory organizationFactory =
                _organizationFactory.Create()
                    .WithNameAndRank("", 1)
                    .WithRequiredProperties(_name, _shortName, _website, _email, _contactNo, _logoImageUrl,
                        _reportImageUrl, _idFooterImageUrl).Persist();
            Organization organization = _organizationService.LoadById(organizationFactory.Object.Id);
            var success = _organizationService.Delete(organization.Id);
            Assert.True(success);
            organization = _organizationService.LoadById(organizationFactory.Object.Id);
            Assert.Null(organization);

        }

        [Fact]
        public void Delete_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _organizationService.Delete(0));
        }

        #endregion

        #region Business Logic Test

        [Theory]
        [MemberData("OrganizationIndex", MemberType = typeof(OrganizationDeleteTest))]
        public void Delete_Should_Throw_Dependency_Exception(int i)
        {
            OrganizationFactory organizationFactory =
                 _organizationFactory.Create()
                     .WithNameAndRank("", 1)
                     .WithRequiredProperties(_name, _shortName, _website, _email, _contactNo, _logoImageUrl,
                         _reportImageUrl, _idFooterImageUrl).Persist();
            Organization organization = _organizationService.LoadById(organizationFactory.Object.Id);
            if (i == 0)
            {
                organization.Programs.Add(new Program());
            }
            if (i == 1)
            {
                organization.Branches.Add(new Branch());
            }
            if (i == 2)
            {
                organization.Teachers.Add(new Teacher());
            }
            if (i == 3)
            {
                organization.ExtraCurricularActivities.Add(new ExtraCurricularActivity());
            }
            if (i == 4)
            {
                organization.SmsMasks.Add(new SmsMask());
            }
            if (i == 5)
            {
                organization.YearlyHolidays.Add(new YearlyHoliday());
            }
            if (i == 6)
            {
                organization.MemberLeaveSummaries.Add(new MemberLeaveSummary());
            }
            Assert.Throws<DependencyException>(() => _organizationService.Delete(organization.Id));
        }

        #endregion
    }
}
