﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IStudentInfoHistoryService :IBaseService
    {

        void Save(IList<StudentInfoHistory> studentInfoHistory);
    }
    public class StudentInfoHistoryService : BaseService, IStudentInfoHistoryService
    {
        private readonly IStudentInfoHistoryDao _studentInfoHistoryDao;
        public StudentInfoHistoryService()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _studentInfoHistoryDao= new StudentInfoHistoryDao(){Session = session};

        }

        public void Save(IList<StudentInfoHistory> studentInfoHistory)
        {
            try
            {
                foreach (var infoHistory in studentInfoHistory)
                {
                    _studentInfoHistoryDao.Save(infoHistory);
                }
            }
            catch (Exception e)
            {
                
                throw;
            }
           
        }
    }
}
