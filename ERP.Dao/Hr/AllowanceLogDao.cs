﻿using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IAllowanceLogDao : IBaseDao<AllowanceLog, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
    public class AllowanceLogDao : BaseDao<AllowanceLog, long>, IAllowanceLogDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion
    }

}
