﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{
    public interface ILectureSettingService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        //IList<LectureSettings> GetLectureSettingsList(int start, int length, string orderBy, string orderDir, string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix);
        IList<LectureSettings> GetLectureSettingsList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix);
        LectureSettings LoadById(long id);
        #endregion

        #region Others Function
        //int LectureSettingsRowCount(string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix);
        int LectureSettingsRowCount(List<UserMenu> userMenu, string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix);

        #endregion
    }
    public class LectureSettingService : BaseService, ILectureSettingService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ILectureSettingsDao _lectureSettingsDao;
        private readonly ICommonHelper _commonHelper;
        public LectureSettingService(ISession session)
        {
            Session = session;
            _lectureSettingsDao = new LectureSettingsDao() { Session = Session };
            _commonHelper = new CommonHelper();
       

        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public LectureSettings LoadById(long id)
        {
            LectureSettings lectureSettings;
            try
            {
                lectureSettings = _lectureSettingsDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return lectureSettings;
        }
        //public IList<LectureSettings> GetLectureSettingsList(int start, int length, string orderBy, string orderDir, string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix)
        public IList<LectureSettings> GetLectureSettingsList(int start, int length, string orderBy, string orderDir, List<UserMenu> userMenu, string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix)
        {
            IList<LectureSettings> lectureSettingses;
            try
            {
                //lectureSettingses = _lectureSettingsDao.GetLectureSettingsList(start, length, orderBy, orderDir, organization, program, session, course, numberOfClasses, classNamePrefix);
                long? convertedOrganizationId = String.IsNullOrEmpty(organization) ? (long?)null : Convert.ToInt32(organization);
                long? convertedProgramId = String.IsNullOrEmpty(program) ? (long?)null : Convert.ToInt32(program);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (convertedProgramId != null) ? _commonHelper.ConvertIdToList((long)convertedProgramId) : null);
                //lectureSettingses = _lectureSettingsDao.GetLectureSettingsList(start, length, orderBy, orderDir, organization, program, session, course, numberOfClasses, classNamePrefix);
                lectureSettingses = _lectureSettingsDao.GetLectureSettingsList(start, length, orderBy, orderDir, organizationIdList, programIdList, session, course, numberOfClasses, classNamePrefix);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return lectureSettingses;
        }
        #endregion

        #region Others Function
        //public int LectureSettingsRowCount(string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix)
        public int LectureSettingsRowCount(List<UserMenu> userMenu, string organization, string program, string session, string course, string numberOfClasses, string classNamePrefix)
        {
            int lectureSettingCount;
            try
            {
                long? convertedOrganizationId = String.IsNullOrEmpty(organization) ? (long?)null : Convert.ToInt64(organization);
                long? convertedProgramId = String.IsNullOrEmpty(program) ? (long?)null : Convert.ToInt64(program);
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenu, (convertedOrganizationId != null) ? _commonHelper.ConvertIdToList((long)convertedOrganizationId) : null);
                List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu, organizationIdList, null, (convertedProgramId != null) ? _commonHelper.ConvertIdToList((long)convertedProgramId) : null);

                lectureSettingCount = _lectureSettingsDao.LectureSettingsRowCount(organizationIdList, programIdList, session, course, numberOfClasses, classNamePrefix);
                //lectureSettingCount = _lectureSettingsDao.LectureSettingsRowCount(organization, program, session, course, numberOfClasses, classNamePrefix);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return lectureSettingCount;
        }
        #endregion

        #region Helper Function

        #endregion
    }
}
