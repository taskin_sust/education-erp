﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Dao.Sms;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface ISmsSenderService : IBaseService
    {

        void Save(IList<BusinessModel.Entity.Sms.SmsSender> smsSenders);

        IList<SmsSender> LoadPendingSms();

        void Update(IList<SmsSender> smsSenders);
    }
    public class SmsSenderService : BaseService, ISmsSenderService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        private readonly ISmsSenderDao _smsSenderDao;
        //private ISession Session;
        public SmsSenderService()
        {
            var session = NHibernateSessionFactory.OpenSession();
            Session = session;
            _smsSenderDao = new SmsSenderDao() { Session = session };
        }

        public void Save(IList<SmsSender> smsSenders)
        {
            //using (var trans = Session.BeginTransaction())
            //{
            try
            {
                foreach (SmsSender smsSenderObj in smsSenders)
                {
                    _smsSenderDao.Save(smsSenderObj);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }

            //  trans.Commit();
            //}
        }

        public IList<SmsSender> LoadPendingSms()
        {
            return _smsSenderDao.LoadPendingSms();
        }

        public void Update(IList<SmsSender> smsSenders)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    foreach (SmsSender smsSenderObj in smsSenders)
                    {
                        smsSenderObj.Status = SmsSender.EntityStatus.Inactive;
                        _smsSenderDao.Update(smsSenderObj);
                    }
                    trans.Commit();
                }

            }
            catch (Exception e)
            {
                _logger.Error(e);

            }
        }
    }
}
