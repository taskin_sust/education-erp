﻿using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentAcademicInfoBaseDataMap : ClassMap<StudentAcademicInfoBaseData>
    {
        public StudentAcademicInfoBaseDataMap()
        {
            Table("StudentAcademicInfoBaseData");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.CreationDate).Column("CreationDate");
            Map(x => x.RollNo).Column("RollNo");
            Map(x => x.Board).Column("Board");
            Map(x => x.ExamGroup).Column("ExamGroup");
            Map(x => x.Session).Column("Session");
            Map(x => x.StudentType).Column("StudentType");
            Map(x => x.Result).Column("Result");
            Map(x => x.Gpa).Column("Gpa");
            Map(x => x.Name).Column("Name");
            Map(x => x.FathersName).Column("FathersName");
            Map(x => x.MothersName).Column("MothersName");
            Map(x => x.RegistrationNo).Column("RegistrationNo");
            Map(x => x.DateOfBirth).Column("DateOfBirth");
            Map(x => x.Institute).Column("Institute");

            References(x => x.StudentAcademicInfo).Column("StudentAcademicInfoId");

            HasMany(x => x.StudentAcademicInfoSubjectResult).KeyColumn("StudentAcademicInfoBaseDataId").Cascade.AllDeleteOrphan().Inverse();

        }
    }
}
