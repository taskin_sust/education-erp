﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Administration.BankBranchTest
{
    public interface IBankBranchUpdateTest
    {
        #region Basic test
        [Fact]
        void Update_Should_Return_Null_Exception();
        [Fact]
        void Should_Update_Successfully();

        #endregion

        #region Business logic test
        [Fact]
        void Should_Not_Update_Duplicate_Routing_No();
        [Fact]
        void Should_Not_Update_Duplicate_Swift_Code();
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "BankBranch")]
    public class BankBranchUpdateTest : BankBranchBaseTest, IBankBranchUpdateTest
    {
        #region Basic Test
        [Fact]
        public void Update_Should_Return_Null_Exception()
        {
            var bankBranchFactory = _bankBranchFactory.Create().WithBank().Persist();
            Assert.Throws<NullObjectException>(() => _bankBranchService.IsUpdate(bankBranchFactory.Object.Id, null));

        }
        [Fact]
        public void Should_Update_Successfully()
        {
            var bankBranchFactory = _bankBranchFactory.Create().WithBank().Persist();
            BankBranch bankBranchObj = _bankBranchService.GetBankBranch(bankBranchFactory.Object.Id);
            bankBranchObj.RoutingNo = bankBranchObj.RoutingNo + "123 HI";
            bankBranchObj.SwiftCode = bankBranchObj.SwiftCode + "456 HI";
            var success = _bankBranchService.IsUpdate(bankBranchFactory.Object.Id, bankBranchObj);
            Assert.True(success);
        } 
        #endregion

        #region Business Logic Test
        [Fact]
        public void Should_Not_Update_Duplicate_Routing_No()
        {
            _bankBranchFactory.CreateMore(2).Persist();
            var bankBranch1 = _bankBranchFactory.GetLastCreatedObjectList()[0];
            var bankBranch2 = _bankBranchFactory.GetLastCreatedObjectList()[1];
            bankBranch2.RoutingNo = bankBranch1.RoutingNo;
            Assert.Throws<DuplicateEntryException>(() => _bankBranchService.IsUpdate(bankBranch2.Id, bankBranch2));
        }
        [Fact]
        public void Should_Not_Update_Duplicate_Swift_Code()
        {
            _bankBranchFactory.CreateMore(2).Persist();
            var bankBranch1 = _bankBranchFactory.GetLastCreatedObjectList()[0];
            var bankBranch2 = _bankBranchFactory.GetLastCreatedObjectList()[1];
            bankBranch2.SwiftCode = bankBranch1.SwiftCode;
            Assert.Throws<DuplicateEntryException>(() => _bankBranchService.IsUpdate(bankBranch2.Id, bankBranch2));
        } 
        #endregion
    }
}
