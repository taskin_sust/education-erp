using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr
{
    public partial class DesignationMap : BaseClassMap<Designation,long> {
        
        public DesignationMap() {
			Table("HR_Designation");
			LazyLoad();
			References(x => x.Organization).Column("OrganizationId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.ShortName).Column("ShortName");
			HasMany(x => x.EmploymentHistory).KeyColumn("DesignationId");
			HasMany(x => x.EmploymentHistoryLog).KeyColumn("DesignationId");
			HasMany(x => x.MentorHistory).KeyColumn("MentorDesignationId");
			HasMany(x => x.MentorHistoryLog).KeyColumn("MemberDesignationId");
            HasMany(x => x.FestivalBonusSheetsSalary).KeyColumn("SalaryDesignationId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.FestivalBonusSheetsJob).KeyColumn("JobDesignationId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.SalaryHistories).KeyColumn("SalaryDesignationId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
