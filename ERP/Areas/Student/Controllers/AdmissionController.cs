﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using NHibernate.Linq;
using NHibernate.Util;
using UdvashERP.App_code;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.StudentModuleView;
using UdvashERP.Services.Administration;
using UdvashERP.App_Start;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using System.Net;
using System.Text;
using HtmlAgilityPack;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Student;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class AdmissionController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        const string DropDownOptionView = "Partial/_DropDownOptions";
        const string CourseDetailsView = "Partial/_CourseSubjectCheckView";
        private readonly IAdmissionService _admissionService;
        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly IBatchService _batchService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly ICourseService _courseService;
        private readonly IStudentService _studentService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IStudentPaymentService _studentPaymentService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly ICommonHelper _commonHelper;
        private readonly IReferenceService _referenceService;
        private readonly IDiscountService _discountService;
        private readonly ICourseSubjectService _courseSubjectService;
        private readonly IStudentCourseDetailsService _studentCourseDetailsService;
        private readonly IStudentVisitedService _sudentVisitedService;
        private readonly IStudentInfoUpdateLogService _studentInfoUpdateLog;
        private readonly IDiscountDetailService _discountDetailService;
        private readonly IStudentBoardService _studentBoardService;
        private readonly IStudentExamService _studentExamService;
        private readonly IStudentAcademicInfoService _studentAcademicInfoService;
        public AdmissionController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _admissionService = new AdmissionService(session);
                _sessionService = new SessionService(session);
                _programService = new ProgramService(session);
                _branchService = new BranchService(session);
                _campusService = new CampusService(session);
                _batchService = new BatchService(session);
                _courseService = new CourseService(session);
                _studentService = new StudentService(session);
                _studentProgramService = new StudentProgramService(session);
                _studentPaymentService = new StudentPaymentService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
                _commonHelper = new CommonHelper();
                _referenceService = new ReferenceService(session);
                _discountService = new DiscountService(session);
                _courseSubjectService = new CourseSubjectService(session);
                _studentCourseDetailsService = new StudentCourseDetailsService(session);
                _sudentVisitedService = new StudentVisitedService(session);
                _discountDetailService = new DiscountDetailService(session);
                _studentInfoUpdateLog = new StudentInfoUpdateLogService(session);
                _studentBoardService = new StudentBoardService(session);
                _studentExamService = new StudentExamService(session);
                _studentAcademicInfoService = new StudentAcademicInfoService(session);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        #endregion

        #region Operational Function
        public ActionResult Index()
        {
            return RedirectToAction("NewAdmission");
        }

        [HttpGet]
        public ActionResult NewAdmission(string message = "")
        {
            if (!String.IsNullOrEmpty(message))
            {
                ViewBag.ErrorMessage = message;
            }
            return View();
        }

        [HttpGet]
        public ActionResult ProgramSummary(string stdRollOrStdProRoll)
        {
            SetViewData();
            ViewBag.StudentRoll = stdRollOrStdProRoll;

            StudentViewModel model = new StudentViewModel();
            //IList<StudentProgram> foundStudentPrograms = new List<StudentProgram>();

            if (String.IsNullOrEmpty(stdRollOrStdProRoll))
                return RedirectToAction("NewAdmission", "Admission", new { message = "You need to input Program Roll / Mobile number" });

            stdRollOrStdProRoll = stdRollOrStdProRoll.Trim();
            if (stdRollOrStdProRoll.Length == 11 || stdRollOrStdProRoll.Length == 7)
            {
                IList<ProgramAttendedSummary> programSummary = _studentProgramService.LoadProgramSummaryData(stdRollOrStdProRoll);
                if (programSummary == null || programSummary.Count == 0)
                    return RedirectToAction("NewAdmission", "Admission", new { message = "Program Roll / Mobile Number is not matching with any student" });
                model = SetProgramSummaryFirstData(programSummary);

                return View("StudentAdmission", model);
            }
            //if (stdRollOrStdProRoll.Length == 11)
            //{
            //    //foundStudentPrograms = _studentProgramService.LoadStudentProgram(stdRollOrStdProRoll);
            //    foreach(var a in _studentProgramService.LoadStudentProgram(stdRollOrStdProRoll).Select(s=>s.Student.RegistrationNo).Distinct()){
            //        foreach (var p in _studentService.GetStudentByRollNumber(a).StudentPrograms.ToList())
            //        {
            //            foundStudentPrograms.Add(p);
            //        }
            //    }

            //    var visitedStudents = _sudentVisitedService.LoadByMobile(stdRollOrStdProRoll);

            //    if ((foundStudentPrograms == null || foundStudentPrograms.Count == 0) && (visitedStudents == null || visitedStudents.Count == 0))
            //        return RedirectToAction("NewAdmission", "Admission", new { message = "Program Roll / Mobile Number is not matching with any student" });
            //    foundStudentPrograms = foundStudentPrograms.DistinctBy(x => x.PrnNo).OrderBy(x => x.Student.RegistrationNo).ThenBy(x => x.PrnNo).ToList();
            //    //var studentObj = _studentService.GetStudentByRollNumber(foundStudentPrograms.FirstOrDefault().Student.RegistrationNo);

            //    var programSummary = SetProgramSummaryData(foundStudentPrograms, visitedStudents);
            //    //var programSummary = SetProgramSummaryData(studentObj, visitedStudents);
            //    model = SetProgramSummaryFirstData(programSummary);

            //    return View("StudentAdmission", model);
            //}
            //else if (stdRollOrStdProRoll.Length == 7)
            //{
            //    var studentObj = _studentService.GetStudentByRollNumber(stdRollOrStdProRoll);
            //    if (studentObj == null || studentObj.Id < 1)
            //        return RedirectToAction("NewAdmission", "Admission", new { message = "Student Registration number is not matching with any student" });

            //    var programSummary = SetProgramSummaryData(studentObj);
            //    model = SetProgramSummaryFirstData(programSummary);

            //    return View("StudentAdmission", model);
            //}
            else
            {
                ViewBag.ErrorMessage = "Student or Program Roll / Mobile Number is not valid. It will be exact 7 or 11 digit long";
                return View("NewAdmission");
            }
        }

        private StudentViewModel SetProgramSummaryFirstData(IList<ProgramAttendedSummary> programAttendedSummaries)
        {
            var model = new StudentViewModel();
            ViewBag.PROGRAMSUMMARY = programAttendedSummaries;
            var firstProgramAttendedSummary = programAttendedSummaries.FirstOrDefault();

            if (firstProgramAttendedSummary != null)
            {
                firstProgramAttendedSummary.IsSelected = true;

                if (firstProgramAttendedSummary.StudentId != 0)
                {
                    ViewData["stdId"] = firstProgramAttendedSummary.StudentId;
                    ViewBag.Name = firstProgramAttendedSummary.NickName;
                    ViewBag.Mobile = firstProgramAttendedSummary.MobileNumber;
                    ViewBag.SelectedStudentVisitId = 0;

                    model = new StudentViewModel() { Id = firstProgramAttendedSummary.StudentId, Name = firstProgramAttendedSummary.NickName, MobNumber = firstProgramAttendedSummary.MobileNumber };

                }
                else if (firstProgramAttendedSummary.StudentVisitedId != 0)
                {
                    ViewData["stdId"] = 0;
                    ViewBag.Name = firstProgramAttendedSummary.NickName;
                    ViewBag.Mobile = firstProgramAttendedSummary.MobileNumber;
                    ViewBag.SelectedStudentVisitId = firstProgramAttendedSummary.StudentVisitedId;
                    model = new StudentViewModel() { Id = 0, Name = firstProgramAttendedSummary.NickName, MobNumber = firstProgramAttendedSummary.MobileNumber };
                }
            }
            else
            {
                ViewData["stdId"] = 0;
                ViewBag.SelectedStudentVisitId = 0;
            }

            return model;
        }

        private IList<ProgramAttendedSummary> SetProgramSummaryData(IList<StudentProgram> studentPrograms, IList<StudentVisited> visitedStudents = null)
        {
            IList<ProgramAttendedSummary> programAttendedSummaries = BuildProgramAttendedSummery(studentPrograms, visitedStudents);
            return programAttendedSummaries;
        }

        private IList<ProgramAttendedSummary> SetProgramSummaryData(BusinessModel.Entity.Students.Student studentObj, IList<StudentVisited> visitedStudents = null)
        {
            IList<ProgramAttendedSummary> programAttendedSummaries = BuildProgramAttendedSummery(studentObj, visitedStudents);
            return programAttendedSummaries;
        }

        private IList<ProgramAttendedSummary> BuildProgramAttendedSummery(IList<StudentProgram> studentPrograms, IList<StudentVisited> visitedStudents)
        {
            BusinessModel.Entity.Students.Student studentObj = null;
            IList<ProgramAttendedSummary> programAttendedSummaries = BuildProgramAttendedSummery(studentObj, visitedStudents);

            foreach (var stdProgram in studentPrograms)
            {
                var programSummery = new ProgramAttendedSummary
                {
                    NickName = stdProgram.Student.NickName,
                    Organization = stdProgram.Program.Organization != null ? stdProgram.Program.Organization.ShortName : "",
                    ProgramName = stdProgram.Program.Name,
                    SessionName = stdProgram.Batch.Session.Name,
                    BranchName = stdProgram.Batch.Branch.Name,
                    MobileNumber = stdProgram.Student.Mobile,
                    RegistrationNo = stdProgram.Student.RegistrationNo,
                    PrNo = stdProgram.PrnNo,
                    StudentId = stdProgram.Student.Id,
                    StudentVisitedId = 0,
                    PaymentStatus = stdProgram.DueAmount != null ? stdProgram.DueAmount <= 0 ? "Payment Clear" : "Payment Due" : "Payment Due"
                };
                programAttendedSummaries.Add(programSummery);
            }

            return programAttendedSummaries;
        }

        private IList<ProgramAttendedSummary> BuildProgramAttendedSummery(BusinessModel.Entity.Students.Student studentObj, IList<StudentVisited> visitedStudents = null)
        {
            IList<ProgramAttendedSummary> programAttendedSummaries = new List<ProgramAttendedSummary>();

            if (visitedStudents != null)
            {
                foreach (StudentVisited studentVisited in visitedStudents)
                {
                    var programSummery = new ProgramAttendedSummary
                    {
                        NickName = studentVisited.NickName,
                        Organization =
                            studentVisited.InterestedOrganization == null
                                ? "-"
                                : studentVisited.InterestedOrganization.ShortName,
                        ProgramName = studentVisited.Class,
                        SessionName = studentVisited.Year,
                        BranchName = "NA",
                        MobileNumber = studentVisited.Mobile,
                        RegistrationNo = "NA",
                        PrNo = studentVisited.AdmittedStudentProgram != null ? studentVisited.AdmittedStudentProgram.PrnNo : studentVisited.MeritPosition,
                        PaymentStatus = studentVisited.IsAdmitted == false ? "Visited" : "Admitted",
                        StudentId = 0,
                        StudentVisitedId = studentVisited.Id
                    };
                    programAttendedSummaries.Add(programSummery);
                }
            }

            if (studentObj != null)
            {
                var programListAgainstThisStudentRollNumber = _studentProgramService.LoadStudentProgram(studentObj.Id); //TODO: This load is redandent. It is possible to pass this list from calling location.
                foreach (var stdProgram in programListAgainstThisStudentRollNumber)
                {
                    var programSummery = new ProgramAttendedSummary
                    {
                        NickName = stdProgram.Student.NickName,
                        Organization = stdProgram.Program.Organization != null ? stdProgram.Program.Organization.ShortName : "",
                        ProgramName = stdProgram.Program.Name,
                        SessionName = stdProgram.Batch.Session.Name,
                        BranchName = stdProgram.Batch.Branch.Name,
                        MobileNumber = stdProgram.Student.Mobile,
                        RegistrationNo = stdProgram.Student.RegistrationNo,
                        PrNo = stdProgram.PrnNo,
                        StudentId = studentObj.Id,
                        StudentVisitedId = 0,
                        //PaymentStatus = _studentPaymentService.LoadPaymentStatusByStudentProgram(stdProgram.Id)
                        PaymentStatus = stdProgram.DueAmount != null ? stdProgram.DueAmount <= 0 ? "Payment Clear" : "Payment Due" : "Payment Due"
                    };
                    programAttendedSummaries.Add(programSummery);
                }
            }

            return programAttendedSummaries;
        }

        [HttpPost]
        public ActionResult NewProgramRegistration(string studentJson, string studentId, string studentVisitedId, string boardInfos = "")
        {
            try
            {
                SetViewData();
                var serializer = new JavaScriptSerializer();
                var studentViewModel = serializer.Deserialize<StudentViewModel>(studentJson);
                if (!String.IsNullOrEmpty(studentId))
                {

                    if (studentId.Trim() == "0")
                    {
                        var student = RegisterNewStudent(studentViewModel);
                        if (student != null && student.Id > 0 && !string.IsNullOrEmpty(student.RegistrationNo))
                        {
                            studentId = student.Id.ToString();
                        }
                    }

                    string aspNetUserId = User.Identity.GetUserId();
                    long aspNetUserIdL = Convert.ToInt64(aspNetUserId);
                    long studentIdL = Convert.ToInt64(studentId.Trim());
                    long studentVisitedIdL = Convert.ToInt64(studentVisitedId.Trim());

                    var alreadyRegisteredStudentObj = _studentService.LoadById(studentIdL);

                    if (ValidateStudentInfo(studentViewModel) && alreadyRegisteredStudentObj != null)
                    {
                        if (!CheckStudentProgramRegistrationInASession(studentViewModel, studentIdL))
                        {
                            var studentCourseDetailList = new List<StudentCourseDetail>();
                            alreadyRegisteredStudentObj.NickName = WebHelper.MakeFirstCharUpper(studentViewModel.Name);
                            alreadyRegisteredStudentObj.Mobile = studentViewModel.MobNumber;
                            alreadyRegisteredStudentObj.Gender = studentViewModel.Gender;
                            alreadyRegisteredStudentObj.Religion = studentViewModel.Religion;
                            alreadyRegisteredStudentObj.ModifyBy = aspNetUserIdL;
                            alreadyRegisteredStudentObj.ModificationDate = DateTime.Now;
                            var studentProgramObj = new StudentProgram
                            {
                                Batch = _batchService.GetBatch(Convert.ToInt64(studentViewModel.Batch.Trim())),
                                Program = _programService.GetProgram(Convert.ToInt64(studentViewModel.Program.Trim())),
                                Student = alreadyRegisteredStudentObj,
                                VersionOfStudy = studentViewModel.VersionOfStudy,
                                CreateBy = aspNetUserIdL,
                                ModifyBy = aspNetUserIdL,
                                IsImage = false,
                                IsPolitical = false
                            };
                            //THIS IS HARD CODED FOR POLITICAL STUDENT CHECK
                            if (studentViewModel.StudentPayment.Referrer != null && studentViewModel.StudentPayment.Referrer.Code == "04")
                            {
                                studentProgramObj.IsPolitical = true;
                            }
                            if (studentViewModel.CourseViewModels != null && studentViewModel.CourseViewModels.Count > 0)
                            {
                                int count = studentViewModel.CourseViewModels.Count(x => x.IsTaken == true);
                                if (count == 0)
                                {
                                    SetProgramSummaryData(studentProgramObj.Student);
                                    ViewBag.ErrorMessage = "Please select atleast one course";
                                    return View("ProgramSummary", studentViewModel);
                                }
                            }
                            else
                            {
                                SetProgramSummaryData(studentProgramObj.Student);
                                ViewBag.ErrorMessage = "Please select atleast one course";
                                return View("ProgramSummary", studentViewModel);
                            }
                            foreach (var course in studentViewModel.CourseViewModels)
                            {
                                if (course.IsTaken)
                                {
                                    foreach (var courseSubject in course.SubjectViewModels)
                                    {
                                        if (courseSubject.IsTaken)
                                        {
                                            var studentCourseDetailObj = new StudentCourseDetail();
                                            var courseSubjectObj = _courseSubjectService.GetCourseSubjectByCourseSubjectId(Convert.ToInt64(courseSubject.Id));
                                            studentCourseDetailObj.CourseSubject = courseSubjectObj;
                                            studentCourseDetailObj.StudentProgram = studentProgramObj;
                                            studentCourseDetailObj.CourseId = courseSubjectObj.Course.Id;//Add Course Id
                                            studentCourseDetailObj.CreateBy = aspNetUserIdL;
                                            studentCourseDetailObj.ModifyBy = aspNetUserIdL;
                                            studentCourseDetailList.Add(studentCourseDetailObj);
                                        }
                                    }
                                }
                            }
                            bool isSuccess = _admissionService.IsSuccessfullyProgramRegistered(alreadyRegisteredStudentObj, studentProgramObj, studentCourseDetailList, studentVisitedIdL);
                            if (isSuccess)
                            {
                                try
                                {
                                    if (!string.IsNullOrEmpty(boardInfos))
                                    {
                                        var boardInfoList = serializer.Deserialize<List<BoardInfoView>>(boardInfos);
                                        if (boardInfoList != null && boardInfoList.Count > 0)
                                        {
                                            _studentAcademicInfoService.SaveOrUpdateStudentAcademicInfo(alreadyRegisteredStudentObj, boardInfoList);
                                            //foreach (var bi in boardInfoList)
                                            //{
                                            //    int year;
                                            //    int.TryParse(bi.Year, out year);
                                            //    if (bi.StudentExamId > 0 && year > 1900 && bi.BoardId > 0 && !string.IsNullOrEmpty(bi.BoardRoll))
                                            //    {
                                            //        var existingInfo = _studentAcademicInfoService.LoadByStudentId(alreadyRegisteredStudentObj.Id, bi.StudentExamId);

                                            //        if(existingInfo == null){
                                            //            var acaInfo = new StudentAcademicInfo();
                                            //            acaInfo.Student = alreadyRegisteredStudentObj;
                                            //            acaInfo.Year = bi.Year;
                                            //            acaInfo.BoradRoll = bi.BoardRoll;
                                            //            acaInfo.RegistrationNumber = bi.RegistrationNumber;
                                            //            acaInfo.StudentBoard = _studentBoardService.GetByBoardId(bi.BoardId);
                                            //            acaInfo.StudentExam = _studentExamService.LoadById(bi.StudentExamId);
                                            //            _studentAcademicInfoService.Save(acaInfo);
                                            //        }
                                            //        else{                                                         
                                            //            existingInfo.Year = bi.Year;
                                            //            existingInfo.BoradRoll = bi.BoardRoll;
                                            //            existingInfo.RegistrationNumber = bi.RegistrationNumber;
                                            //            existingInfo.StudentBoard = _studentBoardService.GetByBoardId(bi.BoardId);                                                        
                                            //            _studentAcademicInfoService.Update(existingInfo);
                                            //        }                                                    
                                            //    }
                                            //}
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error("Board info save error.", ex);
                                }

                                MoneyReceipt moneyReceipt;
                                StudentPayment studentPayment;
                                ResponseMessage responseMessage = PaymentPaidAndMoneyReceiptGeneration(studentViewModel, studentProgramObj, out moneyReceipt, out studentPayment);
                                if (string.IsNullOrEmpty(responseMessage.ErrorMessage))
                                {
                                    try
                                    {
                                        StudentInfoUpdateLog studentInfoUpdateLog = new StudentInfoUpdateLog();
                                        studentInfoUpdateLog.CreationDate = DateTime.Now;
                                        studentInfoUpdateLog.CreateBy = Convert.ToInt64(IdentityExtensions.GetUserId(User.Identity));

                                        studentInfoUpdateLog.NickName = studentProgramObj.Student.NickName;
                                        studentInfoUpdateLog.Mobile = studentProgramObj.Student.Mobile;
                                        studentInfoUpdateLog.Gender = studentProgramObj.Student.Gender;
                                        studentInfoUpdateLog.Religion = studentProgramObj.Student.Religion;
                                        studentInfoUpdateLog.FullName = studentProgramObj.Student.FullName;
                                        studentInfoUpdateLog.FatherName = studentProgramObj.Student.FatherName;
                                        studentInfoUpdateLog.DateOfBirth = studentProgramObj.Student.DateOfBirth;
                                        studentInfoUpdateLog.GuardiansMobile1 = studentProgramObj.Student.GuardiansMobile1;
                                        studentInfoUpdateLog.GuardiansMobile2 = studentProgramObj.Student.GuardiansMobile2;
                                        studentInfoUpdateLog.BloodGroup = studentProgramObj.Student.BloodGroup;
                                        studentInfoUpdateLog.Email = studentProgramObj.Student.Email;//studentObj.Email;
                                        studentInfoUpdateLog.LastMeritPosition = studentProgramObj.Student.LastMeritPosition;
                                        studentInfoUpdateLog.Gpa = studentProgramObj.Student.Gpa;
                                        studentInfoUpdateLog.Exam = studentProgramObj.Student.Exam;
                                        studentInfoUpdateLog.Section = studentProgramObj.Student.Section;
                                        studentInfoUpdateLog.StudentProgramId = studentProgramObj.Id;
                                        studentInfoUpdateLog.RegistrationNo = studentProgramObj.Student.RegistrationNo;
                                        if (studentProgramObj.Student.District != null)
                                            studentInfoUpdateLog.District = Convert.ToInt32(studentProgramObj.Student.District.Id);

                                        //NEW FIELDS
                                        studentInfoUpdateLog.StudentId = studentProgramObj.Student.Id;
                                        if (studentProgramObj.Institute != null)
                                            studentInfoUpdateLog.Institute = studentProgramObj.Institute.Name;
                                        studentInfoUpdateLog.InfoStatus = 1; //FOR UPDATE = 2, FOR Admission = 1

                                        _studentInfoUpdateLog.SaveLog(studentInfoUpdateLog);
                                    }
                                    catch (Exception ex) { _logger.Error(ex); }
                                    bool flag = SendSmsApi.GenarateInstantSmsAndNumber(1,studentProgramObj.Program.Organization.Id, studentProgramObj.Program.Id,
                                        studentProgramObj.Batch.Session.Id, studentProgramObj.Student.Id,
                                        studentProgramObj.Id);

                                    return Json(new { IsSuccess = true, PaymentId = studentPayment.Id });
                                }
                                else
                                {
                                    return Json(new Response(false, "Student PRN: " + studentProgramObj.PrnNo + " , Name: " + studentViewModel.Name + " Admitted successfully. But payment is failed."));
                                }
                            }
                        }
                        else
                        {
                            return Json(new Response(false, "Student " + studentViewModel.Name + " already registerd for this program in the same session."));
                        }
                    }
                    else
                    {
                        return Json(new Response(false, "Student not found or Invalid data has been provided or new student creation failed."));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                _logger.Error("Student Id: " + studentId);
                return Json(new Response(false, "Student Admission Failed. Possible cause: " + ex.Message + ". Please try again."));
            }
            return Json(new Response(false, "Student Admission Failed. Please try again"));
        }

        private BusinessModel.Entity.Students.Student RegisterNewStudent(StudentViewModel studentViewModel)
        {
            string aspNetUserId = User.Identity.GetUserId();
            long aspNetUserIdL = Convert.ToInt64(aspNetUserId);

            BusinessModel.Entity.Students.Student student = new BusinessModel.Entity.Students.Student()
            {
                Gender = studentViewModel.Gender,
                Mobile = studentViewModel.MobNumber,
                NickName = studentViewModel.Name,
                Religion = studentViewModel.Religion,
                CreateBy = aspNetUserIdL,
                ModifyBy = aspNetUserIdL,
                Status = BusinessModel.Entity.Students.Student.EntityStatus.Active,
            };

            return _admissionService.RegisteredStudent(student);
        }

        [HttpGet]
        public ActionResult NewStudentAdmission()
        {
            try
            {
                SetViewData();
                return View(new StudentViewModel());
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return HttpNotFound();
            }
        }
        [HttpPost]
        public JsonResult NewStudentAdmission(string studentObj, string boardInfos = "")
        {
            string logMobileNumber = "";
            try
            {
                SetViewData();

                var serializer = new JavaScriptSerializer();
                var studentViewModel = serializer.Deserialize<StudentViewModel>(studentObj);

                if (ValidateStudentInfo(studentViewModel))
                {
                    string aspNetUserId = User.Identity.GetUserId();
                    long aspNetUserIdL = Convert.ToInt64(aspNetUserId);
                    var studentCourseDetailList = new List<StudentCourseDetail>();
                    logMobileNumber = studentViewModel.MobNumber;
                    var student = new BusinessModel.Entity.Students.Student
                    {
                        NickName = WebHelper.MakeFirstCharUpper(studentViewModel.Name),
                        Mobile = studentViewModel.MobNumber,
                        CreateBy = aspNetUserIdL,
                        ModifyBy = aspNetUserIdL,
                        Status = BusinessModel.Entity.Students.Student.EntityStatus.Active,
                        Religion = studentViewModel.Religion,
                        Gender = studentViewModel.Gender,
                        IsJsc = false,
                        IsSsc = false,
                        IsHsc = false
                    };
                    var studentProgramObj = new StudentProgram
                    {
                        VersionOfStudy = studentViewModel.VersionOfStudy,
                        Batch = _batchService.GetBatch(Convert.ToInt64(studentViewModel.Batch.Trim())),
                        Program = _programService.GetProgram(Convert.ToInt64(studentViewModel.Program.Trim())),
                        Student = student,
                        CreateBy = aspNetUserIdL,
                        ModifyBy = aspNetUserIdL,
                        IsImage = false,
                        IsPolitical = false
                    };
                    if (studentViewModel.CourseViewModels == null || studentViewModel.CourseViewModels.Count(x => x.IsTaken == true) == 0)
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            ErrorMessage = "Please select atleast one course",
                        });
                    }
                    decimal totalMinPayableAmountForAdmission = 0;
                    foreach (var course in studentViewModel.CourseViewModels)
                    {
                        decimal percentMin = 0;
                        if (course.IsTaken)
                        {
                            decimal subcost = 0;
                            foreach (var courseSubject in course.SubjectViewModels)
                            {

                                if (courseSubject.IsTaken)
                                {
                                    var studentCourseDetailObj = new StudentCourseDetail();
                                    var courseSubjectObj = _courseSubjectService.GetCourseSubjectByCourseSubjectId(Convert.ToInt64(courseSubject.Id));
                                    subcost += courseSubjectObj.Payment;
                                    percentMin = courseSubjectObj.Course.OfficeMinPayment;
                                    studentCourseDetailObj.CourseSubject = courseSubjectObj;
                                    studentCourseDetailObj.StudentProgram = studentProgramObj;
                                    studentCourseDetailObj.CourseId = courseSubjectObj.Course.Id;//Add Course Id
                                    studentCourseDetailObj.CreateBy = aspNetUserIdL;
                                    studentCourseDetailObj.ModifyBy = aspNetUserIdL;
                                    studentCourseDetailList.Add(studentCourseDetailObj);
                                }
                            }
                            totalMinPayableAmountForAdmission += subcost * (percentMin / 100);
                        }
                    }
                    //bool isSuccess = _admissionService.IsSuccessfullyRegistered(student, studentProgramObj, studentCourseDetailList);
                    var responseMessage = new ResponseMessage();
                    //if (isSuccess)
                    //{
                    MoneyReceipt moneyReceipt;
                    StudentPayment studentPayment;
                    responseMessage = PaymentPaidAndMoneyReceiptGenerationAdmissionForFirstTimeOnly(studentViewModel, studentProgramObj, out moneyReceipt, out studentPayment);
                    studentProgramObj.StudentCourseDetails = studentCourseDetailList;
                    var sPayment = studentPayment;
                    //var sMoneyReceipt = moneyReceipt;
                    if (String.IsNullOrEmpty(responseMessage.ErrorMessage))
                    {
                        foreach (var cs in studentCourseDetailList)
                        {
                            sPayment.CourseSubjectList.Add(cs.CourseSubject);
                        }
                        decimal totalAmount = 0;
                        string refNote = "";

                        studentProgramObj.StudentCourseDetails.ForEach(x =>
                        {
                            totalAmount += x.CourseSubject.Payment;
                        });

                        var cd = studentProgramObj.StudentCourseDetails.ToList();
                        var cdGroup = cd.GroupBy(x => x.CourseSubject.Course, (key, group) => new { Key = key, Group = group.Distinct().ToList() });

                        foreach (var cg in cdGroup)
                        {
                            refNote += cg.Key.Name + " : ( ";
                            foreach (var cs in cg.Group)
                            {
                                refNote += cs.CourseSubject.Subject.ShortName + ", ";
                            }
                            if (!string.IsNullOrEmpty(refNote))
                            {
                                int lastIndex = refNote.LastIndexOf(",");
                                if (lastIndex >= 0)
                                    refNote = refNote.Remove(lastIndex);
                            }
                            refNote += " ), ";
                        }

                        if (!string.IsNullOrEmpty(refNote))
                        {
                            int lastIndex = refNote.LastIndexOf(",");
                            if (lastIndex >= 0)
                                refNote = refNote.Remove(lastIndex, 1);
                        }



                        studentPayment.CourseFees = totalAmount;
                        studentPayment.Remarks = refNote + " New Admitted.";
                        studentPayment.StudentProgram = studentProgramObj;

                        if (studentProgramObj.Program.Type == Program.ProgramTypeStatus.Paid) {

                            totalMinPayableAmountForAdmission = (decimal)((totalMinPayableAmountForAdmission *
                                                                        (studentPayment.CourseFees - studentPayment.DiscountAmount - studentPayment.SpDiscountAmount)) /
                                                                       studentPayment.CourseFees);

                            if (studentPayment.ReceivedAmount < totalMinPayableAmountForAdmission) { 
                                return Json(new { IsSuccess = false, ErrorMessage = "You must pay Min " + totalMinPayableAmountForAdmission.ToString("F") + " BDT for admission" }); 
                            }
                        
                        }

                        if (studentPayment.Referrer != null && studentPayment.Referrer.Code == "04")/*FOR PoLITICAL */
                        {
                            studentProgramObj.IsPolitical = true;
                        }

                        bool isSuccess = _admissionService.IsSuccessfullyRegisteredForNewAdmission(student, studentProgramObj, studentCourseDetailList, studentPayment);
                        if (isSuccess)
                        {
                            try
                            {
                                try
                                {
                                    StudentInfoUpdateLog studentInfoUpdateLog = new StudentInfoUpdateLog();
                                    studentInfoUpdateLog.CreationDate = DateTime.Now;
                                    studentInfoUpdateLog.CreateBy =
                                        Convert.ToInt64(IdentityExtensions.GetUserId(User.Identity));

                                    studentInfoUpdateLog.NickName = studentProgramObj.Student.NickName;
                                    studentInfoUpdateLog.Mobile = studentProgramObj.Student.Mobile;
                                    studentInfoUpdateLog.Gender = studentProgramObj.Student.Gender;
                                    studentInfoUpdateLog.Religion = studentProgramObj.Student.Religion;
                                    studentInfoUpdateLog.FullName = studentProgramObj.Student.FullName;
                                    studentInfoUpdateLog.FatherName = studentProgramObj.Student.FatherName;
                                    studentInfoUpdateLog.DateOfBirth = studentProgramObj.Student.DateOfBirth;
                                    studentInfoUpdateLog.GuardiansMobile1 = studentProgramObj.Student.GuardiansMobile1;
                                    studentInfoUpdateLog.GuardiansMobile2 = studentProgramObj.Student.GuardiansMobile2;
                                    studentInfoUpdateLog.BloodGroup = studentProgramObj.Student.BloodGroup;
                                    studentInfoUpdateLog.Email = studentProgramObj.Student.Email; //studentObj.Email;
                                    studentInfoUpdateLog.LastMeritPosition = studentProgramObj.Student.LastMeritPosition;
                                    studentInfoUpdateLog.Gpa = studentProgramObj.Student.Gpa;
                                    studentInfoUpdateLog.Exam = studentProgramObj.Student.Exam;
                                    studentInfoUpdateLog.Section = studentProgramObj.Student.Section;
                                    studentInfoUpdateLog.StudentProgramId = studentProgramObj.Id;
                                    studentInfoUpdateLog.RegistrationNo = studentProgramObj.Student.RegistrationNo;
                                    if (studentProgramObj.Student.District != null)
                                        studentInfoUpdateLog.District =
                                            Convert.ToInt32(studentProgramObj.Student.District.Id);

                                    //NEW FIELDS
                                    studentInfoUpdateLog.StudentId = studentProgramObj.Student.Id;
                                    if (studentProgramObj.Institute != null)
                                        studentInfoUpdateLog.Institute = studentProgramObj.Institute.Name;
                                    studentInfoUpdateLog.InfoStatus = 1; //FOR UPDATE = 2, FOR Admission = 1

                                    _studentInfoUpdateLog.SaveLog(studentInfoUpdateLog);
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(ex);
                                }

                                try
                                {
                                    if (!string.IsNullOrEmpty(boardInfos))
                                    {
                                        var boardInfoList = serializer.Deserialize<List<BoardInfoView>>(boardInfos);
                                        if (boardInfoList != null && boardInfoList.Count > 0)
                                        {
                                            foreach (var bi in boardInfoList)
                                            {
                                                try
                                                {
                                                    int year;
                                                    int.TryParse(bi.Year, out year);
                                                    if (bi.StudentExamId > 0 && year > 1900 && bi.BoardId > 0 && !string.IsNullOrEmpty(bi.BoardRoll) && !string.IsNullOrEmpty(bi.RegistrationNumber))
                                                    {
                                                        var acaInfo = new StudentAcademicInfo();
                                                        acaInfo.Student = student;
                                                        acaInfo.Year = bi.Year;
                                                        acaInfo.BoradRoll = bi.BoardRoll;
                                                        acaInfo.RegistrationNumber = bi.RegistrationNumber;
                                                        acaInfo.StudentBoard = _studentBoardService.GetByBoardId(bi.BoardId);
                                                        acaInfo.StudentExam = _studentExamService.LoadById(bi.StudentExamId);
                                                        _studentAcademicInfoService.Save(acaInfo);
                                                        SetStudentExamInfoStatus(student, acaInfo);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    _logger.Error("Single Board Exam info set error.", ex);
                                                }
                                            }
                                            _studentService.UpdateStudent(student);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error("Board info save error.", ex);
                                }

                                bool flag = SendSmsApi.GenarateInstantSmsAndNumber(1,studentProgramObj.Program.Organization.Id, studentProgramObj.Program.Id, studentProgramObj.Batch.Session.Id, studentProgramObj.Student.Id, studentProgramObj.Id);

                            }
                            catch (Exception ex)
                            {
                                if (studentProgramObj.Id.ToString()!= null)
                                {
                                    _logger.Error(
                                        "Student PrnNo : " + studentProgramObj.PrnNo + "Student Id : " +
                                        studentProgramObj.Id);
                                }
                                _logger.Error(ex);
                                
                            }
                            
                            return Json(new
                            {
                                IsSuccess = true,
                                PaymentId = studentPayment.Id
                            });
                        }
                        return Json(new { IsSuccess = false, ErrorMessage = "Registration Failed. Please check registration and try again." });
                    }
                    return Json(new { IsSuccess = false, ErrorMessage = responseMessage.ErrorMessage });
                }
                return Json(new { IsSuccess = false, ErrorMessage = "Please double check your information some invalid data found" });
            }

            catch (Exception e)
            {
                _logger.Error(e);
                _logger.Error("Student Mobile: " + logMobileNumber);
                return Json(new { IsSuccess = false, ErrorMessage = e.Message });
            }

            return Json(new { IsSuccess = false, ErrorMessage = "Invalid student information. Admission Failed. Please check your given input." });
        }

        private void SetStudentExamInfoStatus(BusinessModel.Entity.Students.Student student, StudentAcademicInfo acaInfo)
        {
            if (acaInfo.StudentExam != null)
            {
                if (acaInfo.StudentExam.Name.ToUpper().Contains("jsc"))
                {
                    student.IsJsc = true;
                }
                else if(acaInfo.StudentExam.BusinessId.ToLower().Contains("ssc"))
                {
                    student.IsSsc = true;
                }
                else if (acaInfo.StudentExam.BusinessId.ToLower().Contains("hsc"))
                {
                    student.IsHsc = true;
                }
            }
        }

        private ResponseMessage PaymentPaidAndMoneyReceiptGenerationAdmissionForFirstTimeOnly(
            StudentViewModel studentViewModel,
            StudentProgram studentProgram, 
            out MoneyReceipt moneyReceipt, 
            out StudentPayment studentPayment)
        {
            moneyReceipt = new MoneyReceipt();
            studentPayment = new StudentPayment();
            try
            {
                Referrer referrerObj = null;
                decimal receivedAmount = 0;

                /*If Discount Applicable */
                if (studentViewModel.StudentPayment.SpDiscountAmount > 0)
                {
                    referrerObj = _referenceService.LoadById(studentViewModel.StudentPayment.ReferrerId);
                }
                if (studentViewModel.StudentPayment.ReceivableAmount < 0)
                {
                    return new ResponseMessage() { ErrorMessage = "Transaction Process Failed ( Possible Cause : Invalid Receivable Amount )" };
                }
                if (studentViewModel.StudentPayment.NetReceivable < 0)
                {
                    return new ResponseMessage() { ErrorMessage = "Transaction Process Failed ( Possible Cause : Invalid Net-Receivable Amount )" };
                }

                receivedAmount = studentViewModel.StudentPayment.ReceivedAmount + studentViewModel.StudentPayment.SpDiscountAmount;
                if ( receivedAmount < 0  )
                {
                    return new ResponseMessage() { ErrorMessage = "Transaction Process Failed ( Possible Cause : Negative Transaction Not Supported )" };
                }
                if (receivedAmount == 0 && studentProgram.Program.Type == Program.ProgramTypeStatus.Paid)
                {
                    return new ResponseMessage() { ErrorMessage = "Transaction Process Failed ( Possible Cause : Empty Transaction Not Supported )" };
                }

                if (studentViewModel.StudentPayment.ReceivableAmount >= 0 &&
                    studentViewModel.StudentPayment.NetReceivable >= 0 &&
                    studentViewModel.StudentPayment.ReceivedAmount >= 0 &&
                    (receivedAmount > 0 || (receivedAmount == 0 && studentProgram.Program.Type != Program.ProgramTypeStatus.Paid)) 
                    
                    )
                {
                    /*Discount calculation */
                    var existingDiscountList =
                            _discountService.LoadDiscount(studentProgram.Program,
                                studentProgram.Batch.Session, studentProgram.CreationDate);
                    /*Discount Calculation Start */
                    Discount appliedDiscountObj = GetDiscount(existingDiscountList, studentViewModel);

                    //string recAmountFromStudent = GetReceivableAmountFromStudent(studentProgram.PrnNo);
                    var recAmountDisAmount = new[] { "empty#", (string)appliedDiscountObj.Amount.ToString() };

                    //var studentCourseDetail = _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id);
                    if (studentViewModel.StudentPayment.ReceivedAmount == (studentViewModel.StudentPayment.ReceivableAmount - studentViewModel.StudentPayment.SpDiscountAmount)
                        && studentViewModel.StudentPayment.ReceivableAmount >= studentViewModel.StudentPayment.ReceivedAmount)
                    {
                        /*does not need nextPayment date*/
                        /*Payment Clear*/
                        studentPayment = _studentPaymentService.OrganizeObjectForNewAdmissionForFirstTimeOnly
                            (studentViewModel.StudentPayment.ReceivableAmount,
                            studentViewModel.StudentPayment.ReceivedAmount,
                            studentViewModel.StudentPayment.SpDiscountAmount,
                            studentViewModel.StudentPayment.NetReceivable,
                            studentViewModel.StudentPayment.Remarks,
                            studentViewModel.StudentPayment.PaymentMethod.ToString(),
                            referrerObj, recAmountDisAmount);

                        studentPayment.PaymentType = PaymentType.MoneyReceipt;
                        studentPayment.CourseSubjectList = new List<CourseSubject>();
                        //foreach (var cs in studentCourseDetail)
                        //{
                        //    studentPayment.CourseSubjectList.Add(cs.CourseSubject);
                        //}
                        studentPayment.NextReceivedDate = UdvashERP.BusinessRules.Constants.MaxDateTime;
                        //student payment processing
                        //bool isPaymentProcessSuccess = _studentPaymentService.IsPaymentProcessSuccess(studentPayment, studentProgram.PrnNo);
                        //if (isPaymentProcessSuccess)
                        //{
                        //var sPayment = _studentPaymentService.LoadByStudentProgram(studentProgram);
                        //moneyReceipt = GetMoneyReceipt(sPayment);
                        return new ResponseMessage() { ErrorMessage = "", SuccessMessage = "Success" };

                        //}
                        return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Possible cause: DB operation)" };
                    }
                    if (studentViewModel.StudentPayment.ReceivedAmount < (studentViewModel.StudentPayment.ReceivableAmount - studentViewModel.StudentPayment.SpDiscountAmount)
                        && studentViewModel.StudentPayment.ReceivableAmount >= studentViewModel.StudentPayment.ReceivedAmount)
                    {
                        if (!String.IsNullOrEmpty(studentViewModel.StudentPayment.NextRecDate.Date.ToString()))
                        {
                            var nextRDate = Convert.ToDateTime(studentViewModel.StudentPayment.NextReceivedDate);

                            if (nextRDate.Date >= DateTime.Now.Date)
                            {
                                studentPayment = _studentPaymentService.OrganizeObjectForNewAdmissionForFirstTimeOnly
                                    (studentViewModel.StudentPayment.ReceivableAmount,
                                     studentViewModel.StudentPayment.ReceivedAmount,
                                     studentViewModel.StudentPayment.SpDiscountAmount,
                                    studentViewModel.StudentPayment.NetReceivable,
                                    studentViewModel.StudentPayment.Remarks,
                                    studentViewModel.StudentPayment.PaymentMethod.ToString(),
                                    referrerObj, recAmountDisAmount);

                                studentPayment.NextReceivedDate = nextRDate;
                                studentPayment.PaymentType = PaymentType.MoneyReceipt;
                                studentPayment.CourseSubjectList = new List<CourseSubject>();

                                //foreach (var cs in studentCourseDetail)
                                //{
                                //    studentPayment.CourseSubjectList.Add(cs.CourseSubject);
                                //}

                                //bool isPaymentProcessSuccess = _studentPaymentService.IsPaymentProcessSuccess(studentPayment, studentProgram.PrnNo);
                                //if (isPaymentProcessSuccess)
                                //{
                                //  var sPayment = _studentPaymentService.LoadByStudentProgram(studentProgram);
                                //  moneyReceipt = GetMoneyReceipt(sPayment);
                                return new ResponseMessage() { ErrorMessage = "", SuccessMessage = "Success" };
                                //}
                            }
                            return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Next Receivable date must be greater than Today's Date)" };
                        }
                        return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Next Receivable date Missing)" };
                    }
                    return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Invalid Value Insert)" };
                }
                return new ResponseMessage() { ErrorMessage = "Transaction Process Failed( Possible Cause : Invalid Amount Submitted)" };
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return new ResponseMessage() { ErrorMessage = "Transaction Process Failed" };
            }
        }


        #region verify Board Info 

        [HttpPost]
        public JsonResult VerifyBoardInfo(string examId, string year, string board, string roll, string regNo)
        {
            bool isValidInfo = true;
            string message = "";
            StudentAcademicInfoBaseData academicInfoBaseData = new StudentAcademicInfoBaseData();
            try
            {
                StudentExam exam = _studentExamService.LoadById(long.Parse(examId));
                StudentBoard examBoard = _studentBoardService.GetByBoardId(long.Parse(board));
                if (exam != null && !String.IsNullOrEmpty(exam.BusinessId) && examBoard != null &&
                    !String.IsNullOrEmpty(examBoard.BusinessId) && !String.IsNullOrEmpty(year) &&
                    !String.IsNullOrEmpty(roll) && !String.IsNullOrEmpty(regNo))
                {
                    var r = EducationResult(roll, regNo, examBoard.BusinessId, exam.BusinessId, year);

                    if (r != null)
                    {
                        HtmlDocument htmlDoc = new HtmlDocument();
                        htmlDoc.LoadHtml(r.Content);

                        if (htmlDoc.DocumentNode != null)
                        {
                            /*--------------------------------Check For Result Not Found-------------------------------------------*/
                            if (htmlDoc.DocumentNode.SelectNodes("//table") != null)
                            {
                                foreach (HtmlNode nodeDoc in htmlDoc.DocumentNode.SelectNodes("//table"))
                                {
                                    foreach (var nd in nodeDoc.Descendants().Where(x => x.Name == "td"))
                                    {
                                        if (nd.InnerText.Contains("RESULT NOT FOUND!"))
                                        {
                                            isValidInfo = false;
                                        }
                                    }
                                }
                            }
                            /*--------------------------------End Check For Result Not Found-------------------------------------------*/

                            #region Get Html Content From Board

                            List<string> headDataList = new List<string>();
                            List<string> resultDataList = new List<string>();
                            int count = 0;
                            if (htmlDoc.DocumentNode.SelectNodes("//table[@class='black12']") != null)
                            {
                                foreach (
                                    HtmlNode nodeDoc in htmlDoc.DocumentNode.SelectNodes("//table[@class='black12']"))
                                {
                                    count++;
                                    if (count == 1)
                                    {
                                        int loop = 0;
                                        foreach (var nd in nodeDoc.Descendants().Where(x => x.Name == "td"))
                                        {
                                            loop++;
                                            if (loop%2 == 0)
                                                headDataList.Add(nd.InnerText);
                                        }
                                    }
                                    else
                                    {
                                        foreach (var nd in nodeDoc.Descendants().Where(x => x.Name == "td"))
                                        {
                                            resultDataList.Add(nd.InnerText);
                                        }
                                    }
                                }

                                #region Assign HeadData To AcademicInfo Base Data

                                if (headDataList.Count > 0)
                                {
                                    academicInfoBaseData.RollNo = headDataList[0];
                                    academicInfoBaseData.Name = headDataList[1];
                                    academicInfoBaseData.Board = headDataList[2];
                                    academicInfoBaseData.FathersName = headDataList[3];
                                    academicInfoBaseData.ExamGroup = headDataList[4];
                                    academicInfoBaseData.MothersName = headDataList[5];
                                    academicInfoBaseData.StudentType = headDataList[6];
                                    var stringDate = headDataList[7];
                                    DateTime exactDate = new DateTime();

                                    if (stringDate != "N/A")
                                    {
                                        exactDate = DateTime.ParseExact(stringDate, "dd-MM-yyyy",
                                            CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        exactDate = UdvashERP.BusinessRules.Constants.MinDateTime;
                                    }
                                    academicInfoBaseData.DateOfBirth = exactDate;
                                    academicInfoBaseData.Result = headDataList[8];
                                    academicInfoBaseData.Institute = headDataList[9];
                                    academicInfoBaseData.Gpa = headDataList[10];
                                }

                                #endregion

                            }
                            else
                            {
                                isValidInfo = false;
                            }

                            #endregion
                        }
                        message = "Result Found Successfully.";
                    }
                }
                else
                {
                    isValidInfo = false;
                    message = "Please fill all fields.";
                }
                if (!isValidInfo && !String.IsNullOrEmpty(message))
                {
                    message = "Result Not Found.";
                }

            }
            catch (Exception ex)
            {
                isValidInfo = false;
                message = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                //return Json(new { data = "Information server is not responding. Try again." });
                
            }
            return Json(new { IsSuccess = isValidInfo, Message = message, Data = academicInfoBaseData });
            
        }

        private ContentResult EducationResult(string roll, string reg, string board, string exam, string year)
        {
            //string url = "http://archive.educationboard.gov.bd/result.php";
            //string refUrl = "http://archive.educationboard.gov.bd/index.php";
            //string url = "http://www.educationboardresults.gov.bd/regular/result.php";
            //string refUrl = "http://www.educationboardresults.gov.bd/regular/index.php";
            string url = UdvashERP.Properties.Settings.Default.UdvashERP_board_info_sync_url;
            string refUrl = UdvashERP.Properties.Settings.Default.UdvashERP_board_info_sync_ref_url;
            string html = CommonHelper.EducationResult(url, refUrl, roll, reg, board, exam, year);
            return Content(html);
        }

        //private ContentResult EducationResult(string roll, string reg, string board, string exam, string year)
        //{

        //    //string url = "http://archive.educationboard.gov.bd/result.php";
        //    //string refUrl = "http://archive.educationboard.gov.bd/index.php";
        //    //string url = "http://www.educationboardresults.gov.bd/regular/result.php";
        //    //string refUrl = "http://www.educationboardresults.gov.bd/regular/index.php";
        //    string url = UdvashERP.Properties.Settings.Default.UdvashERP_board_info_sync_url;
        //    string refUrl = UdvashERP.Properties.Settings.Default.UdvashERP_board_info_sync_ref_url;
        //    var summationOfCaptcha = 0;
        //    var cookieCon = new CookieContainer();
        //    var z = GetHtml(refUrl, "", "", cookieCon);
        //    HtmlDocument htmlDoc = new HtmlDocument();
        //    htmlDoc.LoadHtml(z);
        //    if (htmlDoc.DocumentNode != null)
        //    {
        //        /*--------------------------------Quick Fixing Summation of Captcha-------------------------------------------*/
        //        if (htmlDoc.DocumentNode.SelectNodes("//table") != null)
        //        {
        //            foreach (HtmlNode nodeDoc in htmlDoc.DocumentNode.SelectNodes("//table"))
        //            {
        //                //var v = nodeDoc.Descendants().Where(x => x.Name == "td").ToList();
        //                foreach (var nd in nodeDoc.Descendants().Where(x => x.Attributes.Contains("class") && x.Attributes["class"].Value.Contains("black12bold")))
        //                {
        //                    int counter = 0;
        //                    foreach (var tr in nd.Descendants().Where(x => x.Name == "tr"))
        //                    {
        //                        counter++;
        //                        int counterTd = 0;
        //                        if (counter == 7)
        //                            foreach (var td in tr.Descendants().Where(x => x.Name == "td"))
        //                            {
        //                                counterTd++;
        //                                if (counterTd == 2)
        //                                {
        //                                    var numericCaptcha = td.InnerText;
        //                                    var summationOfCaptchaSplit = numericCaptcha.Split('+');
        //                                    foreach (var val in summationOfCaptchaSplit)
        //                                    {
        //                                        summationOfCaptcha = summationOfCaptcha + Convert.ToInt32(val);
        //                                    }

        //                                    break;
        //                                }
        //                            }
        //                    }
        //                }
        //                break;
        //            }

        //        }
        //    }



        //    string sendParam = "sr=3&et=0";
        //    sendParam += "&exam=" + exam;
        //    sendParam += "&year=" + year;
        //    sendParam += "&board=" + board;
        //    sendParam += "&roll=" + roll;
        //    sendParam += "&reg=" + reg;
        //    sendParam += "&value_s=" + summationOfCaptcha;
        //    sendParam += "&button2=Submit";
        //    var html = GetHtml(url, refUrl, sendParam, cookieCon);
        //    //return new ContentResult() { Content = html };
        //    // return ContentResult(){Content=html)}
        //    return Content(html);
        //}

        //private string GetHtml(string url, string refUrl, string param, CookieContainer cookieCon)
        //{
        //    string responseHtml = "";
        //    try
        //    {
        //        byte[] data = new ASCIIEncoding().GetBytes(param);
        //        var webRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
        //        webRequest.CookieContainer = cookieCon;
        //        webRequest.Method = "POST";
        //        webRequest.Referer = refUrl;
        //        webRequest.ContentType = "application/x-www-form-urlencoded";
        //        webRequest.ContentLength = data.Length;

        //        Stream myStream = webRequest.GetRequestStream();
        //        myStream.Write(data, 0, data.Length);

        //        var webResponse = webRequest.GetResponse();
        //        var responseStream = webResponse.GetResponseStream();
        //        StreamReader sr = new StreamReader(responseStream);
        //        responseHtml = sr.ReadToEnd();
        //    }
        //    catch (Exception ex)
        //    {
        //        responseHtml = ex.ToString();
        //    }
        //    return responseHtml;
        //}

        #endregion 

        #endregion

        #region Others Function
        public void SetViewData()
        {
            //var paymentMethods = new SelectList(_commonHelper.GetPaymentMethods(), "Key", "Value");
            //var paymentMethodList = (from KeyValuePair<string, int> statusKeyValuePair in paymentMethods.Items
            //                         select new SelectListItem()
            //                         {
            //                             Text = statusKeyValuePair.Key,
            //                             Value = statusKeyValuePair.Value.ToString()
            //                         }).ToList();

            var refList = new SelectList(_referenceService.GetAllRefferer(), "Id", "Name");
            ViewBag.RefererList = refList;
            ViewBag.PaymentMethods = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentMethod>(new List<int> { (int)PaymentMethod.BKash, (int)PaymentMethod.Cheque }), "Key", "Value");//paymentMethodList;
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var authorizedPrograms = _programService.LoadAuthorizedProgram(userMenu, isOffice: true);
            ViewBag.Program = new SelectList(authorizedPrograms, "Id", "Name");
            ViewBag.VersionOfStudy = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(new List<int> { (int)VersionOfStudy.Combined }), "Key", "Value");
            ViewBag.Gender = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(new List<int> { (int)Gender.Combined }), "Key", "Value");
            //ViewBag.Religion = new SelectList(_commonHelper.GetReligion(), "Key", "Value");
            ViewBag.Religion = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(), "Key", "Value");
        }
        //payment summary purpose
        private bool CheckStudentProgramRegistrationInASession(StudentViewModel studentViewModel, long studentId)
        {
            long programIdL = Convert.ToInt64(studentViewModel.Program.Trim());
            long sessionIdL = Convert.ToInt64(studentViewModel.Session.Trim());
            var studentProgram = _studentProgramService.GetStudentProgram(programIdL, sessionIdL, studentId);
            if (studentProgram != null)
            {
                return true;
            }
            return false;
        }

        //calculate course fees
        [HttpPost]
        public JsonResult CalculateCourseFee(string studentViewModelJson)
        {
            bool success = true;            
            List<ResponseMessage> responseMessages = new List<ResponseMessage>();
            var serializer = new JavaScriptSerializer();
            var studentViewModel = serializer.Deserialize<StudentViewModel>(studentViewModelJson);
            decimal totalCourseFee = GetTotalCourseFee(studentViewModel, out responseMessages);
            decimal? offeredDiscount = GetOfferedDiscount(studentViewModel);
            decimal? receivableAmount = totalCourseFee - offeredDiscount;

            bool isPaidProgram = IsPaidProgram(studentViewModel.Program, out success, ref responseMessages);

            if (responseMessages.Count > 0)
                success = false;
            
            return Json(new
            {
                IsSuccess = success,
                Message = responseMessages,
                TotalCourseFee = totalCourseFee,
                OfferedDiscount = offeredDiscount,
                ReceivableAmount = receivableAmount,
                IsPaidProgram = isPaidProgram
            });
        }

        private bool IsPaidProgram(string programId, out bool isSuccess, ref List<ResponseMessage> responseMessages)
        {
            isSuccess = false;
            long programIdL = 0;
            long.TryParse(programId, out programIdL);

            if (programIdL > 0)
            {
                var program = _programService.GetProgram(programIdL);
                if (program != null)
                {
                    isSuccess = true;
                    if (program.Type == Program.ProgramTypeStatus.Paid)
                    {                        
                        return true;
                    }                    
                }
                else
                {
                    responseMessages.Add(new ResponseMessage()
                    {
                        ErrorMessage = "Program not found"
                    });
                }
            }
            else
            {
                responseMessages.Add(new ResponseMessage()
                {
                    ErrorMessage = "Invalid program ID"
                });
            }
            
            return false;
        }

        public ResponseMessage PaymentPaidAndMoneyReceiptGeneration
            (StudentViewModel studentViewModel,
            StudentProgram studentProgram,
            out MoneyReceipt moneyReceipt,
            out StudentPayment studentPayment)
        {
            moneyReceipt = new MoneyReceipt();
            studentPayment = new StudentPayment();
            try
            {
                Referrer referrerObj = null;
                decimal receivedAmount = 0;

                /*If Discount Applicable */
                if (studentViewModel.StudentPayment.SpDiscountAmount > 0)
                {
                    referrerObj = _referenceService.LoadById(studentViewModel.StudentPayment.ReferrerId);
                }
                if (studentViewModel.StudentPayment.ReceivableAmount < 0)
                {
                    return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Possible Cause : Invalid Receivable Amount )" };
                }
                if (studentViewModel.StudentPayment.NetReceivable < 0)
                {
                    return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Possible Cause : Invalid Net-Receivable Amount )" };
                }

                receivedAmount = studentViewModel.StudentPayment.ReceivedAmount + studentViewModel.StudentPayment.SpDiscountAmount;
                if (receivedAmount < 0)
                {
                    return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Possible Cause : Negative Transaction Not Supported )" };
                }

                if (receivedAmount == 0 && studentProgram.Program.Type == Program.ProgramTypeStatus.Paid)
                {
                    return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Possible Cause : Empty Transaction Not Supported )" };
                }
                
                if (studentViewModel.StudentPayment.ReceivableAmount >= 0 &&
                    studentViewModel.StudentPayment.NetReceivable >= 0 &&
                    studentViewModel.StudentPayment.ReceivedAmount >= 0 &&
                    (receivedAmount > 0 || receivedAmount == 0 && studentProgram.Program.Type != Program.ProgramTypeStatus.Paid))
                {
                    string recAmountFromStudent = GetReceivableAmountFromStudent(studentProgram.PrnNo);
                    var recAmountDisAmount = recAmountFromStudent.Split('#');
                    var studentCourseDetail = _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id);

                    if (studentViewModel.StudentPayment.ReceivedAmount == (studentViewModel.StudentPayment.ReceivableAmount - studentViewModel.StudentPayment.SpDiscountAmount)
                        && studentViewModel.StudentPayment.ReceivableAmount >= studentViewModel.StudentPayment.ReceivedAmount)
                    {
                        /*does not need nextPayment date*/
                        /*Payment Clear*/
                        studentPayment = _studentPaymentService.OrganizeObjectForNewAdmission
                            (studentViewModel.StudentPayment.ReceivableAmount,
                            studentViewModel.StudentPayment.ReceivedAmount,
                            studentViewModel.StudentPayment.SpDiscountAmount,
                            studentViewModel.StudentPayment.NetReceivable,
                            studentViewModel.StudentPayment.Remarks,
                            studentViewModel.StudentPayment.PaymentMethod.ToString(),
                            referrerObj, recAmountDisAmount, studentProgram.PrnNo);

                        studentPayment.PaymentType = PaymentType.MoneyReceipt;
                        studentPayment.CourseSubjectList = new List<CourseSubject>();
                        foreach (var cs in studentCourseDetail)
                        {
                            studentPayment.CourseSubjectList.Add(cs.CourseSubject);
                        }
                        studentPayment.NextReceivedDate = UdvashERP.BusinessRules.Constants.MaxDateTime;
                        //student payment processing
                        bool isPaymentProcessSuccess = _studentPaymentService.IsPaymentProcessSuccess(studentPayment, studentProgram.PrnNo);
                        if (isPaymentProcessSuccess)
                        {
                            var sPayment = _studentPaymentService.LoadByStudentProgram(studentProgram);
                            moneyReceipt = GetMoneyReceipt(sPayment);
                            return new ResponseMessage() { ErrorMessage = "", SuccessMessage = "Success" };

                        }
                        return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Possible cause: DB operation)" };
                    }
                    if (studentViewModel.StudentPayment.ReceivedAmount < (studentViewModel.StudentPayment.ReceivableAmount - studentViewModel.StudentPayment.SpDiscountAmount)
                        && studentViewModel.StudentPayment.ReceivableAmount >= studentViewModel.StudentPayment.ReceivedAmount)
                    {
                        if (!String.IsNullOrEmpty(studentViewModel.StudentPayment.NextRecDate.Date.ToString()))
                        {
                            var nextRDate = Convert.ToDateTime(studentViewModel.StudentPayment.NextReceivedDate);

                            if (nextRDate.Date >= DateTime.Now.Date)
                            {
                                studentPayment = _studentPaymentService.OrganizeObjectForNewAdmission
                                    (studentViewModel.StudentPayment.ReceivableAmount,
                                     studentViewModel.StudentPayment.ReceivedAmount,
                                     studentViewModel.StudentPayment.SpDiscountAmount,
                                    studentViewModel.StudentPayment.NetReceivable,
                                    studentViewModel.StudentPayment.Remarks,
                                    studentViewModel.StudentPayment.PaymentMethod.ToString(),
                                    referrerObj, recAmountDisAmount, studentProgram.PrnNo);
                                studentPayment.NextReceivedDate = nextRDate;
                                studentPayment.PaymentType = PaymentType.MoneyReceipt;
                                studentPayment.CourseSubjectList = new List<CourseSubject>();

                                foreach (var cs in studentCourseDetail)
                                {
                                    studentPayment.CourseSubjectList.Add(cs.CourseSubject);
                                }

                                bool isPaymentProcessSuccess = _studentPaymentService.IsPaymentProcessSuccess(studentPayment, studentProgram.PrnNo);
                                if (isPaymentProcessSuccess)
                                {
                                    var sPayment = _studentPaymentService.LoadByStudentProgram(studentProgram);
                                    moneyReceipt = GetMoneyReceipt(sPayment);
                                    return new ResponseMessage() { ErrorMessage = "", SuccessMessage = "Success" };
                                }
                            }
                            return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Next Receivable date must be greater than Today's Date)" };
                        }
                        return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Next Receivable date Missing)" };
                    }
                    return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Invalid Value Insert)" };
                }
                return new ResponseMessage() { ErrorMessage = "Transaction Process Failed(Possible Cause : Invalid Program Roll )" };
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return new ResponseMessage() { ErrorMessage = "Transaction Process Failed" };
            }
        }
        private string GetReceivableAmountFromStudent(string stdProId)
        {
            try
            {
                decimal rreceivableAmountWithoutDiscount = 0;
                decimal recavailableAmount = 0;
                var studentProgram = _studentProgramService.GetStudentProgram(stdProId.Trim());
                var studentCourseDetailsList =
                      _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(studentProgram.Id);
                var query = studentCourseDetailsList.GroupBy(x => x.CourseSubject.Course, (key, g) => new { Course = key, Subjects = g.ToList() });
                var courseList = query.ToList();
                var studentViewModel = new StudentViewModel();
                foreach (var courseAndCorrespondingSubjectList in courseList)
                {
                    var courseViewModel = new CourseViewModel
                    {
                        Id = courseAndCorrespondingSubjectList.Course.Id,
                        Name = courseAndCorrespondingSubjectList.Course.Name
                    };
                    var courseSubjectList = courseAndCorrespondingSubjectList.Subjects;
                    foreach (var courseSubject in courseSubjectList)
                    {
                        var subjectViewModel = new SubjectViewModel();
                        var subject = courseSubject.CourseSubject;
                        subjectViewModel.Name = courseSubject.CourseSubject.Subject.Name;
                        subjectViewModel.Payment = subject.Payment;
                        subjectViewModel.Id = subject.Id;
                        rreceivableAmountWithoutDiscount += Convert.ToDecimal(subject.Payment.ToString().Trim());
                        courseViewModel.SubjectViewModels.Add(subjectViewModel);
                    }
                    studentViewModel.CourseViewModels.Add(courseViewModel);
                }
                StudentPayment lastStudentPaymentTransication = _studentPaymentService.LoadByStudentProgram(studentProgram);
                if (lastStudentPaymentTransication == null)
                {
                    /*First Transication in this Program*/
                    /*Calculate Discount*/
                    var existingDiscountList =
                            _discountService.LoadDiscount(studentProgram.Program,
                                studentProgram.Batch.Session, studentProgram.CreationDate);
                    /*Discount Calculation Start */
                    Discount appliedDiscountObj = GetDiscount(existingDiscountList, studentViewModel);
                    var courseCount = studentViewModel.CourseViewModels.Count;
                    if (appliedDiscountObj.Amount > 0)
                    {
                        recavailableAmount = (decimal)(rreceivableAmountWithoutDiscount - appliedDiscountObj.Amount);
                        return recavailableAmount.ToString() + '#' + appliedDiscountObj.Amount;
                    }
                    recavailableAmount = rreceivableAmountWithoutDiscount - 0;
                    return recavailableAmount.ToString() + '#' + "0";
                }
                /*Already paid some amount */
                /*Pick Discount amount from db for this Program*/
                if (lastStudentPaymentTransication.DueAmount != null)
                    recavailableAmount = (decimal)lastStudentPaymentTransication.DueAmount;
                return recavailableAmount.ToString() + '#' + lastStudentPaymentTransication.DiscountAmount.ToString();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "";
            }
        }
        private decimal GetTotalCourseFee(StudentViewModel studentViewModel, out List<ResponseMessage> responseMessages)
        {
            decimal totalCost = 0;
            responseMessages = new List<ResponseMessage>();

            foreach (var course in studentViewModel.CourseViewModels)
            {
                int courseSubjectTaken = 0;
                foreach (var courseSubject in course.SubjectViewModels)
                {
                    var courseSubjectObj = _courseSubjectService.LoadCourseSubject(course.Id, courseSubject.Id);
                    totalCost += Convert.ToDecimal(courseSubject.Payment);
                    courseSubjectTaken++;
                }

                var dbCourse = _courseService.GetCourse(course.Id);
                if (dbCourse != null)
                {
                    if (courseSubjectTaken > dbCourse.MaxSubject)
                    {
                        responseMessages.Add(new ResponseMessage()
                        {
                            ErrorMessage = string.Format("You cannot take more than {0} subjects in {1} course", dbCourse.MaxSubject, dbCourse.Name),
                        });
                    }
                }
            }

            return totalCost;
        }
        private decimal? GetOfferedDiscount(StudentViewModel studentViewModel)
        {
            var program = _programService.GetProgram(Convert.ToInt64(studentViewModel.Program));
            var session = _sessionService.LoadById(Convert.ToInt64(studentViewModel.Session));
            var existingDiscountList = _discountService.LoadDiscount(program, session, DateTime.Now.Date);
            Discount appliedDiscountObj = GetDiscount(existingDiscountList, studentViewModel);
            return appliedDiscountObj.Amount;
        }
        private Discount GetDiscount(IList<Discount> existingDiscountList, StudentViewModel studentViewModel)
        {
            IList<Discount> discountApplicableListForStd = new List<Discount>();
            foreach (var discountObj in existingDiscountList)
            {
                IList<DiscountDetail> discountDetailsList = _discountDetailService.GetAllDiscountDetailByDiscountId(discountObj.Id);
                /*Check : does student deserve discount or not */
                if (discountDetailsList.Count <= studentViewModel.CourseViewModels.Count)
                {
                    var countdiscountDetailsList = discountDetailsList.Count;
                    int jj = 0;
                    foreach (var discountDetail in discountDetailsList)
                    {
                        var dCourse = discountDetail.Course;
                        var dMinSub = discountDetail.MinSubject;
                        int ii = 0;
                        foreach (var stdAssignedCourse in studentViewModel.CourseViewModels)
                        {
                            var stdCourse = _courseService.GetCourse(stdAssignedCourse.Id);
                            if (stdCourse == dCourse && dMinSub <= stdAssignedCourse.SubjectViewModels.Count)
                            {
                                jj++;
                                break;
                            }
                        }
                    }
                    /*Contain each and Every Course and their minSubject which allows discount */
                    if (countdiscountDetailsList == jj)
                    {
                        discountApplicableListForStd.Add(discountObj);
                    }
                }
            }
            var appliedDiscountObj = new Discount() { Amount = 0 };
            if (discountApplicableListForStd.Count > 1)
            {
                appliedDiscountObj = _discountService.GetMostApplicableDiscountObj(discountApplicableListForStd);
            }
            if (discountApplicableListForStd.Count == 1)
            {
                appliedDiscountObj = discountApplicableListForStd[0];
            }
            return appliedDiscountObj;
        }

        #endregion

        #region Ajax Request Function
        [HttpPost]
        public JsonResult GetSessionByProgram(long programId = 0, long sessionId = 0)
        {
            var sessionList = new List<Session>();
            List<UserMenu> userMenus = ViewBag.UserMenu;
            if (programId != 0)
            {
                sessionList = _sessionService.LoadAuthorizedSession(userMenus, null, _commonHelper.ConvertIdToList(programId), true).ToList();//LoadAuthorizedOfficePublicSession(userMenus, programId,true).ToList();
            }
            return Json(new
            {
                SessionOptions = this.RenderRazorViewToOptions(DropDownOptionView, sessionList, 0, "Select Session"),
                CourseView = GetCouseViewDetails("Partial/_CourseSubjectView", new List<Course>(), false)
            });
        }
        [HttpPost]
        public ActionResult GetBranchByProgramSession(long programId = 0, long sessionId = 0, string calledFrom = "", long studentId = 0)
        {
            List<UserMenu> userMenus = ViewBag.UserMenu;
            var sessionList = _sessionService.LoadAuthorizedSession(userMenus, null, _commonHelper.ConvertIdToList(programId));
            var branchList = _branchService.LoadAuthorizedBranch(userMenus, null, _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId));
            var courseList = _courseService.LoadCourse(programId, sessionId);
            courseList = courseList.OrderBy(x => x.Rank).ToList();
            return Json(new
            {
                SessionOptions = this.RenderRazorViewToOptions(DropDownOptionView, sessionList, sessionId, "Select Session"),
                BrunchOptions = this.RenderRazorViewToOptions(DropDownOptionView, branchList, 0, "Select Branch"),
                CourseView = GetCouseViewDetails("Partial/_CourseSubjectView", courseList, false),
                ExamBoardView = GetExamBoardViewForStudent(programId, sessionId, studentId)
            });
        }

        [HttpPost]
        public JsonResult GetCampusByProgramSessionAndBranch(long branchId = 0, long campusId = 0)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var branchIds = _commonHelper.ConvertIdToList(branchId);
            var campusList = _campusService.LoadAuthorizeCampus(userMenu, null, null, branchIds);
            return Json(new
            {
                CampusOptions = this.RenderRazorViewToOptions(DropDownOptionView, campusList, campusId, "Select Campus")
            });
        }
        [HttpPost]
        public JsonResult GetBatchDayByProgramSessionBranchAndCampus(long programId = 0, long sessionId = 0, long branchId = 0, long campusId = 0, int versionStudy = 0, int gender = 0)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var branchIds = _commonHelper.ConvertIdToList(branchId);
            var campusIds = _commonHelper.ConvertIdToList(campusId);
            var batchDaysList = _batchService.LoadAuthorizeBatchGroupByDays(userMenu, null, _commonHelper.ConvertIdToList(programId), branchIds, _commonHelper.ConvertIdToList(sessionId), campusIds, _commonHelper.ConvertIdToList(versionStudy), _commonHelper.ConvertIdToList(gender));
            List<DropDownOption> batchDayList = GetBatchDay(batchDaysList.ToList());
            return Json(new
            {
                BatchDayOptioins = this.RenderRazorViewToOptions(DropDownOptionView, batchDayList, "", "Select Batch Days"),
            });
        }
        [HttpPost]
        public JsonResult GetBatchTimeByProgramSessionBranchCampusAndBatchDay(long programId = 0, long sessionId = 0, long branchId = 0, long campusId = 0, string batchDay = "", int versionStudy = 0, int gender = 0)
        {
            if (batchDay == "Select Batch Days")
            {
                batchDay = "";
            }
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var branchIds = _commonHelper.ConvertIdToList(branchId);
            var campusIds = _commonHelper.ConvertIdToList(campusId);
            var batchDays = _commonHelper.ConvertIdToList(batchDay);
            var batchTimesList = _batchService.LoadAuthorizeBatchGroupByTimes(userMenu, null, _commonHelper.ConvertIdToList(programId), branchIds, _commonHelper.ConvertIdToList(sessionId), campusIds, batchDays.ToArray(), _commonHelper.ConvertIdToList(versionStudy), _commonHelper.ConvertIdToList(gender));
            List<DropDownOption> batchTimeList = GetBatchTime(batchTimesList.ToList());
            return Json(new
            {
                BatchTimeOptions = this.RenderRazorViewToOptions(DropDownOptionView, batchTimeList, "", "Select Batch Time")
            });
        }
        [HttpPost]
        public JsonResult GetBatchByProgramSessionBranchCampusAndBatchDayTime(long programId = 0, long sessionId = 0, long branchId = 0, long campusId = 0, long batchId = 0, string batchDay = "", string batchTime = "", int versionStudy = 0, int gender = 0)
        {
            if (batchDay == "Select Batch Days")
            {
                batchDay = "";
            }
            if (batchDay == "Select Batch Time")
            {
                batchDay = "";
            }
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var branchIds = _commonHelper.ConvertIdToList(branchId);
            var campusIds = _commonHelper.ConvertIdToList(campusId);
            var batchDays = _commonHelper.ConvertIdToList(batchDay);
            var batchTimes = _commonHelper.ConvertIdToList(batchTime);
            var batchList = _batchService.LoadAuthorizeBatch(userMenu, null, _commonHelper.ConvertIdToList(programId), branchIds, _commonHelper.ConvertIdToList(sessionId), campusIds, batchDays.ToArray(), batchTimes.ToArray(), _commonHelper.ConvertIdToList(versionStudy), _commonHelper.ConvertIdToList(gender));
            batchList = GetFilteredBatchList(batchList.ToList());
            return Json(new
            {
                BatchOptions = this.RenderRazorViewToOptions(DropDownOptionView, batchList, batchId, "Select Batch Name"),
            });
        }

        #endregion

        #region Helper Function
        private List<Batch> GetFilteredBatchList(List<Batch> batchList)
        {
            batchList.ForEach((x) =>
            {
                x.RemainingCapacity = x.Capacity - (x.StudentPrograms.Where(y => y.Batch == x && y.Status == StudentProgram.EntityStatus.Active).Select(y => y.Student)).Count();
                x.Name += " (" + x.RemainingCapacity+")";
            }
            );
            return batchList;
        }
        private List<DropDownOption> GetBatchDay(List<string> batchDayList, string batchDay = null)
        {
            IEnumerable<DropDownOption> dropDownBatchDays = from b in batchDayList select new DropDownOption { Id = b, Name = b };
            if (batchDay != null)
            {
                dropDownBatchDays = from b in batchDayList where b == batchDay select new DropDownOption { Id = b, Name = b };
            }
            return dropDownBatchDays.ToList();
        }
        private List<DropDownOption> GetBatchTime(List<string> batchTimeList, string batchTime = null)
        {
            IEnumerable<DropDownOption> dropDownBatchTime = from b in batchTimeList select new DropDownOption { Id = b, Name = b };
            if (batchTime != null)
            {
                dropDownBatchTime = from b in batchTimeList where b == batchTime select new DropDownOption { Id = b, Name = b };
            }
            return dropDownBatchTime.ToList();
        }

        #endregion

        [HttpPost]
        public ActionResult GenerateBranch(string programId = "", string sessionId = "")
        {
            List<UserMenu> userMenus = ViewBag.UserMenu;
            IList<Branch> branchList = _branchService.LoadAuthorizedBranch(userMenus, null, _commonHelper.ConvertIdToList(Convert.ToInt64(programId.Trim())), _commonHelper.ConvertIdToList(Convert.ToInt64(sessionId.Trim())));
            var branch = new SelectList(branchList, "Id", "Name");
            ViewBag.BRANCH = branch;
            return PartialView("Partial/_BranchGenerate");
        }
        [HttpGet]
        public ActionResult CourseDetails(string programId = "", string sessionId = "")
        {
            if (!String.IsNullOrEmpty(programId) && !String.IsNullOrEmpty(sessionId))
            {
                IList<Course> courses = _courseService.LoadCourse(Convert.ToInt64(programId), Convert.ToInt64(sessionId));
                ViewBag.COURSES = courses;
                return PartialView("Partial/_CourseDetails");
            }
            return View("NewStudentAdmission");
        }
        private bool ValidateStudentInfo(StudentViewModel studentViewModel)
        {
            if (string.IsNullOrEmpty(studentViewModel.Batch)
                || string.IsNullOrEmpty(studentViewModel.BatchDays)
                || string.IsNullOrEmpty(studentViewModel.BatchTime)
                || string.IsNullOrEmpty(studentViewModel.Branch)
                || string.IsNullOrEmpty(studentViewModel.Campus)
                || string.IsNullOrEmpty(studentViewModel.MobNumber)
                || string.IsNullOrEmpty(studentViewModel.Name)
                || string.IsNullOrEmpty(studentViewModel.BatchTime)
                || string.IsNullOrEmpty(studentViewModel.Program)
                || string.IsNullOrEmpty(studentViewModel.Session)
                || studentViewModel.Gender == null
                || studentViewModel.Religion == null
                || studentViewModel.VersionOfStudy == null
                || studentViewModel.Gender == 0
                || studentViewModel.Religion == 0
                || studentViewModel.VersionOfStudy == 0
                || studentViewModel.CourseViewModels == null
                || studentViewModel.CourseViewModels.Count == 0
                || studentViewModel.StudentPayment == null
                || studentViewModel.StudentPayment.ReceivedAmount < 0
                || studentViewModel.StudentPayment.DueAmount < 0
                || studentViewModel.StudentPayment.DiscountAmount < 0
                || studentViewModel.StudentPayment.OfferedDiscount < 0)
            {
                return false;
            }

            foreach (var courseFromView in studentViewModel.CourseViewModels)
            {
                var courseObj = _courseService.GetCourse(Convert.ToInt64(courseFromView.Id));
                var minSubFromView = courseObj.OfficeMinSubject;
                var maxSubFromView = courseObj.MaxSubject;
                var takenSubCount = courseFromView.SubjectViewModels.Count;
                if (courseObj.OfficeCompulsory)
                {
                    if (takenSubCount == 0)
                    {
                        return false;
                    }
                }
                if (takenSubCount < courseObj.OfficeMinSubject)
                {
                    return false;
                }
                if (takenSubCount > courseObj.MaxSubject)
                {
                    return false;
                }
            }


            return true;
        }
        private MoneyReceipt GetMoneyReceipt(StudentPayment studentPayment)
        {
            var moneyReceipt = new MoneyReceipt();
            moneyReceipt.MrNo = studentPayment.ReceiptNo;
            moneyReceipt.Session = studentPayment.StudentProgram.Batch.Session.Name;
            moneyReceipt.Program = studentPayment.StudentProgram.Program.Name;
            moneyReceipt.RegNo = studentPayment.StudentProgram.Student.RegistrationNo;
            moneyReceipt.DateOFSubmission = studentPayment.CreationDate;
            moneyReceipt.PrRollNo = studentPayment.StudentProgram.PrnNo;
            moneyReceipt.StudentName = studentPayment.StudentProgram.Student.NickName;
            moneyReceipt.Branch = studentPayment.StudentProgram.Batch.Branch.Name;
            moneyReceipt.BatchDays = studentPayment.StudentProgram.Batch.Days;
            moneyReceipt.BatchName = studentPayment.StudentProgram.Batch.Name;
            moneyReceipt.BatchTime = studentPayment.StudentProgram.Batch.FormatedBatchTime;
            moneyReceipt.PayableAmount = (studentPayment.DueAmount + studentPayment.ReceivedAmount).ToString();
            moneyReceipt.PaidAmount = studentPayment.ReceivedAmount.ToString();
            moneyReceipt.CampusAddress = studentPayment.StudentProgram.Batch.Campus.Location;
            moneyReceipt.CampusContact = studentPayment.StudentProgram.Batch.Campus.ContactNumber;
            moneyReceipt.StudentPayment = studentPayment;
            var studentCList =
                _studentCourseDetailsService.LoadStudentCourseDetailsListByStudentProgramId(
                    studentPayment.StudentProgram.Id);
            moneyReceipt.CourseDetails = studentCList;
            moneyReceipt.DueAmount = studentPayment.DueAmount.ToString();
            if (studentPayment.NextReceivedDate != null)
                moneyReceipt.NextPaymentDate = (DateTime)studentPayment.NextReceivedDate;
            return moneyReceipt;
        }
        public string GetCouseViewDetails(string viewName, IList<Course> courseList, bool isChecked)
        {
            ViewBag.CourseViewModels = GetCouseViewModelList(courseList);
            ViewBag.IsChecked = isChecked;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
        
        public string GetExamBoardViewForStudent(long programId, long sessionid, long studentId = 0)
        {
            var viewName = "Partial/_ExamBoard";
            var list = _programBranchSessionService.LoadProgramStudentExamSession(programId, sessionid);
            
            ViewBag.TotalInfo = list.Count;
            ViewBag.HasBoardInfo = "NO";

            if (list.Count > 0)
            {
                ViewBag.HasBoardInfo = "YES";
                foreach (var item in list)
                {
                    var acaInfo = _studentAcademicInfoService.LoadByStudentId(studentId, item.StudentExam.Id);
                    if (acaInfo != null)
                    {
                        var bei = list.Where(x => x.StudentExam.Id == item.StudentExam.Id).FirstOrDefault();
                        if (bei != null)
                        {
                            bei.Year = acaInfo.Year;
                            bei.Board = acaInfo.StudentBoard.Id;
                            bei.Roll = acaInfo.BoradRoll;
                            bei.RegistrationNumber = acaInfo.RegistrationNumber;
                        }
                    }
                }
            }
                        
            ViewBag.ProgramStudentExamSession = list;
            ViewBag.BoardList = _studentBoardService.LoadActive();
             
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        private List<CourseViewModel> GetCouseViewModelList(IList<Course> courseList)
        {
            var courseViewModelList = courseList.Select(x => new CourseViewModel()
            {
                Id = x.Id,
                IsTaken = false,
                Name = x.Name,
                MaxSubject = x.MaxSubject,
                OfficeMinSub = x.OfficeMinSubject,
                PublicMinSubject = x.PublicMinSubject,
                OfficeMinPayment = x.OfficeMinPayment,
                PublicMinPayment = x.PublicMinPayment,
                IsOfficeCompulsary = x.OfficeCompulsory,
                IsPublicCompulsary = x.PublicCompulsory,
                
                ComplementaryCourses = x.ComplementaryCourses,

                SubjectViewModels = x.CourseSubjects.OrderBy(cs => cs.Subject.Rank).Select(y => new SubjectViewModel()
                {
                    Id = y.Id,
                    IsTaken = false,
                    Name = y.Subject.Name,
                    Payment = y.Payment
                }).ToList(),
            }).ToList();

            return courseViewModelList;
        }
        public ActionResult Error(string Message, bool withLayout = true)
        {
            ViewBag.WithLayout = withLayout;
            ViewBag.Message = Message;
            return View();
        }

        #region OLD IS GOLD

        [HttpPost]
        public JsonResult GetBatchByProgramSessionBranch(long programId = 0, long sessionId = 0, long branchId = 0, long campusId = 0, long batchId = 0, string batchDay = "", string batchTime = "")
        {
            if (batchDay == "Select Batch Day")
            {
                batchDay = "";
            }

            if (batchDay == "Select Batch Time")
            {
                batchDay = "";
            }

            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var campusList = _campusService.LoadAuthorizeCampus(userMenu, null, null, _commonHelper.ConvertIdToList(branchId));

            var batchList = _batchService.LoadBatch(null, _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId), _commonHelper.ConvertIdToList(branchId), _commonHelper.ConvertIdToList(campusId));
            batchList = batchList.OrderBy(x => x.Rank).ToList();
            List<DropDownOption> batchDayList = GetBatchDay(batchList.ToList(), batchId);
            List<DropDownOption> batchTimeList = GetBatchTime(batchList.ToList(), batchId, batchDay);
            batchList = GetFilteredBatchList(batchList.ToList(), batchDay, batchTime);

            return Json(new
            {
                CampusOptions = this.RenderRazorViewToOptions(DropDownOptionView, campusList, campusId, "Select Campus"),
                BatchOptions = this.RenderRazorViewToOptions(DropDownOptionView, batchList, batchId, "Select Batch Name"),
                BatchDayOptioins = this.RenderRazorViewToOptions(DropDownOptionView, batchDayList, batchDay, "Select Batch Day"),
                BatchTimeOptions = this.RenderRazorViewToOptions(DropDownOptionView, batchTimeList, batchTime, "Select Batch Time")
            });
        }

        private List<Batch> GetFilteredBatchList(List<Batch> batchList, string batchDay, string batchTime)
        {
            batchList.ForEach((x) =>
            {
                x.RemainingCapacity = x.Capacity - (x.StudentPrograms.Where(y => y.Batch == x).Select(y => y.Student)).Count();
                x.Name += " - " + x.RemainingCapacity;
            }
            );

            var filteredList = new List<Batch>();
            if (!string.IsNullOrEmpty(batchDay) && !string.IsNullOrEmpty(batchTime))
            {
                var list = batchList.Where(x => x.Days.Contains(batchDay) && x.FormatedBatchTime.Contains(batchTime));
                filteredList.AddRange(list.ToList());
            }
            else if (!string.IsNullOrEmpty(batchDay))
            {
                var list = batchList.Where(x => x.Days.Contains(batchDay));
                filteredList.AddRange(list.ToList());
            }
            else if (!string.IsNullOrEmpty(batchTime))
            {
                var list = batchList.Where(x => x.FormatedBatchTime.Contains(batchTime));
                filteredList.AddRange(list.ToList());
            }
            else
            {
                return batchList;
            }

            return filteredList;
        }


        private List<DropDownOption> GetBatchTime(List<Batch> batchList, long batchId, string batchDay)
        {
            IEnumerable<DropDownOption> batchTimeList;

            if (batchId != 0 || (batchId != 0 && string.IsNullOrEmpty(batchDay)))
            {
                batchTimeList = from b in batchList
                                where b.Id == batchId
                                select new DropDownOption() { Id = b.FormatedBatchTime, Name = b.FormatedBatchTime };
            }
            else if (!string.IsNullOrEmpty(batchDay) || (batchId == 0 && !string.IsNullOrEmpty(batchDay)))
            {
                batchTimeList = from b in batchList where b.Days == batchDay select new DropDownOption { Id = b.FormatedBatchTime, Name = b.FormatedBatchTime };
            }
            else if (batchId != 0 && !string.IsNullOrEmpty(batchDay))
            {
                batchTimeList = from b in batchList where b.Id == batchId && b.Days == batchDay select new DropDownOption { Id = b.FormatedBatchTime, Name = b.FormatedBatchTime };
            }
            else
            {
                batchTimeList = from b in batchList select new DropDownOption { Id = b.FormatedBatchTime, Name = b.FormatedBatchTime };
            }

            return batchTimeList.DistinctBy(x => x.Id).ToList();
        }

        private List<DropDownOption> GetBatchDay(List<Batch> batchList, long batchId)
        {
            IEnumerable<DropDownOption> batches = from b in batchList select new DropDownOption { Id = b.Days, Name = b.Days };
            batches = batches.ToList();

            if (batchId != 0)
            {
                batches = from b in batchList where b.Id == batchId select new DropDownOption { Id = b.Days, Name = b.Days };
            }
            var uBatchList = batches.DistinctBy(x => x.Id).ToList();
            return uBatchList;
        }

        #endregion
    }
}