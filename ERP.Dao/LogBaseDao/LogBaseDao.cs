﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessModel.Entity.LogBase;
using Microsoft.AspNet.Identity;

namespace UdvashERP.Dao.LogBaseDao
{
    public interface ILogBaseDao<TEntityT, TIdT>
    {
        ISession Session { get; set; }
        void Save(TEntityT data);
    }
    public class LogBaseDao<TEntityT, TIdT> : ILogBaseDao<TEntityT, TIdT>
    {
        private ISession _session;

        public ISession Session
        {
            get { return _session; }
            set { _session = value; }
        }
        public virtual void Save(TEntityT data)
        {

            ILogBaseEntity<long> entity = (ILogBaseEntity<long>)data;
            if (entity.CreationDate.Year < UdvashERP.BusinessRules.Constants.FirstBussinessYear)
                entity.CreationDate = DateTime.Now;

            entity.CreateBy = entity.CreateBy != 0 ? entity.CreateBy : GetCurrentUserId();
           
            _session.Save(data);

        }
        private long GetCurrentUserId()
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
                return Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
            return 0;
        }
    }
}
