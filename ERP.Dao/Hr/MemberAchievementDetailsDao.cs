﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IMemberAchievementDetailsDao : IBaseDao<MemberAchievementDetails, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        //MemberAchievement GetMemberAchievement(long teamMemberId, int year, int month);

        #endregion

        #region List Loading Function

        //IList<MemberAchievement> LoadMemberAchievementList(int start, int length, string orderBy, string orderDir, int year, int month, int teamMemberPin = 0);

        #endregion

        #region Others Function

        //int GetMemberAchievementCount(string orderBy, string orderDir, int year, int month, int teamMemberPin = 0);

        #endregion

        #region Helper Function

        #endregion
        
    }
    public class MemberAchievementDetailsDao : BaseDao<MemberAchievementDetails, long>, IMemberAchievementDetailsDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        //public MemberAchievement GetMemberAchievement(long teamMemberId, int year, int month)
        //{
        //    MonthsOfYear monthOfYear = (MonthsOfYear) month;
        //    return Session.QueryOver<MemberAchievement>().Where(x => x.Status != MemberAchievement.EntityStatus.Delete && x.Year == year && x.Month == monthOfYear && x.TeamMember.Id == teamMemberId).SingleOrDefault();
        //}

        #endregion

        #region List Loading Function


        //public IList<MemberAchievement> LoadMemberAchievementList(int start, int length, string orderBy, string orderDir, int year, int month, int teamMemberPin = 0)
        //{
        //    ICriteria criteria = GetMemberAchievementCriteria(orderBy, orderDir, year, month, teamMemberPin);
        //    return criteria.SetFirstResult(start).SetMaxResults(length).List<MemberAchievement>();
        //}

        #endregion

        #region Others Function

        //public int GetMemberAchievementCount(string orderBy, string orderDir, int year, int month, int teamMemberPin = 0)
        //{
        //    ICriteria criteria = GetMemberAchievementCriteria(orderBy, orderDir, year, month, teamMemberPin, true);
        //    criteria.SetProjection(Projections.RowCount());
        //    return Convert.ToInt32(criteria.UniqueResult());
        //}

        #endregion

        #region Helper Function

        //private ICriteria GetMemberAchievementCriteria(string orderBy, string orderDir, int year, int month, int teamMemberPin = 0, bool countQuery = false)
        //{
        //    ICriteria criteria = Session.CreateCriteria<MemberAchievement>().Add(Restrictions.Not(Restrictions.Eq("Status", MemberAchievement.EntityStatus.Delete)));
        //    criteria.CreateAlias("TeamMember", "teamMember");
        //    if (teamMemberPin != SelectionType.SelelectAll)
        //    {
        //        criteria.Add(Restrictions.Eq("teamMember.Pin", teamMemberPin));
        //    }
        //    if (year != SelectionType.SelelectAll)
        //    {
        //        criteria.Add(Restrictions.Eq("Year", year));
        //    }
        //    if (month != SelectionType.SelelectAll)
        //    {
        //        criteria.Add(Restrictions.Eq("Month", month));
        //    }
        //    if (!countQuery)
        //    {
        //        if (!String.IsNullOrEmpty(orderBy))
        //        {
        //            criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
        //        }
        //    }
        //    return criteria;
        //}

        #endregion
        
    }

}
