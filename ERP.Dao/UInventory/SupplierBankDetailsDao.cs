using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Remoting.Contexts;
using FluentNHibernate.Data;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Type;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface ISupplierBankDetailsDao : IBaseDao<SupplierBankDetails, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function

        bool IsBankAccountExistsByBankBranch(long id, string acNumber, long bankBranch);

        #endregion

    }

    public class SupplierBankDetailsDao : BaseDao<SupplierBankDetails, long>, ISupplierBankDetailsDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function

        public bool IsBankAccountExistsByBankBranch(long id, string acNumber, long bankBranch)
        {
            bool isExists = false;
            var entity = Session.QueryOver<SupplierBankDetails>()
                            .Where(x => x.BankBranch.Id == bankBranch)
                            .Where(x => x.AccountNo == acNumber)
                            .WhereNot(x => x.Id == id)
                            .WhereNot(x => x.Status == SupplierBankDetails.EntityStatus.Delete)
                            .SingleOrDefault<SupplierBankDetails>();
            if (entity != null)
                isExists = true;
            return isExists;
        }

        #endregion

        #region Helper Function

        #endregion
    }
}
