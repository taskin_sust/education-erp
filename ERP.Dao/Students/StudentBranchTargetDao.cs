﻿using System.Collections.Generic;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentBranchTargetDao : IBaseDao<StudentBranchTarget, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Functions
        
        #endregion             

        #region List Loading Functions
        IList<StudentBranchTarget> LoadBranchTargets(List<long> programIdList, List<long> branchIdList, long programId, long sessionId, string deadLineTitle, bool? loadAuthorizedTargets = null);
        
        IList<StudentBranchTarget> LoadByAdmissionTarget(long studentAdmissionTarget);
        #endregion   

        #region Others Function

        #endregion

        #region Clean
        //IList<StudentBranchTarget> LoadByBranchList(long[] branches);
        //IList<StudentBranchTarget> LoadByBranchList(long branchId);
        //StudentBranchTarget GetByAdmissionTarget(long studentAdmissionTarget, long branchId);
        #endregion
    }
    public class StudentBranchTargetDao : BaseDao<StudentBranchTarget, long>, IStudentBranchTargetDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Functions
        
        #endregion

        #region List Loading Functions
        public IList<StudentBranchTarget> LoadBranchTargets(List<long> programIdList, List<long> branchIdList,
               long programId, long sessionId, string deadLineTitle, bool? loadAuthorizedTargets = null)
        {
            StudentAdmissionTarget studentAdmissionTarget = null;
            var studentBranchTargets = Session.QueryOver<StudentBranchTarget>().JoinAlias(p => p.StudentAdmissionTarget, () => studentAdmissionTarget);
            if (loadAuthorizedTargets == true)
            {
                if (programIdList != null)
                {
                    studentBranchTargets =
                        studentBranchTargets.Where(x => studentAdmissionTarget.Program.Id.IsIn(programIdList));
                }
                if (branchIdList != null)
                {
                    studentBranchTargets =
                        studentBranchTargets.Where(x => x.Branch.Id.IsIn(branchIdList));
                } 
            }

            return studentBranchTargets.Where(x => studentAdmissionTarget.Program.Id == programId &&
                          studentAdmissionTarget.Session.Id == sessionId &&
                          studentAdmissionTarget.DeadLineTitle == deadLineTitle.Trim() &&
                          studentAdmissionTarget.Status == StudentAdmissionTarget.EntityStatus.Active
              ).List<StudentBranchTarget>();

        } 
        

        public IList<StudentBranchTarget> LoadByAdmissionTarget(long studentAdmissionTarget)
        {
            return
                Session.QueryOver<StudentBranchTarget>()
                    .Where(
                        x =>
                            x.Status == StudentBranchTarget.EntityStatus.Active &&
                            x.StudentAdmissionTarget.Id == studentAdmissionTarget).OrderBy(x => x.CreationDate).Desc
                    .List<StudentBranchTarget>();
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

        #region Clean
        //public IList<StudentBranchTarget> LoadByBranchList(long[] branches)
        //{
        //    var criteria = Session.CreateCriteria<StudentBranchTarget>();
        //    criteria.Add(Restrictions.In("Branch", branches));
        //    return criteria.List<StudentBranchTarget>();
        //}

        //public IList<StudentBranchTarget> LoadByBranchList(long branchId)
        //{
        //    var criteria = Session.CreateCriteria<StudentBranchTarget>();
        //    criteria.Add(Restrictions.Eq("Branch.Id", branchId));
        //    return criteria.List<StudentBranchTarget>();
        //}
        //public StudentBranchTarget GetByAdmissionTarget(long studentAdmissionTarget, long branchId)
        //{
        //    return
        //        Session.QueryOver<StudentBranchTarget>()
        //            .Where(
        //                x =>
        //                    x.Status == StudentBranchTarget.EntityStatus.Active &&
        //                    x.StudentAdmissionTarget.Id == studentAdmissionTarget && x.Branch.Id == branchId).OrderBy(x => x.CreationDate).Desc.Take(1)
        //            .SingleOrDefault<StudentBranchTarget>();
        //}
        #endregion
    }
}
