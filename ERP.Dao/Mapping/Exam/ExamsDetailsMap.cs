﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Exam;

namespace UdvashERP.Dao.Mapping.Exam
{
    public partial class ExamsDetailsMap: BaseClassMap<ExamsDetails,long>
    {
        public ExamsDetailsMap()
        {
            Table("ExamsDetails");
            LazyLoad();

            Map(x => x.SubjectType).Column("SubjectType");
            Map(x => x.ExamType).Column("ExamType");
            Map(x => x.QuestionFrom).Column("QuestionFrom");
            Map(x => x.QuestionTo).Column("QuestionTo");
            Map(x => x.TotalQuestion).Column("TotalQuestion");
            Map(x => x.MarksPerQuestion).Column("MarksPerQuestion");
            Map(x => x.NegativeMark).Column("NegativeMark");
            Map(x => x.PassMark).Column("PassMark");
            Map(x => x.WrittenFullMark).Column("WrittenFullMark");
            Map(x => x.IsCompulsary).Column("IsCompulsary");

            References(x => x.Subject).Column("SubjectId");
            References(x => x.Exams).Column("ExamId");
            HasMany(x => x.ExamsStudentAnswerScripts).KeyColumn("ExamsDetailsId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.ExamsStudentMarks).KeyColumn("ExamsDetailsId").Cascade.AllDeleteOrphan().Inverse(); 


        }
    }
}
