﻿/*Written By Habibul MOrsalin & sajjad raihan */
$(document).ready(function () {

    $('#informationViewList').bootstrapDualListbox({
        nonselectedlistlabel: 'Non-selected',
        selectedlistlabel: 'Selected',
        preserveselectiononmove: 'moved',
        moveOnSelect: false,
        showFilterInputs: false,
        infoText: '',
    });
    $(document).on('change', '#OrganizationId', function () {

        var organizationId = $(this).val();

        $('#ProgramId').empty();
        $('#ProgramId').append("<option value=''>Select Program</option>");
        $('#SessionId').empty();
        $('#SessionId').append("<option value=''>Select session</option>");
        $("#course option").remove();
        $("#branchId option").remove();
        $("#campusId option").remove();
        $('#batchDays option').remove();
        $('#batchTime option').remove();
        $('#batchName option').remove();
        
        if (organizationId != null && organizationId != "") {
            //$('#pmsg').text("");
            $.ajax({
                //url: $("body").attr("data-project-root") + "Student/StudentIdCard/AjaxRequestForProgramByOrganization",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                type: "POST",
                data: { organizationIds: organizationId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    //var listItems = "";
                    //$.each(response, function (index) {
                    //    listItems += "<option value='" + response[index].Value + "'>" + response[index].Text + "</option>";
                    //});
                    //$("#ProgramId").html(listItems);
                    if (response.IsSuccess) {
                        $.each(response.returnProgramList, function (i, v) {
                            $('#ProgramId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error:
                    function (response) {
                        // alert("Session load failed");
                        var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a> Session load failed</div>';
                        $('.customMessage').append(errorMessage);
                    }
            });
        }
        // SESSION
    });
    $(document).on('change', '#ProgramId', function () {
        
        // SESSION
        var programId = $("#ProgramId").val();

        $("#SessionId option").remove();
        $("#course option").remove();
        $("#branchId option").remove();
        $("#campusId option").remove();
        $('#batchDays option').remove();
        $('#batchTime option').remove();
        $('#batchName option').remove();
        //var x =
        if (programId != null && programId != "") {
            $('#pmsg').text("");
            $.ajax({
                //url: $("body").attr("data-project-root") + "Student/StudentIdCard/GetSession",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                type: "POST",
                data: { programIds: programId,isAuthorized:true },
                beforeSend: function() {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function(response) {
                    if (response.IsSuccess) {
                        $('#SessionId').append('<option value="">Select Session</option>');
                        $.each(response.returnSessionList, function (i, v) {
                            $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });

                        // $("#SessionId").find('option:eq(0)').prop('selected', true);
                    }
                    $.unblockUI();
                },
                complete: function() {
                    $.unblockUI();
                },
                error:
                    function(response) {
                        
                        var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a> Session load failed</div>';
                        $('.customMessage').append(errorMessage);
                        
                    }
            });
        }
        // SESSION
    });

    // PROGRAM


    // SESION

    $(document).on('change', "#SessionId", function () {

        // BRANCH && COURSE
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();

        $("#course option").remove();
        $("#branchId option").remove();
        $("#campusId option").remove();
        $('#batchDays option').remove();
        $('#batchTime option').remove();
        $('#batchName option').remove();
        if (sessionId != null && sessionId != "") {
            $('#smsg').text("");
        }
        if (programId != null && programId != "" && sessionId != null && sessionId != "") {

            // COURSE

            $.ajax({
                
                //url: $("body").attr("data-project-root") + "Student/StudentIdCard/GetCourseList",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCourse",
                type: "POST",
                data: { programIds: programId, sessionIds: sessionId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },

                success: function (response) {
                    if (response.IsSuccess) {
                        $('#course').append('<option value="0">All Course</option>');
                        $.each(response.returnCourse, function (i, v) {
                            $('#course').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        //$("#course").find('option:eq(0)').prop('selected', true);
                    } $.unblockUI();
                }, complete: function () {
                    $.unblockUI();
                },
                error: function (response) {
                   // alert("Branch load failed");
                    //console.log("Failed -----");
                    //console.log(response);
                    var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Branch load failed</div>';
                    $('.customMessage').append(errorMessage);
                }
            });
            // COURSE
            $.ajax({
                //url: $("body").attr("data-project-root") + "Student/StudentIdCard/AjaxRequestForBranch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                type: "POST",
                data: { programIds: programId, sessionIds: sessionId,isAuthorized:true },
                success: function (response) {
                    if (response.IsSuccess) {
                        $('#branchId').append('<option value="0">All Branch</option>');
                        $.each(response.returnBranchList, function (i, v) {
                            $('#branchId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        //CAMPUS
                    }
                }, complete: function () {
                    $.unblockUI();
                },
                error: function (response) {
                    //alert("Branch load failed");
                    //console.log("Failed -----");
                    //console.log(response);
                    var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Branch load failed</div>';
                    $('.customMessage').append(errorMessage);
                }
            });
        }

        //BRANCH
    });
    
    // SESSION


    // BRANCH

    $(document).on('change', "#branchId", function () {

        $("#campusId option").remove();
        $('#batchDays option').remove();
        $('#batchTime option').remove();
        $('#batchName option').remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#branchId").val();
        if (branchId != null && branchId != "") {
            $('#brmsg').text("");
        }
        if (programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "") {
            $.ajax({
               
                //url: $("body").attr("data-project-root") + "Student/StudentIdCard/AjaxRequestForCampus",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                type: "POST",
                data: { branchIds: branchId, programIds: programId, sessionIds: sessionId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    if (response.IsSuccess) {
                        $('#campusId').append('<option value="0">All Campus</option>');
                        $.each(response.returnCampusList, function (i, v) {
                            $('#campusId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    

                        // BATCH
                    } $.unblockUI();
                }, complete: function () {
                    $.unblockUI();
                },
                error: function (response) {
                   
                    var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Campus load failed</div>';
                    $('.customMessage').append(errorMessage);
                }
            });
        }

    });

    // BRANCH
    
    // CAMPUS
    $(document).on('change', "#campusId", function () {

        $('#batchDays option').remove();
        $('#batchTime option').remove();
        $('#batchName option').remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#branchId").val();
        var campusId = $("#campusId").val();
        if (campusId != null && campusId != "") {
            $('#campmsg').text("");
        }
        if (programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "" && campusId != null && campusId != "") {
            $.ajax({
                //url: $("body").attr("data-project-root") + "Student/StudentIdCard/AjaxRequestForBatchDay",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchDay",
                type: "POST",
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    if (response.IsSuccess) {
                        $('#batchDays').append('<option value="0">All Batch Days</option>');
                        $.each(response.returnBatchDays, function (i, v) {
                            $('#batchDays').append($('<option>').text(v.Text).attr('value', v.Value));
                        });

                    } $.unblockUI();
                }, complete: function () {
                    $.unblockUI();
                },
                error: function (response) {
                    var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Batch Days Time load failed</div>';
                    $('.customMessage').append(errorMessage);
                }
            });
        }
    });
    
   

    //Batch Day Change Functionalities
    $(document).on('change', '#batchDays', function () {
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var campusId = $("#campusId").val();
        var batchDays = $("#batchDays").val();
        var branchId = $("#branchId").val();
        
        $("#batchTime option").remove();
        $("#batchName option").remove();
        if (batchDays != "" && batchDays != null) {
            $('#bdmsg').text("");
        }

        if (sessionId != "" && programId != "" && campusId != "" && campusId != null && batchDays != "" && batchDays != null) {
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Student/StudentIdCard/AjaxRequestForBatchTime",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchTime",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId, batchDays: batchDays },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    if (result.IsSuccess) {
                        $('#batchTime').append('<option value="0">All Batch Times</option>');
                        $.each(result.returnBatchTime, function (i, v) {
                            $('#batchTime').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        
                    }
                    else {

                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                }, complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });

    //Batch Time Change Functionalities
    $(document).on('change', '#batchTime', function () {
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var campusId = $("#campusId").val();
        var batchDays = $("#batchDays").val();
        var batchTime = $("#batchTime").val();
        var branchId = $("#branchId").val();
        var batchTimeText = [];
        $("#batchTime option:selected").each(function () {
            var $this = $(this);
            if ($this.length) {
                batchTimeText.push($this.text());
            }
        });
        // console.log(batchTimeText);
        if (batchTime != null || batchTime != "") {
            $('#btmsg').text("");
        }
        $("#batchName option").remove();


        if (sessionId != "" && programId != "" && campusId != "" && campusId != null  && batchTime != "" && batchTime != null) {
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Student/StudentIdCard/AjaxRequestForBatch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatch",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, campusIds: campusId,branchIds:branchId, batchDays: batchDays, batchTimes: batchTime },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    if (result.IsSuccess) {
                        $('#batchName').append('<option value="0">All Batch</option>');
                        $.each(result.returnBatch, function (i, v) {
                            $('#batchName').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    else {

                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }
    });
    $(document).on('change', '#batchName', function () {
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#branchId").val();
        var campusId = $("#campusId").val();
        var batchDays = $("#batchDays").val();
        var batchTime = $("#batchTime").val();
        var batchName = $("#batchName").val();
        if (batchName != null || batchName != "") {
            $('#bnmsg').text("");
        }

    });
    $(document).on('change', '#course', function () {
        if ($("#course").val() != null || $("#course").val() != "") {
            $('#cmsg').text("");
        }
    });
    $(document).on('change', '#informationViewList', function () {
        if ($("#informationViewList").val() != null || $("#informationViewList").val() != "") {
            $('#infomsg').text("");
        }
    });
    $(document).on('click', '#Submit', function (e) {
        $('#pmsg').text("");
        $('#smsg').text("");
        $('#brmsg').text("");
        $('#campmsg').text("");
        $('#bdmsg').text("");
        $('#btmsg').text("");
        $('#bnmsg').text("");
        $('#cmsg').text("");
        $('#infomsg').text("");
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#branchId").val();
        var campusId = $("#campusId").val();
        var batchDays = $("#batchDays").val();
        var batchTime = $("#batchTime").val();
        var batchName = $("#batchName").val();
        var course = $("#course").val();
        //var paymentStatus = $("#paymentStatus").val();
       // var imageStatus = $("#imageStatus").val();
        var printingStatus = $("#printingStatus").val();
        var informationViewList = $("#informationViewList").val();
     
        var isSuccess = true;
        if (programId == null || programId <= 0) {
            $('#pmsg').text("");
            var programMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="pmsg">Select Program</span></span>';
            $('#pmsg').append(programMessage);
            isSuccess = false;
        }

        if (sessionId == null || sessionId <= 0) {
            $('#smsg').text("");
            var sessionMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="smsg">Select Session</span></span>';

            $('#smsg').append(sessionMessage);
            isSuccess = false;
        }

        if (branchId == null || branchId == "") {
            $('#brmsg').text("");
            var branchMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="brmsg">Select Branch</span></span>';
            $('#brmsg').append(branchMessage);
            isSuccess = false;
        }

        if (campusId == null || campusId == "") {
            $('#campmsg').text("");
            var campusMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="campmsg">Select Campus</span></span>';
            $('#campmsg').append(campusMessage);
            isSuccess = false;
        }

        if (batchDays == null || batchDays == "") {
            $('#bdmsg').text("");
            var batchDayMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="bdmsg">Select Batch Day</span></span>';
            $('#bdmsg').append(batchDayMessage);
            isSuccess = false;
        }

        if (batchTime == null || batchTime == "") {
            $('#btmsg').text("");
            var batchTimeMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="btmsg">Select Batch Time</span></span>';
            $('#btmsg').append(batchTimeMessage);
            isSuccess = false;
        }

        if (batchName == null || batchName == "") {
            $('#bnmsg').text("");
            var batchNameMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="bmsg">Select Batch</span></span>';
            $('#bnmsg').append(batchNameMessage);
            isSuccess = false;
        }


        if (course == null || course == "") {
            $('#cmsg').text("");
            var batchNameMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="coursemsg">Select Course</span></span>';
            $('#cmsg').append(batchNameMessage);
            isSuccess = false;
        }
        if (informationViewList == null || informationViewList == "") {
            $('#infomsg').text("");
            var batchNameMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="infomsg">Select at least one information</span></span>';
            $('#infomsg').append(batchNameMessage);
            isSuccess = false;
        }
        if (isSuccess == true) {
            $('form#generateIdCardForm').submit();
            return true;
        } else {
            e.preventDefault();
            return false;
        }
    });
    

});