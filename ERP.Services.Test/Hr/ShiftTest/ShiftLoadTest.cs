﻿using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ShiftTest
{

    [Trait("Area", "Hr")]
    [Trait("Service", "Shift")]
    public class ShiftLoadTest : ShiftBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
           var obj = _shiftService.LoadById(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            _shiftFactory.Create().WithOrganization().WithNameAndRank(_name, 1).WithStartTimeEndTime(_startTime, _endTime).Persist();
            var obj = _shiftService.LoadById(_shiftFactory.Object.Id);
            Assert.NotNull(obj);
        }

        [Fact]
        public void LoadActive_should_return_list()
        {
            _shiftFactory.CreateMore(100).Persist();
            IList<Shift> shiftList = _shiftService.LoadShift(organizationIds: commonHelper.ConvertIdToList(_shiftFactory.GetLastCreatedObjectList()[0].Organization.Id));
            Assert.Equal(100, shiftList.Count);
        }

        [Fact]
        public void LoadActive_should_Not_return_list()
        {
            int totalrowCountWithOrganization = _shiftService.GetShiftRowCount(new List<UserMenu>(), "", "Asc", -1);
            IList<Shift> listWithOrganization = _shiftService.LoadShift(new List<UserMenu>(), "", "Asc", -1);
            Assert.Equal(0, totalrowCountWithOrganization);
            Assert.Equal(0, listWithOrganization.Count);
        }

    }
}
