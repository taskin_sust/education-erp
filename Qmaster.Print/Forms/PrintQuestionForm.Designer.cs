﻿namespace Qmaster.Print.Forms
{
    partial class PrintQuestionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbOrganization = new System.Windows.Forms.ComboBox();
            this.bsOrganizationList = new System.Windows.Forms.BindingSource(this.components);
            this.cbProgram = new System.Windows.Forms.ComboBox();
            this.bsProgramList = new System.Windows.Forms.BindingSource(this.components);
            this.cbSession = new System.Windows.Forms.ComboBox();
            this.bsSessionList = new System.Windows.Forms.BindingSource(this.components);
            this.cbCourse = new System.Windows.Forms.ComboBox();
            this.bsCourseList = new System.Windows.Forms.BindingSource(this.components);
            this.cbVersion = new System.Windows.Forms.ComboBox();
            this.bsVersionList = new System.Windows.Forms.BindingSource(this.components);
            this.txtbExamCode = new System.Windows.Forms.TextBox();
            this.txtbPrintQuestionNo = new System.Windows.Forms.TextBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.probarPrintQuestion = new System.Windows.Forms.ProgressBar();
            this.txtbExamName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listUniqueSet = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnLoadOrganization = new System.Windows.Forms.Button();
            this.btnReload = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.progressText = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bsOrganizationList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsProgramList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSessionList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsCourseList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsVersionList)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Organization : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Session : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(501, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Program : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(512, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Course : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Exam Code : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(471, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "Exam Name : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(56, 163);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 18);
            this.label7.TabIndex = 6;
            this.label7.Text = "Version : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(-2, 204);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 18);
            this.label8.TabIndex = 7;
            this.label8.Text = "Total Print No. : ";
            // 
            // cbOrganization
            // 
            this.cbOrganization.DataSource = this.bsOrganizationList;
            this.cbOrganization.DisplayMember = "ShortName";
            this.cbOrganization.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOrganization.Location = new System.Drawing.Point(149, 37);
            this.cbOrganization.Name = "cbOrganization";
            this.cbOrganization.Size = new System.Drawing.Size(241, 26);
            this.cbOrganization.TabIndex = 0;
            this.cbOrganization.ValueMember = "Id";
            this.cbOrganization.SelectedIndexChanged += new System.EventHandler(this.cbOrganization_SelectedIndexChanged);
            // 
            // bsOrganizationList
            // 
            this.bsOrganizationList.DataSource = typeof(Qmaster.Print.Model.Organization);
            // 
            // cbProgram
            // 
            this.cbProgram.DataSource = this.bsProgramList;
            this.cbProgram.DisplayMember = "FullName";
            this.cbProgram.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProgram.FormattingEnabled = true;
            this.cbProgram.Location = new System.Drawing.Point(601, 40);
            this.cbProgram.Name = "cbProgram";
            this.cbProgram.Size = new System.Drawing.Size(241, 26);
            this.cbProgram.TabIndex = 1;
            this.cbProgram.ValueMember = "Id";
            this.cbProgram.SelectedIndexChanged += new System.EventHandler(this.cbProgram_SelectedIndexChanged);
            // 
            // bsProgramList
            // 
            this.bsProgramList.DataSource = typeof(Qmaster.Print.Model.Program);
            // 
            // cbSession
            // 
            this.cbSession.DataSource = this.bsSessionList;
            this.cbSession.DisplayMember = "Name";
            this.cbSession.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSession.FormattingEnabled = true;
            this.cbSession.Location = new System.Drawing.Point(149, 78);
            this.cbSession.Name = "cbSession";
            this.cbSession.Size = new System.Drawing.Size(241, 26);
            this.cbSession.TabIndex = 2;
            this.cbSession.ValueMember = "Id";
            this.cbSession.SelectedIndexChanged += new System.EventHandler(this.cbSession_SelectedIndexChanged);
            // 
            // bsSessionList
            // 
            this.bsSessionList.DataSource = typeof(Qmaster.Print.Model.Session);
            // 
            // cbCourse
            // 
            this.cbCourse.DataSource = this.bsCourseList;
            this.cbCourse.DisplayMember = "Name";
            this.cbCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCourse.FormattingEnabled = true;
            this.cbCourse.Location = new System.Drawing.Point(601, 80);
            this.cbCourse.Name = "cbCourse";
            this.cbCourse.Size = new System.Drawing.Size(241, 26);
            this.cbCourse.TabIndex = 3;
            this.cbCourse.ValueMember = "Id";
            this.cbCourse.SelectedIndexChanged += new System.EventHandler(this.cbCourse_SelectedIndexChanged);
            // 
            // bsCourseList
            // 
            this.bsCourseList.DataSource = typeof(Qmaster.Print.Model.Course);
            // 
            // cbVersion
            // 
            this.cbVersion.DataSource = this.bsVersionList;
            this.cbVersion.DisplayMember = "Name";
            this.cbVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVersion.FormattingEnabled = true;
            this.cbVersion.Location = new System.Drawing.Point(149, 161);
            this.cbVersion.Name = "cbVersion";
            this.cbVersion.Size = new System.Drawing.Size(241, 26);
            this.cbVersion.TabIndex = 5;
            this.cbVersion.ValueMember = "Id";
            // 
            // bsVersionList
            // 
            this.bsVersionList.DataSource = typeof(Qmaster.Print.Model.Version);
            // 
            // txtbExamCode
            // 
            this.txtbExamCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtbExamCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtbExamCode.Location = new System.Drawing.Point(149, 119);
            this.txtbExamCode.Name = "txtbExamCode";
            this.txtbExamCode.Size = new System.Drawing.Size(241, 27);
            this.txtbExamCode.TabIndex = 4;
            this.txtbExamCode.TextChanged += new System.EventHandler(this.txtbExamCode_TextChanged);
            this.txtbExamCode.Enter += new System.EventHandler(this.txtbExamCode_Enter);
            this.txtbExamCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbExamCode_KeyPress);
            this.txtbExamCode.Leave += new System.EventHandler(this.txtbExamCode_Leave);
            // 
            // txtbPrintQuestionNo
            // 
            this.txtbPrintQuestionNo.Location = new System.Drawing.Point(149, 202);
            this.txtbPrintQuestionNo.Name = "txtbPrintQuestionNo";
            this.txtbPrintQuestionNo.Size = new System.Drawing.Size(241, 27);
            this.txtbPrintQuestionNo.TabIndex = 6;
            this.txtbPrintQuestionNo.TextChanged += new System.EventHandler(this.txtbPrintQuestionNo_TextChanged);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(539, 289);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(154, 33);
            this.btnPrint.TabIndex = 7;
            this.btnPrint.Text = "Print Question";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // probarPrintQuestion
            // 
            this.probarPrintQuestion.Location = new System.Drawing.Point(23, 250);
            this.probarPrintQuestion.Name = "probarPrintQuestion";
            this.probarPrintQuestion.Size = new System.Drawing.Size(850, 21);
            this.probarPrintQuestion.TabIndex = 14;
            // 
            // txtbExamName
            // 
            this.txtbExamName.Location = new System.Drawing.Point(601, 120);
            this.txtbExamName.Name = "txtbExamName";
            this.txtbExamName.ReadOnly = true;
            this.txtbExamName.Size = new System.Drawing.Size(241, 27);
            this.txtbExamName.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox1.Controls.Add(this.listUniqueSet);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.btnLoadOrganization);
            this.groupBox1.Controls.Add(this.btnReload);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.progressText);
            this.groupBox1.Controls.Add(this.btnStop);
            this.groupBox1.Controls.Add(this.cbOrganization);
            this.groupBox1.Controls.Add(this.probarPrintQuestion);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnPrint);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtbExamName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtbPrintQuestionNo);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtbExamCode);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cbVersion);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbCourse);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbSession);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cbProgram);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(17, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(905, 362);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // listUniqueSet
            // 
            this.listUniqueSet.FormattingEnabled = true;
            this.listUniqueSet.ItemHeight = 18;
            this.listUniqueSet.Location = new System.Drawing.Point(601, 153);
            this.listUniqueSet.Name = "listUniqueSet";
            this.listUniqueSet.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listUniqueSet.Size = new System.Drawing.Size(241, 76);
            this.listUniqueSet.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(472, 165);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 18);
            this.label9.TabIndex = 21;
            this.label9.Text = "Unique Set  : ";
            // 
            // btnLoadOrganization
            // 
            this.btnLoadOrganization.Location = new System.Drawing.Point(60, 289);
            this.btnLoadOrganization.Name = "btnLoadOrganization";
            this.btnLoadOrganization.Size = new System.Drawing.Size(189, 33);
            this.btnLoadOrganization.TabIndex = 19;
            this.btnLoadOrganization.Text = "Load Organization";
            this.btnLoadOrganization.UseVisualStyleBackColor = true;
            this.btnLoadOrganization.Click += new System.EventHandler(this.btnLoadOrganization_Click);
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(272, 289);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(140, 33);
            this.btnReload.TabIndex = 18;
            this.btnReload.Text = "Refresh Config";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(716, 289);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(81, 33);
            this.btnClose.TabIndex = 17;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // progressText
            // 
            this.progressText.AutoSize = true;
            this.progressText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressText.Location = new System.Drawing.Point(432, 252);
            this.progressText.Name = "progressText";
            this.progressText.Size = new System.Drawing.Size(27, 16);
            this.progressText.TabIndex = 16;
            this.progressText.Text = "0%";
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(435, 289);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(81, 33);
            this.btnStop.TabIndex = 15;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // PrintQuestionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 429);
            this.Controls.Add(this.groupBox1);
            this.Name = "PrintQuestionForm";
            this.Text = "Print Question";
            this.Load += new System.EventHandler(this.PrintQuestionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bsOrganizationList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsProgramList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSessionList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsCourseList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsVersionList)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbOrganization;
        private System.Windows.Forms.ComboBox cbProgram;
        private System.Windows.Forms.ComboBox cbSession;
        private System.Windows.Forms.ComboBox cbCourse;
        private System.Windows.Forms.ComboBox cbVersion;
        private System.Windows.Forms.TextBox txtbExamCode;
        private System.Windows.Forms.TextBox txtbPrintQuestionNo;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.ProgressBar probarPrintQuestion;
        private System.Windows.Forms.TextBox txtbExamName;
        private System.Windows.Forms.BindingSource bsOrganizationList;
        private System.Windows.Forms.BindingSource bsProgramList;
        private System.Windows.Forms.BindingSource bsSessionList;
        private System.Windows.Forms.BindingSource bsCourseList;
        private System.Windows.Forms.BindingSource bsVersionList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label progressText;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.Button btnLoadOrganization;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox listUniqueSet;
    }
}