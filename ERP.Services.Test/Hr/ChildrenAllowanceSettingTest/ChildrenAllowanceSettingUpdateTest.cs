﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.ChildrenAllowanceSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "ChildrenAllowanceSettingService")]
    public class ChildrenAllowanceSettingUpdateTest : ChildrenAllowanceSettingBaseTest
    {
        #region Edit Test

        [Fact]
        public void Should_Return_AuthenticationException_ForEdit()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            Assert.Throws<AuthenticationException>(
                () => _childrenAllowanceSettingService.SaveOrUpdate(null, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException_ForEdit()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            _childrenAllowanceSettingFactory.Object.EffectiveDate = DateTime.Now.AddDays(5);
            _childrenAllowanceSettingFactory.Object.ClosingDate = DateTime.Now;
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        #endregion

        #region Basic Test

        [Fact]
        public void Should_Return_NullObjectException_ForEdit()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            Assert.Throws<NullObjectException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, null));
        }

        [Fact]
        public void Should_Edited_Succesfully()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _childrenAllowanceSettingFactory.Object.Amount = 250;
            _childrenAllowanceSettingFactory.Object.StartingAge = 1;
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            Assert.True(_childrenAllowanceSettingFactory.Object.Id > 0);
        }

        #endregion

        #region Authorization Check

        [Fact]
        public void Should_Return_UnAuthorized_Menu_Message()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { _organizationFactory.Create().Persist().Object }, null, null);
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true));
        }

        #endregion

        #region Invalid Property

        [Fact]
        public void Should_Check_MaxNoOfChild_For_Range_MinValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            _childrenAllowanceSettingFactory.Object.MaxNoOfChild = 0;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_MaxNoOfChild_For_Range_MaxValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            _childrenAllowanceSettingFactory.Object.MaxNoOfChild = 11;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_Amount_For_Range_MinValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            _childrenAllowanceSettingFactory.Object.Amount = 0;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_Amount_For_Range_MaxValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            _childrenAllowanceSettingFactory.Object.Amount = 1000001;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_StartingAge_For_Range_MinValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            _childrenAllowanceSettingFactory.Object.StartingAge = -1;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_StartingAge_For_Range_MaxValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            _childrenAllowanceSettingFactory.Object.StartingAge = 101;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }
        [Fact]
        public void Should_Check_ClosingAge_For_Range_MinValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            _childrenAllowanceSettingFactory.Object.ClosingAge = -1;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_ClosingAge_For_Range_MaxValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            _childrenAllowanceSettingFactory.Object.ClosingAge = 101;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_PaymentType_For_Range_MinValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            _childrenAllowanceSettingFactory.Object.PaymentType = 1;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_PaymentType_For_Range_MaxValue()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);
            _childrenAllowanceSettingFactory.Object.PaymentType = 5;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        #endregion

        #region Business Logic

        [Fact]
        public void Should_Return_AuthenticationException()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            Assert.Throws<AuthenticationException>(
                () => _childrenAllowanceSettingService.SaveOrUpdate(null, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_Closing_Date()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            _childrenAllowanceSettingFactory.Object.EffectiveDate = DateTime.Now.AddDays(5);
            _childrenAllowanceSettingFactory.Object.ClosingDate = DateTime.Now;
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_PaymentType()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _childrenAllowanceSettingFactory.Object.PaymentType = Convert.ToInt32(null);
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_MaxNoOfChild()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _childrenAllowanceSettingFactory.Object.MaxNoOfChild = 0;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_Amount()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _childrenAllowanceSettingFactory.Object.Amount = 0;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_StartingAge()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _childrenAllowanceSettingFactory.Object.StartingAge = -1;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_ClosingAge()
        {
            _organizationFactory.Create().Persist();
            _childrenAllowanceSettingFactory.Create().WithOrganization(_organizationFactory.Object);
            var menu = BuildUserMenu(commonHelper.ConvertIdToList(_organizationFactory.Object));
            _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object);

            _childrenAllowanceSettingFactory.Object.ClosingAge = 0;
            Assert.Throws<InvalidDataException>(() => _childrenAllowanceSettingService.SaveOrUpdate(menu, _childrenAllowanceSettingFactory.Object));
        }

        [Fact]
        public void Should_Check_Empty_ChildrenSetting()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel);
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_Check_Empty_ChildrenInfo()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel, true);
            allowanceSheetList[0].AllowanceSheetSecondDetails[0].ChildrenInfo = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_Check_Empty_TeamMember()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel);
            allowanceSheetList[0].TeamMember = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_check_EmptyDepartment()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel);
            allowanceSheetList[0].AllowanceSheetSecondDetails[0].TeamMemberDepartment = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_check_Empty_Designation()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel);
            allowanceSheetList[0].AllowanceSheetSecondDetails[0].TeamMemberDesignation = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));
        }

        [Fact]
        public void Should_check_Empty_Branch()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel);
            allowanceSheetList[0].AllowanceSheetSecondDetails[0].TeamMemberBranch = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));

        }

        [Fact]
        public void Should_check_Empty_Campus()
        {
            TeamMember member = PersistTeamMember(new DateTime(2016, 01, 01), new DateTime(2016, 01, 01));
            Organization org = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Organization;
            List<UserMenu> menus = BuildUserMenu(new List<Organization>() { org }, null, new List<Branch>() { member.EmploymentHistory.FirstOrDefault().Campus.Branch });
            _childrenAllowanceSettingFactory.Create().WithStartAndClosingAge(0, 10)
                .WithEffectiveAndCloxingDate(new DateTime(2015, 01, 01), new DateTime(2020, 12, 30)).WithOrganization(org).Persist(menus);
            AllowanceSheetFormViewModel sheetFormViewModel = new AllowanceSheetFormViewModel()
            {
                BranchId = member.EmploymentHistory.FirstOrDefault().Campus.Branch.Id,
                CampusId = member.EmploymentHistory.FirstOrDefault().Campus.Id,
                DepartmentId = member.EmploymentHistory.FirstOrDefault().Department.Id,
                Month = 1,
                OrganizationId = org.Id,
                PinList = member.Pin.ToString(),
                TeamMemberSearchType = (AllowanceSheetTeamMemberSearchType)1,
                Year = 2016
            };
            List<AllowanceSheet> allowanceSheetList = _allowanceSheetService.LoadTeamMemberAllowanceSheet(menus, sheetFormViewModel);
            allowanceSheetList[0].AllowanceSheetSecondDetails[0].TeamMemberCampus = null;
            Assert.Throws<InvalidDataException>(() => _allowanceSheetService.SaveOrUpdateTeamMemberAllowanceSheet(allowanceSheetList));

        }


        #endregion
    }
}
