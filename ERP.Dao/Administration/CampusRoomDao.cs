﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Administration
{
    public interface ICampusRoomDao : IBaseDao<CampusRoom, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<CampusRoom> LoadByBranchId(long branchId);
        IList<CampusRoom> LoadByCampusIds(string[] campusIds);
        #endregion

        #region Others Function

        #endregion
    }
    public class CampusRoomDao : BaseDao<CampusRoom, long>, ICampusRoomDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<CampusRoom> LoadByBranchId(long branchId)
        {
            ICriteria criteria = Session.CreateCriteria<CampusRoom>();
            criteria.Add(Restrictions.Eq("Status", CampusRoom.EntityStatus.Active));
            criteria.CreateCriteria("Campus", "c").Add(Restrictions.Eq("c.Branch.Id", branchId)).Add(Restrictions.Eq("c.Status", Campus.EntityStatus.Active));
            return criteria.List<CampusRoom>();
        }
        public IList<CampusRoom> LoadByCampusIds(string[] campusIds)
        {
            ICriteria criteria = Session.CreateCriteria<CampusRoom>();
            criteria.Add(Restrictions.Eq("Status", CampusRoom.EntityStatus.Active));
            criteria.CreateCriteria("Campus", "c").Add(Restrictions.In("c.Id", campusIds)).Add(Restrictions.Eq("c.Status", Campus.EntityStatus.Active));
            return criteria.List<CampusRoom>();
        }
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}