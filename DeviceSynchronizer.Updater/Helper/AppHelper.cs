﻿using System;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using log4net;
using Synchronizer.Updater.BusinessRule;

namespace Synchronizer.Updater.Helper
{
    public static class AppHelper
    {

        private static readonly ILog Log = LogManager.GetLogger("HelperLogger");

        #region Synchronizer Config File

        public static string GetAppVersion()
        {
            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            string path = Application.StartupPath + "\\" + "synchronizer.config";

            fileMap.ExeConfigFilename = path;
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            var returnValue = config.AppSettings.Settings["AppVersion"];
            return returnValue.Value;
        }

        #endregion

        #region Helper Function

        public static DirectoryInfo GetRequiredDirectory(RequiredDirectory value = RequiredDirectory.Base)
        {
            DirectoryInfo returnDir = null;
            try
            {   //init     
                DirectoryInfo baseDir = new DirectoryInfo(Application.StartupPath).Parent;

                //create directory as per requirements
                if (value == RequiredDirectory.Base)
                    returnDir = baseDir;
                else if (value == RequiredDirectory.Synchronizer)
                    returnDir = new DirectoryInfo(baseDir.FullName + "\\" + UpdaterConstant.SynchronizerDirectory);
                else if (value == RequiredDirectory.Backup)
                    returnDir = new DirectoryInfo(baseDir.FullName + "\\" + UpdaterConstant.BackupDirectory);
                else if (value == RequiredDirectory.Download)
                    returnDir = new DirectoryInfo(baseDir.FullName + "\\" + UpdaterConstant.DownloadDirectory);

                //create new directory if directory is not exist
                if (returnDir != null && returnDir.Exists == false)
                    returnDir.Create();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return returnDir;
        }

        public enum RequiredDirectory
        {
            Base,
            Synchronizer,
            Backup,
            Download
        }

        public static bool IsSynchronizerRunning()
        {
            bool newInstance;
            string appName = SoftwareInfo.DeviceSynchronizerAppName;
            Mutex mutex = new Mutex(true, appName, out newInstance);
            return !newInstance;
        }

        #endregion
    }
}
