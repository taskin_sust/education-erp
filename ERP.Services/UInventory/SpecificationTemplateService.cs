using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Conventions;
using FluentNHibernate.Testing.Values;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;

namespace UdvashERP.Services.UInventory
{
    public interface ISpecificationTemplateService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(SpecificationTemplate specificationTemplate);

        bool Delete(long id);
        #endregion

        #region Single Instances Loading Function
        SpecificationTemplate GetSpecificationTemplate(long id);

        #endregion

        #region List Loading Function

        IList<SpecificationTemplateDto> LoadSpecificationTemplateList(int start, int length, string searchString = "");

        IList<SpecificationTemplate> LoadISpecificationTemplateList(int? status = null);
        #endregion

        #region Others Function

        int SpecificationTemplateRowCount(string searchString);

        #endregion

        #region Helper Function

        #endregion}


    }
    public class SpecificationTemplateService : BaseService, ISpecificationTemplateService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("InventoryService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly ISpecificationTemplateDao _specificationTemplateDao;
        private readonly ISpecificationCriteriaDao _specificationCriteriaDao;
        private readonly ISpecificationCriteriaOptionDao _specificationCriteriaOptionDao;
        private readonly ISpecificationCriteriaLogDao _specificationCriteriaLogDao;

        public SpecificationTemplateService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _specificationTemplateDao = new SpecificationTemplateDao { Session = session };
            _specificationCriteriaDao = new SpecificationCriteriaDao { Session = session };
            _specificationCriteriaOptionDao = new SpecificationCriteriaOptionDao() { Session = session };
            _specificationCriteriaLogDao = new SpecificationCriteriaLogDao() { Session = session };
        }

        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(SpecificationTemplate specificationTemplate)
        {
            ITransaction trans = null;
            try
            {
                var spclog = new List<SpecificationCriteriaLog>();

                if (specificationTemplate == null)
                {
                    throw new NullObjectException("Specification Template can not be null");
                }

                CheckSpecificationTemplateDuplication(specificationTemplate);

                //Check Entity Validation for Specification Template
                SpecificationTemplateValidation(specificationTemplate);


                if (!EnumerableExtensions.Any(specificationTemplate.SpecificationCriteriaList))
                {
                    throw new InvalidDataException("One or more criteria must be add.");
                }

                //Chack Criteria Duplication
                CheckCriteriaDuplication(specificationTemplate);
                int rankCriteria = 1;
                foreach (var spc in specificationTemplate.SpecificationCriteriaList)
                {
                    //Check Entity Validation for Specification Criteria
                    SpecificationCriteriaValidation(spc);

                    // spc.Rank = _specificationCriteriaDao.GetMaximumRank(spc) + 1;
                    spc.Rank = rankCriteria++;
                    spc.Status = SpecificationCriteria.EntityStatus.Active;
                    if (System.Web.HttpContext.Current != null)
                    {
                        spc.CreateBy = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                        spc.ModifyBy = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    }

                    string optionString = "";

                    if (spc.Controltype == FieldType.Dropdown)
                    {
                        if (!EnumerableExtensions.Any(spc.SpecificationCriteriaOptions))
                        {
                            throw new InvalidDataException("One or more criteria option must be add for +" +
                                                           spc.Name);
                        }

                        //Chack Criteria Option Duplication
                        CheckCriteriaOptionDuplication(spc);

                        int rankOption = 1;
                        foreach (var spco in spc.SpecificationCriteriaOptions)
                        {
                            //Check Entity Validation for Specification Criteria Options
                            SpecificationCriteriaOptionValidation(spco);

                            //spco.Rank = _specificationCriteriaOptionDao.GetMaximumRank(spco) + 1;
                            spco.Rank = rankOption++;
                            spco.Status = SpecificationCriteriaOption.EntityStatus.Active;
                            if (System.Web.HttpContext.Current != null)
                            {
                                spco.CreateBy = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                                spco.ModifyBy = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                            }

                            optionString = optionString + spco.Name + ",";
                        }
                        if (optionString != "")
                        {
                            optionString = optionString.Remove(optionString.Length - 1);
                        }
                    }

                    //Add Criteria log 
                    var spcLogObj = new SpecificationCriteriaLog();
                    spcLogObj.Rank = spc.Rank;
                    spcLogObj.Name = spc.Name;
                    spcLogObj.Status = spc.Status;
                    spcLogObj.Controltype = spc.Controltype;
                    spcLogObj.SpecificationCriteria = spc;
                    spcLogObj.Criteriaoptions = optionString;
                    spclog.Add(spcLogObj);
                }

                specificationTemplate.Rank = _specificationTemplateDao.GetMaximumRank(specificationTemplate) + 1;
                specificationTemplate.Status = SpecificationTemplate.EntityStatus.Active;

                using (trans = Session.BeginTransaction())
                {
                    if (specificationTemplate.Id < 1)
                    {
                        _specificationTemplateDao.Save(specificationTemplate);
                        foreach (var splogo in spclog)
                        {
                            //Session.Save(splogo);
                            _specificationCriteriaLogDao.SaveOrUpdate(splogo);
                        }
                    }
                    else if (specificationTemplate.Id > 0)
                    {
                        var specificationTemplateDb = _specificationTemplateDao.LoadById(specificationTemplate.Id);
                        specificationTemplateDb.Name = specificationTemplate.Name;
                        var criteriaList = specificationTemplate.SpecificationCriteriaList;
                        var criteriaListDb = specificationTemplateDb.SpecificationCriteriaList;
                        var criteriaListId = criteriaList.Select(x => x.Id).ToList();
                        var criteriaListIDb = criteriaListDb.Select(x => x.Id).ToList();
                        var commonCriteriaIdList = criteriaListId.Intersect(criteriaListIDb).ToList();
                        var newCriteriaList = criteriaList.Where(x => x.Id == 0).ToList();
                        var deletableCriteriaIdList = criteriaListIDb.Except(criteriaListId).ToList();
                        foreach (var criteriaId in commonCriteriaIdList)
                        {
                            var cdb = criteriaListDb.SingleOrDefault(x => x.Id == criteriaId);
                            var c = criteriaList.SingleOrDefault(x => x.Id == criteriaId);
                            cdb.Name = c.Name;
                            if (c.Controltype == FieldType.Text && cdb.Controltype == FieldType.Dropdown)
                            {
                                cdb.Controltype = FieldType.Text;
                                cdb.SpecificationCriteriaOptions.Clear();
                            }
                            else if (c.Controltype == FieldType.Dropdown)
                            {
                                if (cdb.Controltype == FieldType.Dropdown)
                                {
                                    var optionList = c.SpecificationCriteriaOptions;
                                    var optionListDb = cdb.SpecificationCriteriaOptions;
                                    var optionIdList = optionList.Select(x => x.Id).ToList();
                                    var optionIdListDb = optionListDb.Select(x => x.Id).ToList();
                                    var commonOptionIdList = optionIdList.Intersect(optionIdListDb).ToList();
                                    var newOptionList = optionList.Where(x => x.Id == 0).ToList();
                                    var deletableOptionIdList = optionIdListDb.Except(optionIdList).ToList();
                                    foreach (var optionId in commonOptionIdList)
                                    {
                                        var option = optionList.SingleOrDefault(x => x.Id == optionId);
                                        var optionDb = optionListDb.SingleOrDefault(x => x.Id == optionId);
                                        optionDb.Name = option.Name;
                                    }
                                    foreach (var option in newOptionList)
                                    {
                                        optionListDb.Add(option);
                                    }
                                    foreach (var optionId in deletableOptionIdList)
                                    {
                                        var option = optionListDb.SingleOrDefault(x => x.Id == optionId);
                                        optionListDb.Remove(option);
                                    }

                                }
                                else if (cdb.Controltype == FieldType.Text)
                                {
                                    cdb.Controltype = FieldType.Dropdown;
                                    foreach (var option in c.SpecificationCriteriaOptions)
                                    {
                                        cdb.SpecificationCriteriaOptions.Add(option);
                                    }
                                }

                            }
                        }
                        foreach (var criteria in newCriteriaList)
                        {
                            specificationTemplateDb.SpecificationCriteriaList.Add(criteria);
                        }
                        foreach (var criteriaId in deletableCriteriaIdList)
                        {
                            var id = criteriaId;
                            var c = specificationTemplateDb.SpecificationCriteriaList.SingleOrDefault(x => x.Id == id);
                            specificationTemplateDb.SpecificationCriteriaList.Remove(c);
                        }
                        _specificationTemplateDao.Update(specificationTemplateDb);
                    }
                    trans.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (InvalidDataException ex)
            {
                _logger.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }
        
        public bool Delete(long id)
        {
            ITransaction transaction = null;
            try
            {
                var tempSpecificationTemplateObj = _specificationTemplateDao.LoadById(id);
                CheckBeforeDelete(tempSpecificationTemplateObj);
                using (transaction = Session.BeginTransaction())
                {
                    tempSpecificationTemplateObj.Status = SpecificationTemplate.EntityStatus.Delete;
                    _specificationTemplateDao.Update(tempSpecificationTemplateObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Single Instances Loading Function

        public SpecificationTemplate GetSpecificationTemplate(long id)
        {
            try
            {
                var specificationTemplate = _specificationTemplateDao.LoadById(id);
                return specificationTemplate;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<SpecificationTemplateDto> LoadSpecificationTemplateList(int start, int length,
            string searchString = "")
        {
            try
            {
                var specificationTemplateList = _specificationTemplateDao.LoadSpecificationTemplateList(start,
                    length, searchString);
                return specificationTemplateList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public IList<SpecificationTemplate> LoadISpecificationTemplateList(int? status = null)
        {
            try
            {
                return _specificationTemplateDao.LoadAll(status).OrderBy(x => x.Rank).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int SpecificationTemplateRowCount(string searchString)
        {
            try
            {
                var rowCount = _specificationTemplateDao.SpecificationTemplateRowCount(searchString);
                return rowCount;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function
        private void CheckSpecificationTemplateDuplication(SpecificationTemplate specificationTemplate)
        {
            bool isDuplicateSpecificationTemplate =
                _specificationTemplateDao.IsDuplicateSpecificationTemplate(specificationTemplate.Name.Trim(),
                    specificationTemplate.Id);

            if (isDuplicateSpecificationTemplate)
            {
                throw new DuplicateEntryException("Duplicate Specification Template found");
            }
        }

        private static void SpecificationCriteriaOptionValidation(SpecificationCriteriaOption spco)
        {
            var spcoValidationResult =
                ValidationHelper
                    .ValidateEntity
                    <SpecificationCriteriaOption,
                        SpecificationCriteriaOption.SpecificationCriteriaOptionMetaData>(spco);
            if (spcoValidationResult.HasError)
            {
                string errorMessage = "";
                spcoValidationResult.Errors.ForEach(
                    r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private static void CheckCriteriaOptionDuplication(SpecificationCriteria spc)
        {
            var hasDupesOptions =
                spc.SpecificationCriteriaOptions.GroupBy(x => new { x.Name }).Any(x => x.Skip(1).Any());
            if (hasDupesOptions)
            {
                throw new DuplicateEntryException("Duplication Criteria option found for " + spc.Name);
            }
        }

        private static void SpecificationCriteriaValidation(SpecificationCriteria spc)
        {
            var spcValidationResult =
                ValidationHelper
                    .ValidateEntity
                    <SpecificationCriteria, SpecificationCriteria.SpecificationCriteriaMetaData>(spc);
            if (spcValidationResult.HasError)
            {
                string errorMessage = "";
                spcValidationResult.Errors.ForEach(
                    r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private static void CheckCriteriaDuplication(SpecificationTemplate specificationTemplate)
        {
            var hasDupes =
                specificationTemplate.SpecificationCriteriaList.GroupBy(x => new { x.Name })
                    .Any(x => x.Skip(1).Any());
            if (hasDupes)
            {
                throw new DuplicateEntryException("Duplication Criteria found for " + specificationTemplate.Name);
            }
        }

        private static void SpecificationTemplateValidation(SpecificationTemplate specificationTemplate)
        {
            var validationResult =
                ValidationHelper
                    .ValidateEntity<SpecificationTemplate, SpecificationTemplate.SpecificationTemplateMetaData>(
                        specificationTemplate);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(
                    r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        private void CheckBeforeDelete(SpecificationTemplate specificationTemplateObj)
        {
            try
            {
                var items = specificationTemplateObj.Items;
                if (items.Count > 0)
                {
                    throw new DependencyException("One/more items are specified.");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        
        #endregion
    }

}

