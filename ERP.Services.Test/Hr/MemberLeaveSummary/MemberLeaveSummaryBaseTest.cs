﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.MemberLeaveSummary
{
    public class MemberLeaveSummaryBaseTest:TestBase,IDisposable
    {
        internal readonly IMembersLeaveSummaryService _membersLeaveSummaryService;
        internal readonly ILeaveService _leaveService ;
        internal readonly ILeaveApplicationService _leaveApplicationService;
        internal readonly IHolidaySettingService _holidaySettingService;
        internal readonly IOrganizationService _organizationService;
        internal readonly MemberLeaveSummaryFactory _memberLeaveSummaryFactory;
        public MemberLeaveSummaryBaseTest()
        {
            _leaveService = new LeaveService(Session);
            _membersLeaveSummaryService = new MembersLeaveSummaryService(Session);
            _memberLeaveSummaryFactory = new MemberLeaveSummaryFactory(null,Session);
            _holidaySettingService = new HolidaySettingService(Session);
            _organizationService = new OrganizationService(Session);
        }

        #region disposal

        public void Dispose()
        {
            //_leaveApplicationFactory.LogCleanUp(new List<LeaveApplication>() { _leaveApplicationFactory .Object});
            //_leaveApplicationFactory.CleanUp();
            base.Dispose();
        }

        #endregion
    }
}
