﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessRules;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class MaterialTypeController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Propertise/Services & Initialization
        private MaterialType _materialTypeObj;
        private readonly IMaterialTypeService _materialTypeService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private List<UserMenu> _userMenu; 
        public MaterialTypeController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _materialTypeObj = new MaterialType();
            _userService = new UserService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _materialTypeService = new MaterialTypeService(session);
            
        }
        #endregion

        #region Index/Manage Session
        public ActionResult Index()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                //ViewBag.Organization = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
                GetStatusText();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                GetStatusText();
            }
            return View();
        }
        #endregion

        #region Render Data Table
        public JsonResult MaterialList(int draw, int start, int length, string organization, string name, string shortName, string rank, string status)
        {
            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "organization.Name";
                            break;
                        case "1":
                            orderBy = "Name";
                            break;
                        case "2":
                            orderBy = "ShortName";
                            break;
                        case "3":
                            orderBy = "Rank";
                            break;
                        case "4":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                #endregion
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                int recordsTotal = _materialTypeService.GetMaterialTypeCount(_userMenu,orderBy, orderDir.ToUpper(), organization, name, shortName, rank, status);
                long recordsFiltered = recordsTotal;
                var materialTypesList = _materialTypeService.LoadMaterialType(_userMenu,start, length, orderBy, orderDir.ToUpper(), organization, name, shortName, rank, status).ToList();
                
                MaterialType obj = new MaterialType();
                int[] maxMinRank = new int[2];
                maxMinRank[0] = _materialTypeService.GetMinimumRank(obj);
                maxMinRank[1] = _materialTypeService.GetMaximumRank(obj);
                foreach (var materialType in materialTypesList)
                {
                    var str = new List<string>();
                    str.Add(materialType.Organization != null ? materialType.Organization.ShortName : "NULL");
                    str.Add(materialType.Name);
                    str.Add(materialType.ShortName);
                    string rankbtn = materialType.Rank.ToString();
                    if (materialType.Rank > maxMinRank[0])
                    {
                        rankbtn += "<a href='#'  id='" + materialType.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                    }
                    if (materialType.Rank < maxMinRank[1])
                    {
                        rankbtn += "<a href='#'  id='" + materialType.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                    }
                    str.Add(rankbtn);
                    str.Add(StatusTypeText.GetStatusText(materialType.Status));
                    str.Add(
                            "<a href='" + Url.Action("Details", "MaterialType") + "?Id=" + materialType.Id + "' data-id='" + materialType.Id + "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;&nbsp"
                            +
                            "<a href='" + Url.Action("Edit", "MaterialType") + "?Id=" + materialType.Id + "' data-id='" + materialType.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; "
                            +
                            "<a  id='" + materialType.Id.ToString() + "'    href='#' data-name='" + materialType.Name.ToString() + "' class='glyphicon glyphicon-trash'></a> "
                        );
                    getData.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                _logger.Error(ex.Message);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }
        #endregion

        #region Save Operation
        // GET: Administration/MaterialType/Create
        public ActionResult Create(string message)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //ViewBag.organizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.SuccessMessage = message;
            return View();
        }
        // POST: Administration/MaterialType/Create
        [HttpPost]
        public ActionResult Create(MaterialType materialTypeObj)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            //ViewBag.organizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            try
            {
                var organization = _organizationService.LoadById(materialTypeObj.OrganizationId);
                materialTypeObj.Organization = organization;
               _materialTypeService.Save(materialTypeObj);
               return RedirectToAction("Create", new { message = "Material type successfully saved." });
                
            }
            catch (NullObjectException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
            }
            
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
            }
            return View();
        }
        #endregion

        #region Update Operation
        // GET: Administration/MaterialType/Edit/5
        public ActionResult Edit(long id)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                _materialTypeObj = _materialTypeService.GetMaterialType(id);
                _materialTypeObj.OrganizationId = _materialTypeObj.Organization.Id;
                GetStatusText();
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", _materialTypeObj.Organization.Id);
                return View(_materialTypeObj);
                
            }
            catch (Exception ex)
            {
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return View();
            }
        }

        // POST: Administration/MaterialType/Edit/5
        [HttpPost]
        public ActionResult Edit(long id, MaterialType materialTypeObj)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            try
            {
                var organization = _organizationService.LoadById(materialTypeObj.OrganizationId);
                materialTypeObj.Organization = organization;

                bool isSuccess = _materialTypeService.Update(id, materialTypeObj);
                if (isSuccess)
                    ViewBag.SuccessMessage = "Material type successfully updated.";
                else
                    ViewBag.ErrorMessage = "Problem Occurred, during material type update.";
                InitializeEditView(id);
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
                InitializeEditView(id);
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                _logger.Error(ex);
                InitializeEditView(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                InitializeEditView(id);
            }
            return View(_materialTypeObj);
        }

        #region Initialize Edit View
        private void InitializeEditView(long id)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            GetStatusText();
            _materialTypeObj = _materialTypeService.GetMaterialType(id);
            ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", _materialTypeObj.Organization.Id);
        }

        #endregion

        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isSuccess = _materialTypeService.Delete(id);
                if (isSuccess)
                {
                    return Json(new Response(true, "Material type sucessfully deleted."));
                }
                return Json(new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "You can't delete this material type, material is already assigned."));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Material type Delete Fail !"));
            }
        }
        #endregion

        #region Details Operation
        // GET: Administration/MaterialType/Details/5
        public ActionResult Details(long id)
        {
            try
            {
                _materialTypeObj = _materialTypeService.GetMaterialType(id);
                if (_materialTypeObj != null)
                {
                    _materialTypeObj.CreateByText = _userService.GetUserNameByAspNetUserId(_materialTypeObj.CreateBy);
                    _materialTypeObj.ModifyByText = _userService.GetUserNameByAspNetUserId(_materialTypeObj.ModifyBy);
                }
                return View(_materialTypeObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
        }
        #endregion

        #region Rank Operation
        public JsonResult RankChange(long id, int current, string action)
        {
            try
            {
                MaterialType materialtypeUpdateObj = _materialTypeService.GetMaterialType(Convert.ToInt64(id));
                if (materialtypeUpdateObj == null)
                {
                    return null;
                }
                int newRank;
                if (action == "up")
                    newRank = materialtypeUpdateObj.Rank - 1;
                else
                    newRank = materialtypeUpdateObj.Rank + 1;
                var materialOldObj = _materialTypeService.GetMaterialTypeByRankNextOrPrevious(newRank, action);
                //LoadByRankMaterialType(newRank);
                newRank = materialOldObj.Rank;
                materialOldObj.Rank = materialtypeUpdateObj.Rank;
                materialtypeUpdateObj.Rank = newRank;
                bool isSuccess = _materialTypeService.UpdateRank(materialOldObj, materialtypeUpdateObj);
                if (isSuccess)
                {
                    return Json("Rank Changes");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json("Something Wrong Occured!!");
        }
        #endregion

        #region Helper
        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
        }
        #endregion
    }
}
