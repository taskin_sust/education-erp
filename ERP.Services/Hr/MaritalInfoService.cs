﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{

    public interface IMaritalInfoService : IBaseService
    {
        #region Operational Function

        bool UpdateMaritalInformation(TeamMember member, int maritalStatus, string marriageDate, string spouseName, string spouseContactNumber);
        bool Save(MaritalInfo maritalInfo);

        #endregion

        #region Single Instances Loading Function

        MaritalInfo GetTeamMemberMaritalInfo(long teamMemberId);

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        int GetCurrentMaritalStatus(long memeId);

        #endregion

        #region Helper Function

        #endregion
        
    }
    public class MaritalInfoService : BaseService, IMaritalInfoService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IMaritalInfoDao _hrMaritalInfoDao;
        
        public MaritalInfoService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrMaritalInfoDao = new MaritalInfoDao(){Session = session};
        }

        #endregion

        #region Operational Functions

        public bool UpdateMaritalInformation(TeamMember member, int maritalStatus, string marriageDate, string spouseName, string spouseContactNumber)
        {
            ITransaction trans = null;
            try
            {
                using (trans = Session.BeginTransaction())
                {

                    CheckBeforeSavingMaritalInformation(maritalStatus, spouseContactNumber);
                    MaritalInfo maritalInfo = _hrMaritalInfoDao.GetTeamMemberMaritalInfo(member.Id);
                    if (maritalInfo != null)
                    {
                        maritalInfo.MaritalStatus = maritalStatus;
                        if (!String.IsNullOrEmpty(marriageDate) && !String.IsNullOrWhiteSpace(marriageDate))
                        {
                            maritalInfo.MarriageDate = Convert.ToDateTime(marriageDate);
                        }
                        if (!String.IsNullOrEmpty(spouseName) && !String.IsNullOrWhiteSpace(spouseName))
                        {
                            maritalInfo.SpouseName = spouseName;
                        }
                        if (!String.IsNullOrEmpty(spouseContactNumber) &&
                            !String.IsNullOrWhiteSpace(spouseContactNumber))
                        {
                            maritalInfo.SpouseContatctNo = spouseContactNumber;
                        }
                        _hrMaritalInfoDao.Update(maritalInfo);
                    }
                    else
                    {
                        maritalInfo = new MaritalInfo();
                        maritalInfo.Status = MaritalInfo.EntityStatus.Active;
                        maritalInfo.MaritalStatus = maritalStatus;
                        if (!String.IsNullOrEmpty(marriageDate) && !String.IsNullOrWhiteSpace(marriageDate))
                        {
                            maritalInfo.MarriageDate = Convert.ToDateTime(marriageDate);
                        }
                        if (!String.IsNullOrEmpty(spouseName) && !String.IsNullOrWhiteSpace(spouseName))
                        {
                            maritalInfo.SpouseName = spouseName;
                        }
                        if (!String.IsNullOrEmpty(spouseContactNumber) &&
                            !String.IsNullOrWhiteSpace(spouseContactNumber))
                        {
                            maritalInfo.SpouseContatctNo = spouseContactNumber;
                        }
                        _hrMaritalInfoDao.Save(maritalInfo);
                    }
                    trans.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }

        private void CheckBeforeSavingMaritalInformation(int maritalStatus, string spouseContactNumber)
        {
            var reg = new Regex(@"^(?:\+?88)?0?1[15-9]\d{8}$");
            if (!String.IsNullOrEmpty(spouseContactNumber) && !String.IsNullOrWhiteSpace(spouseContactNumber))
            {
                if (!reg.IsMatch(spouseContactNumber))
                {
                    throw new InvalidDataException("Invalid Mother Mobile Nulber!");
                }
            }
            List<int> maritalValue = new List<int>(Enum.GetValues(typeof(MaritalType)).Cast<int>());
            if (!maritalValue.Contains(Convert.ToInt32(maritalStatus)))
            {
                throw new InvalidDataException("Invalid Marital Status");
            }
        }

        public bool Save(MaritalInfo maritalInfo)
        {
            _hrMaritalInfoDao.Save(maritalInfo);
            return true;
        }

        #endregion

        #region Single Instances Loading Function
        
        public MaritalInfo GetTeamMemberMaritalInfo(long teamMemberId)
        {
            try
            {
                return _hrMaritalInfoDao.GetTeamMemberMaritalInfo(teamMemberId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        public int GetCurrentMaritalStatus(long memeId)
        {
            try
            {
                IList<MaritalInfo> hrMaritalInfos = _hrMaritalInfoDao.LoadByMemberId(memeId);
                var singleOrDefault = hrMaritalInfos.OrderByDescending(x => x.Id).Take(1).SingleOrDefault();
                if (singleOrDefault != null)
                    if (singleOrDefault.MaritalStatus != null) return (int)singleOrDefault.MaritalStatus;
                return -404;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        #endregion

        #region Helper function
        
        #endregion
        
    }
}
