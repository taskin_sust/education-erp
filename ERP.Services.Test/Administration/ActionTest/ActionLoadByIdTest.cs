﻿using UdvashERP.Services.Test.AdminisTration.ProgramTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ActionTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Action")]
    public class ActionLoadByIdTest : ActionBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _actionsService.LoadById(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {

            var actionFactory =
                _actionsFactory.Create().WithController().WithNameAndRank(_name, 1)
                    .Persist();
            var obj = _actionsService.LoadById(actionFactory.Object.Id);
            Assert.NotNull(obj);
        }

    }
}
