﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.Administration.MaterialTypeTest
{
    public class MaterialTypeBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        private ISession _session;
        internal readonly IMaterialTypeService _materialTypeService;
        internal readonly MaterialTypeFactory _materialTypeFactory;

        internal readonly string _name = "Material_Type_" + Guid.NewGuid().ToString().Substring(0, 6);
        internal readonly string _short_name = "MT_"+Guid.NewGuid().ToString().Substring(0, 6);  
       
        public MaterialTypeBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _materialTypeFactory = new MaterialTypeFactory(null, _session);
            _materialTypeService = new MaterialTypeService(_session);
        }
        #endregion
        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        #region CustomText

        public const string NameNull = "Material type name is empty!";
        public const string ShortNameNull = "Material type short name is empty!";
        public const string OrganizationNull = "Organization is empty!";

        #endregion

        public void Dispose()
        {
            _materialTypeFactory.Cleanup();
            base.Dispose();
        }
    }
}
