﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public partial class ProgramSessionSubjectMap: BaseClassMap<ProgramSessionSubject,long>
    {
        public ProgramSessionSubjectMap()
        {
            Table("ProgramSessionSubject");
            LazyLoad();
            //Id(x => x.Id).GeneratedBy.Identity().Column("Id");

            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.Subject).Column("SubjectId");
        }
    }
}
