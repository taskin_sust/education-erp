﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Web.Helpers;
using System.Web.Mvc;
using log4net;
using NHibernate;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.App_code;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class OrganizationController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly IOrganizationService _organizationService;
        private readonly ICommonHelper _commonHelper;
        private readonly IUserService _userService;

        public OrganizationController()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            _organizationService = new OrganizationService(nHsession);
            _userService = new UserService(nHsession);
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Index/Manage Page

        public ActionResult Index()
        {
            try
            {
                ViewBag.StatusText = _commonHelper.GetStatus();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return View(new List<Organization>());
            }
        }

        #region Render Data Table
        [HttpPost]
        public ActionResult OrganizationList(int draw, int start, int length,string name, string shortName, string website, string status)
        {
            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "ShortName";
                            break;
                        case "2":
                            orderBy = "Website";
                            break;
                        case "3":
                            orderBy = "Email";
                            break;
                        case "4":
                            orderBy = "ContactNo";
                            break;
                        case "5":
                            orderBy = "Rank";
                            break;
                        case "6":
                            orderBy = "Status";
                            break;

                        default:
                            orderBy = "";
                            break;
                    }
                }
                #endregion
                var organizationList = _organizationService.LoadOrganization(start, length, orderBy, orderDir.ToUpper(), name, shortName, website, status);
                int recordsTotal = _organizationService.GetOrganizationCount(name, shortName, website, status);
                long recordsFiltered = recordsTotal;
                var org = new Organization();
                var maxMinRank = new int[2];
                maxMinRank[0] = _organizationService.GetMinRank(org);
                maxMinRank[1] = _organizationService.GetMaxRank(org);
                foreach (var organization in organizationList)
                {
                    var str = new List<string>
                    {
                        organization.Name,
                        organization.ShortName,
                        organization.Website,
                        organization.Email,
                        organization.ContactNo
                    };
                    string rankbtn = organization.Rank.ToString();
                    if (organization.Rank > maxMinRank[0])
                    {
                        rankbtn += "<a href='#'  id='" + organization.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                    }
                    if (organization.Rank < maxMinRank[1])
                    {
                        rankbtn += "<a href='#'  id='" + organization.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                    }
                    str.Add(rankbtn);
                    str.Add(StatusTypeText.GetStatusText(organization.Status));
                    str.Add(LinkGenerator.GetGeneratedLink("Details", "Edit", "Delete", "Organization", organization.Id, organization.Name));
                    getData.Add(str);
                }
                return Json(new
                {
                    draw, recordsTotal, recordsFiltered, start, length,
                    data = getData,
                    isSuccess = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw,
                recordsTotal = 0,
                recordsFiltered = 0, start, length,
                data = getData,
                isSuccess = false
            });
        }
        #endregion

        #endregion

        #region Operational Function

        #region Save Operation
        public ActionResult Create()
        {
            if (TempData["organizationCreate"] != null)
            {
                ViewBag.SuccessMessage = TempData["organizationCreate"].ToString();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Organization organizationObj)
        {
            //try
            //{
            try
            {
                //set AttendanceStartTime manually
                organizationObj.AttendanceStartTime = Convert.ToDateTime("2000-01-01 06:00:00.000");
                bool isSuccess = _organizationService.SaveOrUpdate(organizationObj);
                if (isSuccess)
                {
                    TempData["organizationCreate"] = "Organization successfully saved.";
                    return RedirectToAction("Create");
                }
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return View();
            }
            return View();

            //}
            //catch(Exception ex)
            //{
            //    _logger.Error(ex);
            //    return View();
            //}
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(long id)
        {
            try
            {
                var organizationObj = _organizationService.LoadById(id);
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                return View(organizationObj);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return View();
            }
        }

        [HttpPost]
        public ActionResult Edit(Organization organization)
        {
            try
            {
                
                bool success = _organizationService.SaveOrUpdate(organization);
                if (success)
                {
                    ViewBag.SuccessMessage = "Organization Successfully Updated.";
                }
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error("Edit", ex);
            }
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
            return View(organization);
        }
        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isSuccess = _organizationService.Delete(id);
                if (isSuccess)
                {
                    return Json(new { isSuccess = true, message = "Organization deleted successfully." });
                }
                return Json(new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                return Json(new {isSuccess = false, message = ex.Message});
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Organization Delete Fail !"));
            }
        }
        #endregion

        #region Details Operation
        public ActionResult Details(long id)
        {
            var organization = new Organization();
            try
            {
                organization = _organizationService.LoadById(id);
                if (organization != null)
                {
                    organization.CreateByText = _userService.GetUserNameByAspNetUserId(organization.CreateBy);
                    organization.ModifyByText = _userService.GetUserNameByAspNetUserId(organization.ModifyBy);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            if (organization == null)
            {
                return HttpNotFound();
            }
            return View(organization);
        }
        #endregion

        #region Rank Operation
        public JsonResult RankChange(long id, int current, string action)
        {
            bool isSuccess = _organizationService.UpdateRank(id, current, action);
            if (isSuccess)
            {
                return Json("Rank Changes");
            }
            return Json("Rank Changes, Something Wrong Occured!!");
        }
        #endregion

        #endregion
    }
}
