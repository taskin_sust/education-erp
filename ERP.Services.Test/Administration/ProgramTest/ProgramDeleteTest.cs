﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ProgramTest
{
    interface IProgramDeleteTest
    {
        #region Basic Test
        void Delete_Program_Should_Be_Fail_For_Passing_Null_Data();
        void Delete_Program_Should_Be_Success();
        #endregion

        #region Business Logic Test
        void Delete_Program_Should_Be_Fail_For_ProgramBranchSession();
        #endregion

    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Program")]
    public class ProgramDeleteTest : ProgramBaseTest, IProgramDeleteTest
    {
        #region Basic Test
        [Fact]
        public void Delete_Program_Should_Be_Fail_For_Passing_Null_Data()
        {
            Assert.Throws<NullObjectException>(() => _programService.Delete(null));
        }

        [Fact]
        public void Delete_Program_Should_Be_Success()
        {
            var programFactory = _programFactory.Create().WithOrganization().WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            _programService.Delete(programFactory.Object);
            Program program = _programService.GetProgram(programFactory.Object.Id);
            Assert.Null(program);
        }

        #endregion

        #region Business Logic Test
        [Fact]
        public void Delete_Program_Should_Be_Fail_For_ProgramBranchSession()
        {
            var organization = organizationFactory.Create().Persist();
            var sessionObj = _sessionFactory.Create().Persist();
            var programFactory = _programFactory.Create().WithOrganization(organization.Object).WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            int?[] admissiontype = { 1 };

            List<long> organizationIds = new List<long> { organization.Object.Id };
            _branchFactory.Create().WithOrganization(organization.Object).Persist();

            var existingBranch = _branchService.LoadBranch(organizationIds);
            List<long> branchList = existingBranch.Select(c => c.Id).Distinct().ToList();
            long[] branchIds = branchList.ToArray();
            var studentExams = _studentExamService.LoadBoardExam(true);
            int?[] examIds = studentExams.Select(x => (int?)Convert.ToInt32(x.Id)).ToArray();
            List<long> nonDeletableBranchIds;
            _programBranchSessionService.AssignProgram(organization.Object.Id, programFactory.Object.Id, sessionObj.Object.Id, branchIds, admissiontype, examIds, out nonDeletableBranchIds);
            _session.Evict(_programFactory.Object);
            Program program = _programService.GetProgram(_programFactory.Object.Id);

            Assert.Throws<DependencyException>(() => _programService.Delete(program));
        }
        #endregion

    }
}
