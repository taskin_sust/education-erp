using log4net;
using NHibernate;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.UInventory
{
public interface ISpecificationCriteriaOptionService : IBaseService
{
    #region Operational Function

    bool SaveOrUpdate(SpecificationCriteriaOption specificationCriteriaOption);

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function

    #endregion

    #region Helper Function

    #endregion
}
public class SpecificationCriteriaOptionService : BaseService, ISpecificationCriteriaOptionService {
    #region Logger
    private readonly ILog _logger = LogManager.GetLogger("InventoryService");
    #endregion

    #region Propertise & Object Initialization
    private readonly CommonHelper _commonHelper;
    private readonly ISpecificationCriteriaOptionDao _specificationCriteriaOptionDao;
    public SpecificationCriteriaOptionService(ISession session)
    {
        Session = session;
        _commonHelper = new CommonHelper();
        _specificationCriteriaOptionDao=new SpecificationCriteriaOptionDao{Session = session};}
    #endregion

    #region Operational Functions

    public bool SaveOrUpdate(SpecificationCriteriaOption specificationCriteriaOption)
    {

        return true;
    }

    #endregion

    #region Single Instances Loading Function

    #endregion

    #region List Loading Function

    #endregion

    #region Others Function


    #endregion

    #region Helper function



    #endregion
}
}
                                    
