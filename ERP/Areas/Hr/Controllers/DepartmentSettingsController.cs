﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using log4net;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class DepartmentSettingsController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly IOrganizationService _organizationService;
        private readonly ICommonHelper _commonHelper;
        private readonly IDepartmentService _hrDepartmentService;
        private List<UserMenu> _userMenu; 
        public DepartmentSettingsController()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            _organizationService = new OrganizationService(nHsession);
            _hrDepartmentService = new DepartmentService(nHsession);
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Index/Manage
        public ActionResult Index()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }
        public ActionResult DepartmentSettingsList(int draw, int start, int length, string organizationId, string status)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            NameValueCollection nvc = Request.Form;

            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            try
            {
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "Rank";
                            break;
                        case "2":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                int recordsTotal = _hrDepartmentService.TotalRowCountOfDepartmentSettings(_userMenu, organizationId,
                    status);
                long recordsFiltered = recordsTotal;

                var rowLists =
                    _hrDepartmentService.LoadActive(_userMenu, start, length, orderBy, orderDir.ToUpper(),
                        organizationId, status).ToList();

                foreach (Department row in rowLists)
                {
                    var str = new List<string>();
                    str.Add(row.Organization.ShortName);
                    str.Add(row.Name);
                    str.Add(StatusTypeText.GetStatusText(row.Status));

                    str.Add("<a href='" + Url.Action("Edit", "DepartmentSettings") + "?Id=" + row.Id + "' data-id='" +
                            row.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; " +
                            "<a href='#' data-id='" + row.Id +
                            "' class='glyphicon glyphicon-trash deleteDepartment' name='" + row.Name + "'></a>"
                        );
                    getData.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData

                });
            }
            catch (InvalidDataException ex)
            {
                ViewBag.Errormessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }
        #endregion
        
        #region Operational Function
        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                if (TempData["SuccessMessage"] != null)
                {
                    ViewBag.SuccessMessage = TempData["SuccessMessage"];
                    TempData["SuccessMessage"] = null;
                }
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }
        [HttpPost]
        public ActionResult Create(string submit, DepartmentSettingsViewModel dSViewModel)
        {

            //collection["submit"]
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            try
            {
                if (ModelState.IsValid)
                {

                    var hrDepartment = new Department
                    {
                        Organization = _organizationService.LoadById(dSViewModel.OrganizationId),
                        Name = dSViewModel.Name
                    };
                    bool isSuccess = _hrDepartmentService.Save(hrDepartment);
                    //bool isSuccess  = true;
                    if (isSuccess)
                    {
                        //ViewBag.SuccessMessage = "Successfully Create";
                        ModelState.Clear();
                        if (submit == "Save & New")
                        {
                            TempData["SuccessMessage"] = "Successfully Create";
                            return RedirectToAction("Create");
                        }
                        else if (submit == "Save & Exit")
                        {
                            TempData["SuccessMessage"] = null;
                            return RedirectToAction("Index");
                        }
                        //switch (dSViewModel.ActionType)
                        //{
                        //    case "Save & New":
                        //        return RedirectToAction("Create");
                        //    case "Save & Exit":
                        //        return RedirectToAction("Index");
                        //}
                    }
                    else
                    {
                        ViewBag.ErrrorMessage = "Problem occured";
                    }
                }
            }
            catch (NullObjectException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return View();
            }
            return View();
        }
        [HttpGet]
        public ActionResult Edit(long id)
        {
            var dSViewModel = new DepartmentSettingsViewModel();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                Department obj = _hrDepartmentService.LoadById(id);
                if (obj != null)
                {
                    dSViewModel.Id = obj.Id;
                    dSViewModel.OrganizationId = obj.Organization.Id;
                    dSViewModel.Name = obj.Name;
                    dSViewModel.Status = Convert.ToString(obj.Status);
                    GetStatusText();
                    ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", obj.Organization.Id);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View(dSViewModel);
        }
        [HttpPost]
        public ActionResult Edit(long id,DepartmentSettingsViewModel dsViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                    GetStatusText();
                    ViewBag.OrganizationList = new SelectList(
                        _organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName",
                        dsViewModel.OrganizationId);
                    var hrDepartment = new Department();
                    hrDepartment.Id = id;
                    hrDepartment.Organization = _organizationService.LoadById(dsViewModel.OrganizationId);
                    hrDepartment.Name = dsViewModel.Name;
                    hrDepartment.Status = Convert.ToInt32(dsViewModel.Status);
                    bool isUpdated = _hrDepartmentService.Update(hrDepartment);
                    if (isUpdated)
                    {
                        ViewBag.SuccessMessage = "Department settings successfully updated.";
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "Problem Occurred, during department settings update.";
                    }
                }
            }
            catch (NullObjectException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred, during department settings update.";
            }
            return View(dsViewModel);
        }
        [HttpPost]
        public JsonResult Delete(long id)
        {
            try
            {
                Department hrDepartment = _hrDepartmentService.LoadById(id);
                if (hrDepartment != null)
                {
                    var isSuccess = _hrDepartmentService.Delete(hrDepartment);
                    return Json(isSuccess ? new Response(true, "Successfully deleted") : new Response(false, "Department Delete fail !"));
                }
                else
                {
                    return Json(new Response(false, "Department Delete fail !"));
                }
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Department Delete fail !"));
            }
        }

        #endregion

        #region Helper function
        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
        }



        #endregion

        #region others function

        [HttpPost]
        public JsonResult DoesDepartmentNameExist(string name, long organizationId, long? id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (id != null)
                    {
                        bool checkRank = _hrDepartmentService.HasDuplicateByDepartmentName(name, organizationId, id);
                        return Json(checkRank);
                    }
                    else
                    {
                        bool checkRank = _hrDepartmentService.HasDuplicateByDepartmentName(name, organizationId, null);
                        return Json(checkRank);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            else { return Json(false); }
        }

        #endregion        

        #region Export
        public ActionResult ExportDepartmentList(string organizationId, string status)
        {
            try
            {
                _userMenu = (List<UserMenu>) ViewBag.UserMenu;
                int recordsTotal = _hrDepartmentService.TotalRowCountOfDepartmentSettings(_userMenu, organizationId,
                    status);
                var length = recordsTotal;
                const int start = 0;
                string orderDir = "ASC";
                string orderBy = "Name";
                var rowLists =
                    _hrDepartmentService.LoadActive(_userMenu, start, length, orderBy, orderDir, organizationId, status)
                        .ToList();

                var headerList = new List<string> {ErpInfo.OrganizationNameFull};

                var footerList = new List<string> {""};

                var columnList = new List<string>();
                columnList.Add("Organization");
                columnList.Add("Name");
                columnList.Add("Status");

                var userExcelList = new List<List<object>>();

                foreach (var row in rowLists)
                {
                    var xlsRow = new List<object>();

                    xlsRow.Add(row.Organization.ShortName);
                    xlsRow.Add(row.Name);
                    xlsRow.Add(StatusTypeText.GetStatusText(row.Status));
                    userExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, userExcelList, footerList,
                    "department-list-report__"
                    + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }

            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View("AutoClose");
        }
        #endregion
        
    }
}