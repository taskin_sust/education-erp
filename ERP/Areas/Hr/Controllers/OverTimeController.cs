﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class OverTimeController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IOvertimeService _overtimeService;
        private readonly IOrganizationService _organizationService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IMentorHistoryService _mentorHistoryService;
        private readonly ICommonHelper _commonHelper;
        private readonly IUserService _userService;
        public OverTimeController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _overtimeService = new OvertimeService(session);
            _organizationService = new OrganizationService(session);
            _teamMemberService = new TeamMemberService(session);
            _mentorHistoryService = new MentorHistoryService(session);
            _commonHelper = new CommonHelper();
            _userService = new UserService(session);

        }
        #endregion

        #region Over Time Approval By Mentor
        public ActionResult OverTime()
        {
            return View();
        }

        public ActionResult OverTimeApproval()
        {
            ViewBag.ShowNoData = false;
            return View();
        }

        [HttpPost]
        public ActionResult OverTime(OverTimeFormViewModel data)
        {
            ViewBag.ShowNoData = true;
            try
            {
                if (ModelState.IsValid)
                {

                    var pinList = (!String.IsNullOrEmpty(data.PinList)) ? data.PinList.Trim() : "";
                    var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    pinList = regex.Replace(pinList, @",");
                    var mentor = _teamMemberService.GetCurrentMember();
                    var organizationIdList = new List<long>();
                    var memberPinList = (!String.IsNullOrEmpty(pinList)) ? pinList.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList() : null;
                    var mentorTeamMemberIdList = _mentorHistoryService.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now, null, memberPinList);
                    if (!mentorTeamMemberIdList.Any())
                    {
                        mentorTeamMemberIdList.Add(0);
                    }
                    IList<OverTimeDto> overTimeList = new List<OverTimeDto>();
                    //overTimeList = _overtimeService.LoadOvertime(data.Date, organizationIdList, mentorTeamMemberIdList, pinList);
                    overTimeList = _overtimeService.LoadOvertime(data.Date, data.Date, mentorTeamMemberIdList, pinList);
                    ViewBag.OverTimeList = overTimeList;
                    //ViewBag.SuccessMessage = "successfull";
                }
                else
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }

            return View();
        }

        public ActionResult OverTimeSaveByMentor(OverTimeFormViewModel data)
        {
            try
            {
                if (data == null || !data.OverTimeList.Any())
                {
                    return Json(new Response(false, "No data found for update"));
                }
                List<int> memberPinList = data.OverTimeList.Select(x => x.Pin).ToList();
                if (!memberPinList.Any())
                    return Json(new Response(false, "Please select TeamMember"));
                string pinList = string.Join(",", memberPinList);
                var mentor = _teamMemberService.GetCurrentMember();
                var mentorTeamMemberIdList = _mentorHistoryService.LoadMentorTeamMemberId(_commonHelper.ConvertIdToList(mentor.Id), DateTime.Now, null, memberPinList);
                if (!mentorTeamMemberIdList.Any())
                {
                    return Json(new Response(false, "You have no TeamMember to Mentor"));
                }
                IList<OverTimeDto> overTimeListDto = _overtimeService.LoadOvertime(data.Date, data.Date, mentorTeamMemberIdList, pinList);

                IList<Overtime> overTimeList = new List<Overtime>();
                foreach (var ovt in data.OverTimeList)
                {
                    if (ovt.Pin != 0)
                    {
                        var exist = overTimeListDto.FirstOrDefault(x => x.Pin == ovt.Pin);
                        if (exist != null)
                        {
                            if (ovt.OverTime > exist.TotalOvertime)
                            {
                                throw new InvalidDataException("Over Time is greater then extra time for pin (" + ovt.Pin + ")!! ");
                               // return Json(new Response(false, "Over Time is greater then extra time !!"));
                            }
                            var overTime = new Overtime();
                            overTime = _overtimeService.GetOverTime(ovt.Pin, data.Date);
                            if (overTime != null)
                            {
                                overTime.ApprovedTime = ovt.OverTime;
                            }
                            else
                            {
                                overTime = new Overtime();
                                TeamMember member = _teamMemberService.GetMember(Convert.ToInt32(ovt.Pin));
                                if(member == null)
                                    throw new InvalidDataException("Invalid Team Member found for Pin: " + ovt.Pin);
                                overTime.ApprovedTime = ovt.OverTime;
                                overTime.TeamMember = member;
                                overTime.OverTimeDate = data.Date;

                            }
                            overTimeList.Add(overTime);
                        }
                    }
                }
                _overtimeService.Save(overTimeList);
                return Json(new Response(true, "Over Time Approval Successful"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #region Over time approval by HR
        public ActionResult OverTimeByHr()
        {
            return View();
        }

        public ActionResult OverTimeByHrApproval()
        {
            ViewBag.ShowNoData = false;
            return View();
        }

        [HttpPost]
        public ActionResult OverTimeByHr(OverTimeFormViewModel data)
        {
            ViewBag.ShowNoData = true;
            try
            {
                if (ModelState.IsValid)
                {

                    var pinList = (!String.IsNullOrEmpty(data.PinList)) ? data.PinList.Trim() : "";
                    var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    pinList = regex.Replace(pinList, @",");
                    var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    //var organizationList = _organizationService.LoadAuthorizedOrganization(userMenu);
                    //var organizationIdList = organizationList.Select(x => x.Id).ToList();
                    var mentorTeamMemberIdList = new List<long>();
                    IList<OverTimeDto> overTimeList = new List<OverTimeDto>();
                    //overTimeList = _overtimeService.LoadOvertime(data.Date, organizationIdList, mentorTeamMemberIdList, pinList);
                    overTimeList = _overtimeService.LoadOvertime(data.Date, data.Date, mentorTeamMemberIdList, pinList, userMenu);
                    ViewBag.OverTimeList = overTimeList;
                    //ViewBag.SuccessMessage = "successfull";
                }
                else
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }

            return View();
        }

        public ActionResult OverTimeSaveByHr(OverTimeFormViewModel data)
        {
            try
            {
                if (data == null || !data.OverTimeList.Any())
                {
                    return Json(new Response(false, "No data found for update"));
                }
                List<int> memberPinList = data.OverTimeList.Select(x => x.Pin).ToList();
                if (!memberPinList.Any())
                    return Json(new Response(false, "Please select TeamMember"));
                string pinList = string.Join(",", memberPinList);

                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var mentorTeamMemberIdList = new List<long>();
                IList<OverTimeDto> overTimeListDto = _overtimeService.LoadOvertime(data.Date, data.Date, mentorTeamMemberIdList, pinList, userMenu);

                IList<Overtime> overTimeList = new List<Overtime>();
                foreach (var ovt in data.OverTimeList)
                {
                    if (ovt.Pin != 0)
                    {
                        var exist = overTimeListDto.FirstOrDefault(x => x.Pin == ovt.Pin);
                        if (exist != null)
                        {
                            if (ovt.OverTime > exist.TotalOvertime)
                            {
                                throw new InvalidDataException("Over Time is greater then extra time for pin (" + ovt.Pin + ")!! ");
                                //return Json(new Response(false, "Over Time is greater then extra time !!"));
                            }
                            var overTime = new Overtime();
                            overTime = _overtimeService.GetOverTime(ovt.Pin, data.Date);
                            if (overTime != null)
                            {
                                overTime.ApprovedTime = ovt.OverTime;
                            }
                            else
                            {
                                overTime = new Overtime();
                                TeamMember member = _teamMemberService.GetMember(Convert.ToInt32(ovt.Pin));
                                overTime.ApprovedTime = ovt.OverTime;
                                overTime.TeamMember = member;
                                overTime.OverTimeDate = data.Date;

                            }
                            overTimeList.Add(overTime);
                        }
                    }
                }
                _overtimeService.Save(overTimeList);
                //_overtimeService.Save(date, pinList, fullValues, halfValues, noValues);
                return Json(new Response(true, "Over Time Approval Successful"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion
        
        #region Over time History

        public ActionResult OverTimeHistory()
        {
            try
            {
                SelectList branch = null, organization = null, campus = null;
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.organizationlist = organization;
                ViewBag.BranchId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.CampusId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.DepartmentId = new SelectList(string.Empty, "Value", "Text");
                ViewBag.dateFromField = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("yyyy-MM-dd");
                ViewBag.dateToField = DateTime.Now.ToString("yyyy-MM-dd");
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return PartialView("_OvertimeHistory");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }

        }

        [HttpPost]
        //public ActionResult LoadOverTimeHistory(int draw, int start, int length, long? organizationId, long? departmentId, long? branchId, long? campusId, string pin, string date)
        public ActionResult LoadOverTimeHistory(int draw, int start, int length, long? organizationId, long? departmentId, long? branchId, long? campusId, string pin, string dateFrom = "", string dateTo = "")
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    var mentorTeamMemberIdList = new List<long>();

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        // orderBy = nvc["order[0][column]"];
                        orderBy = "Pin";
                        orderDir = nvc["order[0][dir]"];
                    }
                    #endregion

                    DateTime dateFromDateTime = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).Date;
                    DateTime dateToDateTime = DateTime.Today.Date;
                    if (!String.IsNullOrEmpty(dateFrom))
                    {
                        dateFromDateTime = Convert.ToDateTime(dateFrom).Date;
                    }
                    if (!String.IsNullOrEmpty(dateTo))
                    {
                        dateToDateTime = Convert.ToDateTime(dateTo).Date;
                    }

                    if (dateFromDateTime > dateToDateTime)
                    {
                        DateTime temp = dateToDateTime;
                        dateToDateTime = dateFromDateTime;
                        dateFromDateTime = temp;
                    }

                    int recordsTotal = _overtimeService.LoadOvertimeCount(dateFromDateTime, dateToDateTime, mentorTeamMemberIdList, pin.Trim(), userMenu, organizationId, branchId, campusId, departmentId, true);
                    long recordsFiltered = recordsTotal;
                    IList<OverTimeDto> dataList = _overtimeService.LoadOvertime(dateFromDateTime, dateToDateTime, mentorTeamMemberIdList, pin, userMenu, organizationId, branchId, campusId, departmentId, start, length, orderBy, orderDir, true);
                    var data = new List<object>();
                    foreach (var c in dataList)
                    {
                        var str = new List<string>();
                        str.Add(c.OverTimeDate.ToString("yyyy-MM-dd"));
                        str.Add(c.Pin.ToString());
                        str.Add(c.Name);
                        str.Add(c.Department);
                        str.Add(c.Organization);
                        str.Add(c.Branch);
                        str.Add(c.Campus);
                        str.Add(c.Designation);
                        str.Add(c.EarlyEntry.ToString("HH:mm"));
                        str.Add(c.LateLeave.ToString("HH:mm"));
                        str.Add(c.TotalOvertime.ToString("HH:mm"));
                        str.Add("<span class='overtimeclass' style='cursor:pointer;'>" + c.ApprovedTime.ToString("HH:mm") + "</span><input type='text' name='overtimeinline' class='form-control overtimeinline' style='width: 65px; display:none;' data-pin='" + c.Pin + "' data-date='" + c.OverTimeDate + "' value='" + c.ApprovedTime.ToString("HH:mm") + "' placeholder='HH:mm' size='5' maxlength='5' />");
                        //str.Add(c.ApprovedTime.ToString("HH:mm"));
                        str.Add(c.ModifiedBy != null ? _userService.GetUserNameByAspNetUserId((long)c.ModifiedBy) : "-");
                        str.Add(CommonHelper.GetReportCommonDateTimeFormat(c.LastModificationDate));
                        data.Add(str);
                    }
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = recordsTotal,
                        recordsFiltered = recordsFiltered,
                        start = start,
                        length = length,
                        data = data
                    });

                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                    _logger.Error(ex);
                    var data = new List<object>();
                    return Json(new
                    {
                        draw = draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        start = start,
                        length = length,
                        data = data
                    });
                }
            }
            return HttpNotFound();
        }

        #endregion

        #region Update

        [HttpPost]
        public ActionResult Edit(int Pin, DateTime overTimeDate, DateTime overTime)
        {
            try
            {
                //var overtimeobj = _overtimeService.LoadById(id); 
                _overtimeService.Update(Pin, overTimeDate, overTime);
                return Json(new Response(true, "Update Successfully"));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                return Json(new Response(false, WebHelper.CommonErrorMessage));
                _logger.Error(ex);
                return View();
            }
        }
        #endregion

        // GET: Hr/OverTime
        public ActionResult Index()
        {
            return View();
        }
    }
}