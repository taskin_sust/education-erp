using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Dao.Mapping.Base;


namespace UdvashERP.Dao.Mapping.Hr
{


    public partial class ChildrenAllowanceSettingMap : BaseClassMap<ChildrenAllowanceSetting, long>
    {

        public ChildrenAllowanceSettingMap()
        {
            Table("HR_ChildrenAllowanceSetting");
            LazyLoad();
            References(x => x.Organization).Column("OrganizationId");
            Map(x => x.Name).Column("Name").Not.Nullable();
            Map(x => x.MaxNoOfChild).Column("MaxNoOfChild").Not.Nullable();
            Map(x => x.Amount).Column("Amount").Not.Nullable();
            Map(x => x.StartingAge).Column("StartingAge").Not.Nullable();
            Map(x => x.ClosingAge).Column("ClosingAge").Not.Nullable();
            Map(x => x.PaymentType).Column("PaymentType").Not.Nullable();
            Map(x => x.EffectiveDate).Column("EffectiveDate").Not.Nullable();
            Map(x => x.ClosingDate).Column("ClosingDate").Nullable();
            Map(x => x.IsPermanent).Column("IsPermanent").Not.Nullable();
            Map(x => x.IsProbation).Column("IsProbation").Not.Nullable();
            Map(x => x.IsPartTime).Column("IsPartTime").Not.Nullable();
            Map(x => x.IsContractual).Column("IsContractual").Not.Nullable();
            Map(x => x.IsIntern).Column("IsIntern").Not.Nullable();
            HasMany(x => x.PrAllowanceBlock).KeyColumn("ChildrenAllowanceId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
