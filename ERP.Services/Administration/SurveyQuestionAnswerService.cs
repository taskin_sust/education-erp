﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Administration
{
    public interface ISurveyQuestionAnswerService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        SurveyQuestionAnswer LoadSurveyQuestionAnswer(long answerId, long programId, long sessionId);
        #endregion

        #region List Loading Function
        IList<SurveyQuestionAnswer> LoadSurveyQuestionAnswer(long programId, long sessionId, long[] questionId);
        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class SurveyQuestionAnswerService : BaseService, ISurveyQuestionAnswerService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ISurveyQuestionAnswerDao _surveyQuestionAnswerDao;
        public SurveyQuestionAnswerService(ISession session)
        {
            Session = session;

            _surveyQuestionAnswerDao = new SurveyQuestionAnswerDao() { Session = Session };

        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        public SurveyQuestionAnswer LoadSurveyQuestionAnswer(long answerId, long programId, long sessionId)
        {
            try
            {
                return _surveyQuestionAnswerDao.LoadSurveyQuestionAnswer(answerId, programId, sessionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function
        public IList<SurveyQuestionAnswer> LoadSurveyQuestionAnswer(long programId, long sessionId, long[] questionId)
        {
            try
            {
                return _surveyQuestionAnswerDao.LoadSurveyQuestionAnswer(programId, sessionId, questionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
