﻿using System;
using System.Configuration;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.Models;
using SubjectAssign = UdvashERP.BusinessModel.Dto.SubjectAssign;


namespace UdvashERP.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class UserRoleLongPk : IdentityUserRole<long>
    {

    }

    public class UserClaimLongPk : IdentityUserClaim<long>
    {
    }

    public class UserLoginLongPk : IdentityUserLogin<long>
    {
    }

    public class RoleLongPk : IdentityRole<long, UserRoleLongPk>
    {
        public RoleLongPk() { }
        public RoleLongPk(string name) { Name = name; }
    }

    public class UserStoreLongPk : UserStore<ApplicationUser, RoleLongPk, long, UserLoginLongPk, UserRoleLongPk, UserClaimLongPk>
    {
        public UserStoreLongPk(ApplicationDbContext context): base(context)
        {
        }
    }

    public class RoleStoreLongPk : RoleStore<RoleLongPk, long, UserRoleLongPk>
    {
        public RoleStoreLongPk(ApplicationDbContext context): base(context)
        {
        }
    }

    public class ApplicationUser : IdentityUser<long, UserLoginLongPk, UserRoleLongPk, UserClaimLongPk>
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public ApplicationUser()
        {
            CreationDate = DateTime.Now;
            ModificationDate = DateTime.Now;
        }

        /*public long UserProfileId { get; set; }*/
        public virtual int Rank { get; set; }
        public virtual string FullName { get; set; }
        public virtual int VersionNumber { get; set; }
        public virtual string BusinessId { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual DateTime ModificationDate { get; set; }
        public virtual int Status { get; set; }
        public virtual  long CreateBy{ get; set; }
        public virtual long ModifyBy { get; set; }

    }

    public class ApplicationUserManager : UserManager<ApplicationUser, long>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser, long> store): base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStoreLongPk(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser, long>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true,
                
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                //RequireNonLetterOrDigit = true,
                //RequireDigit = true,
                //RequireLowercase = true,
                //RequireUppercase = true,
            };
            manager.RegisterTwoFactorProvider("PhoneCode", new PhoneNumberTokenProvider<ApplicationUser, long>
            {
                MessageFormat = "Your security code is: {0}"
            });
            manager.RegisterTwoFactorProvider("EmailCode", new EmailTokenProvider<ApplicationUser, long>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is: {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser, long>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }


    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.

            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your sms service here to send a text message.
            return Task.FromResult(0);
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, RoleLongPk, long, UserLoginLongPk, UserRoleLongPk, UserClaimLongPk>
    {
        public ApplicationDbContext(): base("UdvashErpConnectionString")
        {

        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        //public System.Data.Entity.DbSet<UdvashERP.BusinessModel.Entity.MenuGroup> MenuGroups { get; set; }
        //public System.Data.Entity.DbSet<Branch> Branches { get; set; }
        //public System.Data.Entity.DbSet<UdvashERP.BusinessModel.Entity.MaterialType> MaterialTypes { get; set; }
        //public DbSet<SubjectAssign> SubjectAssigns { get; set; }
        //public System.Data.Entity.DbSet<UdvashERP.BusinessModel.Entity.Program> Programs { get; set; }
        //public System.Data.Entity.DbSet<UdvashERP.BusinessModel.Entity.ProgramBranchSession> ProgramBranchSessions { get; set; }
        //public System.Data.Entity.DbSet<UdvashERP.BusinessModel.Entity.Campus> Campus { get; set; }
        //public System.Data.Entity.DbSet<UdvashERP.BusinessModel.Entity.Course> Courses { get; set; }
        //public System.Data.Entity.DbSet<UdvashERP.BusinessModel.ViewModel.DiscountViewModel> DiscountViewModels { get; set; }
    }
}