﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Utils;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Mapping;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Exam
{
    public interface IExamsDao : IBaseDao<Exams, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Functions
        IList<Exams> GetByExamCode(string examCode);
        Exams GetExams(string examCode, long courseId);

        #endregion

        #region List Loading Functions
        IList<Exams> LoadExams(List<long> examIds);
        IList<Exams> GetExamByProgramSessionAndCourse(long programId, long sessionId, long courseId);
        IList<Exams> CheckDuplicateExamCode(Exams createExams, long batchId, long branchId);
        List<Exams> GetExamList(int start, int length, string orderBy, string direction, string organization, string program, string session, string course, string name, string shortName, string code, string status, string rank);
        IList<Exams> GetExamByProgramSessionCourseAutoComplete(string query, long programId, long sessionId, long? courseId, bool? isMcq = null, bool isQuestion = false);
        IList<Exams> GetExamCodeByAutoComplete(string query, long programId, long sessionId, long[] courseId);
        IList<Exams> CheckDuplicateExamCodeForEdit(Exams editExams, long examId);
        IList<Exams> GetExamByProgramAndSessionAndBranchsAndCampusesAndBatches(long programId, long sessionId, long[] courseId, long[] branchId, long[] campusId, long[] batchId);
        IList<Exams> LoadExamByProgramAndSessionAndBranchsAndCampusesAndBatches(List<long> authorizedProgramIdList, long sessionId, List<long> courseId,
            List<long> authorizedBranchIdList, List<long> campusId, List<long> batchId, long[] subjectIds, DateTime? dateFrom, DateTime? dateTo, bool? returnDoneExams = null);
        IList<Exams> LoadExam(string query, long courseId);
        IList<Exams> LoadExam(long courseId);
        IList<Exams> CheckDuplicateExamName(Exams exam, long? examId = null);

        IList<Exams> CheckDuplicateExamShortName(Exams exam, long? examId = null);
        IList<Exams> LoadExams(long programId, long sessionId, long[] courseId, List<Batch> batchList, List<Branch> branchList, bool? enterProgress = null, bool? manageProgress = null, bool? attendanceGiven = null, long[] subjectIdList = null, DateTime? dateFrom = null, DateTime? dateTo = null, bool? clearExamAttendance = null, long[] userIdList = null);
        IList<Exams> LoadExams(long programId, long sessionId, List<long> courseList, DateTime dateFrom, DateTime dateTo);
        IList<Exams> LoadExam(long programId, long sessionId, long branchId, long batchId, long courseId, string examName, long[] mcqSubject, long[] writtenSubject);

        #endregion

        #region Others Function
        IList<long> LoadExamIds(string studentProgramRoll);
        string GetExamName(long selectedExamId);
        bool IsRegisteredExam(StudentProgram studentProgram, long examId);

        bool CheckDuplicateIndividualExamName(long parentExamId, long examId, string examName);

        #endregion

        IList<long> LoadCompExamIds(List<long> complementaryCourseIdList);

    }
    public class ExamsDao : BaseDao<Exams, long>, IExamsDao
    {

        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Functions
        public IList<Exams> GetByExamCode(string examCode)
        {
            return
                Session.QueryOver<Exams>()
                    .Where(x => x.Code == examCode.Trim() && x.Status == Exams.EntityStatus.Active)
                    .List<Exams>();
        }

        public Exams GetExams(string examCode, long courseId)
        {
            return
                Session.QueryOver<Exams>()
                    .Where(x => x.Code == examCode.Trim() && x.Status == Exams.EntityStatus.Active && x.Course.Id == courseId)
                    .SingleOrDefault<Exams>();
        }

        #endregion

        #region List Loading Functions
        public IList<Exams> LoadExams(List<long> examIds)
        {
            return Session.QueryOver<Exams>().Where(x => x.Id.IsIn(examIds.ToArray())).List<Exams>();
        }
        public List<Exams> GetExamList(int start, int length, string orderBy, string direction, string organization, string program, string session, string course,
            string name, string shortName, string code, string status, string rank)
        {

            ICriteria criteria = Session.CreateCriteria<Exams>().Add(Restrictions.Not(Restrictions.Eq("Status", Referrer.EntityStatus.Delete)));
            criteria.CreateAlias("Program", "p");
            criteria.CreateAlias("Session", "s");
            criteria.CreateAlias("Course", "c");
            criteria.CreateAlias("p.Organization", "o");
            if (!String.IsNullOrEmpty(organization))
            {
                criteria.Add(Restrictions.Eq("o.Id", Convert.ToInt64(organization)));
                //criteria.Add(Restrictions.Like("o.Id", program, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(program))
            {
                criteria.Add(Restrictions.Eq("p.Id", Convert.ToInt64(program)));
                //criteria.Add(Restrictions.Like("p.Name", program, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(session))
            {
                criteria.Add(Restrictions.Eq("s.Id", Convert.ToInt64(session)));
                //criteria.Add(Restrictions.Like("s.Name", session, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(course))
            {
                criteria.Add(Restrictions.Eq("c.Id", Convert.ToInt64(course)));
                //criteria.Add(Restrictions.Like("c.Name", course, MatchMode.Anywhere));
            }

            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(shortName))
            {
                criteria.Add(Restrictions.Like("ShortName", shortName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(code))
            {
                criteria.Add(Restrictions.Like("Code", code, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));

            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }

            if (direction == "ASC")
            {
                criteria.AddOrder(Order.Asc(orderBy));
            }
            else
            {
                criteria.AddOrder(Order.Desc(orderBy));
            }
            return (List<Exams>)criteria.List<Exams>();
        }

        public IList<Exams> GetExamByProgramSessionCourseAutoComplete(string query, long programId, long sessionId, long? courseId, bool? isMcq = null, bool isQuestion = false)
        {
            ICriteria criteria =
                    Session.CreateCriteria<Exams>()
                        .Add(Restrictions.Eq("Status", Exams.EntityStatus.Active));

            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            //if (isQuestion)
            //    criteria.CreateAlias("ExamMcqQuestions", "emq").Add(Restrictions.Eq("emq.Status", Program.EntityStatus.Active));
            if (courseId > 0)
                criteria.Add(Restrictions.Eq("Course.Id", courseId));
            if (!String.IsNullOrEmpty(query))
            {
                criteria.Add(Restrictions.Like("Code", query, MatchMode.Start));
            }
            if (isMcq != null)
            {
                criteria.Add(Restrictions.Eq("IsMcq", isMcq));
            }
            if (isQuestion)
            {
                DetachedCriteria emqList = DetachedCriteria.For<ExamMcqQuestion>().
                    SetProjection(Projections.Distinct(Projections.Property("Exams.Id")))
                    .CreateAlias("Exams", "exm").Add(Restrictions.Eq("exm.Status", Branch.EntityStatus.Active));
                criteria.Add(Subqueries.PropertyIn("Id", emqList));
            }
            //criteria.SetFirstResult(0).SetMaxResults(10);
            return criteria.List<Exams>();
        }
        public IList<Exams> GetExamCodeByAutoComplete(string query, long programId, long sessionId, long[] courseId)
        {
            ICriteria criteria =
                    Session.CreateCriteria<Exams>()
                        .Add(Restrictions.Eq("Status", Exams.EntityStatus.Active));

            criteria.Add(Restrictions.Eq("Program.Id", programId));
            criteria.Add(Restrictions.Eq("Session.Id", sessionId));
            if (courseId[0] != 0)
                criteria.Add(Restrictions.In("Course.Id", courseId));
            if (!String.IsNullOrEmpty(query))
            {
                criteria.Add(Restrictions.Like("Code", query, MatchMode.Start));
            }
            //criteria.SetFirstResult(0).SetMaxResults(10);
            return criteria.List<Exams>();
        }
        public IList<Exams> GetExamByProgramSessionAndCourse(long programId, long sessionId, long courseId)
        {
            return
                Session.QueryOver<Exams>()
                    .Where(
                        x =>
                            x.Program.Id == programId && x.Session.Id == sessionId && x.Course.Id == courseId &&
                            x.Status == Exams.EntityStatus.Active)
                    .List<Exams>();
        }

        public IList<Exams> CheckDuplicateExamCode(Exams createExams, long batchId, long branchId)
        {
            long programId = createExams.Program.Id;
            long sessionId = createExams.Session.Id;
            long courseId = createExams.Course.Id;
            // long batchId = createExams.Batch.Id;
            // long branchId = createExams.Branch.Id;
            string examsCode = createExams.Code;
            return
                  Session.QueryOver<Exams>()
                      .Where(
                          x =>
                              x.Program.Id == programId
                              && x.Session.Id == sessionId
                              //&& x.Course.Id == courseId
                              //&& (x.Batch.Id == batchId || x.Batch.Id == null)
                              //&& (x.Branch.Id == branchId || x.Branch.Id == null)
                              && x.Code == examsCode
                              && x.Status == Exams.EntityStatus.Active)
                      .List<Exams>();
        }

        public IList<Exams> CheckDuplicateExamCodeForEdit(Exams editExams, long examId)
        {
            long programId = editExams.Program.Id;
            long sessionId = editExams.Session.Id;
            long courseId = editExams.Course.Id;
            long batchId = editExams.Batch != null ? editExams.Batch.Id : 0;
            long branchId = editExams.Branch != null ? editExams.Branch.Id : 0;
            string examsCode = editExams.Code;
            return
                  Session.QueryOver<Exams>()
                      .Where(
                          x =>
                              x.Id != examId
                              && x.Program.Id == programId
                              && x.Session.Id == sessionId
                              //&& x.Course.Id == courseId
                              // && (x.Batch.Id == batchId || x.Batch.Id == null)
                              // && (x.Branch.Id == branchId || x.Branch.Id == null)
                              && x.Code == examsCode
                              && x.Status == Exams.EntityStatus.Active)
                      .List<Exams>();
        }

        public IList<Exams> GetExamByProgramAndSessionAndBranchsAndCampusesAndBatches(
            long programId, long sessionId, long[] courseId, long[] branchId,
            long[] campusId, long[] batchId)
        {
            var criteria = Session.CreateCriteria<Exams>();
            criteria.Add(Restrictions.Eq("Status", Exams.EntityStatus.Active));
            criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Session", "s")
                .Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("p.Id", programId));
            criteria.Add(Restrictions.Eq("s.Id", sessionId));

            if (courseId.Length == 1 && courseId[0] == 0)
            {
                //criteria.Add(Restrictions.Eq("Course", null));
            }
            else
            {
                criteria.CreateAlias("Course", "cou");
                criteria.Add(Restrictions.In("cou.Id", courseId))
                    .Add(Restrictions.Eq("cou.Status", BusinessModel.Entity.Administration.Course.EntityStatus.Active));
            }

            if (branchId.Length == 1 && branchId[0] == 0)
            {
                // criteria.Add(Restrictions.IsNull("Branch"));
            }
            else
            {
                criteria.Add(Restrictions.Or(Restrictions.IsNull("Branch"), Restrictions.In("Branch.Id", branchId)));

            }

            if (batchId.Length == 1 && batchId[0] == 0)
            {
                //criteria.Add(Restrictions.IsNull("Batch"));
            }
            else
            {
                criteria.Add(Restrictions.Or(Restrictions.IsNull("Batch"), Restrictions.In("Batch.Id", batchId)));

            }

            var ss = criteria.List<Exams>();
            return ss;

        }


        public IList<Exams> LoadExamByProgramAndSessionAndBranchsAndCampusesAndBatches(List<long> authorizedProgramIdList, long sessionId, List<long> courseId,
            List<long> authorizedBranchIdList, List<long> campusId, List<long> batchId, long[] subjectIds, DateTime? dateFrom, DateTime? dateTo, bool? returnDoneExams = null)
        {
            var criteria = Session.CreateCriteria<Exams>();
            criteria.Add(Restrictions.Eq("Status", Exams.EntityStatus.Active));
            criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
            criteria.CreateAlias("Session", "s").Add(Restrictions.Eq("s.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
            criteria.Add(Restrictions.In("p.Id", authorizedProgramIdList));
            criteria.Add(Restrictions.Eq("s.Id", sessionId));

            if (!courseId.Contains(SelectionType.SelelectAll))
            {
                criteria.CreateAlias("Course", "cou");
                criteria.Add(Restrictions.In("cou.Id", courseId)).Add(Restrictions.Eq("cou.Status", Course.EntityStatus.Active));
            }

            if (!authorizedBranchIdList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.Or(Restrictions.IsNull("Branch"), Restrictions.In("Branch.Id", authorizedBranchIdList)));
            }

            if (!batchId.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.Or(Restrictions.IsNull("Batch"), Restrictions.In("Batch.Id", batchId)));
            }
            if (dateFrom != null && dateTo != null)
            {
                DetachedCriteria studentAttendExam = DetachedCriteria.For<StudentExamAttendance>().SetProjection(Projections.Distinct(Projections.Property("Exams.Id")))
                .Add(Restrictions.Between("HeldDate", dateFrom, dateTo));
                criteria.Add(Subqueries.PropertyIn("Id", studentAttendExam));

            }
            if (subjectIds != null)
            {
                if (!subjectIds.Contains(SelectionType.SelelectAll))
                {
                    DetachedCriteria examDetails = DetachedCriteria.For<ExamsDetails>().SetProjection(Projections.Distinct(Projections.Property("Exams.Id")));
                    examDetails.Add(Restrictions.Or(Restrictions.IsNull("Subject"), Restrictions.In("Subject", subjectIds)));
                    //.Add(Restrictions.In("Subject", subjectIds));

                    criteria.Add(Subqueries.PropertyIn("Id", examDetails));
                }
            }
            var ss = criteria.List<Exams>();
            if (returnDoneExams == true)
            {
                var doneExamIds =
                    Session.QueryOver<ExamProgress>()
                        .Where(x => x.Status == 1)
                        .Select(x => x.Exam.Id)
                        .List<long>()
                        .Distinct().ToList();
                ss = ss.Where(x => x.Id.In(doneExamIds.ToArray())).ToList();
            }
            if (returnDoneExams == false)
            {
                var doneExamIds = new List<long>();
                if (batchId.Contains(0))
                {
                    doneExamIds = Session.QueryOver<ExamProgress>()
                        .Where(x => x.Status == 1)
                        .Select(x => x.Exam.Id)
                        .List<long>()
                        .Distinct().ToList();
                }
                else
                {
                    Session.QueryOver<ExamProgress>()
                        .Where(x => x.Status == 1 && x.Batch.Id.IsIn(batchId))
                        .Select(x => x.Exam.Id)
                        .List<long>()
                        .Distinct().ToList();
                }

                ss = ss.Where(x => !x.Id.IsIn(doneExamIds.ToArray())).ToList();
            }
            return ss.OrderBy(x => x.Name).ToList();
        }

        public IList<Exams> LoadExam(string query, long courseId)
        {
            ICriteria criteria =
                    Session.CreateCriteria<Exams>()
                        .Add(Restrictions.Eq("Status", Exams.EntityStatus.Active));

            criteria.Add(Restrictions.Eq("Course.Id", courseId));
            if (!String.IsNullOrEmpty(query))
            {
                criteria.Add(Restrictions.Like("Code", query, MatchMode.Start));
            }
            criteria.SetFirstResult(0).SetMaxResults(10);
            return criteria.List<Exams>();
        }
        public IList<Exams> LoadExam(long courseId)
        {
            ICriteria criteria =
                    Session.CreateCriteria<Exams>()
                        .Add(Restrictions.Eq("Status", Exams.EntityStatus.Active));

            criteria.Add(Restrictions.Eq("Course.Id", courseId));
            // criteria.SetFirstResult(0).SetMaxResults(10);
            return criteria.List<Exams>();
        }

        public IList<Exams> CheckDuplicateExamName(Exams exam, long? examId = null)
        {
            long programId = exam.Program.Id;
            long sessionId = exam.Session.Id;
            string examName = exam.Name;
            if (examId != null)
            {
                return
                          Session.QueryOver<Exams>()
                              .Where(
                                  x =>
                                         x.Id != examId
                                      && x.Program.Id == programId
                                      && x.Session.Id == sessionId
                                      && x.Name == exam.Name
                                      && x.Status == Exams.EntityStatus.Active)
                              .List<Exams>();
            }
            return
                          Session.QueryOver<Exams>()
                              .Where(
                                  x =>
                                         x.Program.Id == programId
                                      && x.Session.Id == sessionId
                                      && x.Name == exam.Name
                                      && x.Status == Exams.EntityStatus.Active)
                              .List<Exams>();
        }

        public IList<Exams> CheckDuplicateExamShortName(Exams exam, long? examId = null)
        {
            long programId = exam.Program.Id;
            long sessionId = exam.Session.Id;
            string examShortName = exam.ShortName;
            if (examId != null)
            {
                return
                          Session.QueryOver<Exams>()
                              .Where(
                                  x =>
                                         x.Id != examId
                                      && x.Program.Id == programId
                                      && x.Session.Id == sessionId
                                      && x.ShortName == exam.ShortName
                                      && x.Status == Exams.EntityStatus.Active)
                              .List<Exams>();
            }
            return
                          Session.QueryOver<Exams>()
                              .Where(
                                  x =>
                                      x.Program.Id == programId
                                      && x.Session.Id == sessionId
                                      && x.ShortName == exam.ShortName
                                      && x.Status == Exams.EntityStatus.Active)
                              .List<Exams>();
        }

        public IList<Exams> LoadExams(long programId, long sessionId, long[] courseId,
            List<Batch> batchList, List<Branch> branchList, bool? enterProgress = null, bool? manageProgress = null, bool? attendanceGiven = null, long[] subjectIdList = null, DateTime? dateFrom = null, DateTime? dateTo = null, bool? clearExamAttendance = null, long[] userIdList = null)
        {
            var batchIdList = batchList.Select(x => x.Id).ToArray();
            var examListQuery =
                Session.QueryOver<Exams>()
                    .Where(x => x.Program.Id == programId && x.Session.Id == sessionId
                                && (x.Branch == null || x.Branch.Id.IsIn(branchList.Select(y => y.Id).ToArray()))
                                && (x.Batch == null || x.Batch.Id.IsIn(batchIdList))
                                && x.Status == 1
                    );
            if (!courseId.Contains(0))
            {
                examListQuery = examListQuery.Where(x => x.Course.Id.IsIn(courseId));
            }
            if (enterProgress == true)
            {
                var examInExamProgress =
                 Session.QueryOver<ExamProgress>()
                     .Where(x => x.Batch.Id.IsIn(batchIdList) && x.Status == 1)
                     .Select(x => x.Exam.Id)
                     .List<long>().Distinct().ToArray();
                examListQuery = examListQuery.Where(x => !x.Id.IsIn(examInExamProgress));
            }
            if (manageProgress == true)
            {
                var examInExamProgress =
                Session.QueryOver<ExamProgress>()
                    .Where(x => x.Batch.Id.IsIn(batchIdList) && x.Status == 1)
                    .Select(x => x.Exam.Id)
                    .List<long>().Distinct().ToArray();
                examListQuery = examListQuery.Where(x => x.Id.IsIn(examInExamProgress));
            }
            if (attendanceGiven == true)
            {
                var examInExamProgress =
               Session.QueryOver<ExamProgress>()
                   .Where(x => x.Batch.Id.IsIn(batchIdList) && x.HeldDate >= dateFrom && x.HeldDate <= dateTo.Value.AddDays(1) && x.Status == 1)
                   .Select(x => x.Exam.Id)
                   .List<long>().Distinct().ToArray();
                //examListQuery = examListQuery.Where(x => x.Id.IsIn(examInExamProgress));


                var query = "select distinct e.Id from Exams e inner join ExamsDetails ed on e.Id=ed.ExamId";
                query += " where e.status=1 and (ed.subjecttype=3 or ed.SubjectId in(" + string.Join(", ", subjectIdList) +
                         ") )and e.ProgramId=" + programId + " and e.SessionId=" + sessionId;
                if (!courseId.Contains(0))
                {
                    query += " and e.CourseId in(" + string.Join(", ", courseId) + ")";
                }
                IQuery iQuery = Session.CreateSQLQuery(query);
                iQuery.SetTimeout(2700);
                var examIdListBySubject = iQuery.List<long>().Distinct().ToArray();
                var examIdListByAttendance =
                    Session.QueryOver<StudentExamAttendance>()
                        .Where(x => x.HeldDate >= dateFrom && x.HeldDate <= dateTo.Value.AddDays(1) && x.Exams != null)
                        .Select(x => x.Exams.Id)
                        .List<long>()
                        .Distinct()
                        .ToArray();
                if (clearExamAttendance == null)
                {
                    examListQuery = examListQuery.Where(x => x.Id.IsIn(examInExamProgress));
                }

                examListQuery = examListQuery.Where(x => x.Id.IsIn(examIdListBySubject) && x.Id.IsIn(examIdListByAttendance));
            }
            var examList = examListQuery.List<Exams>();
            return examList.Where(x => x.Status == 1).OrderBy(x => x.Name.Trim()).ToList();
        }

        public IList<Exams> LoadExams(long programId, long sessionId, List<long> courseList, DateTime dateFrom,
            DateTime dateTo)
        {
            var query = @"select distinct e.Id,e.Name from Exams e inner join StudentExamAttendance sea on e.Id=sea.ExamId
                            where e.ProgramId={0} and e.SessionId={1} and e.CourseId in({2})
                            and e.Status=1 and sea.Status=1 
                            and sea.HeldDate>='{3}' and sea.HeldDate<'{4}' order by e.Name";
            query = string.Format(query, programId, sessionId, string.Join(",", courseList), dateFrom, dateTo.AddDays(1));
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<Exams>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<Exams>().ToList();
            return list;
        }

        public IList<Exams> LoadExam(long programId, long sessionId, long branchId, long batchId, long courseId, string examName,
            long[] mcqSubject, long[] writtenSubject)
        {
            string filter = "Where e.Status != " + Exams.EntityStatus.Delete + " and e.programId=" + programId + " and e.SessionId = " + sessionId + " and e.courseId = " + courseId;

            if (branchId > 0)
            {
                filter += " and e.BranchId = " + branchId;
            }
            else
            {
                filter += " and e.BranchId is null";
            }
            if (batchId > 0)
            {
                filter += " and e.BatchId = " + batchId;
            }
            else
            {
                filter += " and e.BatchId is null";
            }
            filter += " and e.Name like '" + examName + "%'";

            if (mcqSubject != null && writtenSubject != null)
            {
                var arr = mcqSubject.Union(writtenSubject).ToArray();
                filter += " and ed.SubjectId IN (" + string.Join(",", arr) + ")";
            }
            else if (mcqSubject != null)
            {
                filter += " and ed.SubjectId IN (" + string.Join(",", mcqSubject) + ")";
            }
            else
            {
                filter += " and ed.SubjectId IN (" + string.Join(",", writtenSubject) + ")";
            }
            string query = @"Select distinct e.Id, e.Name
            from [dbo].[Exams] as e
            inner join [dbo].[ExamsDetails] as ed ON e.Id = ed.ExamId " + filter;

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<Exams>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<Exams>().ToList();
            return list;

            //return new List<Exams>();
        }

        #endregion

        #region Others Function

        public IList<long> LoadExamIds(string studentProgramRoll)
        {
            var examIds =
                Session.Query<ExamsStudentMarks>()
                    .Where(x => x.StudentProgram.PrnNo == studentProgramRoll)
                    .Select(x => x.ExamsDetails.Exams.Id).Distinct()
                    .ToList();
            return examIds;
        }

        public IList<long> LoadCompExamIds(string studentProgramRoll)
        {
            var examIds =
                Session.Query<ExamsStudentMarks>()
                    .Where(x => x.StudentProgram.PrnNo == studentProgramRoll)
                    .Select(x => x.ExamsDetails.Exams.Id).Distinct()
                    .ToList();

            return examIds;
        }
        public IList<long> LoadCompExamIds(List<long> complementaryCourseIdList)
        {
            // var ints = complementaryCourseIdList.ToArray();
            //var examIds =
            //    Session.QueryOver<Exams>()
            //        .Where(x => x.Course.Id.IsIn(complementaryCourseIdList.Distinct().ToArray()))
            //        .Select(x => x.Id)
            //        .List();
            //return examIds.Select(x=>x.Id).Distinct().ToList();
            ICriteria criteria =
                    Session.CreateCriteria<Exams>()
                        .Add(Restrictions.Eq("Status", Exams.EntityStatus.Active));

            criteria.Add(Restrictions.In("Course.Id", complementaryCourseIdList.Distinct().ToArray()));
            // criteria.SetFirstResult(0).SetMaxResults(10);
            return criteria.List<Exams>().Select(x => x.Id).ToList();
        }


        public string GetExamName(long selectedExamId)
        {
            var examName =
                Session.QueryOver<Exams>()
                    .Where(x => x.Id == selectedExamId)
                    .Select(x => x.Name).SingleOrDefault<string>();

            return examName;
        }

        public bool IsRegisteredExam(StudentProgram studentProgram, long examId)
        {
            return false;
        }

        public bool CheckDuplicateIndividualExamName(long parentExamId, long examId, string examName)
        {
            var examList =
                Session.QueryOver<Exams>()
                    .Where(x => x.ExamParent.Id == parentExamId && x.Id != examId && x.Name == examName)
                    .List<Exams>();
            return examList != null && examList.Count > 0;
        }
        
        #endregion

        #region Helper Function

        #endregion
    }

}
