﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AllowanceTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Allowance")]
    public class AllowanceUpdateTest : AllowanceBaseTest
    {
        //#region Basic Test

        //[Fact]
        //public void Allowance_Update_By_Mentor_Should_Return_True()
        //{
        //    const string reason = "testing";
        //    DateTime date = DateTime.Now.AddDays(1);
        //    var teamMember = PersistTeamMember(MentorId);
        //    var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
        //    var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);

        //    _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
        //    _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
        //    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });
        //    var campus = teamMember.EmploymentHistory.Select(x => x.Campus).LastOrDefault();
        //    var userMenu = BuildUserMenu(new List<Organization>() { campus.Branch.Organization },
        //                                    null, new List<Branch>() { campus.Branch });

        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);
        //    _allowanceService.Save(userMenu, date.ToString("MM-dd-yyyy")
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "", true);
        //    var idList = Session.QueryOver<Allowance>().Select(x => x.Id).Where(x => x.TeamMember == teamMember).List<long>();
        //    _allowanceFactory.Object.Id = idList[0];
        //    _allowanceFactory.Object.IsApproved = true;
        //    _allowanceService.Update(userMenu, _allowanceFactory.Object, true);
        //    Assert.StrictEqual(_allowanceFactory.Object.IsApproved, true);
        //}

        //[Fact]
        //public void Allowance_Update_By_Hr_Should_Return_True()
        //{
        //    const string reason = "testing";
        //    DateTime date = DateTime.Now.AddDays(1);
        //    var teamMember = PersistTeamMember(MentorId);
        //    var startTime = new DateTime(date.Year, date.Month, date.Day, 10, 10, 10);
        //    var endTime = new DateTime(date.Year, date.Month, date.Day, 17, 10, 10);

        //    _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(teamMember);
        //    _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
        //    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });

        //    var branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).LastOrDefault();
        //    var userMenu = BuildUserMenu(new List<Organization>() { branch.Organization },
        //                                    null, new List<Branch>() { branch });


        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);
        //    _allowanceService.Save(userMenu, date.ToString("MM-dd-yyyy")
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "");
        //    var idList = Session.QueryOver<Allowance>().Select(x => x.Id).Where(x => x.TeamMember == teamMember).List<long>();
        //    _allowanceFactory.Object.Id = idList[0];

        //    _allowanceFactory.Object.IsApproved = true;
        //    _allowanceService.Update(userMenu, _allowanceFactory.Object, true);
        //    Assert.StrictEqual(_allowanceFactory.Object.IsApproved, true);
        //}

        //#endregion

        //#region Business Logic Test

        //[Fact]
        //public void InvalidDataException_Allowance_Update_For_UserMenu_Null()
        //{
        //    var allowanceFactory = _allowanceFactory.Create();
        //    Assert.Throws<ArgumentNullException>(() => _allowanceService.Update(null, allowanceFactory.Object));
        //}

        //[Fact]
        //public void InvalidDataException_Allowance_Update_For_TeamMember_Null()
        //{
        //    var teamMember = PersistTeamMember(MentorId);
        //    var branch = teamMember.EmploymentHistory.Select(x => x.Campus.Branch).LastOrDefault();
        //    var userMenu = BuildUserMenu(new List<Organization>() { branch.Organization },
        //                                    null, new List<Branch>() { branch });
        //    var allowanceFactory = _allowanceFactory.Create();
        //    Assert.Throws<InvalidDataException>(() => _allowanceService.Update(userMenu, allowanceFactory.Object));
        //}
        
        //[Fact]
        //public void InvalidDataException_Allowance_Update_For_Date_Null()
        //{
        //    const string reason = "testing";
        //    DateTime date = DateTime.Now.AddDays(1);
        //    var teamMember = PersistTeamMember(MentorId);
        //    var startTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 10, 10, 10);
        //    var endTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 17, 10, 10);
        //    var campus = teamMember.EmploymentHistory.Select(x => x.Campus).LastOrDefault();
        //    var userMenu = BuildUserMenu(new List<Organization>() { campus.Branch.Organization },
        //                                    null, new List<Branch>() { campus.Branch });

        //    _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(_teamMemberFactory.Object);
        //    _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
        //    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });
            
        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);
        //    _allowanceService.Save(userMenu, date.ToString("MM-dd-yyyy")
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "", true);
        //    var idList = Session.QueryOver<Allowance>().Select(x => x.Id).Where(x => x.TeamMember == _teamMemberFactory.Object).List<long>();
        //    _allowanceFactory.Object.Id = idList[0];

        //    _allowanceFactory.Object.DailyAllowanceDate = null;
        //    Assert.Throws<InvalidDataException>(() => _allowanceService.Update(userMenu, _allowanceFactory.Object));
        //}

        //[Fact]
        //public void InvalidDataException_Allowance_Update_For_Invalid_Mentor_Account()
        //{
        //    const string reason = "testing";
        //    DateTime date = DateTime.Now.AddDays(1);
        //    var teamMember = PersistTeamMember(MentorId);
        //    var startTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 10, 10, 10);
        //    var endTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 17, 10, 10);
        //    var campus = teamMember.EmploymentHistory.Select(x => x.Campus).LastOrDefault();
        //    var userMenu = BuildUserMenu(new List<Organization>() { campus.Branch.Organization },
        //                                    null, new List<Branch>() { campus.Branch });

        //    _attendanceAdjustmentFactory.CreateWith("Name", date, startTime, endTime, reason).WithTeamMember(_teamMemberFactory.Object);
        //    _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
        //    _attendanceAdjustmentService.SaveOrUpdate(new List<AttendanceAdjustment>() { _attendanceAdjustmentFactory.Object });

        //    var allowanceFactory = _allowanceFactory.Create().WithTeamMember(teamMember);
        //    _allowanceService.Save(userMenu, date.ToString("MM-dd-yyyy")
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture)
        //                            , allowanceFactory.Object.TeamMember.Pin.ToString(CultureInfo.InvariantCulture), "", true);
        //    var idList = Session.QueryOver<Allowance>().Select(x => x.Id).Where(x => x.TeamMember == _teamMemberFactory.Object).List<long>();
        //    _allowanceFactory.Object.Id = idList[0];

        //    using (ITransaction trans = Session.BeginTransaction())
        //    {
        //        for (int i = 0; i < _teamMemberFactory.Object.MentorHistory.Count; i++)
        //        {
        //            _teamMemberFactory.Object.MentorHistory.RemoveAt(i);
        //        }
        //        trans.Commit();
        //    }
        //    Assert.Throws<InvalidDataException>(() => _allowanceService.Update(userMenu, _allowanceFactory.Object, true));
        //}
        
        //#endregion
    }
}
