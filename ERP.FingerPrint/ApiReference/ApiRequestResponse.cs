﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using UdvashERP.FingerPrint.BusinessModel;
using UdvashERP.FingerPrint.BusinessRule;
using UdvashERP.FingerPrint.Helper;

namespace UdvashERP.FingerPrint.ApiReference
{
    public class ApiRequestResponse
    {
        private readonly string _baseUrl;
        private const string AttendanceDeviceTypeInfo = "GetAttendanceDeviceTypeInfo/AttendanceDeviceTypeApi/";
        private const string MemberInfo = "MemberInfo/GetMemberInfo?pin=";
        private const string SetMemberFingerPrintInfo = "MemberInfo/SetFingerPrint";
        //private const string SetMemberFingerPrintInfo = "MemberInfo/SetFingerPrint/";
        HttpHelper _httpHelper;

        public ApiRequestResponse()
        {
            _baseUrl = ConfigHelper.GetString(FingerPrintConstant.GetBaseUrl);
            _httpHelper = new HttpHelper();
        }

        public JsonResponse GetAttendanceDeviceTypeInfo()
        {
            JsonResponse response = new JsonResponse();
            try
            {
                string url = GenerateUrl(AttendanceDeviceTypeInfo);
                string jsonString = _httpHelper.GetRequest(url);
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public JsonResponse GetMemberInfo(string memberPin)
        {
            JsonResponse response = new JsonResponse();
            try
            {
                string url = GenerateUrl(MemberInfo, memberPin);
                string jsonString = _httpHelper.GetRequest(url);

                JavaScriptSerializer serializer = new JavaScriptSerializer();

               response= serializer.Deserialize<JsonResponse>(jsonString);
            }
            catch (Exception ex)
            {

            }
            return response;
        }

        public Response SetMemberFingerPrint(string jsonContent)
        {
            Response response = new Response();
            try
            {
                string url = GenerateUrl(SetMemberFingerPrintInfo);
               response= _httpHelper.HttpPostRequestX(url,jsonContent);
            }
            catch(Exception)
            {

            }
            return response;
        }

        private string GenerateUrl(string functionName)
        {
            try
            {
                return _baseUrl + functionName;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        private string GenerateUrl(string functionName, string pram)
        {
            try
            {
                return _baseUrl + functionName + pram;
            }
            catch (Exception ex)
            {

            }
            return null;
        }



    }


    public class Response
    {
        public bool IsSuccess { get; set; }
        public dynamic Data { get; set; }
    }
}
