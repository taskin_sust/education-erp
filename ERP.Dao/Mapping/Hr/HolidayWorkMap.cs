using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class HolidayWorkMap : BaseClassMap<HolidayWork,long> {
        
        public HolidayWorkMap() {
			Table("HR_HolidayWork");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.HolidayWorkDate).Column("HolidayWorkDate");
			Map(x => x.ApprovalType).Column("ApprovalType");
            Map(x => x.Reason).Column("Reason");
            Map(x => x.IsPost).Column("IsPost");
			//HasMany(x => x.HolidayWorkLog).KeyColumn("HolidayWorkId");
        }
    }
}
