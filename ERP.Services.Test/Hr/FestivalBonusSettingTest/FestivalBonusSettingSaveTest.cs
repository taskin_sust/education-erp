﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.FestivalBonusSettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "FestivalBonusSetting")]
    public class FestivalBonusSettingSaveTest : FestivalBonusSettingBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Return_Save_Successfully()
        {
            _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist();
            var obj = _festivalBonusSettingService.LoadById(_festivalBonusSettingFactory.Object.Id);
            Assert.NotNull(obj);
        }
        
        [Fact]
        public void Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _festivalBonusSettingService.SaveOrUpdate(null));
        }

        [Fact]
        public void Should_Return_Null_Exception_Empty_Organization()
        {
            _festivalBonusSettingFactory.Create()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object);
            Assert.Throws<NullObjectException>(
                () => _festivalBonusSettingService.SaveOrUpdate(_festivalBonusSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_InvalidDataException_For_BonusCalculation()
        {
            _festivalBonusSettingFactory.Create().WithOrganization();
            Assert.Throws<InvalidDataException>(
                () => _festivalBonusSettingService.SaveOrUpdate(_festivalBonusSettingFactory.Object));
        }

        [Fact]
        public void Should_Return_Property_Exception_For_Bonus_Calculation()
        {
            var fb = _festivalBonusSettingFactory.Create()
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object).Object;
            fb.PrFestivalBonusCalculation[0].PrFestivalBonusSetting = null;
            Assert.Throws<MessageException>(() => _festivalBonusSettingService.SaveOrUpdate(fb));
        }
        
        #endregion

        #region Business Logic Test

        [Fact]
        public void Should_Return_InvalidDataException_For_Date()
        {
            var fb = _festivalBonusSettingFactory.CreateWith(0, "test", DateTime.Now.AddDays(4), DateTime.Now.AddDays(2), true, true, true, true, true)
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object).Object;

            Assert.Throws<InvalidDataException>(() => _festivalBonusSettingService.SaveOrUpdate(fb));
        }

        [Fact]
        public void Save_Should_Return_InvalidDataException_Empty_Religion()
        {
            _festivalBonusSettingFactory.CreateWith(0, Guid.NewGuid().ToString(), DateTime.Now.AddDays(2), DateTime.Now.AddDays(5), false, false, false, false, false)
                .WithOrganization()
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object);
            Assert.Throws<InvalidDataException>(
                () => _festivalBonusSettingService.SaveOrUpdate(_festivalBonusSettingFactory.Object));
        }

        [Fact]
        public void Save_Should_Check_Duplicate()
        {
            organizationFactory.Create().Persist();
            var obj1 = _festivalBonusSettingFactory.Create()
                .WithOrganization(organizationFactory.Object)
                .WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object)
                .Persist().Object;
            var obj2 = _festivalBonusSettingFactory.CreateWith(0, obj1.Name, DateTime.Now.AddDays(2), DateTime.Now.AddDays(5), true,
               true, false, false, true).WithOrganization(organizationFactory.Object).WithFestivalBonusSettingCalculation(_festivalBonusSettingFactory.Object);
            Assert.Throws<DuplicateEntryException>(() => _festivalBonusSettingService.SaveOrUpdate(obj2.Object));
        } 

        #endregion
        
    }
}