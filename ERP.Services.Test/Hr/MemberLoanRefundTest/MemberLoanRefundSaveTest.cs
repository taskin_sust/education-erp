﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.Services.Helper;
using Xunit;

namespace UdvashERP.Services.Test.Hr.MemberLoanRefundTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "MemberLoanRefund")]
    public class MemberLoanRefundSaveTest : MemberLoanRefundBaseTest
    {
        [Fact]
        public void Save_Should_Be_Successfully()
        {
            #region prerequisit

            var teamMember = PersistTeamMember(2);
            var campus = teamMember.EmploymentHistory.Select(x => x.Campus).SingleOrDefault();
            var dept = teamMember.EmploymentHistory.Select(x => x.Department).FirstOrDefault();
            _salaryHistoryFactory.Create().WithTeamMember(teamMember).WithOrganization(campus.Branch.Organization).WithCampus(campus).WithDepartment(dept).Persist();

            #endregion

            #region loan application

            var memberLoanApplication = _memberLoanApplicationFactory.Create();
            memberLoanApplication.Object.TeamMember = teamMember;
            const decimal approvedAmount = 8000;
            const decimal monthlyRefund = 1000;
            var memberLoan = _memberLoanFactory.CreateWithLoanApprovedAmount(approvedAmount, monthlyRefund).WithMemberLoanApplication(memberLoanApplication.Object);
            _memberLoanService.SaveOrUpdate(new List<MemberLoan>() { memberLoan.Object });

            #endregion

            #region search member

            var userMenu = BuildUserMenu(campus.Branch.Organization, (Program)null, campus.Branch);
            List<long> authOrganizationIds = AuthHelper.LoadOrganizationIdList(userMenu);
            List<long> authBranchIds = AuthHelper.LoadBranchIdList(userMenu, authOrganizationIds);

            var authorizeTeamMember = _teamMemberService.LoadHrAuthorizedTeamMember(userMenu, null, authOrganizationIds, authBranchIds, null, null, null, commonHelper.ConvertIdToList(teamMember.Pin), false);

            #region salary info

            var organizationSalary = _teamMemberService.GetTeamMemberSalaryOrganization(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);
            var branchSalary = _teamMemberService.GetTeamMemberSalaryBranch(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);
            var campusSalary = _teamMemberService.GetTeamMemberSalaryCampus(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);
            var departmentSalary = _teamMemberService.GetTeamMemberSalaryDepartment(null, authorizeTeamMember[0].Id, authorizeTeamMember[0].Pin);

            #endregion

            var memberLoanRefund = new MemberLoanRefundViewModel
            {
                TeamMemberId = authorizeTeamMember[0].Id,
                MemberPin = authorizeTeamMember[0].Pin,
                MemberName = authorizeTeamMember[0].Name,
                Organization = organizationSalary,
                Branch = branchSalary,
                Campus = campusSalary,
                Department = departmentSalary
            };

            var previousLoan = _memberLoanRefundService.GetLoanBalance(authorizeTeamMember[0].Id);
            memberLoanRefund.LoanBalance = previousLoan.LoanBalance;

            #endregion

            #region Save Operation

            memberLoanRefund.RefundAmount = 2000;
            memberLoanRefund.Remarks = "Test remarks from service test";
            //var teamMember = _teamMemberService.LoadById(memberLoanRefund.TeamMemberId);

            var obj = new MemberLoanRefund
            {
                TeamMember = teamMember,
                SalaryOrganization = _organizationService.LoadById(memberLoanRefund.Organization.Id),
                SalaryBranch = _branchService.GetBranch(memberLoanRefund.Branch.Id),
                SalaryCampus = _campusService.GetCampus(memberLoanRefund.Campus.Id),
                RefundAmount = memberLoanRefund.RefundAmount,
                Remarks = memberLoanRefund.Remarks,
                ReceivedById=1,

            };
            _memberLoanRefundFactory.Create()
                                    .WithRefundAmount(memberLoanRefund.RefundAmount)
                                    .WithRemark(memberLoanRefund.Remarks)
                                    .WithSalaryBranch(memberLoanRefund.Branch)
                                    .WithSalaryCampus(memberLoanRefund.Campus)
                                    .WithSalaryOrganization(memberLoanRefund.Organization)
                                    .WithTeamMember(teamMember).WithReceivedBy(1);
            _memberLoanRefundService.SaveOrUpdate(userMenu, _memberLoanRefundFactory.Object);
            memberLoanRefund = new MemberLoanRefundViewModel();
            var loanRefund = _memberLoanRefundService.GetMemberLoanRefund(_memberLoanRefundFactory.Object.Id);

            #endregion

            Assert.NotNull(loanRefund);
        }
    }
}
