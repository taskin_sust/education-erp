﻿using System.Collections.Generic;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.LectureSettingTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "LectureSetting")]
    public class LectureSettingLoadByIdTest : LectureSettingBaseTest
    {
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _lectureSettingService.LoadById(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            var lectureSettingFactory =
               _lectureSettingFactory.Create().WithCourseSubject()
                   .Persist();
            LectureSettings lectureSettings = _lectureSettingService.LoadById(lectureSettingFactory.Object.Id);
            IList<Lecture> lectures = _lectureService.LoadLecture(lectureSettings.Id);
            Assert.NotNull(lectureSettings);
            _TestBaseService.DeleteProgramSessionSubject(lectureSettings.Course.Program.Id, lectureSettings.Course.RefSession.Id);
            _TestBaseService.DeleteLecture(lectures);
        }

    }
}
