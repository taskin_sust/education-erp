﻿using System;
using System.Collections.Generic;
using System.Linq;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface ISalaryHistoryDao : IBaseDao<SalaryHistory, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        SalaryHistory GetTeamMemberJoiningSalaryHistory(long teamMemberId);
        SalaryHistory GetTeamMemberLastSalaryHistory(long teamMemberId);
        SalaryHistory GetTeamMemberSalaryHistory(long teamMemberId, DateTime date);

        #endregion

        #region List Loading Function

        IList<SalaryHistory> LoadSalaryHistories(TeamMember member, DateTime? searchingDate);

        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion

        
    }
    public class SalaryHistoryDao : BaseDao<SalaryHistory, long>, ISalaryHistoryDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public SalaryHistory GetTeamMemberJoiningSalaryHistory(long teamMemberId)
        {
            return Session.QueryOver<SalaryHistory>().Where(x => x.TeamMember.Id == teamMemberId && x.Status == SalaryHistory.EntityStatus.Active).OrderBy(x => x.EffectiveDate).Asc.ThenBy(x => x.Id).Asc.List<SalaryHistory>().FirstOrDefault();
        }

        public SalaryHistory GetTeamMemberLastSalaryHistory(long teamMemberId)
        {
            return Session.QueryOver<SalaryHistory>().Where(x => x.TeamMember.Id == teamMemberId && x.Status == SalaryHistory.EntityStatus.Active).OrderBy(x => x.EffectiveDate).Desc.ThenBy(x => x.Id).Desc.List<SalaryHistory>().FirstOrDefault();
        }

        public SalaryHistory GetTeamMemberSalaryHistory(long teamMemberId, DateTime date)
        {
            return Session.QueryOver<SalaryHistory>().Where(x => x.TeamMember.Id == teamMemberId && x.Status == SalaryHistory.EntityStatus.Active && x.EffectiveDate <= date.Date).OrderBy(x => x.EffectiveDate).Desc.ThenBy(x => x.Salary).Desc.List<SalaryHistory>().FirstOrDefault();
        }

        #endregion

        #region List Loading Function
        
        public IList<SalaryHistory> LoadSalaryHistories(TeamMember member, DateTime? searchingDate)
        {
            if (searchingDate != null)
                return Session.QueryOver<SalaryHistory>().Where(x => x.TeamMember.Id == member.Id && x.Status == SalaryHistory.EntityStatus.Active && x.EffectiveDate <= searchingDate.Value.Date).OrderBy(x => x.EffectiveDate).Desc.List<SalaryHistory>();
            else
                return Session.QueryOver<SalaryHistory>().Where(x => x.TeamMember.Id == member.Id && x.Status == SalaryHistory.EntityStatus.Active).OrderBy(x => x.EffectiveDate).Desc.List<SalaryHistory>();
        }

        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion
    }

}
