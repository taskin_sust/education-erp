﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class ComplementaryCourseMap :BaseClassMap<ComplementaryCourse, long>
    {
        public ComplementaryCourseMap ()
        {
            Table("ComplementaryCourse");
            LazyLoad();
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.Course).Column("CourseId");
            References(x => x.CompCourse).Column("CompCourseId");


        }
    }
}
