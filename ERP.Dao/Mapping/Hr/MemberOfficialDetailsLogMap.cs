using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    public partial class MemberOfficialDetailLogMap : ClassMap<MemberOfficialDetailLog>
    {

        public MemberOfficialDetailLogMap()
        {
            Table("HR_MemberOfficialDetailLog");
            LazyLoad();
            //References(x => x.MemberOfficialDetail).Column("MemberOfficialDetailId");//old
            Map(x => x.CardNo).Column("CardNo");
            Map(x => x.OfficialContact).Column("OfficialContact");
            Map(x => x.OfficialEmail).Column("OfficialEmail");
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
            Map(x => x.MemberOfficialDetailId);
        }
    }
}
