﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.MediaDao;
using UdvashERP.Dao.Teachers;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;

namespace UdvashERP.Services.Administration
{
    public interface IOrganizationService : IBaseService
    {
        #region Operational Function
        bool SaveOrUpdate(Organization organizationObj);
        bool Delete(long id);
        bool UpdateRank(long id, int current, string action);
        bool IsOrganizationUpdateSuccessfully(List<Organization> list, Teacher teacher,
            IList<TeacherExtraCurricularActivity> extraCurricularActivities,
            IList<TeacherAdmissionInfo> teacherAdmissionInfos,
            IList<TeacherVersionOfStudyPriority> versionofStudyPriorities,
            IList<TeacherSubjectPriority> teacherSubjectPriorities,
            IList<TeacherActivityPriority> teacherActivityPriorities, TeacherImages teacherImages, byte[] imageFile);
        #endregion

        #region Single Instances Loading Function
        Organization LoadById(long id);
        Organization GetByBusinessId(string businessId);
        Organization GetActiveOrganization();
        Organization GetOrganization(string shortName);
        #endregion

        #region List Loading Function

        IList<Organization> LoadOrganization(int? status = null);
        IList<Organization> LoadAuthorizedOrganization(List<UserMenu> userMenus, bool eagerLoadOrgBranch = false);

        IList<Organization> LoadOrganization(int start, int length, string orderBy, string orderDir, string name, string shortName, string website, string status);
        #endregion

        #region Others Function
        int GetMinRank(Organization org);
        int GetMaxRank(Organization org);
        int GetOrganizationCount(string name, string shortName, string website, string status);

        bool IsTeacherUpdateSuccessfullyEditPurpose(List<Organization> toList, Teacher teacher, IList<TeacherExtraCurricularActivity> extraCurricularActivities,
         IList<TeacherAdmissionInfo> teacherAdmissionInfos, IList<TeacherVersionOfStudyPriority> versionofStudyPriorities, IList<TeacherSubjectPriority> teacherSubjectPriorities,
         IList<TeacherActivityPriority> teacherActivityPriorities, Teacher prevTeacherObj, TeacherImages teacherImages, byte[] imageFile);
        #endregion
    }
    public class OrganizationService : BaseService, IOrganizationService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IOrganizationDao _organizationDao;
        private readonly ITeacherExtraCurricularActivityDao _teacgExtraCurricularActivityDao;
        private readonly ITeacherAdmissionInfoDao _teacherAdmissionInfoDao;
        private readonly ITeacherVersionOfStudyPriorityDao _ofStudyPriorityDao;
        private readonly ITeacherSubjectPriorityDao _teacherSubjectPriorityDao;
        private readonly ITeacherActivityPriorityDao _teacherActivityPriorityDao;
        private readonly ITeacherImageDao _teacherImageDao;
        private readonly ITeacherDao _teacherDao;
        //private readonly ITeacherImageMediaService _teacherImageMediaService;
        private readonly ITeacherImageMediaDao _teacherImageMediaDao;

        public OrganizationService(ISession session, bool initialize = true)
        {
            Session = session;
            _organizationDao = new OrganizationDao() { Session = Session };
            _teacgExtraCurricularActivityDao = new TeacherExtraCurricularActivityDao() { Session = Session };
            _teacherAdmissionInfoDao = new TeacherAdmissionInfoDao() { Session = Session };
            _ofStudyPriorityDao = new TeacherVersionOfStudyPriorityDao() { Session = Session };
            _teacherSubjectPriorityDao = new TeacherSubjectPriorityDao() { Session = Session };
            _teacherActivityPriorityDao = new TeacherActivityPriorityDao() { Session = Session };
            _teacherImageDao = new TeacherImageDao() { Session = Session };
            _teacherDao = new TeacherDao() { Session = Session };


            var sessionMedia = NHibernateMediaSessionFactory.OpenSession();
            _teacherImageMediaDao = new TeacherImageMediaDao { Session = sessionMedia }; //(sessionMedia);

            //if (initialize)
            //    _teacherImageMediaService = new TeacherImageMediaService();
        }

        //public OrganizationService(ISession session, ISession mediaSession, bool initialize)
        //    : this(session, initialize)
        //{
        //    _teacherImageMediaService = new TeacherImageMediaService(mediaSession);
        //}
        #endregion

        #region Operational Function

        private void CheckDuplicateFields(Organization organizationObj, long id = 0)
        {
            var isDuplicateName = _organizationDao.CheckDuplicateFields(id, organizationObj.Name);
            if (isDuplicateName)
                throw new DuplicateEntryException("Name can not be duplicate");
            var isDuplicateShortName = _organizationDao.CheckDuplicateFields(id, shortName: organizationObj.ShortName);
            if (isDuplicateShortName)
                throw new DuplicateEntryException("Short name can not be duplicate");
        }

        private void CheckBeforeSave(Organization organizationObj)
        {
            CheckDuplicateFields(organizationObj);
            CheckNullEntry(organizationObj);
        }

        private void CheckNullEntry(Organization organizationObj)
        {
            if (string.IsNullOrEmpty(organizationObj.Name))
                throw new EmptyFieldException("Name is empty!");
            if (string.IsNullOrEmpty(organizationObj.ShortName))
                throw new EmptyFieldException("Short Name is empty!");
            if (string.IsNullOrEmpty(organizationObj.Website))
                throw new EmptyFieldException("Website is empty!");
            if (string.IsNullOrEmpty(organizationObj.Email))
                throw new EmptyFieldException("Email is empty!");
            if (string.IsNullOrEmpty(organizationObj.ContactNo))
                throw new EmptyFieldException("Contact number is empty!");
            if (string.IsNullOrEmpty(organizationObj.LogoImageUrl))
                throw new EmptyFieldException("Logo  url is empty!");
            if (string.IsNullOrEmpty(organizationObj.ReportImageUrl))
                throw new EmptyFieldException("Report image url is empty!");
            if (string.IsNullOrEmpty(organizationObj.IdFooterImageUrl))
                throw new EmptyFieldException("Footer image url is empty!");


        }

        public void ModelValidationCheck(Organization organizationObj)
        {
            var validationResult = ValidationHelper.ValidateEntity<Organization, Organization.OrganizationMetaData>(organizationObj);
            if (validationResult.HasError)
            {
                string errorMessage = "";
                validationResult.Errors.ForEach(r => errorMessage = errorMessage + r.ErrorMessage + Environment.NewLine);
                throw new InvalidDataException(errorMessage);
            }
        }

        public bool SaveOrUpdate(Organization organization)
        {
            ITransaction transaction = null;
            try
            {
                if (organization == null)
                {
                    throw new NullObjectException("organization can not be null");
                }

                ModelValidationCheck(organization);
                using (transaction = Session.BeginTransaction())
                {
                    if (organization.Id < 1)
                    {
                        CheckBeforeSave(organization);
                        organization.Rank = _organizationDao.GetMaximumRank(organization) + 1;
                        _organizationDao.Save(organization);
                    }
                    else
                    {
                        var oldOrganization = _organizationDao.LoadById(organization.Id);
                        //ModelValidationCheck(organization);
                        CheckDuplicateFields(organization, organization.Id);
                        CheckNullEntry(organization);

                        oldOrganization.Name = organization.Name;
                        oldOrganization.ShortName = organization.ShortName;
                        oldOrganization.Website = organization.Website;
                        oldOrganization.Email = organization.Email;
                        oldOrganization.ContactNo = organization.ContactNo;
                        oldOrganization.LogoImageUrl = organization.LogoImageUrl;
                        oldOrganization.ReportImageUrl = organization.ReportImageUrl;
                        oldOrganization.IdFooterImageUrl = organization.IdFooterImageUrl;
                        oldOrganization.SmsApiUserName = organization.SmsApiUserName;
                        oldOrganization.SmsApiPassword = organization.SmsApiPassword;
                        oldOrganization.HighPrioritySmsApiUserName = organization.HighPrioritySmsApiUserName;
                        oldOrganization.HighPrioritySmsApiPassword = organization.HighPrioritySmsApiPassword;
                        oldOrganization.BasicSalary = organization.BasicSalary;
                        oldOrganization.HouseRent = organization.HouseRent;
                        oldOrganization.MedicalAllowance = organization.MedicalAllowance;
                        oldOrganization.Convenyance = organization.Convenyance;
                        oldOrganization.MonthlyWorkingDay = organization.MonthlyWorkingDay;
                        oldOrganization.WorkingHour = organization.WorkingHour;
                        oldOrganization.DeductionPerAbsent = organization.DeductionPerAbsent;
                        oldOrganization.Status = organization.Status;
                        _organizationDao.Update(oldOrganization);
                        // trans.Commit();
                        //return true;


                    }
                    transaction.Commit();
                    return true;
                }
            }
            catch (DuplicateEntryException ex)
            {
                throw;
            }
            catch (InvalidDataException ex)
            {
                throw;
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (EmptyFieldException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        private void CheckBeforeDelete(Organization organizationObject)
        {
            if (organizationObject.Programs.Count > 0)
                throw new DependencyException("You can't delete this organization, program is already assigned.");
            if (organizationObject.Branches.Count > 0)
                throw new DependencyException("You can't delete this organization, branch is already assigned.");
            if (organizationObject.Teachers.Count > 0)
                throw new DependencyException("You can't delete this organization, teacher is already assigned.");
            if (organizationObject.ExtraCurricularActivities.Count > 0)
                throw new DependencyException("You can't delete this organization, extra curricular activities is already assigned.");
            if (organizationObject.SmsMasks.Count > 0)
                throw new DependencyException("You can't delete this organization, sms mask is already assigned.");
            if (organizationObject.YearlyHolidays.Count > 0)
                throw new DependencyException("You can't delete this organization, yearly holiday is already assigned.");
            if (organizationObject.MemberLeaveSummaries.Count > 0)
                throw new DependencyException("You can't delete this organization, member leave summary is already assigned.");
        }
        public bool Delete(long id)
        {
            ITransaction transaction = null;
            try
            {
                var organizationObj = _organizationDao.LoadById(id);
                if (organizationObj == null)
                {
                    throw new NullObjectException("organization is null");
                }
                CheckBeforeDelete(organizationObj);
                using (transaction = Session.BeginTransaction())
                {
                    organizationObj.Status = BusinessModel.Entity.Administration.Organization.EntityStatus.Delete;
                    _organizationDao.Update(organizationObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (NullObjectException ex)
            {
                throw;
            }
            catch (DependencyException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                throw ex;
            }
        }

        public bool UpdateRank(long id, int current, string action)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    Organization orgUpdateObj = _organizationDao.LoadById(Convert.ToInt64(id));
                    if (orgUpdateObj != null)
                    {
                        int newRank;
                        if (action == RankOperation.Up)
                            newRank = orgUpdateObj.Rank - 1;
                        else
                            newRank = orgUpdateObj.Rank + 1;
                        var orgOldObj = _organizationDao.LoadByRankDirection(newRank, action);//LoadByRank(newRank);
                        newRank = orgOldObj.Rank;
                        orgOldObj.Rank = orgUpdateObj.Rank;
                        orgUpdateObj.Rank = newRank;

                        _organizationDao.Update(orgOldObj);
                        _organizationDao.Update(orgUpdateObj);
                        transaction.Commit();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                return false;
            }
        }

        public bool IsOrganizationUpdateSuccessfully(List<Organization> list, Teacher teacher,
            IList<TeacherExtraCurricularActivity> extraCurricularActivities,
            IList<TeacherAdmissionInfo> teacherAdmissionInfos,
            IList<TeacherVersionOfStudyPriority> versionofStudyPriorities,
            IList<TeacherSubjectPriority> teacherSubjectPriorities,
            IList<TeacherActivityPriority> teacherActivityPriorities,
            TeacherImages teacherImages, byte[] imageFile)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        IList<TeacherImages> teacherImageses = new List<TeacherImages>();
                        if (teacherImages != null)
                        {
                            teacherImageses.Add(teacherImages);
                            foreach (var timg in teacherImageses)
                            {
                                timg.Teacher = teacher;
                            }
                            teacher.TeacherImageses = teacherImageses;
                        }
                        else
                        {
                            teacher.TeacherImageses = new List<TeacherImages>();
                        }
                        foreach (var organization in list)
                        {
                            var proxyOrgObj = _organizationDao.LoadById(organization.Id);
                            proxyOrgObj.Teachers.Add(teacher);
                            teacher.Organizations.Add(organization);
                            proxyOrgObj.ModificationDate = DateTime.Now;
                            _organizationDao.Update(proxyOrgObj);
                        }
                        //image save to media db 
                        if (imageFile != null && imageFile.Length > 0)
                        {
                            TeacherMediaImage mediaImage = new TeacherMediaImage();
                            mediaImage.Hash = teacherImages.Hash;
                            mediaImage.ImageGuid = Guid.NewGuid();
                            mediaImage.Images = imageFile;
                            //_teacherImageMediaService.Save(mediaImage);
                            _teacherImageMediaDao.Save(mediaImage);
                        }

                        //end
                        if (extraCurricularActivities.Any())
                        {
                            foreach (var extraCarricularActivity in extraCurricularActivities)
                            {
                                extraCarricularActivity.CreationDate = DateTime.Now;
                                extraCarricularActivity.ModificationDate = DateTime.Now;
                                _teacgExtraCurricularActivityDao.Save(extraCarricularActivity);
                            }
                        }

                        if (teacherAdmissionInfos.Any())
                        {
                            foreach (var teacherAdmissionInfo in teacherAdmissionInfos)
                            {
                                teacherAdmissionInfo.CreationDate = DateTime.Now;
                                teacherAdmissionInfo.ModificationDate = DateTime.Now;
                                teacherAdmissionInfo.Teacher = teacher;
                                _teacherAdmissionInfoDao.Save(teacherAdmissionInfo);
                            }
                        }

                        foreach (var versionofStudyPriority in versionofStudyPriorities)
                        {
                            versionofStudyPriority.CreationDate = DateTime.Now;
                            versionofStudyPriority.ModificationDate = DateTime.Now;
                            _ofStudyPriorityDao.Save(versionofStudyPriority);
                        }

                        foreach (var teacherSubjectPriority in teacherSubjectPriorities)
                        {
                            teacherSubjectPriority.CreationDate = DateTime.Now;
                            teacherSubjectPriority.ModificationDate = DateTime.Now;
                            _teacherSubjectPriorityDao.Save(teacherSubjectPriority);
                        }

                        foreach (var teacherActivityPriority in teacherActivityPriorities)
                        {
                            teacherActivityPriority.CreationDate = DateTime.Now;
                            teacherActivityPriority.ModificationDate = DateTime.Now;
                            _teacherActivityPriorityDao.Save(teacherActivityPriority);
                        }
                        trans.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        trans.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public bool IsTeacherUpdateSuccessfullyEditPurpose(List<Organization> list, Teacher newteacherObj, IList<TeacherExtraCurricularActivity> extraCurricularActivities,
        IList<TeacherAdmissionInfo> teacherAdmissionInfos, IList<TeacherVersionOfStudyPriority> versionofStudyPriorities, IList<TeacherSubjectPriority> teacherSubjectPriorities,
        IList<TeacherActivityPriority> teacherActivityPriorities, Teacher prevTeacherObj, TeacherImages teacherImages, byte[] imageFile)
        {
            try
            {
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        foreach (Organization organization in prevTeacherObj.Organizations)
                        {
                            organization.Teachers.Remove(prevTeacherObj);
                        }
                        prevTeacherObj.Organizations.Clear();
                        prevTeacherObj.TeacherExtraCurricularActivities.Clear();
                        prevTeacherObj.TeacherAdmissionInfos.Clear();
                        prevTeacherObj.TeacherVersionOfStudyPriorities.Clear();
                        prevTeacherObj.TeacherSubjectPriorities.Clear();
                        prevTeacherObj.TeacherActivityPriorities.Clear();
                        prevTeacherObj.TeacherImageses.Clear();
                        prevTeacherObj.FullName = newteacherObj.FullName;
                        prevTeacherObj.NickName = newteacherObj.NickName;
                        prevTeacherObj.PersonalMobile = newteacherObj.PersonalMobile;
                        prevTeacherObj.AlternativeMobile = newteacherObj.AlternativeMobile;
                        prevTeacherObj.Institute = newteacherObj.Institute;
                        prevTeacherObj.Department = newteacherObj.Department;
                        prevTeacherObj.HscPassingYear = newteacherObj.HscPassingYear;
                        prevTeacherObj.Religion = newteacherObj.Religion;
                        prevTeacherObj.Gender = newteacherObj.Gender;
                        prevTeacherObj.FatherName = newteacherObj.FatherName;
                        prevTeacherObj.DateOfBirth = newteacherObj.DateOfBirth;
                        prevTeacherObj.BloodGroup = newteacherObj.BloodGroup;
                        prevTeacherObj.RoomMatesMobile = newteacherObj.RoomMatesMobile;
                        prevTeacherObj.FathersMobile = newteacherObj.FathersMobile;
                        prevTeacherObj.MothersMobile = newteacherObj.MothersMobile;
                        prevTeacherObj.Email = newteacherObj.Email;
                        prevTeacherObj.FbId = newteacherObj.FbId;
                        prevTeacherObj.PresentAddress = newteacherObj.PresentAddress;
                        prevTeacherObj.Area = newteacherObj.Area;
                        prevTeacherObj.PermanentAddress = newteacherObj.PermanentAddress;
                        prevTeacherObj.District = newteacherObj.District;

                        prevTeacherObj.TeacherImageses = new List<TeacherImages>();
                        IList<TeacherImages> teacherImageses = new List<TeacherImages>();
                        if (teacherImages != null)
                        {
                            teacherImageses.Add(teacherImages);
                            foreach (var timg in teacherImageses)
                            {
                                timg.Teacher = prevTeacherObj;
                            }
                            prevTeacherObj.TeacherImageses = teacherImageses;
                        }
                        else
                        {
                            prevTeacherObj.TeacherImageses = new List<TeacherImages>(); ;
                        }
                        foreach (var organization in list)
                        {
                            var proxyOrgObj = _organizationDao.LoadById(organization.Id);
                            proxyOrgObj.Teachers.Add(prevTeacherObj);
                            proxyOrgObj.ModificationDate = DateTime.Now;
                            _organizationDao.Update(proxyOrgObj);
                        }

                        //Image save to media db 
                        if (imageFile != null && imageFile.Length > 0)
                        {
                            TeacherMediaImage mediaImage = new TeacherMediaImage();
                            if (teacherImages != null) mediaImage.Hash = teacherImages.Hash;
                            mediaImage.ImageGuid = Guid.NewGuid();
                            mediaImage.Images = imageFile;
                            //_teacherImageMediaService.Save(mediaImage);
                            _teacherImageMediaDao.Save(mediaImage);
                        }

                        if (extraCurricularActivities.Any())
                        {
                            foreach (var extraCarricularActivity in extraCurricularActivities)
                            {
                                extraCarricularActivity.CreationDate = DateTime.Now;
                                extraCarricularActivity.ModificationDate = DateTime.Now;
                                extraCarricularActivity.Teacher = prevTeacherObj;
                                extraCarricularActivity.Status = TeacherExtraCurricularActivity.EntityStatus.Active;
                                if (prevTeacherObj.TeacherExtraCurricularActivities != null)
                                    prevTeacherObj.TeacherExtraCurricularActivities.Add(extraCarricularActivity);
                                _teacgExtraCurricularActivityDao.SaveOrUpdate(extraCarricularActivity);
                            }
                        }

                        if (teacherAdmissionInfos.Any())
                        {
                            foreach (var teacherAdmissionInfo in teacherAdmissionInfos)
                            {
                                teacherAdmissionInfo.CreationDate = DateTime.Now;
                                teacherAdmissionInfo.ModificationDate = DateTime.Now;
                                teacherAdmissionInfo.Status = TeacherAdmissionInfo.EntityStatus.Active;
                                teacherAdmissionInfo.Teacher = prevTeacherObj;
                                if (prevTeacherObj.TeacherAdmissionInfos != null)
                                    prevTeacherObj.TeacherAdmissionInfos.Add(teacherAdmissionInfo);
                                _teacherAdmissionInfoDao.SaveOrUpdate(teacherAdmissionInfo);
                            }
                        }

                        foreach (var versionofStudyPriority in versionofStudyPriorities)
                        {
                            versionofStudyPriority.CreationDate = DateTime.Now;
                            versionofStudyPriority.ModificationDate = DateTime.Now;
                            versionofStudyPriority.Status = TeacherVersionOfStudyPriority.EntityStatus.Active;
                            versionofStudyPriority.Teacher = prevTeacherObj;
                            if (prevTeacherObj.TeacherVersionOfStudyPriorities != null)
                                prevTeacherObj.TeacherVersionOfStudyPriorities.Add(versionofStudyPriority);
                            _ofStudyPriorityDao.SaveOrUpdate(versionofStudyPriority);
                        }

                        foreach (var teacherSubjectPriority in teacherSubjectPriorities)
                        {
                            teacherSubjectPriority.CreationDate = DateTime.Now;
                            teacherSubjectPriority.ModificationDate = DateTime.Now;
                            teacherSubjectPriority.Status = TeacherSubjectPriority.EntityStatus.Active;
                            teacherSubjectPriority.Teacher = prevTeacherObj;
                            if (prevTeacherObj.TeacherSubjectPriorities != null)
                                prevTeacherObj.TeacherSubjectPriorities.Add(teacherSubjectPriority);
                            _teacherSubjectPriorityDao.SaveOrUpdate(teacherSubjectPriority);
                        }

                        foreach (var teacherActivityPriority in teacherActivityPriorities)
                        {
                            teacherActivityPriority.CreationDate = DateTime.Now;
                            teacherActivityPriority.ModificationDate = DateTime.Now;
                            teacherActivityPriority.Status = TeacherActivityPriority.EntityStatus.Active;
                            teacherActivityPriority.Teacher = prevTeacherObj;
                            if (prevTeacherObj.TeacherActivityPriorities != null)
                                prevTeacherObj.TeacherActivityPriorities.Add(teacherActivityPriority);
                            _teacherActivityPriorityDao.SaveOrUpdate(teacherActivityPriority);
                        }
                        trans.Commit();
                        return true;

                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        trans.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region Single Instances Loading Function

        public Organization LoadById(long id)
        {
            try
            {
                return _organizationDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public Organization GetByBusinessId(string businessId)
        {
            try
            {
                return _organizationDao.GetByBusinessId(businessId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public Organization GetActiveOrganization()
        {
            try
            {
                return _organizationDao.GetActiveOrganization();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return null;
            }
        }

        public Organization GetOrganization(string shortName)
        {
            try
            {
                return _organizationDao.GetOrganization(shortName);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return null;
            }
        }

        #endregion

        #region List Loading Function
        public IList<Organization> LoadOrganization(int? status = null)
        {
            try
            {
                return _organizationDao.LoadAll(status).OrderBy(x => x.Rank).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Organization> LoadAuthorizedOrganization(List<UserMenu> userMenus, bool eagerLoadOrgBranch = false)
        {
            try
            {
                List<long> organizationIdList = AuthHelper.LoadOrganizationIdList(userMenus);
                return _organizationDao.LoadAuthorizedOrganization(organizationIdList, eagerLoadOrgBranch);
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<Organization> LoadOrganization(int start, int length, string orderBy, string orderDir, string name, string shortName, string website, string status)
        {
            try
            {
                return _organizationDao.LoadOrganization(start, length, orderBy, orderDir, name, shortName, website, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion

        #region Others Function
        public int GetMaxRank(Organization org)
        {
            try
            {
                return _organizationDao.GetMaximumRank(org);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        public int GetMinRank(Organization org)
        {
            try
            {
                return _organizationDao.GetMinimumRank(org);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public int GetOrganizationCount(string name, string shortName, string website, string status)
        {
            try
            {
                return _organizationDao.GetOrganizationCount(name, shortName, website, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }
        #endregion
    }
}
