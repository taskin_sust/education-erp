﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNet.Identity;
using Org.BouncyCastle.Pkcs;
using UdvashERP.App_code;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User Email")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    //Need for UserName (Identity)
    public static class GenericPrincipalExtensions
    {
        public static string FullName(this IPrincipal user)
        {
            string fullName = user.Identity.Name;
            if (user.Identity.IsAuthenticated)
            {
                var claimsIdentity = user.Identity as ClaimsIdentity;
                var userNameClaim = claimsIdentity.Claims.FirstOrDefault(c => c.Type == "FullName");
                if (userNameClaim != null)
                    fullName = userNameClaim.Value;
            }
            return fullName;
        }

        public static byte[] Image(this IPrincipal user)
        {
            var userId = user.Identity.GetUserId();
            IUserService userService = new UserService(NHibernateSessionFactory.OpenSession());
            IEmployeeImageMediaService employeeImageMediaService = new EmployeeImageMediaService();
            UserProfile userprofile = userService.LoadById(Convert.ToInt64(userId));
            if (userprofile != null && userprofile.UserImages.Count > 0)
            {
                EmployeeMediaImage mediaEmployeeImages = employeeImageMediaService.GetEmployeeImageMediaByRefId(userprofile.UserImages[0].Id);
                if (mediaEmployeeImages != null)
                {
                    return mediaEmployeeImages.Images;
                }
                return null;
            }
            return null;
        }
        
    }
}
