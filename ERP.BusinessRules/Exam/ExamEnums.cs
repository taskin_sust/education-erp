﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.BusinessRules.Exam
{
    public enum ExamVersion
    {
        All = 0,
        Bangla = 1,
        English = 2,
    }

    public enum PrintStatus
    {
        [Description("All")]
        All = -1,
        [Description("Requested")]
        NotPrinted = 0,
        [Description("Printed")]
        Printed = 1,
    }
}
