using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {


    public partial class SalaryHistoryLogMap : ClassMap<SalaryHistoryLog>
    {
        
        public SalaryHistoryLogMap() {
			Table("HR_SalaryHistoryLog");
			LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
            Map(x => x.OrganizationId).Column("OrganizationId");
            Map(x => x.DepartmentId).Column("DepartmentId");
            Map(x => x.DesignationId).Column("DesignationId");
            Map(x => x.CampusId).Column("CampusId");
            Map(x => x.TeamMemberId).Column("TeamMemberId");
            Map(x => x.SalaryHistoryId).Column("SalaryHistoryId");
            Map(x => x.SalaryPurpose).Column("SalaryPurpose");
			Map(x => x.Salary).Column("Salary");
			Map(x => x.BankAmount).Column("BankAmount");
			Map(x => x.CashAmount).Column("CashAmount");
			Map(x => x.EffectiveDate).Column("EffectiveDate");
            Map(x => x.Status).Column("Status");
        }
    }
}
