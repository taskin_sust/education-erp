﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using NHibernate.Mapping;
using UdvashERP.App_code;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class MenuAccessController : Controller
    {
        #region Objects/Propertise/Services/Dao & Initialization
        private ILog logger = LogManager.GetLogger("AdministrationArea");
        private readonly ICommonHelper _commonHelper;
        private readonly IMenuService _menuService;
        private readonly IAreaControllersService _areacontrollersService;
        private readonly IActionsService _actionsService;
        private readonly IMenuAccessService _menuAccessService;
        public MenuAccessController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _menuService = new MenuService(session);
                _actionsService = new ActionsService(session);
                _areacontrollersService = new AreaControllersService(session);
                _commonHelper = new CommonHelper();
                _menuAccessService = new MenuAccessService(session);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
            }
        }
        #endregion

        #region Index/Manage
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Save Operation
        public ActionResult Create()
        {
            ViewBag.menuGroupList = new SelectList(_menuService.LoadAllMenuGroup(), "Id", "Name");
            ViewBag.controllersList = new SelectList(string.Empty, "Id", "Name");
            ViewBag.areaList = new SelectList(_areacontrollersService.LoadArea(), "Area", "Area");
            ViewBag.allMenuList = new SelectList(string.Empty, "Id", "Name");  
            return View();
        }
        [HttpPost]
        public ActionResult Create(MenuAccessViewModel menuAccessViewModel)
        {
            try
            {
                foreach (var value in menuAccessViewModel.MenuAccessListViewModels)
                {
                    Menu menu = _menuService.MenuLoadById(value.MaMenuId);
                    AreaControllers areaController = _areacontrollersService.LoadById(value.MaControllersId);
                    Actions action = _actionsService.LoadById(value.MaActionsId);
                    bool checkedAccess = _menuAccessService.CheckedMenuAccess(menu, areaController, action);
                    //MenuAccess menuAccessModel = _menuAccessService.LoadById(value.MaId);
                    MenuAccess menuAccess = new MenuAccess();
                    if (checkedAccess && value.MaCheckedValue==false)
                    {
                        menuAccess.Id = value.MaId;
                        menuAccess.Menu = menu;
                        menuAccess.HasReferrer = false;
                        menuAccess.Actions = action;
                        _menuAccessService.Delete(menuAccess);
                    }
                    else if(checkedAccess==false && value.MaCheckedValue)
                    {
                        menuAccess.Menu = menu;
                        menuAccess.HasReferrer = value.CheckedHasReferrer;
                        menuAccess.Actions = action;
                        _menuAccessService.Save(menuAccess);
                    }
                    else if (checkedAccess && value.MaCheckedValue)
                    {
                        menuAccess.Menu = menu;
                        menuAccess.HasReferrer = value.CheckedHasReferrer;
                        menuAccess.Actions = action;
                        _menuAccessService.Update(value.MaId, menuAccess);
                    }
                }
                var menuGroupList = new SelectList(_menuService.LoadAllMenuGroup(), "Id", "Name", menuAccessViewModel.MenuGroupId);
                List<SelectListItem> mGList = menuGroupList.ToList();
                ViewBag.menuGroupList = mGList;
                
                IList<Menu> menuList = _menuService.LoadByMenuGroupId(Convert.ToInt32(menuAccessViewModel.MenuGroupId));
                var finalList = new List<Menu>();
                foreach (var menu in menuList)
                {
                    finalList.AddRange(MenuDisplay(menu, "- "));
                }
                var allMenuLists = new SelectList(finalList, "Id", "VariableNames", menuAccessViewModel.MenuId);
                List<SelectListItem> mlist = allMenuLists.ToList();
                ViewBag.allMenuList = mlist;
                
                var aList = new SelectList(_areacontrollersService.LoadArea(), "Area", "Area",menuAccessViewModel.Area);
                List < SelectListItem > areaList = aList.ToList();
                ViewBag.areaList = areaList;

                ViewBag.controllersList = new SelectList(_areacontrollersService.LoadControllersByArea(menuAccessViewModel.Area), "Id", "Name");
                
                TempData["MenuAccessSuccessMessage"] = "Menu Access successfully updated.";
                
            }
            catch (Exception ex)
            {
                TempData["MenuAccessErrorMessage"] = WebHelper.CommonErrorMessage;
                logger.Error(ex);
                return View();
            }
            return View();
        }
        #endregion

        #region Helper Function
        public List<Menu> MenuDisplay(Menu menu, string prefix)
        {
            List<Menu> chieldren = new List<Menu>();
            chieldren.Add(menu);

            if (menu.MenuSelfMenu == null)
            {
                menu.VariableNames = menu.Name;
            }
            else
            {
                menu.VariableNames = prefix + menu.Name;
            }


            if (menu.Menus != null)
            {
                prefix = prefix + prefix;

                foreach (Menu child in menu.Menus)
                {
                    var m = MenuDisplay(child, prefix);
                    chieldren.AddRange(m);
                }
            }

            return chieldren;
        }

        public ActionResult GetControllersForDropDownList(string area)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (area != null)
                    {
                        SelectList controllersList = new SelectList(_areacontrollersService.LoadControllersByArea(area), "Id", "Name");
                        List<SelectListItem> _list = controllersList.ToList();
                        _list.Insert(0, new SelectListItem() { Value = "", Text = "Select Controller" });
                        ViewBag.ControllersList = new SelectList(_list, "Value", "Text");
                    }
                    return PartialView("Partial/_ControllersList");
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return null; }
        }

        public ActionResult GetMenuForDropDownList(int menuGroupId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (menuGroupId > 0)
                    {
                        IList<Menu> menuList = _menuService.LoadByMenuGroupId(menuGroupId);
                        var finalList = new List<Menu>();
                        foreach (var menu in menuList)
                        {
                            finalList.AddRange(MenuDisplay(menu, "- "));
                        }
                        var allMenuLists = new SelectList(finalList, "Id", "VariableNames");
                        List<SelectListItem> _list = allMenuLists.ToList();
                        _list.Insert(0, new SelectListItem() {Value = "", Text = "Select Menu"});
                        ViewBag.allMenuListByGroupId = new SelectList(_list, "Value", "Text");
                        ViewBag.CountAllMenuListByGroupId = allMenuLists.Count();
                    }
                    else
                    {
                        ViewBag.CountAllMenuListByGroupId = 0;
                    }
                    return PartialView("Partial/_MenuList");
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return null; }
        }

        public ActionResult GetActionsByControllers(long? controllersId, long menuId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var menu = _menuService.MenuLoadById(menuId);
                    var menuList = _menuAccessService.LoadActiveByMenuId(menuId);
                    if (controllersId != null && controllersId > 0)
                    {
                        var actionsList = _actionsService.LoadActiveByControllers(controllersId.Value);
                        foreach (Actions actions in actionsList)
                        {
                            bool isMenuList = _menuAccessService.CheckedMenuAccess(menu, actions.AreaControllers, actions);
                            if (isMenuList == false)
                            {
                                var ma = new MenuAccess()
                                {
                                    Actions = actions,
                                    Menu = menu
                                };
                                menuList.Add(ma);
                            }
                        }
                    }
                    
                    ViewBag.countMenuAccessList = menuList.Count;
                    ViewBag.MenuAccessList = menuList;
                    
                    return PartialView("Partial/_MenuAccessList");
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else { return null; }
        }

        public ActionResult GetMenuAccessData(long menu, long? controllerId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var menuModel = _menuService.MenuLoadById(menu);
                    var menuList = _menuAccessService.LoadActiveByMenuId(menu);
                    if (controllerId != null && controllerId > 0)
                    {
                        var actionsList = _actionsService.LoadActiveByControllers(controllerId.Value);
                        foreach (Actions actions in actionsList)
                        {
                            bool isMenuList = _menuAccessService.CheckedMenuAccess(menuModel, actions.AreaControllers, actions);
                            if (isMenuList == false)
                            {
                                var ma = new MenuAccess()
                                {
                                    Actions = actions,
                                    Menu = menuModel
                                };
                                menuList.Add(ma);
                            }
                        }
                    }
                    ViewBag.countMenuAccessList = menuList.Count;
                    ViewBag.MenuAccessList = menuList;
                    return PartialView("Partial/_MenuAccessList");
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            else{ return null;}
        }
        #endregion
    }
}