﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.UInventory;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.ItemGroupTest
{
    interface IItemGroupLoadItemGroupCountTest
    {
        #region Basic Test
        void Should_Return_Load_itemGroup_Count();
        void should_Return_Load_ItemGroup_ParameterWize();
        void Should_Return_ItemGroup_Empty_Value();
        #endregion
    }

    [Trait("Area", "UInventory")]
    [Trait("Service", "ItemGroup")]
    public class ItemGroupLoadItemGroupCountTest : ItemGroupBaseTest, IItemGroupLoadItemGroupCountTest
    {
        #region Basic Test
        [Fact]
        public void Should_Return_Load_itemGroup_Count()
        {
            const int numOfItemGroup = 10;
            var itemGroupFactory = _itemGroupFactory.CreateMore(numOfItemGroup).Persist();
            //var list = _itemGroupService.LoadItemGroupCount(null,null,null,null);
            Assert.StrictEqual(itemGroupFactory.ObjectList.Count, numOfItemGroup);
        }

        [Fact]
        public void should_Return_Load_ItemGroup_ParameterWize()
        {
            const int numOfItemGroup = 10;
            var itemGroupFactory = _itemGroupFactory.CreateMore(numOfItemGroup).Persist();
            var list = _itemGroupService.LoadItemGroupCount(null, null, itemGroupFactory.ObjectList.Select(x=>x.Name).FirstOrDefault(), null);
            Assert.StrictEqual(list, 1);
        }

        [Fact]
        public void Should_Return_ItemGroup_Empty_Value()
        {
            const int numOfItemGroup = 10;
            var itemGroupFactory = _itemGroupFactory.CreateMore(numOfItemGroup).Persist();
            var list = _itemGroupService.LoadItemGroupCount(null, null, itemGroupFactory.ObjectList.Select(x => x.Name).FirstOrDefault()+"TEST", null);
            Assert.StrictEqual(list, 0);
        }
        #endregion
    }
}
