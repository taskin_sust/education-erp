using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.UInventory;

namespace UdvashERP.Services.UInventory
{
    public interface IItemGroupService : IBaseService
    {

        #region Operational Function

        void SaveOrUpdate(ItemGroup itemGroup);

        void IsDelete(long id);

        #endregion

        #region Single Instances Loading Function

        ItemGroup LoadById(long id);

        #endregion

        #region List Loading Function

        IList<ItemGroup> LoadItemGroup(int start, int length, string orderBy, string orderDir, string name,string status);
        IList<ItemGroup> LoadItemGroup(List<long> itemGroupIdList = null);

        #endregion

        #region Others Function

        int LoadItemGroupCount(string orderBy, string orderDir, string name, string status);

        #endregion

        #region Helper Function
        #endregion

    }

    public class ItemGroupService : BaseService, IItemGroupService
    {

        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IItemGroupDao _itemGroupDao;
        private readonly ItemDao _itemDao;

        public ItemGroupService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _itemGroupDao = new ItemGroupDao {Session = session};
            _itemDao = new ItemDao() {Session = session};
        }

        #endregion

        #region Operational Functions

        public void SaveOrUpdate(ItemGroup itemGroup)
        {
            ITransaction transaction = null;
            if (itemGroup == null)
            {
                throw new NullObjectException("object can not be null");
            }
            try
            {
                CheckBeforeSaveOrUpdate(itemGroup);
                using (transaction = Session.BeginTransaction())
                {
                    ItemGroup obj = itemGroup;
                    if (itemGroup.Id == 0)
                    {
                        obj.Status = ItemGroup.EntityStatus.Active;
                        obj.Rank = _itemGroupDao.GetMaximumRank(itemGroup) + 1;
                    }
                    else
                    {
                        obj = _itemGroupDao.LoadById(itemGroup.Id);
                        obj.Name = itemGroup.Name;
                        obj.Status = itemGroup.Status;
                    }
                    _itemGroupDao.SaveOrUpdate(obj);
                    transaction.Commit();
                    
                }
            }
            catch (MessageException)
            {
                throw;
            }
            catch (EmptyFieldException)
            {
                throw;
            }
            catch (DuplicateEntryException)
            {
                throw;
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            finally
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }

        public void IsDelete(long id)
        {
            ITransaction trans = null;
            try
            {
                var obj = _itemGroupDao.LoadById(id);
                if (obj == null)
                    throw new NullObjectException("item group is not valid");

                CheckBeforeDelete(obj);
                using (trans = Session.BeginTransaction())
                {
                    obj.Status = ItemGroup.EntityStatus.Delete;
                    _itemGroupDao.Update(obj);
                    trans.Commit();
                }
            }
            catch (DependencyException){throw;}

            catch (NullObjectException){throw;}

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        #endregion

        #region Single Instances Loading Function

        public ItemGroup LoadById(long id)
        {
            try
            {
                return _itemGroupDao.LoadById(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region List Loading Function

        public IList<ItemGroup> LoadItemGroup(int start, int length, string orderBy, string orderDir, string name,string status)
        {
            try
            {
                return _itemGroupDao.LoadItemGroup(start, length, orderBy, orderDir, name, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<ItemGroup> LoadItemGroup(List<long> itemGroupIdList = null)
        {
            try
            {
                IList<ItemGroup> itemGroups = _itemGroupDao.LoadItemGroup(itemGroupIdList);
                return itemGroups;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function

        public int LoadItemGroupCount(string orderBy, string orderDir, string name, string status)
        {
            try
            {
                return _itemGroupDao.LoadItemGroupCount(orderBy, orderDir, name, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Helper function

        private void CheckBeforeSaveOrUpdate(ItemGroup itemGroup)
        {
            string fieldName;
            CustomModelValidationCheck.DataAnnotationCheck(itemGroup, new ItemGroup.ItemGroupMetaData());
            var checkDuplicateName = _itemGroupDao.IsDuplicateItemGroup(out fieldName, itemGroup.Name, itemGroup.Id);
            if (checkDuplicateName)
            {
                if (fieldName == "Name")
                    throw new DuplicateEntryException("Duplicate item group found!!");
            }
        }

        private void CheckBeforeDelete(ItemGroup itemGroup)
        {
            int count = _itemDao.HasManyItemObjects(itemGroup.Id);
            if (count > 0)
                throw new DependencyException("You can't delete this Item Group, item is already declared here.");
        }

        #endregion

    }
}
                                    
