﻿using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.UserAuth
{
    public interface IActionsDao : IBaseDao<Actions, long>
    {
        #region Single Instances Loading Function
        Actions LoadByActionsNameAndController(string action, AreaControllers areaController);
        Actions LoadByRank(int rank);
        #endregion

        #region List Loading Function
        IList<Actions> LoadActive(bool eagerLoadController = false);
        IList<Actions> LoadActive(int start, int length, string orderBy, string orderDir, string name, string controllersId, string status, string rank);

        IList<Actions> LoadLike(string name);
        IList<Actions> LoadActiveByControllers(long controllersId);
        #endregion

        #region Others Function
        bool HasDuplicateByName(string name, AreaControllers areaControllers);
        bool HasDuplicateByName(string name, AreaControllers areaControllers, long id);
        int ActionsRowCount(string name, string controllersId, string status, string rank);
        #endregion

        //no need 
        //new IList<Actions> LoadByName(string name);
    }
    public class ActionsDao:BaseDao<Actions,long>,IActionsDao
    {
        #region Single Instances Loading Function
        public Actions LoadByActionsNameAndController(string action, AreaControllers areaController)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(Actions));
            criteria.Add(Restrictions.Eq("Status", Actions.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Name", action));
            criteria.CreateAlias("AreaControllers", "ac").Add(Restrictions.Eq("ac.Id", areaController.Id));
            return criteria.UniqueResult<Actions>();
        }
        public Actions LoadByRank(int rank)
        {
            return Session.QueryOver<Actions>().Where(x => x.Rank == rank && x.Status == Actions.EntityStatus.Active).SingleOrDefault<Actions>();
        }
        #endregion

        #region List Loading Function
        public IList<Actions> LoadLike(string name)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(Actions));
            criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            criteria.Add(Expression.Eq("Status", Actions.EntityStatus.Active));
            return criteria.List<Actions>();
        }
        public IList<Actions> LoadActive(int start, int length, string orderBy, string orderDir, string name, string controllersId, string status, string rank)
        {
            ICriteria criteria =
                    Session.CreateCriteria<Actions>()
                        .Add(Restrictions.Not(Restrictions.Eq("Status", Actions.EntityStatus.Delete)));


            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }

            if (!String.IsNullOrEmpty(controllersId))
            {
                criteria.Add(Restrictions.Eq("AreaControllers.Id", Convert.ToInt64(controllersId)));
            }

            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));

            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }
            if (!string.IsNullOrEmpty(orderBy.Trim()))
            {
                if (orderDir == "ASC")
                {
                    criteria.AddOrder(Order.Asc(orderBy));
                }
                else
                {
                    criteria.AddOrder(Order.Desc(orderBy));
                } 
            }
            return (List<Actions>)criteria.SetFirstResult(start).SetMaxResults(length).List<Actions>();

        }
        public IList<Actions> LoadActive(bool eagerLoadController = false)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(Actions));
            criteria.Add(Expression.Eq("Status", Actions.EntityStatus.Active));
            if(eagerLoadController)
                criteria.SetFetchMode("AreaControllers", FetchMode.Eager);

            return criteria.List<Actions>();
        }
        public IList<Actions> LoadActiveByControllers(long controllersId)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(Actions));
            criteria.Add(Expression.Eq("Status", Actions.EntityStatus.Active));
            criteria.Add(Expression.Eq("AreaControllers.Id", controllersId));
            return criteria.List<Actions>();
        }
        #endregion

        #region Others Function
        public int ActionsRowCount(string name, string controllersId, string status, string rank)
        {
            ICriteria criteria = Session.CreateCriteria<Actions>();

            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(controllersId))
            {
                criteria.Add(Restrictions.Eq("AreaControllers.Id", Convert.ToInt64(controllersId)));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }

            IList list = criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Actions.EntityStatus.Delete))).List();
            return list.Count;
        }
        public bool HasDuplicateByName(string name, AreaControllers areaControllers)
        {
            var rowList =
                Session.QueryOver<Actions>()
                    .Where(x => x.Name == name && x.AreaControllers == areaControllers && x.Status == Actions.EntityStatus.Active)
                    .RowCount();
            if (rowList < 1)
                return false;
            return true;
        }
        public bool HasDuplicateByName(string name, AreaControllers areaControllers, long id)
        {
            var rowList =
                Session.QueryOver<Actions>()
                    .Where(x => x.Name == name && x.AreaControllers == areaControllers && x.Id != id && x.Status == Actions.EntityStatus.Active)
                    .RowCount();
            if (rowList < 1)
                return false;
            return true;
        }
        #endregion

        //no need
        //public new IList<Actions> LoadByName(string name)
        //{
        //    IQuery query = Session.CreateQuery("from Actions c where c.Name=:name and c.Status=:status");
        //    query.SetString("name", name);
        //    query.SetInt32("status", Actions.EntityStatus.Active);

        //    IList<Actions> rowList = query.List<Actions>();

        //    if (rowList == null || rowList.Count < 1)
        //        return null;
        //    return rowList;
        //}
    }
}
