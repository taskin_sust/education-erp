﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IDepartmentDao : IBaseDao<Department, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
        IList<Department> LoadActive(int start, int length, string orderBy, string toUpper, List<long> organizationIds, string status);
        IList<Department> LoadHrDepartmentList(List<long> organizationIds);
        #endregion

        #region Others Function
        int TotalRowCountOfDepartmentSettings(List<long> organizationIds , string status);
        bool HasDuplicateByDepartmentName(string name, long organizationId, long? id);
        #endregion

        #region Helper Function

        #endregion

        
        
    }
    public class DepartmentDao : BaseDao<Department, long>, IDepartmentDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        
        #endregion

        #region List Loading Function
        public IList<Department> LoadActive(int start, int length, string orderBy, string orderDir , List<long> organizationIds, string status)
        {
            ICriteria criteria = Session.CreateCriteria<Department>().Add(Restrictions.Not(Restrictions.Eq("Status",Department.EntityStatus.Delete)));
            criteria.CreateAlias("Organization", "organization");
            if (organizationIds!=null && organizationIds.Count > 0)
            {
                criteria.Add(Restrictions.In("organization.Id", organizationIds));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            if (orderDir == "ASC" && orderBy!="")
            {
                criteria.AddOrder(Order.Asc(orderBy));
            }
            else if(orderBy!="")
            {
                criteria.AddOrder(Order.Desc(orderBy));
            }
            return criteria.SetFirstResult(start).SetMaxResults(length).List<Department>();
            
        }

        public IList<Department> LoadHrDepartmentList(List<long> organizationIds)
        {
            var departmenrtList =
                Session.QueryOver<Department>().Where(x => x.Organization.Id.IsIn(organizationIds.ToArray()) && x.Status == Department.EntityStatus.Active).OrderBy(x => x.Rank).Asc.List<Department>();
            return departmenrtList;
        }
        #endregion

        #region Others Function
        public int TotalRowCountOfDepartmentSettings(List<long> organizationIds, string status)
        {
            ICriteria criteria = Session.CreateCriteria<Department>().Add(Restrictions.Not(Restrictions.Eq("Status", Department.EntityStatus.Delete)));
            criteria.CreateAlias("Organization", "Organization");
            if (organizationIds!=null)
            {
                criteria.Add(Restrictions.In("Organization.Id", organizationIds));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));
            }
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public bool HasDuplicateByDepartmentName(string name, long organizationId, long? id)
        {
            ICriteria criteria = Session.CreateCriteria<Department>();
            criteria.CreateAlias("Organization", "org").Add(Restrictions.Eq("org.Status", Organization.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Name", name));
            criteria.Add(Restrictions.Eq("org.Id", organizationId));
            criteria.Add(Restrictions.Eq("Status", Designation.EntityStatus.Active));
            if (id != null)
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            IList<Department> rowList = criteria.List<Department>();
            if (rowList == null || rowList.Count < 1)
                return true;
            return false;
        }

        #endregion

        #region Helper Function

        #endregion

        
    }

}
