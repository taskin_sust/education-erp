﻿using System;
using System.Collections.Generic;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class ReferrerFactory : ObjectFactoryBase<Referrer>
    {
        #region Object Initialization

        protected readonly IReferenceService _referenceService;

        public ReferrerFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _referenceService = new ReferenceService(_session);
        }

        #endregion

        #region Create

        public ReferrerFactory WithNameAndRank(string name, int rank)
        {
            Object.Name = name;
            Object.Rank = rank;
            return this;
        }
        public ReferrerFactory WithCode(string code)
        {
            Object.Code = code;
            return this;
        }
        public ReferrerFactory Create()
        {
            Object = new Referrer()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                Rank = 1,
                CreateBy = 1,
                ModifyBy = 1,
                Code = DateTime.Now.ToString("yyyyMMddHHmmss") + Guid.NewGuid().ToString().Substring(0, 5),
                Status = Referrer.EntityStatus.Active
            };
            SingleObjectList.Add(Object);
            return this;
        }


        public ReferrerFactory Persist()
        {
            if (ObjectList != null)
            {
                foreach (var item in ObjectList)
                {
                    if (item.Id == 0)
                    {
                        _referenceService.Save(item);
                    }
                }
            }

            if (SingleObjectList != null)
            {
                foreach (var item in SingleObjectList)
                {
                    if (item.Id == 0)
                    {
                        _referenceService.Save(item);
                    }
                }
            }
            return this;
        }

        public ReferrerFactory CreateMore(int numberOfReferrer)
        {
            CreateRerrerList(numberOfReferrer);
            return this;
        }

        private List<Referrer> CreateRerrerList(int numberOfReferrer)
        {
            ListStartIndex = ObjectList.Count;
            var list = new List<Referrer>();
            for (int i = 0; i < numberOfReferrer; i++)
            {
                var session = new Referrer()
                {
                    Id = 0,
                    Name = Guid.NewGuid() + "_" + i,
                    Code = "0" + i,
                    Rank = i,
                    CreateBy = 1,
                    ModifyBy = 1,
                    Status = Referrer.EntityStatus.Active
                };
                list.Add(session);
            }
            ObjectList.AddRange(list);
            return list;
        }

        #endregion

        #region Others
        #endregion

        #region Cleanup

        public void Cleanup()
        {
            DeleteAll();
        }

        #endregion

    }
}
