using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IEmployeeBenefitsFundSettingDao : IBaseDao<EmployeeBenefitsFundSetting, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        IList<EmployeeBenefitsFundSetting> LoadEmployeeBenefitFundSetting(int start, int length, string orderBy, string orderDir, List<long> authorizedOrganization, long? employeeStatus, long? calculationOn);
        IList<EmployeeBenefitFundBalanceView> LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(List<long> authOrganizationIds, List<long> authBranchIds, IList<long> campusIds, IList<long> departmentIds, IList<long> designationIds, int employmentStatus, DateTime effectiveDate);


        #endregion

        #region Others Function

        ICriteria LoadEmployeeBenefitFundSettingCount(string orderBy, string orderDir, List<long> authorizedOrganization, long? employeeStatus, long? calculationOn);

        #endregion

        #region Helper Function

        bool HasDuplicate(EmployeeBenefitsFundSetting employeeBenefitsFund, out string name, bool isUpdate);
        bool HasDuplicateCombination(EmployeeBenefitsFundSetting employeeBenefitsFund, bool isUpdate);
        bool HasSalarySheet(EmployeeBenefitsFundSetting employeeBenefitsFund);

        #endregion
    }

    public class EmployeeBenefitsFundSettingDao : BaseDao<EmployeeBenefitsFundSetting, long>, IEmployeeBenefitsFundSettingDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public IList<EmployeeBenefitsFundSetting> LoadEmployeeBenefitFundSetting(int start, int length, string orderBy, string orderDir, List<long> authorizedOrganization, long? employeeStatus, long? calculationOn)
        {
            ICriteria criteria = GetEmployeeBenefitFundSettingCriteria(orderBy, orderDir, authorizedOrganization, employeeStatus, calculationOn);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<EmployeeBenefitsFundSetting>();
        }


        public IList<EmployeeBenefitFundBalanceView> LoadAvailableTeamMemberForOpeningEmployeeBenefitfundSetting(List<long> authOrganizationIds, List<long> authBranchIds, IList<long> campusIds, IList<long> departmentIds, IList<long> designationIds, int employmentStatus, DateTime effectiveDate)
        {
            string condition = "";
            condition += " AND o.Id IN (" + string.Join(", ", authOrganizationIds) + ")";
            if (authBranchIds.Count != 0)
            {
                condition += " AND br.Id IN (" + string.Join(", ", authBranchIds) + ")";
            }

            if (campusIds.Count != 0)
            {
                condition += " AND c.Id IN (" + string.Join(", ", campusIds) + ")";
            }

            if (departmentIds.Count != 0)
            {
                condition += " AND dp.Id IN (" + string.Join(", ", departmentIds) + ")";
            }

            if (designationIds.Count != 0)
            {
                condition += " AND dg.Id IN (" + string.Join(", ", designationIds) + ")";
            }

            condition += " AND currentPosition.[EmploymentStatus] IN (" + string.Join(", ", employmentStatus) + ")";
            condition += " AND ebfb.TeamMemberId IS NULL";

            string query = String.Format(@"
                                            SELECT 
		                                    t.Id AS TeamMemberId
		                                    ,t.Pin AS Pin
		                                    ,t.Name AS TeamMember
		                                    ,currentPosition.[EmploymentStatus] AS MemberType
		                                    ,dp.Name as Department
		                                    ,dg.Name as Designation
		                                    ,o.Id as OrganizationId
                                            ,ebfb.TeamMemberId
		                                    FROM (
		                                    SELECT * FROM (
			                                    SELECT [CampusId]
				                                    ,[DesignationId]
				                                    ,[DepartmentId]
				                                    ,[TeamMemberId]
				                                    ,[EffectiveDate]
				                                    ,[EmploymentStatus]
				                                    ,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC) AS R  
			                                    FROM [dbo].[HR_EmploymentHistory] Where [Status] = 1 AND EffectiveDate <='{0}'
		                                    ) AS a WHERE a.R = 1
		                                    ) AS currentPosition
		                                    LEFT JOIN [dbo].[HR_TeamMember] AS t ON t.Id = currentPosition.TeamMemberId  AND t.Status = 1
		                                    LEFT JOIN [dbo].[Campus] AS c ON c.Id = currentPosition.CampusId and c.Status = 1
		                                    LEFT JOIN [dbo].[Branch] AS br ON br.Id = c.BranchId and br.Status = 1
		                                    LEFT JOIN [dbo].[Organization] AS o ON o.Id = br.OrganizationId AND o.Status =1
		                                    LEFT JOIN [dbo].[HR_Department] AS dp ON dp.Id = currentPosition.DepartmentId and dp.Status = 1
		                                    LEFT JOIN [dbo].[HR_Designation] AS dg ON dg.Id = currentPosition.DesignationId and dg.Status = 1
	                                        LEFT JOIN HR_MemberEbfBalance AS ebfb ON ebfb.TeamMemberId=t.Id
		                                    WHERE 	1=1
                                           
                                            " + condition + @"

                                            ", effectiveDate, authOrganizationIds);

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<EmployeeBenefitFundBalanceView>());
            iQuery.SetTimeout(5000);
            return iQuery.List<EmployeeBenefitFundBalanceView>().ToList();

        }

        #endregion

        #region Others Function

        private ICriteria GetEmployeeBenefitFundSettingCriteria(string orderBy, string orderDir, List<long> authorizedOrganization, long? employeeStatus, long? calculationOn, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<EmployeeBenefitsFundSetting>().Add(Restrictions.Not(Restrictions.Eq("Status", EmployeeBenefitsFundSetting.EntityStatus.Delete)));
            criteria.CreateAlias("Organization", "organization").Add(Restrictions.Not(Restrictions.Eq("organization.Status", Organization.EntityStatus.Delete)));

            if (authorizedOrganization.Count > 0)
            {
                criteria.Add(Restrictions.In("organization.Id", authorizedOrganization));
            }

            if (employeeStatus != null)
            {
                criteria.Add(Restrictions.Eq("EmploymentStatus", Convert.ToInt32(employeeStatus)));
            }

            if (calculationOn != null)
            {
                criteria.Add(Restrictions.Eq("CalculationOn", Convert.ToInt32(calculationOn)));
            }

            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }



        public ICriteria LoadEmployeeBenefitFundSettingCount(string orderBy, string orderDir, List<long> authorizedOrganization, long? employeeStatus, long? calculationOn)
        {
            ICriteria criteria = GetEmployeeBenefitFundSettingCriteria(orderBy, orderDir, authorizedOrganization, employeeStatus, calculationOn, true);
            return criteria;
        }

        #endregion

        #region Helper Function

        public bool HasDuplicate(EmployeeBenefitsFundSetting employeeBenefitsFund, out string name, bool isUpdate)
        {
            ICriteria criteria = Session.CreateCriteria<EmployeeBenefitsFundSetting>();
            criteria.Add(Restrictions.Eq("Status", EmployeeBenefitsFundSetting.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Name", employeeBenefitsFund.Name));
            criteria.CreateAlias("Organization", "organization").Add(Restrictions.Not(Restrictions.Eq("organization.Status", Organization.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("organization.Id", employeeBenefitsFund.OrganizationId));

            if (isUpdate)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", employeeBenefitsFund.Id)));
                criteria.Add(Restrictions.Eq("organization.Id", employeeBenefitsFund.OrganizationId));
            }
            criteria.SetProjection(Projections.RowCount());

            name = "";
            bool returnValue = false;
            int count = Convert.ToInt32(criteria.UniqueResult());
            if (count > 0)
            {
                name = employeeBenefitsFund.Name;
                returnValue = true;
            }
            else
            {
                returnValue = false;
            }
            return returnValue;
        }

        public bool HasDuplicateCombination(EmployeeBenefitsFundSetting employeeBenefitsFund, bool isUpdate)
        {
            #region OLD Code

            //            //date logic   
            //            //--SELECT *
            //            //--FROM table
            //            //--WHERE (StartNumber <= 201000261        AND 201000261 <= EndNumber) 
            //            //--     OR (StartNumber <= 201000265          AND 201000265 <= EndNumber)
            //            //--     OR (201000261 <= StartNumber          AND StartNumber <= 201000265)
            //            //--     OR (201000261 <= EndNumber          AND EndNumber <= 201000265)

            //            string condition = "";

            //            if (isUpdate)
            //                condition = string.Format(@"AND ebfs.Id <> {0}", employeeBenefitsFund.Id);

            //            string query = string.Format(@"
            //
            //                            SELECT COUNT(*) FROM (
            //
            //                            SELECT 
            //		                            ebfs.OrganizationId,
            //		                            ebfs.EmploymentStatus,ebfs.EffectiveDate,ebfs.ClosingDate
            //		                            FROM HR_EmployeeBenefitsFundSetting ebfs
            //		                            WHERE 1=1 
            //                                    AND Status=1
            //		                            AND ebfs.OrganizationId={0}
            //		                            AND ebfs.EmploymentStatus={1}
            //                                    AND (ebfs.ClosingDate IS NULL OR ebfs.ClosingDate>=  '{2}')
            //                                    " + condition + @"
            //		
            //                            ) AS s ",
            //                                     employeeBenefitsFund.OrganizationId
            //                                    , employeeBenefitsFund.EmploymentStatus
            //                                    , employeeBenefitsFund.EffectiveDate.ToString()
            //                                    );

            //            IQuery iQuery = Session.CreateSQLQuery(query);
            //            var count = Convert.ToInt32(iQuery.UniqueResult());
            //            return count > 0;


            #endregion

            ICriteria criteria = Session.CreateCriteria<EmployeeBenefitsFundSetting>();
            criteria.Add(Restrictions.Eq("Status", EmployeeBenefitsFundSetting.EntityStatus.Active));

            criteria.CreateAlias("Organization", "organization").Add(Restrictions.Not(Restrictions.Eq("organization.Status", Organization.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("organization.Id", employeeBenefitsFund.OrganizationId));
            criteria.Add(Restrictions.Eq("EmploymentStatus", employeeBenefitsFund.EmploymentStatus));

            var disjunction = Restrictions.Disjunction(); // for OR statement 
            disjunction.Add(Restrictions.IsNull("ClosingDate") || Restrictions.Ge("ClosingDate", employeeBenefitsFund.EffectiveDate));
            criteria.Add(disjunction);
            
            if (isUpdate)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", employeeBenefitsFund.Id)));
            }

            criteria.SetTimeout(3000);
            criteria.SetProjection(Projections.RowCount());
            int count = Convert.ToInt32(criteria.UniqueResult());
            return count > 0;
        }

        public bool HasSalarySheet(EmployeeBenefitsFundSetting employeeBenefitsFund)
        {
//            string query = string.Format(@"SELECT COUNT(*) FROM HR_SalarySheet ss
//                                            INNER JOIN HR_MemberEbfHistory meh ON meh.SalarySheetId=ss.Id
//                                            WHERE 1=1");

            string query = string.Format(@"SELECT COUNT(*) FROM HR_MemberEbfHistory ebfh
                                            INNER JOIN HR_EmployeeBenefitsFundSetting emp ON emp.Id=ebfh.EmployeeBenefitsFundSettingId
                                            WHERE 1=1
                                            AND emp.Id={0}",employeeBenefitsFund.Id);

            IQuery iQuery = Session.CreateSQLQuery(query);
            var count = Convert.ToInt32(iQuery.UniqueResult());

            if (count > 0)
                return true;
            return false;
        }

        #endregion
    }
}
