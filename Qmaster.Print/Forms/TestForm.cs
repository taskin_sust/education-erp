﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Qmaster.Print.Model;
using UdvashERP.FingerPrint.BusinessRule;
using Qmaster.Print.Helper;

namespace Qmaster.Print.Forms
{
    public partial class TestForm : BaseForm
    {
        private PrintConfigInfo _printConfigInfo = null;
        private string _xmlFileName = PrintConstant.ConfigXmlFileName;

        public PrinterSettings ps = new PrinterSettings();
        public TestForm()
        {
            InitializeComponent();
        }
        private void TestForm_Load(object sender, EventArgs e)
        {
            _printConfigInfo = new PrintConfigInfo();
            LoadPrintConfigInfoFromXml();

            //txtPrintTest.Text = "For Print Question Please Follow this" + Environment.NewLine
            //                    + " 1. Go to your control panel and select \"Device and Printers\" " + Environment.NewLine
            //                    + " 2. Select your printing printer and right click it and the click \"Printer Properties\" " + Environment.NewLine
            //                    + " 3. Then select Advance tab's Printing Defaults button  " + Environment.NewLine
            //                    + " 4. And then configure how you want to print it."
            //                    ;
        }

        private void PrintDocumentOnPrintPage(object sender, PrintPageEventArgs e)
        {
            string s = "";
            e.Graphics.DrawString(s, Font.FromHdc(IntPtr.Zero), Brushes.Black, 10, 25);
        }

        private void btnPrintTest_Click(object sender, EventArgs e)
        {
            PrintDialog pdlg = new PrintDialog();
            PrintDocument printDocument = new PrintDocument();
            printDocument.PrintPage += PrintDocumentOnPrintPage;
            pdlg.Document = printDocument;
            pdlg.ShowDialog();
            ps = pdlg.PrinterSettings;
        }

        public void BtnPrintTestClick()
        {
            btnPrintTest_Click(null, null);
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            string message = "";
            bool state = false;
            try
            {
                if (SavePrintConfigInfoToXml())
                {
                    message = "Config save successfully!";
                    state = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                MessageBox.Show(message);

            }
        }

        private bool LoadPrintConfigInfoFromXml()
        {
            try
            {
                CheckPrintConfigInfoFile();

                XmlSerializer xs = new XmlSerializer(typeof(PrintConfigInfo));
                FileStream fs = new FileStream(Application.StartupPath + "\\" + _xmlFileName, FileMode.Open);
                _printConfigInfo = (PrintConfigInfo)xs.Deserialize(fs);

                cbIsBarcode.Checked = _printConfigInfo.IsBarCode;
                cbIsQrCode.Checked = _printConfigInfo.IsQrCode;
                cbIsPrint.Checked = _printConfigInfo.IsPrint;
                cbDownloadNewFile.Checked = _printConfigInfo.DownloadNewFile;
                cbDeleteAllGeneratedFile.Checked = _printConfigInfo.IsDeleteAllGeneratedFile;
                cbDeleteUnzipFile.Checked = _printConfigInfo.IsDeleteUnzipFile;
                cbDeleteZipFile.Checked = _printConfigInfo.IsDeleteZipFile;
                cbDeleteMargeFile.Checked = _printConfigInfo.IsDeleteAllMargeFile;
                cbbarNo.Checked = _printConfigInfo.ShowBarNo;
                txtBarWidth.Text = _printConfigInfo.BarWidth.ToString();
                txtBarHeight.Text = _printConfigInfo.BarHeight.ToString();
                txtQrWidth.Text = _printConfigInfo.QrWidth.ToString();
                txtQrHeight.Text = _printConfigInfo.QrHeight.ToString();
                txtBarDipX.Text = _printConfigInfo.BarDipX.ToString();
                txtBarDipY.Text = _printConfigInfo.BarDipY.ToString();
                txtQrDipX.Text = _printConfigInfo.QrDipX.ToString();
                txtQrDipY.Text = _printConfigInfo.QrDipY.ToString();
                txtMinPage.Text = _printConfigInfo.MinPages.ToString();
                txtMaxPage.Text = _printConfigInfo.MaxPages.ToString();
                txtQrXaxis.Text = _printConfigInfo.QrAxis.ToString();
                txtQrYaxis.Text = _printConfigInfo.QrYaxis.ToString();
                txtBarXaxis.Text = _printConfigInfo.BarAxis.ToString();
                txtBarYaxis.Text = _printConfigInfo.BarYaxis.ToString();
                txtMargePdf.Text = _printConfigInfo.MargePdf.ToString();
                txtShowBarQrCodeOnPage.Text = _printConfigInfo.ShowBarQrCodeOnPage.ToString();
                cbIsAlpha.Checked = _printConfigInfo.IsAlpha;
                cbIsOsl.Checked = _printConfigInfo.IsOsl;
                cbShowCodeNumber.Checked = _printConfigInfo.ShowCodeNumber;
                cbShowCodeNumberWithExamNo.Checked = _printConfigInfo.ShowCodeNumberWithExamNo;
                txtShowCodeNumberOnPage.Text = _printConfigInfo.ShowCodeNumberOnPage.ToString();
                txtCodeNumberX.Text = _printConfigInfo.CodeNumberX.ToString();
                txtCodeNumberY.Text = _printConfigInfo.CodeNumberY.ToString();

                txtShowCodeNumberOnPage2nd.Text = _printConfigInfo.ShowCodeNumberOnPage2Nd.ToString();
                txtCodeNumberX2nd.Text = _printConfigInfo.CodeNumberX2Nd.ToString();
                txtCodeNumberY2nd.Text = _printConfigInfo.CodeNumberY2Nd.ToString();
                fs.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void CheckPrintConfigInfoFile()
        {
            PrintPdfHelper.SavePrintConfigInfo();
        }

        private bool SavePrintConfigInfoToXml()
        {
            PrintConfigInfo printConfigInfo = new PrintConfigInfo();
            printConfigInfo.IsBarCode = cbIsBarcode.Checked;
            printConfigInfo.IsQrCode = cbIsQrCode.Checked;
            printConfigInfo.IsPrint = cbIsPrint.Checked;
            printConfigInfo.DownloadNewFile = cbDownloadNewFile.Checked;
            printConfigInfo.IsDeleteAllGeneratedFile = cbDeleteAllGeneratedFile.Checked;
            printConfigInfo.IsDeleteUnzipFile = cbDeleteUnzipFile.Checked;
            printConfigInfo.IsDeleteZipFile = cbDeleteZipFile.Checked;
            printConfigInfo.IsDeleteAllMargeFile = cbDeleteMargeFile.Checked;
            printConfigInfo.ShowBarNo = cbbarNo.Checked;
            printConfigInfo.BarWidth = Convert.ToInt32(txtBarWidth.Text);
            printConfigInfo.BarHeight = Convert.ToInt32(txtBarHeight.Text);
            printConfigInfo.QrWidth =  Convert.ToInt32(txtQrWidth.Text);
            printConfigInfo.QrHeight = Convert.ToInt32(txtQrHeight.Text);
            printConfigInfo.BarDipX = Convert.ToInt32(txtBarDipX.Text) ;
            printConfigInfo.BarDipY = Convert.ToInt32(txtBarDipY.Text);
            printConfigInfo.QrDipX = Convert.ToInt32(txtQrDipX.Text);
            printConfigInfo.QrDipY = Convert.ToInt32(txtQrDipY.Text);
            printConfigInfo.MinPages = Convert.ToInt32(txtMinPage.Text);
            printConfigInfo.MaxPages = Convert.ToInt32(txtMaxPage.Text);
            printConfigInfo.QrAxis = Convert.ToInt32(txtQrXaxis.Text);
            printConfigInfo.QrYaxis = Convert.ToInt32(txtQrYaxis.Text);
            printConfigInfo.BarAxis = Convert.ToInt32(txtBarXaxis.Text);
            printConfigInfo.BarYaxis = Convert.ToInt32(txtBarYaxis.Text);
            printConfigInfo.MargePdf = Convert.ToInt32(txtMargePdf.Text);
            printConfigInfo.ShowBarQrCodeOnPage = Convert.ToInt32(txtShowBarQrCodeOnPage.Text);
            printConfigInfo.IsOsl = cbIsOsl.Checked;
            printConfigInfo.IsAlpha = cbIsAlpha.Checked;
            printConfigInfo.ShowCodeNumber = cbShowCodeNumber.Checked;
            printConfigInfo.ShowCodeNumberWithExamNo = cbShowCodeNumberWithExamNo.Checked;
            printConfigInfo.ShowCodeNumberOnPage = Convert.ToInt32(txtShowCodeNumberOnPage.Text);
            printConfigInfo.CodeNumberX = Convert.ToInt32(txtCodeNumberX.Text);
            printConfigInfo.CodeNumberY = Convert.ToInt32(txtCodeNumberY.Text);

            if (!String.IsNullOrEmpty(txtShowCodeNumberOnPage2nd.Text))
                printConfigInfo.ShowCodeNumberOnPage2Nd = Convert.ToInt32(txtShowCodeNumberOnPage2nd.Text);
            if (!String.IsNullOrEmpty(txtCodeNumberX2nd.Text))
                printConfigInfo.CodeNumberX2Nd = Convert.ToInt32(txtCodeNumberX2nd.Text);
            if (!String.IsNullOrEmpty(txtCodeNumberY2nd.Text))
                printConfigInfo.CodeNumberY2Nd = Convert.ToInt32(txtCodeNumberY2nd.Text);

            XmlSerializer xs = new XmlSerializer(typeof(PrintConfigInfo));
            // Create a new file stream to write the serialized object to a file
            TextWriter WriteFileStream = new StreamWriter(Application.StartupPath + "\\" + _xmlFileName);
            xs.Serialize(WriteFileStream, printConfigInfo);
            // Cleanup
            WriteFileStream.Close();
            return true;
        }

    }
}
