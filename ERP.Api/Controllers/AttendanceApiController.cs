﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Security.Policy;
using UdvashERP.Api;
using Newtonsoft.Json;
using System.Net;

namespace UdvashERP.Api.Controllers
{
    public class AttendanceApiController : Controller
    {

        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AttendanceAPI");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IAttendanceDeviceService _hrAttendanceDeviceService;
        private readonly IAttendanceSynchronizerService _hrAttendanceSynchronizerService;
        private readonly IAttendanceSynchronizerSettingService _hrAttendanceSynchronizerSettingService;
        private readonly IMemberOfficialDetailService _memberOfficialDetailService;
        private readonly IAttendanceDeviceRawDataService _attendanceDeviceRawDataService;
        public AttendanceApiController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _hrAttendanceDeviceService = new AttendanceDeviceService(session);
                _hrAttendanceSynchronizerService = new AttendanceSynchronizerService(session);
                _hrAttendanceSynchronizerSettingService = new AttendanceSynchronizerSettingService(session);
                _memberOfficialDetailService = new MemberOfficialDetailService(session);
                _attendanceDeviceRawDataService = new AttendanceDeviceRawDataService(session);
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }
        private string GetIPAddress()
        {
            //string ipAdd = "";
            //IPHostEntry Host = default(IPHostEntry);
            //string Hostname = null;
            //Hostname = System.Environment.MachineName;
            //Host = Dns.GetHostEntry(Hostname);
            //foreach (IPAddress IP in Host.AddressList)
            //{
            //    if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            //    {
            //        ipAdd = Convert.ToString(IP);
            //    }
            //}
            //return ipAdd;

            string VisitorsIPAddr = string.Empty;
            if (HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (HttpContext.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = HttpContext.Request.UserHostAddress;
            }
            return VisitorsIPAddr;


        }
        #endregion

        [HttpGet]
        public JsonResult GetCurrentDateTime(string key)
        {
            var attendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(key);

            if (attendanceSynchronizer == null)
                return Json(new JsonResponse(false, "Invalid Key"), JsonRequestBehavior.AllowGet);
            //return Json(new JsonResponse(true, JsonConvert.SerializeObject(DateTime.Now)), JsonRequestBehavior.AllowGet);
            return Json(new JsonResponse(true, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAttendanceSynchronizer(string key, string hddSerialList = "", string version = "")
        {
            try
            {
                //var attendanceSynchronizerViewModel = new AttendanceSynchronizerViewModel();
                var attendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(key);
                DateTime serverTime = DateTime.Now;
                string serverTimeStr = serverTime.ToString("yyyy-MM-dd HH:mm:ss");
                if (attendanceSynchronizer == null || attendanceSynchronizer.AttendanceDevice.Any() == false)
                {
                    return Json(new JsonResponse(false, "No Device Found"), JsonRequestBehavior.AllowGet);
                }

                //get memberlist
                IList<dynamic> memberList = new List<dynamic>();
                var teamMemberList = _memberOfficialDetailService.GetTeamMemberLists(key, false);
                foreach (var memberOfficialDetail in teamMemberList)
                {
                    var memberInfo = new
                    {
                        Name = memberOfficialDetail.TeamMember.Name,
                        CardNumber = memberOfficialDetail.CardNo,
                        Pin = memberOfficialDetail.TeamMember.Pin,
                        Status = memberOfficialDetail.Status,
                        //LastModificationTime = DateTime.Now,
                        VersionNumber = memberOfficialDetail.VersionNumber,
                        IsAdmin = memberOfficialDetail.TeamMember.Pin == 1 ? true : false
                    };
                    memberList.Add(memberInfo);
                }

                IList<dynamic> memberListFull = new List<dynamic>();
                var teamMemberListFull = _memberOfficialDetailService.GetTeamMemberLists(key, true);
                foreach (var memberOfficialDetail in teamMemberListFull)
                {
                    var memberInfo = new
                    {
                        Name = memberOfficialDetail.TeamMember.Name,
                        CardNumber = memberOfficialDetail.CardNo,
                        Pin = memberOfficialDetail.TeamMember.Pin,
                        Status = memberOfficialDetail.Status,
                        //LastModificationTime = DateTime.Now,
                        VersionNumber = memberOfficialDetail.VersionNumber,
                        IsAdmin = memberOfficialDetail.TeamMember.Pin == 1 ? true : false
                    };
                    memberListFull.Add(memberInfo);
                }

                //get device
                var attendanceDevices = attendanceSynchronizer.AttendanceDevice.Where(a => a.Status == AttendanceDevice.EntityStatus.Active).Select(attendanceDevice => new
                {
                    Id = attendanceDevice.Id,
                    Name = attendanceDevice.Name,
                    DeviceModelNo = attendanceDevice.DeviceModelNo,
                    //CampusId = attendanceDevice.Campus.Id,
                    CommunicationType = attendanceDevice.CommunicationType,
                    IPAddress = attendanceDevice.IpAddress,
                    Port = attendanceDevice.Port,
                    CommunicationKey = attendanceDevice.CommunicationKey,
                    MachineNo = attendanceDevice.MachineNo,
                    //UpdatedMemberList = memberList,
                    TeamMembers = attendanceDevice.IsReset == true ? memberListFull : memberList,
                    VersionNumber = attendanceDevice.VersionNumber,
                    IsReset = attendanceDevice.IsReset,
                    //LastUpdateTime = JsonConvert.SerializeObject(serverTime)
                    LastUpdateTime = serverTimeStr
                }).ToList();

                //generate json
                var data = new
                {
                    Name = attendanceSynchronizer.Name,
                    //CampusId = attendanceSynchronizer.Campus.Id,
                    //SynchronizerKey = attendanceSynchronizer.SynchronizerKey,
                    DataCallingInterval = attendanceSynchronizer.DataCallingInterval,
                    //OperatorContact = attendanceSynchronizer.OperatorContact,
                    //LastMemberInfoUpdateTime = attendanceSynchronizer.LastMemberInfoUpdateTime == null ? null : JsonConvert.SerializeObject(attendanceSynchronizer.LastMemberInfoUpdateTime.Value),
                    UpdateRequired = false,
                    AttendanceDevices = attendanceDevices
                };
                return Json(new JsonResponse(true, data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new JsonResponse(false, e.Message), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UploadAttendanceData(string key, string attendanceString)
        {
            bool success = false;
            string message = "";
            //GET USER
            long aspNetUserIdL = Convert.ToInt64(ConfigurationManager.AppSettings["UserId"]);

            var attendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(key);
            if (attendanceSynchronizer == null || attendanceSynchronizer.AttendanceDevice.Any() == false)
            {
                message = "No Device Found";
            }

            if (attendanceString.Equals("[]"))
            {
                message = "No Data Found";
            }
            else
            {
                var attendanceStrList = new JavaScriptSerializer().Deserialize<List<AttendanceDeviceRawDataViewModel>>(attendanceString);
                string requestIp = GetIPAddress();
                var isSuccess = _attendanceDeviceRawDataService.AttendanceDeviceSynchronizations(attendanceStrList, aspNetUserIdL, requestIp);
                success = isSuccess;
                message = "Successfull";
            }

            return Json(new JsonResponse(success, ""), JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult UploadAttendanceErrorPins(string key, string deviceId, string pinCsv)
        {
            bool success = false;
            string message = "";
            //GET USER
            long aspNetUserIdL = Convert.ToInt64(ConfigurationManager.AppSettings["UserId"]);

            var attendanceSynchronizer = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(key);
            if (attendanceSynchronizer == null || attendanceSynchronizer.AttendanceDevice.Where(x => x.Id == Convert.ToInt64(deviceId)).Count() <= 0)
            {
                message = "No Device Found";
            }

            if (pinCsv.Trim().Equals(""))
            {
                //lastUpdateTime = DateTime.Now;
                message = "No Pin Found";
            }
            else
            {
                var pins = pinCsv.Split(',');
                List<AttendanceDeviceErrorPinViewModel> attendanceStrList = new List<AttendanceDeviceErrorPinViewModel>();
                foreach (var pin in pins)
                {
                    attendanceStrList.Add(new AttendanceDeviceErrorPinViewModel { DeviceId = deviceId, Pin = pin });
                }
                var isSuccess = _attendanceDeviceRawDataService.AttendanceDeviceErrorPinUpdate(attendanceStrList, aspNetUserIdL);
                success = isSuccess;
            }

            return Json(new JsonResponse(success, message), JsonRequestBehavior.DenyGet);
            //return Json(new JsonResponse(true, ""), JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult UpdateTimeOfAttendanceDevice(string key, string deviceId, DateTime updateTime)
        {
            try
            {
                var model = _hrAttendanceSynchronizerService.GetAttendanceSynchronizer(key);
                model.LastMemberInfoUpdateTime = updateTime;
                foreach (var d in model.AttendanceDevice)
                {
                    if (d.Id == Convert.ToInt64(deviceId))
                    {
                        d.IsReset = false;
                        d.LastMemberInfoUpdateTime = updateTime;
                        d.LastRequestIp = GetIPAddress();
                    }

                }
                _hrAttendanceSynchronizerService.Update(model);
            }
            catch (Exception e)
            {
                return Json(new JsonResponse(false, e.Message));
            }
            return Json(new JsonResponse(true, ""));
        }
    }
}