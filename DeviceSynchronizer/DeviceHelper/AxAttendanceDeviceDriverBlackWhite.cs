﻿using DeviceSynchronizer.Helper;
using DeviceSynchronizer.Model.ApiModel;
using DeviceSynchronizer.Model.BusinessModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceSynchronizer.DeviceHelper
{
    public class AxAttendanceDeviceDriverBlackWhite : AxAttendanceDeviceDriver
    {
        public AxAttendanceDeviceDriverBlackWhite(AttendanceDeviceModel attendanceDevice, MessagePass throwMessage)
            : base(attendanceDevice, throwMessage)
        {
        }

        protected override bool Register(TeamMemberModel member, string sPassword, int iPrivilege)
        {
            bool returnResult = false;
            try
            {
                bool isEnabled = (member.Status == MemberStatus.Active);
                int enrollNumber = member.EnrollNo;
                string displayName = member.Name;
                string cardnumber = member.CardNumber;
                string name = "";
                string nPass = "";
                int priv = 0;
                bool isExitToDevice = false;
                if (!String.IsNullOrEmpty(cardnumber))
                {
                    bool isSet = AxDevice.SetStrCardNumber(cardnumber);
                    if (!isSet) return false;
                    if (isEnabled)
                        returnResult = AxDevice.SetUserInfo(_attendanceDevice.MachineNo, enrollNumber, displayName, sPassword, iPrivilege, isEnabled);
                    else
                    {
                        //check if user exits in device. Delete User if only Exit 
                        AxDevice.GetUserInfoByCard(_attendanceDevice.MachineNo, ref name, ref nPass, ref priv, ref isExitToDevice);
                        if (isExitToDevice)
                            returnResult = AxDevice.DeleteEnrollData(_attendanceDevice.MachineNo, enrollNumber, _attendanceDevice.MachineNo, BusinessRules.BackupNumber);
                        else
                            return returnResult;
                    }
                    if (returnResult)
                        OnThrowingMessage("Register user " + member.Name + " in device with pin " + member.Pin, MessageType.Info);
                    else
                        OnThrowingMessage("Failed to save user " + member.Name + " in device ", MessageType.Error);
                }
                return returnResult;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

        public override List<AttendanceLog> LoadLogList()
        {
            //for black-white device
            var idwTMachineNumber = 0;
            var idwEnrollNumber = 0;
            var idwEMachineNumber = 0;
            var idwVerifyMode = 0;
            var idwInOutMode = 0;
            var idwYear = 0;
            var idwMonth = 0;
            var idwDay = 0;
            var idwHour = 0;
            var idwMinute = 0; var idwSecond = 0;
            var attdlogList = new List<AttendanceLog>();
            try
            {
                OnThrowingMessage("Retriving attendance data from device", MessageType.Info);

                //for BW
                while (AxDevice.GetGeneralLogData(_attendanceDevice.MachineNo, ref idwTMachineNumber, ref idwEnrollNumber,
                         ref idwEMachineNumber, ref idwVerifyMode, ref idwInOutMode, ref idwYear, ref idwMonth, ref idwDay, ref idwHour, ref idwMinute))
                {
                    var attdLogObj = new AttendanceLog
                    {
                        EnrollNumber = idwEnrollNumber,
                        PunchTime = new DateTime(idwYear, idwMonth, idwDay, idwHour, idwMinute, 0).ToString("yyyy-MM-dd HH:mm:ss"),
                        DeviceId = _attendanceDevice.Id
                    };

                    if (attdLogObj.EnrollNumber != 0)
                        attdlogList.Add(attdLogObj);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
            return attdlogList;
        }
    }
}
