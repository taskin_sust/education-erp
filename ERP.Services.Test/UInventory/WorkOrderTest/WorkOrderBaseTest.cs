﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.WorkOrderTest
{
    public class WorkOrderBaseTest:TestBase,IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper CommonHelper;
        private ISession _session;
        internal QuotationService _quotationService;
        internal QuotationFactory _quotationFactory;
        internal BranchFactory _branchFactory;
        internal ItemFactory _itemFactory;
        internal ProgramSessionItemFactory _programSessionItemFactory;
        internal TestBaseService<SpecificationCriteria> _testBaseService;
        internal SupplierFactory _supplierFactory;
        internal SupplierBankDetailsFactory _supplierBankDetailsFactory;
        internal SupplierItemFactory _supplierItemFactory;
        internal SupplierPriceQuoteFactory _supplierPriceQuoteFactory;
        internal WorkOrderFactory _workOrderFactory;
        internal WorkOrderService _workOrderService;
        internal GoodsReceiveFactory _goodsReceiveFactory;
        internal GoodsReceiveDetailsFactory _goodsReceiveDetailsFactory;
        internal readonly string _name = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";

        #endregion

        public WorkOrderBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _quotationFactory = new QuotationFactory(null, _session);
            CommonHelper = new CommonHelper();
            _quotationService = new QuotationService(_session);
            _testBaseService = new TestBaseService<SpecificationCriteria>(_session);
            _branchFactory = new BranchFactory(null, _session);
            _itemFactory = new ItemFactory(null, _session);
            _supplierFactory = new SupplierFactory(null, _session);
            _supplierBankDetailsFactory = new SupplierBankDetailsFactory(null, _session);
            _programSessionItemFactory = new ProgramSessionItemFactory(null, _session);
            _supplierItemFactory = new SupplierItemFactory(null, _session);
            _supplierPriceQuoteFactory = new SupplierPriceQuoteFactory(null, _session);
            _workOrderFactory = new WorkOrderFactory(null, _session);
            _goodsReceiveFactory = new GoodsReceiveFactory(null,_session);
            _goodsReceiveDetailsFactory = new GoodsReceiveDetailsFactory(null,_session);
            _workOrderService = new WorkOrderService(_session);
        }

        public void Dispose()
        {
            ItemFactory itemFactory = _itemFactory;
            _workOrderFactory.CleanUpCurrentStockQuantity(
                _quotationFactory.Object!=null?_quotationFactory.Object.Program:null,
                _quotationFactory.Object!=null?_quotationFactory.Object.Session:null,
                _branchFactory.Object,
                _itemFactory.Object);
            _goodsReceiveDetailsFactory.OnlyGoodsReceiveDetailsCleanUp();
            _goodsReceiveFactory.OnlyGoodsReceiveFactoryCleanUp();
            _workOrderFactory.CleanUp();
            _quotationFactory.QuotationCriteraiCleanUp(_quotationFactory);
            _quotationFactory.ProgramSessionItemCleanUp(itemFactory);
            _supplierPriceQuoteFactory.CleanUp();
            _quotationFactory.CleanUp();
            _programSessionItemFactory.CleanUp();
            _supplierItemFactory.CleanUp();
            _supplierBankDetailsFactory.CleanUp();
            _supplierFactory.CleanUp();
            _itemFactory.CleanUp();
            _branchFactory.Cleanup();
            base.Dispose();
        }
    }
}
