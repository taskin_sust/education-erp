﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using log4net.Config;
using UdvashERP.App_code;
using UdvashERP.BusinessRules;
using UdvashERP.Models;

namespace UdvashERP
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer<ApplicationDbContext>(null);

            //for database connection string 
            DbConnectionString.UerpDbConnectionString = ConfigurationManager.ConnectionStrings["UdvashErpConnectionString"].ConnectionString;
            DbConnectionString.MediaDbConnectionString = ConfigurationManager.ConnectionStrings["UdvashErpMediaConnectionString"].ConnectionString;

            //Do not use this HibernatingRhinos.Profiler in Production Server
            if (AppConfigHelper.GetBoolean("NHibernateProfiler-Enable", false))
                HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();

            //Log4Net
            if (AppConfigHelper.GetBoolean("Log4Net-Enable", false))
            {
                FileInfo info = new FileInfo(Server.MapPath("~/bin/Log4net.config"));
                XmlConfigurator.Configure(info);
            }
        }
    }
}
