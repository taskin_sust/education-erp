﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using log4net.Config;
using Synchronizer.Updater.BusinessRule;

namespace Synchronizer.Updater
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //prevent run duplicate copy
            bool newCopy;
            string appName = SoftwareInfo.ApplicationName;
            Mutex mutex = new Mutex(true, appName, out newCopy);
            if (newCopy == false)
            {
                MessageBox.Show("\"" + appName + "\" application is already running", @"Duplicate Instance", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
           
            if (args.Length > 0 && args[0].Equals("-AppExit", StringComparison.CurrentCultureIgnoreCase))
            {
                bool isExit = false;
                Boolean.TryParse(args[1], out isExit);
                SoftwareInfo.AutomaticExit = isExit;
        
            }
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "log4net.config"));
            Application.Run(new MainUi());
        }
    }
}
