﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class DesignationSettingsController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IDesignationService _hrDesignationService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private List<UserMenu> _userMenu; 
        public DesignationSettingsController()
        {
            var nHSession = NHibernateSessionFactory.OpenSession();
            _hrDesignationService = new DesignationService(nHSession);
            _organizationService = new OrganizationService(nHSession);
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Function
        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                if (TempData["SuccessMessage"] != null)
                {
                    ViewBag.SuccessMessage = TempData["SuccessMessage"];
                    TempData["SuccessMessage"] = null;
                }
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }
        [HttpPost]
        public ActionResult Create(DesignationSettingsViewModel dsViewModel, string submit)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            try
            {
                if (ModelState.IsValid)
                {
                    var hrDesignation = new Designation
                    {
                        Organization = _organizationService.LoadById(dsViewModel.OrganizationId),
                        Name = dsViewModel.Name,
                        ShortName = dsViewModel.ShortName,
                        Rank = dsViewModel.Rank
                    };
                    bool isSuccess = _hrDesignationService.DesignationSaveUpdate(hrDesignation);
                    if (isSuccess)
                    {
                        ModelState.Clear();
                        if (submit == "Save & New")
                        {
                            TempData["SuccessMessage"] = "Successfully Create";
                        }
                        else if (submit == "Save & Exit")
                        {
                            TempData["SuccessMessage"] = null;
                        }
                    }
                    else
                    {
                        ViewBag.ErrrorMessage = "Problem occured";
                    }
                }

                switch (submit)
                {
                    case "Save & New":
                        return RedirectToAction("Create");
                    case "Save & Exit":
                        return RedirectToAction("Index");
                }
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                _logger.Error(ex);
                return View();
            }
            return View();
        }

        public ActionResult Edit(long id)
        {
            var dSViewModel = new DesignationSettingsViewModel();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                Designation obj = _hrDesignationService.LoadById(id);
                if (obj != null)
                {
                    dSViewModel.Id = obj.Id;
                    dSViewModel.OrganizationId = obj.Organization.Id;
                    dSViewModel.Name = obj.Name;
                    dSViewModel.ShortName = obj.ShortName;
                    if (obj.Rank != null) dSViewModel.Rank = (int) obj.Rank;
                    dSViewModel.Status = obj.Status;
                    GetStatusText();
                    ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", obj.Organization.Id);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View(dSViewModel);
        }
        [HttpPost]
        public ActionResult Edit(long id, DesignationSettingsViewModel dsViewModel)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                GetStatusText();
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", dsViewModel.OrganizationId);
                var hrDesignation = new Designation
                {
                    Id = id,
                    Organization = _organizationService.LoadById(dsViewModel.OrganizationId),
                    Name = dsViewModel.Name,
                    ShortName = dsViewModel.ShortName,
                    Rank =  dsViewModel.Rank,
                    Status = Convert.ToInt32(dsViewModel.Status)
                };
                bool isUpdated = _hrDesignationService.DesignationSaveUpdate(hrDesignation);
                if (isUpdated)
                {
                    ViewBag.SuccessMessage = "Designation settings successfully updated.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Problem Occurred, during designation settings update.";
                }
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred, during designation settings update.";
            }
            return View(dsViewModel);
        }
        [HttpPost]
        public JsonResult Delete(long id)
        {
            try
            {
                Designation hrDesignation = _hrDesignationService.LoadById(id);
                if (hrDesignation != null)
                {
                    var isSuccess = _hrDesignationService.Delete(hrDesignation);
                    return Json(isSuccess ? new Response(true, "Successfully deleted") : new Response(false, "Designation Delete fail !"));
                }
                else
                {
                    return Json(new Response(false, "Designation Delete fail !"));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, "Designation Delete fail !"));
            }
        }

        #endregion

        #region Index/Manage
        public ActionResult Index()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }
        public ActionResult DesignationSettingsList(int draw, int start, int length, string organizationId, string status)
        {
            NameValueCollection nvc = Request.Form;
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            try
            {
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Organization.ShortName";
                            break;
                        case "1":
                            orderBy = "Name";
                            break;
                        case "2":
                            orderBy = "ShortName";
                            break;
                        case "3":
                            orderBy = "Rank";
                            break;
                        case "4":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                int recordsTotal = _hrDesignationService.TotalRowCountOfDepartmentSettings(_userMenu,organizationId, status);
                long recordsFiltered = recordsTotal;

                var rowLists = _hrDesignationService.LoadActive(_userMenu,start, length, orderBy, orderDir.ToUpper(), organizationId, status).ToList();

                foreach (Designation row in rowLists)
                {
                    var str = new List<string>();
                    str.Add(row.Organization.ShortName);
                    str.Add(row.Name);
                    str.Add(row.ShortName);
                    str.Add(row.Rank.ToString());
                    str.Add(StatusTypeText.GetStatusText(row.Status));

                    str.Add("<a href='" + Url.Action("Edit", "DesignationSettings") + "?Id=" + row.Id + "' data-id='" + row.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; " +
                            "<a href='#' data-id='" + row.Id + "' class='glyphicon glyphicon-trash deleteDesignation' name='" + row.Name + "'></a>"
                            );
                    getData.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData

                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }
        #endregion

        #region Helper Function
        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
        }
        [HttpPost]
        public JsonResult DoesRankExist(int rank, long? id, long organizationId)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (id != null)
                    {
                        bool checkRank = _hrDesignationService.HasDuplicateByRank(rank,organizationId, id);
                        return Json(checkRank);
                    }
                    else
                    {
                        bool checkRank = _hrDesignationService.HasDuplicateByRank(rank,organizationId, null);
                        return Json(checkRank);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            else { return Json(false); }
        }
        [HttpPost]
        public JsonResult DoesDesignationNameExist(string name, long organizationId, long? id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (id != null)
                    {
                        bool checkRank = _hrDesignationService.HasDuplicateByDesignationName(name, organizationId, id);
                        return Json(checkRank);
                    }
                    else
                    {
                        bool checkRank = _hrDesignationService.HasDuplicateByDesignationName(name, organizationId, null);
                        return Json(checkRank);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            else { return Json(false); }
        }
        [HttpPost]
        public JsonResult DoesDesignationShortNameExist(string shortName, long organizationId, long? id)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    if (id != null)
                    {
                        bool checkRank = _hrDesignationService.HasDuplicateByDesignationShortName(shortName, organizationId, id);
                        return Json(checkRank);
                    }
                    else
                    {
                        bool checkRank = _hrDesignationService.HasDuplicateByDesignationShortName(shortName, organizationId, null);
                        return Json(checkRank);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    return Json(false);
                }
            }
            else { return Json(false); }
        }
        #endregion

        #region Export
        public ActionResult ExportDegisnationList(string organizationId, string status)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                int recordsTotal = _hrDesignationService.TotalRowCountOfDepartmentSettings(_userMenu,organizationId, status);
                var length = recordsTotal;
                const int start = 0;
                string orderDir = "ASC";
                string orderBy = "Rank";
                var rowLists = _hrDesignationService.LoadActive(_userMenu,start, length, orderBy, orderDir, organizationId, status).ToList();
                
                var headerList = new List<string> { ErpInfo.OrganizationNameFull };

                var footerList = new List<string> { "" };

                var columnList = new List<string>();
                columnList.Add("Organization");
                columnList.Add("Name");
                columnList.Add("Short Name");
                columnList.Add("Rank");
                columnList.Add("Status");
                
                var userExcelList = new List<List<object>>();

                foreach (var row in rowLists)
                {
                    var xlsRow = new List<object>();

                    xlsRow.Add(row.Organization.ShortName);
                    xlsRow.Add(row.Name);
                    xlsRow.Add(row.ShortName);
                    xlsRow.Add(row.Rank);
                    xlsRow.Add(StatusTypeText.GetStatusText(row.Status));
                    userExcelList.Add(xlsRow);
                }
                ExcelGenerator.GenerateExcel(headerList, columnList, userExcelList, footerList, "designation-list-report__"
                        + DateTime.Now.ToString("yyyyMMdd-HHmmss"));

                return View("AutoClose");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("AutoClose");
            }


        }
        #endregion
    }
}