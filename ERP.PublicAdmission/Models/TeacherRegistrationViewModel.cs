﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UdvashERP.PublicAdmission.Models
{
    public class TeacherRegistrationViewModel
    {
        [Required]
        [Display(Name = "Full Name")]
        public virtual string FullName { get; set; }
        [Required]
        [Display(Name = "Nick Name")]
        public virtual string NickName { get; set; }
        [Required]
        [Display(Name = "Mobile Number 1")]
        public virtual string MobileNumber1 { get; set; }
        [Display(Name = "Mobile Number 2")]
        public virtual string MobileNumber2 { get; set; }
        [Required]
        [Display(Name = "Email Address")]
        public virtual string EmailAddress { get; set; }
        [Display(Name = "Facebook ID")]
        public virtual string FacebookId { get; set; }
        [Required]
        [Display(Name = "Institute")]
        public virtual string Institute { get; set; }
        [Required]
        [Display(Name = "Department")]
        public virtual string Department { get; set; }
        [Required]
        [Display(Name = "HSC Passing Year")]
        public virtual string HscPassingYear { get; set; }
        [Required]
        [Display(Name = "Religion")]
        public virtual string Religion { get; set; }
        [Required]
        [Display(Name = "Gender")]
        public virtual string Gender { get; set; }
        [Required]
        [Display(Name = "Teacher Activity")]
        public virtual string TeacherActivity1 { get; set; }
        public virtual string TeacherActivity2 { get; set; }
        public virtual string TeacherActivity3 { get; set; }
        [Required]
        [Display(Name = "Subject Priority")]
        public virtual string Subject1 { get; set; }
        public virtual string Subject2 { get; set; }
        public virtual string Subject3 { get; set; }
        [Required]
        [Display(Name = "Version Priority")]
        public virtual string VersionPriority1 { get; set; }
        public virtual string VersionPriority2 { get; set; }
        [Required]
        [Display(Name = "Are you an Academic Student of Udvash?")]
        public virtual bool IsUdvashStudent { get; set; }
        [Required]
        [Display(Name = "Why Udvash Should appoint you as a teacher?")]
        public virtual string AppointmentRemarks { get; set; }
        public virtual string OrganizationId { get; set; }
    }
}