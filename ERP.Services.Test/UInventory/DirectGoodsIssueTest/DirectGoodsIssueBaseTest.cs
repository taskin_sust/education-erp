﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.UInventory;

namespace UdvashERP.Services.Test.UInventory.DirectGoodsIssueTest
{
    public class DirectGoodsIssueBaseTest : TestBase, IDisposable
    {
        #region Object Initialization

        internal readonly CommonHelper CommonHelper;
        protected readonly ItemFactory _itemFactory;
        protected readonly ProgramFactory _programFactory;
        protected readonly SessionFactory _sessionFactory;
        protected readonly ProgramSessionItemFactory _programSessionItemFactory;
        protected readonly GoodsIssueFactory _goodsIssueFactory;
        protected readonly GoodsIssueDetailsFactory _goodsIssueDetailsFactory;
        protected readonly IGoodsIssueService _goodsIssueService;

        public string _name = "Test_Demo_Quotation01" + new Random().Next(9999999);

        #endregion

        public DirectGoodsIssueBaseTest()
        {
            CommonHelper = new CommonHelper();
            _itemFactory = new ItemFactory(null, Session);
            _programFactory = new ProgramFactory(null, Session);
            _sessionFactory = new SessionFactory(null, Session);
            _programSessionItemFactory = new ProgramSessionItemFactory(null, Session);
            _goodsIssueFactory = new GoodsIssueFactory(null, Session);
            _goodsIssueDetailsFactory = new GoodsIssueDetailsFactory(null, Session);
            _goodsIssueService = new GoodsIssueService(Session);
        }


        public void Dispose()
        {
            if (_goodsIssueFactory.Object != null)
                foreach (var giDetails in _goodsIssueFactory.Object.GoodsIssueDetails)
                {
                    if (giDetails.GoodsIssue.Branch != null && giDetails.Item != null)
                    {
                        string swData = "DELETE FROM [UINV_CurrentStockSummary] WHERE [BranchId] = " + giDetails.GoodsIssue.Branch.Id + " and [ItemId] =" + giDetails.Item.Id + " ; ";
                        Session.CreateSQLQuery(swData).ExecuteUpdate();
                    }
                }
            _goodsIssueDetailsFactory.CleanUp();
            _goodsIssueFactory.CleanUp();
            _programSessionItemFactory.CleanUp();
            _itemFactory.CleanUp();
            branchFactory.Cleanup();
            _programFactory.Cleanup();
        }

        
    }
}
