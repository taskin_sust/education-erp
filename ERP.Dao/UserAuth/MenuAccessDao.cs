﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.UserAuth
{
    public interface IMenuAccessDao : IBaseDao<MenuAccess, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<MenuAccess> LoadActive();
        IList<MenuAccess> LoadActiveByMenuId(long menu);
        #endregion

        #region Others Function
        bool CheckedMenuAccess(Menu menu, AreaControllers controller, Actions action);
        #endregion
    }
    public class MenuAccessDao:BaseDao<MenuAccess,long>,IMenuAccessDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<MenuAccess> LoadActive()
        {
            ICriteria criteria = Session.CreateCriteria(typeof(MenuAccess));
            criteria.Add(Restrictions.Eq("Status", MenuAccess.EntityStatus.Active));
            return criteria.List<MenuAccess>();
        }
        public IList<MenuAccess> LoadActiveByMenuId(long menu)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(MenuAccess));
            criteria.Add(Restrictions.Eq("Status", MenuAccess.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("Menu.Id", menu));
            return criteria.List<MenuAccess>();
        }
        #endregion

        #region Others Function
        public bool CheckedMenuAccess(Menu menu, AreaControllers controller, Actions action)
        {
            var rowList =
                Session.QueryOver<MenuAccess>()
                    .Where(x => x.Menu == menu && x.Actions == action && x.Status == MenuAccess.EntityStatus.Active)
                    .RowCount();
            if (rowList < 1)
                return false;
            return true;
        }
        #endregion
    }
}
