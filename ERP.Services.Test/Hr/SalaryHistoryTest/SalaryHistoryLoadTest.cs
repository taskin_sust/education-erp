﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.SalaryHistoryTest
{

    [Trait("Area", "Hr")]
    [Trait("Service", "SalaryHistory")]
    public class SalaryHistoryLoadTest : SalaryHistoryBaseTest
    {
        #region GetSalaryHistory

        [Fact]
        public void Should_Return_GetSalaryHistory_LoadById_Null_Check()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.Save(_salaryHistoryFactory.Object);
            var salaryObj = _salaryHistoryService.GetSalaryHistory(_salaryHistoryFactory.Object.Id+1);
            Assert.Null(salaryObj);
        }

        [Fact]
        public void Should_Return_GetSalaryHistory_GetById()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.Save(_salaryHistoryFactory.Object);

           Assert.NotNull(_salaryHistoryService.GetSalaryHistory(_salaryHistoryFactory.Object.Id));
        }

        #endregion

        #region LoadSalaryHistories

        [Fact]
        public void Should_Return_LoadSalaryHistories_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.Save(_salaryHistoryFactory.Object);
            Assert.NotEqual(0, _salaryHistoryService.LoadSalaryHistories(teamMember).Count());
        }

        [Fact]
        public void Should_Return_LoadSalaryHistories_InvalidException()
        {
            var teamMember = PersistTeamMember(MentorId);
            var department = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            var org = teamMember.EmploymentHistory[0].Campus.Branch.Organization;
            var designation = teamMember.EmploymentHistory.FirstOrDefault().Designation;
            _salaryHistoryFactory.Create()
                .WithCampus(campus)
                .WithDepartment(department)
                .WithDesignation(designation)
                .WithOrganization(org)
                .WithTeamMember(teamMember);
            _salaryHistoryService.Save(_salaryHistoryFactory.Object);
            Assert.Throws<InvalidDataException>(() => _salaryHistoryService.LoadSalaryHistories(null));
        }
  
        #endregion
    }
}
