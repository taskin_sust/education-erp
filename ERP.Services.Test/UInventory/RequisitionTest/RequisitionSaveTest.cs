﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.RequisitionTest
{

    [Trait("Area", "UInventory")]
    [Trait("Service", "Requisition")]
    public class RequisitionSaveTest : RequisitionBaseTest
    {

        #region Basic Test

        [Fact]
        public void Invalid_Data_Exception_For_Null_UserMenu()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var refactory = _requisitionFactory.Create(orgFactory.ObjectList[0]);
            var rdFactory = _requisitionDetailsFactory.Create(orgFactory.ObjectList[0], refactory.Object);
            refactory.Object.RequisitionDetails.Add(rdFactory.Object);

            Assert.Throws<InvalidDataException>(() => _requisitionService.SaveRequisition(refactory.Object, null));
        }



        [Fact]
        public void Null_Exception_For_Null_Requisition_Object()
        {
            var orgFactory = organizationFactory.Create().Persist();
            var branchFactory = _branchFactory.Create().WithOrganization(orgFactory.Object).Persist();
            var userMenu = BuildUserMenu(new List<Organization>() { orgFactory.Object },
                                            null, new List<Branch>() { branchFactory.Object });

            Assert.Throws<NullObjectException>(() => _requisitionService.SaveRequisition(null, userMenu));
        }

        [Fact]
        public void Requisition_Should_Return_True_With_Item()
        {
            var orgFactory = organizationFactory.CreateMore(1).Persist();
            var refactory = _requisitionFactory.Create(orgFactory.ObjectList[0]);

            var rdFactory = _requisitionDetailsFactory.Create(orgFactory.ObjectList[0], refactory.Object);
            refactory.Object.RequisitionDetails.Add(rdFactory.Object);

            var userMenu = BuildUserMenu(orgFactory.ObjectList, new List<Program>() { rdFactory.Object.Program }, new List<Branch>() { refactory.Object.Branch });
            Assert.True(_requisitionService.SaveRequisition(refactory.Object, userMenu));
        }

        #endregion

        #region Business Logic

        [Fact]
        public void Null_Exception_For_RequisitionDetails()
        {
            var orgFactory = organizationFactory.Create().Persist();
            var refactory = _requisitionFactory.Create(orgFactory.Object);
            var userMenu = BuildUserMenu(orgFactory.ObjectList, new List<Program>(), new List<Branch>() { refactory.Object.Branch });
            Assert.Throws<NullObjectException>(() => _requisitionService.SaveRequisition(refactory.Object, userMenu));
        }

        [Fact]
        public void Invalid_Data_Exception_For_Purpose_Item_Null()
        {
            var orgFactory = organizationFactory.Create().Persist();
            var refactory = _requisitionFactory.Create(orgFactory.Object);
            var listFactory = _requisitionDetailsFactory.Create(1, orgFactory.Object, refactory.Object);
            listFactory.ObjectList[0].Item = null;
            refactory.Object.RequisitionDetails = listFactory.ObjectList.ToList();

            var userMenu = BuildUserMenu(new List<Organization>() { orgFactory.Object },
                                            null, new List<Branch>() { refactory.Object.Branch });
            Assert.Throws<InvalidDataException>(() => _requisitionService.SaveRequisition(refactory.Object, userMenu));
        }

        [Fact]
        public void Invalid_Data_Exception_For_Purpose_Quantity_Invalid()
        {
            var orgFactory = organizationFactory.Create().Persist();
            var refactory = _requisitionFactory.Create(orgFactory.Object);
            var listFactory = _requisitionDetailsFactory.Create(1, orgFactory.Object, refactory.Object);
            listFactory.ObjectList[0].RequiredQuantity = 0;
            refactory.Object.RequisitionDetails = listFactory.ObjectList.ToList();

            var userMenu = BuildUserMenu(new List<Organization>() { orgFactory.Object },
                                            null, new List<Branch>() { refactory.Object.Branch });
            Assert.Throws<InvalidDataException>(() => _requisitionService.SaveRequisition(refactory.Object, userMenu));
        }

        [Fact]
        public void Invalid_Data_Exception_For_ProgramSession_Null()
        {
            var orgFactory = organizationFactory.Create().Persist();
            var refactory = _requisitionFactory.Create(orgFactory.Object);
            var listFactory = _requisitionDetailsFactory.Create(1, orgFactory.Object, refactory.Object);
            listFactory.ObjectList[0].Program = null;
            listFactory.ObjectList[0].Session = null;
            refactory.Object.RequisitionDetails = listFactory.ObjectList.ToList();

            var userMenu = BuildUserMenu(new List<Organization>() { orgFactory.Object },
                                            null, new List<Branch>() { refactory.Object.Branch });
            Assert.Throws<InvalidDataException>(() => _requisitionService.SaveRequisition(refactory.Object, userMenu));
        }

        [Fact]
        public void Duplicate_Entry_Exception_For_Requisition()
        {
            var orgFactory = organizationFactory.Create().Persist();
            var branchFactory = _branchFactory.Create().WithOrganization(orgFactory.Object).Persist();
            
            var factoryObj1 = _requisitionFactory.CreateWithBranch(orgFactory.Object, branchFactory.Object);
            var listFactory = _requisitionDetailsFactory.Create(2, orgFactory.Object, factoryObj1.Object);
            factoryObj1.Object.RequisitionDetails = listFactory.ObjectList.ToList();

            var userMenu = BuildUserMenu(orgFactory.ObjectList, listFactory.ObjectList.Select(x => x.Program).ToList(), new List<Branch>() { _branchFactory.Object });

            _requisitionService.SaveRequisition(factoryObj1.Object, userMenu);

            var factoryObj2 = _requisitionFactory.CreateWithBranch(orgFactory.Object, branchFactory.Object);
            var listFactory1 = _requisitionDetailsFactory.Create(2, orgFactory.Object, factoryObj1.Object);
            factoryObj2.Object.RequisitionDetails = listFactory1.ObjectList.ToList();
            factoryObj2.Object.RequisitionNo = factoryObj1.SingleObjectList[0].RequisitionNo;

            Assert.Throws<DuplicateEntryException>(() => _requisitionService.SaveRequisition(factoryObj1.Object, userMenu));
        }

        #endregion
    }
}
