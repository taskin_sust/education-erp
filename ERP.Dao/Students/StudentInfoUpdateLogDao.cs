﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentInfoUpdateLogDao : IBaseDao<StudentInfoUpdateLog, long>
    {
        #region Operational Function
        void SaveLog(StudentInfoUpdateLog obj);
        #endregion


        #region List Loading Functions
        IList<StudentInfoEditLogDto> LoadEditedStudentsInfo(int start, int length, List<long> authorizedProgramLists, List<long> branchIdList, long programId,
            long sessionId,
            long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId,
            int version, int gender, int[] religion, DateTime? startDate, DateTime? endDate, int selectedStatus, List<long> batchIdList,
            string prnNo,
            string name, string mobile);
        IList<StudentInfoUpdateLog> LoadIndividualEditedStudentsInfo(int start, int length, List<BusinessModel.Entity.UserAuth.UserMenu> _userMenu, DateTime startDate, DateTime endDate, long studentId); 
        #endregion
       
        #region Others Function
        int GetEditedStudentsCount(List<long> authorizedProgramLists, List<long> branchIdList, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, DateTime? startDate, DateTime? endDate, int selectedStatus, List<long> batchIdList, string prnNo, string name, string mobile);
        int GetIndividualEditCount(List<BusinessModel.Entity.UserAuth.UserMenu> _userMenu, DateTime startDate, DateTime endDate, long studentId);
        #endregion
        
        
    }

    public class StudentInfoUpdateLogDao:BaseDao<StudentInfoUpdateLog,long>,IStudentInfoUpdateLogDao
    {
        #region Operational Function
        public void SaveLog(StudentInfoUpdateLog obj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    Session.Save(obj);
                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
            }
        }
        #endregion

        #region List Loading Functions
        public IList<StudentInfoEditLogDto> LoadEditedStudentsInfo(int start, int length, List<long> authorizedProgramLists, List<long> branchIdList, long programId, long sessionId,
            long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId,
            int version, int gender, int[] religion, DateTime? startDate, DateTime? endDate, int selectedStatus, List<long> batchIdList, string prnNo,
            string name, string mobile)
        {
            string innerQuery = GetInnerQueryEditedStudentList(authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus, batchIdList, prnNo, name, mobile, start, length);
            string tempTableQuery = @"IF OBJECT_ID('tempdb..##StudentProgramIdTempTable') IS NOT NULL
                                      DROP TABLE ##StudentProgramIdTempTable
                                      CREATE TABLE ##StudentProgramIdTempTable(studentProgramId bigint) 
                                      INSERT INTO ##StudentProgramIdTempTable(studentProgramId)
                                      select 
		                              sp.Id
		                              from StudentProgram sp ";
            string innerWhereClause = "where programId=" + programId;
            if (!(Array.Exists(courseId, item => item == 0)))
            {
                innerWhereClause += " and exists(" +
                          " select 1 from StudentCourseDetails scd inner join CourseSubject cs on scd.CourseSubjectId=cs.Id" +
                          " where cs.Status = 1 and scd.StudentProgramId=sp.id";
                innerWhereClause += " and cs.courseId in (" + string.Join(",", courseId) + ") ";
                innerWhereClause += ") ";
            }

            if (!String.IsNullOrEmpty(prnNo))
            {
                innerWhereClause += " and sp.PrnNo like '%" + prnNo + "%'";
            }

            tempTableQuery += innerWhereClause;
            string query = "SELECT * FROM(" + innerQuery;

            query += " ) as A";
            query += @" left join   (
                                    select 
                                    StudentProgramId,
		                            Institute PrevInstitute,
		                            NickName PrevNickName,
                                    FullName PrevFullName,
                                    Mobile PrevMobile,
                                    GuardiansMobile1 PrevGuardiansMobile1,
                                    GuardiansMobile2 PrevGuardiansMobile2,
                                    Gender PrevGender,
                                    Religion PrevReligion,
                                    Email PrevEmail,
                                    RegistrationNo PrevRegistrationNo,
                                    FatherName PrevFatherName
		                            from StudentInfoUpdateLog sul where sul.InfoStatus=1
                                    ) AS B
                                    on A.Id = B.StudentProgramId";
            if (length == 0)
            {
                query += " order by A.PrnNo";
            }
            IQuery iQuery = Session.CreateSQLQuery(tempTableQuery + query).SetTimeout(2700);
            iQuery.SetResultTransformer(Transformers.AliasToBean<StudentInfoEditLogDto>());
            var list = iQuery.List<StudentInfoEditLogDto>().ToList();
            return list;
        }

        public IList<StudentInfoUpdateLog> LoadIndividualEditedStudentsInfo(int start, int length,
            List<BusinessModel.Entity.UserAuth.UserMenu> _userMenu, DateTime startDate, DateTime endDate, long studentId)
        {
            IList<StudentInfoUpdateLog> studentEditinfo = new List<StudentInfoUpdateLog>();
            var individualEditedStudentsInfo =
                Session.QueryOver<StudentInfoUpdateLog>()
                    .Where(
                        x =>
                            x.CreationDate >= startDate && x.CreationDate <= endDate.AddDays(1) &&
                            x.StudentId == studentId);
            if (length > 0)
            {
                var individualEditedStudentsInfoPaged = individualEditedStudentsInfo.Skip(start).Take(length);
                studentEditinfo = individualEditedStudentsInfoPaged.List<StudentInfoUpdateLog>();
            }
            else
            {
                studentEditinfo = individualEditedStudentsInfo.List<StudentInfoUpdateLog>();
            }
            return studentEditinfo;
        } 
        #endregion

        
        #region Others Function
        public int GetEditedStudentsCount(List<long> authorizedProgramLists, List<long> branchIdList, long programId, long sessionId,
            long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId,
            int version, int gender, int[] religion, DateTime? startDate, DateTime? endDate, int selectedStatus, List<long> batchIdList, string prnNo,
            string name, string mobile)
        {
            string innerQuery = GetInnerQueryEditedStudentList(authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus,batchIdList, prnNo, name, mobile);
            string tempTableQuery = @"IF OBJECT_ID('tempdb..##StudentProgramIdTempTable') IS NOT NULL
                                      DROP TABLE ##StudentProgramIdTempTable
                                      CREATE TABLE ##StudentProgramIdTempTable(studentProgramId bigint) 
                                      INSERT INTO ##StudentProgramIdTempTable(studentProgramId)
                                      select 
		                              sp.Id
		                              from StudentProgram sp ";
            string innerWhereClause = "where programId="+programId;
            if (!(Array.Exists(courseId, item => item == 0)))
            {
                innerWhereClause += " and exists(" +
                          " select 1 from StudentCourseDetails scd inner join CourseSubject cs on scd.CourseSubjectId=cs.Id" +
                          " where cs.Status = 1 and scd.StudentProgramId=sp.id";
                innerWhereClause += " and cs.courseId in (" + string.Join(",", courseId) + ") ";
                innerWhereClause += ") ";
            }
            

            //if (startDate != null)
            //{
            //    innerWhereClause += " and sp.CreationDate >= '" + startDate + "'";
            //}
            //if (endDate != null)
            //{
            //    innerWhereClause += " and sp.CreationDate <= '" + endDate.Value.AddDays(1) + "'";
            //}
            

            if (!String.IsNullOrEmpty(prnNo))
            {
                innerWhereClause += " and sp.PrnNo like '%" + prnNo + "%'";
            }
            
            tempTableQuery += innerWhereClause;
            string query = "SELECT count(A.Id) as total FROM(" + innerQuery + ") as A";
            IQuery iQuery = Session.CreateSQLQuery(tempTableQuery+query);
            iQuery.SetTimeout(2000);
            return Convert.ToInt32(iQuery.UniqueResult());
        }
        public int GetIndividualEditCount(List<BusinessModel.Entity.UserAuth.UserMenu> _userMenu, DateTime startDate, DateTime endDate,
            long studentId)
        {
            return Session.QueryOver<StudentInfoUpdateLog>().Where(x => x.CreationDate >= startDate && x.CreationDate <= endDate.AddDays(1) && x.StudentId == studentId).RowCount();
        }
        #endregion

        #region Helper Function
        private string GetInnerQueryEditedStudentList(List<long> authorizedProgramLists,
            List<long> authorizedBranchLists, long programId, long sessionId, long[] branchId, long[] campusId,
            string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender,
            int[] religion, DateTime? startDate, DateTime? endDate, int selectedStatus, List<long> batchIdList, string prnNo, string name, string mobile, int? start = null, int? length = null)
        {
            
            string query =
                " SELECT * FROM(SELECT sp.Id, sp.PrnNo,sp.VersionOfStudy,sp.StudentId, sp.InstituteId, b.Name as Batch, br.Name as Branch," +
                " st.NickName,st.FullName,st.Mobile,st.GuardiansMobile1,st.GuardiansMobile2,st.Gender,st.Religion,st.Email,st.RegistrationNo,st.FatherName," +
                //" (count(sp.Id)-1) as totalEdited " +
                " sum((case when sul.InfoStatus = 2 then 1 else 0 end)) as totalEdited"+
                " from StudentInfoUpdateLog sul" +
                " inner join StudentProgram sp on sp.Id = sul.StudentProgramId " +
                " inner join Program p on p.Id = sp.ProgramId" +
                " inner join Student st on st.Id = sp.StudentId" +
                " inner join Batch b on b.Id = sp.BatchId" +
                " inner join Branch br on br.Id = b.BranchId" +
                " inner join [Session] s on s.Id = b.SessionId" +
                " inner join Campus c on c.Id = b.CampusId" +
                @" inner join ##StudentProgramIdTempTable stt
			    on sp.id=stt.studentProgramId";
            string whereClause = " Where " +
                                 " st.Status = 1 and" +
                                 " p.Status = 1 and" +
                                 " b.Status = 1 and" +
                                 " br.Status = 1 and" +
                                 " s.Status = 1 and" +
                                 " c.Status = 1 ";
            if (startDate != null)
            {
                whereClause += " and sul.CreationDate >= '" + startDate + "'";
            }
            if (endDate != null)
            {
                whereClause += " and sul.CreationDate <= '" + endDate.Value.AddDays(1) + "'";
            }
            whereClause += " and s.Id = " + sessionId + " and p.Id = " + programId;
            if (authorizedProgramLists != null)
            {
                whereClause += " and p.Id in(" + string.Join(",", authorizedProgramLists.ToArray()) + ") ";
            }

            if (authorizedBranchLists != null)
            {
                whereClause += " and br.Id in(" + string.Join(",", authorizedBranchLists.ToArray()) + ") ";
            }
            
            #region MyRegion
            

            //if (!(Array.Exists(branchId, item => item == 0)))
            //{
            //    whereClause += " and br.Id in(" + string.Join(",", branchId) + ") ";
            //}
            //if (!(Array.Exists(campusId, item => item == 0)))
            //{
            //    whereClause += " and c.Id in(" + string.Join(",", campusId) + ") ";
            //}

            //if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            //{

            //    whereClause += " and b.Days in ('" + string.Join("','", batchDays) + "')";
            //}
            //if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            //{
            //    string btQ = "";
            //    int iteration = 0;
            //    foreach (string bt in batchTime)
            //    {
            //        var batchArrays = bt.Replace("To", ",");
            //        var batchArray = batchArrays.Split(',');
            //        var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
            //        var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
            //        if (iteration == 0)
            //        {
            //            btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
            //        }
            //        else
            //        {
            //            btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
            //        }
            //        iteration++;
            //    }
            //    whereClause += " and (" + btQ + ")";
            //}

            //if (!(Array.Exists(batchId, item => item == 0)))
            //{
            //    whereClause += " and b.Id in (" + string.Join(",", batchId) + ")";
            //} 
            #endregion
              

            #region MyRegion
            //if (version != SelectionType.SelelectAll)
            //{
            //    whereClause += " and sp.VersionOfStudy = " + version;
            //}
            //if (gender != SelectionType.SelelectAll)
            //{
            //    whereClause += " and st.Gender = " + gender;
            //} 
            #endregion

            if (!String.IsNullOrEmpty(name))
            {
                whereClause += " and st.NickName like '%" + name + "%'";
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                whereClause += " and st.Mobile like '%" + mobile + "%'";
            }
            if (!(Array.Exists(religion, item => item == 0)))
            {
                whereClause += " and st.Religion in (" + string.Join(",", religion) + ")";
            }
            whereClause += " and b.Id in (" + string.Join(",", batchIdList) + ")";
            query += whereClause;
            
                query +=
                " group by sp.Id, sp.PrnNo,sp.VersionOfStudy,sp.StudentId, sp.InstituteId, b.Name, br.Name, st.NickName,st.FullName,st.Mobile," +
                " st.GuardiansMobile1,st.GuardiansMobile2,st.Gender,st.Religion,st.Email,st.RegistrationNo,st.FatherName) AS A WHERE 1=1";
            
            switch (selectedStatus)
            {
                case 1:
                    query += " and A.totalEdited>0";
                    break;
                case 2:
                    query += " and A.totalEdited=0";
                    break;
            }
            if (start != null && length != null)
            {
                
                if (length > 0)
                {
                    query +=
                    " order by A.PrnNo ";
                    query += " offset " + start + " rows fetch next " + length + " rows only";

                }
                //query += ") AS A ";
            }
            return query;
        }
        #endregion
    }
}
