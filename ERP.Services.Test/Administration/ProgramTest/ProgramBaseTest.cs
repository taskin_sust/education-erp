﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Students;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.AdminisTration.ProgramTest
{
    public class ProgramBaseTest : TestBase, IDisposable
    {
        #region Object Initialization
        internal readonly CommonHelper CommonHelper;
        internal ProgramService _programService;
        internal BranchService _branchService;
        internal SessionService _sessionService;
        internal OrganizationService _organizationService;
        internal List<long> IdList = new List<long>();
        internal ISession _session;
        internal ProgramFactory _programFactory;
        internal BranchFactory _branchFactory;
        internal SessionFactory _sessionFactory;
        internal readonly string _programName = Guid.NewGuid() + "_(ABC CDE FGH IJK LMN OPQ RST UVW XYZ)";
        internal readonly string _shortName = Guid.NewGuid() + "_AB";
        //internal readonly string _code;
        internal const long OrgId = 1;
        internal DateTime _startTime = DateTime.Now.AddDays(1).AddHours(1);
        internal DateTime _endTime = DateTime.Now.AddDays(1).AddHours(10);
        internal int _paidProgram = (int)Program.ProgramTypeStatus.Paid;
        internal int _unpaidProgram = (int)Program.ProgramTypeStatus.UnPaid;
        internal int _csrProgram = (int)Program.ProgramTypeStatus.Csr;
        internal StudentExamService _studentExamService;
        internal ProgramBranchSessionService _programBranchSessionService;
        internal TestBaseService<ProgramBranchSession> _TestBaseService;
        #endregion

        public ProgramBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _programFactory = new ProgramFactory(null, _session);
            _branchFactory = new BranchFactory(null, _session);
            _sessionFactory = new SessionFactory(null, _session);
            _programService = new ProgramService(_session);
            _branchService = new BranchService(_session);
            _sessionService = new SessionService(_session);
            _organizationService = new OrganizationService(_session);
            _studentExamService = new StudentExamService(_session);
            _programBranchSessionService = new ProgramBranchSessionService(_session);
            _TestBaseService = new TestBaseService<ProgramBranchSession>(_session);
            //_code = _programFactory._code;
        }

        public ISession TestSession
        {
            get { return _session; }
            set { _session = value; }
        }

        public Program AssignProgram()
        {
            var organization = organizationFactory.Create().Persist();
            var sessionObj = _sessionFactory.Create().Persist();
            var programFactory = _programFactory.Create().WithOrganization(organization.Object).WithProperties(_programName, _shortName, _programFactory.GetRandomNumber(), _paidProgram).Persist();
            int?[] admissiontype = { 1 };

            List<long> organizationIds = new List<long> { organization.Object.Id };
            _branchFactory.Create().WithOrganization(organization.Object).Persist();

            var existingBranch = _branchService.LoadBranch(organizationIds);
            List<long> branchList = existingBranch.Select(c => c.Id).Distinct().ToList();
            long[] branchIds = branchList.ToArray();
            var studentExams = _studentExamService.LoadBoardExam(true);
            int?[] examIds = studentExams.Select(x => (int?)Convert.ToInt32(x.Id)).ToArray();
            List<long> nonDeletableBranchIds;
            _programBranchSessionService.AssignProgram(organization.Object.Id, programFactory.Object.Id, sessionObj.Object.Id, branchIds, admissiontype, examIds, out nonDeletableBranchIds);
            _session.Evict(_programFactory.Object);
            Program program = _programService.GetProgram(_programFactory.Object.Id);
            return program;
        }


        public void CleanUpProgramBranchSession(bool singleObject)
        {
            if (singleObject)
                _TestBaseService.DeleteProgramBranchSession(_programFactory.Object.Id, _sessionFactory.Object.Id, _branchFactory.SingleObjectList.Select(x => x.Id).ToArray());
            _TestBaseService.DeleteProgramBranchSession(_programFactory.Object.Id, _sessionFactory.Object.Id, _branchFactory.ObjectList.Select(x => x.Id).ToArray());
        }
        public void CleanUpProgramStudentExamSession(bool singleObject)
        {
            if (singleObject)
                _TestBaseService.DeleteProgramStudentExamSession(_programFactory.Object.Id, _sessionFactory.Object.Id);
           // _TestBaseService.DeleteProgramStudentExamSession(_programFactory.Object.Id, _sessionFactory.Object.Id);
        }

        public void Dispose()
        {
            if (_programFactory.Object != null && _sessionFactory.Object != null)
            {
                if (_branchFactory.SingleObjectList != null && _branchFactory.SingleObjectList.Count > 0)
                {
                    CleanUpProgramBranchSession(true);
                    CleanUpProgramStudentExamSession(true);
                }


                if (_branchFactory.ObjectList != null && _branchFactory.ObjectList.Count > 0)
                {
                    CleanUpProgramBranchSession(false);
                    CleanUpProgramStudentExamSession(true);
                }

            }
            _programFactory.Cleanup();
            _branchFactory.Cleanup();
            _sessionFactory.Cleanup();
            base.Dispose();

        }
    }
}
