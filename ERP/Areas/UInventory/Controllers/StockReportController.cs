﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.UInventory.ReportsViewModel;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UInventory;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Areas.UInventory.Controllers
{
    [UerpArea("UInventory")]
    [Authorize]
    [AuthorizeAccess]
    public class StockReportController : Controller
    {
        //
        // GET: /UInventory/StockReport/
        #region logger

        private readonly ILog _logger = LogManager.GetLogger("UInventoryArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly IItemService _itemService;
        private readonly IItemGroupService _itemGroupService;
        private readonly IStockReportService _stockReportService;
        private readonly IBranchService _branchService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;

        public StockReportController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
            _itemService = new ItemService(session);
            _itemGroupService = new ItemGroupService(session);
            _stockReportService = new StockReportService(session);
            _branchService = new BranchService(session);
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
        }

        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region Report

        public ActionResult Report()
        {
            try
            {
                Initialize();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Report(StockReportViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (model.OrganizationId <= 0)
                    {
                        throw new InvalidDataException("Organization is not defined!");
                    }
                    if (model.BranchId <= 0)
                    {
                        throw new InvalidDataException("Branch is not defined!");
                    }
                    InitialReportHeaderDiv(model);
                    if (model.ShowReportType == (int)StockScheduleReportType.Summary)
                    {
                        return PartialView("_partial/_StockSummaryReport", model);
                    }
                    if (model.ShowReportType == (int)StockScheduleReportType.Details)
                    {
                        return PartialView("_partial/_StockDetailsSummaryReport", model);
                    }
                }
                catch (InvalidDataException ex)
                {
                    _logger.Error(ex);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                }
            }
            return View(model);
        }

        #endregion

        #region Data Table Render

        [HttpPost]
        public ActionResult ScheduleReport(int draw, int start, int length, long organizationId, long[] branchIds
                                                        , int[] purposeIds, long[] itemGroupIds, long[] itemIds, string[] programSession
                                                        , DateTime dateFrom, DateTime dateTo)
        {
            int recordsFiltered = 0;
            int recordsTotal = 0;
            bool isSuccess = true;

            var getData = new List<object>();
            try
            {
                const string orderBy = "ItemGroup";
                const string orderDir = "desc";


                IList<dynamic> queryResult = _stockReportService.StockSummaryReport(start, length, orderBy, orderDir, (List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds[0], purposeIds
                                                                                        , programSession, itemGroupIds, itemIds, dateFrom, dateTo).ToList();
                recordsTotal = _stockReportService.StockSummaryReportRowCount((List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds[0], purposeIds, programSession, itemGroupIds, itemIds, dateFrom, dateTo);
                
                int sl = start + 1;
                foreach (var obj in queryResult)
                {
                    int index = 1;
                    var str = new List<string>();
                    str.Add(sl.ToString(CultureInfo.InvariantCulture));
                    foreach (var entry in obj)
                    {
                        if (index > 4)
                        {
                            str.Add((entry != null) ? entry.ToString() : "0");
                        }
                        else
                        {
                            str.Add((entry != null) ? entry.ToString() : "N/A");
                        }
                        index++;
                    }
                    getData.Add(str);
                    sl++;
                }
                recordsFiltered = recordsTotal;
            }
            catch (NullObjectException ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        [HttpPost]
        public ActionResult ScheduleDetailsReport(int draw, int start, int length, long organizationId, long[] branchIds
                                                        , int[] purposeIds, long[] itemGroupIds, long[] itemIds, string[] programSession
                                                        , DateTime dateFrom, DateTime dateTo)
        {
            int recordsFiltered = 0;
            int recordsTotal = 0;
            bool isSuccess = true;

            var getData = new List<object>();
            try
            {
                IList<dynamic> queryResult = _stockReportService.StockSummaryDetailsReport(start, length, (List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds[0], purposeIds
                                                                                        , programSession, itemGroupIds, itemIds, dateFrom, dateTo).ToList();
                recordsTotal = _stockReportService.StockSummaryDetailsReportRowCount((List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds[0], purposeIds, programSession, itemGroupIds, itemIds, dateFrom, dateTo);
                int sl = start + 1;
                foreach (var obj in queryResult)
                {
                    int index = 1;
                    var str = new List<string>();
                    str.Add(sl.ToString(CultureInfo.InvariantCulture));
                    foreach (var entry in obj)
                    {
                        if (index > 4)
                        {
                            str.Add((entry != null) ? entry.ToString() : "0");
                        }
                        else
                        {
                            str.Add((entry != null) ? entry.ToString() : "N/A");
                        }
                        index++;
                    }
                    getData.Add(str);
                    sl++;
                }

                recordsFiltered = recordsTotal;
            }
            catch (NullObjectException ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                isSuccess = false;
                recordsFiltered = 0;
                recordsTotal = 0;
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = getData,
                isSuccess = isSuccess
            });
        }

        #endregion

        #region Export

        public ActionResult ExportScheduleDetailsReport(long organizationId, long[] branchIds, int reportType, int[] purposeIds, long[] itemGroupIds, long[] itemIds,
                                                    string[] programSession, int[] requisitionStatus, DateTime fromDate, DateTime toDate)
        {

            try
            {
                var organization = _organizationService.LoadById(organizationId);
                var branchName = _branchService.LoadBranch(branchIds).Select(x => x.Name).ToList();
                var headerContent = GetHeader(purposeIds, itemGroupIds, itemIds, programSession);
                var headerList = new List<string>()
                {
                    organization.Name,
                     branchName[0],
                    "Stock " + GetEnumDescription.Description((StockScheduleReportType)reportType),
                    "Purpose: " + headerContent[0],
                    "Item Group(s): " + headerContent[1],
                    "Item(s): " + headerContent[2],
                    "Program & Session: " + headerContent[3],
                    fromDate.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture) 
                    + " To " + toDate.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture)
                };

                var footerList = new List<string>() { "" };

                var columnList = new List<string>()
                {
                    "Item Group", "Item Name", "Program & Session", 
                    "Opening Quantity", "Opening Rate", "Opening Value", 
                    "Receive Quantity", "Receive Rate", "Receive Value", 
                    "Transfer In Quantity", "Transfer In Rate", "Transfer In Value", 
                    "Issued Quantity", "Issued Rate", "Issued Value", 
                    "Returned Quantity", "Returned Rate", "Returned Value",
                    "Transfer Out Quantity", "Transfer Out Rate", "Transfer Out Value", 
                    "Cloasing Quantity", "Closing Rate", "Closing Value"
                };

                int recordsTotal = _stockReportService.StockSummaryDetailsReportRowCount((List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds[0]
                                                                                        , purposeIds, programSession, itemGroupIds, itemIds, fromDate, toDate);
                if (recordsTotal < 1)
                {
                    return View("AutoClose");
                }

                IList<dynamic> objectList = _stockReportService.StockSummaryDetailsReport(0, recordsTotal, (List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds[0], purposeIds
                                                                                        , programSession, itemGroupIds, itemIds, fromDate, toDate).ToList();

                var excelList = new List<List<object>>();
                foreach (var obj in objectList)
                {
                    int index = 0;
                    var xlsRow = new List<object>();
                    foreach (var entry in obj)
                    {
                        if (index > 4)
                        {
                            xlsRow.Add(entry ?? 0);
                        }
                        else
                        {
                            xlsRow.Add((entry != null) ? entry.ToString() : "-");
                        }
                        index++;
                    }
                    excelList.Add(xlsRow);
                }
                string fileName = "DetailsStockReport_" + DateTime.Now.ToString("yyyyMMdd-HHmmss");
                ExcelGenerator.GenerateExcel(headerList, columnList, excelList, footerList, fileName);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View("AutoClose");
        }

        public ActionResult ExportSummaryReport(long organizationId, long[] branchIds, int reportType, int[] purposeIds, long[] itemGroupIds, long[] itemIds,
                                                    string[] programSession, int[] requisitionStatus, DateTime fromDate, DateTime toDate)
        {

            try
            {
                const string orderBy = "ItemGroup";
                const string orderDir = "desc";
                var organization = _organizationService.LoadById(organizationId);
                var branchName = _branchService.LoadBranch(branchIds).Select(x => x.Name).ToList();
                var headerContent = GetHeader(purposeIds, itemGroupIds, itemIds, programSession);
                var headerList = new List<string>()
                {
                    organization.Name,
                     branchName[0],
                    "Stock " + GetEnumDescription.Description((StockScheduleReportType)reportType),
                    "Purpose: " + headerContent[0],
                    "Item Group(s): " + headerContent[1],
                    "Item(s): " + headerContent[2],
                    "Program & Session: " + headerContent[3],
                    fromDate.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture) + " To " + toDate.ToString("MMM dd, yyyyy", CultureInfo.InvariantCulture)
                };

                var footerList = new List<string>() { "" };

                var columnList = new List<string>()
                {
                    "Item Group", "Item Name", "ProgramSession", 
                    "OpeningQuantity", "OpeningRate", "OpeningValue", 
                    "ReceiveQuantity", "ReceiveRate", "ReceiveValue",
                    "IssuedQuantity", "IssuedRate", "IssuedValue",
                    "CloasingQuantity", "ClosingRate", "ClosingValue"
                };

                int recordsTotal = _stockReportService.StockSummaryReportRowCount((List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds[0], purposeIds, programSession, itemGroupIds, itemIds, fromDate, toDate);
                if (recordsTotal < 1)
                {
                    return View("AutoClose");
                }

                IList<dynamic> objectList = _stockReportService.StockSummaryReport(0, recordsTotal, orderBy, orderDir, (List<UserMenu>)ViewBag.UserMenu, organizationId, branchIds[0], purposeIds
                                                                                        , programSession, itemGroupIds, itemIds, fromDate, toDate).ToList();

                var excelList = new List<List<object>>();
                foreach (var obj in objectList)
                {
                    int index = 0;
                    var xlsRow = new List<object>();
                    foreach (var entry in obj)
                    {
                        if (index > 4)
                        {
                            xlsRow.Add(entry ?? 0);
                        }
                        else
                        {
                            xlsRow.Add((entry != null) ? entry.ToString() : "-");
                        }
                        index++;
                    }
                    excelList.Add(xlsRow);
                }
                string fileName = "StockReport_" + DateTime.Now.ToString("yyyyMMdd-HHmmss");
                ExcelGenerator.GenerateExcel(headerList, columnList, excelList, footerList, fileName);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View("AutoClose");
        }

        #endregion

        #region Helper

        private void Initialize()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var authorgList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");

            ViewBag.organizationList = authorgList;
            ViewBag.branchIdList = new SelectList(string.Empty, "Value", "Text");

            var list = _itemGroupService.LoadItemGroup(0, int.MaxValue, "", "", "", ItemGroup.EntityStatus.Active.ToString());
            list.Insert(0, new ItemGroup() { Id = 0, Name = "All Selected" });
            ViewBag.itemGroupList = new SelectList(list, "Id", "Name");

            ViewBag.itemList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.programSessionList = new SelectList(string.Empty, "Value", "Text");

            var purposeList = (new Dictionary<int, string> { { 0, "Select All" } }).Concat(_commonHelper.LoadEmumToDictionary<Purpose>()).ToDictionary(k => k.Key, v => v.Value);
            ViewBag.PurposeList = new SelectList(purposeList.ToList().OrderBy(x => x.Key), "Key", "Value");

            var reportType = _commonHelper.LoadEmumToDictionary<StockScheduleReportType>();
            ViewBag.reportType = new SelectList(reportType, "key", "value");
        }

        private void InitialReportHeaderDiv(StockReportViewModel model)
        {
            #region Report Title

            string reportTypeText = "";
            switch (model.ShowReportType)
            {
                case (int)StockScheduleReportType.Summary:
                    reportTypeText = "Stock Schedule Summary Report";
                    break;
                case (int)StockScheduleReportType.Details:
                    reportTypeText = "Stock Schedule Details Summary Report";
                    break;
                default:
                    reportTypeText = "";
                    break;
            }

            var organization = _organizationService.LoadById(model.OrganizationId);
            var branchName = _branchService.LoadBranch(null, null, null, _commonHelper.ConvertIdToList(model.BranchId)).Select(x => x.Name).ToList();
            var reportTitle = new string[4];
            reportTitle[0] = organization.Name;
            reportTitle[1] = branchName[0];
            reportTitle[2] = reportTypeText;
            reportTitle[3] = model.DateFrom.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture) 
                            + " To " + model.DateTo.Date.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture);

            #endregion

            #region Program & Session List

            var programSession = "";
            if (model.ProgramSession != null && model.ProgramSession[0] != "0")
            {
                var psNamelist = GetProgramSessionNameList(model.ProgramSession);
                programSession = string.Join(",", psNamelist);
            }
            else if (model.ProgramSession == null || model.ProgramSession[0] == "0")
            {
                programSession = "All";
            }
            #endregion

            #region Purpose List

            var purpose = "All";
            if (model.Purpose != null && model.Purpose[0] > 0)
            {
                purpose = model.Purpose.Aggregate("", (current, purposeId) => current + (GetEnumDescription.Description((Purpose)purposeId) + ", "));
                purpose = purpose.TrimEnd(',');
            }

            #endregion

            var itemGroupString = "";
            if (model.ItemGroupId != null && model.ItemGroupId[0] > 0)
            {
                itemGroupString = string.Join(", ", _itemGroupService.LoadItemGroup(model.ItemGroupId.ToList()).Select(x => x.Name).ToList());
            }
            else if (model.ItemGroupId == null || model.ItemGroupId[0] == 0)
            {
                itemGroupString = "All";
            }
            var itemString = "";
            if (model.ItemId != null && model.ItemId[0] > 0)
            {
                itemString = string.Join(", ", string.Join(", ", _itemService.LoadItemList(model.ItemId).Select(x => x.Name).ToList()));
            }
            else if (model.ItemId == null || model.ItemId[0] == 0)
            {
                itemString = "All";
            }
            ViewBag.ReportTitle = reportTitle;
            ViewBag.Purposelbl = purpose;
            ViewBag.ProgramSessionlbl = programSession;
            ViewBag.ItemGrouplbl = itemGroupString;
            ViewBag.Itemlbl = itemString;
        }

        private IEnumerable<string> GetProgramSessionNameList(IEnumerable<string> programSession)
        {
            IList<string> programSessionNameList = new List<string>();

            foreach (var ps in programSession)
            {
                string[] psIds = ps.Split(new string[] { "::" }, StringSplitOptions.None);
                if (psIds.Length == 2)
                {
                    string programName = _programService.GetProgram(Convert.ToInt64(psIds[0])).ShortName;
                    string sessionName = _sessionService.LoadById(Convert.ToInt64(psIds[1])).Name;
                    if (!string.IsNullOrWhiteSpace(programName) && !string.IsNullOrWhiteSpace(sessionName))
                    {
                        programSessionNameList.Add(programName + " " + sessionName);
                    }
                }
                else
                {
                    programSessionNameList.Add("N/A");
                }
            }
            return programSessionNameList.Distinct().ToList();
        }

        private IList<string> GetHeader(int[] purposeIds, long[] itemGroupIds, long[] itemIds,
                                                    string[] programSessionArray)
        {
            var programSession = "";
            if (programSessionArray != null && programSessionArray[0] != "0")
            {
                var psNamelist = GetProgramSessionNameList(programSessionArray);
                programSession = string.Join(",", psNamelist);
            }
            else if (programSessionArray == null || programSessionArray[0] == "0")
            {
                programSession = "All";
            }
            var purpose = "All";
            if (purposeIds != null && purposeIds[0] > 0)
            {
                purpose = purposeIds.Aggregate("", (current, purposeId) => current + (GetEnumDescription.Description((Purpose)purposeId) + ", "));
                purpose = purpose.TrimEnd(',');
            }
            var itemGroupString = "All";
            if (itemGroupIds != null && itemGroupIds[0] > 0)
            {
                itemGroupString = string.Join(", ", _itemGroupService.LoadItemGroup(itemGroupIds.ToList()).Select(x => x.Name).ToList());
            }
            var itemString = "All";
            if (itemIds != null && itemIds[0] > 0)
            {
                itemString = string.Join(", ", string.Join(", ", _itemService.LoadItemList(itemIds).Select(x => x.Name).ToList()));
            }
            return new List<string>()
            {
                purpose,
                itemGroupString,
                itemString,
                programSession
            };
        }
        #endregion
    }
}