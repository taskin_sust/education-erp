﻿
/*Written By Taskin*/
$(document).ready(function () {

    //var successMessage = '<div style="text-align: center;"class="alert alert-success"><a class="close" data-dismiss="alert">×</a><strong>Success!</strong> Discount Add Successfully</div>';
    //var errorMessage = '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a><span class="errorPurpose"> Failed ! </span>  </div>';
    //var paymentErrorMsg = '<p class="errorMsg" style="color: #F00"> Field Value Missing </p>';

    
    var ajaxError = "Slow Internet Connection";

    function Discount() {
        this.Id = 0;
        this.ProgramId = 0;
        this.SessionId = 0;
        this.DiscountAmount = 0;
        this.StartTime = "";
        this.EndTime = "";
        this.Status = "";
        this.DiscountObjProperty = [];
        this.Discount = function () { };
    };
    function DiscountCourse() {
        this.Id = 0;
        this.courseId = 0;
        this.minSub = 0;
        this.DiscountCourse = function () { };
    };
    /*Get Session Against Program*/
    //$("#Organization").change(function () {
    $(document).on('change', '#Organization', function (event) {
        $('#Program').empty();
        $('#Program').append("<option value=''>Select Program</option>");
        $('#Session').empty();
        $('#Session').append("<option value=''>Select Session</option>");
        var organizationId = $(this).val();
        if (organizationId.length > 0) {
            $.ajax({
                type: "POST",
                //url: "/Administration/Discount/GetProgram",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                data: { organizationIds: organizationId, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    //$('#Program').html(data);
                    //$.unblockUI();
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnProgramList, function (i, v) {
                            $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: window.AJAX_ERROR,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
    });

    $(document).on('change', '#Program', function (event) {

        $('#Session').empty();
        $('#Session').append("<option value=''>Select Session</option>");

        var organizationId = $('#Organization').val();
        var program = $('#Program').val();
        //alert(program);
        if (program.length > 0) {
            $.ajax({
                //url: "/Administration/Discount/GetSession",
                type: "POST",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                data: { organizationIds: organizationId, programIds: program, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    //// $('.sessionListGenerate').html(data);
                    //$('#Session').html(data);
                    //$.unblockUI();
                    $.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnSessionList, function (i, v) {
                            $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
    });

    /*Get Course against program and session */
    $(document).on('change', '#Session', function () {
        var programId = $('#Program').val();
        var sessionId = $('#Session').val();

        $.ajax({
            url: "/Administration/Discount/CourseListGenerate",
            type: "POST",
            data: { programId: programId, sessionId: sessionId },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $('#discountDetail').html(data);
                $.unblockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                $.fn.customMessage({
                    displayMessage: ajaxError,
                    displayMessageType: "error",
                    displayTime: 5000
                });
            }
        });
    });

    /* Validation check */
    function checkValidateForDiscount(update) {
        if (typeof (update) === 'undefined') update = false;
        var isValid = true;
        var amount;
        $('#Organization').removeClass('highlight');
        $('#Program').removeClass('highlight');
        $('#Session').removeClass('highlight');
        $('#DiscountAmount').removeClass('highlight');
        //$('#Amount').removeClass('highlight');
        $('#StartTime').removeClass('highlight');
        $('#EndTime').removeClass('highlight');
        if(!update)
            var organization = $('#Organization').val();
        var program = $('#Program').val();
        var session = $('#Session').val();
        var discountAmount = $('#DiscountAmount').val();
        if (discountAmount.length > 0) {
            amount = parseFloat(discountAmount.trim());
        } else {
            discountAmount = $('#Amount').val();
            if (discountAmount.length > 0) {
                amount = parseFloat(discountAmount.trim());
            } else {
                amount = 0;
            }
        }
        var startTime = $('.form_datetime1').val();
        var endTime = $('.form_datetime2').val();
        if (!update)
            if (organization.length <= 0) { $('#Organization').addClass('highlight'); isValid = false; }
        if (program.length <= 0) { $('#Program').addClass('highlight'); isValid = false; }
        if (session.length <= 0) { $('#Session').addClass('highlight'); isValid = false; }
        if (amount == 0) { $('#DiscountAmount').addClass('highlight'); isValid = false; }
        if (amount < 0) {
            $('#DiscountAmount').addClass('highlight'); isValid = false;
            $.fn.customMessage({
                displayMessage: "Negative Discount Amount Not Acceptable",
                displayMessageType: "error",
                displayTime: 5000
            });
        }
        if (amount == 0) { $('#Amount').addClass('highlight'); isValid = false; }
        if (amount < 0) {
            $('#Amount').addClass('highlight'); isValid = false;
            $.fn.customMessage({
                displayMessage: "Invalid Discount Amount",
                displayMessageType: "error",
                displayTime: 5000
            });
        }
        if (startTime.length <= 0) { $('.form_datetime1').addClass('highlight'); isValid = false; }
        if (endTime.length <= 0) { $('.form_datetime2').addClass('highlight'); isValid = false; }

        return isValid;
    }
    /*Update Discount */
    $(document).on('click', '#UpdateDiscount', function () {

        var isMinSubFieldNotEmpty = false;
        var amount;
        var jj = 1;
        var program = $('#Program').val();
        var session = $('#Session').val();
        var discountAmount = $('#Amount').val();
        if (discountAmount.length > 0) {
            amount = parseFloat(discountAmount.trim());
        } else {
            amount = 0;
        }

        var startTime = $('.form_datetime1').val();
        var endTime = $('.form_datetime2').val();
        var status = $('#Status').val();

        var isValid = checkValidateForDiscount(true);

        if (isValid == true) {
            var discount = new Discount();
            discount.ProgramId = program;
            discount.SessionId = session;
            discount.DiscountAmount = discountAmount;
            discount.StartTime = startTime;
            discount.EndTime = endTime;
            discount.Status = status;
            $('#discountDetail tr').each(function (index, value) {
                if ($(this).find('input[type=checkbox]').is(':checked')) {
                    jj++;
                    var discountCourse = new DiscountCourse();
                    var courseId = $(this).find('input[type=checkbox]').attr('data-courseid');
                    var minSubValue = $(this).find('select').val();
                    if (minSubValue.length > 0) {
                        $(this).find('select').removeClass('highlight');
                        var minSub = $(this).find('select').val();
                        discountCourse.courseId = courseId;
                        discountCourse.minSub = minSub;
                        discount.DiscountObjProperty.push(discountCourse);
                        isMinSubFieldNotEmpty = true;
                    } else {
                        $(this).find('select').addClass('highlight');
                        isMinSubFieldNotEmpty = false;
                        return false;
                    }
                }
            });
            if (jj > 1) {
                if (isMinSubFieldNotEmpty == true) {
                    var discountObj = JSON.stringify(discount);
                    var disId = $('#disId').val();

                    $.ajax({
                        url: "/Administration/Discount/Edit",
                        type: "POST",
                        data: { discountObj: discountObj, disId: disId },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (data) {
                            if (data.IsSuccess) {
                                $.fn.customMessage({
                                    displayMessage: data.Message,
                                    displayMessageType: "s"
                                });
                                $.unblockUI();
                            } else {
                                $.fn.customMessage({
                                    displayMessage: data.Message,
                                    displayMessageType: "e"
                                });
                                $.unblockUI();
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (data) {
                            $.fn.customMessage({
                                displayMessage: ajaxError,
                                displayMessageType: "e"
                            });
                        }
                    });
                }
            } else {
                $.fn.customMessage({
                    displayMessage: " No Course Selected ",
                    displayMessageType: "w",
                    displayTime: 5000
                });
            }
        } else {
            $.fn.customMessage({
                displayMessage: "Empty Input Field",
                displayMessageType: "w"
            });
        }
    });

    /*END UPDATE CODE */

    // Add button click handler
    $(document).on('click', '#addDiscount', function () {
        var isMinSubFieldNotEmpty = false;
        var jj = 1;
        var program = $('#Program').val();
        var session = $('#Session').val();
        var discountAmount = $('#DiscountAmount').val().trim();
        var amount = parseFloat(discountAmount);
        var startTime = $('.form_datetime1').val();
        var endTime = $('.form_datetime2').val();

        var isValid = checkValidateForDiscount();
        if (isValid == true) {
            var discount = new Discount();
            discount.ProgramId = program;
            discount.SessionId = session;
            discount.DiscountAmount = discountAmount;
            discount.StartTime = startTime;
            discount.EndTime = endTime;

            $('#discountDetail tr').each(function (index, value) {
                if ($(this).find('input[type=checkbox]').is(':checked')) {
                    jj++;
                    var discountCourse = new DiscountCourse();
                    var courseId = $(this).find('input[type=checkbox]').attr('data-courseid');
                    var minSubValue = $(this).find('select').val();
                    if (minSubValue.length > 0) {
                        $(this).find('select').removeClass('highlight');
                        var minSub = $(this).find('select').val();
                        discountCourse.courseId = courseId;
                        discountCourse.minSub = minSub;
                        discount.DiscountObjProperty.push(discountCourse);
                        isMinSubFieldNotEmpty = true;
                    } else {
                        $(this).find('select').addClass('highlight');
                        isMinSubFieldNotEmpty = false;
                        return false;
                    }
                }
            });
            if (jj > 1) {
                if (isMinSubFieldNotEmpty == true) {
                    var discountObj = JSON.stringify(discount);
                    $.ajax({
                        url: "/Administration/Discount/AddDiscount",
                        type: "POST",
                        data: { discountObj: discountObj },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (data) {
                            if (data.IsSuccess) {
                                $.fn.customMessage({
                                    displayMessage: data.Message,
                                    displayMessageType: "s"
                                });
                                $.unblockUI();
                            } else {
                                $.fn.customMessage({
                                    displayMessage: data.Message,
                                    displayMessageType: "e"
                                });
                                $.unblockUI();
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (data) {
                            $.fn.customMessage({
                                displayMessage: ajaxError,
                                displayMessageType: "error",
                                displayTime: 5000
                            });
                        }
                    });
                }
            } else {
                $.fn.customMessage({
                    displayMessage: "No Course Selected ",
                    displayMessageType: "w",
                    displayTime: 5000
                });
            }
        } else {
            //alert("asdf");
            $.fn.customMessage({
                displayMessage: "Empty Input Field",
                displayMessageType: "w"
            });
        }
    });

});


