﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Transform;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Teachers
{
    public interface ITeacherClassOpeningBalanceDao : IBaseDao<TeacherClassOpeningBalance, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        TeacherClassOpeningBalance GetTeacherClassOpeningBalance(long id); 
        TeacherClassOpeningBalance GetTeacherClassOpeningBalanceByTeacherIdOrganizationId(long teacherId, long organizationId);

        #endregion

        #region List Loading Function

        IList<TeacherClassOpeningBalanceDto> LoadTeacherClassOpeningBalance(List<long> authOrganizationIdList, string pinList);

        #endregion

        #region Others Function
        #endregion

        
    }

    public class TeacherClassOpeningBalanceDao : BaseDao<TeacherClassOpeningBalance, long>, ITeacherClassOpeningBalanceDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public TeacherClassOpeningBalance GetTeacherClassOpeningBalance(long id)
        {
            TeacherClassOpeningBalance openingBalance = Session.QueryOver<TeacherClassOpeningBalance>()
                .Where(x =>x.Status == TeacherClassOpeningBalance.EntityStatus.Active 
                    && x.Id == id).SingleOrDefault();

            return openingBalance;
        }

        public TeacherClassOpeningBalance GetTeacherClassOpeningBalanceByTeacherIdOrganizationId(long teacherId, long organizationId)
        {
            TeacherClassOpeningBalance openingBalance = Session.QueryOver<TeacherClassOpeningBalance>()
                .Where(x => x.Status == TeacherClassOpeningBalance.EntityStatus.Active
                    && x.Teacher.Id == teacherId && x.Organization.Id == organizationId).SingleOrDefault();

            return openingBalance;
        }

        #endregion

        #region List Loading Function

        public IList<TeacherClassOpeningBalanceDto> LoadTeacherClassOpeningBalance(List<long> authOrganizationIdList, string pinList)
        {
            string query = GetLoadTeacherClassOpeningBalanceQuery(authOrganizationIdList, pinList);

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeacherClassOpeningBalanceDto>());
            iQuery.SetTimeout(2700);
            IList<TeacherClassOpeningBalanceDto> list = iQuery.List<TeacherClassOpeningBalanceDto>();
            return list;
        }

        private string GetLoadTeacherClassOpeningBalanceQuery(List<long> authOrganizationIdList, string pinList)
        {
            string organizationFilter = "";
            string tPinFilter = "";
            string teacherOrganizationFilter = "";
            if (!String.IsNullOrEmpty(pinList))
            {
                tPinFilter = " AND t.Id in (" + pinList + @") ";
            }
            if (authOrganizationIdList != null && authOrganizationIdList.Any())
            {
                organizationFilter = "and torg.OrganizationId in (" + string.Join(",", authOrganizationIdList) + @")";
                teacherOrganizationFilter = "and tcob.OrganizationId in (" + string.Join(",", authOrganizationIdList) + @")";
            }
            string query = @"Select 
                             t.Id as TeacherId
                             , t.Tpin as Tpin
                             , CASE when t.FullName is not null then t.FullName+' ('+t.NickName+')' else t.NickName end as TeacherName
                             , t.HscPassingYear as Batch
                             , CASE when tcob.OpeningDate is null then '-' ELSE left(convert(varchar(25), tcob.OpeningDate, 120),10)  END as OpeningDate 
                             , tcob.TotalClass as TotalClass 
                             , tcob.Id as TeacherClassOpeningBalanceId
                            from Teacher as t
                            inner join (
	                            select DISTINCT t.Id  as TeacherId
	                            from Teacher as t
	                            inner join TeacherOrganization as torg on t.Id = torg.TeacherId
	                            where t.status = " + Teacher.EntityStatus.Active + tPinFilter + organizationFilter + @"
                            ) as torg on torg.TeacherId = t.Id
                            left join TeacherClassOpeningBalance as tcob on tcob.TeacherId = t.Id and tcob.Status = " + TeacherClassOpeningBalance.EntityStatus.Active + teacherOrganizationFilter + @"";

            return query;
        }

        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion

        
    }

}
