﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Hr
{

    public class TrainingInfoFactory : ObjectFactoryBase<TrainingInfo>
    {
        private TeamMemberFactory _teamMemberFactory;
        private InstituteFactory _instituteFactory;

        private readonly ITrainingInfoService _trainingInfoService;
        public TrainingInfoFactory(IObjectFactoryBase caller, ISession session) : base(caller, session)
        {
            _teamMemberFactory = new TeamMemberFactory(this, session);
            _instituteFactory = new InstituteFactory(this, session);
            _trainingInfoService = new TrainingInfoService(session);
        }

        #region Create
        public TrainingInfoFactory Create()
        {
            Object = new TrainingInfo()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = Name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Department.EntityStatus.Active,
                TrainingTitle = "training_" + Name,
                TopicCovere = "NIL",
                Location = "DHAKA",
                Duration = "2",
                Year = 2016,
                Certification = "NIL",
                IsTraining = true
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public TrainingInfoFactory CreateWith(string name, string trainingTitle, string topicCovere, string location, string duration,
            int year, string certification, bool isTraining)
        {
            Object = new TrainingInfo()
            {
                Id = 0,
                BusinessId = GetPcUserName(),
                Name = name,
                CreateBy = 1,
                ModifyBy = 1,
                Status = Department.EntityStatus.Active,
                TrainingTitle = trainingTitle,
                TopicCovere = topicCovere,
                Location = location,
                Duration = duration,
                Year = year,
                Certification = certification,
                IsTraining = isTraining
            };
            SingleObjectList.Add(Object);
            return this;
        }
        public TrainingInfoFactory WithTeamMember()
        {
            _teamMemberFactory.Create().Persist();
            Object.TeamMember = _teamMemberFactory.Object;
            return this;
        }
        public TrainingInfoFactory WithTeamMember(TeamMember teamMember)
        {
            Object.TeamMember = teamMember;
            return this;
        }
        public TrainingInfoFactory WithInstitute()
        {
            _instituteFactory.Create().Persist();
            Object.Institute = _instituteFactory.Object;
            return this;
        }
        public TrainingInfoFactory WithInstitute(Institute institute)
        {
            Object.Institute = institute;
            return this;
        }

        public TrainingInfoFactory CreateMore(int num)
        {
            _teamMemberFactory.Create().Persist();
            _instituteFactory.Create().Persist();
            for (int i = 0; i < num; i++)
            {
                var obj = new TrainingInfo();
                obj.TeamMember = _teamMemberFactory.Object;
                obj.Institute = _instituteFactory.Object;
                ObjectList.Add(obj);
            }
            return this;
        }
         
        #endregion

        #region Others
        #endregion

        #region CleanUp

        public TrainingInfoFactory CleanUp()
        {
            DeleteAll();
            _teamMemberFactory.Cleanup();
            _instituteFactory.Cleanup();
            return this;
        }

        #endregion


    }

}
