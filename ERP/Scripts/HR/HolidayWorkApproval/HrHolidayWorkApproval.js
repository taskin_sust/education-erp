﻿$(document).ready(function () {
    $("#holidayWorkApprovalDisplayList").tableHeadFixer({ "left": 3, "z-index": 2 });
    $(document).on("focus", ".form-date", function () {
        //var today = new Date();
        //var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();
        ////var month = today.getMonth();
        ////if (month == 0)
        ////    month = 1;
        ////var startDate = today.getFullYear().toString() + "-" + (month) + "-" + "01";
        //var startDate = today.getFullYear().toString() + "-" + (1) + "-" + "01";
        //var endDate = initialDate;
        //console.log(startDate);

        var today = new Date();
        var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();

        var pastMonth = new Date(today.setMonth(today.getMonth() - 1, 1));
        var startDate = pastMonth.getFullYear().toString() + "-" + (pastMonth.getMonth() + 1) + "-" + "01";
        var endDate = initialDate;

        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: initialDate,
            startDate: startDate,
            endDate: endDate,
            startView: 2,
            minView: 2,
            maxView: 4
        }).on('changeDate', function (ev) {
            if ($(".form-date").val() !== "") {
                $(".form-date").parent().find('span').remove();
                $(".form-date").parent().append('<span class="field-validation-valid text-danger" data-valmsg-for="FromTime" data-valmsg-replace="true"></span>');
            }
        });
    });

    $(document).on("focus", ".form-date-history", function () {
        var today = new Date();
        var initialDate = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();
        var startDate = (today.getFullYear()-1).toString() + "-" + (1) + "-" + "01";
        var endDate = initialDate;
        console.log(startDate);
        $(this).datetimepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: false,
            showMeridian: true,
            initialDate: initialDate,
            startDate: startDate,
            endDate: endDate,
            startView: 2,
            minView: 2,
            maxView: 4
        }).on('changeDate', function (ev) {
            if ($(".form-date").val() !== "") {
                $(".form-date").parent().find('span').remove();
                $(".form-date").parent().append('<span class="field-validation-valid text-danger" data-valmsg-for="FromTime" data-valmsg-replace="true"></span>');
            }
        });
    });

    $(document).on('change', '#Organization', function (e) {
        $("#Department option").remove();
        $('#Department').append("<option value=''>---Department---</option>");
        $("#Campus option").remove();
        $('#Campus').append("<option value=''>---Campus---</option>");
        $("#Branch option").remove();
        $('#Branch').append("<option value=''>---Branch---</option>");
        var orgId = $("#Organization").val();
        if (orgId != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                cache: false,
                async: true,
                data: { organizationIds: orgId, isAuthorized: true, isProgramBranSession: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.each(result.returnBranchList, function (i, v) {
                            $('#Branch').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "error",
                        });
                    }
                    //Load Department
                    $.ajax({
                        url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadDepartment",
                        type: "POST",
                        data: { organizationIds: orgId },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (responses) {
                            if (responses.IsSuccess) {
                                //$('#Department').append('<option value="0">All Department</option>');
                                $.each(responses.returndepartmentList, function (i, v) {
                                    $('#Department').append($('<option>').text(v.Text).attr('value', v.Value));
                                });
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (responses) {
                            $.unblockUI();
                        }
                    });
                    //End Loading Department
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
    });

    $(document).on('change', '#Branch', function (e) {
        $("#Campus option").remove();
        $('#Campus').append("<option value=''>---Campus---</option>");
        var orgId = $("#Organization").val();
        var branchId = $("#Branch").val();
        if (orgId != "" && branchId != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                cache: false,
                async: true,
                data: { organizationIds: orgId, branchIds: branchId, isAuthorized: true, isProgramBranSession: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.each(result.returnCampusList, function (i, v) {
                            $('#Campus').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
    });

    $(document).on('change', '#OrganizationId', function (e) {
        $("#DepartmentId option").remove();
        $('#DepartmentId').append("<option value='0'>All</option>");
        $("#CampusId option").remove();
        $('#CampusId').append("<option value='0'>All</option>");
        $("#BranchId option").remove();
        $('#BranchId').append("<option value='0'>All</option>");
        var orgId = $("#OrganizationId").val();
        if (orgId != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                cache: false,
                async: true,
                data: { organizationIds: orgId, isAuthorized: true, isProgramBranSession: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.each(result.returnBranchList, function (i, v) {
                            $('#BranchId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "error",
                        });
                    }
                    //Load Department
                    $.ajax({
                        url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadDepartment",
                        type: "POST",
                        data: { organizationIds: orgId },
                        beforeSend: function () {
                            $.blockUI({
                                timeout: 0,
                                message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                            });
                        },
                        success: function (responses) {
                            if (responses.IsSuccess) {
                                $.each(responses.returndepartmentList, function (i, v) {
                                    $('#DepartmentId').append($('<option>').text(v.Text).attr('value', v.Value));
                                });
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (responses) {
                            $.unblockUI();
                        }
                    });
                    //End Loading Department
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
    });

    $(document).on('change', '#BranchId', function (e) {
        $("#CampusId option").remove();
        $('#CampusId').append("<option value='0'>All</option>");
        var orgId = $("#OrganizationId").val();
        var branchId = $("#BranchId").val();
        if (orgId != "" && branchId != "") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                cache: false,
                async: true,
                data: { organizationIds: orgId, branchIds: branchId, isAuthorized: true, isProgramBranSession: false },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.each(result.returnCampusList, function (i, v) {
                            $('#CampusId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    } else {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "error",
                        });
                    }
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
    });

    $(document).on("input propertychange", "#PinList ", function () {
        var input = $(this),
            text = input.val().replace(/[^0-9 ]/g, "");
        input.val(text);
    });

    $(document).on("click", "#Update", function (event) {
        var message = "Are you sure you want to Approved All HolidayWork(s)?";
        bootbox.confirm(message, function (result) {
            if (result === true) {
                //var date = $("#Date").val();
                var tableId = "holidayWorkApprovalDisplayList";
                var rowCount = $('#' + tableId + ' > tbody > tr').length;
                console.log(rowCount);
                if (rowCount > 0) {
                    var holidayWorkApprovalObjArray = [];
                    var holidayWorkApprovalStatusArray = [];
                    $('#' + tableId + ' > tbody > tr').each(function () {
                        var parentTr = $(this);
                        var holidayWorkId = parentTr.find('.holidayId').val();
                        var memberPin = parentTr.find('.memberPin').attr('data-pin');
                        var approveType = 0;
                        var date = parentTr.find('.memberholidayWork').attr('data-holidayWork');
                        if (parentTr.find('.halfHolidayApproval').prop('checked'))
                            approveType = 1;
                        else if (parentTr.find('.fullHolidayApproval').prop('checked'))
                            approveType = 2;
                        var reason = parentTr.find('.reason ').val();
                        if (validateApplicationRow(parentTr) === true) {
                            var holidayWorkApprovalObj = new HolidayWorkApprovalViewModel();
                            holidayWorkApprovalObj.HolidayWorkId = holidayWorkId;
                            holidayWorkApprovalObj.Pin = memberPin;
                            holidayWorkApprovalObj.ApprovalType = approveType;
                            holidayWorkApprovalObj.Reason = reason;
                            holidayWorkApprovalObj.HolidayWorkDate = date;
                            holidayWorkApprovalObjArray.push(holidayWorkApprovalObj);
                        } else {
                            holidayWorkApprovalStatusArray.push(1);
                        }
                    });
                    var holidayWorkApprovalViewModelList = holidayWorkApprovalObjArray;
                    if (holidayWorkApprovalStatusArray.length <= 0) {
                        $.ajax({
                            url: $("body").attr("data-project-root") + "Hr/HolidayWork/HolidayWorkApproval",
                            type: "POST",
                            data: { "holidayWorkApprovalViewModelList": holidayWorkApprovalViewModelList, isHr: true },
                            beforeSend: function () {
                                $.blockUI({
                                    timeout: 0,
                                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                                });
                            },
                            success: function (data) {
                                $.unblockUI();
                                if (data.IsSuccess) {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "s"
                                    });
                                    $("#displayList").remove();
                                    $("#Update").hide();
                                } else {
                                    $.fn.customMessage({
                                        displayMessage: data.Message,
                                        displayMessageType: "e"
                                    });
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function () {
                                $.unblockUI();
                                bootbox.alert("Slow Internet Connection").css('margin-top', (($(window).height() / 4)));
                            }
                        });
                    }
                }
            }
        }).css({
            'color': 'red',
            'font-size': '30px',
            'margin-top': (($(window).height() / 4))
        });

    });

    //$('#holidayWorkTabs a').click(function (e) {
    $(document).on("click","#holidayWorkTabs a",function (e){
        e.preventDefault();
        $(this).tab('show');
        var controls = $(this).attr("aria-controls");

        if (controls === "history") {
            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Hr/HolidayWork/HrHolidayWorkHistory",
                cache: false,
                async: true,
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    console.log(result);
                    if (!result.hasOwnProperty("IsSuccess")) {
                        $("#history").html(result);
                        var today = new Date();
                        var initialDateHistory = today.getFullYear().toString() + "-" + (today.getMonth() + 1) + "-" + today.getDate().toString();
                        var startDateHistory = (today.getFullYear() - 2).toString() + "-" + today.getMonth() + "-" + "01";
                        var endDateHistory = initialDateHistory;

                        $('.form-date').datetimepicker({
                            format: "yyyy-mm-dd",
                            autoclose: true,
                            todayBtn: false,
                            showMeridian: true,
                            initialDate: initialDateHistory,
                            startDate: startDateHistory,
                            endDate: endDateHistory,
                            startView: 2,
                            minView: 2,
                            maxView: 4
                        });
                        dataTableRender();


                    } else {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "error",
                        });
                    }

                },
                complete: function () {
                    $.unblockUI();

                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
    });

    $(document).on("click", "#search", function () {
        var dateFrom = $("#dateFrom").val();
        var dateTo = $("#dateTo").val();
        if (dateFrom > dateTo) {

            $.fn.customMessage({
                displayMessage: "From Datetime must be less than or equal to To Datetime",
                displayMessageType: "error",
            });
        }
        else {
            dataTableRender();
        }
    });

});

function dataTableRender() {

    var pageSize = 100;

    if ($('#PageSize').val() != "" && /^\d+$/.test($('#PageSize').val())) {
        pageSize = parseInt($('#PageSize').val());
    } else {
        $('#PageSize').val(pageSize);
    }

    $('#DataGrid').dataTable({
        destroy: true,
        "processing": true,
        searching: false,
        serverSide: true,
        "scrollX": true,
        "scrollCollapse": true,
        "iDisplayLength": pageSize,
        "scrollY": "60vh",
        "bLengthChange": false,
        "aoColumnDefs": [{ "bSortable": false, "aTargets": ['_all'] }],
        fixedColumns: {
            leftColumns: 3
        },
        order: [[6, "Desc"]],
        ajax: {
            url: $("body").attr("data-project-root") + "Hr/HolidayWork/LoadHolidayWorkHistory",
            type: 'POST',
            data: function (d) {
                d.organizationId = $('#Organization').val();
                d.branchId = $('#Branch').val();
                d.campusId = $('#Campus').val();
                d.departmentId = $("#Department").val();
                d.pin = $('#pin').val();
                d.dateFrom = $("#dateFrom").val();
                d.dateTo = $("#dateTo").val();
            }
        }
    });
}

function validateApplicationRow(parentTr) {
    var success = true;
    var memberPin = parentTr.find('.memberPin').attr('data-pin');
    if (memberPin.length <= 0 || memberPin === '') {
        success = false;
        parentTr.find('.memberPin').addClass('highlight');
    } else {
        parentTr.find('.memberPin').removeClass('highlight');
    }

    var approveType = 0;
    if (parentTr.find('.halfHolidayApproval').prop('checked'))
        approveType = 1;
    else if (parentTr.find('.fullHolidayApproval').prop('checked'))
        approveType = 2;

    var reason = parentTr.find('.reason ').val();
    if (reason.length <= 0 || reason === '') {
        parentTr.find('.reason ').addClass('highlight');
        success = false;
    } else {
        parentTr.find('.reason ').removeClass('highlight');
    }
    //if (approveType > 0) {
    //    if (reason.length <= 0 || reason ==='') {
    //        parentTr.find('.reason ').addClass('highlight');
    //        success = false;
    //    } else {
    //        parentTr.find('.reason ').removeClass('highlight');
    //    }
    //} 
    return success;
}

function HolidayWorkApprovalViewModel() {
    this.HolidayWorkId = 0;
    this.Pin = 0;
    this.HolidayWorkDate = "";
    this.Reason = "";
    this.ApprovalType = 0;
    this.HolidayWorkApprovalViewModel = function () { };
}
