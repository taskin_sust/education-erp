﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;
using NHibernate.Loader.Criteria;
using NHibernate.Persister.Entity;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IAllowanceSheetDao : IBaseDao<AllowanceSheet, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        AllowanceSheetFirstDetails GetTeamMemberAllowanceFirstDetailsSheet(long? teamMemberId = null, int? pin = null);
        TeamMemberJobHistorySalaryHistoryAttadeanceSummaryDto GetTeamMemberJobHistorySalaryHistoryAttadeanceSummaryDto(long teamMemberId, DateTime date);
        AllowanceSheet GetTeamMemberAllowanceSheet(long teammemberId, DateTime dateFrom, DateTime dateTo);
        AllowanceSheet GetTeamMemberLastIsSubmittedAllowanceSheet(long? teammemberId = null, int? pin = null);

        #endregion

        #region List Loading Function

        
        #endregion

        #region Others Function

        AllowanceSheet GetEligibleAllowanceSheetByOrganizationId(long orgId);

        #endregion

        #region Helper Function

        #endregion


    }
    public class AllowanceSheetDao : BaseDao<AllowanceSheet, long>, IAllowanceSheetDao
    {

        #region Propertise & Object Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function

        public AllowanceSheetFirstDetails GetTeamMemberAllowanceFirstDetailsSheet(long? teamMemberId = null, int? pin = null)
        {

            return null;
        }

        public TeamMemberJobHistorySalaryHistoryAttadeanceSummaryDto GetTeamMemberJobHistorySalaryHistoryAttadeanceSummaryDto(long teamMemberId, DateTime date)
        {
            string query = @"select 
                            tem.Id as TeamMemberId
                            , tem.Pin as TeamMemberPin
                            , jobHistory.JobCampusId as JobCampusId
                            , jobHistory.JobBranchId as JobBranchId
                            , jobHistory.JobDepartmentId as JobDepartmentId
                            , jobHistory.JobDesignationId as JobDesignationId
                            , jobHistory.JobOrganizationId as JobOrganizationId
                            , jobOrg.MonthlyWorkingDay as JobOrgMonthlyWorkingDay
                            , jobOrg.BasicSalary as JobOrgBasicSalary
                            , jobHistory.JobEffectiveDate as JobEffectiveDate
                            , jobHistory.EmploymentStatus as EmploymentStatus
                            , salaryHistory.SalaryEffectiveDate as  SalaryEffectiveDate
                            , salaryHistory.SalaryOrganizationId as  SalaryOrganizationId
                            , salaryHistory.Salary as Salary
                            , saOrg.MonthlyWorkingDay as SalaryOrgMonthlyWorkingDay
                            , saOrg.BasicSalary as SalaryOrgBasicSalary
                            , atdSummary.AttendanceDate as AttendanceDate
                            , atdSummary.InTime as InTime
                            , atdSummary.OutTime as OutTime
                            , atdSummary.IsWeekend as IsWeekend
                            , holidaySetting.HolidayType as HolidayType
                            from HR_TeamMember as tem
                            left join (
	                            select * from (
		                            Select 
		                            TeamMemberId AS[SalaryTeamMemberId]
		                            ,EffectiveDate AS [SalaryEffectiveDate]
		                            , SalaryOrganizationId as SalaryOrganizationId
                                    , Salary as Salary
		                            ,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC, Id DESC) AS R
		                            from [dbo].[HR_SalaryHistory] 
		                            Where [Status] = 1
			                            AND EffectiveDate <= '" + date.ToString("yyyy-MM-dd 23:59:59") + @"' 
	                            ) AS A Where A.R=1
                            ) as salaryHistory on salaryHistory.SalaryTeamMemberId = tem.Id
                            left join (
	                            select A.*
	                            , b.Id as JobBranchId
	                            , b.OrganizationId as  JobOrganizationId
	                            from (
		                            Select [CampusId] as JobCampusId
		                            ,[DesignationId] as JobDesignationId
		                            ,[DepartmentId] as JobDepartmentId
		                            ,[TeamMemberId]
		                            ,[EffectiveDate] as JobEffectiveDate
		                            ,[EmploymentStatus]
		                            ,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC, Id DESC) AS R  
		                            from [dbo].[HR_EmploymentHistory]
		                            Where [Status] = 1
		                            AND EffectiveDate <= '" + date.ToString("yyyy-MM-dd 23:59:59") + @"' 	                   
	                            ) AS A 
	                            inner join Campus as c on c.Id = A.JobCampusId
	                            inner join Branch as b on b.Id = c.BranchId
	                            Where A.R = 1 
                            ) as jobHistory on jobHistory.TeamMemberId = tem.Id
                            left join Organization as saOrg on saOrg.Id = salaryHistory.SalaryOrganizationId
                            left join Organization as jobOrg on jobOrg.Id = jobHistory.JobOrganizationId
                            left join HR_AttendanceSummary as atdSummary on atdSummary.TeamMemberId = tem.Id and atdSummary.AttendanceDate = '" + date.ToString("yyyy-MM-dd 00:00:00") + @"'
                            left Join HR_HolidaySetting as holidaySetting on holidaySetting.Id = atdSummary.HolidaySettingId 
                            where tem.Status = 1
                            and tem.Id = " + teamMemberId + @" ";

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMemberJobHistorySalaryHistoryAttadeanceSummaryDto>());
            TeamMemberJobHistorySalaryHistoryAttadeanceSummaryDto dto = iQuery.UniqueResult<TeamMemberJobHistorySalaryHistoryAttadeanceSummaryDto>();
            return dto;
        }

        public AllowanceSheet GetTeamMemberAllowanceSheet(long teammemberId, DateTime dateFrom, DateTime dateTo)
        {
            var query = Session.QueryOver<AllowanceSheet>();
            query.Where(x => x.TeamMember.Id == teammemberId);
            query.Where(x => x.Year == dateFrom.Year);
            query.Where(x => (int)x.Month == dateFrom.Month);
            query = query.Where(x => x.DateFrom.Date == dateFrom.Date);
            query = query.Where(x => x.DateTo.Date == dateTo.Date);
            return query.SingleOrDefault<AllowanceSheet>();
        }

        public AllowanceSheet GetTeamMemberLastIsSubmittedAllowanceSheet(long? teamMemberId = null, int? pin = null)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<AllowanceSheet>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", AllowanceSheet.EntityStatus.Delete))); //Add(Restrictions.Eq("Status", AllowanceSheet.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("IsFinalSubmit", true));
            criteria.CreateAlias("TeamMember", "member");//.Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("DateTo"));
            return criteria.List<AllowanceSheet>().Take(1).FirstOrDefault();
        }
        
        #endregion

        #region List Loading Function

       

        #endregion

        #region Others Function

        public AllowanceSheet GetEligibleAllowanceSheetByOrganizationId(long orgId)
        {
            AllowanceSheet allowanceSheetAlies = null;
            Department departmentAlies = null;
            var result = Session.QueryOver(() => allowanceSheetAlies)
                .Left.JoinAlias(all => all.TeamMemberDepartment, () => departmentAlies)
                .Where(all => departmentAlies.Organization.Id == orgId && allowanceSheetAlies.IsFinalSubmit)
                .OrderBy(allow => allowanceSheetAlies.DateTo)
                .Desc.Take(1)
                .SingleOrDefault<AllowanceSheet>();
            return result;
        }

        #endregion

        #region Helper Function

        #endregion

    }

}
