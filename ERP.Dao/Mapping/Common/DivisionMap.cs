using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.Common {


    public partial class DivisionMap : BaseClassMap<Division, long>//ClassMap<Division>
    {

        public DivisionMap()
        {
            Table("Division");
            LazyLoad();
            //Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            //Map(x => x.VersionNumber).Column("VersionNumber").Not.Nullable();
            //Map(x => x.BusinessId).Column("BusinessId");
            //Map(x => x.CreationDate).Column("CreationDate");
            //Map(x => x.ModificationDate).Column("ModificationDate");
            //Map(x => x.Status).Column("Status");
            //Map(x => x.CreateBy).Column("CreateBy");
            //Map(x => x.ModifyBy).Column("ModifyBy");
            Map(x => x.Name).Column("Name");
            HasMany(x => x.District).KeyColumn("DivisionId");
        }
    }
}
