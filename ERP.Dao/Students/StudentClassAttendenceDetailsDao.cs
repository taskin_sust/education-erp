﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Utils;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Students
{
    public interface IStudentClassAttendenceDetailsDao : IBaseDao<StudentClassAttendanceDetails, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function
        #endregion

        #region Others Function
        int ClassAttendanceCount(long lectureId, long studentProgramId, long teacherId);
        #endregion

    }

    public class StudentClassAttendenceDetailsDao : BaseDao<StudentClassAttendanceDetails, long>, IStudentClassAttendenceDetailsDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        public int ClassAttendanceCount(long lectureId, long studentProgramId, long teacherId)
        {
            var classAttendance = Session.Query<StudentClassAttendanceDetails>()
                 .Where(
                     x =>
                         x.StudentProgram.Id == studentProgramId && x.StudentClassAttendence.Teacher.Id == teacherId &&
                         x.StudentClassAttendence.Lecture.Id == lectureId);
            return classAttendance.Count();
        }
        #endregion

        #region Helper Function

        #endregion
    }
}