﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using Microsoft.AspNet.Identity;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Teacher;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Teachers;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Teachers;

namespace UdvashERP.Areas.Teachers.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Teachers")]
    public class TeacherPublicRegistrationController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeachersArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private readonly ITeacherActivityService _teacherActivityService;
        private readonly ISubjectService _subjectService;
        private readonly ITeacherPublicRegistrationService _teacherPublicRegistrationService;
        private readonly ITeacherService _teacherService;
        private List<UserMenu> _userMenu;

        public TeacherPublicRegistrationController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _commonHelper = new CommonHelper();
                _organizationService = new OrganizationService(session);
                _teacherActivityService = new TeacherActivityService(session);
                _subjectService = new SubjectService(session);
                _teacherPublicRegistrationService = new TeacherPublicRegistrationService(session);
                _teacherService = new TeacherService(session);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Save

        #endregion

        #region Edit

        #endregion

        #region Assign Teacher

        [HttpPost]
        public JsonResult AssignTeacher(long applicationId)
        {
            int tpin = 0;
            bool successState = false;
            string message = "";
            try
            {
                TeacherPublicRegistration teacherPublicRegistration = _teacherPublicRegistrationService.GetById(applicationId);
                if (teacherPublicRegistration.Tpin != null && teacherPublicRegistration.Tpin > 0)
                {
                    message = "Teacher Application already assigned to a teacher and He/She TPIN is " + teacherPublicRegistration.Tpin;
                }
                if (teacherPublicRegistration != null)
                {
                    #region Teacher Object Fill

                    DateTime date = DateTime.Now;
                    long createdBy = Convert.ToInt64(HttpContext.User.Identity.GetUserId());
                    long modifyBy = createdBy;
                    Teacher teacher = new Teacher();

                    //quick information here 

                    #region quick information

                    teacher.NickName = teacherPublicRegistration.NickName;
                    teacher.PersonalMobile = teacherPublicRegistration.MobileNumber1;
                    teacher.AlternativeMobile = teacherPublicRegistration.MobileNumber2;
                    teacher.Institute = teacherPublicRegistration.Institute;
                    teacher.Department = teacherPublicRegistration.Department;
                    teacher.HscPassingYear = teacherPublicRegistration.HscPassingYear;
                    teacher.Religion = teacherPublicRegistration.Religion;
                    teacher.Gender = teacherPublicRegistration.Gender;

                    #region Teacher Organization

                    if (teacherPublicRegistration.Organization != null)
                    {
                        List<Organization> organizationList = new List<Organization>
                        {
                            teacherPublicRegistration.Organization
                        };
                        teacher.Organizations = organizationList;
                    }

                    #endregion

                    #region Teacher Activity

                    List<TeacherActivityPriority> teacherActivityPriorityList = new List<TeacherActivityPriority>();

                    if (teacherPublicRegistration.TeacherActivity1 != null)
                    {
                        TeacherActivityPriority tempTeacherActivity = new TeacherActivityPriority
                        {
                            Teacher = teacher,
                            TeacherActivity = teacherPublicRegistration.TeacherActivity1,
                            Priority = 1,
                            CreationDate = date,
                            ModificationDate = date,
                            CreateBy = createdBy,
                            ModifyBy = modifyBy,
                            Status = TeacherActivityPriority.EntityStatus.Active
                        };
                        teacherActivityPriorityList.Add(tempTeacherActivity);
                    }

                    if (teacherPublicRegistration.TeacherActivity2 != null)
                    {
                        TeacherActivityPriority tempTeacherActivity = new TeacherActivityPriority
                        {
                            Teacher = teacher,
                            TeacherActivity = teacherPublicRegistration.TeacherActivity2,
                            Priority = 2,
                            CreationDate = date,
                            ModificationDate = date,
                            CreateBy = createdBy,
                            ModifyBy = modifyBy,
                            Status = TeacherActivityPriority.EntityStatus.Active
                        };
                        teacherActivityPriorityList.Add(tempTeacherActivity);
                    }

                    if (teacherPublicRegistration.TeacherActivity3 != null)
                    {
                        TeacherActivityPriority tempTeacherActivity = new TeacherActivityPriority
                        {
                            Teacher = teacher,
                            TeacherActivity = teacherPublicRegistration.TeacherActivity3,
                            Priority = 3,
                            CreationDate = date,
                            ModificationDate = date,
                            CreateBy = createdBy,
                            ModifyBy = modifyBy,
                            Status = TeacherActivityPriority.EntityStatus.Active
                        };
                        teacherActivityPriorityList.Add(tempTeacherActivity);
                    }
                    teacher.TeacherActivityPriorities = teacherActivityPriorityList;

                    #endregion

                    #region Teacher Subject priority

                    List<TeacherSubjectPriority> teacherSubjectPriorityList = new List<TeacherSubjectPriority>();

                    if (teacherPublicRegistration.Subject1 != null)
                    {
                        TeacherSubjectPriority tempTeacherSubjectPriority = new TeacherSubjectPriority
                        {
                            Teacher = teacher,
                            Subject = teacherPublicRegistration.Subject1,
                            Priority = 1,
                            CreationDate = date,
                            ModificationDate = date,
                            CreateBy = createdBy,
                            ModifyBy = modifyBy,
                            Status = TeacherSubjectPriority.EntityStatus.Active
                        };
                        teacherSubjectPriorityList.Add(tempTeacherSubjectPriority);
                    }

                    if (teacherPublicRegistration.Subject2 != null)
                    {
                        TeacherSubjectPriority tempTeacherSubjectPriority = new TeacherSubjectPriority
                        {
                            Teacher = teacher,
                            Subject = teacherPublicRegistration.Subject2,
                            Priority = 2,
                            CreationDate = date,
                            ModificationDate = date,
                            CreateBy = createdBy,
                            ModifyBy = modifyBy,
                            Status = TeacherSubjectPriority.EntityStatus.Active
                        };
                        teacherSubjectPriorityList.Add(tempTeacherSubjectPriority);
                    }

                    if (teacherPublicRegistration.Subject3 != null)
                    {
                        TeacherSubjectPriority tempTeacherSubjectPriority = new TeacherSubjectPriority
                        {
                            Teacher = teacher,
                            Subject = teacherPublicRegistration.Subject3,
                            Priority = 3,
                            CreationDate = date,
                            ModificationDate = date,
                            CreateBy = createdBy,
                            ModifyBy = modifyBy,
                            Status = TeacherSubjectPriority.EntityStatus.Active
                        };
                        teacherSubjectPriorityList.Add(tempTeacherSubjectPriority);
                    }
                    teacher.TeacherSubjectPriorities = teacherSubjectPriorityList;

                    #endregion

                    #region Teacher Version priority

                    List<TeacherVersionOfStudyPriority> teacherVersionOfStudyPriorityList =
                        new List<TeacherVersionOfStudyPriority>();

                    if (teacherPublicRegistration.VersionOfStudy1 > 0)
                    {
                        TeacherVersionOfStudyPriority teacherVersionOfStudyPriority = new TeacherVersionOfStudyPriority
                        {
                            Teacher = teacher,
                            VersionOfStudy = teacherPublicRegistration.VersionOfStudy1,
                            Priority = 1,
                            CreationDate = date,
                            ModificationDate = date,
                            CreateBy = createdBy,
                            ModifyBy = modifyBy,
                            Status = TeacherVersionOfStudyPriority.EntityStatus.Active
                        };
                        teacherVersionOfStudyPriorityList.Add(teacherVersionOfStudyPriority);
                    }

                    if (teacherPublicRegistration.VersionOfStudy2 != null)
                    {
                        TeacherVersionOfStudyPriority teacherVersionOfStudyPriority = new TeacherVersionOfStudyPriority
                        {
                            Teacher = teacher,
                            VersionOfStudy = (int)teacherPublicRegistration.VersionOfStudy2,
                            Priority = 2,
                            CreationDate = date,
                            ModificationDate = date,
                            CreateBy = createdBy,
                            ModifyBy = modifyBy,
                            Status = TeacherVersionOfStudyPriority.EntityStatus.Active
                        };
                        teacherVersionOfStudyPriorityList.Add(teacherVersionOfStudyPriority);
                    }
                    teacher.TeacherVersionOfStudyPriorities = teacherVersionOfStudyPriorityList;

                    #endregion

                    #endregion

                    #region details Information

                    teacher.FullName = teacherPublicRegistration.FullName;
                    teacher.Email = teacherPublicRegistration.Email;
                    teacher.FbId = teacherPublicRegistration.FacebookId;

                    #endregion
                    #endregion

                    _teacherService.SaveTeacher(teacher, null, teacherPublicRegistration);
                    if (teacher.Id > 0)
                    {
                        successState = true;
                        message = "Teacher Application successfully assign to a new teacher. New TPIN is " + teacher.Tpin;
                        tpin = teacher.Tpin;
                    }
                }
                throw new InvalidDataException("Invalid Input found!");
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
            }

            return Json(new { IsSuccess = successState, Message = message, tpin = tpin });
        }

        #endregion

        #region Register Teacher List

        [HttpGet]
        public ActionResult RegisterTeacherList()
        {
            try
            {
                List<SelectListItem> teacherActivityPrioritySelectList = new SelectList(_teacherActivityService.LoadTeacherActivity(), "Id", "Name").ToList();
                teacherActivityPrioritySelectList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Activity Priority" });

                List<SelectListItem> subjectSelectList = new SelectList(_subjectService.LoadSubject(), "Id", "Name").ToList();
                subjectSelectList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Subject" });

                List<SelectListItem> religionSelectList = new SelectList(_commonHelper.LoadEmumToDictionary<Religion>(), "Key", "Value").ToList();
                religionSelectList.Insert(0, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Religion" });

                List<SelectListItem> versionOfStudySelectList = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(), "Key", "Value").ToList();
                versionOfStudySelectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Version Priority" });
                versionOfStudySelectList.Insert(1, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Version Priority" });

                List<SelectListItem> genderSelectList = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(new List<int> { (int)Gender.Combined }), "Key", "Value").ToList();
                genderSelectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Gender" });
                genderSelectList.Insert(1, new SelectListItem() { Value = SelectionType.SelelectAll.ToString(), Text = "All Gender" });

                IList<SelectListItem> academicStudentSelectList = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Select Academic Student", Value = ""},
                    new SelectListItem {Text = "All", Value = SelectionType.SelelectAll.ToString()},
                    new SelectListItem {Text = "Yes", Value = "1"},
                    new SelectListItem {Text = "No", Value = "-1"}
                };

                IList<SelectListItem> teacherStatusSelectList = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Select Status", Value = ""},
                    new SelectListItem {Text = "All Status", Value = SelectionType.SelelectAll.ToString()},
                    new SelectListItem {Text = "Assign Teacher", Value = "1"},
                    new SelectListItem {Text = "Register Teacher", Value = "-1"}
                };

                List<SelectListItem> informationViewSelectList = new SelectList(TeacherRegisterApplicationConstants.GetTeacherRegistrationListInformationStringList().Select(x => new SelectListItem() { Text = x, Value = x }), "Text", "Value").ToList();

                ViewBag.TeacherActivityPrioritySelectList = teacherActivityPrioritySelectList;
                ViewBag.SubjectSelectList = subjectSelectList;
                ViewBag.ReligionSelectList = religionSelectList;
                ViewBag.VersionOfStudySelectList = versionOfStudySelectList;
                ViewBag.GenderSelectList = genderSelectList;
                ViewBag.AcademicStudentSelectList = academicStudentSelectList;
                ViewBag.TeacherStatusSelectList = teacherStatusSelectList;
                ViewBag.InformationViewSelectList = informationViewSelectList;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        public ActionResult RegisterTeacherList(RegisterTeacherList registerTeacherList)
        {
            try
            {
                if (registerTeacherList.InformationViewList == null)
                {
                    registerTeacherList.InformationViewList = new List<string>()
                    {
                        TeacherRegisterApplicationConstants.FullName, 
                        TeacherRegisterApplicationConstants.NickName
                    };
                }


                if (registerTeacherList.InformationViewList != null && registerTeacherList.InformationViewList.Any())
                {
                    registerTeacherList.InformationViewList.Add(TeacherRegisterApplicationConstants.Status);
                    registerTeacherList.InformationViewList.Add(TeacherRegisterApplicationConstants.Action);
                }
                ViewBag.InformationViewList = registerTeacherList.InformationViewList;
                var json = new JavaScriptSerializer().Serialize(registerTeacherList);
                ViewData["registerTeacherListViewModel"] = json;
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Problem occurred during Register Teacher List";
            }
            return View("RegisterTeacherListReport");
        }
        
        [HttpPost]
        public JsonResult RegisterTeacherListReportDataTable(int draw, int start, int length, string registerTeacherListViewModel = "", string nickName = "", string institute = "", string mobileNumber = "")
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            int recordsTotal = 0;
            int recordsFiltered = 0;
            var data = new List<object>();
            List<string> informationViewList = new List<string>();
            try
            {
                RegisterTeacherList registerTeacherList = new JavaScriptSerializer().Deserialize<RegisterTeacherList>(registerTeacherListViewModel);
                informationViewList = registerTeacherList.InformationViewList;

                #region datatable

                NameValueCollection nvc = Request.Form;
                var orderBy = "";
                var orderDir = "";

                #region OrderBy and Direction

                //if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                //{
                //    orderBy = nvc["order[0][column]"];
                //    orderDir = nvc["order[0][dir]"];
                //    switch (orderBy)
                //    {
                //        default:
                //            orderBy = "";
                //            break;
                //    }
                //}

                #endregion

                RegisterTeacherList rtl = registerTeacherList;
                IList<TeacherPublicRegistration> teacherPublicRegistrations = _teacherPublicRegistrationService.LoadRegisterTeacherList(_userMenu, start, length, orderBy, orderDir.ToUpper(), null, rtl.TeacherActivities, rtl.Subjects, rtl.Religions, rtl.VersionOfStudies, rtl.Genders, rtl.AcademicStudentStatus, rtl.TeacherApplicationStatus, nickName, institute, mobileNumber);

                int rowCount = _teacherPublicRegistrationService.GetRegisterTeacherCount(_userMenu, null, rtl.TeacherActivities, rtl.Subjects, rtl.Religions, rtl.VersionOfStudies, rtl.Genders, rtl.AcademicStudentStatus, rtl.TeacherApplicationStatus, nickName, institute, mobileNumber);
                recordsTotal = rowCount;
                recordsFiltered = recordsTotal;

                int sl = start;
                foreach (var publicRegisterTeacher in teacherPublicRegistrations)
                {
                    sl++;
                    List<string> str = new List<string> { sl.ToString() };
                     string status = "Unassigned";
                    string actionUrl = "-";
                    #region datatable
                    if (informationViewList != null && informationViewList.Any())
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case TeacherRegisterApplicationConstants.FullName:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.FullName))
                                    {
                                        str.Add(publicRegisterTeacher.FullName);
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.NickName:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.NickName))
                                    {
                                        str.Add(publicRegisterTeacher.NickName);
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.MobileNumber1:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.MobileNumber1))
                                    {
                                        str.Add(publicRegisterTeacher.MobileNumber1);
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.MobileNumber2:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.MobileNumber2))
                                    {
                                        str.Add(publicRegisterTeacher.MobileNumber2);
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.Email:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.Email))
                                    {
                                        str.Add(publicRegisterTeacher.Email);
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.FacebookId:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.FacebookId))
                                    {
                                        str.Add(publicRegisterTeacher.FacebookId);
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.Institute:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.Institute))
                                    {
                                        str.Add(publicRegisterTeacher.Institute);
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.Department:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.Department))
                                    {
                                        str.Add(publicRegisterTeacher.Department);
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.HscPassingYear:
                                    if (publicRegisterTeacher.HscPassingYear != null && publicRegisterTeacher.HscPassingYear > 0)
                                    {
                                        str.Add(publicRegisterTeacher.HscPassingYear.ToString());
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.Religion:
                                    if (publicRegisterTeacher.Religion != null && publicRegisterTeacher.Religion > 0)
                                    {
                                        str.Add(_commonHelper.GetEmumIdToValue<Religion>(publicRegisterTeacher.Religion));
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.Gender:
                                    if (publicRegisterTeacher.Gender != null && publicRegisterTeacher.Gender > 0)
                                    {
                                        str.Add(_commonHelper.GetEmumIdToValue<Gender>(publicRegisterTeacher.Gender));
                                        break;
                                    }
                                    str.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.TeacherActivity:
                                    string teacherActivityStr = "-";
                                    if (publicRegisterTeacher.TeacherActivity1 != null)
                                    {
                                        teacherActivityStr = publicRegisterTeacher.TeacherActivity1.Name;
                                        if (publicRegisterTeacher.TeacherActivity2 != null)
                                        {
                                            teacherActivityStr += ", " + publicRegisterTeacher.TeacherActivity2.Name;
                                        }
                                        if (publicRegisterTeacher.TeacherActivity3 != null)
                                        {
                                            teacherActivityStr += ", " + publicRegisterTeacher.TeacherActivity3.Name;
                                        }
                                    }
                                    str.Add(teacherActivityStr);
                                    break;
                                case TeacherRegisterApplicationConstants.SubjectPriority:
                                    string subjectPriorityStr = "-";
                                    if (publicRegisterTeacher.Subject1 != null)
                                    {
                                        subjectPriorityStr = publicRegisterTeacher.Subject1.Name;
                                        if (publicRegisterTeacher.Subject2 != null)
                                        {
                                            subjectPriorityStr += ", " + publicRegisterTeacher.Subject2.Name;
                                        }
                                        if (publicRegisterTeacher.Subject3 != null)
                                        {
                                            subjectPriorityStr += ", " + publicRegisterTeacher.Subject3.Name;
                                        }
                                    }
                                    str.Add(subjectPriorityStr);
                                    break;
                                case TeacherRegisterApplicationConstants.VersionPriority:
                                    string versionPriorityStr = "-";
                                    if (publicRegisterTeacher.VersionOfStudy1 != null)
                                    {
                                        versionPriorityStr = _commonHelper.GetEmumIdToValue<VersionOfStudy>(publicRegisterTeacher.VersionOfStudy1);
                                        if (publicRegisterTeacher.VersionOfStudy2 != null)
                                        {
                                            versionPriorityStr += ", " + _commonHelper.GetEmumIdToValue<VersionOfStudy>((int)publicRegisterTeacher.VersionOfStudy2);
                                        }
                                    }
                                    str.Add(versionPriorityStr);
                                    break;
                                case TeacherRegisterApplicationConstants.Status:
                                    if (publicRegisterTeacher.Tpin != null && publicRegisterTeacher.Tpin > 0)
                                    {
                                        status = "Assigned";
                                    }
                                    else
                                    {
                                        actionUrl = "<a id='" + publicRegisterTeacher.Id + "'   href='#' data-name='" + publicRegisterTeacher.NickName + "'  class='glyphicon glyphicon-share AssignTeacher'></a> ";

                                        actionUrl += LinkGenerator.GetDeleteLinkForModal(publicRegisterTeacher.Id, publicRegisterTeacher.DisplayTeacherName);
                                    }
                                    str.Add(status);
                                    break;
                                case TeacherRegisterApplicationConstants.Action:
                                    str.Add(actionUrl);
                                    break;
                                default:
                                    break;
                            }

                        }
                    }
                    #endregion

                    data.Add(str);
                }

                #endregion
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Problem occurred during Teacher search";
            }

            return Json(new { draw = draw, recordsTotal = recordsTotal, recordsFiltered = recordsFiltered, start = start, length = length, data = data });
        }

        public ActionResult ExportRegisterTeacherListReport(string registerTeacherListViewModel = "", string nickName = "", string institute = "", string mobileNumber = "")
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<string> headerList = new List<string>();
                headerList.Add("Register Teacher List");
                List<string> footerList = new List<string>();
                footerList.Add("");
                List<string> columnList = new List<string>();
                RegisterTeacherList registerTeacherList = new JavaScriptSerializer().Deserialize<RegisterTeacherList>(registerTeacherListViewModel);
                List<string> informationViewList = registerTeacherList.InformationViewList;

                if (informationViewList != null)
                {
                    columnList.AddRange(informationViewList.Where(info => info != TeacherRegisterApplicationConstants.Action));
                }
                RegisterTeacherList rtl = registerTeacherList;
                IList<TeacherPublicRegistration> teacherPublicRegistrations = _teacherPublicRegistrationService.LoadRegisterTeacherList(_userMenu, 0, int.MaxValue, "Tpin", "ASC", null, rtl.TeacherActivities, rtl.Subjects, rtl.Religions, rtl.VersionOfStudies, rtl.Genders, rtl.AcademicStudentStatus, rtl.TeacherApplicationStatus, nickName, institute, mobileNumber);


                var registerTeacherXlsList = new List<List<object>>();

                int sl = 0;
                foreach (var publicRegisterTeacher in teacherPublicRegistrations)
                {
                    sl++;
                    var xlsRow = new List<object>();
                    string status = "-";

                    #region datatable
                    if (informationViewList != null && informationViewList.Any())
                    {
                        foreach (string informationView in informationViewList)
                        {
                            switch (informationView)
                            {
                                case TeacherRegisterApplicationConstants.FullName:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.FullName))
                                    {
                                        xlsRow.Add(publicRegisterTeacher.FullName);
                                        break;
                                    }
                                    xlsRow.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.NickName:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.NickName))
                                    {
                                        xlsRow.Add(publicRegisterTeacher.NickName);
                                        break;
                                    }
                                    xlsRow.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.MobileNumber1:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.MobileNumber1))
                                    {
                                        xlsRow.Add(publicRegisterTeacher.MobileNumber1);
                                        break;
                                    }
                                    xlsRow.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.MobileNumber2:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.MobileNumber2))
                                    {
                                        xlsRow.Add(publicRegisterTeacher.MobileNumber2);
                                        break;
                                    }
                                    xlsRow.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.Email:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.Email))
                                    {
                                        xlsRow.Add(publicRegisterTeacher.Email);
                                        break;
                                    }
                                    xlsRow.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.FacebookId:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.FacebookId))
                                    {
                                        xlsRow.Add(publicRegisterTeacher.FacebookId);
                                        break;
                                    }
                                    xlsRow.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.Institute:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.Institute))
                                    {
                                        xlsRow.Add(publicRegisterTeacher.Institute);
                                        break;
                                    }
                                    xlsRow.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.Department:
                                    if (!string.IsNullOrEmpty(publicRegisterTeacher.Department))
                                    {
                                        xlsRow.Add(publicRegisterTeacher.Department);
                                        break;
                                    }
                                    xlsRow.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.HscPassingYear:
                                    if (publicRegisterTeacher.HscPassingYear != null && publicRegisterTeacher.HscPassingYear > 0)
                                    {
                                        xlsRow.Add(publicRegisterTeacher.HscPassingYear.ToString());
                                        break;
                                    }
                                    xlsRow.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.Religion:
                                    if (publicRegisterTeacher.Religion != null && publicRegisterTeacher.Religion > 0)
                                    {
                                        xlsRow.Add(_commonHelper.GetEmumIdToValue<Religion>(publicRegisterTeacher.Religion));
                                        break;
                                    }
                                    xlsRow.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.Gender:
                                    if (publicRegisterTeacher.Gender != null && publicRegisterTeacher.Gender > 0)
                                    {
                                        xlsRow.Add(_commonHelper.GetEmumIdToValue<Gender>(publicRegisterTeacher.Gender));
                                        break;
                                    }
                                    xlsRow.Add("-");
                                    break;
                                case TeacherRegisterApplicationConstants.TeacherActivity:
                                    string teacherActivityStr = "-";
                                    if (publicRegisterTeacher.TeacherActivity1 != null)
                                    {
                                        teacherActivityStr = publicRegisterTeacher.TeacherActivity1.Name;
                                        if (publicRegisterTeacher.TeacherActivity2 != null)
                                        {
                                            teacherActivityStr += ", " + publicRegisterTeacher.TeacherActivity2.Name;
                                        }
                                        if (publicRegisterTeacher.TeacherActivity3 != null)
                                        {
                                            teacherActivityStr += ", " + publicRegisterTeacher.TeacherActivity3.Name;
                                        }
                                    }
                                    xlsRow.Add(teacherActivityStr);
                                    break;
                                case TeacherRegisterApplicationConstants.SubjectPriority:
                                    string subjectPriorityStr = "-";
                                    if (publicRegisterTeacher.Subject1 != null)
                                    {
                                        subjectPriorityStr = publicRegisterTeacher.Subject1.Name;
                                        if (publicRegisterTeacher.Subject2 != null)
                                        {
                                            subjectPriorityStr += ", " + publicRegisterTeacher.Subject2.Name;
                                        }
                                        if (publicRegisterTeacher.Subject3 != null)
                                        {
                                            subjectPriorityStr += ", " + publicRegisterTeacher.Subject3.Name;
                                        }
                                    }
                                    xlsRow.Add(subjectPriorityStr);
                                    break;
                                case TeacherRegisterApplicationConstants.VersionPriority:
                                    string versionPriorityStr = "-";
                                    if (publicRegisterTeacher.VersionOfStudy1 != null)
                                    {
                                        versionPriorityStr = _commonHelper.GetEmumIdToValue<VersionOfStudy>(publicRegisterTeacher.VersionOfStudy1);
                                        if (publicRegisterTeacher.VersionOfStudy2 != null)
                                        {
                                            versionPriorityStr += ", " + _commonHelper.GetEmumIdToValue<VersionOfStudy>((int)publicRegisterTeacher.VersionOfStudy2);
                                        }
                                    }
                                    xlsRow.Add(versionPriorityStr);
                                    break;
                                case TeacherRegisterApplicationConstants.Status:
                                    if (publicRegisterTeacher.Tpin != null && publicRegisterTeacher.Tpin > 0)
                                    {
                                        status = "Assigned";
                                    }
                                    else
                                    {
                                        status = "Unassigned";
                                    }
                                    xlsRow.Add(status);
                                    break;
                                default:
                                    break;
                            }

                        }
                    }
                    #endregion

                    registerTeacherXlsList.Add(xlsRow);
                }

                ExcelGenerator.GenerateExcel(headerList, columnList, registerTeacherXlsList, footerList, "RegisterTeacherSearchListReport__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("Autoclose");
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return View("Autoclose");
            }
        }

        #endregion

        #region Delete Public Register Teacher 

        [HttpPost]
        public JsonResult DeletePublicTeacher(long id)
        {
            bool successState = false;
            string message = "";
            try
            {
                _teacherPublicRegistrationService.DeletePublicTeacher(id);
                message = "Public Teacher successfully removed from the list.";
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.SetExceptionMessage(ex);
            }

            return Json(new {IsSuccess = successState, Message= message});
        }

        #endregion

        #region Helper Function

        [HttpPost]
        public JsonResult RegisterTeacherListCount(RegisterTeacherList registerTeacherList)
        {
            string message = "";
            bool successState = false;
            int registerTeacherCount = 0;
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                RegisterTeacherList rtl = registerTeacherList;
                registerTeacherCount = _teacherPublicRegistrationService.GetRegisterTeacherCount(_userMenu, null, rtl.TeacherActivities, rtl.Subjects, rtl.Religions, rtl.VersionOfStudies, rtl.Genders, rtl.AcademicStudentStatus, rtl.TeacherApplicationStatus);
                successState = true;
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = WebHelper.CommonErrorMessage;
                throw;
            }
            return Json(new { registerTeacherCount = registerTeacherCount, IsSuccess = successState, Message = message });
        }

        #endregion
    }


}