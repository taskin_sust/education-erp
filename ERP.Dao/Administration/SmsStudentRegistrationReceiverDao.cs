﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Administration
{
    public interface ISmsStudentRegistrationReceiverDao : IBaseDao<SmsStudentRegistrationReceiver, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function
        
        #endregion

        #region Public API
       
        #endregion


    }
    public class SmsStudentRegistrationReceiverDao : BaseDao<SmsStudentRegistrationReceiver, long>, ISmsStudentRegistrationReceiverDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function


        #endregion

        #region List Loading Function
       

        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function
       

        #endregion

        #region Public API Functions
        
        #endregion



    }
}