﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Util;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Mapping.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Hr
{
    public interface IAllowanceSheetService : IBaseService
    {
        #region Operational Function

        void SaveOrUpdateTeamMemberAllowanceSheet(List<AllowanceSheet> allowanceSheetList);

        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        List<AllowanceSheet> LoadTeamMemberAllowanceSheet(List<UserMenu> userMenuList, AllowanceSheetFormViewModel allowanceSheetFormViewModel, bool saveObj = false);

        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        #endregion

    }
    public class AllowanceSheetService : BaseService, IAllowanceSheetService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrService");

        #endregion

        #region Propertise & Object Initialization

        private readonly CommonHelper _commonHelper;
        private readonly IAllowanceDao _hrAllowanceDao;
        private readonly ITeamMemberDao _teamMemberDao;
        private readonly IAllowanceSheetDao _allowanceSheetDao;
        private readonly IAttendanceSummaryDao _attendanceSummaryDao;
        private readonly IDailyAllowanceBlockDao _dailyAllowanceBlockDao;
        private readonly IDailySupportAllowanceSettingDao _dailySupportAllowanceSettingDao;
        private readonly IEmploymentHistoryDao _employmentHistoryDao;
        private readonly ISalaryHistoryDao _salaryHistoryDao;
        private readonly IHolidayWorkDao _holidayWorkDao;
        private readonly IExtradayAllowanceSettingDao _extradayAllowanceSettingDao;
        private readonly IOvertimeAllowanceSettingDao _overtimeAllowanceSettingDao;
        private readonly IOvertimeDao _overtimeDao;
        private readonly INightWorkAllowanceSettingDao _nightWorkAllowanceSettingDao;
        private readonly INightWorkDao _nightWorkDao;
        private readonly IMemberLoanZakatDao _memberLoanZakatDao;
        private readonly IMemberLoanCsrDao _memberLoanCsrDao;
        private readonly ISpecialAllowanceSettingDao _specialAllowanceSettingDao;
        private readonly IChildrenInfoDao _childrenInfoDao;
        private readonly IChildrenAllowanceSettingDao _childrenAllowanceSettingDao;
        private readonly IChildrenAllowanceBlockDao _allowanceBlockDao;
        private readonly IDepartmentDao _departmentDao;
        private readonly IDesignationDao _designationDao;
        private readonly ICampusDao _campusDao;
        private readonly IOrganizationDao _organizationDao;
        private readonly IAllowanceSheetFirstDetailsDao _allowanceSheetFirstDetailsDao;
        private readonly IAllowanceSheetSecondDetailsDao _allowanceSheetSecondDetailsDao;

        public AllowanceSheetService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _hrAllowanceDao = new AllowanceDao() { Session = session };
            _teamMemberDao = new TeamMemberDao { Session = session };
            _allowanceSheetDao = new AllowanceSheetDao { Session = session };
            _attendanceSummaryDao = new AttendanceSummaryDao { Session = session };
            _dailyAllowanceBlockDao = new DailyAllowanceBlockDao { Session = session };
            _employmentHistoryDao = new EmploymentHistoryDao { Session = session };
            _dailySupportAllowanceSettingDao = new DailySupportAllowanceSettingDao { Session = session };
            _salaryHistoryDao = new SalaryHistoryDao { Session = session };
            _holidayWorkDao = new HolidayWorkDao { Session = session };
            _extradayAllowanceSettingDao = new ExtradayAllowanceSettingDao { Session = session };
            _overtimeAllowanceSettingDao = new OvertimeAllowanceSettingDao { Session = session };
            _overtimeDao = new OvertimeDao { Session = session };
            _nightWorkAllowanceSettingDao = new NightWorkAllowanceSettingDao { Session = session };
            _nightWorkDao = new NightWorkDao { Session = session };
            _memberLoanZakatDao = new MemberLoanZakatDao { Session = session };
            _memberLoanCsrDao = new MemberLoanCsrDao { Session = session };
            _specialAllowanceSettingDao = new SpecialAllowanceSettingDao { Session = session };
            _childrenInfoDao = new ChildrenInfoDao { Session = session };
            _childrenAllowanceSettingDao = new ChildrenAllowanceSettingDao { Session = session };
            _allowanceBlockDao = new ChildrenAllowanceBlockDao { Session = session };
            _departmentDao = new DepartmentDao { Session = session };
            _designationDao = new DesignationDao { Session = session };
            _campusDao = new CampusDao { Session = session };
            _allowanceSheetFirstDetailsDao = new AllowanceSheetFirstDetailsDao { Session = session };
            _organizationDao = new OrganizationDao { Session = session };
            _allowanceSheetSecondDetailsDao = new AllowanceSheetSecondDetailsDao { Session = session };
        }
        #endregion

        #region Operational Functions

        public void SaveOrUpdateTeamMemberAllowanceSheet(List<AllowanceSheet> allowanceSheetList)
        {
            ITransaction trans = null;
            try
            {
                if (allowanceSheetList.Any())
                {
                    int row = 0;
                    DateTime dateTime = DateTime.Now;
                    long currentUserId = GetCurrentUserId();
                    List<TeamMember> teamMemberList = new List<TeamMember>();
                    List<AllowanceSheet> validAllowanceSheetList = new List<AllowanceSheet>();
                    foreach (AllowanceSheet allowanceSheet in allowanceSheetList)
                    {
                        if (allowanceSheet.Id == 0 || allowanceSheet.IsFinalSubmit == false)
                        {
                            ValidateAllowanceSheet(allowanceSheet);
                            validAllowanceSheetList.Add(allowanceSheet);
                        }
                    }

                    using (trans = Session.BeginTransaction())
                    {
                        foreach (AllowanceSheet allowanceSheet in validAllowanceSheetList)
                        {
                            //if (allowanceSheet.Id != 0 && allowanceSheet.IsFinalSubmit)
                            //    continue;
                            allowanceSheet.IsFinalSubmit = true;
                            allowanceSheet.FinalSubmissionDate = dateTime;
                            allowanceSheet.PreparedBy = currentUserId;
                            _allowanceSheetDao.SaveOrUpdate(allowanceSheet);
                            teamMemberList.Add(allowanceSheet.TeamMember);
                            row++;
                        }
                        if (row > 0)
                        {
                            if (teamMemberList.Any())
                            {
                                foreach (TeamMember teamMember in teamMemberList)
                                {
                                    #region set IsPost Daily Support AllowanceBlock 

                                    List<DailySupportAllowanceBlock> dailySupportAllowanceBlockListForIsPost = _dailyAllowanceBlockDao.LoadDailyAllownceBlocking(allowanceSheetList[0].DateFrom, allowanceSheetList[0].DateTo, teamMember.Id).ToList();
                                    if (dailySupportAllowanceBlockListForIsPost.Any())
                                    {
                                        foreach (DailySupportAllowanceBlock dailySupportAllowanceBlock in dailySupportAllowanceBlockListForIsPost)
                                        {
                                            dailySupportAllowanceBlock.IsPost = true;
                                            _dailyAllowanceBlockDao.SaveOrUpdate(dailySupportAllowanceBlock);
                                        }
                                    }

                                    #endregion

                                    #region set IsPost Night Work

                                    List<NightWork> nightWorkListForIsPost = _nightWorkDao.LoadNightWork(allowanceSheetList[0].DateFrom, allowanceSheetList[0].DateTo, teamMember.Id).ToList();
                                    if (nightWorkListForIsPost.Any())
                                    {
                                        foreach (NightWork nightWork in nightWorkListForIsPost)
                                        {
                                            nightWork.IsPost = true;
                                            _nightWorkDao.SaveOrUpdate(nightWork);
                                        }
                                    }

                                    #endregion

                                    #region set IsPost Over Time

                                    List<Overtime> overtimeListForIsPost = _overtimeDao.LoadOvertime(allowanceSheetList[0].DateFrom, allowanceSheetList[0].DateTo, teamMember.Id).ToList();
                                    if (overtimeListForIsPost.Any())
                                    {
                                        foreach (Overtime overtime in overtimeListForIsPost)
                                        {
                                            overtime.IsPost = true;
                                            _overtimeDao.SaveOrUpdate(overtime);
                                        }
                                    }

                                    #endregion

                                    #region set IsPost Holiday Work

                                    List<HolidayWork> holidayWorkListForIsPost = _holidayWorkDao.LoadHolidayWork(allowanceSheetList[0].DateFrom, allowanceSheetList[0].DateTo, teamMember.Id).ToList();
                                    if (holidayWorkListForIsPost.Any())
                                    {
                                        foreach (HolidayWork holidayWork in holidayWorkListForIsPost)
                                        {
                                            holidayWork.IsPost = true;
                                            _holidayWorkDao.SaveOrUpdate(holidayWork);
                                        }
                                    }

                                    #endregion

                                }
                            }

                            trans.Commit();
                        }
                    }
                }
                else
                {
                    throw new InvalidDataException("No TeamMember Found to Save Allownace Sheet!");
                }
            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            finally
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
            }
        }

        private void ValidateAllowanceSheet(AllowanceSheet allowanceSheet)
        {
            if(allowanceSheet == null)
                throw new InvalidDataException("Invalid Allowance Sheet Found!");   
            if(allowanceSheet.TeamMember == null)
                throw new InvalidDataException("Invalid TeamMember Found!");
            AllowanceSheet tempAllowanceSheet = _allowanceSheetDao.GetTeamMemberAllowanceSheet(allowanceSheet.TeamMember.Id, allowanceSheet.DateFrom, allowanceSheet.DateTo);
            int pin = allowanceSheet.TeamMember.Pin;
            string commonMsg = " for this Pin (" + pin + ")!";
            if (tempAllowanceSheet != null)
                throw new InvalidDataException("Already have allowance sheet for this Pin (" + allowanceSheet.TeamMember.Pin + ")!");

            #region first Details Check
            if (allowanceSheet.AllowanceSheetFirstDetails.Any())
            {
                foreach (AllowanceSheetFirstDetails firstDetail in allowanceSheet.AllowanceSheetFirstDetails)
                {
                    string commonMsgFirstDetails = " for this Pin (" + pin + ") for Allowance Date " + firstDetail.AllowanceDate.ToString("yyyy-MM-dd") + "!";
                    if (firstDetail.AllowanceSheet == null)
                        throw new InvalidDataException("Invalid allowance sheet" + commonMsgFirstDetails);
                    if (firstDetail.TeamMemberDepartment == null)
                        throw new InvalidDataException("Invalid allowance sheet's Department" + commonMsgFirstDetails);
                    if (firstDetail.TeamMemberDesignation == null)
                        throw new InvalidDataException("Invalid allowance sheet's Designation" + commonMsgFirstDetails);
                    if (firstDetail.TeamMemberBranch == null)
                        throw new InvalidDataException("Invalid allowance sheet's Branch" + commonMsgFirstDetails);
                    if (firstDetail.TeamMemberCampus == null)
                        throw new InvalidDataException("Invalid allowance sheet's Campus" + commonMsgFirstDetails);
                    if (firstDetail.DailySupportAllowanceAmount > 0 && firstDetail.DailySupportAllowanceSetting == null)
                        throw new InvalidDataException("Invalid allowance sheet's for Daily Support Allowance Setting" + commonMsgFirstDetails);
                    if (firstDetail.ExtradayAllowanceAmount > 0 && firstDetail.ExtradayAllowanceSetting == null)
                        throw new InvalidDataException("Invalid allowance sheet's for Extra Day Allowance Setting" + commonMsgFirstDetails);
                    if (firstDetail.OvertimeAllowanceAmount > 0 && firstDetail.OvertimeAllowanceSetting == null)
                        throw new InvalidDataException("Invalid allowance sheet's for Overtime Allowance Setting" + commonMsgFirstDetails);
                    if (firstDetail.NightWorkAllowanceAmount > 0 && firstDetail.NightWorkAllowanceSetting == null)
                        throw new InvalidDataException("Invalid allowance sheet's for NightWork Allowance Setting" + commonMsgFirstDetails); 
                    if ( (firstDetail.DailySupportAllowanceAmount > 0
                        || firstDetail.ExtradayAllowanceAmount > 0
                        || firstDetail.OvertimeAllowanceAmount > 0
                        || firstDetail.NightWorkAllowanceAmount > 0 ) && firstDetail.TotalDailyAllowanceAmount <= 0)
                        throw new InvalidDataException("Invalid allowance sheet's Total Allowance Amount" + commonMsgFirstDetails);
                }
            }
            #endregion

            #region Second details check
            if (allowanceSheet.AllowanceSheetSecondDetails.Any())
            {
                foreach (AllowanceSheetSecondDetails secondDetail in allowanceSheet.AllowanceSheetSecondDetails)
                {
                    string commonMsgSecondDetails = " for this Pin (" + pin + ") for Allowance Date " + secondDetail.AllowanceDate.ToString("yyyy-MM-dd") + "!";
                    if (secondDetail.AllowanceSheet == null)
                        throw new InvalidDataException("Invalid allowance sheet" + commonMsgSecondDetails);
                    if (secondDetail.TeamMemberDepartment == null)
                        throw new InvalidDataException("Invalid allowance sheet's Department" + commonMsgSecondDetails);
                    if (secondDetail.TeamMemberDesignation == null)
                        throw new InvalidDataException("Invalid allowance sheet's Designation" + commonMsgSecondDetails);
                    if (secondDetail.TeamMemberBranch == null)
                        throw new InvalidDataException("Invalid allowance sheet's Branch" + commonMsgSecondDetails);
                    if (secondDetail.TeamMemberCampus == null)
                        throw new InvalidDataException("Invalid allowance sheet's Campus" + commonMsgSecondDetails);

                    if (secondDetail.ChildrenAllowanceAmount > 0 && (secondDetail.ChildrenAllowanceSetting == null || secondDetail.ChildrenInfo == null))
                        throw new InvalidDataException("Invalid allowance sheet's for Children Allowance Setting or no Children Info found " + commonMsgSecondDetails);
                    if (secondDetail.SpecialAllowanceAmount > 0 && secondDetail.SpecialAllowanceSetting == null)
                        throw new InvalidDataException("Invalid allowance sheet's fro Special Allowance Setting" + commonMsgSecondDetails);
                    if (secondDetail.MemberLoanZakatAmount > 0 && secondDetail.MemberLoanZakat == null)
                        throw new InvalidDataException("Invalid allowance sheet's for Member Zakat" + commonMsgSecondDetails);
                    if (secondDetail.MemberLoanCsrAmount > 0 && secondDetail.MemberLoanCsr == null)
                        throw new InvalidDataException("Invalid allowance sheet's for Member Csr" + commonMsgSecondDetails);
                    if ((secondDetail.ChildrenAllowanceAmount > 0
                        || secondDetail.SpecialAllowanceAmount > 0
                        || secondDetail.MemberLoanZakatAmount > 0
                        || secondDetail.MemberLoanCsrAmount > 0) && secondDetail.TotalAllowanceAmount <= 0)
                        throw new InvalidDataException("Invalid allowance sheet's Total Alloeance Amount" + commonMsgSecondDetails);
                }
            }
            #endregion

            if (allowanceSheet.TeamMemberDepartment == null)
                throw new InvalidDataException("Invalid allowance sheet's Department" + commonMsg);
            if (allowanceSheet.TeamMemberDesignation == null)
                throw new InvalidDataException("Invalid allowance sheet's Designation" + commonMsg);
            if (allowanceSheet.TeamMemberBranch == null)
                throw new InvalidDataException("Invalid allowance sheet's Branch" + commonMsg);
            if (allowanceSheet.TeamMemberCampus == null)
                throw new InvalidDataException("Invalid allowance sheet's Campus" + commonMsg);

        }

        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public List<AllowanceSheet> LoadTeamMemberAllowanceSheet(List<UserMenu> userMenuList, AllowanceSheetFormViewModel allowanceSheetFormViewModel, bool saveObj = false)
        {
            try
            {
                AllowanceSheetFormViewModel asfvm = allowanceSheetFormViewModel;
                List<AllowanceSheet> allowanceSheetList = new List<AllowanceSheet>();
                List<int> pinList = null;

                string pinListString = (!String.IsNullOrEmpty(asfvm.PinList)) ? asfvm.PinList.Trim() : "";
                Regex regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                pinListString = regex.Replace(pinListString, @",");
                if (!String.IsNullOrEmpty(pinListString))
                {
                    pinList = pinListString.Split(',').Select(Int32.Parse).ToList();
                }

                List<long> authOrgIdList = AuthHelper.LoadOrganizationIdList(userMenuList, (asfvm.OrganizationId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(asfvm.OrganizationId) : null);
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenuList, authOrgIdList, null, (asfvm.BranchId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(asfvm.BranchId) : null);

                if (!authOrgIdList.Any() || !authBranchIdList.Any())
                {
                    throw new InvalidDataException("Your are not authorized to access this operation");
                }

                DateTime searchingDate = new DateTime(asfvm.Year, (asfvm.Month), 1, 23, 59, 59).AddMonths(1).AddDays(-1);
                List<TeamMember> authoTeamMemberList = _teamMemberDao.LoadHrAuthorizedTeamMember(searchingDate, authBranchIdList, (asfvm.CampusId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(asfvm.CampusId) : null, (asfvm.DepartmentId != SelectionType.SelelectAll) ? _commonHelper.ConvertIdToList(asfvm.DepartmentId) : null, null, pinList, false, (int)asfvm.TeamMemberSearchType).ToList();

                DateTime dateFrom = new DateTime(searchingDate.Year, searchingDate.Month, 01);
                DateTime dateTo = searchingDate;

                CheckBeforeReport(authoTeamMemberList, dateFrom, dateTo);
                allowanceSheetList = LoadTeamMemberAllowanceSheet(authoTeamMemberList, dateFrom, dateTo, saveObj);

                return allowanceSheetList;

            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public List<AllowanceSheet> LoadTeamMemberAllowanceSheet(List<TeamMember> authoTeamMemberList, DateTime dateFrom, DateTime dateTo, bool saveObj = false)
          {
            try
            {
                List<AllowanceSheet> allowanceSheetList = new List<AllowanceSheet>();
                long currentUserId = GetCurrentUserId();
                DateTime dateTime = DateTime.Now;

                List<DailySupportAllowanceSetting> dailySupportAllowanceSettingObjList = new List<DailySupportAllowanceSetting>();
                List<ExtradayAllowanceSetting> extradayAllowanceSettingObjList = new List<ExtradayAllowanceSetting>();
                List<OvertimeAllowanceSetting> overtimeAllowanceSettingObjList = new List<OvertimeAllowanceSetting>();
                List<NightWorkAllowanceSetting> nightWorkAllowanceSettingObjList = new List<NightWorkAllowanceSetting>();

                List<MemberLoanZakat> memberLoanZakatObjList = new List<MemberLoanZakat>();
                List<MemberLoanCsr> memberLoanCsrObjList = new List<MemberLoanCsr>();
                List<SpecialAllowanceSetting> specialAllowanceSettingObjList = new List<SpecialAllowanceSetting>();
                List<ChildrenAllowanceSetting> childrenAllowanceSettingObjList = new List<ChildrenAllowanceSetting>();

                List<Department> departmentObjList = new List<Department>();
                List<Designation> designationObjList = new List<Designation>();
                List<Campus> campusObjList = new List<Campus>();

                List<ChildrenInfo> teamMemberChildrenInfoList = new List<ChildrenInfo>();

                if (authoTeamMemberList.Any())
                {
                    teamMemberChildrenInfoList = _childrenInfoDao.LoadTeamMemberChildrenInfo(authoTeamMemberList.Select(x => x.Id).ToList(), dateTo).ToList();
                    foreach (TeamMember teamMember in authoTeamMemberList)
                    {

                        #region Allowance Sheet Initial Set up

                        AllowanceSheet tempAllowanceSheet = _allowanceSheetDao.GetTeamMemberAllowanceSheet(teamMember.Id, dateFrom, dateTo);
                        if (tempAllowanceSheet == null)
                        {
                            tempAllowanceSheet = new AllowanceSheet();
                        }
                        else if (tempAllowanceSheet.IsFinalSubmit)
                        {
                            allowanceSheetList.Add(tempAllowanceSheet);
                            continue;
                        }
                        else
                        {
                            tempAllowanceSheet.AllowanceSheetFirstDetails.Clear();
                            tempAllowanceSheet.AllowanceSheetSecondDetails.Clear();
                        }
                        
                        #endregion

                        Organization organization = null;
                        Department department = null;
                        Designation designation = null;
                        Campus campus = null;
                        Branch branch = null;
                        Organization salaryOrganization = null;
                        Department salaryDepartment = null;
                        Designation salaryDesignation = null;
                        Campus salaryCampus = null;
                        Branch salaryBranch = null;
                        SalaryHistory salaryHistory = null;
                        long organizationId = 0;
                        int employmentStatus = 0;
                        decimal dailySupportAllowanceAmount = 0;
                        decimal extraDayAllowanceAmount = 0;
                        decimal overtimeAllowanceAmount = 0;
                        decimal nightWorkAllowanceAmount = 0;
                        decimal childrenEducationAllowanceAmount = 0;
                        decimal specialAllowanceAmount = 0;
                        decimal zakatAllowanceAmount = 0;
                        decimal csrAllowanceAmount = 0;
                        decimal totalAllowanceAmount = 0;

                        #region Get First Details DailySupportAllowance ExtradayAllowance OvertimeAllowance NightWorkAllowance

                        List<TeamMemberAllowanceSheetFirstDetailsDto> firstDetailsDtoList = _allowanceSheetFirstDetailsDao.LoadAllowanceSheetFirstDetailsDto(teamMember.Id, dateFrom, dateTo).ToList();
                        List<AllowanceSheetFirstDetails> firstDetailsList = new List<AllowanceSheetFirstDetails>();
                        if (firstDetailsDtoList.Any())
                        {
                            long? departmentId = firstDetailsDtoList[0].JobDepartmentId;
                            long? designationId = firstDetailsDtoList[0].JobDesignationId;
                            long? campusId = firstDetailsDtoList[0].JobCampusId;
                            long? salaryDepartmentId = firstDetailsDtoList[0].SalaryDepartmentId;
                            long? salaryDesignationId = firstDetailsDtoList[0].SalaryDesignationId;
                            long? salaryCampusId = firstDetailsDtoList[0].SalaryCampusId;
                            long? salaryHistoryId = firstDetailsDtoList[0].SalaryId;
                            organizationId = firstDetailsDtoList[0].JobOrganizationId ?? 0;
                            employmentStatus = firstDetailsDtoList[0].EmployeeStatus ?? 0;

                            #region Department
                            if (departmentId != null)
                            {
                                if (departmentObjList.Any() && departmentObjList.Where(x => x.Id == departmentId).ToList().Any())
                                    department = departmentObjList.SingleOrDefault(x => x.Id == departmentId);
                                else
                                {
                                    department = _departmentDao.LoadById(departmentId.Value);
                                    if (department != null)
                                        departmentObjList.Add(department);
                                }
                            }
                            if (salaryDepartmentId != null)
                            {
                                if (departmentObjList.Any() && departmentObjList.Where(x => x.Id == salaryDepartmentId).ToList().Any())
                                    salaryDepartment = departmentObjList.SingleOrDefault(x => x.Id == salaryDepartmentId);
                                else
                                {
                                    salaryDepartment = _departmentDao.LoadById(salaryDepartmentId.Value);
                                    if (salaryDepartment != null)
                                        departmentObjList.Add(salaryDepartment);
                                }
                            }
                            #endregion

                            #region Designation
                            if (designationId != null)
                            {
                                if (designationObjList.Any() && designationObjList.Where(x => x.Id == designationId).ToList().Any())
                                    designation = designationObjList.SingleOrDefault(x => x.Id == designationId);
                                else
                                {
                                    designation = _designationDao.LoadById(designationId.Value);
                                    designationObjList.Add(designation);
                                }
                            }
                            #endregion

                            #region campus
                            if (campusId != null)
                            {
                                if (campusObjList.Any() && campusObjList.Where(x => x.Id == campusId).ToList().Any())
                                {
                                    campus = campusObjList.SingleOrDefault(x => x.Id == campusId);
                                    branch = campus.Branch;
                                }
                                else
                                {
                                    campus = _campusDao.LoadById(campusId.Value);
                                    if (campus != null)
                                    {
                                        campusObjList.Add(campus);
                                        branch = campus.Branch;
                                    }
                                }
                            }
                            if (salaryCampusId != null)
                            {
                                if (campusObjList.Any() && campusObjList.Where(x => x.Id == salaryCampusId).ToList().Any())
                                {
                                    salaryCampus = campusObjList.SingleOrDefault(x => x.Id == salaryCampusId);
                                    salaryBranch = salaryCampus.Branch;
                                }
                                else
                                {
                                    salaryCampus = _campusDao.LoadById(salaryCampusId.Value);
                                    if (salaryCampus != null)
                                    {
                                        campusObjList.Add(salaryCampus);
                                        salaryBranch = salaryCampus.Branch;
                                    }
                                }
                            }
                            #endregion

                            salaryHistory = (salaryHistoryId != null) ? _salaryHistoryDao.LoadById(salaryHistoryId.Value) : null;


                           
                            foreach (TeamMemberAllowanceSheetFirstDetailsDto sheetFirstDetailsDto in firstDetailsDtoList)
                            {
                                AllowanceSheetFirstDetails allowanceSheetFirstDetails = new AllowanceSheetFirstDetails();
                                decimal totalDailyAllowanceAmount = 0;
                                allowanceSheetFirstDetails.TeamMemberDepartment = department;
                                allowanceSheetFirstDetails.TeamMemberDesignation = designation;
                                allowanceSheetFirstDetails.TeamMemberBranch = branch;
                                allowanceSheetFirstDetails.TeamMemberCampus = campus;
                                allowanceSheetFirstDetails.AllowanceDate = sheetFirstDetailsDto.Date;
                                allowanceSheetFirstDetails.AllowanceSheet = tempAllowanceSheet;

                                allowanceSheetFirstDetails.Status = AllowanceSheetFirstDetails.EntityStatus.Active;
                                allowanceSheetFirstDetails.CreateBy = currentUserId;
                                allowanceSheetFirstDetails.ModifyBy = currentUserId;
                                allowanceSheetFirstDetails.CreationDate = dateTime;

                                #region Daily Support Allowance Calculation
                                if (sheetFirstDetailsDto.DailySupportAllowanceSettingId != null && sheetFirstDetailsDto.DailySupportAllowanceSettingId > 0)
                                {
                                    DailySupportAllowanceSetting tempDailySupportAllowanceSetting = null;
                                    if (saveObj)
                                    {
                                        if (dailySupportAllowanceSettingObjList.Any() && dailySupportAllowanceSettingObjList.Where(x => x.Id == sheetFirstDetailsDto.DailySupportAllowanceSettingId).ToList().Any())
                                        {
                                            tempDailySupportAllowanceSetting = dailySupportAllowanceSettingObjList.SingleOrDefault(x => x.Id == sheetFirstDetailsDto.DailySupportAllowanceSettingId);
                                        }
                                        else
                                        {
                                            tempDailySupportAllowanceSetting = _dailySupportAllowanceSettingDao.LoadById(sheetFirstDetailsDto.DailySupportAllowanceSettingId.Value);
                                            if (tempDailySupportAllowanceSetting != null)
                                                dailySupportAllowanceSettingObjList.Add(tempDailySupportAllowanceSetting);
                                        }
                                    }
                                    decimal tempAmount = Decimal.Round(sheetFirstDetailsDto.DailySupportAllowanceAmount.Value, 2);
                                    allowanceSheetFirstDetails.DailySupportAllowanceSetting = tempDailySupportAllowanceSetting;
                                    allowanceSheetFirstDetails.DailySupportAllowanceAmount = tempAmount;
                                    totalAllowanceAmount += tempAmount;
                                    totalDailyAllowanceAmount += tempAmount;
                                    dailySupportAllowanceAmount += tempAmount;
                                }
                                #endregion Daily Support Allowance Calculation End

                                #region Extraday Allowance Calculation
                                if (sheetFirstDetailsDto.ExtraDayAllowanceId != null && sheetFirstDetailsDto.ExtraDayAllowanceId > 0)
                                {
                                    ExtradayAllowanceSetting tempExtradayAllowanceSetting = null;
                                    if (saveObj)
                                    {
                                        if (extradayAllowanceSettingObjList.Any() && extradayAllowanceSettingObjList.Where(x => x.Id == sheetFirstDetailsDto.ExtraDayAllowanceId).ToList().Any())
                                        {
                                            tempExtradayAllowanceSetting = extradayAllowanceSettingObjList.SingleOrDefault(x => x.Id == sheetFirstDetailsDto.ExtraDayAllowanceId);
                                        }
                                        else
                                        {
                                            tempExtradayAllowanceSetting = _extradayAllowanceSettingDao.LoadById(sheetFirstDetailsDto.ExtraDayAllowanceId.Value);
                                            if (tempExtradayAllowanceSetting != null)
                                                extradayAllowanceSettingObjList.Add(tempExtradayAllowanceSetting);
                                        }
                                    }
                                   
                                    decimal tempAmount = Decimal.Round(sheetFirstDetailsDto.ExtradayAllowanceAmount.Value, 2);
                                    allowanceSheetFirstDetails.ExtradayAllowanceSetting = tempExtradayAllowanceSetting;
                                    allowanceSheetFirstDetails.ExtradayAllowanceAmount = tempAmount;
                                    totalAllowanceAmount += tempAmount;
                                    totalDailyAllowanceAmount += tempAmount;
                                    extraDayAllowanceAmount += tempAmount;
                                }
                                #endregion Extraday Allowance Calculation End

                                #region Overtime Allowance Calculation
                                if (sheetFirstDetailsDto.OverTimeAllowanceId != null && sheetFirstDetailsDto.OverTimeAllowanceId > 0)
                                {
                                    OvertimeAllowanceSetting tempOvertimeAllowanceSetting = null;
                                    if (saveObj)
                                    {
                                        if (overtimeAllowanceSettingObjList.Any() && overtimeAllowanceSettingObjList.Where(x => x.Id == sheetFirstDetailsDto.OverTimeAllowanceId).ToList().Any())
                                        {
                                            tempOvertimeAllowanceSetting = overtimeAllowanceSettingObjList.SingleOrDefault(x => x.Id == sheetFirstDetailsDto.OverTimeAllowanceId);
                                        }
                                        else
                                        {
                                            tempOvertimeAllowanceSetting = _overtimeAllowanceSettingDao.LoadById(sheetFirstDetailsDto.OverTimeAllowanceId.Value);
                                            if (tempOvertimeAllowanceSetting != null)
                                                overtimeAllowanceSettingObjList.Add(tempOvertimeAllowanceSetting);
                                        }
                                    }
                                   
                                    decimal tempAmount = Decimal.Round(sheetFirstDetailsDto.OvertimeAllowanceAmount.Value, 2);
                                    allowanceSheetFirstDetails.OvertimeAllowanceSetting = tempOvertimeAllowanceSetting;
                                    allowanceSheetFirstDetails.OvertimeAllowanceAmount = tempAmount;
                                    totalAllowanceAmount += tempAmount;
                                    totalDailyAllowanceAmount += tempAmount;
                                    overtimeAllowanceAmount += tempAmount;
                                }
                                #endregion Overtime Allowance Calculation End

                                #region NightWork Allowance Calculation
                                if (sheetFirstDetailsDto.NightWorkAllowanceId != null && sheetFirstDetailsDto.NightWorkAllowanceId > 0)
                                {
                                    NightWorkAllowanceSetting tempNightWorkAllowanceSetting = null;
                                    if (saveObj)
                                    {
                                        if (nightWorkAllowanceSettingObjList.Any() && nightWorkAllowanceSettingObjList.Where(x => x.Id == sheetFirstDetailsDto.NightWorkAllowanceId).ToList().Any())
                                        {
                                            tempNightWorkAllowanceSetting = nightWorkAllowanceSettingObjList.SingleOrDefault(x => x.Id == sheetFirstDetailsDto.NightWorkAllowanceId);
                                        }
                                        else
                                        {
                                            tempNightWorkAllowanceSetting = _nightWorkAllowanceSettingDao.LoadById(sheetFirstDetailsDto.NightWorkAllowanceId.Value);
                                            if (tempNightWorkAllowanceSetting != null)
                                                nightWorkAllowanceSettingObjList.Add(tempNightWorkAllowanceSetting);
                                        }
                                    }
                                    
                                    decimal tempAmount = Decimal.Round(sheetFirstDetailsDto.NightWorkAllowanceAmount.Value, 2);
                                    allowanceSheetFirstDetails.NightWorkAllowanceSetting = tempNightWorkAllowanceSetting;
                                    allowanceSheetFirstDetails.NightWorkAllowanceAmount = tempAmount;
                                    totalAllowanceAmount += tempAmount;
                                    totalDailyAllowanceAmount += tempAmount;
                                    nightWorkAllowanceAmount += tempAmount;
                                }
                                #endregion NightWork Allowance Calculation End

                                allowanceSheetFirstDetails.TotalDailyAllowanceAmount = totalDailyAllowanceAmount;
                                firstDetailsList.Add(allowanceSheetFirstDetails);
                            }
                        }

                        #endregion

                        #region Second Details MemberLoadZakat MemberLoadCsr SpecialAllowance ChildrenAllowance

                        List<TeamMemberAllowanceSheetSecondDetailsDto> secondDetailsDtoList = _allowanceSheetSecondDetailsDao.LoadAllowanceSheetSecondDetailsDto(teamMember.Id, organizationId, employmentStatus, dateFrom, dateTo).ToList();
                        List<AllowanceSheetSecondDetails> secondDetailsList = new List<AllowanceSheetSecondDetails>();
                        if (secondDetailsDtoList.Any())
                        {
                            #region MemberLoadZakat AllowanceSheet Start

                            List<TeamMemberAllowanceSheetSecondDetailsDto> zakatSecondDetailsDtoList = secondDetailsDtoList.Where(x => x.MemberLoanZakatId > 0).ToList();
                            if (zakatSecondDetailsDtoList.Any())
                            {
                                foreach (TeamMemberAllowanceSheetSecondDetailsDto zakatSecondDetailsDto in zakatSecondDetailsDtoList)
                                {
                                    MemberLoanZakat memberLoanZakat = null;
                                    decimal tempAmount = zakatSecondDetailsDto.ZakatAmount.Value;
                                    if (saveObj)
                                    {
                                        if (memberLoanZakatObjList.Any() && memberLoanZakatObjList.Where(x => x.Id == zakatSecondDetailsDto.MemberLoanZakatId).ToList().Any())
                                            memberLoanZakat = memberLoanZakatObjList.SingleOrDefault(x => x.Id == zakatSecondDetailsDto.MemberLoanZakatId);
                                        else
                                        {
                                            memberLoanZakat = _memberLoanZakatDao.LoadById(zakatSecondDetailsDto.MemberLoanZakatId.Value);
                                            if (memberLoanZakat != null)
                                                memberLoanZakatObjList.Add(memberLoanZakat);
                                        }
                                    }

                                    AllowanceSheetSecondDetails tempAllowanceSheetSecondDetails = new AllowanceSheetSecondDetails
                                    {
                                        
                                        AllowanceSheet = tempAllowanceSheet,
                                        TeamMemberDepartment = department,
                                        TeamMemberBranch = branch,
                                        TeamMemberCampus = campus,
                                        TeamMemberDesignation = designation,
                                        AllowanceDate = zakatSecondDetailsDto.Date,
                                        MemberLoanZakat = memberLoanZakat,
                                        MemberLoanZakatAmount = tempAmount,
                                        TotalAllowanceAmount = tempAmount,
                                        Status = AllowanceSheetSecondDetails.EntityStatus.Active,
                                        CreateBy = currentUserId,
                                        ModifyBy = currentUserId,
                                        CreationDate = dateTime,
                                        ModificationDate = dateTime
                                    };
                                    totalAllowanceAmount += tempAmount;
                                    zakatAllowanceAmount += tempAmount;
                                    secondDetailsList.Add(tempAllowanceSheetSecondDetails);
                                }
                            }

                            #endregion MemberLoadZakat AllowanceSheet End

                            #region MemberLoadCsr AllowanceSheet Start

                            List<TeamMemberAllowanceSheetSecondDetailsDto> csrSecondDetailsDtoList = secondDetailsDtoList.Where(x => x.MemberLoanCsrId > 0).ToList();
                            if (csrSecondDetailsDtoList.Any())
                            {
                                foreach (TeamMemberAllowanceSheetSecondDetailsDto csrSecondDetailsDto in csrSecondDetailsDtoList)
                                {
                                    MemberLoanCsr memberLoanCsr = null;
                                    decimal tempAmount = csrSecondDetailsDto.CsrAmount.Value;
                                    if (saveObj)
                                    {
                                        if (memberLoanCsrObjList.Any() && memberLoanCsrObjList.Where(x => x.Id == csrSecondDetailsDto.MemberLoanCsrId).ToList().Any())
                                            memberLoanCsr = memberLoanCsrObjList.SingleOrDefault(x => x.Id == csrSecondDetailsDto.MemberLoanCsrId);
                                        else
                                        {
                                            memberLoanCsr = _memberLoanCsrDao.LoadById(csrSecondDetailsDto.MemberLoanCsrId.Value);
                                            if (memberLoanCsr != null)
                                                memberLoanCsrObjList.Add(memberLoanCsr);
                                        }
                                    }
                                    
                                    AllowanceSheetSecondDetails tempAllowanceSheetSecondDetails = new AllowanceSheetSecondDetails
                                    {
                                        AllowanceSheet = tempAllowanceSheet,
                                        TeamMemberDepartment = department,
                                        TeamMemberBranch = branch,
                                        TeamMemberCampus = campus,
                                        TeamMemberDesignation = designation,
                                        AllowanceDate = csrSecondDetailsDto.Date,
                                        MemberLoanCsr = memberLoanCsr,
                                        MemberLoanCsrAmount = tempAmount,
                                        TotalAllowanceAmount = tempAmount,
                                        Status = AllowanceSheetSecondDetails.EntityStatus.Active,
                                        CreateBy = currentUserId,
                                        ModifyBy = currentUserId,
                                        CreationDate = dateTime,
                                        ModificationDate = dateTime
                                    };
                                    totalAllowanceAmount += tempAmount;
                                    csrAllowanceAmount += tempAmount;
                                    secondDetailsList.Add(tempAllowanceSheetSecondDetails);
                                }
                            }

                            #endregion MemberLoadZakat AllowanceSheet End

                            #region SpecialAllowance AllowanceSheet Start

                            List<TeamMemberAllowanceSheetSecondDetailsDto> specialAllowanceSecondDetailsDtoList = secondDetailsDtoList.Where(x => x.SpecialAllowanceSettingId > 0).ToList();
                            if (specialAllowanceSecondDetailsDtoList.Any())
                            {

                                #region Daily PaymentType
                                List<TeamMemberAllowanceSheetSecondDetailsDto> specialAllowanceDailySecondDetailsDtoList = specialAllowanceSecondDetailsDtoList.Where(x => x.SpecialAllowancePaymentType == (int)UdvashERP.BusinessRules.Hr.PaymentType.Daily).ToList();

                                foreach (TeamMemberAllowanceSheetSecondDetailsDto specialAllowanceSecondDetailsDto in specialAllowanceDailySecondDetailsDtoList)
                                {
                                    int closeDay = dateTo.Day;
                                    if (specialAllowanceSecondDetailsDto.SpecialAllowanceClosingDate != null && dateTo >= specialAllowanceSecondDetailsDto.SpecialAllowanceClosingDate.Value.Date)
                                        closeDay = specialAllowanceSecondDetailsDto.SpecialAllowanceClosingDate.Value.Day;
                                   
                                    SpecialAllowanceSetting specialAllowanceSetting = null;
                                    decimal tempAmount = specialAllowanceSecondDetailsDto.SpecialAllowanceAmount.Value;
                                    if (saveObj)
                                    {
                                        if (specialAllowanceSettingObjList.Any() && specialAllowanceSettingObjList.Where(x => x.Id == specialAllowanceSecondDetailsDto.SpecialAllowanceSettingId).ToList().Any())
                                            specialAllowanceSetting = specialAllowanceSettingObjList.SingleOrDefault(x => x.Id == specialAllowanceSecondDetailsDto.SpecialAllowanceSettingId);
                                        else
                                        {
                                            specialAllowanceSetting = _specialAllowanceSettingDao.LoadById(specialAllowanceSecondDetailsDto.SpecialAllowanceSettingId.Value);
                                            if (specialAllowanceSetting != null)
                                                specialAllowanceSettingObjList.Add(specialAllowanceSetting);
                                        }
                                    }

                                    for (int i = specialAllowanceSecondDetailsDto.Date.Day; i <= closeDay; i++)
                                    {

                                        AllowanceSheetSecondDetails tempAllowanceSheetSecondDetails = new AllowanceSheetSecondDetails
                                        {
                                            AllowanceSheet = tempAllowanceSheet,
                                            TeamMemberDepartment = department,
                                            TeamMemberBranch = branch,
                                            TeamMemberCampus = campus,
                                            TeamMemberDesignation = designation,
                                            AllowanceDate = specialAllowanceSecondDetailsDto.Date,
                                            SpecialAllowanceSetting = specialAllowanceSetting,
                                            SpecialAllowanceAmount = tempAmount,
                                            TotalAllowanceAmount = tempAmount,
                                            Status = AllowanceSheetSecondDetails.EntityStatus.Active,
                                            CreateBy = currentUserId,
                                            ModifyBy = currentUserId,
                                            CreationDate = dateTime,
                                            ModificationDate = dateTime
                                        };
                                        totalAllowanceAmount += tempAmount;
                                        specialAllowanceAmount += tempAmount;
                                        secondDetailsList.Add(tempAllowanceSheetSecondDetails);
                                    }
                                }
                                #endregion

                                #region Once montly Yearly PaymentType
                                List<TeamMemberAllowanceSheetSecondDetailsDto> specialAllowanceOmySecondDetailsDtoList = specialAllowanceSecondDetailsDtoList.Where(x => x.SpecialAllowancePaymentType != (int)UdvashERP.BusinessRules.Hr.PaymentType.Daily).ToList();
                                foreach (TeamMemberAllowanceSheetSecondDetailsDto specialAlloanceSecondDetailsDto in specialAllowanceOmySecondDetailsDtoList)
                                {
                                    SpecialAllowanceSetting specialAllowanceSetting = null;
                                    decimal tempAmount = specialAlloanceSecondDetailsDto.SpecialAllowanceAmount.Value;
                                    if (saveObj)
                                    {
                                        if (specialAllowanceSettingObjList.Any() && specialAllowanceSettingObjList.Where(x => x.Id == specialAlloanceSecondDetailsDto.SpecialAllowanceSettingId).ToList().Any())
                                            specialAllowanceSetting = specialAllowanceSettingObjList.SingleOrDefault(x => x.Id == specialAlloanceSecondDetailsDto.SpecialAllowanceSettingId);
                                        else
                                        {
                                            specialAllowanceSetting = _specialAllowanceSettingDao.LoadById(specialAlloanceSecondDetailsDto.SpecialAllowanceSettingId.Value);
                                            if (specialAllowanceSetting != null)
                                                specialAllowanceSettingObjList.Add(specialAllowanceSetting);
                                        }
                                    }
                                    
                                    AllowanceSheetSecondDetails tempAllowanceSheetSecondDetails = new AllowanceSheetSecondDetails
                                    {
                                        AllowanceSheet = tempAllowanceSheet,
                                        TeamMemberDepartment = department,
                                        TeamMemberBranch = branch,
                                        TeamMemberCampus = campus,
                                        TeamMemberDesignation = designation,
                                        AllowanceDate = specialAlloanceSecondDetailsDto.Date,
                                        SpecialAllowanceSetting = specialAllowanceSetting,
                                        SpecialAllowanceAmount = tempAmount,
                                        TotalAllowanceAmount = tempAmount,
                                        Status = AllowanceSheetSecondDetails.EntityStatus.Active,
                                        CreateBy = currentUserId,
                                        ModifyBy = currentUserId,
                                        CreationDate = dateTime,
                                        ModificationDate = dateTime
                                    };
                                    totalAllowanceAmount += tempAmount;
                                    specialAllowanceAmount += tempAmount;
                                    secondDetailsList.Add(tempAllowanceSheetSecondDetails);
                                }
                                #endregion
                            }

                            #endregion SpecialAllowance AllowanceSheet End

                            #region Children Allowance

                            List<TeamMemberAllowanceSheetSecondDetailsDto> childrenAllowanceSecondDetailsDtoList = secondDetailsDtoList.Where(x => x.ChildrenAllowanceSettingId > 0).ToList();
                            List<ChildrenInfo> memberChildrenInfoList = new List<ChildrenInfo>();
                            if (teamMemberChildrenInfoList.Any())
                            {
                                memberChildrenInfoList = teamMemberChildrenInfoList.Where(x => x.TeamMember.Id == teamMember.Id).OrderBy(x => x.Dob).ThenBy(x => x.Id).ToList();
                            }
                            if (memberChildrenInfoList.Any() && childrenAllowanceSecondDetailsDtoList.Any())
                            {
                                int childrenNo = 0;
                                // all childrens of the teamMember
                                foreach (ChildrenInfo childrenInfo in memberChildrenInfoList)
                                {
                                    childrenNo++;
                                    int currentAge = (int)(dateTo - childrenInfo.Dob.Value).TotalDays / 365;

                                    List<TeamMemberAllowanceSheetSecondDetailsDto> tempSecondDetailsDtoList = childrenAllowanceSecondDetailsDtoList.Where(x => x.ChildrenAllowanceMaxNoOfChild >= childrenNo && x.ChildrenAllowanceStartingAge <= currentAge && x.ChildrenAllowanceClosingAge >= currentAge).ToList();
                                    if (tempSecondDetailsDtoList.Any())
                                    {
                                        foreach (TeamMemberAllowanceSheetSecondDetailsDto childrenSheetSecondDetailsDto in tempSecondDetailsDtoList)
                                        {
                                            ChildrenAllowanceSetting childrenAllowanceSetting = null;
                                            decimal tempAmount = childrenSheetSecondDetailsDto.ChildrenAllowanceAmount.Value;
                                            if (saveObj)
                                            {
                                                if (childrenAllowanceSettingObjList.Any() && childrenAllowanceSettingObjList.Where(x => x.Id == childrenSheetSecondDetailsDto.ChildrenAllowanceSettingId).ToList().Any())
                                                    childrenAllowanceSetting = childrenAllowanceSettingObjList.SingleOrDefault(x => x.Id == childrenSheetSecondDetailsDto.ChildrenAllowanceSettingId);
                                                else
                                                {
                                                    childrenAllowanceSetting = _childrenAllowanceSettingDao.LoadById(childrenSheetSecondDetailsDto.ChildrenAllowanceSettingId.Value);
                                                    if (childrenAllowanceSetting != null)
                                                        childrenAllowanceSettingObjList.Add(childrenAllowanceSetting);
                                                }
                                            }

                                            AllowanceSheetSecondDetails tempAllowanceSheetSecondDetails = new AllowanceSheetSecondDetails
                                            {
                                                AllowanceSheet = tempAllowanceSheet,
                                                TeamMemberDepartment = department,
                                                TeamMemberBranch = branch,
                                                TeamMemberCampus = campus,
                                                TeamMemberDesignation = designation,
                                                AllowanceDate = childrenSheetSecondDetailsDto.Date,
                                                ChildrenInfo = childrenInfo,
                                                ChildrenAllowanceSetting = childrenAllowanceSetting,
                                                ChildrenAllowanceAmount = tempAmount,
                                                TotalAllowanceAmount = tempAmount,
                                                Status = AllowanceSheetSecondDetails.EntityStatus.Active,
                                                CreateBy = currentUserId,
                                                ModifyBy = currentUserId,
                                                CreationDate = dateTime,
                                                ModificationDate = dateTime
                                            };
                                            totalAllowanceAmount += tempAmount;
                                            childrenEducationAllowanceAmount += tempAmount;
                                            secondDetailsList.Add(tempAllowanceSheetSecondDetails);
                                        }

                                    }
                                }
                                //end children loop

                            }
                            #endregion
                        }

                        #endregion

                        #region Allowance Sheet Set
                        tempAllowanceSheet.TeamMember = teamMember;
                        tempAllowanceSheet.AllowanceSheetFirstDetails = firstDetailsList;
                        tempAllowanceSheet.AllowanceSheetSecondDetails = secondDetailsList;
                        tempAllowanceSheet.DateFrom = dateFrom;
                        tempAllowanceSheet.DateTo = dateTo;
                        tempAllowanceSheet.Year = dateFrom.Year;
                        tempAllowanceSheet.Month = GetYearOfMonthEnum(dateFrom.Month);

                        tempAllowanceSheet.TeamMemberDepartment = department;
                        tempAllowanceSheet.TeamMemberDesignation = designation;
                        tempAllowanceSheet.TeamMemberBranch = branch;
                        tempAllowanceSheet.TeamMemberCampus = campus;
                        tempAllowanceSheet.SalaryHistory = salaryHistory;
                        tempAllowanceSheet.TeamMemberSalaryDepartment = salaryDepartment;
                        tempAllowanceSheet.TeamMemberSalaryBranch = salaryBranch;
                        tempAllowanceSheet.TeamMemberSalaryCampus = salaryCampus;
                        tempAllowanceSheet.DailySupportAllowanceAmount = dailySupportAllowanceAmount;
                        tempAllowanceSheet.ExtradayAllowanceAmount = extraDayAllowanceAmount;
                        tempAllowanceSheet.OvertimeAllowanceAmount = overtimeAllowanceAmount;
                        tempAllowanceSheet.NightWorkAllowanceAmount = nightWorkAllowanceAmount;
                        tempAllowanceSheet.ChildrenEducationAllowanceAmount = childrenEducationAllowanceAmount;
                        tempAllowanceSheet.SpecialAllowanceAmount = specialAllowanceAmount;
                        tempAllowanceSheet.ZakatAllowanceAmount = zakatAllowanceAmount;
                        tempAllowanceSheet.CsrAllowanceAmount = csrAllowanceAmount;
                        tempAllowanceSheet.TotalAllowanceAmount = totalAllowanceAmount;
                        #endregion
                        allowanceSheetList.Add(tempAllowanceSheet);
                    }
                }

                return allowanceSheetList;

            }
            catch (InvalidDataException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
       
        #endregion

        #region Others Function


        #endregion

        #region Helper function

        private void CheckBeforeReport(List<TeamMember> authoTeamMemberList, DateTime dateFrom, DateTime dateTo)
        {
            DateTime tempdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01);
            if (!authoTeamMemberList.Any())
            {
                throw new InvalidDataException("No Team Member found to generate Allowance Sheet!");
            }
            if (DateTime.Now.Year < dateFrom.Date.Year)
            {
                throw new InvalidDataException("Please Select Year to Current Year or past Year!");
            }
            if (tempdate <= dateFrom.Date)
            {
                throw new InvalidDataException("Can't generate Allowance Sheet to Current Month or Future Month!");
            }
        }

        public MonthsOfYear GetYearOfMonthEnum(int month)
        {
            MonthsOfYear temp = new MonthsOfYear();
            switch (month)
            {
                case 1:
                    temp = MonthsOfYear.January;
                    break;
                case 2:
                    temp = MonthsOfYear.February;
                    break;
                case 3:
                    temp = MonthsOfYear.March;
                    break;
                case 4:
                    temp = MonthsOfYear.April;
                    break;
                case 5:
                    temp = MonthsOfYear.May;
                    break;
                case 6:
                    temp = MonthsOfYear.June;
                    break;
                case 7:
                    temp = MonthsOfYear.July;
                    break;
                case 8:
                    temp = MonthsOfYear.August;
                    break;
                case 9:
                    temp = MonthsOfYear.September;
                    break;
                case 10:
                    temp = MonthsOfYear.October;
                    break;
                case 11:
                    temp = MonthsOfYear.November;
                    break;
                case 12:
                    temp = MonthsOfYear.December;
                    break;
            }
            return temp;
        }

        #endregion

    }
}
