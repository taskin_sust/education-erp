﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceSynchronizerTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceSynchronizer")]
   public class AttendanceSynchronizerUpdateTest : AttendanceSynchronizerBaseTest
   {
       #region Basic test

       [Fact]
       public void Update_Should_Return_Null_Exception()
       {
           _campusRoomFactory.CreateMore(1, null);
           _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList());          
           Assert.Throws<NullObjectException>(() => _attendanceSynchronizerService.Update(null));
           Assert.Throws<NullObjectException>(() => _attendanceSynchronizerService.Update(_attendanceSynchronizerFactory.Object));
       }

       [Fact]
       public void Save_Should_Return_Model_Invalid_SyncKey()
       {
           _campusRoomFactory.CreateMore(1, null);
           _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList());
           _attendanceSynchronizerFactory.Persist();

           _attendanceSynchronizerFactory.Object.SynchronizerKey = "";
           Assert.Throws<MessageException>(() => _attendanceSynchronizerService.Update(_attendanceSynchronizerFactory.Object));
       }

       [Fact]
       public void Update_Should_Return_Model_InValid()
       {
           _campusRoomFactory.CreateMore(1, null);
           _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList()).Persist();
           var obj = _attendanceSynchronizerService.GetAttendanceSynchronizer(_attendanceSynchronizerFactory.Object.Id);
           obj.Campus = null;
           Assert.Throws<MessageException>(() => _attendanceSynchronizerService.Update(obj));
       }

       [Fact]
       public void Update_Should_Check_Duplicate()
       {
           _campusRoomFactory.CreateMore(1, null);
           var obj1 = _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList()).WithSynchronizerKey(_synchronizerKey + Guid.NewGuid().ToString().Substring(0, 5)).Persist().Object;
           var obj2 = _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.GetLastCreatedObjectList()).WithSynchronizerKey(_synchronizerKey + Guid.NewGuid().ToString().Substring(0, 5)).Persist().Object;
          var objUpdate = _attendanceSynchronizerService.GetAttendanceSynchronizer(obj2.Id);
          objUpdate.SynchronizerKey = obj1.SynchronizerKey;
          Assert.Throws<DuplicateEntryException>(() => _attendanceSynchronizerService.Update(objUpdate));
       }

        [Fact]
       public void Should_Update_Successfully()
       {
           var s =  Guid.NewGuid().ToString().Substring(0, 5);
           var obj1 = _attendanceSynchronizerFactory.Create().WithCampus(_campusRoomFactory.CreateMore(1, null).GetLastCreatedObjectList()).WithSynchronizerKey(_synchronizerKey).Persist().Object;
           var objUpdate = _attendanceSynchronizerService.GetAttendanceSynchronizer(obj1.Id);
           objUpdate.Name = "Update Synchronizer Test" + s;
           _attendanceSynchronizerService.Update(objUpdate);
           var objUpdate2 = _attendanceSynchronizerService.GetAttendanceSynchronizer(obj1.Id);
           Assert.Equal(objUpdate2.Name, "Update Synchronizer Test" + s);
       }

       #endregion
   }
}
