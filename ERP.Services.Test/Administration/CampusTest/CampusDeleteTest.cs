﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.CampusTest
{
    public interface ICampusDeleteTest
    {
        #region Basic Test
        [Fact]
        void Should_Delete_Successfully();

        [Fact]
        void Campus_Delete_Should_Not_Delete_Branch(); 
        #endregion

        #region Business Logic Test
        [Fact]
        void Delete_Should_Throw_Dependency_Exception(); 
        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Campus")]
    public class CampusDeleteTest : CampusBaseTest, ICampusDeleteTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            var campusFactory =
               _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                })
                   .Persist();
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            var success = _campusService.Delete(campus.Id, campus);
            Assert.True(success);
            campus = _campusService.GetCampus(campusFactory.Object.Id);
            Assert.Null(campus);
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
        }


        [Fact]
        public void Campus_Delete_Should_Not_Delete_Branch()
        {
            var campusFactory =
                _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                })
                    .Persist();
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            var success = _campusService.Delete(campus.Id, campus);
            Assert.True(success);
            campus = _campusService.GetCampus(campusFactory.Object.Id);
            Assert.Null(campus);
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
            Branch branch = _branchService.GetBranch(_campusFactory.Object.Branch.Id);
            Assert.NotNull(branch);
        }

        
        #endregion

        #region Business Logic Test
        [Fact]
        public void Delete_Should_Throw_Dependency_Exception()
        {
            var campusFactory =
               _campusFactory.Create().WithBranch().WithNameAndRank(_name, 1).WithCampusRoomList(new List<CampusRoom>()
                {
                    new CampusRoom()
                    {
                        RoomNo = "101",
                        ClassCapacity = 100,
                        ExamCapacity = 100
                    }
                });
            _campusFactory.Object.Batches = new List<Batch>() { new Batch() { Status = Batch.EntityStatus.Active } };
            _campusFactory.Persist();
            Campus campus = _campusService.GetCampus(campusFactory.Object.Id);
            var campusRoomList = _campusRoomService.LoadCampusRoom(new[]
            {
                campus.Id.ToString(CultureInfo.InvariantCulture)
            });
            Assert.Throws<DependencyException>(() => _campusService.Delete(campus.Id, campus));
            _campusTestService.DeleteCampusRoom(campusRoomList.Select(x => x.Id).ToList());
            _campusFactory.CleanUpBatch(_campusFactory.Object.Batches.Select(x => x.Id).ToList());
        } 
        #endregion

    }
}
