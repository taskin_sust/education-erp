using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class AllowanceSheetMap : BaseClassMap<AllowanceSheet,long> {
        
        public AllowanceSheetMap() {
			Table("HR_AllowanceSheet");
			LazyLoad();
            References(x => x.TeamMember).Column("TeamMemberId");
            References(x => x.TeamMemberDepartment).Column("TeamMemberDepartmentId");
            References(x => x.TeamMemberDesignation).Column("TeamMemberDesignationId");
            References(x => x.TeamMemberBranch).Column("TeamMemberBranchId");
            References(x => x.TeamMemberCampus).Column("TeamMemberCampusId");
            References(x => x.SalaryHistory).Column("SalaryHistoryId");
            References(x => x.TeamMemberSalaryDepartment).Column("TeamMemberSalaryDepartmentId");
            References(x => x.TeamMemberSalaryBranch).Column("TeamMemberSalaryBranchId");
            References(x => x.TeamMemberSalaryCampus).Column("TeamMemberSalaryCampusId");
            Map(x => x.DailySupportAllowanceAmount).Column("DailySupportAllowanceAmount");
            Map(x => x.ExtradayAllowanceAmount).Column("ExtradayAllowanceAmount");
            Map(x => x.OvertimeAllowanceAmount).Column("OvertimeAllowanceAmount");
            Map(x => x.NightWorkAllowanceAmount).Column("NightWorkAllowanceAmount");
            Map(x => x.ChildrenEducationAllowanceAmount).Column("ChildrenEducationAllowanceAmount");
            Map(x => x.SpecialAllowanceAmount).Column("SpecialAllowanceAmount");
            Map(x => x.ZakatAllowanceAmount).Column("ZakatAllowanceAmount");
            Map(x => x.CsrAllowanceAmount).Column("CsrAllowanceAmount");
            Map(x => x.TotalAllowanceAmount).Column("TotalAllowanceAmount");
            Map(x => x.Year).Column("Year");
            Map(x => x.Month).Column("Month");
            Map(x => x.DateFrom).Column("DateFrom");
            Map(x => x.DateTo).Column("DateTo");
            Map(x => x.FinalSubmissionDate).Column("FinalSubmissionDate");
            Map(x => x.IsFinalSubmit).Column("IsFinalSubmit");
            Map(x => x.PreparedBy).Column("PreparedBy");

            HasMany(x => x.AllowanceSheetFirstDetails).KeyColumn("AllowanceSheetId").Cascade.AllDeleteOrphan().Inverse();
            HasMany(x => x.AllowanceSheetSecondDetails).KeyColumn("AllowanceSheetId").Cascade.AllDeleteOrphan().Inverse();
            //HasMany(x => x.StudentAcademicInfoBaseData).KeyColumn("StudentAcademicInfoId").Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
