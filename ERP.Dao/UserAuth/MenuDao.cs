﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Mapping;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.BusinessRules;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.UserAuth
{
    public interface IMenuDao : IBaseDao<Menu, long>
    {
        #region Operational Function
        void DeleteMenuPermanently(List<long> menuIdList);
        #endregion

        #region Single Instances Loading Function
        Menu LoadByRank(int rank, long parentId);
        Menu LoadByIdEgarly(long id);
        #endregion

        #region List Loading Function
        IList<Menu> LoadActive(bool eagerLoadMenuAccess = false);
        IList<Menu> LoadByMenuGroupId(int menuGroupId);
        IList<Menu> LoadByMenuId(int menuId);
        IList<Menu> LoadAllRoot();
        List<Menu> LoadUserMenuByUser(UserProfile userProfile);
        IList<Menu> LoadUserMenuByUserAndGroup(UserProfile profile, long groupId);
        List<Menu> GetMenuList(int start, int length, string orderBy, string direction, string menuGroupId, string parentId, string name, string controllerName, string actionName, string status, string rank);
        IList<Menu> LoadMenuByControllerAction(AreaControllers controller, Actions action);
        IList<Menu> LoadMenuByControllerAction(string controller, string action);
        List<Menu> LoadMenuForUserPermission(long userProfileId, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null);
        List<Menu> LoadMenuForUserObservePermission(long userProfileId);
        #endregion

        #region Others Function
        int GetMenuMaximumRank(long parentId);
        bool DuplicationCheck(string name, long? menuGroupId, long? parentId, long? id);
        int MenuRowCount(string menuGroupId, string parentId, string name, string controllerName, string actionName, string status, string rank);
        #endregion

    }
    public class MenuDao : BaseDao<Menu, long>, IMenuDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        public void DeleteMenuPermanently(List<long> menuIdList)
        {
            Session.CreateSQLQuery("Delete from MenuGroup where id in(" + string.Join(", ", menuIdList) + ")").ExecuteUpdate();
        }
        #endregion

        #region Single Instances Loading Function
        public Menu LoadByRank(int rank, long parentId)
        {
            if (parentId == 0)
            {
                return Session.QueryOver<Menu>().Where(x => x.Rank == rank && x.MenuSelfMenu == null).SingleOrDefault<Menu>();
            }
            else
            {
                return Session.QueryOver<Menu>().Where(x => x.Rank == rank && x.MenuSelfMenu.Id == parentId).SingleOrDefault<Menu>();
            }
        }

        public Menu LoadByIdEgarly(long id)
        {
            ICriteria criteria = Session.CreateCriteria<Menu>();
            criteria.Add(Restrictions.Eq(Projections.Property<Menu>(x => x.Id), id));
            criteria.CreateAlias("MenuAccess", "ma", JoinType.InnerJoin);
            criteria.SetFetchMode("ma", FetchMode.Eager);
            var list = criteria.List<Menu>();
            return list.FirstOrDefault();
        }

        #endregion

        #region List Loading Function
        public IList<Menu> LoadActive(bool eagerLoadMenuAccess = false)
        {
            Session.Clear();                                //first level cache
            Session.SessionFactory.Evict(typeof(Menu));     //second level cache

            var criteria = Session.CreateCriteria<Menu>();
            criteria.Add(Restrictions.Eq("Status", Menu.EntityStatus.Active));
            if (eagerLoadMenuAccess)
            {
                criteria.SetFetchMode("MenuGroup", FetchMode.Eager);
                criteria.SetFetchMode("MenuAccess", FetchMode.Eager);
            }

            criteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            var dataList = criteria.List<Menu>().ToList();

            return dataList;
        }

        public IList<Menu> LoadByMenuGroupId(int menuGroupId)
        {
            var dataList = Session.QueryOver<Menu>()
                .Where(x => x.MenuGroup.Id == menuGroupId
                            && x.MenuSelfMenu == null
                            && x.Status != Menu.EntityStatus.Delete)
                .Fetch(x => x.MenuSelfMenu).Eager
                .Fetch(x => x.Menus).Eager
                .TransformUsing(new DistinctRootEntityResultTransformer())
                .List<Menu>();

            return dataList;
        }
        public IList<Menu> LoadByMenuId(int menuId)
        {
            return Session.QueryOver<Menu>().Where(x => x.MenuSelfMenu.Id == menuId && x.MenuSelfMenu == null && x.Status != Menu.EntityStatus.Delete).List<Menu>();
        }
        public List<Menu> LoadUserMenuByUser(UserProfile userProfile)
        {
            Session.Clear();                                //first level cache
            Session.SessionFactory.Evict(typeof(Menu));     //second level cache

            var criteria = Session.CreateCriteria<Menu>();
            criteria.CreateAlias("UserMenus", "um");
            criteria.Add(Restrictions.Eq("um.User", userProfile));
            criteria.SetFetchMode("UserMenus", FetchMode.Eager);

            //criteria.SetFetchMode("DefaultAction", FetchMode.Eager);
            //criteria.SetFetchMode("DefaultAction.AreaControllers", FetchMode.Eager);

            criteria.SetFetchMode("MenuAccess", FetchMode.Eager);
            //criteria.SetFetchMode("MenuAccess.Actions", FetchMode.Eager);
            //criteria.SetFetchMode("MenuAccess.Actions.AreaControllers", FetchMode.Eager);

            criteria.SetFetchMode("MenuGroup", FetchMode.Eager);

            criteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            List<Menu> menu = criteria.List<Menu>().ToList();
            return menu;

        }
        public IList<Menu> LoadUserMenuByUserAndGroup(UserProfile profile, long groupId)
        {
            if (profile.Name != ApplicationUsers.SuperAdmin)
            {
                var criteria = Session.CreateCriteria<UserMenu>();

                criteria.CreateAlias("Menu", "m");
                criteria.CreateAlias("m.MenuGroup", "mg");
                criteria.Add(Restrictions.Eq("m.Status", Menu.EntityStatus.Active));
                criteria.Add(Restrictions.Eq("User.Id", profile.Id));
                criteria.Add(Restrictions.Eq("mg.Id", groupId));
                criteria.SetProjection(Projections.Property("Menu"));
                return criteria.List<Menu>().ToList();
            }
            else
            {
                var criteria = Session.CreateCriteria<Menu>();
                criteria.CreateAlias("MenuGroup", "mg");
                criteria.Add(Restrictions.Eq("mg.Id", groupId));
                criteria.Add(Restrictions.Eq("Status", Menu.EntityStatus.Active));
                return criteria.List<Menu>().ToList();
            }
        }
        public IList<Menu> LoadAllRoot()
        {
            var query = Session.CreateCriteria<Menu>();
            query.Add(Restrictions.IsNull("MenuSelfMenu"));
            return query.List<Menu>();
        }
        public IList<Menu> LoadMenuByControllerAction(AreaControllers controller, Actions action)
        {
            var criteria = Session.CreateCriteria<MenuAccess>();
            criteria.Add(Restrictions.Eq("AreaControllers", controller)).Add(Restrictions.Eq("Actions", action));
            criteria.Add(Restrictions.Eq("Status", MenuAccess.EntityStatus.Active));
            criteria.SetProjection(Projections.Distinct(Projections.Property("Menu")));
            var list = criteria.List<Menu>();
            return list;
        }
        public IList<Menu> LoadMenuByControllerAction(string controller, string action)
        {
            var criteria = Session.CreateCriteria<MenuAccess>();
            criteria.CreateAlias("Actions", "a");
            criteria.Add(Restrictions.Eq("a.Status", Menu.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("a.Name", action));
            criteria.CreateAlias("a.AreaControllers", "aac");
            criteria.Add(Restrictions.Eq("aac.Status", Menu.EntityStatus.Active));
            criteria.Add(Restrictions.Eq("aac.Name", controller));
            criteria.Add(Restrictions.Eq("Status", MenuAccess.EntityStatus.Active));
            criteria.SetProjection(Projections.Distinct(Projections.Property("Menu")));
            var list = criteria.List<Menu>();
            return list;
        }

        public List<Menu> LoadMenuForUserPermission(long userProfileId, List<long> organizationIdList = null, List<long> programIdList = null, List<long> branchIdList = null)
        {
            int count = 1;
            string organizationFilterQuery = "";
            string programFilterQuery = "";
            string branchFilterQuery = "";
            if (organizationIdList != null)
            {
                if (organizationIdList.Contains(SelectionType.SelelectAll))
                {
                    //count = 1;
                }
                else
                {
                    organizationFilterQuery = " or OrganizationId IN (" + string.Join(",", organizationIdList) + ") ";
                   // count = organizationIdList.Count;
                }
            }
            if (programIdList != null)
            {
                if (programIdList.Contains(SelectionType.SelelectAll))
                {
                    count *= 1;
                }
                else
                {
                    programFilterQuery = " or ProgramId IN (" + string.Join(",", programIdList) + ") ";
                    count *= programIdList.Count;
                }
            }
            if (branchIdList != null)
            {
                if (branchIdList.Contains(SelectionType.SelelectAll))
                {
                    count *= 1;
                }
                else
                {
                    branchFilterQuery = " or BranchId IN (" + string.Join(",", branchIdList) + ") ";
                    count *= branchIdList.Count;
                }
            }
            string query2 = @"select MenuId from UserMenu 
                                        where UserProfileId = " + userProfileId + @" 
                                         and (OrganizationId is null )
                                         and (BranchId is null )
                                         and (ProgramId is null )
                                        UNION ALL
                                        SELECT MenuId
                                        FROM (
                                         select MenuId, count(*) c from UserMenu 
                                         where UserProfileId = " + userProfileId + @" 
                                          and (OrganizationId is null " + organizationFilterQuery + @")
                                          and (BranchId is null " + branchFilterQuery + @")
                                          and (ProgramId is null  " + programFilterQuery + @" )
                                         GROUP BY MenuId
                                        ) As a
                                        WHERE a.c >= " + count + @"
                                        UNION ALL
                                        SELECT ParentId as MenuId
                                        FROM Menu
                                        WHERE ID IN (
                                         select MenuId from UserMenu 
                                         where UserProfileId = " + userProfileId + @" 
                                          and (OrganizationId is null )
                                          and (BranchId is null )
                                          and (ProgramId is null )
                                         UNION ALL
                                         SELECT MenuId
                                         FROM (
                                          select MenuId, count(*) c from UserMenu 
                                          where UserProfileId = " + userProfileId + @" 
                                          and (OrganizationId is null " + organizationFilterQuery + @")
                                          and (BranchId is null " + branchFilterQuery + @")
                                          and (ProgramId is null  " + programFilterQuery + @" )
                                          GROUP BY MenuId
                                         ) As a
                                         WHERE a.c >= " + count + @"
                                        )";

            string query = @" Select DISTINCT {m.*} from Menu as m 
                                        inner join ( " + query2 + @" ) as a on a.MenuId = m.Id
                                        where m.Status = " + Menu.EntityStatus.Active + @"
                                        ";
            ISQLQuery iQuery = Session.CreateSQLQuery(query).AddEntity("m", typeof(Menu));
            var aa = iQuery.List<Menu>().ToList();
            return aa;
        }

        public List<Menu> LoadMenuForUserObservePermission(long userProfileId)
        {
            string query2 = @"select DISTINCT MenuId from UserMenu 
                            where UserProfileId = " + userProfileId + @" 
                            UNION ALL
                            SELECT ParentId as MenuId
                            FROM Menu
                            WHERE ID IN (
                             select MenuId from UserMenu 
                             where UserProfileId  = " + userProfileId + @" 
                            )";
            string query = @" Select DISTINCT {m.*} from Menu as m 
                                        inner join ( " + query2 + @" ) as a on a.MenuId = m.Id
                                        where m.Status = " + Menu.EntityStatus.Active + @"
                                        ";
            ISQLQuery iQuery = Session.CreateSQLQuery(query).AddEntity("m", typeof(Menu));
            var aa = iQuery.List<Menu>().ToList();
            return aa;
        }

        public List<Menu> GetMenuList(int start, int length, string orderBy, string direction, string menuGroupId, string parentId, string name, string controllerName, string actionName, string status, string rank)
        {
            ICriteria criteria =
                    Session.CreateCriteria<Menu>()
                        .Add(Restrictions.Not(Restrictions.Eq("Status", Menu.EntityStatus.Delete)));

            if (!String.IsNullOrEmpty(menuGroupId))
            {
                criteria.Add(Restrictions.Eq("MenuGroup.Id", Convert.ToInt64(menuGroupId)));
            }

            if (!String.IsNullOrEmpty(parentId))
            {
                if (parentId == "-1")
                {
                    criteria.Add(Restrictions.IsNull("MenuSelfMenu"));
                }
                else
                {
                    criteria.Add(Restrictions.Eq("MenuSelfMenu.Id", Convert.ToInt64(parentId)));
                }
            }

            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }

            if (!String.IsNullOrEmpty(controllerName) && !String.IsNullOrEmpty(actionName))
            {
                criteria.CreateAlias("MenuAccess", "ma")
                    .CreateAlias("ma.Actions", "ac")
                    .CreateAlias("ac.AreaControllers", "arc")
                    .Add(Restrictions.Conjunction()
                        .Add(Restrictions.Like("arc.Name", controllerName, MatchMode.Anywhere))
                        .Add(Restrictions.Like("ac.Name", actionName, MatchMode.Anywhere))
                        );

            }
            else if (!String.IsNullOrEmpty(controllerName) && String.IsNullOrEmpty(actionName))
            {
                criteria.CreateAlias("MenuAccess", "ma")
                    .CreateAlias("ma.Actions", "ac")
                    .CreateAlias("ac.AreaControllers", "arc")
                    .Add(Restrictions.Like("arc.Name", controllerName, MatchMode.Anywhere));

            }
            else if (String.IsNullOrEmpty(controllerName) && !String.IsNullOrEmpty(actionName))
            {
                criteria.CreateAlias("MenuAccess", "ma")
                    .CreateAlias("ma.Actions", "ac")
                    .Add(Restrictions.Like("ac.Name", actionName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));

            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }

            if (!string.IsNullOrEmpty(orderBy.Trim()))
            {
                if (direction == "ASC")
                {
                    criteria.AddOrder(Order.Asc(orderBy));
                }
                else
                {
                    criteria.AddOrder(Order.Desc(orderBy));
                } 
            }
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            var list = (List<Menu>)criteria.SetFirstResult(start).SetMaxResults(length).List<Menu>();
            var nlist = new List<Menu>();
            if (string.IsNullOrEmpty(actionName))
            {
                nlist.AddRange(list);
            }
            else if (!string.IsNullOrEmpty(actionName))
            {
                nlist.AddRange(list.Where(menu => !string.IsNullOrEmpty(menu.DefaultAction.Name.ToString(CultureInfo.CurrentCulture))).Where(menu => menu.DefaultAction.Name.Contains(actionName)));
            }
            return nlist;
        }
        public override IList<Menu> LoadAll(int? status = null)
        {
            Session.Clear();    //first level cache
            Session.SessionFactory.Evict(typeof(Menu));     //second level cache
            return base.LoadAll();
        }
        #endregion

        #region Others Function
        public int GetMenuMaximumRank(long parentId)
        {
            try
            {
                var max = 0;
                if (parentId == 0)
                {
                    max = Session.QueryOver<Menu>().Where(x => x.Status != Menu.EntityStatus.Delete && x.MenuSelfMenu == null).Select(Projections.Max<Menu>(x => x.Rank)).SingleOrDefault<int>();
                }
                else
                {
                    max = Session.QueryOver<Menu>().Where(x => x.Status != Menu.EntityStatus.Delete && x.MenuSelfMenu.Id == parentId).Select(Projections.Max<Menu>(x => x.Rank)).SingleOrDefault<int>();
                }
                return max;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public bool DuplicationCheck(string name, long? menuGroupId, long? parentId, long? id)
        {
            var menu = Session.QueryOver<Menu>().Where(x => x.Status != Menu.EntityStatus.Delete && x.Name == name);
            if (menuGroupId == null)
            {
                menu = menu.Where(x => x.MenuGroup == null);
            }
            else
            {
                menu = menu.Where(x => x.MenuGroup.Id == menuGroupId);
            }
            if (parentId == null)
            {
                menu = menu.Where(x => x.MenuSelfMenu == null);
            }
            else
            {
                menu = menu.Where(x => x.MenuSelfMenu.Id == parentId);
            }

            if (id != null)
            {
                menu = menu.Where(x => x.Id != id);
            }

            IList<Menu> catList = menu.List<Menu>();

            if (catList == null || catList.Count < 1)
                return true;
            else
                return false;
        }
        public int MenuRowCount(string menuGroupId, string parentId, string name, string controllerName, string actionName, string status, string rank)
        {
            ICriteria criteria =
                    Session.CreateCriteria<Menu>()
                        .Add(Restrictions.Not(Restrictions.Eq("Status", Menu.EntityStatus.Delete)));

            if (!String.IsNullOrEmpty(menuGroupId))
            {
                criteria.Add(Restrictions.Eq("MenuGroup.Id", Convert.ToInt64(menuGroupId)));
            }

            if (!String.IsNullOrEmpty(parentId))
            {
                if (parentId == "-1")
                {
                    criteria.Add(Restrictions.IsNull("MenuSelfMenu"));
                }
                else
                {
                    criteria.Add(Restrictions.Eq("MenuSelfMenu.Id", Convert.ToInt64(parentId)));
                }
            }

            if (!String.IsNullOrEmpty(name))
            {
                criteria.Add(Restrictions.Like("Name", name, MatchMode.Anywhere));
            }

            if (!String.IsNullOrEmpty(controllerName) && !String.IsNullOrEmpty(actionName))
            {
                criteria.CreateAlias("MenuAccess", "ma")
                    .CreateAlias("ma.Actions", "ac")
                    .CreateAlias("ac.AreaControllers", "arc")
                    .Add(Restrictions.Conjunction()
                        .Add(Restrictions.Like("arc.Name", controllerName, MatchMode.Anywhere))
                        .Add(Restrictions.Like("ac.Name", actionName, MatchMode.Anywhere))
                        );

            }
            else if (!String.IsNullOrEmpty(controllerName) && String.IsNullOrEmpty(actionName))
            {
                criteria.CreateAlias("MenuAccess", "ma")
                    .CreateAlias("ma.Actions", "ac")
                    .CreateAlias("ac.AreaControllers", "arc")
                    .Add(Restrictions.Like("arc.Name", controllerName, MatchMode.Anywhere));

            }
            else if (String.IsNullOrEmpty(controllerName) && !String.IsNullOrEmpty(actionName))
            {
                criteria.CreateAlias("MenuAccess", "ma")
                    .CreateAlias("ma.Actions", "ac")
                    .Add(Restrictions.Like("ac.Name", actionName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(status))
            {
                criteria.Add(Restrictions.Eq("Status", Convert.ToInt32(status)));

            }
            if (!String.IsNullOrEmpty(rank))
            {
                criteria.Add(Restrictions.Eq("Rank", Convert.ToInt32(rank)));
            }
            
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);
            var list = (List<Menu>)criteria.List<Menu>();
            var nlist = new List<Menu>();
            if (string.IsNullOrEmpty(actionName))
            {
                nlist.AddRange(list);
            }
            else if (!string.IsNullOrEmpty(actionName))
            {
                nlist.AddRange(list.Where(menu => !string.IsNullOrEmpty(menu.DefaultAction.Name.ToString(CultureInfo.CurrentCulture))).Where(menu => menu.DefaultAction.Name.Contains(actionName)));
            }
            return nlist.Count;
        }
        #endregion
        
    }
}