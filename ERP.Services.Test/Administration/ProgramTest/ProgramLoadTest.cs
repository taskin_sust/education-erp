﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Test.Hr.DepartmentTest;
using UdvashERP.Services.Test.Hr.ShiftTest;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.ProgramTest
{
    interface IProgramLoadTest
    {
        #region Basic Test
        void Load_Program_Should_Be_Return_Zero();
        void Load_Program_Should_Be_Return_Expected_Program();

        #endregion
    }

    [Trait("Area", "Administration")]
    [Trait("Service", "Program")]
    public class ProgramLoadTest : ProgramBaseTest, IProgramLoadTest
    {
        #region Basic Test
        [Fact]
        public void Load_Program_Should_Be_Return_Zero()
        {
            List<long> organizationId = new List<long>();
            List<long> sessionId = new List<long>();
            var returnValue = _programService.LoadProgram(organizationId, sessionId);
            Assert.True(returnValue.Count == 0);
        }

        [Fact]
        public void Load_Program_Should_Be_Return_Expected_Program()
        {
            var organization = organizationFactory.Create().Persist();
            List<long> organizationId = new List<long>() { organization.Object.Id };
            var program = _programFactory.CreateMore(2, organization.SingleObjectList).Persist();
            var returnValue = _programService.LoadProgram(organizationId);
            Assert.True(returnValue.Count == 2);
        }

        #endregion

        //[Fact]
        //public void LoadActive_should_return_list()
        //{
        //    _programFactory.CreateMore(50).Persist();
        //    IList<Program> programList = _programService.LoadProgram(0, 50, "", "", _programFactory.GetLastCreatedObjectList()[0].Organization.Id.ToString(), "", "", "", "", "", "");
        //    Assert.Equal(50, programList.Count);
        //}

        //[Fact]
        //public void LoadActive_should_Not_return_list()
        //{
        //    IList<Program> programList = _programService.LoadProgram(0, 100, "", "", "-1", "", "", "", "1", "1", "");
        //    Assert.Equal(0, programList.Count);  



    }
}
