using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.BusinessModel.ViewModel.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface IGoodsTransferDao : IBaseDao<GoodsTransfer, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        List<GoodsTransferDto> GetGoodsTransfer(long id);

        #endregion

        #region List Loading Function

        IList<GoodsTransferDto> LoadGoodsTransfer(int start, int length, string orderBy, string orderDir, List<long> authBranchFrom, List<long> authBranchTo, IList<long> authorizeProgramIds, string itemName, string requiredDate);

        #endregion

        #region Others Function

        int GetGoodsTransferRowCount(List<long> authBranchFrom, List<long> authBranchTo, IList<long> authorizeProgramIds, string itemName, string requiredDate);
        int GetGoodsTransferByBranchRowCount(List<long> authOrganizationFrom, List<long> authBranchFrom, List<long> authOrganizationTo, List<long> authBranchTo, List<long> authProgramIds, IList<string> branchNameList, long[] itemId, DateTime dateFrom, DateTime dateTo);
        IList<dynamic> LoadGoodsTransferByBranch(int start, int length, List<long> authOrganizationFrom, List<long> authBranchFrom, List<long> authOrganizationTo, List<long> authBranchTo, List<long> authProgramIds, IList<string> branchNameList, long[] itemId, DateTime dateFrom, DateTime dateTo);
        IList<dynamic> LoadGoodsTransferByProgram(int start, int length, List<long> authOrganizationFrom, List<long> authBranchFrom, List<long> authOrganizationTo, List<long> authBranchTo,List<long> authProgramIds , IList<string> programNameList, long[] itemId, DateTime dateFrom, DateTime dateTo);
        int GetGoodsTransferByProgramRowCount(List<long> authOrganizationFrom, List<long> authBranchFrom, List<long> authOrganizationTo, List<long> authBranchTo, List<long> authProgramIds, IList<string> programNameList, long[] itemId, DateTime dateFrom, DateTime dateTo);


        #endregion
    }

    public class GoodsTransferDao : BaseDao<GoodsTransfer, long>, IGoodsTransferDao
    {
        #region Properties & Object & Initialization

        public static IResultTransformer ExpandoObject;

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public List<GoodsTransferDto> GetGoodsTransfer(long id)
        {
            string query = GetGoodsTransferQuery();
            query += string.Format(@" AND gt.Id ={0}", id);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<GoodsTransferDto>());
            iQuery.SetTimeout(2700);
            //var returnResult = iQuery.UniqueResult<GoodsTransferDto>();
            var returnResult = iQuery.List<GoodsTransferDto>().ToList();
            return returnResult;
        }

        #endregion

        #region List Loading Function

        public IList<GoodsTransferDto> LoadGoodsTransfer(int start, int length, string orderBy, string orderDir, List<long> authBranchFrom, List<long> authBranchTo, IList<long> authorizeProgramIds, string itemName, string requiredDate)
        {
            string query = GetGoodsTransferQuery(authBranchFrom, authBranchTo, authorizeProgramIds, itemName, requiredDate);

            if (length > 0)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    if (orderDir == "asc")
                    {
                        query += "ORDER BY  " + orderBy.Trim() + " ASC OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
                    }
                    else
                    {
                        query += "ORDER BY  " + orderBy.Trim() + " DESC OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";

                    }
                }
                else
                {
                    query += "ORDER BY  gt.CreationDate DESC OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";

                }
            }
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<GoodsTransferDto>());
            iQuery.SetTimeout(2700);
            var list = iQuery.List<GoodsTransferDto>().ToList();
            return list;
        }

        private string GetGoodsTransferQuery(List<long> authBranchFrom = null, List<long> authBranchTo = null, IList<long> authorizeProgramIds = null, string itemName = "", string requiredDate = "")
        {
            string condition = "";

            if (authorizeProgramIds != null && authorizeProgramIds.Count > 0)
            {
                string programIds = string.Join(",", authorizeProgramIds);
                condition += string.Format(@" AND ( p.Id IN ({0}) OR p.Id IS NULL )", programIds);
                //condition += string.Format(@" AND p.Id IN ({0})", programIds);
            }

            if (authBranchFrom != null && authBranchFrom.Count > 0)
            {
                string branchId = string.Join(",", authBranchFrom);
                condition += string.Format(@" AND gt.BranchFromId IN ({0})", branchId);
            }

            if (authBranchTo != null && authBranchTo.Count > 0)
            {
                string branchId = string.Join(",", authBranchTo);
                condition += string.Format(@" AND gt.BranchToId IN ({0})", branchId);
            }

            if (!String.IsNullOrEmpty(itemName))
            {
                condition += string.Format(@" AND it.Name LIKE '%{0}%' ", itemName);
            }

            if (!String.IsNullOrEmpty(requiredDate))
            {
                //condition += string.Format(@" AND CONVERT(varchar,gt.CreationDate,112) =convert(varchar, '{0}', 112) ", requiredDate);
                condition += string.Format(@" AND CAST(gt.CreationDate AS date) = '{0}'", requiredDate);
            }

            string query = string.Format(@"
                                        SELECT  
                                                gtd.Id AS TransferDetailsId,
                                                gt.Id AS TransferId,
                                                gt.BranchFromId  AS BranchFromId,
                                                bfrm.Name AS BranchFromName,
                                                bfrm.IsCorporate AS IsCorporateBranchFrom,
                                                bfrm.OrganizationId  AS OrganizationFromId,
                                                OrgF.Name  AS OrgFromName,
                                                OrgF.ShortName  AS OrgFromShortName,
                                                gt.BranchToId  AS BranchToId,
                                                bto.Name  AS BranchToName,
                                                bto.IsCorporate  AS IsCorporateBranchTo,
                                                bto.OrganizationId  AS OrgToId,
                                                OrgT.Name  AS OrgToName,
	                                            OrgT.ShortName  AS OrgToShortName,
                                                itg.Name  AS ItemGroupName,
                                                gt.Remarks  AS TransferRemark,
                                                gt.TransferNo  AS TransferNo,
                                                gt.CreationDate  AS TransferDate,
                                                gtd.ItemId  AS ItemId,
                                                it.Name  AS ItemName,
                                                gtd.ProgramId  AS ProgramId,
                                                p.Name  AS ProgramName ,
                                                gtd.SessionId  AS SessionId,
                                                s.Name  AS SessionName,
                                                gtd.TransferQuantity  AS TransferQuantity
                                        FROM    UINV_GoodsTransfer gt
                                                INNER JOIN Branch bfrm ON bfrm.Id = gt.BranchFromId AND bfrm.Status = {0}
                                                INNER JOIN Branch bto ON bto.Id = gt.BranchToId AND bto.Status = {1}
                                                INNER JOIN Organization OrgF ON OrgF.Id = bfrm.OrganizationId AND OrgF.Status = {2}
                                                INNER JOIN Organization OrgT ON OrgT.Id = bto.OrganizationId AND OrgT.Status ={3}
                                                INNER  JOIN UINV_GoodsTransferDetails gtd ON gtd.GoodsTransferId = gt.Id AND gtd.Status ={4}
                                                LEFT JOIN Program p ON p.Id = gtd.ProgramId  AND p.Status = {5} 
                                                LEFT JOIN Session s ON s.Id = gtd.SessionId AND s.Status = {6}
                                                INNER JOIN UINV_Item it ON it.Id = gtd.ItemId AND it.Status ={7}
                                                INNER JOIN UINV_ItemGroup itg ON itg.Id = it.ItemGroupId AND itg.Status ={8}
                                                WHERE 1=1
                                        ", Branch.EntityStatus.Active
                                          , Branch.EntityStatus.Active
                                          , Organization.EntityStatus.Active
                                          , Organization.EntityStatus.Active
                                          , GoodsTransferDetails.EntityStatus.Active
                                          , Program.EntityStatus.Active
                                          , BusinessModel.Entity.Administration.Session.EntityStatus.Active
                                          , Item.EntityStatus.Active
                                          , ItemGroup.EntityStatus.Active

                                         );

            if (!String.IsNullOrEmpty(condition))
            {
                query += condition;
            }
            return query;
        }

        //private ICriteria GetGoodsTransferListCriteria(List<long> authBranch, string itemName, string requiredDate)
        //{
        //    ICriteria criteria = Session.CreateCriteria<GoodsTransfer>().Add(Restrictions.Not(Restrictions.Eq("Status", GoodsTransfer.EntityStatus.Delete)));

        //    if (authBranch.Count > 0)
        //    {
        //        criteria.CreateAlias("BranchTo", "branch").Add(Restrictions.Not(Restrictions.Eq("branch.Status", Branch.EntityStatus.Delete)));
        //        criteria.Add(Restrictions.In("branch.Id", authBranch));
        //    }

        //    if (!String.IsNullOrEmpty(itemName))
        //    {

        //        criteria.CreateAlias("GoodsTransferDetails", "GoodsDetails").Add(Restrictions.Not(Restrictions.Eq("GoodsDetails.Status", GoodsTransferDetails.EntityStatus.Delete)));
        //        criteria.CreateAlias("GoodsDetails.Item", "item").Add(Restrictions.Not(Restrictions.Eq("item.Status", Item.EntityStatus.Delete)));
        //        criteria.Add(Restrictions.Like("item.Name", itemName, MatchMode.Anywhere));
        //    }
        //    if (!String.IsNullOrEmpty(requiredDate))
        //    {
        //        criteria.Add(Restrictions.Eq("CreationDate", Convert.ToDateTime(requiredDate)));
        //    }
        //    return criteria;
        //}

        #endregion

        #region Others Function

        public int GetGoodsTransferRowCount(List<long> authBranchFrom, List<long> authBranchTo, IList<long> authorizeProgramIds, string itemName, string requiredDate)
        {
            string returnQuery = GetGoodsTransferQuery(authBranchFrom, authBranchTo, authorizeProgramIds, itemName, requiredDate);
            string finalQuery = string.Format(@"SELECT COUNT(*) FROM ({0} ) x", returnQuery);
            IQuery iQuery = Session.CreateSQLQuery(finalQuery);
            return Convert.ToInt32(iQuery.UniqueResult());
        }

        public int GetGoodsTransferByBranchRowCount(List<long> authOrganizationFrom, List<long> authBranchFrom, List<long> authOrganizationTo, List<long> authBranchTo, List<long> authProgramIds, IList<string> branchNameList, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {
            string query = GetGoodsTransferBrachReportQuery(authOrganizationFrom, authBranchFrom, authOrganizationTo, authBranchTo, authProgramIds, branchNameList, itemId, dateFrom, dateTo);
            string finalQuery = string.Format(@"SELECT COUNT(*) FROM ({0} ) x", query);
            IQuery iQuery = Session.CreateSQLQuery(finalQuery);
            return Convert.ToInt32(iQuery.UniqueResult());
        }

        public IList<dynamic> LoadGoodsTransferByBranch(int start, int length, List<long> authOrganizationFrom, List<long> authBranchFrom, List<long> authOrganizationTo, List<long> authBranchTo, List<long> authProgramIds, IList<string> branchNameList, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {
            string query = GetGoodsTransferBrachReportQuery(authOrganizationFrom, authBranchFrom, authOrganizationTo, authBranchTo, authProgramIds, branchNameList, itemId, dateFrom, dateTo);

            if (length > 0)
            {
                query += " ORDER BY SS.Name OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }


            IQuery iQuery = Session.CreateSQLQuery(query);
            IList<dynamic> returnList = DynamicList(iQuery);
            return returnList;
        }

        public IList<dynamic> DynamicList(IQuery query)
        {
            var result = query.SetResultTransformer(ExpandoObject).List<dynamic>();
            return result;
        }

        private string GetGoodsTransferBrachReportQuery(List<long> authOrganizationFrom, List<long> authBranchFrom, List<long> authOrganizationTo, List<long> authBranchTo, List<long> authProgramIds, IList<string> branchNameList, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {
            string condition = " AND bFr.OrganizationId IN ( " + string.Join(", ", authOrganizationFrom) + ")";
            condition += " AND bFr.Id IN (" + string.Join(", ", authBranchFrom) + ")";
            condition += " AND bT.OrganizationId IN (" + string.Join(", ", authOrganizationTo) + ")";

            if (authProgramIds.Any())
            {
                condition += " AND ( p.Id IN (" + string.Join(", ", authProgramIds) + ") OR p.Id IS NULL )";
            }

            if (authBranchTo.Any())
            {
                condition += " AND bT.Id IN (" + string.Join(", ", authBranchTo) + ")";
            }

            if (!itemId.Contains(0))
            {
                condition += " AND i.Id IN (" + string.Join(", ", itemId) + ")";
            }

            condition += " AND CAST(gt.CreationDate AS date) BETWEEN '" + dateFrom.ToShortDateString() + "' AND '" + dateTo.ToShortDateString() + "'";


            string queryFirstPart = string.Format(@"SELECT SS.IGNAME,SS.Name,SS.PName,{0},{1} AS Total", JoinBranchNameForHeader(branchNameList), JoinBranchName(branchNameList));

            string query = string.Format(@"
                                           FROM (
                                                SELECT 
                                                        bT.Name AS BRTNAME,
                                                        CASE
															WHEN p.ShortName IS NULL THEN 'N/A'
																ELSE concat(p.ShortName,' ',s.Name)
															END AS PNAME,
                                                        i.Name,
                                                        ig.Name AS IGNAME,
                                                        gtd.TransferQuantity AS QUANTITY
                                                FROM    UINV_GoodsTransfer gt
                                                        INNER JOIN Branch bFr ON bFr.Id = gt.BranchFromId
                                                        INNER JOIN Branch bT ON bT.Id = gt.BranchToId
                                                        INNER JOIN UINV_GoodsTransferDetails gtd ON gtd.GoodsTransferId = gt.Id
                                                        INNER JOIN UINV_Item i ON i.Id=gtd.ItemId
                                                        INNER JOIN UINV_ItemGroup ig ON i.ItemGroupId=ig.Id
                                                        LEFT JOIN Program p ON p.Id=gtd.ProgramId
                                                        LEFT JOIN [Session] s ON s.Id =gtd.SessionId
                                                  WHERE 1=1 
                                                 " + condition + @"
                                                 ) AS PIV  
                                                 PIVOT
	                                             (
                                                     SUM(QUANTITY)
                                                     FOR BRTNAME IN (" + ExtractBranchName(branchNameList) + ")"
                                               + ") as SS"
                                             );

            return queryFirstPart + query;
        }

        public IList<dynamic> LoadGoodsTransferByProgram(int start, int length, List<long> authOrganizationFrom, List<long> authBranchFrom, List<long> authOrganizationTo, List<long> authBranchTo,List<long> authProgramIds , IList<string> programNameList, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {
            string query = GetGoodsTransferProgramReportQuery(authOrganizationFrom, authBranchFrom, authOrganizationTo, authBranchTo, authProgramIds,programNameList, itemId, dateFrom, dateTo);

            if (length > 0)
            {
                query += " ORDER BY SS.Name OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }


            IQuery iQuery = Session.CreateSQLQuery(query);
            IList<dynamic> returnList = DynamicList(iQuery);
            return returnList;


        }

        private string GetGoodsTransferProgramReportQuery(List<long> authOrganizationFrom, List<long> authBranchFrom, List<long> authOrganizationTo, List<long> authBranchTo, List<long> authProgramIds ,IList<string> programNameList, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {

            var orgFrom = string.Join(", ", authOrganizationFrom);
            var branchFrom = string.Join(", ", authBranchFrom);
            var orgTo = string.Join(", ", authOrganizationTo);
            // var branchTo = string.Join(", ", authBranchTo);
            var item = string.Join(", ", itemId);

            string queryFirstPart = string.Format(@"SELECT SS.GroupName,SS.Name,{0},{1} AS Total", JoinBranchNameForHeader(programNameList), JoinBranchName(programNameList));

            string condition = "";

            if (authProgramIds.Count > 0)
            {
                condition += " AND ( p.Id IN (" + string.Join(", ", authProgramIds) + ") OR p.Id IS NULL )";
            }

            if (authBranchTo.Count > 0)
            {
                condition += " AND bT.Id IN (" + string.Join(", ", authBranchTo) + ")";
            }

            if (!itemId.Contains(0))
            {
                condition += " AND i.Id IN (" + string.Join(", ", itemId) + ")";
            }

            string query = string.Format(@"
                                           FROM (
                                                SELECT 
                                                        ig.Name AS GroupName,
                                                        i.Name,
                                                            CASE
															WHEN p.ShortName IS NULL AND s.Name IS NULL THEN 'N/A'
															ELSE concat(p.ShortName,' ',s.Name) END 
                                                        AS PNAME,
                                                        gtd.TransferQuantity AS QUANTITY
                                                FROM    UINV_GoodsTransfer gt
                                                        INNER JOIN Branch bFr ON bFr.Id = gt.BranchFromId
                                                        INNER JOIN Branch bT ON bT.Id = gt.BranchToId
                                                        INNER JOIN UINV_GoodsTransferDetails gtd ON gtd.GoodsTransferId = gt.Id
                                                        INNER JOIN UINV_Item i ON i.Id=gtd.ItemId
                                                        INNER JOIN UINV_ItemGroup ig ON ig.Id=i.ItemGroupId
														LEFT JOIN Program p ON p.Id=gtd.ProgramId
                                                        LEFT JOIN [Session] s ON s.Id =gtd.SessionId
                                                  WHERE 1=1 
                                                  AND bFr.OrganizationId IN ({0})
                                                  AND bFr.Id IN ({1})
                                                  AND bT.OrganizationId IN ({2})
                                                   " + condition + @"
                                                  AND  CAST(gt.CreationDate AS date) BETWEEN '{3}' AND '{4}') AS PIV  
                                                 PIVOT
	                                             (
                                                     SUM(QUANTITY)
                                                     FOR PNAME IN (" + ExtractBranchName(programNameList) + ")"
                                         + ") as SS"
                                         , orgFrom
                                         , branchFrom
                                         , orgTo
                                         , dateFrom.ToShortDateString()
                                         , dateTo.ToShortDateString());

            return queryFirstPart + query;
        }

        public int GetGoodsTransferByProgramRowCount(List<long> authOrganizationFrom, List<long> authBranchFrom, List<long> authOrganizationTo, List<long> authBranchTo, List<long> authProgramIds, IList<string> programNameList, long[] itemId, DateTime dateFrom, DateTime dateTo)
        {
            string query = GetGoodsTransferProgramReportQuery(authOrganizationFrom, authBranchFrom, authOrganizationTo, authBranchTo, authProgramIds,programNameList, itemId, dateFrom, dateTo);
            string finalQuery = string.Format(@"SELECT COUNT(*) FROM ({0} ) x", query);
            IQuery iQuery = Session.CreateSQLQuery(finalQuery);
            return Convert.ToInt32(iQuery.UniqueResult());
        }

        #endregion

        #region Helper Function

        private string ExtractBranchName(IList<string> branchNameList)
        {
            string branch = branchNameList.Aggregate("", (b1, b2) => b1 + ("[" + b2 + "],"));
            return branch.TrimEnd(',');
        }

        private string JoinBranchNameForHeader(IList<string> branchNameList)
        {
            string branch = branchNameList.Aggregate("", (b1, b2) => b1 + ("ISNULL([" + b2 + "],0) AS [" + b2 + "],"));
            return branch.TrimEnd(',');
        }

        private string JoinBranchName(IList<string> branchNameList)
        {
            string branch = branchNameList.Aggregate("", (b1, b2) => b1 + ("ISNULL([" + b2 + "],0)+"));
            return branch.TrimEnd('+');
        }

        #endregion

    }
}
