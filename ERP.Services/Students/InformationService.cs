﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Conventions;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Dao.Common;
using UdvashERP.Dao.Students;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Students
{
    public interface IInformationService : IBaseService
    {
        #region Operational Function
        bool Update(Student studentObj);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<BatchWiseAdmissionReportDto> LoadAuthorizedBatchAdmissionReport(List<UserMenu> userMenu, List<long> programIdList, List<long> sessionIdList, List<long> branchIdList,
            List<long> campusIdList, string[] batchDaysList, string[] batchTimeList, List<int> versionList,
            List<int> genderList, long[] batchIds, int start, int length);
        IList<StudentInfoEditLogDto> LoadEditedStudentsInfo(int start, int length, List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, DateTime? startDate, DateTime? endDate, int selectedStatus, List<long> batchIdList, string prnNo = null, string name = null, string mobile = null);
        IList<StudentInfoUpdateLog> LoadIndividualEditedStudentsInfo(int start, int length, List<UserMenu> _userMenu, DateTime startDate, DateTime endDate, long studentId);
        #endregion

        #region Others Function
        int GetAuthorizedBatchCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchIds,
            long[] campusIds, string[] batchDaysList, string[] batchTimeList, List<int> versionList,
            List<int> genderList, long[] batchIds);
        int GetEditedStudentsCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion, DateTime? startDate, DateTime? endDate, int selectedStatus, List<long> batchIdList, string prnNo = null, string name = null, string mobile = null);
        int GetIndividualEditCount(List<UserMenu> _userMenu, DateTime startDate, DateTime endDate, long studentId);
        #endregion

        #region  Clean
        //IList<StudentProgram> LoadStudentProgramByCriteria(List<UserMenu> userMenu, long programId, long sessionId, string searchKey, string[] informationViewList, string prnNo, string name, string mobile);

        //IList<StudentProgram> LoadStudentProgramByCriteria(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime
        //         , long[] batch, string[] informationViewList);
        //IList<StudentProgram> LoadStudentProgramCompareWithBoard(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime
        //    , long[] batch, string[] informationViewList);

        //IList<StudentProgram> LoadStudentProgramByCriteria(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime
        //    , long[] batch, long[] courseId, string startDate, string endDate, string[] informationViewList);
        #endregion

    }
    public class InformationService : BaseService, IInformationService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IStudentService _studentService;
        private readonly IStudentDao _studentDao;
        private readonly InstituteDao _instituteDao;
        private readonly IStudentExamDao _studentExamDao;
        private readonly IStudentBoardDao _studentBoardDao;
        private readonly IBatchDao _batchDao;
        private readonly IDistrictDao _districtDao;
        private readonly IStudentAdmissionSubjectDao _studentAdmissionSubjectDao;
        private readonly IStudentProgramDao _studentProgramDao;
        private IStudentAcademicInfoDao _studentAcademicInfoDao;
        private readonly IStudentInfoUpdateLogDao _studentInfoUpdateLogDao;
        private CommonHelper _commonHelper;
        public InformationService(ISession session)
        {
            Session = session;
            _studentService = new StudentService(Session);
            _studentDao = new StudentDao { Session = session };
            _instituteDao = new InstituteDao { Session = session };
            _studentExamDao = new StudentExamDao { Session = session };
            _studentBoardDao = new StudentBoardDao { Session = session };
            _studentAdmissionSubjectDao = new StudentUniversitySubjectDao() { Session = session };
            _studentProgramDao = new StudentProgramDao { Session = session };
            _districtDao = new DistrictDao { Session = session };
            _batchDao = new BatchDao { Session = session };
            _studentAcademicInfoDao = new StudentAcademicInfoDao { Session = session };
            _studentInfoUpdateLogDao = new StudentInfoUpdateLogDao { Session = session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Function

        #region Update Operation
        public bool Update(Student studentObj)
        {
            ITransaction trans = null;
            try
            {
                Student tempStudentObj = new Student();
                //using (trans = Session.BeginTransaction())
                //{
                //CheckStudentAdmissionInfoBeforeUpdate(studentObj);

                //need to check
                tempStudentObj = _studentService.LoadById(studentObj.Id);
                if (tempStudentObj == null || studentObj.StudentPrograms == null)
                {
                    throw new EmptyFieldException("Invalid Student found. Please try again!");
                }

                foreach (var sp in tempStudentObj.StudentPrograms)
                {
                    if (sp.PrnNo == studentObj.StudentPrograms[0].PrnNo)
                    {
                        sp.VersionOfStudy = studentObj.StudentPrograms[0].VersionOfStudy;
                        // sp.Institute = studentObj.StudentPrograms[0].Institute;
                        //sp.Institute = _instituteDao.LoadById(studentObj.StudentPrograms[0].Institute.Id);
                        var eiin = studentObj.StudentPrograms[0].Institute.Eiin;
                        if (!string.IsNullOrEmpty(eiin))
                        {
                            sp.Institute = _instituteDao.GetInstitute(eiin);
                        }
                        else
                        {
                            sp.Institute = null;
                        }
                        Batch batch = _batchDao.LoadById(studentObj.StudentPrograms[0].Batch.Id);
                        if (batch == null)
                        {
                            throw new EmptyFieldException("Invalid batch found!");
                        }
                        sp.Batch = batch;

                    }
                    // tempStudentObj.StudentPrograms.Add(sp);
                }

                #region 1 Admission & SIF Information


                if (studentObj.District != null && studentObj.District.Id > 0)
                {
                    tempStudentObj.District = _districtDao.LoadById(studentObj.District.Id);

                }

                tempStudentObj.NickName = studentObj.NickName;
                tempStudentObj.Mobile = studentObj.Mobile;
                tempStudentObj.Gender = studentObj.Gender;
                tempStudentObj.Religion = studentObj.Religion;
                tempStudentObj.FullName = studentObj.FullName;
                tempStudentObj.FatherName = studentObj.FatherName;
                tempStudentObj.DateOfBirth = studentObj.DateOfBirth;
                tempStudentObj.GuardiansMobile1 = studentObj.GuardiansMobile1;
                tempStudentObj.GuardiansMobile2 = studentObj.GuardiansMobile2;
                tempStudentObj.BloodGroup = studentObj.BloodGroup;
                tempStudentObj.Email = studentObj.Email;
                tempStudentObj.LastMeritPosition = studentObj.LastMeritPosition;
                tempStudentObj.Gpa = studentObj.Gpa;
                tempStudentObj.Exam = studentObj.Exam;
                tempStudentObj.Section = studentObj.Section;

                #endregion

                #region 2.2 Student Previous Academic Information

                #region check Dublicate Exam

                if (studentObj.StudentAcademicInfos != null && studentObj.StudentAcademicInfos.Count > 0)
                {
                    int dublicateStudentAcademicInfosCount = studentObj.StudentAcademicInfos.GroupBy(
                        x => x.StudentExam.Id).Select(x => new
                        {
                            Id = x.Key,
                            Count = x.Count()
                        }).Count(x => x.Count > 1);
                    if (dublicateStudentAcademicInfosCount > 0)
                    {
                        throw new EmptyFieldException("This Student has two or more same Academic Info");
                    }
                }

                #endregion

                bool jscFlag = false;
                bool sscFlag = false;
                bool hscFlag = false;

                List<StudentAcademicInfo> tempDeleteStudentAcademicInfo = new List<StudentAcademicInfo>();
                if (studentObj.StudentAcademicInfos.Count > 0)
                {
                    foreach (var temp in tempStudentObj.StudentAcademicInfos.Where(x => x.Status == StudentAcademicInfo.EntityStatus.Active).ToList())
                    {
                        if (!studentObj.StudentAcademicInfos.Select(x => x.StudentExam.Id).ToList().Contains(temp.StudentExam.Id))
                        {
                            tempDeleteStudentAcademicInfo.Add(temp);
                        }
                    }

                }
                else
                {
                    tempDeleteStudentAcademicInfo = tempStudentObj.StudentAcademicInfos.Where(x => x.Status == StudentAcademicInfo.EntityStatus.Active).ToList();
                }
                #region New && Update
                if (studentObj.StudentAcademicInfos != null && studentObj.StudentAcademicInfos.Count > 0)
                {
                    DateTime date = DateTime.Now;
                    foreach (var c in studentObj.StudentAcademicInfos)
                    {
                        bool updateAcademicFlag = false;
                        if (c.Id > 0)
                        {
                            StudentAcademicInfo tempAlreadyStudentAcademicInfo = tempStudentObj.StudentAcademicInfos.FirstOrDefault(x => x.Id == c.Id && x.Status == StudentAcademicInfo.EntityStatus.Active);
                            if (tempAlreadyStudentAcademicInfo != null)
                            {
                                tempAlreadyStudentAcademicInfo.StudentExam = _studentExamDao.LoadById(c.StudentExam.Id);
                                tempAlreadyStudentAcademicInfo.StudentBoard = _studentBoardDao.LoadById(c.StudentBoard.Id);
                                tempAlreadyStudentAcademicInfo.ModifyBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId());
                                tempAlreadyStudentAcademicInfo.ModificationDate = date;
                                tempAlreadyStudentAcademicInfo.Year = c.Year;
                                tempAlreadyStudentAcademicInfo.BoradRoll = c.BoradRoll;
                                tempAlreadyStudentAcademicInfo.RegistrationNumber = c.RegistrationNumber;
                                updateAcademicFlag = true;
                            }
                        }
                        else if (c.Id == 0)
                        {
                            StudentAcademicInfo tempNewStudentAcademicInfo = new StudentAcademicInfo
                            {
                                StudentExam = _studentExamDao.LoadById(c.StudentExam.Id),
                                StudentBoard = _studentBoardDao.LoadById(c.StudentBoard.Id),
                                Year = c.Year,
                                BoradRoll = c.BoradRoll,
                                RegistrationNumber = c.RegistrationNumber,
                                Status = StudentAcademicInfo.EntityStatus.Active,
                                Student = tempStudentObj,
                                CreateBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId()),
                                ModifyBy = Convert.ToInt64(HttpContext.Current.User.Identity.GetUserId()),
                                CreationDate = date,
                                ModificationDate = date
                            };
                            tempStudentObj.StudentAcademicInfos.Add(tempNewStudentAcademicInfo);
                            updateAcademicFlag = true;
                        }
                        if (updateAcademicFlag)
                        {
                            //student information update according to acedimic info
                            switch (c.StudentExam.Id)
                            {
                                case 2:
                                    jscFlag = true;
                                    break;
                                case 3:
                                    sscFlag = true;
                                    break;
                                case 4:
                                    hscFlag = true;
                                    break;
                            }
                            //student information update according to acedimic info End
                        }

                    }
                }
                #endregion
                #region delete

                if (tempDeleteStudentAcademicInfo.Any())
                {
                    DateTime date = DateTime.Now;
                    foreach (var temp in tempDeleteStudentAcademicInfo)
                    {
                        temp.Status = StudentAcademicInfo.EntityStatus.Delete;
                        temp.ModificationDate = date;
                        //student information update according to acedimic info
                        switch (temp.StudentExam.Id)
                        {
                            case 2:
                                jscFlag = false;
                                break;
                            case 3:
                                sscFlag = false;
                                break;
                            case 4:
                                hscFlag = false;
                                break;
                        }
                        //student information update according to acedimic info End
                    }
                }
                #endregion
                tempStudentObj.IsJsc = jscFlag;
                tempStudentObj.IsSsc = sscFlag;
                tempStudentObj.IsHsc = hscFlag;

                #endregion

                #region 3 Call Center Information

                #region 3.3 Student Admission Information

                tempStudentObj.StudentUniversityInfos.Clear();

                if (studentObj.StudentUniversityInfos != null && studentObj.StudentUniversityInfos.Count > 0)
                {
                    foreach (var c in studentObj.StudentUniversityInfos)
                    {
                        c.Student = tempStudentObj;
                        if (c.Institute.Id > 0 && c.StudentUniversitySubject.Id > 0)
                        {
                            c.Institute = _instituteDao.LoadById(c.Institute.Id);
                            c.StudentUniversitySubject = _studentAdmissionSubjectDao.LoadById(c.StudentUniversitySubject.Id);
                            tempStudentObj.StudentUniversityInfos.Add(c);
                        }

                        //validateStudentCurrentAcademicInfos.Add(c);
                    }
                }
                #endregion
                #endregion

                using (trans = Session.BeginTransaction())
                {
                    _studentDao.SaveOrUpdate(tempStudentObj);
                    trans.Commit();
                    return true;
                }
            }

            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
        }


        #endregion

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<BatchWiseAdmissionReportDto> LoadAuthorizedBatchAdmissionReport(List<UserMenu> userMenu, List<long> programIdList, List<long> sessionIdList, List<long> branchIdList,
            List<long> campusIdList, string[] batchDaysList, string[] batchTimeList, List<int> versionList,
            List<int> genderList, long[] batchIds, int start, int length)
        {
            try
            {
                //List<long> authOrganizationIdList = AuthHelper.LoadOrganizationIdList(userMenu);
                //List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu);
                //List<long> authorizedBranchLists = AuthHelper.LoadBranchIdList(userMenu, null, programIdList);

                List<long> authProgramIdList = AuthHelper.LoadProgramIdList(userMenu, null, CommonHelper.ConvertSelectedAllIdList(branchIdList), CommonHelper.ConvertSelectedAllIdList(programIdList));
                List<long> authBranchIdList = AuthHelper.LoadBranchIdList(userMenu, null, CommonHelper.ConvertSelectedAllIdList(programIdList), CommonHelper.ConvertSelectedAllIdList(branchIdList));

                return _batchDao.LoadAuthorizedBatchAdmissionReport(authProgramIdList, authBranchIdList, CommonHelper.ConvertSelectedAllIdList(sessionIdList),
                CommonHelper.ConvertSelectedAllIdList(campusIdList), batchDaysList, batchTimeList, versionList,
                genderList, batchIds, start, length);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        public IList<StudentInfoEditLogDto> LoadEditedStudentsInfo(int start, int length, List<UserMenu> userMenu, long programId, long sessionId,
            long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, long[] courseId,
            int version, int gender, int[] religion, DateTime? startDate, DateTime? endDate, int selectedStatus, List<long> batchIdList,
            string prnNo = null, string name = null, string mobile = null)
        {
            try
            {
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu);//by menu
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                return _studentInfoUpdateLogDao.LoadEditedStudentsInfo(start, length, authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus, batchIdList, prnNo, name, mobile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public IList<StudentInfoUpdateLog> LoadIndividualEditedStudentsInfo(int start, int length, List<UserMenu> _userMenu,
            DateTime startDate, DateTime endDate, long studentId)
        {

            try
            {
                return _studentInfoUpdateLogDao.LoadIndividualEditedStudentsInfo(start, length, _userMenu, startDate, endDate, studentId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }
        #endregion

        #region Others Function
        public int GetAuthorizedBatchCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchIds,
            long[] campusIds, string[] batchDaysList, string[] batchTimeList, List<int> versionList,
            List<int> genderList, long[] batchIds)
        {
            try
            {
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu);//by menu
                List<long> authorizedBranchLists = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));

                return _batchDao.GetAuthorizedBatchCount(authorizedProgramLists, authorizedBranchLists, programId, sessionId, branchIds,
                campusIds, batchDaysList, batchTimeList, versionList,
                genderList, batchIds);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetEditedStudentsCount(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId,
           string[] batchDays, string[] batchTime, long[] batchId, long[] courseId, int version, int gender, int[] religion,
           DateTime? startDate, DateTime? endDate, int selectedStatus, List<long> batchIdList, string prnNo = null, string name = null,
           string mobile = null)
        {
            try
            {
                List<long> authorizedProgramLists = AuthHelper.LoadProgramIdList(userMenu);//by menu
                List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
                return _studentInfoUpdateLogDao.GetEditedStudentsCount(authorizedProgramLists, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batchId, courseId, version, gender, religion, startDate, endDate, selectedStatus, batchIdList, prnNo, name, mobile);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        public int GetIndividualEditCount(List<UserMenu> _userMenu, DateTime startDate, DateTime endDate, long studentId)
        {
            try
            {
                return _studentInfoUpdateLogDao.GetIndividualEditCount(_userMenu, startDate, endDate, studentId);
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                return 0;
            }
        }
        #endregion

        #region Helper Function

        #endregion

        #region Clean
        //public IList<StudentProgram> LoadStudentProgramByCriteria(List<UserMenu> userMenu, long programId, long sessionId, string searchKey, string[] informationViewList, string prnNo, string name, string mobile)
        //{
        //    IList<StudentProgram> studentPrograms;
        //    try
        //    {
        //        //bool allBranch = false;
        //        //bool allProgram = false;
        //        //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
        //        //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);

        //        List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
        //        List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));

        //        studentPrograms = _studentProgramDao.LoadStudentProgramByCriteria(programIdList, branchIdList, programId, sessionId, searchKey, informationViewList, prnNo, name, mobile);

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    return studentPrograms;
        //}
        //public IList<StudentProgram> LoadStudentProgramByCriteria(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, string[] informationViewList)
        //{
        //    IList<StudentProgram> studentPrograms;
        //    try
        //    {
        //        //bool allBranch = false;
        //        //bool allProgram = false;
        //        //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
        //        //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);

        //        List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
        //        List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));
        //        studentPrograms = _studentProgramDao.LoadStudentProgramByCriteria(programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batch, informationViewList);

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    return studentPrograms;
        //}
        //public IList<StudentProgram> LoadStudentProgramCompareWithBoard(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays, string[] batchTime, long[] batch, string[] informationViewList)
        //{
        //    IList<StudentProgram> studentPrograms;
        //    try
        //    {
        //        //bool allBranch = false;
        //        //bool allProgram = false;
        //        //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
        //        //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);
        //        List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
        //        List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));

        //        studentPrograms = _studentProgramDao.LoadStudentProgramCompareWithBoard(programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batch, informationViewList);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    return studentPrograms;

        //}
        //public IList<StudentProgram> LoadStudentProgramByCriteria(List<UserMenu> userMenu, long programId, long sessionId, long[] branchId, long[] campusId, string[] batchDays
        //    , string[] batchTime, long[] batch, long[] courseId, string startDate, string endDate, string[] informationViewList)
        //{
        //    IList<StudentProgram> studentPrograms;
        //    try
        //    {
        //        //bool allBranch = false;
        //        //bool allProgram = false;
        //        //long[] authorizedProgramLists = AuthHelper.GetUserProgramIdList(userMenu, out allProgram);
        //        //long[] authorizedBranchLists = AuthHelper.GetUserBranchIdList(userMenu, out allBranch);
        //        List<long> programIdList = AuthHelper.LoadProgramIdList(userMenu);
        //        List<long> branchIdList = AuthHelper.LoadBranchIdList(userMenu, null, _commonHelper.ConvertIdToList(programId));

        //        studentPrograms = _studentProgramDao.LoadStudentProgramByCriteria(programIdList, branchIdList, programId, sessionId, branchId, campusId, batchDays, batchTime, batch, courseId, startDate, endDate, informationViewList);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        throw;
        //    }
        //    return studentPrograms;

        //}
        #endregion
    }
}
