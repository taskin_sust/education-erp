﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration
{
    public class ProgramStudentExamSessionMap : BaseClassMap<ProgramStudentExamSession, long>
    {
        public ProgramStudentExamSessionMap()
        {
            Table("ProgramStudentExamSession");
            LazyLoad();
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");
            References(x => x.StudentExam).Column("StudentExamId");
        }
    }
}
