﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.Students;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessRules;

namespace UdvashERP.Dao.Students
{
    public interface IStudentAcademicInfoDao : IBaseDao<StudentAcademicInfo, long>
    {      
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        StudentAcademicInfo LoadByStudentId(long studentId, long studentExmId);
        
        #endregion

        #region List Loading Function

        IList<ViewBoardInfoSynchronizerDto> LoadStudentForInfoSynchronizer(List<long> authoProgramIdList, List<long> authoBranchIdList, int start, int length, long sessionId, long studentExamId, string year, long[] campusId, string[] batchDays, string[] batchTimes, long[] batch, int? syncStatus, string prnNo, string name, string mobile);
        IList<ViewBoardRollListDto> LoadAuthorizedStudents(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, int selectStatus, string orderBy, string orderDir, int start, int length, string prnNo, string name, string mobile, string[] informationViewList);

        #endregion

        #region Others Function

        long StudentForInfoSynchronizerCount(List<long> authoProgramIdList, List<long> authoBranchIdList, long sessionId, long studentExamId, string year, long[] campus, string[] batchDays, string[] batchTimes, long[] batch, int? status);
        long GetAuthorizedStudentsCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, int selectStatus, string prnNo, string name, string mobile, string[] informationViewList);
        
        #endregion

        #region Clean
        //StudentAcademicInfo LoadByStudentId(long studentId);
        #endregion

        
    }

    public class StudentAcademicInfoDao : BaseDao<StudentAcademicInfo, long>, IStudentAcademicInfoDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public StudentAcademicInfo LoadByStudentId(long studentId, long studentExmId)
        {
            ICriteria criteria = Session.CreateCriteria<StudentAcademicInfo>();
            criteria.Add(Restrictions.Eq("Status", StudentAcademicInfo.EntityStatus.Active));
            criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Id", studentId)).Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
            criteria.CreateAlias("StudentExam", "se")
                .Add(Restrictions.Eq("se.Id", studentExmId))
                .Add(Restrictions.Eq("se.Status", StudentExam.EntityStatus.Active));
            return criteria.UniqueResult<StudentAcademicInfo>();
        }

        #endregion

        #region List Loading Function

        public IList<ViewBoardInfoSynchronizerDto> LoadStudentForInfoSynchronizer(List<long> authoProgramIdList, List<long> authoBranchIdList, int start, int length, long sessionId, long studentExamId, string year, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, int? statusSync, string prnNo, string name, string mobile)
        {
            string query = GetStudentForInfoSynchronizerQuery(authoProgramIdList, authoBranchIdList, sessionId, studentExamId, year, campusIds, batchDays, batchTimes, batchIds, statusSync, prnNo, name, mobile);
            query = query + " Order By ProgramRoll ASC ";
            if (length > 0)
            {
                query += " OFFSET " + start + " ROWS" + " FETCH NEXT " + length + " ROWS ONLY";
            }

            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ViewBoardInfoSynchronizerDto>());
            iQuery.SetTimeout(2700);
            List<ViewBoardInfoSynchronizerDto> list = iQuery.List<ViewBoardInfoSynchronizerDto>().ToList();
            return list;

        }
        public IList<ViewBoardRollListDto> LoadAuthorizedStudents(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, int selectStatus, string orderBy, string orderDir, int start, int length, string prnNo, string name, string mobile, string[] informationViewList)
        {
            string innerQuery = GetInnerQueryStudentList(authorizedProgramLists, authorizedBranchLists, sessionId, campusId, batchDays, batchTime, batchId, selectStatus, prnNo, name, mobile, informationViewList);

            string query = @" Select A.* 
            					    ,SAc.JscYear, SAc.JscRoll, SAc.JscRegistrationNumber, SAc.JscBoard
            					    ,SAc.SscYear, SAc.SscRoll, SAc.SscRegistrationNumber, SAc.SscBoard
            					    ,SAc.HscYear, SAc.HscRoll, SAc.HscRegistrationNumber, SAc.HscBoard
                                from #StudentSummary AS A
                                left join  #AcademicSummary AS SAc ON SAc.StudentId = A.StudentId
            				    where 1=1";

            if (selectStatus == 1)
            {
                query += " AND (SAc.IsBoardRoll = 1 and SAc.IsRegistrationNumber = 1) ";
            }
            else if (selectStatus == 2)
            {
                query += " AND (SAc.IsBoardRoll = 0 or SAc.IsRegistrationNumber = 0 or SAc.IsBoardRoll is null or SAc.IsRegistrationNumber is null ) ";
            }

            string listQuery = "DECLARE @rowsperpage INT DECLARE @start INT SET @start = " + (start + 1) + " SET @rowsperpage = " + length + " " + innerQuery +
                          " select * from ( SELECT row_number() OVER (ORDER BY " + orderBy + " " + orderDir + ") AS RowNum,  A.* FROM(" + query + ") AS A" +
                         ") as pagination";

            if (length > 0)
            {
                listQuery += " where pagination.RowNum BETWEEN (@start) AND (@start + @rowsperpage-1) ";
            }

            listQuery = listQuery + @" IF EXISTS(SELECT * FROM #StudentSummary) DROP TABLE #StudentSummary;
                             IF EXISTS(SELECT * FROM #AcademicSummary) DROP TABLE #AcademicSummary;";

            IQuery iQuery = Session.CreateSQLQuery(listQuery);
            iQuery.SetResultTransformer(Transformers.AliasToBean<ViewBoardRollListDto>());
            return iQuery.List<ViewBoardRollListDto>().ToList();

        }

        #endregion

        #region Others Function

        public long StudentForInfoSynchronizerCount(List<long> authoProgramIdList, List<long> authoBranchIdList, long sessionId, long studentExamId, string year, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, int? statusSync)
        {
            string query = GetStudentForInfoSynchronizerQuery(authoProgramIdList, authoBranchIdList, sessionId, studentExamId, year, campusIds, batchDays, batchTimes, batchIds, statusSync);

            string countQuery = "Select count(*) From ( " + query + " ) as a ";
            IQuery iQuery = Session.CreateSQLQuery(countQuery);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public long GetAuthorizedStudentsCount(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, int selectStatus, string prnNo, string name, string mobile, string[] informationViewList)
        {
            string innerQuery = GetInnerQueryStudentList(authorizedProgramLists, authorizedBranchLists, sessionId, campusId, batchDays, batchTime, batchId, selectStatus, prnNo, name, mobile, informationViewList);

            string query = @" Select A.* 
            					    ,SAc.JscYear, SAc.JscRoll, SAc.JscRegistrationNumber, SAc.JscBoard
            					    ,SAc.SscYear, SAc.SscRoll, SAc.SscRegistrationNumber, SAc.SscBoard
            					    ,SAc.HscYear, SAc.HscRoll, SAc.HscRegistrationNumber, SAc.HscBoard
                                from #StudentSummary AS A
                                left join  #AcademicSummary AS SAc ON SAc.StudentId = A.StudentId
            				    where 1=1";

            if (selectStatus == 1)
            {
                query += " AND (SAc.IsBoardRoll = 1 and SAc.IsRegistrationNumber = 1) ";
            }
            else if (selectStatus == 2)
            {
                query += " AND (SAc.IsBoardRoll = 0 or SAc.IsRegistrationNumber = 0 or SAc.IsBoardRoll is null or SAc.IsRegistrationNumber is null ) ";
            }

            string countQuery = innerQuery + " SELECT count(A.Id) as total FROM(" + query + ") as A";

            countQuery = countQuery + @" IF EXISTS(SELECT * FROM #StudentSummary) DROP TABLE #StudentSummary;
                             IF EXISTS(SELECT * FROM #AcademicSummary) DROP TABLE #AcademicSummary;";

            IQuery iQuery = Session.CreateSQLQuery(countQuery);
            iQuery.SetTimeout(2000);
            return Convert.ToInt32(iQuery.UniqueResult());
        }

        #endregion

        #region Helper Function

        private string GetStudentForInfoSynchronizerQuery(List<long> authoProgramIdList, List<long> authoBranchIdList, long sessionId, long studentExamId, string year, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, int? statusSync, string prnNo = "", string name = "", string mobile = "")
        {
            #region filter section

            string programFilter = "";
            string branchFilter = "";
            string campusFilter = "";
            string batchDaysFilter = "";
            string batchTimeFilter = "";
            string batchFilter = "";
            string isSyncSelect = "";
            string isSyncJoin = "";
            string isSyncFilter = "";
            string prnNoFilter = "";
            string nameFilter = "";
            string mobileFilter = "";

            if (authoProgramIdList.Any() && !authoProgramIdList.Contains(SelectionType.SelelectAll))
            {
                programFilter = " and sp.ProgramId in(" + string.Join(",", authoProgramIdList.ToArray()) + ") ";
            }
            if (authoBranchIdList.Any() && !authoBranchIdList.Contains(SelectionType.SelelectAll))
            {
                branchFilter = " and br.Id in(" + string.Join(",", authoBranchIdList.ToArray()) + ") ";
            }
            if (!(Array.Exists(campusIds, item => item == 0)))
            {
                campusFilter = " and cam.Id in(" + string.Join(",", campusIds) + ") ";
            }
            if (!(Array.Exists(batchIds, item => item == 0)))
            {
                batchFilter = " and b.Id in(" + string.Join(",", batchIds) + ") ";
            }
            if (statusSync != null && statusSync == 1)
            {
                isSyncSelect = " , 1 as IsSync ";
                isSyncJoin = " inner join StudentAcademicInfoBaseData as saibd on saibd.StudentAcademicInfoId = sai.Id ";
                isSyncFilter = " and saiFilter.IsSync = 1 ";
            }
            else if (statusSync != null && statusSync == 0)
            {
                isSyncSelect = " , CASE WHEN saibd.Id is null then 0 ELSE 1 END as IsSync ";
                isSyncJoin = " left join StudentAcademicInfoBaseData as saibd on saibd.StudentAcademicInfoId = sai.Id ";
                isSyncFilter = " and saiFilter.IsSync = 0 ";
            }
            if (!String.IsNullOrEmpty(prnNo))
            {
                prnNoFilter = " AND sp.PrnNo = '" + prnNo + "' ";
            }
            if (!String.IsNullOrEmpty(name))
            {
                nameFilter = " AND s.NickName like '%" + name + "%' ";
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                mobileFilter = " AND s.MobileNumber like '%" + mobile + "%' ";
            }
            if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTimes)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                batchTimeFilter = " and (" + btQ + ")";
            }
            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {

                batchDaysFilter = " and b.Days in ('" + string.Join("','", batchDays) + "')";
            }

            #endregion

            string query = "";

            query = @"select 
                        sp.Id as StudentProgramId
                        , sp.StudentId as StudentId
                        , sp.PrnNo as ProgramRoll
                        , s.FullName as FullName
                        , s.Mobile as MobileNumber
                        , s.FatherName as FatherName
                        , i.Name as Institute
                        , s.NickName as NickName
                        , s.GuardiansMobile1 as FatherMobileNumber  
                        , s.GuardiansMobile2 as MotherMobileNumber
                        , saiFilter.Year as 'Year'
                        , saiFilter.BoardId as BoardId
                        , stdbr.Name as BoardName
                        , saiFilter.BoardRoll as BoardRoll
                        , saiFilter.RegistrationNumber as RegistrationNumber
                        , saiFilter.StudentExamId as StudentExamId
                        from StudentProgram as sp 
                        inner join Student as s on s.Id = sp.StudentId and s.Status = " + Student.EntityStatus.Active + @"
                        inner join Batch as b on b.Id = sp.BatchId and b.Status = " + Batch.EntityStatus.Active + " and b.SessionId = " + sessionId + @"
                        inner join Branch as br on br.Id = b.BranchId and br.Status = " + Branch.EntityStatus.Active + @"
                        inner join Campus as cam on cam.Id = b.CampusId and cam.Status = " + Campus.EntityStatus.Active + @"
                        inner join (
	                        select 
	                        sai.Id as StudentAcademicInfoId
	                        , sai.StudentId as StudentId
	                        , sai.BoardId as BoardId
	                        , sai.BoradRoll as BoardRoll
	                        , sai.Year as 'Year'
	                        , sai.StudentExamId as StudentExamId
	                        , sai.RegistrationNumber as RegistrationNumber
	                        , RANK() OVER(PARTITION BY sai.StudentId ORDER BY sai.Id DESC) AS R  
	                        " + isSyncSelect + @"
	                        from StudentAcademicInfo  as  sai 
	                        " + isSyncJoin + @"
	                        where sai.Status = " + StudentAcademicInfo.EntityStatus.Active + @"
	                        and sai.Year = '" + year + @"'
	                        and sai.StudentExamId = " + studentExamId + @"
	                        and (sai.BoradRoll is not null or sai.BoradRoll !='')
	                        and (sai.RegistrationNumber is not null or sai.RegistrationNumber !='')
                        ) as  saiFilter on saiFilter.StudentId = sp.StudentId and saiFilter.R = 1
                        inner join StudentExam as stdex on stdex.Id = saiFilter.StudentExamId and stdex.Status = " + StudentExam.EntityStatus.Active + @"
                        inner join StudentBoard as stdbr on stdbr.Id = saiFilter.BoardId and stdbr.Status = " + StudentBoard.EntityStatus.Active + @"
                        left join Institute as i on i.Id = sp.InstituteId and i.Status = " + Institute.EntityStatus.Active + @"
                        where sp.Status = " + StudentProgram.EntityStatus.Active + " " + programFilter + branchFilter + campusFilter + batchDaysFilter + batchTimeFilter + batchFilter + isSyncFilter + prnNoFilter + nameFilter + mobileFilter + @"
                        ";

            return query;
        }

        //private ICriteria GetStudentForInfoSynchronizerQuery(List<long> authoProgramIdList, List<long> authoBranchIdList, long sessionId, long studentExamId, string year, long[] campusIds, string[] batchDays, string[] batchTimes, long[] batchIds, int? statusSync, string prnNo = "", string name = "", string mobile = "")
        //{
        //    ICriteria criteria = Session.CreateCriteria<StudentProgram>().Add(Restrictions.Eq("Status", StudentProgram.EntityStatus.Active));
        //    criteria.CreateCriteria("Batch", "b", JoinType.InnerJoin).Add(Restrictions.Eq("b.Status", Batch.EntityStatus.Active));
        //    criteria.CreateAlias("Program", "p").Add(Restrictions.Eq("p.Status", Program.EntityStatus.Active));
        //    criteria.CreateAlias("Student", "st").Add(Restrictions.Eq("st.Status", Student.EntityStatus.Active));
        //    criteria.CreateAlias("b.Branch", "br").Add(Restrictions.Eq("br.Status", Branch.EntityStatus.Active));
        //    criteria.CreateAlias("b.Session", "se").Add(Restrictions.Eq("se.Status", BusinessModel.Entity.Administration.Session.EntityStatus.Active));
        //    criteria.CreateAlias("b.Campus", "ca").Add(Restrictions.Eq("ca.Status", Campus.EntityStatus.Active));

        //    criteria.CreateCriteria("st.StudentAcademicInfos", "sai", JoinType.InnerJoin)
        //        .Add(Restrictions.Eq("sai.Status", StudentAcademicInfo.EntityStatus.Active));
        //    criteria.CreateCriteria("sai.StudentExam", "studentExm")
        //        .Add(Restrictions.Eq("studentExm.Status", StudentExam.EntityStatus.Active));

        //    if (authoProgramIdList != null)
        //    {
        //        criteria.Add(Restrictions.In("Program.Id", authoProgramIdList));
        //    }
        //    if (authoBranchIdList != null)
        //    {
        //        criteria.Add(Restrictions.In("b.Branch.Id", authoBranchIdList));
        //    }
        //    if (!String.IsNullOrEmpty(prnNo))
        //    {
        //        criteria.Add(Restrictions.Like("PrnNo", prnNo, MatchMode.Anywhere));
        //    }
        //    if (!String.IsNullOrEmpty(name))
        //    {
        //        criteria.Add(Restrictions.Like("st.NickName", name, MatchMode.Anywhere));
        //    }
        //    if (!String.IsNullOrEmpty(mobile))
        //    {
        //        criteria.Add(Restrictions.Like("st.Mobile", mobile, MatchMode.Anywhere));
        //    }
        //    criteria.Add(Restrictions.Eq("b.Session.Id", sessionId));

        //    if (!(Array.Exists(campusIds, item => item == 0)))
        //    {
        //        criteria.Add(Restrictions.In("b.Campus.Id", campusIds));
        //    }
        //    if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")))
        //    {
        //        criteria.Add(Restrictions.In("b.Days", batchDays));
        //    }

        //    if (batchTimes != null && !(Array.Exists(batchTimes, item => item == "0")))
        //    {
        //        var disjunction = Restrictions.Disjunction(); // for OR statement 
        //        foreach (string bt in batchTimes)
        //        {
        //            var batchArrays = bt.Replace("To", ",");
        //            var batchArray = batchArrays.Split(',');
        //            var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
        //            var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
        //            disjunction.Add(Restrictions.Ge("b.StartTime", startTime) && Restrictions.Le("b.EndTime", endTime));
        //        }
        //        criteria.Add(disjunction);
        //    }

        //    if ((!Array.Exists(batchIds, item => item == 0)))
        //    {
        //        criteria.Add(Restrictions.In("Batch.Id", batchIds));
        //    }

        //    criteria.Add(Restrictions.Eq("sai.Year", year));
        //    criteria.Add(Restrictions.Eq("studentExm.Id", studentExamId));
        //    if (statusSync != null)
        //    {
        //        criteria.CreateCriteria("sai.StudentAcademicInfoBaseData", "saiBase", JoinType.LeftOuterJoin);
        //        if (statusSync == 1)
        //            criteria.Add(Restrictions.IsNotNull("saiBase.StudentAcademicInfo"));
        //        else
        //            criteria.Add(Restrictions.IsNull("saiBase.StudentAcademicInfo"));
        //    }

        //    return criteria;

        //}

        private string GetInnerQueryStudentList(List<long> authorizedProgramLists, List<long> authorizedBranchLists, long sessionId, long[] campusId, string[] batchDays, string[] batchTime, long[] batchId, int selectStatus, string prnNo, string name, string mobile, string[] informationViewList)
        {

            List<string> studentExamBusinessIdList = new List<string>();
            string studentExamBusinessIdFilterList = "";
            if (informationViewList.Any())
            {
                if (informationViewList.ToList().Contains("JSC Year") ||
                    informationViewList.ToList().Contains("JSC Board") ||
                    informationViewList.ToList().Contains("JSC Roll") ||
                    informationViewList.ToList().Contains("JSC Registration Number"))
                {
                    studentExamBusinessIdList.Add("jsc");
                } 
                if (informationViewList.ToList().Contains("SSC Year") ||
                     informationViewList.ToList().Contains("SSC Board") ||
                     informationViewList.ToList().Contains("SSC Roll") ||
                     informationViewList.ToList().Contains("SSC Registration Number"))
                {
                    studentExamBusinessIdList.Add("ssc");
                }
                if (informationViewList.ToList().Contains("HSC Year") ||
                   informationViewList.ToList().Contains("HSC Board") ||
                   informationViewList.ToList().Contains("HSC Roll") ||
                   informationViewList.ToList().Contains("HSC Registration Number"))
                {
                    studentExamBusinessIdList.Add("hsc");
                }
            }
            if (studentExamBusinessIdList.Any())
            {
                studentExamBusinessIdFilterList = " AND  SE.BusinessId IN ('" + string.Join("','", studentExamBusinessIdList.ToArray()) + "') ";
            }


            string query =
                @"SELECT sp.Id, sp.PrnNo,sp.StudentId, st.NickName,st.FullName,st.Mobile,st.GuardiansMobile1,st.GuardiansMobile2
                from StudentProgram sp 
                inner join Student st on st.Id = sp.StudentId 
                inner join Batch b on b.Id = sp.BatchId 
                inner join Program p on p.Id = b.ProgramId 
                inner join Branch br on br.Id = b.BranchId 
                inner join [Session] s on s.Id = b.SessionId 
                inner join Campus c on c.Id = b.CampusId";

            string whereClause = @" Where  st.Status = " + Student.EntityStatus.Active + @" and p.Status = " + Program.EntityStatus.Active + @" and b.Status = " + Batch.EntityStatus.Active + @" and br.Status = " + Branch.EntityStatus.Active + @" and s.Status = " + BusinessModel.Entity.Administration.Session.EntityStatus.Active + @" and c.Status = " + Campus.EntityStatus.Active + @"  and sp.Status!=" + StudentProgram.EntityStatus.Delete;

           // whereClause += " and s.Id = " + sessionId + " and p.Id = " + programId;
            whereClause += " and s.Id = " + sessionId;
            
            if (authorizedProgramLists != null)
            {
                whereClause += " and p.Id in(" + string.Join(",", authorizedProgramLists.ToArray()) + ") ";
            }

            if (authorizedBranchLists != null)
            {
                whereClause += " and br.Id in(" + string.Join(",", authorizedBranchLists.ToArray()) + ") ";
            }

            //if (!(Array.Exists(branchId, item => item == 0)))
            //{
            //    whereClause += " and br.Id in(" + string.Join(",", branchId) + ") ";
            //}

            if (!(Array.Exists(campusId, item => item == 0)))
            {
                whereClause += " and c.Id in(" + string.Join(",", campusId) + ") ";
            }

            if (batchDays != null && !(Array.Exists(batchDays, item => item == "0")) && !(Array.Exists(batchDays, item => item == "")))
            {

                whereClause += " and b.Days in ('" + string.Join("','", batchDays) + "')";
            }
            if (batchTime != null && !(Array.Exists(batchTime, item => item == "0")))
            {
                string btQ = "";
                int iteration = 0;
                foreach (string bt in batchTime)
                {
                    var batchArrays = bt.Replace("To", ",");
                    var batchArray = batchArrays.Split(',');
                    var startTime = Convert.ToDateTime("2000-01-01 " + batchArray[0]);
                    var endTime = Convert.ToDateTime("2000-01-01 " + batchArray[1]);
                    if (iteration == 0)
                    {
                        btQ += "(b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    else
                    {
                        btQ += " or (b.StartTime >= '" + startTime + "' and b.EndTime <= '" + endTime + "')";
                    }
                    iteration++;
                }
                whereClause += " and (" + btQ + ")";
            }

            if (!(Array.Exists(batchId, item => item == 0)))
            {
                whereClause += " and b.Id in (" + string.Join(",", batchId) + ")";
            }


            if (!String.IsNullOrEmpty(prnNo))
            {
                whereClause += " and sp.PrnNo like '%" + prnNo + "%'";
            }
            if (!String.IsNullOrEmpty(name))
            {
                whereClause += " and st.NickName like '%" + name + "%'";
            }
            if (!String.IsNullOrEmpty(mobile))
            {
                whereClause += " and st.Mobile like '%" + mobile + "%'";
            }
            query += whereClause;


            query = "Select * INTO #StudentSummary from (" + query + ") AS A";

            query += @"
                Select * INTO #AcademicSummary
                FROM (
			     Select StudentId
		                , MAX(JscYear) AS JscYear,MAX(JscRoll) AS JscRoll,MAX(JscRegistrationNumber) AS JscRegistrationNumber,MAX(JscBoard) AS JscBoard
		                , MAX(SscYear) AS SscYear,MAX(SscRoll) AS SscRoll,MAX(SscRegistrationNumber) AS SscRegistrationNumber,MAX(SscBoard) AS SscBoard
		                , MAX(HscYear) AS HscYear,MAX(HscRoll) AS HscRoll,MAX(HscRegistrationNumber) AS HscRegistrationNumber,MAX(HscBoard) AS HscBoard
						, MIN(IsRegistrationNumber) as IsRegistrationNumber
						, MIN(IsBoardRoll) as IsBoardRoll
		                from (
							    Select S.StudentId,

						    case when SE.Name = 'JSC' then  S.[Year] else '' end as JscYear,
						    case when SE.Name = 'JSC' then  S.BoradRoll else '' end as JscRoll,
						    case when SE.Name = 'JSC' then  S.RegistrationNumber else '' end as JscRegistrationNumber,
						    case when SE.Name = 'JSC' then  SB.Name else '' end as JscBoard,
						    case when SE.Name = 'SSC' then  S.[Year] else '' end as SscYear,
						    case when SE.Name = 'SSC' then  S.BoradRoll else '' end as SscRoll,
						    case when SE.Name = 'SSC' then  S.RegistrationNumber else '' end as SscRegistrationNumber,
						    case when SE.Name = 'SSC' then  SB.Name else '' end as SscBoard,
						    case when SE.Name = 'HSC' then  S.[Year] else '' end as HscYear,
						    case when SE.Name = 'HSC' then  S.BoradRoll else '' end as HscRoll,
						    case when SE.Name = 'HSC' then  S.RegistrationNumber else '' end as HscRegistrationNumber,
						    case when SE.Name = 'HSC' then  SB.Name else '' end as HscBoard
							, case when S.RegistrationNumber is not null then 1 else 0 end IsRegistrationNumber
							, case when S.BoradRoll is not null then 1 else 0 end IsBoardRoll

						    from [dbo].[StudentAcademicInfo] as S
						    inner join  #StudentSummary AS SS ON SS.StudentId = S.StudentId
						    left join [dbo].[StudentExam] AS SE ON SE.Id = S.StudentExamId
						    left join [dbo].[StudentBoard] AS SB ON SB.Id = S.BoardId
						    Where S.Status = 1 
                            " + studentExamBusinessIdFilterList + @"
                            -- and BoradRoll is not null
					     ) as A GROUP BY StudentId
                ) AS A";

            return query;

        }
        
        #endregion
        
    }
}