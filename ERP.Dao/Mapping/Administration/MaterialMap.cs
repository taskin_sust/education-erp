using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;

namespace UdvashERP.Dao.Mapping.Administration {

    public partial class MaterialMap : BaseClassMap<Material, long>
    {
        public MaterialMap() {
			Table("Material");
			LazyLoad();
            Map(x => x.Rank);
            Map(x => x.Name);
            Map(x => x.ShortName);
           
            References(x => x.MaterialType).Column("MaterialTypeId");
            References(x => x.Program).Column("ProgramId");
            References(x => x.Session).Column("SessionId");

            HasManyToMany(x => x.StudentPrograms).Cascade.All().Table("StudentMaterials");
        }
    }
}
