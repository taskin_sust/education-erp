using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class LeaveApplicationMap : BaseClassMap<LeaveApplication,long> {
        
        public LeaveApplicationMap() {
			Table("HR_LeaveApplication");
			LazyLoad();
			References(x => x.Leave).Column("LeaveId");
			References(x => x.TeamMember).Column("TeamMemberId");
            References(x => x.ResponsiblePerson).Column("ResponsiblePersonId");

            //Map(x => x.Name).Column("Name");
			Map(x => x.DateFrom).Column("DateFrom");
			Map(x => x.DateTo).Column("DateTo");
			Map(x => x.TotalLeaveDay).Column("TotalLeaveDay");
			Map(x => x.LeaveNote).Column("LeaveNote");
			Map(x => x.LeaveStatus).Column("LeaveStatus");
			Map(x => x.Remarks).Column("Remarks");

            HasMany(x => x.LeaveApplicationDetail).KeyColumn("LeaveApplicationId").Cascade.SaveUpdate();
        }
    }
}
