﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdvashERP.FingerPrint
{
    public class ConfigHelper
    {
        public static string GetString(string key)
        {
            return GetString(key, "");
        }

        public static string GetString(string key, string defaultValue)
        {
            if (ConfigurationManager.AppSettings[key] == null)
                return defaultValue;
            else
                return ConfigurationManager.AppSettings[key];
        }
    }
}
