﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;

namespace UdvashERP.Dao.Hr
{
    public interface ILeaveApplicationDetailsDao : IBaseDao<LeaveApplicationDetails, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        LeaveApplicationDetails GetTeamMemberLastIsPostLeaveApplicationDetails(long? teamMemberId, int? pin = null);

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

        IList<LeaveApplicationDetails> LoadByLeaveAppId(long leaveAppId);
    }
    public class LeaveApplicationDetailsDao : BaseDao<LeaveApplicationDetails, long>, ILeaveApplicationDetailsDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public LeaveApplicationDetails GetTeamMemberLastIsPostLeaveApplicationDetails(long? teamMemberId, int? pin = null)
        {
            if ((teamMemberId == null && pin == null) || (teamMemberId == 0 && pin == 0))
                return null;

            ICriteria criteria = Session.CreateCriteria<LeaveApplicationDetails>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", LeaveApplicationDetails.EntityStatus.Delete)));
            criteria.Add(Restrictions.Eq("IsPost", true));
            criteria.CreateAlias("LeaveApplication", "leaveApplication")
                .Add(Restrictions.Not(Restrictions.Eq("leaveApplication.Status", LeaveApplicationDetails.EntityStatus.Delete)))
              //  .Add(Restrictions.Eq("leaveApplication.LeaveStatus",LeaveStatus.Approved))
                ;
            criteria.CreateAlias("leaveApplication.TeamMember", "member");//.Add(Restrictions.Eq("member.Status", TeamMember.EntityStatus.Active))
            if (teamMemberId != null)
                criteria.Add(Restrictions.Eq("member.Id", teamMemberId));
            if (pin != null)
                criteria.Add(Restrictions.Eq("member.Pin", pin));
            criteria.AddOrder(Order.Desc("DateTo"));
            return criteria.List<LeaveApplicationDetails>().Take(1).FirstOrDefault();
        }

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

        public IList<LeaveApplicationDetails> LoadByLeaveAppId(long leaveAppId)
        {
            return
                Session.QueryOver<LeaveApplicationDetails>()
                    .Where(x => x.LeaveApplication.Id == leaveAppId)
                    .List<LeaveApplicationDetails>();
        }
    }
}
