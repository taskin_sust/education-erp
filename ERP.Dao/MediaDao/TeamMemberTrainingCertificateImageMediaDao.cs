﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.Dao.Base;

namespace UdvashERP.Dao.MediaDao
{
    public interface ITeamMemberTrainingCertificateImageMediaDao : IBaseDao<TeamMemberTrainingCertificateMediaImage, long>
    {

        TeamMemberTrainingCertificateMediaImage GetTrainingCertificateImageMediaByRefId(long trainingInfoId, long teamMemberId);
        void DeleteTeamMemberMediaImage(long academicInfoId, long teamMemberId);
    }
    public class TeamMemberTrainingCertificateImageMediaDao : BaseDao<TeamMemberTrainingCertificateMediaImage, long>, ITeamMemberTrainingCertificateImageMediaDao
    {
        public TeamMemberTrainingCertificateMediaImage GetTrainingCertificateImageMediaByRefId(long trainingInfoId, long teamMemberId)
        {
            return
                Session.QueryOver<TeamMemberTrainingCertificateMediaImage>()
                    .Where(x => x.TrainingInfoId == trainingInfoId)
                    .Where(x => x.TeamMemberId == teamMemberId)
                    .Take(1)
                    .SingleOrDefault<TeamMemberTrainingCertificateMediaImage>();
        }
        public void DeleteTeamMemberMediaImage(long trainingInfoId, long teamMemberId)
        {
            var certificateMediaImage = Session.QueryOver<TeamMemberTrainingCertificateMediaImage>()
                                            .Where(x => x.TrainingInfoId == trainingInfoId)
                                            .Where(x => x.TeamMemberId == teamMemberId)
                                            .SingleOrDefault<TeamMemberTrainingCertificateMediaImage>();
            Session.Delete(certificateMediaImage);
        }
    }
}
