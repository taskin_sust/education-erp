﻿using log4net;
using NHibernate;
using UdvashERP.Dao.Administration;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Students
{
    public interface IStudentMaterialService : IBaseService
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion
    }

    public class StudentMaterialService : BaseService, IStudentMaterialService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("StudentService");
        #endregion

        #region Properties and Object Initialization
        private readonly IMaterialDao _materialDao;
        public StudentMaterialService(ISession session)
        {
            Session = session;
            _materialDao = new MaterialDao() { Session = Session };
        }
        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
       
    }
}
