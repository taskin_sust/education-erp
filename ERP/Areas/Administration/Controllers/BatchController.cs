﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class BatchController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IBatchService _batchService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;
        private List<UserMenu> _authorizeMenu;

        public BatchController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _batchService = new BatchService(session);
            _programService = new ProgramService(session);
            _sessionService = new SessionService(session);
            _branchService = new BranchService(session);
            _campusService = new CampusService(session);
            _userService = new UserService(session);
            _organizationService = new OrganizationService(session);
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Index/Manage Page

        [HttpGet]
        public ActionResult Index()
        {
            SelectList organization = new SelectList(new List<Organization>(), "Id", "Name");
            SelectList program = new SelectList(new List<Program>(), "Id", "Name");
            SelectList branch = new SelectList(new List<Branch>(), "Id", "Name");
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
            ViewBag.Organization = organization;
            ViewBag.Program = program;
            ViewBag.Session = null;
            ViewBag.Branch = branch;
            ViewBag.Campus = null;
            ViewBag.StatusText = _commonHelper.GetStatus();
            ViewBag.PageSize = Constants.PageSize;
            return View();
        }

        #region Render Data Table

        [HttpPost]
        public ActionResult Index(int draw, int start, int length, string organizationId, string programId, string sessionName, string branchId, string campusName, string batchName, string batchStatus)
        {
            var data = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var convertedOrganizationId = String.IsNullOrEmpty(organizationId) ? (long?)null : Convert.ToInt32(organizationId);
                    var convertedProgramId = String.IsNullOrEmpty(programId) ? (long?)null : Convert.ToInt32(programId);
                    var convertedBranchId = String.IsNullOrEmpty(branchId) ? (long?)null : Convert.ToInt32(branchId);
                    _authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                    NameValueCollection nvc = Request.Form;
                    var orderBy = "";
                    var orderDir = "";

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {
                            case "0":
                                orderBy = "Organization.Name";
                                break;
                            case "1":
                                orderBy = "Program.Name";
                                break;
                            case "2":
                                orderBy = "Session.Name";
                                break;
                            case "3":
                                orderBy = "Branch.Name";
                                break;
                            case "4":
                                orderBy = "Campus.Name";
                                break;
                            case "5":
                                orderBy = "Name";
                                break;
                            case "6":
                                orderBy = "VersionOfStudy";
                                break;
                            case "7":
                                orderBy = "Gender";
                                break;
                            case "8":
                                orderBy = "Rank";
                                break;
                            case "9":
                                orderBy = "Status";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }

                    #endregion

                    var batchFake = new Batch();
                    recordsTotal = _batchService.GetBatchCount(_authorizeMenu, orderBy, orderDir.ToUpper(), convertedOrganizationId, convertedProgramId, sessionName, convertedBranchId, campusName, batchName, batchStatus);
                    recordsFiltered = recordsTotal;
                    var batchList = _batchService.LoadBatch(_authorizeMenu, start, length, orderBy, orderDir.ToUpper(), convertedOrganizationId, convertedProgramId, sessionName, convertedBranchId, campusName, batchName, batchStatus).ToList();
                    var maxMinRank = new int[2];
                    maxMinRank[0] = _batchService.GetMinimumRank(batchFake);
                    maxMinRank[1] = _batchService.GetMaximumRank(batchFake);

                    foreach (var batch in batchList)
                    {
                        var str = new List<string>();
                        str.Add(batch.Program.Organization.ShortName);
                        str.Add(batch.Program.Name);
                        str.Add(batch.Session.Name);
                        str.Add(batch.Branch.Name);
                        str.Add(batch.Campus.Name);
                        str.Add(batch.Name);
                        str.Add(VertionText.GetVersionText(batch.VersionOfStudy));
                        str.Add(GenderTypeText.GetGenderText(batch.Gender));
                        str.Add(LinkGenerator.GetRankLinkForModal(batch.Id, batch.Rank, maxMinRank[0], maxMinRank[1]));
                        str.Add(StatusTypeText.GetStatusText(batch.Status));
                        str.Add(LinkGenerator.GetGeneratedDetailsEditModelDeleteLink("Details", "Edit", "Batch", batch.Id, batch.Name));
                        data.Add(str);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                }
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        #endregion

        #endregion

        #region Operational Function

        #region Save Operation
        
        [HttpGet]
        public ActionResult Create(long organizationId = 0, long programId = 0, long sessionId = 0, long branchId = 0, long campusId = 0, string concatDays = "", int versionOfStudyId = 0, int genderId = 0)
        {
            try
            {
                if (TempData["BatchAdd"] != null)
                {
                    ViewBag.SuccessMessage = TempData["BatchAdd"];   
                }
                InitializeViewBag(organizationId, programId, sessionId, branchId, campusId, concatDays, versionOfStudyId, genderId);
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Batch batch, long OrganizationId, long ProgramId, long SessionId, long BranchId, long CampusId, string[] groups, int VersionOfStudy, int Gender)
        {
            var concatDays = "";
            try
            {
                concatDays = String.Join(", ", groups);
                batch.Organization = _organizationService.LoadById(OrganizationId);
                batch.Program = _programService.GetProgram(ProgramId);
                batch.Session = _sessionService.LoadById(SessionId);
                batch.Branch = _branchService.GetBranch(BranchId);
                batch.Campus = _campusService.GetCampus(CampusId);
                batch.Days = concatDays;
                batch.Gender = Gender;
                batch.VersionOfStudy = VersionOfStudy;
                var isSussecc = false;
                var errorMessage = "Batch Add Failed";
                if (Convert.ToDateTime(batch.StartTime) >= Convert.ToDateTime(batch.EndTime))
                    errorMessage = "Batch start time grater then or equal to end time!";
                else
                    isSussecc = _batchService.Save(batch);
                if (isSussecc)
                {
                    TempData["BatchAdd"] = "Batch Added Successfull";
                    return RedirectToAction("Create", new
                    {
                        organizationId = OrganizationId,
                        programId = ProgramId,
                        sessionId = SessionId,
                        branchId = BranchId,
                        campusId = CampusId,
                        concatDays = concatDays,
                        versionOfStudyId = VersionOfStudy,
                        genderId = Gender
                    });
                }
                ViewBag.ErrorMessage = errorMessage;
            }
            catch (ValueNotAssignedException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                
            }

            finally
            {
                InitializeViewBag(OrganizationId, ProgramId, SessionId, BranchId, CampusId, concatDays, VersionOfStudy, Gender);
            }
            return View(batch);
        }

        #endregion

        #region Update Operation

        [HttpGet]
        public ActionResult Edit(long id, bool isSucc = false, string message = "")
        {
            var batch = new Batch();
            try
            {
                if (id < 1)
                    return HttpNotFound();
                _authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
                batch = _batchService.GetBatchAuthorize(id, _authorizeMenu);
                if (batch == null)
                    return RedirectToAction("PermissionDenied", "Home", new { area = "" });
                InitializeViewBag(batch.Program.Organization.Id, batch.Program.Id, batch.Session.Id, batch.Branch.Id, batch.Campus.Id, batch.Days, batch.VersionOfStudy, batch.Gender);
                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key", batch.Status);
                if (isSucc && !String.IsNullOrEmpty(message)) ViewBag.SuccessMessage = message;
                else if (!isSucc && !String.IsNullOrEmpty(message)) ViewBag.ErrorMessage = message;
                return View(batch);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
                return View(batch);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(long id, Batch batch, long OrganizationId, long ProgramId, long SessionId, long BranchId, long CampusId, string[] groups, int VersionOfStudy, int Gender)
        {
            var concatDays = "";
            try
            {
                concatDays = String.Join(", ", groups);
                batch.Organization = _organizationService.LoadById(OrganizationId);
                batch.Program = _programService.GetProgram(ProgramId);
                batch.Session = _sessionService.LoadById(SessionId);
                batch.Branch = _branchService.GetBranch(BranchId);
                batch.Campus = _campusService.GetCampus(CampusId);
                batch.Days = concatDays;
                batch.Gender = Gender;
                batch.VersionOfStudy = VersionOfStudy;
                var isSussecc = false;
                var errorMessage = "Batch Add Failed";
                if (batch.StartTime >= batch.EndTime)
                    errorMessage = "Batch start time grater then  or equal to end time!";
                else
                    isSussecc = _batchService.Update(id, batch);
                if (isSussecc)
                    return RedirectToAction("Edit", new {id = id, isSucc = true, message = "Batch updated Successfully"});
                return RedirectToAction("Edit", new {id = id, isSucc = false, message = errorMessage});
            }
            catch (ValueNotAssignedException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            finally
            {
                InitializeViewBag(OrganizationId, ProgramId, SessionId, BranchId, CampusId, concatDays, VersionOfStudy, Gender);
                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");

            }
            return View(batch);
        }

        #endregion

        #region Delete Operation

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                Batch batch = _batchService.GetBatch(id);
                bool isSuccess = _batchService.Delete(id, batch);

                if (isSuccess)
                {
                    ViewBag.SuccessMessage = "Deleted Successfully";
                    return Json(new Response(true, "Batch sucessfully deleted."));
                }
                else
                {
                    return Json(new Response(false, "Batch delete failed"));
                }
            }
            catch (ValueNotAssignedException ex)
            {
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Details Operation

        public ActionResult Details(long id)
        {
            try
            {
                Batch batch = _batchService.GetBatch(id);
                if (batch == null)
                    return HttpNotFound();
                else
                {
                    batch.CreateByText = _userService.GetUserNameByAspNetUserId(batch.CreateBy);
                    batch.ModifyByText = _userService.GetUserNameByAspNetUserId(batch.ModifyBy);
                }

                return View(batch);
            }
            catch (DataNotFoundException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.Message = WebHelper.SetExceptionMessage(ex);
                return View();
            }
        }

        #endregion

        #region Rank Operation

        public ActionResult RankChange(int id, int current, string action)
        {
            try
            {
                Batch batchObj = _batchService.GetBatch(Convert.ToInt64(id));
                if (batchObj == null)
                {
                    return HttpNotFound();
                }
                int newRank;
                if (action == "up")
                    newRank = batchObj.Rank - 1;
                else
                    newRank = batchObj.Rank + 1;

                var batchOldObj = _batchService.GetBatchByRankNextOrPrevious(newRank, action); //LoadByRank(newRank);
                newRank = batchOldObj.Rank;

                batchOldObj.Rank = batchObj.Rank;
                batchObj.Rank = newRank;

                bool isSuccessfullyUpdateRank = _batchService.UpdateRank(batchOldObj, batchObj);
                if (isSuccessfullyUpdateRank)
                {
                    return Json("Successfully changed");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(WebHelper.CommonErrorMessage);
        }

        #endregion

        #endregion

        #region Helper Function

        private void InitializeViewBag(long organizationId = 0, long programId = 0, long sessionId = 0, long branchId = 0, long campusId = 0, string concatDays = "", int versionOfStudyId = 0, int genderId = 0, Batch batch = null)
        {
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.Organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName", organizationId);
                ViewBag.ProgramId = organizationId == 0 ? new SelectList(new List<Program>(), "Id", "Name") : new SelectList(_programService.LoadAuthorizedProgram(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(sessionId)), "Id", "Name", programId);
                ViewBag.SessionId = programId == 0 ? new SelectList(new List<Session>(), "Id", "Name") : new SelectList(_sessionService.LoadAuthorizedSession(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId)), "Id", "Name", sessionId);
                if (programId == 0 || sessionId == 0)
                    ViewBag.BranchId = new SelectList(new List<Branch>(), "Id", "Name");
                else
                    ViewBag.BranchId = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId)), "Id", "Name", branchId);
                if (branchId == 0)
                    ViewBag.CampusId = new SelectList(new List<Campus>(), "Id", "Name");
                else
                {
                    var branchIds = new List<long> { branchId };
                    ViewBag.CampusId = new SelectList(_campusService.LoadAuthorizeCampus(userMenu, _commonHelper.ConvertIdToList(organizationId), _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(branchId), _commonHelper.ConvertIdToList(sessionId)), "Id", "Name", campusId);
                }
                var batchOptions = new List<BatchOption>
                {
                    new BatchOption {Id = "Sat", Type = "Sat"},
                    new BatchOption {Id = "Sun", Type = "Sun"},
                    new BatchOption {Id = "Mon", Type = "Mon"},
                    new BatchOption {Id = "Tue", Type = "Tue"},
                    new BatchOption {Id = "Wed", Type = "Wed"},
                    new BatchOption {Id = "Thu", Type = "Thu"},
                    new BatchOption {Id = "Fri", Type = "Fri"}
                };
                if (concatDays != null && concatDays.Trim().Length > 1)
                    ViewBag.WeekDays = new MultiSelectList(batchOptions, "Id", "Type", from x in batchOptions where concatDays.Contains(x.Id) select x.Id);
                else
                    ViewBag.WeekDays = new MultiSelectList(batchOptions, "Id", "Type");
                if (batch != null)
                {
                    var versionofStudy = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(), "Key", "Value", batch.VersionOfStudy);
                    var versionofStudyList = versionofStudy.ToList();
                    if (batch.VersionOfStudy <= 0 || batch.VersionOfStudy > Convert.ToInt32(VersionOfStudy.Combined))
                        versionofStudyList.Insert(0, new SelectListItem() { Text = "Unknown", Value = "0" });
                    var gender = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(), "Key", "Value", batch.Gender);
                    var genderList = gender.ToList();
                    if (batch.Gender <= 0 || batch.Gender > Convert.ToInt32(Gender.Combined))
                        genderList.Insert(0, new SelectListItem() { Text = "Unknown", Value = "0" });
                    ViewBag.VersionOfStudyList = versionofStudyList;
                    ViewBag.GenderList = genderList;
                }
                else
                {
                    ViewBag.VersionOfStudy = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(), "Key", "Value", versionOfStudyId);
                    ViewBag.Gender = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(), "Key", "Value", genderId);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
                ViewBag.SessionId = new SelectList(new List<Session>(), "Id", "Name");
                ViewBag.BranchId = new SelectList(new List<Branch>(), "Id", "Name");
                ViewBag.CampusId = new SelectList(new List<Campus>(), "Id", "Name");
                ViewBag.VersionOfStudy = new SelectList(_commonHelper.LoadEmumToDictionary<VersionOfStudy>(), "Key", "Value");
                ViewBag.Gender = new SelectList(_commonHelper.LoadEmumToDictionary<Gender>(), "Key", "Value");
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
        }

        #endregion
    }
}