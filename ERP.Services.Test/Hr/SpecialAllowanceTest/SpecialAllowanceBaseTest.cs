﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.SpecialAllowanceTest
{
   public class SpecialAllowanceBaseTest : TestBase, IDisposable
   {
       #region Initialization

       protected readonly int MentorId = 2;
       protected readonly ISession _session;
       private ICommonHelper _commonHelper;
       private OrganizationFactory _organizationObjFactory;
       private BranchFactory _branchObjFactory;
       public SpecialAllowanceSettingFactory _specialAllowanceSettingFactory;
       private TeamMemberFactory _teamMemberObjFactory;
       public CampusRoomFactory _campusRoomObjFactory;
       public CampusFactory _campusObjFactory;
       public DepartmentFactory _departmentObjFactory;
       public DesignationFactory _designationObjFactory;
       public ISpecialAllowanceSettingService _specialAllowanceSettingService;
       public SpecialAllowanceBaseTest()
       {
           _session = NHibernateSessionFactory.OpenSession();
           _commonHelper = new CommonHelper();
           _branchObjFactory = new BranchFactory(null, _session);
           _organizationObjFactory = new OrganizationFactory(null, _session);
           _teamMemberObjFactory = new TeamMemberFactory(null, _session);
           _campusRoomObjFactory = new CampusRoomFactory(null, _session);
           _campusObjFactory = new CampusFactory(null, _session);
           _departmentObjFactory = new DepartmentFactory(null, _session);
           _designationObjFactory = new DesignationFactory(null, _session);
           _specialAllowanceSettingFactory = new SpecialAllowanceSettingFactory(null, _session);
           _specialAllowanceSettingService = new SpecialAllowanceSettingService(_session);
       }

       #endregion

       #region Disposal

       public void Dispose()
       {
           _specialAllowanceSettingFactory.CleanUp();
           _campusRoomObjFactory.CleanUp();
           _campusObjFactory.Cleanup();
           _departmentObjFactory.Cleanup();
           _designationObjFactory.Cleanup();
           _teamMemberObjFactory.Cleanup();
           _branchObjFactory.Cleanup();
           _organizationObjFactory.Cleanup();
           base.Dispose();
       }

       #endregion
    }
}
