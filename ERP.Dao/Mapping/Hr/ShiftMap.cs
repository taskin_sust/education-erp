using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Mapping.Hr 
{
    public partial class ShiftMap : BaseClassMap<Shift,long> {
        
        public ShiftMap() {
			Table("HR_Shift");
			LazyLoad();
			References(x => x.Organization).Column("OrganizationId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.StartTime).Column("StartTime");
			Map(x => x.EndTime).Column("EndTime");
			HasMany(x => x.ShiftWeekendHistory).KeyColumn("ShiftId");
			HasMany(x => x.ShiftWeekendHistoryLog).KeyColumn("ShiftId");
        }
    }
}
