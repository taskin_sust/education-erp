﻿namespace UdvashERP.FingerPrint.FormUI
{
    partial class ScanFinger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpScanFinger = new DevExpress.XtraEditors.GroupControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lueDeviceType = new DevExpress.XtraEditors.LookUpEdit();
            this.deviceTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lblOrganization = new System.Windows.Forms.Label();
            this.txtUserID = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.lblDesignation = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnRetriveOnlineData = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.lblCampus = new System.Windows.Forms.Label();
            this.lblBranch = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnD7 = new System.Windows.Forms.Button();
            this.btnD9 = new System.Windows.Forms.Button();
            this.btnD5 = new System.Windows.Forms.Button();
            this.btnD6 = new System.Windows.Forms.Button();
            this.btnD8 = new System.Windows.Forms.Button();
            this.btnD4 = new System.Windows.Forms.Button();
            this.btnD3 = new System.Windows.Forms.Button();
            this.btnD0 = new System.Windows.Forms.Button();
            this.btnD2 = new System.Windows.Forms.Button();
            this.btnD1 = new System.Windows.Forms.Button();
            this.picRight = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnSaveToOnline = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grpScanFinger)).BeginInit();
            this.grpScanFinger.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDeviceType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserID.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // grpScanFinger
            // 
            this.grpScanFinger.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.grpScanFinger.Controls.Add(this.groupBox3);
            this.grpScanFinger.Controls.Add(this.groupBox2);
            this.grpScanFinger.Controls.Add(this.btnSaveToOnline);
            this.grpScanFinger.Location = new System.Drawing.Point(12, 12);
            this.grpScanFinger.Name = "grpScanFinger";
            this.grpScanFinger.Size = new System.Drawing.Size(880, 621);
            this.grpScanFinger.TabIndex = 0;
            this.grpScanFinger.Text = "Team Member Finger Template Scanning";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Controls.Add(this.lueDeviceType);
            this.groupBox3.Controls.Add(this.lblOrganization);
            this.groupBox3.Controls.Add(this.txtUserID);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.lblDesignation);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.lblName);
            this.groupBox3.Controls.Add(this.lblDepartment);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.btnRetriveOnlineData);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.lblCampus);
            this.groupBox3.Controls.Add(this.lblBranch);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(122, 24);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(619, 180);
            this.groupBox3.TabIndex = 56;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Team Member Information";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(201, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(109, 110);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            // 
            // lueDeviceType
            // 
            this.lueDeviceType.Location = new System.Drawing.Point(201, 145);
            this.lueDeviceType.Name = "lueDeviceType";
            this.lueDeviceType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDeviceType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ModelName", "Model Name", 81, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueDeviceType.Properties.DataSource = this.deviceTypeBindingSource;
            this.lueDeviceType.Properties.DisplayMember = "ModelName";
            this.lueDeviceType.Properties.NullText = "Select a Device Type";
            this.lueDeviceType.Properties.ValueMember = "ModelId";
            this.lueDeviceType.Size = new System.Drawing.Size(203, 20);
            this.lueDeviceType.TabIndex = 55;
            this.lueDeviceType.EditValueChanged += new System.EventHandler(this.lueDeviceType_EditValueChanged);
            // 
            // deviceTypeBindingSource
            // 
            this.deviceTypeBindingSource.DataSource = typeof(UdvashERP.FingerPrint.BusinessModel.DeviceType);
            // 
            // lblOrganization
            // 
            this.lblOrganization.AutoSize = true;
            this.lblOrganization.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrganization.Location = new System.Drawing.Point(410, 115);
            this.lblOrganization.Name = "lblOrganization";
            this.lblOrganization.Size = new System.Drawing.Size(12, 14);
            this.lblOrganization.TabIndex = 45;
            this.lblOrganization.Text = "-";
            // 
            // txtUserID
            // 
            this.txtUserID.EditValue = "";
            this.txtUserID.Location = new System.Drawing.Point(69, 55);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Properties.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserID.Properties.Appearance.Options.UseFont = true;
            this.txtUserID.Properties.DisplayFormat.FormatString = "0000";
            this.txtUserID.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtUserID.Properties.EditFormat.FormatString = "0000";
            this.txtUserID.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtUserID.Properties.Mask.EditMask = "0000";
            this.txtUserID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtUserID.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtUserID.Size = new System.Drawing.Size(126, 22);
            this.txtUserID.TabIndex = 44;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(350, 96);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 14);
            this.label10.TabIndex = 46;
            this.label10.Text = "Branch :";
            // 
            // lblDesignation
            // 
            this.lblDesignation.AutoSize = true;
            this.lblDesignation.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesignation.Location = new System.Drawing.Point(410, 58);
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Size = new System.Drawing.Size(12, 14);
            this.lblDesignation.TabIndex = 53;
            this.lblDesignation.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(312, 115);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 14);
            this.label9.TabIndex = 44;
            this.label9.Text = "Organization :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(342, 77);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 14);
            this.label11.TabIndex = 47;
            this.label11.Text = "Campus :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(318, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 14);
            this.label8.TabIndex = 52;
            this.label8.Text = "Designation :";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(410, 20);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(12, 14);
            this.lblName.TabIndex = 43;
            this.lblName.Text = "-";
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartment.Location = new System.Drawing.Point(410, 39);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(12, 14);
            this.lblDepartment.TabIndex = 51;
            this.lblDepartment.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(357, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 14);
            this.label6.TabIndex = 42;
            this.label6.Text = "Name :";
            // 
            // btnRetriveOnlineData
            // 
            this.btnRetriveOnlineData.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetriveOnlineData.Location = new System.Drawing.Point(69, 83);
            this.btnRetriveOnlineData.Name = "btnRetriveOnlineData";
            this.btnRetriveOnlineData.Size = new System.Drawing.Size(126, 23);
            this.btnRetriveOnlineData.TabIndex = 39;
            this.btnRetriveOnlineData.Text = "Search";
            this.btnRetriveOnlineData.UseVisualStyleBackColor = true;
            this.btnRetriveOnlineData.Click += new System.EventHandler(this.btnRetriveOnlineData_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(318, 39);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 14);
            this.label12.TabIndex = 48;
            this.label12.Text = "Department :";
            // 
            // lblCampus
            // 
            this.lblCampus.AutoSize = true;
            this.lblCampus.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCampus.Location = new System.Drawing.Point(410, 77);
            this.lblCampus.Name = "lblCampus";
            this.lblCampus.Size = new System.Drawing.Size(12, 14);
            this.lblCampus.TabIndex = 50;
            this.lblCampus.Text = "-";
            // 
            // lblBranch
            // 
            this.lblBranch.AutoSize = true;
            this.lblBranch.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBranch.Location = new System.Drawing.Point(410, 96);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(12, 14);
            this.lblBranch.TabIndex = 49;
            this.lblBranch.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(124, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 16);
            this.label5.TabIndex = 38;
            this.label5.Text = "PIN";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnD7);
            this.groupBox2.Controls.Add(this.btnD9);
            this.groupBox2.Controls.Add(this.btnD5);
            this.groupBox2.Controls.Add(this.btnD6);
            this.groupBox2.Controls.Add(this.btnD8);
            this.groupBox2.Controls.Add(this.btnD4);
            this.groupBox2.Controls.Add(this.btnD3);
            this.groupBox2.Controls.Add(this.btnD0);
            this.groupBox2.Controls.Add(this.btnD2);
            this.groupBox2.Controls.Add(this.btnD1);
            this.groupBox2.Controls.Add(this.picRight);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Location = new System.Drawing.Point(122, 210);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(619, 338);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Scan Finger";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(134, 296);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "LEFT HAND";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(410, 296);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "RIGHT HAND";
            // 
            // btnD7
            // 
            this.btnD7.BackgroundImage = global::UdvashERP.FingerPrint.Properties.Resources._8D;
            this.btnD7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnD7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnD7.Location = new System.Drawing.Point(458, 22);
            this.btnD7.Name = "btnD7";
            this.btnD7.Size = new System.Drawing.Size(40, 35);
            this.btnD7.TabIndex = 2;
            this.btnD7.UseVisualStyleBackColor = true;
            this.btnD7.Click += new System.EventHandler(this.btnD7_Click);
            // 
            // btnD9
            // 
            this.btnD9.BackgroundImage = global::UdvashERP.FingerPrint.Properties.Resources._10D;
            this.btnD9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnD9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnD9.Location = new System.Drawing.Point(546, 100);
            this.btnD9.Name = "btnD9";
            this.btnD9.Size = new System.Drawing.Size(40, 35);
            this.btnD9.TabIndex = 4;
            this.btnD9.UseVisualStyleBackColor = true;
            this.btnD9.Click += new System.EventHandler(this.btnD9_Click);
            // 
            // btnD5
            // 
            this.btnD5.BackgroundImage = global::UdvashERP.FingerPrint.Properties.Resources._6D;
            this.btnD5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnD5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnD5.Location = new System.Drawing.Point(312, 143);
            this.btnD5.Name = "btnD5";
            this.btnD5.Size = new System.Drawing.Size(40, 35);
            this.btnD5.TabIndex = 0;
            this.btnD5.UseVisualStyleBackColor = true;
            this.btnD5.Click += new System.EventHandler(this.btnD5_Click);
            // 
            // btnD6
            // 
            this.btnD6.BackgroundImage = global::UdvashERP.FingerPrint.Properties.Resources._7D;
            this.btnD6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnD6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnD6.Location = new System.Drawing.Point(406, 33);
            this.btnD6.Name = "btnD6";
            this.btnD6.Size = new System.Drawing.Size(40, 35);
            this.btnD6.TabIndex = 1;
            this.btnD6.UseVisualStyleBackColor = true;
            this.btnD6.Click += new System.EventHandler(this.btnD6_Click);
            // 
            // btnD8
            // 
            this.btnD8.BackgroundImage = global::UdvashERP.FingerPrint.Properties.Resources._9D;
            this.btnD8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnD8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnD8.Location = new System.Drawing.Point(508, 50);
            this.btnD8.Name = "btnD8";
            this.btnD8.Size = new System.Drawing.Size(40, 35);
            this.btnD8.TabIndex = 3;
            this.btnD8.UseVisualStyleBackColor = true;
            this.btnD8.Click += new System.EventHandler(this.btnD8_Click);
            // 
            // btnD4
            // 
            this.btnD4.BackgroundImage = global::UdvashERP.FingerPrint.Properties.Resources._5D;
            this.btnD4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnD4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnD4.Location = new System.Drawing.Point(270, 143);
            this.btnD4.Name = "btnD4";
            this.btnD4.Size = new System.Drawing.Size(40, 35);
            this.btnD4.TabIndex = 0;
            this.btnD4.UseVisualStyleBackColor = true;
            this.btnD4.Click += new System.EventHandler(this.btnD4_Click);
            // 
            // btnD3
            // 
            this.btnD3.BackgroundImage = global::UdvashERP.FingerPrint.Properties.Resources._3D;
            this.btnD3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnD3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnD3.Location = new System.Drawing.Point(173, 32);
            this.btnD3.Name = "btnD3";
            this.btnD3.Size = new System.Drawing.Size(40, 35);
            this.btnD3.TabIndex = 1;
            this.btnD3.UseVisualStyleBackColor = true;
            this.btnD3.Click += new System.EventHandler(this.btnD3_Click);
            // 
            // btnD0
            // 
            this.btnD0.BackgroundImage = global::UdvashERP.FingerPrint.Properties.Resources._1D;
            this.btnD0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnD0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnD0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnD0.Location = new System.Drawing.Point(36, 103);
            this.btnD0.Name = "btnD0";
            this.btnD0.Size = new System.Drawing.Size(40, 35);
            this.btnD0.TabIndex = 4;
            this.btnD0.UseVisualStyleBackColor = true;
            this.btnD0.Click += new System.EventHandler(this.btnD0_Click);
            // 
            // btnD2
            // 
            this.btnD2.BackgroundImage = global::UdvashERP.FingerPrint.Properties.Resources._3D;
            this.btnD2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnD2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnD2.Location = new System.Drawing.Point(127, 27);
            this.btnD2.Name = "btnD2";
            this.btnD2.Size = new System.Drawing.Size(40, 35);
            this.btnD2.TabIndex = 2;
            this.btnD2.UseVisualStyleBackColor = true;
            this.btnD2.Click += new System.EventHandler(this.btnD2_Click);
            // 
            // btnD1
            // 
            this.btnD1.BackgroundImage = global::UdvashERP.FingerPrint.Properties.Resources._2D;
            this.btnD1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnD1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnD1.Location = new System.Drawing.Point(71, 51);
            this.btnD1.Name = "btnD1";
            this.btnD1.Size = new System.Drawing.Size(40, 35);
            this.btnD1.TabIndex = 3;
            this.btnD1.UseVisualStyleBackColor = true;
            this.btnD1.Click += new System.EventHandler(this.btnD1_Click);
            // 
            // picRight
            // 
            this.picRight.Image = global::UdvashERP.FingerPrint.Properties.Resources.RightHand;
            this.picRight.Location = new System.Drawing.Point(312, 20);
            this.picRight.Name = "picRight";
            this.picRight.Size = new System.Drawing.Size(274, 300);
            this.picRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picRight.TabIndex = 34;
            this.picRight.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::UdvashERP.FingerPrint.Properties.Resources.LeftHand;
            this.pictureBox3.Location = new System.Drawing.Point(36, 20);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(274, 300);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 35;
            this.pictureBox3.TabStop = false;
            // 
            // btnSaveToOnline
            // 
            this.btnSaveToOnline.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveToOnline.Location = new System.Drawing.Point(642, 563);
            this.btnSaveToOnline.Name = "btnSaveToOnline";
            this.btnSaveToOnline.Size = new System.Drawing.Size(75, 40);
            this.btnSaveToOnline.TabIndex = 36;
            this.btnSaveToOnline.Text = "Save";
            this.btnSaveToOnline.UseVisualStyleBackColor = true;
            this.btnSaveToOnline.Click += new System.EventHandler(this.btnSaveToOnline_Click);
            // 
            // ScanFinger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 646);
            this.Controls.Add(this.grpScanFinger);
            this.Name = "ScanFinger";
            this.Text = "ScanFinger";
            this.Load += new System.EventHandler(this.ScanFinger_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grpScanFinger)).EndInit();
            this.grpScanFinger.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDeviceType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserID.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl grpScanFinger;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnD5;
        private System.Windows.Forms.Button btnD8;
        private System.Windows.Forms.Button btnD7;
        private System.Windows.Forms.Button btnD6;
        private System.Windows.Forms.Button btnD0;
        private System.Windows.Forms.Button btnD3;
        private System.Windows.Forms.Button btnD2;
        private System.Windows.Forms.Button btnD1;
        private System.Windows.Forms.Button btnD4;
        private System.Windows.Forms.Button btnD9;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox picRight;
        private System.Windows.Forms.Label lblDesignation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblDepartment;
        private System.Windows.Forms.Label lblCampus;
        private System.Windows.Forms.Label lblBranch;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnSaveToOnline;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblOrganization;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnRetriveOnlineData;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.BindingSource deviceTypeBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueDeviceType;
        private DevExpress.XtraEditors.TextEdit txtUserID;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}