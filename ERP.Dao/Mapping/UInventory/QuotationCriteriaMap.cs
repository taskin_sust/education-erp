using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Mapping.Base;

namespace UdvashERP.Dao.Mapping.UInventory
{
    public class QuotationCriteriaMap : BaseClassMap<QuotationCriteria, long>
    {

        public QuotationCriteriaMap()
        {
            Table("UINV_QuotationCriteria");
            LazyLoad();
            References(x => x.Quotation).Column("QuotationId");
            Map(x => x.Criteria).Column("Criteria").Not.Nullable();
            Map(x => x.CriteriaValue).Column("CriteriaValue").Not.Nullable();
        }
    }
}
