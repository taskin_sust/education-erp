﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using FluentNHibernate.Utils;
using log4net;
using Newtonsoft.Json;
using UdvashERP.App_code;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.App_Start;
using UdvashERP.Areas.Student.Controllers;
using UdvashERP.BusinessModel.Dto.Administration;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.BusinessModel.ViewModel.Administration;
using UdvashERP.com.onnorokomsms.api2;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Sms;
using UdvashERP.Services.UserAuth;
using SmsSender = UdvashERP.BusinessModel.ViewModel.Administration.SmsSender;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class SmsRegistrationController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IOrganizationService _organizationService;
        private readonly ISmsService _smsService;
        private readonly ISmsStudentRegistrationSettingService _srsService;
        private readonly ISmsRegistrationService _smsrs;
        private readonly ISmsRegistrationReceiverLogService _srlService;
        private readonly ICommonHelper _commonHelper;
        private List<UserMenu> _authorizeMenu;

        public SmsRegistrationController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _organizationService = new OrganizationService(session);
            _smsService = new SmsService(session);
            _srsService = new SmsStudentRegistrationSettingService(session);
            _smsrs = new SmsRegistrationService(session);
            _srlService = new SmsRegistrationReceiverLogService(session);
            _commonHelper = new CommonHelper();
        }

        #endregion

        #region Operational Function

        #region Save Operation

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SmsStudentRegistrationBulk(string data, string key = "")
        {
            _logger.Info("Data Received by sms student registration bulk at" + DateTime.Now + ": " + data);
            IList<string> messageList = new List<string>();
            try
            {
                //Match key
                //var smsApiPassKey = ConfigurationManager.AppSettings["SmsRegistrationKey"];
                //if (smsApiPassKey != key)
                //  throw new InvalidDataException("Invalid SMS Api key.");
                //parse data as json 
                if (string.IsNullOrEmpty(data)) throw new DataNotFoundException("No sms found.");
                var serializer = new JavaScriptSerializer();
                //deserialize data 
                var smsSenderListJson = serializer.Deserialize<RootObject>(data);
                if (smsSenderListJson == null || !smsSenderListJson.SmsList.Any())
                    throw new DataNotFoundException("No sms found.");
                //var request = System.Web.Helpers.Json.Decode<SmsRegistrationRequestList>(data);
                // var senderListJson = smsSenderListJson.SmsList as List<SmsSender>;
                foreach (SmsList smsSenderJson in smsSenderListJson.SmsList)
                {
                    long id = Convert.ToInt64(smsSenderJson.Id);
                    string senderMobile = smsSenderJson.Sender ?? "";
                    string time = smsSenderJson.Time;
                    string text = smsSenderJson.Text;
                    messageList.Add(SmsStudentRegistration(id, senderMobile, time, text, key));
                }
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message), JsonRequestBehavior.AllowGet);
            }
            catch (DataNotFoundException ex)
            {
                return Json(new Response(false, ex.Message), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message), JsonRequestBehavior.AllowGet);
            }
            return Json(string.Join("; ", messageList), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult SmsStudentRegistrationRanksTell(string sender, string cleanText, string key = "")
        {
            _logger.Info("Data Received from RanksTell. Student registration bulk at" + DateTime.Now + ": " + sender + "->" + cleanText);
            IList<string> messageList = new List<string>();
            try
            {
                if (string.IsNullOrEmpty(cleanText)) throw new DataNotFoundException("No sms found.");
                messageList.Add(SmsStudentRegistration(0, sender, DateTime.Now.ToString(), cleanText, key));
            }
            catch (InvalidDataException ex)
            {
                return Json(new Response(false, ex.Message), JsonRequestBehavior.AllowGet);
            }
            catch (DataNotFoundException ex)
            {
                return Json(new Response(false, ex.Message), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message), JsonRequestBehavior.AllowGet);
            }
            return Json(string.Join("; ", messageList), JsonRequestBehavior.AllowGet);
        }

        public string SmsStudentRegistration(long senderId, string mobileNumber, string senderTime, string message, string key)
        {
            string errorMessage = @" Sorry, Registration failed due to Wrong SMS Format. Please Resend SMS in correct format. UDVASH-UNMESH shikkha poribar";
            string errorExceptionMessage = @" Sorry, Registration failed. Server busy. Please try again later. UDVASH-UNMESH shikkha poribar";
            string udvShortName = "UDVASH";
            Organization udv = new Organization();
            string defaultMaskName = "";
            SmsRegistrationReceiverLog smsRegisterRecieverLog = new SmsRegistrationReceiverLog();
            SmsRegistrationDto srd = new SmsRegistrationDto();
            try
            {
                udv = _organizationService.GetOrganization(udvShortName);
                if (string.IsNullOrEmpty(mobileNumber))
                {
                    throw new DataNotFoundException("Sender mobile number not found.");
                }
                if (string.IsNullOrEmpty(message))
                {
                    throw new InvalidDataException("Message can not be empty.");
                }
                smsRegisterRecieverLog.SenderMobileNumber = mobileNumber.Trim();
                smsRegisterRecieverLog.Message = message;
                smsRegisterRecieverLog.SmsKey = key;
                smsRegisterRecieverLog.SmsSenderId = senderId;
                DateTime senderDate;
                DateTime.TryParse(senderTime, out senderDate);
                smsRegisterRecieverLog.SmsSenderTime = senderDate;
                srd = _smsrs.RegisterStudentBySms(smsRegisterRecieverLog, srd);
                //_smsrs.RegisterStudentBySms(smsRegisterRecieverLog, srd);
                var organization = srd.Program.Organization;
                SendSms(organization.HighPrioritySmsApiUserName, organization.HighPrioritySmsApiPassword, srd.Message, "", "TEXT", "", "", srd, smsRegisterRecieverLog, (int)SmsRegistrationStatus.Success);
                return srd.Message;
            }
            catch (DataNotFoundException ex)
            {
                return ex.Message;
            }
            catch (InvalidDataException ex)
            {
                srd.Message = ex.Message;
                SendSms(udv.HighPrioritySmsApiUserName, udv.HighPrioritySmsApiPassword, errorMessage/*ex.Message*/, mobileNumber, "TEXT", defaultMaskName, "", srd, smsRegisterRecieverLog, (int)SmsRegistrationStatus.Error);
                return ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                srd.Message = ex.Message;
                SendSms(udv.HighPrioritySmsApiUserName, udv.HighPrioritySmsApiPassword, ex.Message, mobileNumber, "TEXT", defaultMaskName, "", srd, smsRegisterRecieverLog, (int)SmsRegistrationStatus.Error);
                return ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                SendSms(udv.HighPrioritySmsApiUserName, udv.HighPrioritySmsApiPassword, errorExceptionMessage/*WebHelper.CommonErrorMessage + " Please try again."*/, mobileNumber, "TEXT", defaultMaskName, "", srd, smsRegisterRecieverLog, (int)SmsRegistrationStatus.Error);
                return ex.Message;
            }
        }

        public void SendSms(string userName, string userPassword, string messageText, string receiverNumbers, string smsType, string maskName, string campaignName, SmsRegistrationDto srd, SmsRegistrationReceiverLog srLog, int registrationStatus)
        {
            try
            {
                var udvash = _organizationService.GetOrganization("UDVASH");
                var smsUserId = Convert.ToInt64(ConfigurationManager.AppSettings["SmsUserId"]);
                var smsToBeSend = srd.SmsToBeSend;
                if (string.IsNullOrEmpty(receiverNumbers))
                {
                    foreach (SmsViewModel rN in smsToBeSend)
                    {
                        receiverNumbers += rN.ReceiverNumber + ",";
                    }
                    receiverNumbers = receiverNumbers.Remove(receiverNumbers.Length - 1);
                }
                var sendSms = new SendSms();
                if (string.IsNullOrEmpty(maskName))
                    maskName = srd.Mask;
                var returnResult = sendSms.OneToMany(userName, userPassword, messageText, receiverNumbers, smsType, maskName, campaignName);
                var returnArray = returnResult.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                int mobIteration = 0;
                foreach (var rMessagePerMobile in returnArray)
                {
                    var smsHistory = new SmsHistory();
                    var exploded = rMessagePerMobile.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                    var receiverNumberfrmSplit = exploded[1];
                    smsHistory.Sms = srd.Message;
                    smsHistory.Program = srd.Program;
                    smsHistory.Organization = srd.Program != null ? srd.Program.Organization : udvash;
                    smsHistory.Student = srd.Student;
                    smsHistory.Mask = string.IsNullOrEmpty(maskName) ? srd.Mask : maskName;
                    if (srd.SmsType == 0)
                    {
                        smsHistory.Type = (int)_smsService.LoadSmsTypeName("SMS Registration Error").Id;
                    }
                    else
                    {
                        smsHistory.Type = srd.SmsType;
                    }
                    smsHistory.Batch = srd.Batch;
                    smsHistory.Priority = 2;
                    smsHistory.ErrorCode = exploded[0];
                    if (exploded.Length == 3 && smsHistory.ErrorCode == "1900")
                    {
                        if (!string.IsNullOrEmpty(exploded[2]))
                        {
                            smsHistory.ResponseCode = exploded[2];
                            SmsViewModel currentNumber = srd.SmsToBeSend != null ? srd.SmsToBeSend.FirstOrDefault(r => r.ReceiverNumber == receiverNumberfrmSplit) : new SmsViewModel { ReceiverNumber = receiverNumberfrmSplit, SmsReceiverId = 0 };
                            if (currentNumber != null)
                            {
                                smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                                smsHistory.SmsReceiver = _smsService.LoadReceiverById(currentNumber.SmsReceiverId);
                            }
                        }
                    }
                    else
                    {
                        smsHistory.Status = SmsHistory.EntityStatus.Pending;
                        SmsViewModel currentNumber = smsToBeSend.Skip(mobIteration).Take(1).FirstOrDefault();
                        if (currentNumber != null)
                        {
                            smsHistory.ReceiverNumber = currentNumber.ReceiverNumber;
                            smsHistory.SmsReceiver = _smsService.LoadReceiverById(currentNumber.SmsReceiverId);
                        }
                    }
                    smsHistory.CreateBy = smsUserId;
                    smsHistory.ModifyBy = smsUserId;
                    if (!string.IsNullOrEmpty(smsHistory.ReceiverNumber))
                        smsHistory.ReceiverNumber = "88" + smsHistory.ReceiverNumber;
                    _smsService.SaveSmsHistory(smsHistory);
                }
                srLog.RegistrationStatus = registrationStatus;
                srLog.CreateBy = smsUserId;
                srLog.ModifyBy = smsUserId;
                if (!string.IsNullOrEmpty(srLog.SenderMobileNumber))
                    srLog.SenderMobileNumber = "88" + srLog.SenderMobileNumber;
                _srlService.Save(srLog);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #endregion
    }
}