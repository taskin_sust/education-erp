﻿/*HabibXP */
$(document).ready(function () {

    $('#informationViewList').bootstrapDualListbox({
        nonselectedlistlabel: 'Non-selected',
        selectedlistlabel: 'Selected',
        preserveselectiononmove: 'moved',
        moveOnSelect: false,
        showFilterInputs: false,
        infoText: '',
    });

    $(document).on('change', '#OrganizationId', function () {

        $('.field-validation-error').remove();

        $("#missingImageCount").val("...");
        $("#existingImageCount").val("...");

        //remove all dependant dropdownlist & selection option
        $("#BranchId option").remove();
        $("#CampusId option").remove();

        $("#batchDays option").remove();
        $("#batchTime option").remove();
        $("#batchName option").remove();
        $('#ProgramId').empty();
        $('#ProgramId').append("<option value=''>Select a Program</option>");
        $('#SessionId').empty();
        $('#SessionId').append("<option value=''>Select a session</option>");


        var organizationId = $("#OrganizationId").val();

        if (organizationId != null && organizationId != "") {
            $.ajax({
                type: "post",
                //url: "/Student/StudentImage/AjaxRequestForProgramByOrganization",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                cache: false,
                async: true,
                data: { organizationIds: organizationId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    //var listItems = "";
                    //$.each(response, function (index) {
                    //    listItems += "<option value='" + response[index].Value + "'>" + response[index].Text + "</option>";
                    //});
                    //$("#ProgramId").html(listItems);
                    $.each(response.returnProgramList, function (i, v) {
                        $('#ProgramId').append($('<option>').text(v.Text).attr('value', v.Value));
                        
                    });
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
    });
    //Program Change Functionalities
    $(document).on('change', '#ProgramId', function () {

        $('.field-validation-error').remove();

        $("#missingImageCount").val("...");
        $("#existingImageCount").val("...");

        //remove all dependant dropdownlist & selection option
        $("#BranchId option").remove();
        $("#CampusId option").remove();

        $("#batchDays option").remove();
        $("#batchTime option").remove();
        $("#batchName option").remove();

        $('#SessionId').empty();
        $('#SessionId').append("<option value=''>Select a session</option>");


        var programId = $("#ProgramId").val();

        if (programId != null && programId != "") {
            $.ajax({
                type: "post",
                //url: "/Student/StudentImage/AjaxRequestForSession",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                cache: false,
                async: true,
                data: { programIds: programId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $.each(result.returnSessionList, function (i, v) {
                        $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                        
                    });
                    //$("#SessionId").html(result);
                    console.log("Success -----");
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });

    //Session Change Functionalities
    $(document).on('change', '#SessionId', function () {
         
        $('.field-validation-error').remove();

        $("#missingImageCount").val("...");
        $("#existingImageCount").val("...");

        //remove all dependant dropdownlist & selection option
        $("#BranchId option").remove();
        $("#CampusId option").remove();

        $("#batchDays option").remove();
        $("#batchTime option").remove();
        $("#batchName option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();

        if (programId != null && programId != "" && sessionId != null && sessionId != "") {
            $.ajax({
                type: "post",
                //url: "/Student/StudentImage/AjaxRequestForBranch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBranch",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    if (result.IsSuccess) {
                        $('#BranchId').append('<option value="0">All Branch</option>');
                        $.each(result.returnBranchList, function (i, v) {
                            $('#BranchId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        
                        console.log("Success -----");
                    } else {
                        console.log("Error -----");
                        console.log(result);
                    }

                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });

    //Branch Change Functionalities
    $(document).on('change', '#BranchId', function () {
         
        $('.field-validation-error').remove();

        $("#missingImageCount").val("...");
        $("#existingImageCount").val("...");

        //remove all dependant dropdownlist & selection option 
        $("#CampusId option").remove();

        $("#batchDays option").remove();
        $("#batchTime option").remove();
        $("#batchName option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#BranchId").val();

        if (programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "") {
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Student/StudentImage/AjaxRequestForCampus",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadCampus",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#CampusId').append('<option value="0">All Campus</option>');
                    if (result.IsSuccess) {
                        $.each(result.returnCampusList, function (i, v) {
                            $('#CampusId').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        console.log(result);
                    }

                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });


    //Campus Change Functionalities
    $(document).on('change', '#CampusId', function () {

        $('.field-validation-error').remove();

        $("#missingImageCount").val("...");
        $("#existingImageCount").val("...");

        //remove all dependant dropdownlist & selection option  
        $("#batchDays option").remove();
        $("#batchTime option").remove();
        $("#batchName option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#BranchId").val();
        var campusId = $("#CampusId").val();

        if (programId != null && programId != "" && sessionId != null && sessionId != "" && branchId != null && branchId != "" && campusId != null && campusId != "") {
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Student/StudentImage/AjaxRequestForBatchDay",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchDay",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#batchDays').append('<option value="0">All Batch Days</option>');
                    if (result.IsSuccess) {
                        $.each(result.returnBatchDays, function (i, v) {
                            $('#batchDays').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        //console.log(result);
                    }

                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    //console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    //console.log(result);
                }
            });
        }
    });


    //Batch Day Change Functionalities
    $(document).on('change', '#batchDays', function () {

        $('.field-validation-error').remove();

        $("#missingImageCount").val("...");
        $("#existingImageCount").val("...");

        //remove all dependant dropdownlist & selection option  
        $("#batchTime option").remove();
        $("#batchName option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#BranchId").val();
        var campusId = $("#CampusId").val();
        var batchDays = $("#batchDays").val();

        if (programId != null && programId != ""
            && sessionId != null && sessionId != ""
            && branchId != null && branchId != ""
            && campusId != null && campusId != ""
            && batchDays != null && batchDays != "") {
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Student/StudentImage/AjaxRequestForBatchTime",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatchTime",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId, batchDays: batchDays },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#batchTime').append('<option value="0">All Batch Times</option>');
                    if (result.IsSuccess) {
                        $.each(result.returnBatchTime, function (i, v) {
                            $('#batchTime').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        //$("#batchTime").find('option:eq(0)').prop('selected', true);
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                }
            });
        }
    });


    //Batch Time Change Functionalities
    $(document).on('change', '#batchTime', function () {

        $('.field-validation-error').remove();

        $("#missingImageCount").val("...");
        $("#existingImageCount").val("...");

        //remove all dependant dropdownlist & selection option  
        $("#batchName option").remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#BranchId").val();
        var campusId = $("#CampusId").val();
        var batchDays = $("#batchDays").val();
        var batchTime = $("#batchTime").val();

        var batchTimeText = [];
        $("#batchTime option:selected").each(function () {
            var $this = $(this);
            if ($this.length) {
                batchTimeText.push($this.text());
            }
        });

        if (programId != null && programId != ""
            && sessionId != null && sessionId != ""
            && branchId != null && branchId != ""
            && campusId != null && campusId != ""
            && batchDays != null && batchDays != ""
            && batchTime != null && batchTime != "") {
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Student/StudentImage/AjaxRequestForBatch",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadBatch",
                cache: false,
                async: true,
                data: { programIds: programId, sessionIds: sessionId, branchIds: branchId, campusIds: campusId, batchDays: batchDays, batchTimes: batchTime},
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    $.unblockUI();
                    $('#batchName').append('<option value="0">All Batch</option>');
                    if (result.IsSuccess) {
                        $.each(result.returnBatch, function (i, v) {
                            $('#batchName').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                        /// $("#batchName").find('option:eq(0)').prop('selected', true);
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        console.log(result);
                    }
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });


    //Batch Day Change Functionalities
    $(document).on('change', '#batchName', function () {

        $('.field-validation-error').remove();
        $("#missingImageCount").val("...");
        $("#existingImageCount").val("...");
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#BranchId").val();
        var campusId = $("#CampusId").val();
        var batchDays = $("#batchDays").val();
        var batchTime = $("#batchTime").val();
        var batchId = $("#batchName").val();

        if (programId != null && programId != ""
            && sessionId != null && sessionId != ""
            && branchId != null && branchId != ""
            && campusId != null && campusId != ""
            && batchDays != null && batchDays != ""
            && batchTime != null && batchTime != ""
            && batchId != null && batchId != "") {

            $.ajax({
                type: "post",
                url: $("body").attr("data-project-root") + "Student/StudentImage/AjaxRequestForImageMissingCount",
                cache: false,
                async: true,
                data: { programId: programId, sessionId: sessionId, branchId: branchId, campusId: campusId, batchDays: batchDays, batchTime: batchTime, batchId: batchId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    if (result.IsSuccess) {
                        $("#existingImageCount").val(result.existingImageCount);
                        $("#missingImageCount").val(result.missingImageCount);
                        console.log("Success ------");
                    } else {
                        console.log("Error -----");
                        console.log(result);
                    }
                    //$.ajax({
                    //    type: "post",
                    //    url: $("body").attr("data-project-root") + "Student/StudentImage/AjaxRequestForImageMissingCount",
                    //    cache: false,
                    //    async: true,
                    //    data: { programId: programId, sessionId: sessionId, branchId: branchId, campusId: campusId, batchDays: batchDays, batchTime: batchTime, batchId: batchId },
                    //    success: function (result) {
                    //        if (result.IsSuccess) {
                    //            $("#missingImageCount").val(result.missingImageCount);
                    //            console.log("Success ------");
                    //        } else {
                    //            console.log("Error -----");
                    //            console.log(result);
                    //        }
                    //        $.unblockUI();
                    //    },
                    //    complete: function () {
                    //        $.unblockUI();
                    //        console.log("Complete -----");
                    //        console.log(result);
                    //    },
                    //    error: function (result) {
                    //        $.unblockUI();
                    //        console.log("Failed -----");
                    //        console.log(result);
                    //    }
                    //});
                },
                complete: function () {
                    $.unblockUI();
                    console.log("Complete -----");
                    console.log(result);
                },
                error: function (result) {
                    $.unblockUI();
                    console.log("Failed -----");
                    console.log(result);
                }
            });
        }
    });

    //Existing & Missing image report
    $(document).on('click', '#MissingImageReport,#ExistingImageReport', function (e) {

        $('.field-validation-error').remove();

        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        var branchId = $("#BranchId").val();
        var campusId = $("#CampusId").val();
        var batchName = $("#batchName").val();
        var batchDays = $("#batchDays").val();
        var batchTime = $("#batchTime").val();

        sessionId != ""
            && programId != ""
            && branchId == null && branchId == ""
            && campusId != null && campusId != ""
            && batchName != null && batchName != ""
            && batchDays != null && batchDays != ""
            && batchTime != null && batchTime != "";

        var isSuccess = true;

        if (programId == null || programId <= 0) {
            var programMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Select a Program</span></span>';
            $('#ProgramId').parent().append(programMessage);
            isSuccess = false;
        }

        if (sessionId == null || sessionId <= 0) {
            var sessionMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Select a Session</span></span>';
            $('#SessionId').parent().append(sessionMessage);
            isSuccess = false;
        }

        if (branchId == null || branchId == "") {
            var branchMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Select a Branch</span></span>';
            $('#BranchId').parent().append(branchMessage);
            isSuccess = false;
        }

        if (campusId == null || campusId == "") {
            var campusMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Select a Campus</span></span>';
            $('#CampusId').parent().append(campusMessage);
            isSuccess = false;
        }

        if (batchDays == null || batchDays == "") {
            var batchDayMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Select a Batch Day</span></span>';
            $('#batchDays').parent().append(batchDayMessage);
            isSuccess = false;
        }

        if (batchTime == null || batchTime == "") {
            var batchTimeMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Select a Batch Time</span></span>';
            $('#batchTime').parent().append(batchTimeMessage);
            isSuccess = false;
        }

        if (batchName == null || batchName == "") {
            var batchNameMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Select a Batch</span></span>';
            $('#batchName').parent().append(batchNameMessage);
            isSuccess = false;
        }


        if (isSuccess == true) {
            if ($(this).val() == "Existing Image Report") {
                $("#isMissing").val(0);
                $('form#MissingImage').attr("action", "/Student/StudentImage/GenerateMissingImageReport");
                $('form#MissingImage').submit();
            }
            else { 
                $("#isMissing").val(1);
                $('form#MissingImage').attr("action", "/Student/StudentImage/GenerateMissingImageReport");
                $('form#MissingImage').submit();
            }

            return true;
        } else {
            e.preventDefault();
            return false;
        }
    });
});