﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NHibernate;
using UdvashERP.Dao.Teachers;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Teachers
{
    public interface ITeacherAcademicMaterialService:IBaseService
    {
        #region Operational function
        bool Save(TeacherAcademicMaterials teacherAcademicMaterial);
        #endregion

        #region Others function
        int? GetSerialNumber();
        #endregion
    }

    public class TeacherAcademicMaterialService:BaseService,ITeacherAcademicMaterialService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("TeacherAcademicMaterialService");
        #endregion

        #region Propertise & Object Initialization
        private readonly ITeacherAcademicMaterialDao _teacherAcademicMaterialDao;
        public TeacherAcademicMaterialService(ISession session)
        {
            Session = session;
            _teacherAcademicMaterialDao = new TeacherAcademicMaterialDao(){Session = session};
        }
        #endregion

        #region Operational function
        public bool Save(TeacherAcademicMaterials teacherAcademicMaterial)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    _teacherAcademicMaterialDao.Save(teacherAcademicMaterial);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region other function
        public int? GetSerialNumber()
        {
            try
            {
                int? serial = 300001;
                var academicMaterial = _teacherAcademicMaterialDao.GetTeacherAcademicMaterial();
                if (academicMaterial != null)
                {
                    serial = academicMaterial.SerialNumber + 1;
                }
                return serial;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion
    }
}
