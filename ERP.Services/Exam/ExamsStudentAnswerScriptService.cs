﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NHibernate;
using UdvashERP.Dao.Exam;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.Exam
{
    public interface IExamsStudentAnswerScriptService : IBaseService
    {
        #region Operational Function
        bool IsAnswerSaveSuccessfully(string answers, string setCode, long examId);
        bool IsAnswerUpdateSuccessfully(string answers, string setCode, long toInt64);
        bool IsAnswerDeleteSuccessfully(string setCode, long examId);
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        IList<ExamsStudentAnswerScript> GetExamStudentAnsScriptByExamId(long examId, int setCode);
        #endregion

        #region Others Function
        string GetAssociateSecCode(string setCode);
        bool CheckIsDuplicateAnswer(long programId, long sessionId, long courseId, long examId, int setCode);
        bool IsValidAnswerSheet(string answers, string examId, out int totalQues);
        #endregion

        #region Helper Function
        Dictionary<string, int> LoadSetCodes();
        #endregion
    }
    public class ExamsStudentAnswerScriptService : BaseService, IExamsStudentAnswerScriptService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("ExamsStudentAnswerScriptService");
        #endregion

        #region Propertise & Object Initialization
        private readonly IExamsStudentAnswerScriptDao _examsStudentAnswerScriptDao;
        private readonly IExamsDao _examsDao;
        private readonly IExamsDetailsDao _examsDetailsDao;
        private readonly IExamsStudentMarksDao _examsStudentMarksDao;
        public ExamsStudentAnswerScriptService(ISession session)
        {
            Session = session;
            _examsStudentAnswerScriptDao = new ExamsStudentAnswerScriptDao() { Session = Session };
            _examsDao = new ExamsDao() { Session = Session };
            _examsDetailsDao = new ExamsDetailsDao() { Session = Session };
            _examsStudentMarksDao = new ExamsStudentMarksDao() { Session = Session };
        }
        #endregion

        #region Operational Function
        public bool IsAnswerSaveSuccessfully(string answers, string setCode, long examId)
        {
            try
            {
                IList<ExamsStudentAnswerScript> examsStudentAnswerScripts = new List<ExamsStudentAnswerScript>();
                var exam = _examsDao.LoadById(examId);
                //var ss = exam.ExamDetails.Count;
                var ans = answers.Split(',');
                foreach (var examDetail in exam.ExamDetails)
                {
                    if (examDetail.ExamType == ExamType.Mcq)
                    {
                        var quesFrom = examDetail.QuestionFrom;
                        var quesTo = examDetail.QuestionTo;

                        for (int i = quesFrom - 1; i < quesTo; i++)
                        {
                            var examStdAnsScript = new ExamsStudentAnswerScript();
                            examStdAnsScript.ExamsDetails = examDetail;
                            examStdAnsScript.SetCode = Convert.ToInt32(setCode);
                            examStdAnsScript.QuestionNo = i + 1;
                            examStdAnsScript.QuestionAnswer = !String.IsNullOrEmpty(ans[i]) ? ans[i] : "";
                            //examStdAnsScript.QuestionAnswer = ans[i];
                            examsStudentAnswerScripts.Add(examStdAnsScript);
                        }
                    }
                }

                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        foreach (ExamsStudentAnswerScript examsStudentAnswerScript in examsStudentAnswerScripts)
                        {
                            var prevAnswerScript =
                                _examsStudentAnswerScriptDao.LoadExamsStudentAnswerScript(
                                    examsStudentAnswerScript.ExamsDetails.Id, examsStudentAnswerScript.SetCode,
                                    examsStudentAnswerScript.QuestionNo);

                            if (prevAnswerScript.Count>0)
                            {
                                var answerScript= prevAnswerScript.Single();
                                answerScript.QuestionAnswer = examsStudentAnswerScript.QuestionAnswer;
                                _examsStudentAnswerScriptDao.Update(answerScript);
                            }
                            else
                            {
                                _examsStudentAnswerScriptDao.Save(examsStudentAnswerScript);
                            }
                        }
                        trans.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        trans.Rollback();
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;

            }
        }
        public bool IsAnswerUpdateSuccessfully(string answers, string setCode, long examId)
        {
            try
            {
                IList<ExamsStudentAnswerScript> examsStudentAnswerScripts = new List<ExamsStudentAnswerScript>();
                var exam = _examsDao.LoadById(examId);
                var ans = answers.Split(',');
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        foreach (var examDetail in exam.ExamDetails)
                        {
                            if (examDetail.ExamsStudentAnswerScripts != null)
                                if (examDetail.ExamType == ExamType.Mcq)
                                {
                                    var quesFrom = examDetail.QuestionFrom;
                                    //var quesTo = examDetail.QuestionTo;
                                    int i = quesFrom - 1;
                                    var ed =
                                        examDetail.ExamsStudentAnswerScripts.Where(
                                            x => x.SetCode == Convert.ToInt32(setCode)).ToList();
                                    foreach (var examStdAnsSheet in ed)
                                    {
                                        //int i = quesFrom - 1;
                                        //if (examStdAnsSheet.SetCode == Convert.ToInt32(setCode.Trim()))
                                        //{
                                        //examDetail.ExamsStudentAnswerScripts.Where(
                                        //    x => x.SetCode == Convert.ToInt32(setCode));
                                        //examDetail.ExamsStudentAnswerScripts.Clear();
                                        //for (int i = quesFrom - 1; i < quesTo; i++)
                                        //{
                                        //var examStdAnsScript = new ExamsStudentAnswerScript();
                                        examStdAnsSheet.ExamsDetails = examDetail;
                                        examStdAnsSheet.SetCode = Convert.ToInt32(setCode);
                                        examStdAnsSheet.QuestionNo = i + 1;
                                        examStdAnsSheet.QuestionAnswer = !String.IsNullOrEmpty(ans[i]) ? ans[i] : "";
                                        i++;
                                        _examsStudentAnswerScriptDao.Update(examStdAnsSheet);
                                        //examDetail.ExamsStudentAnswerScripts.Add(examStdAnsSheet);
                                        //}
                                        //}
                                    }
                                    //examDetail.ExamsStudentAnswerScripts.Clear();
                                    //_examsDetailsDao.Update(examDetail);
                                }
                        }
                        trans.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        //throw ex;
                        _logger.Error(ex);
                        trans.Rollback();
                        return false;
                    }

                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
        }
        public bool IsAnswerDeleteSuccessfully(string setCode, long examId)
        {
            try
            {
                var exam = _examsDao.LoadById(examId);
                using (var trans = Session.BeginTransaction())
                {
                    try
                    {
                        bool isDelete = true;
                        foreach (var examDetail in exam.ExamDetails)
                        {
                            if (examDetail.ExamType == ExamType.Mcq)
                            {
                                var examstdMarks = _examsStudentMarksDao.LoadByExamDetailId(examDetail.Id);
                                if (examstdMarks == null)
                                {
                                    var deletedList = new List<ExamsStudentAnswerScript>();

                                    foreach (var examStdAnsSheet in examDetail.ExamsStudentAnswerScripts.Where(x => x.SetCode == Convert.ToInt32(setCode)))
                                    {
                                        //examStdAnsSheet.ExamsDetails = null;
                                        deletedList.Add(examStdAnsSheet);
                                        //_examsStudentAnswerScriptDao.Update(examStdAnsSheet);
                                        //_examsStudentAnswerScriptDao.Delete(examStdAnsSheet);
                                    }
                                    foreach (ExamsStudentAnswerScript examsStudentAnswerScript in deletedList)
                                    {
                                        examDetail.ExamsStudentAnswerScripts.Remove(examsStudentAnswerScript);
                                    }
                                    _examsDetailsDao.Update(examDetail);
                                    //_examsStudentAnswerScriptDao.Update(examDetail);
                                    //trans.Commit();
                                }
                                else
                                {
                                    isDelete = false;
                                    break;
                                }
                            }
                        }
                        if (isDelete)
                        {
                            _examsDao.Update(exam);
                            trans.Commit();
                            return true;
                        }
                        else
                        {
                            throw new Exception("Can't Delete This Exam Sheet! It's already been used for Student marks Entry");
                        }

                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        trans.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function
        public IList<ExamsStudentAnswerScript> GetExamStudentAnsScriptByExamId(long examId, int setCode)
        {
            try
            {
                var exam = _examsDao.LoadById(examId);
                return _examsStudentAnswerScriptDao.GetExamStudentAnsScriptByExamId(exam, setCode);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        #endregion

        #region Others Function
        public bool IsValidAnswerSheet(string answers, string examId, out int totalQues)
        {
            try
            {
                var exam = _examsDao.LoadById(Convert.ToInt64(examId));
                var onlyMcq = exam.ExamDetails.Where(x => x.ExamType == ExamType.Mcq).ToList();
                if (onlyMcq.Count <= 0)
                {
                    throw new Exception("Can't upload mcq answer to a written exam");
                }
                totalQues = onlyMcq[onlyMcq.Count - 1].QuestionTo;
                string[] ans = answers.Split(',');
                if (totalQues <= ans.Length)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw ;
            }
        }
        public string GetAssociateSecCode(string setCode)
        {
            try
            {
                switch (setCode)
                {
                    case "A":
                        return "1";
                    case "B":
                        return "2";
                    case "C":
                        return "3";
                    case "D":
                        return "4";
                    case "E":
                        return "5";
                    case "F":
                        return "6";
                    case "G":
                        return "7";
                    case "H":
                        return "8";
                    case "I":
                        return "9";
                    case "J":
                        return "10";
                }
                return "";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return "";

            }
        }
        public bool CheckIsDuplicateAnswer(long programId, long sessionId, long courseId, long examId, int setCode)
        {
            try
            {
                return _examsStudentAnswerScriptDao.CheckIsDuplicateAnswer(programId, sessionId, courseId, examId, setCode);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            
        }
        #endregion

        #region Helper Function
        public Dictionary<string, int> LoadSetCodes()
        {
            var dictionary = new Dictionary<string, int>();

            dictionary.Add("A", 1);
            dictionary.Add("B", 2);
            dictionary.Add("C", 3);
            dictionary.Add("D", 4);
            dictionary.Add("E", 5);
            dictionary.Add("F", 6);
            dictionary.Add("G", 7);
            dictionary.Add("H", 8);
            dictionary.Add("I", 9);
            dictionary.Add("J", 10);
            return dictionary;
        }
        #endregion
    }
}
