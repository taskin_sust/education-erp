﻿using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Teachers;

namespace UdvashERP.Dao.Mapping.Teachers
{
    public class TeacherAdmissionInfoMap : BaseClassMap<TeacherAdmissionInfo, long>
    {
        public TeacherAdmissionInfoMap()
        {
            Table("TeacherAdmissionInfo");
			LazyLoad();
			Map(x => x.UniversityName);
			Map(x => x.AtPosition);
            Map(x => x.AtSession);
            Map(x => x.GotSubject);
            //Map(x => x.TeacherActivity);
            References(x => x.Teacher).Column("TeacherId");
        }
    }
}
