﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Services.Test.AdminisTration.SurveyQuestionTest;
using Xunit;

namespace UdvashERP.Services.Test.Administration.SurveyQuestionTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "SurveyQuestion")]
   public class GetSurveyQuestionTest : SurveyQuestionBaseTest
    {
        #region Basic test
        [Fact]
        public void LoadById_Null_Check()
        {
            var obj = _surveyQuestionService.GetSurveyQuestion(-1);
            Assert.Null(obj);
        }

        [Fact]
        public void LoadById_NotNull_Check()
        {
            _surveyQuestionFactory.Create().WithQuestionAndRank(_question, 10);
            _surveyQuestionAnswerFactory.CreateMore(2, _surveyQuestionFactory.Object);
            _surveyQuestionFactory.WithAnswer(_surveyQuestionAnswerFactory.GetLastCreatedObjectList()).Persist();
            var obj = _surveyQuestionService.GetSurveyQuestion(_surveyQuestionFactory.Object.Id);
            Assert.NotNull(obj);
        }
        #endregion
    }
}
