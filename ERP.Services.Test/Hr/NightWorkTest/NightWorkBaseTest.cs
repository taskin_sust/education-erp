﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Hr.AttendanceSummaryServices;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.NightWorkTest
{
    public class NightWorkBaseTest : TestBase, IDisposable
    {
        #region Initialization

        public const int MentorId = 2;
        private ISession _session;
        protected readonly CommonHelper CommonHelper;
        protected readonly TeamMemberFactory _teamMemberFactory;
        protected readonly INightWorkService _nightWorkService;
        internal readonly NightWorkFactory _nightWorkFactory;
        protected readonly IMentorHistoryService _mentorHistoryService;

        #endregion

        public NightWorkBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _nightWorkService = new NightWorkService(_session);
            _nightWorkFactory = new NightWorkFactory(null, _session);
            _teamMemberFactory = new TeamMemberFactory(null, _session);
            _mentorHistoryService = new MentorHistoryService(_session);
        }



        #region Disposal

        public void Dispose()
        {
            _nightWorkFactory.CleanUp(_nightWorkFactory);
            _nightWorkFactory.LogCleanUp(_nightWorkFactory);
            _nightWorkFactory.CleanUp();
            base.Dispose();
        }

        #endregion
    }
}
