﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Mapping.Students
{
    public class StudentInfoHistoryMap : BaseClassMap<StudentInfoHistory, long>
    {
        public StudentInfoHistoryMap()
        {
            Table("StudentInfoHistory");
            LazyLoad();
            Map(x => x.ExamName);
            Map(x => x.Year);
            Map(x => x.Board);
            Map(x => x.BoardRoll);
            References(x => x.Student).Column("StudentId");

        }
    }
}
