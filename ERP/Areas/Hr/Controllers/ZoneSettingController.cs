﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class ZoneSettingController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Properties/Services/Dao & Initialization
        private readonly ICommonHelper _commonHelper;
        private readonly IZoneSettingService _hrZoneSettingService;
        private readonly IOrganizationService _organizationService;
        private List<UserMenu> _userMenu;
        public ZoneSettingController()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _hrZoneSettingService = new ZoneSettingService(nHsession);
            _organizationService = new OrganizationService(nHsession);
        }

        #endregion

        #region Index/Manage Page
        public ActionResult Index()
        {
            return RedirectToAction("ManageZoneSetting");
        }

        public ActionResult ManageZoneSetting(string message)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                if (!String.IsNullOrEmpty(message))
                {

                    ViewBag.SuccessMessage = message;
                }
                var organizationSelectList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.Organization = organizationSelectList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        public JsonResult LoadZoneSetting(int draw, long? organizationId = null)
        {
            var getData = new List<object>();
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                #region OrderBy and Direction
                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Organization.ShortName";
                            break;
                        case "1":
                            orderBy = "Name";
                            break;
                        case "2":
                            orderBy = "ToleranceTime";
                            break;
                        case "3":
                            orderBy = "ToleranceDay";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion
                int recordsTotal = _hrZoneSettingService.GetZoneSettingRowCount(_userMenu, orderBy, orderDir.ToUpper(), organizationId);
                long recordsFiltered = recordsTotal;
                List<ZoneSetting> zoneSettingList = _hrZoneSettingService.LoadZoneSetting(_userMenu, orderBy, orderDir.ToUpper(), organizationId).ToList();
                var data = new List<object>();
                foreach (var zoneSetting in zoneSettingList)
                {
                    var str = new List<string>();
                    str.Add(zoneSetting.Organization.ShortName);
                    str.Add(zoneSetting.Name);
                    str.Add(zoneSetting.ToleranceTime.ToString());
                    str.Add((zoneSetting.ToleranceDay == null) ? "-" : zoneSetting.ToleranceDay.ToString());
                    str.Add(StatusTypeText.GetStatusText(zoneSetting.Status));
                    str.Add(LinkGenerator.GetGeneratedLink("Edit", "Delete", "ZoneSetting", zoneSetting.Id, zoneSetting.Name));
                    data.Add(str);
                }

                return Json(new
                {
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    data = data,
                    isSuccess = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                recordsTotal = 0,
                recordsFiltered = 0,
                data = getData,
                isSuccess = false
            });
        }


        #endregion

        #region Operational Function

        #region Save Operation
        public ActionResult Create()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            var organizationSelectList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
            ViewBag.organizationList = organizationSelectList;
            return View();
        }
        [HttpPost]
        public ActionResult Create(string name, string organizationId, string toleranceTime, string toleranceDay, string deductionMultiplier)
        {
            var successState = false;
            var message = "";
            var customError = true;
            try
            {
                
                if (deductionMultiplier == "N/A")
                    deductionMultiplier = "";
                if (toleranceDay == "N/A")
                    toleranceDay = "";
                ZoneSetting zoneSetting = new ZoneSetting()
                {
                    Name = name,
                    Organization = _organizationService.LoadById(Convert.ToInt64(organizationId)),
                    ToleranceTime = Convert.ToInt32(toleranceTime),
                    ToleranceDay =!String.IsNullOrEmpty(toleranceDay) ? Convert.ToInt32(toleranceDay) : (int?) null,
                    DeductionMultiplier = !String.IsNullOrEmpty(deductionMultiplier) ? Convert.ToDecimal(deductionMultiplier) : (decimal?)null,
                };
                bool success = _hrZoneSettingService.Save(zoneSetting);
                if (success)
                {
                    customError = false;
                    successState = true;
                    message = "Zone Setting successfully saved";
                }
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                message = ex.Message;
            }
            return Json(new { customError = customError, IsSuccess = successState, Message = message });
            // return Json(new Response(successState, successMessage));
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(long id)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var zoneSettingObj = _hrZoneSettingService.LoadById(id);
                ViewBag.organizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName", zoneSettingObj.Organization.Id);
                GetStatusText();
                return View(zoneSettingObj);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
                GetStatusText();
                return View();
            }
        }

        [HttpPost]
        public JsonResult Edit(long id, string name, string organizationId, string toleranceTime, string toleranceDay, string deductionMultiplier, string status)
        {
            var successState = false;
            var message = "";
            var customError = true;
            try
            {
                if (deductionMultiplier == "N/A")
                    deductionMultiplier = "";
                if (toleranceDay == "N/A")
                    toleranceDay = "";
                bool isSuccess = _hrZoneSettingService.Update(id, name, organizationId, toleranceTime, toleranceDay, deductionMultiplier, status);
                if (isSuccess)
                {
                    successState = true;
                    customError = false;
                    message = "Zone Setting successfully updated.";
                }
            }
            catch (EmptyFieldException ex)
            {
                message = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occurred.";
                _logger.Error(ex);
            }
            return Json(new { customError = customError, IsSuccess = successState, Message = message });
        }
        #endregion

        #region Delete Operation
        [HttpPost]
        public JsonResult Delete(long id)
        {
            var successState = false;
            var message = "";
            var customError = true;
            try
            {
                bool isSuccess = _hrZoneSettingService.Delete(id);
                if (isSuccess)
                {
                    successState = true;
                    message = "Zone Setting sucessfully deleted.";
                }
                // return Json(isSuccess ? new Response(true, "Shift Setting sucessfully deleted.") : new Response(false, "Problem Occurred. Please Retry"));
            }
            catch (DependencyException ex)
            {
                message = ex.Message;
            }
            catch (Exception ex)
            {
                message = "Problem Occurred.";
                _logger.Error(ex);
                //  return Json(new Response(false, "Shift Setting Delete Fail !"));
            }
            return Json(new { customError = customError, IsSuccess = successState, Message = message });
        }
        #endregion

        #region Details Operation
        #endregion

        #endregion

        #region Helper Function
        private void GetStatusText()
        {
            ViewBag.StatusText = _commonHelper.GetStatus();
        }
        #endregion
    }
}