﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class BranchController : Controller
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private Branch _branchObj;
        private IList<Branch> _branchesList;
        private readonly IBranchService _branchService;
        private readonly ICommonHelper _commonHelper;
        private readonly IUserService _userService;
        private readonly IOrganizationService _organizationService;
        public BranchController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _branchObj = new Branch();
            _branchesList = new List<Branch>();
            _branchService = new BranchService(session);
            _commonHelper = new CommonHelper();
            _userService = new UserService(session);
            _organizationService = new OrganizationService(session);
        }
        #endregion

        #region Index/Manage Page
        public ActionResult Index()
        {
            try
            {
                ViewBag.Organization = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active), "Id", "ShortName");
                ViewBag.StatusText = _commonHelper.GetStatus();
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        #region Render Data Table
        public JsonResult BranchList(int draw, int start, int length, string organization, string name,string shortName, string code, string status)
        {
            var getData = new List<object>();
            try
            {
                #region OrderBy and Direction

                NameValueCollection nvc = Request.Form;
                string orderBy = "";
                string orderDir = "";

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "organization.Name";
                            break;
                        case "1":
                            orderBy = "Name";
                            break;
                        case "3":
                            orderBy = "Code";
                            break;
                        case "5":
                            orderBy = "Rank";
                            break;
                        case "6":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }

                #endregion

                int recordsTotal = _branchService.GetBranchCount(orderBy, orderDir.ToUpper(), organization, name, code,
                    status);
                long recordsFiltered = recordsTotal;
                _branchesList =
                    _branchService.LoadBranch(start, length, orderBy, orderDir.ToUpper(), organization, name, code,
                        status).ToList();

                var branchObj = new Branch();
                var maxMinRank = new int[2];
                maxMinRank[0] = _branchService.GetMinimumRank(branchObj);
                maxMinRank[1] = _branchService.GetMaximumRank(branchObj);
                //int srn = 1;
                foreach (var branch in _branchesList)
                {
                    var str = new List<string>();
                    //str.Add(srn.ToString());
                    str.Add(branch.Organization != null ? branch.Organization.ShortName : "");
                    str.Add(branch.Name);
                    str.Add(branch.ShortName);
                    str.Add(branch.Code);
                    str.Add(branch.IsCorporate.ToString());
                    string rankbtn = branch.Rank.ToString();
                    if (branch.Rank > maxMinRank[0])
                    {
                        rankbtn += "<a href='#'  id='" + branch.Id +
                                   "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                    }
                    if (branch.Rank < maxMinRank[1])
                    {
                        rankbtn += "<a href='#'  id='" + branch.Id +
                                   "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                    }
                    str.Add(rankbtn);
                    str.Add(StatusTypeText.GetStatusText(branch.Status));
                    str.Add(
                        "<a href='" + Url.Action("Details", "Branch") + "?Id=" + branch.Id + "' data-id='" + branch.Id +
                        "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp;&nbsp"
                        +
                        "<a href='" + Url.Action("Edit", "Branch") + "?Id=" + branch.Id + "' data-id='" + branch.Id +
                        "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; "
                        +
                        "<a  id='" + branch.Id.ToString() + "'    href='#' data-name='" + branch.Name.ToString() +
                        "' class='glyphicon glyphicon-trash'></a> "
                        );
                    //srn++;
                    getData.Add(str);
                }
                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData
                });
            }
            catch (NullObjectException ex)
            {
                _logger.Error(ex);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });
        }
        #endregion

        #endregion

        #region Operational Function

        #region Save Operation
        public ActionResult Create(string message)
        {
            try
            {
                var organizations = _organizationService.LoadOrganization(Organization.EntityStatus.Active);
                ViewBag.organizationList = new SelectList(organizations.ToList(), "Id", "ShortName");
                ViewBag.SuccessMessage = message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(Branch branchObj)
        {
            try
            {
                CheckNullOrEmpty(branchObj);
                var organizations = _organizationService.LoadOrganization(Organization.EntityStatus.Active);
                ViewBag.organizationList = new SelectList(organizations.ToList(), "Id", "ShortName");
                bool isSaved = _branchService.IsSave(branchObj);
                if (isSaved)
                {
                    return RedirectToAction("Create", new { message = "Branch successfully saved." });
                }
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
            }
            return View();
        }

      
        #endregion

        #region Update Operation

        public ActionResult Edit(long id)
        {
            try
            {
                InitializeUpdateView(id);
                //ViewBag.organizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(), "Id", "ShortName");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred.";
            }
           
            return View(_branchObj);
        }

   
        [HttpPost]
        public ActionResult Edit(long id, Branch branchObj)
        {
            try
            {
                //empty or null check
                CheckNullOrEmpty(branchObj);
                bool isUpdated = _branchService.IsUpdate(id, branchObj);
                if (isUpdated)
                {
                    ViewBag.SuccessMessage = "Branch successfully updated.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Problem Occurred, during branch update.";
                } 
            }
            catch (EmptyFieldException ex)
            {
                ViewBag.ErrorMessage = ex.Message;    
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;               
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred, during branch update.";
            }
            //initialize update view
            InitializeUpdateView(id);
            return View(_branchObj);
        }

    
        #endregion

        #region Delete Operation
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                bool isDeleted = _branchService.IsDelete(id);
                if (isDeleted)
                {
                    return Json(new Response(true, "Branch sucessfully deleted."));
                }
                return Json(new Response(true, "Problem Occurred. Retry"));
            }
            catch (DependencyException ex)
            {
                return Json(new Response(false, "You can't delete this branch, campus or program is already declared here."));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Problem Occurred.";
                _logger.Error(ex);
                return Json(new Response(false, "Branch Delete Fail !"));
            }
        }

        #endregion

        #region Details Operation

        public ActionResult Details(long id)
        {
            try
            {
                _branchObj = _branchService.GetBranch(id);
                if (_branchObj != null)
                {
                    _branchObj.CreateByText = _userService.GetUserNameByAspNetUserId(_branchObj.CreateBy);
                    _branchObj.ModifyByText = _userService.GetUserNameByAspNetUserId(_branchObj.ModifyBy);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return View(_branchObj);
        }
        #endregion

        #region Rank Operation

        #region old code
        //public JsonResult RankChange(long id, int current, string action)
        //{
        //    try
        //    {
        //        Branch branchUpdateObj = _branchService.GetBranch(Convert.ToInt64(id));
        //        if (branchUpdateObj == null)
        //        {
        //            return null;
        //        }
        //        int newRank;
        //        if (action == "up")
        //            newRank = branchUpdateObj.Rank - 1;
        //        else
        //            newRank = branchUpdateObj.Rank + 1;
        //        var branchOldObj = _branchService.GetBranchByRankNextOrPrevious(newRank, action); //LoadByRank(newRank);
        //        newRank = branchOldObj.Rank;
        //        branchOldObj.Rank = branchUpdateObj.Rank;
        //        branchUpdateObj.Rank = newRank;
        //        bool isSuccess = _branchService.UpdateRank(branchOldObj, branchUpdateObj);
        //        if (isSuccess)
        //        {
        //            return Json("Rank Changes");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //    }
        //    return Json("Something Wrong Occured!!");
        //}
        #endregion

        public JsonResult RankChange(long id, int current, string action)
        {
            try
            {
                Branch branchUpdateObj = _branchService.GetBranch(Convert.ToInt64(id));
                bool isSuccess = _branchService.UpdateRank(branchUpdateObj, action);
                if (isSuccess)
                {
                    return Json("Rank Changes");
                }
            }
            catch (NullObjectException ex)
            {
                return Json(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json("Something Wrong Occured!!");
        }

        #endregion
        #endregion

        #region Helper Function
        private void CheckNullOrEmpty(Branch branchObj)
        {
            if (branchObj.Name == null)
                throw new EmptyFieldException("Branch name in empty");
            if (branchObj.Code == null)
                throw new EmptyFieldException("Branch code in empty");
        }
        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
        }
        private void InitializeUpdateView(long branchId)
        {
            _branchObj = _branchService.GetBranch(branchId);
            ViewBag.organizationList = new SelectList(_organizationService.LoadOrganization(Organization.EntityStatus.Active).ToList(), "Id", "ShortName", _branchObj.Organization.Id.ToString());
            ViewBag.SelectedOrganization = _branchObj.Organization != null ? _branchObj.Organization.Id.ToString() : "";
            GetStatusText();
        }

        #endregion
    }
}
