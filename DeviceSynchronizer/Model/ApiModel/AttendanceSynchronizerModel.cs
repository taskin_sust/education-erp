﻿using System.Collections.Generic;

namespace DeviceSynchronizer.Model.ApiModel
{

    public class AttendanceSynchronizerModel
    {
        public string Name { get; set; }
        public int DataCallingInterval { get; set; }
        public List<AttendanceDeviceModel> AttendanceDevices { get; set; }
    }
}

