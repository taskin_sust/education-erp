﻿using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IOvertimeLogDao : IBaseDao<OvertimeLog, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

    }
    public class OvertimeLogDao : BaseDao<OvertimeLog, long>, IOvertimeLogDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function
        
        #endregion

        #region Helper Function

        #endregion
    }

}
