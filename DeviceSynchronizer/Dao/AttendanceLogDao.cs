﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeviceSynchronizer.Model.BusinessModel;

namespace DeviceSynchronizer.Dao
{
    public class AttendanceLogDao : BaseDao
    {
        public AttendanceLogDao(SQLiteConnection connection)
            : base(connection)
        {
        }

        #region Single object load

        //return list of AttendanceLog by querying into AttendanceLog table according to it's deviceId
        internal List<AttendanceLog> LoadByDeviceId(int deviceId, bool onlyValidPin = true)
        {
            string subQuery = "";
            if (onlyValidPin)
                subQuery = " AND EnrollNo != '' ";

            var query = string.Format("select * from AttendanceLog where DeviceId ={0} {1}", deviceId, subQuery);
            this.command.CommandText = query;
            return Fill();
        }

        #endregion

        #region Operation

        //Inser into AttendanceLog table
        public bool Insert(AttendanceLog obj)
        {
            try
            {
                string queryStr = string.Format(@"INSERT INTO  AttendanceLog
                                               (
                                                 EnrollNo
                                                ,PunchType
                                                , PunchTime
                                                , DeviceId
                                                    )
                                            VALUES('{0}', '{1}', '{2}','{3}')
                                          ", obj.EnrollNumber,obj.PunchType,
                                            AddSlashes(obj.PunchTime),
                                           obj.DeviceId
                                           );
                int numOfRows = ExecuteOperationQuery(queryStr);
                return numOfRows > 0;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return false;
            }
        }

        //Insert a list into AttendanceLog table
        internal bool Insert(List<AttendanceLog> attdlogList)
        {
            try
            {
                foreach (var obj in attdlogList)
                {
                    Insert(obj);
                }
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return false;
            }
        }

        //Delete list of AttendanceLog
        internal bool Delete(List<int> list)
        {
            try
            {
                string queryStr = string.Format(@"DELETE FROM AttendanceLog 
                                           WHERE Id in ({0})
                                          ", String.Join(",", list.ToArray())
                                              );
                ExecuteOperationQuery(queryStr);
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return false;
            }
        }

        #endregion

        #region Others

        //Fill up AttendanceLog object
        private List<AttendanceLog> Fill()
        {
            var resList = new List<AttendanceLog>();
            try
            {
                //now execute query
                this.reader = this.command.ExecuteReader();
                if (this.reader.HasRows)
                {
                    while (this.reader.Read())
                    {
                        int i = 0;
                        var row = new AttendanceLog();
                        row.ID = reader.GetInt32(i++);
                        row.EnrollNumber = reader.GetInt32(i++);
                        row.PunchType = reader.GetInt32(i++);
                        row.PunchTime = reader.GetString(i++);
                        row.DeviceId = reader.GetInt32(i++);
                        resList.Add(row);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                //close the reader
                if (this.reader != null && this.reader.IsClosed == false)
                    this.reader.Close();
            }

            //return value
            if (resList == null || resList.Count < 1)
                return null;
            return resList;
        }

        #endregion

    }
}
