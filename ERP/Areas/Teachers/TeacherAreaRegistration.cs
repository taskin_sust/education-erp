﻿using System.Web.Mvc;

namespace UdvashERP.Areas.Teachers
{
    public class TeacherAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Teachers";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Teachers_default",
                "Teachers/{controller}/{action}/{id}",
                new { controller = "TeachersHome", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}