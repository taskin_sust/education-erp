﻿/*Written By Sajjad Raihan */
$(document).ready(function () {
   
    $("#StudentProgramRoll").css('font-size', '140%');
    var maxLength = 11;
    $('#StudentProgramRoll').on('input focus keydown keyup', function (e) {

        //if ((e.keyCode >= 65 && e.keyCode <= 90)) {
           
        //    return false;
        //}
    //else
       // {
             var text = $(this).val();
        var lines = text.split(/(\r\n|\n|\r)/gm);
        for (var i = 0; i < lines.length; i++) {
            if (lines[i].length > maxLength) {
                lines[i] = lines[i].substring(0, maxLength);
            }
        }
        $(this).val(lines.join(''));
       // }
    });
    $(document).on('change', '#OrganizationId', function () {
        var organizationId = $(this).val();
        $("#courseId").empty();
        $("#SessionId").empty();
        $('#SessionId').append("<option value=''>Select Session</option>");
        $("#ProgramId").empty();
        $('#ProgramId').append("<option value=''>Select Program</option>");
        $("#subjectId").empty();
        if (organizationId != "" && organizationId!=null) {
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Student/StudentIdCard/AjaxRequestForProgramByOrganization",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                cache: false,
                async: true,
                data: { organizationIds: organizationId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    //var listItems = "";
                    //$.each(response, function (index) {
                    //    listItems += "<option value='" + response[index].Value + "'>" + response[index].Text + "</option>";
                    //});
                    //$("#ProgramId").html(listItems);
                    $.each(response.returnProgramList, function (i, v) {
                        $('#ProgramId').append($('<option>').text(v.Text).attr('value', v.Value));
                    });
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    bootbox.alert("Unable to update Program.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }

    });
    var totalerrormessage = "";
    $(document).on('change', '#ProgramId', function () {
        var programId = $("#ProgramId").val();
        // var sessionId = $("#SessionId").val();

        $("#courseId").empty();
        $("#SessionId").empty();
        $('#SessionId').append('<option value="">Select Session</option>');
        $("#subjectId").empty();
        if (programId != "") {
            $.ajax({
                type: "post",
                //url: $("body").attr("data-project-root") + "Student/StudentIdCard/GetSession",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                cache: false,
                async: true,
                data: { programIds: programId,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    if (result.IsSuccess) {
                        $.each(result.returnSessionList, function (i, v) {
                            $('#SessionId').append($('<option>').text(v.Text).attr('value', v.Value));
                            //$("#SessionId").find('option:eq(0)').prop('selected', true);
                        });
                    }
                    else {

                        bootbox.alert(result.Message).css('margin-top', (($(window).height() / 4)));
                    }
                    $.unblockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (result) {
                    bootbox.alert("Unable to update status.").css('margin-top', (($(window).height() / 4)));
                }
            });
        }

    });
    $("#ProgramId").on("change", function (e) {
        $('#pmsg').text("");
       
    });
    $(document).on('change', "#SessionId", function() {
        $('#smsg').text("");
    });
    function cleanArray(actual) {
        var newArray = new Array();
        for (var i = 0; i < actual.length; i++) {
            if (actual[i]) {
                newArray.push(actual[i]);
            }
        }
        return newArray;
    }
    var rolls = [];
    $("#StudentProgramRoll").val('');
    $("#StudentProgramRoll").on("input propertychange", function (e) {
       // alert();
        rolls = $('#StudentProgramRoll').val().split("\n");
        rolls = cleanArray(rolls);
        console.log(rolls);
        if (rolls.length > 0) {
            
            $("#prmsg").text("");
           
            if (rolls[rolls.length - 1] == "") {
                $('#lastId').text(rolls[rolls.length - 2]);
            } else {
                $('#lastId').text(rolls[rolls.length - 1]);
            }
        } else {
            $('#lastId').text("");
        }

    });
    $('#Submit').click(function() {
        
        $('.customMessage').empty();
        function isNull(inputArray) {

            var allNull = true;
            if (inputArray.length > 0) {
                for (var i = 0; i < inputArray.length; i++) {
                    if (inputArray[i].trim().length == 0) {
                        continue;
                    } else {
                        allNull = false;
                        break;
                    }
                }
            } else {
                return allNull;
            }
            return allNull;
        }
        $('#pmsg').text("");
        $('#smsg').text("");
        $('#prmsg').text("");
        var message = "";
        var programId = $("#ProgramId").val();
        var sessionId = $("#SessionId").val();
        //var StudentProgramRoll = $("StudentProgramRoll").val();
        for (i = 0; i < rolls.length; i++) {
            if (rolls[i]==null||rolls[i]=="") {
                rolls.splice(i, 1);
            } 
        }
        var isSuccess = true;
        if (programId == null || programId <= 0) {
            $('#pmsg').text("");
            // var programMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="pmsg">Select Program</span></span>';
            $('#pmsg').append("Select Program");
            isSuccess = false;
        }

        if (sessionId == null || sessionId <= 0) {
            $('#smsg').text("");
            //var sessionMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="smsg">Select Session</span></span>';

            $('#smsg').append("Select Session");
            isSuccess = false;
        }

        if (rolls.length < 1 && isNull(rolls) == true) {
            $('#prmsg').text("");
            //var branchMessage = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="brmsg">Select Branch</span></span>';
            $('#prmsg').append("Enter Program Roll");
            isSuccess = false;
        }
        if (isSuccess == true) {
            var finalSuccess = true;
            $("#StudentProgramRoll").val('');
            $('#StudentProgramRoll').css('border-color', '#CCC');
            if (sessionId != "" && programId != "") {
                $.ajax({
                    type: "post",
                    url: $("body").attr("data-project-root") + "Student/StudentIdCard/DistributeIdCard",
                    cache: false,
                    async: true,
                    data: { programId: programId, sessionId: sessionId, programRoll: rolls },
                    beforeSend: function () {
                        $.blockUI({
                            timeout: 0,
                            message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                        });
                    },
                    success: function (result) {
                        var successCount = 0;
                        var errormsg = "";
                        var infomsg = "";
                        // console.log(result.length);
                        if (result != null && result.length > 0) {
                            for (var i = 0; i < result.length; i++) {
                                if (result[i].SuccessMessage != null) {
                                    successCount++;
                                    continue;
                                }
                                if (result[i].ErrorMessage != null) {
                                    errormsg += result[i].ErrorMessage + '<br>';
                                    continue;
                                }
                                if (result[i].InfoMessage != null) {
                                    infomsg += result[i].InfoMessage + '<br>';
                                    continue;
                                }
                            }
                            if (successCount > 0) {
                                $.fn.customMessage({
                                    displayMessage: successCount + " id card/s successfully distributed",
                                    displayMessageType: "s",
                                });
                            }
                            if (errormsg != "") {
                                $.fn.customMessage({
                                    displayMessage: errormsg,
                                    displayMessageType: "e",
                                });
                            }
                            if (infomsg != "") {
                                $.fn.customMessage({
                                    displayMessage: infomsg,
                                    displayMessageType: "i",
                                });
                            }
                        }
                       
                        $.unblockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    
                });
            } 
        } else {
            e.preventDefault();
            return false;
        }
    });

});
