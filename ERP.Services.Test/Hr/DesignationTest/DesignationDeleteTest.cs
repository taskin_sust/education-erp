﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.DesignationTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "Designation")]
    public class DesignationDeleteTest : DesignationBaseTest
    {
        #region Basic Test


        [Fact]
        public void Delete_Should_Return_True()
        {
            organizationFactory.Create().Persist();
            _designationFactory.Create().WithNameAndRank(TestName, ShortName, TestRank)
                .WithOrganization(organizationFactory.Object).Persist();
            var obj = _designationService.LoadById(_designationFactory.Object.Id);
            Assert.True(_designationService.Delete(obj));
        }
        
        #endregion

        #region Business Logic Test

        [Fact]
        public void Delete_Should_Return_DependencyException_Exception()
        {
            var objFactory = _designationFactory.Create().WithOrganization().WithNameAndRank(TestName, ShortName, TestRank).Persist();
            objFactory.Object.MentorHistory.Add(new MentorHistory());
            objFactory.Object.EmploymentHistory.Add(new EmploymentHistory());
            Assert.Throws<DependencyException>(() => _designationService.Delete(objFactory.Object));
        }

        #endregion
    }
}
