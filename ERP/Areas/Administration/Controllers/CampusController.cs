﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.Areas.Administration.Models;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class CampusController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("AdministrationArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICampusService _campusService;
        private readonly ICampusRoomService _campusRoomService;
        private readonly IBranchService _branchService;
        private readonly IUserService _userService;
        private readonly ICommonHelper _commonHelper;
        private readonly IOrganizationService _organizationService;

        public CampusController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _campusService = new CampusService(session);
            _campusRoomService = new CampusRoomService(session);
            _branchService = new BranchService(session);
            _userService = new UserService(session);
            _commonHelper = new CommonHelper();
            _organizationService = new OrganizationService(session);
        }

        #endregion

        #region Index/Manage Page

        [HttpGet]
        public ActionResult Index()
        {
            SelectList branch = null, organization = null;
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                organization = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                branch = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, null, null, null, false), "Id", "Name");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
                return View();
            }
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.CurrentPage = 1;
            ViewBag.organizationlist = organization;
            ViewBag.BranchId = branch;
            return View();
        }

        [HttpPost]
        public ActionResult Index(int draw, int start, int length, string orgId, string bId, string cName, string contactNumber, string cRank, string cStatus)
        {
            int recordsTotal = 0;
            long recordsFiltered = 0;
            var data = new List<object>();
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";

                    #region OrderBy and Direction

                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                        switch (orderBy)
                        {

                            case "0":
                                orderBy = "Branch";
                                break;
                            case "1":
                                orderBy = "Name";
                                break;
                            case "2":
                                orderBy = "Location";
                                break;
                            case "3":
                                orderBy = "ContactNumber";
                                break;
                            case "4":
                                orderBy = "Rank";
                                break;
                            case "5":
                                orderBy = "Status";
                                break;
                            default:
                                orderBy = "";
                                break;
                        }
                    }

                    #endregion

                    Campus campus = new Campus();
                    recordsTotal = _campusService.GetCampusCount(userMenu, orgId, bId, cName, contactNumber, cRank, cStatus);
                    recordsFiltered = recordsTotal;
                    List<Campus> campusList = _campusService.LoadCampus(userMenu, start, length, orderBy, orderDir.ToUpper(), orgId, bId, cName, contactNumber, cRank, cStatus).ToList();
                    int[] MaxMinRank = new int[2];
                    MaxMinRank[0] = _campusService.GetMinimumRank(campus);//1;
                    MaxMinRank[1] = _campusService.GetMaximumRank(campus);
                    
                    foreach (var cam in campusList)
                    {
                        var str = new List<string>();
                        str.Add(cam.Branch.Organization.ShortName);
                        str.Add(cam.Branch.Name);
                        str.Add(cam.Name);
                        str.Add(cam.Location);
                        str.Add(cam.ContactNumber);
                        str.Add(LinkGenerator.GetRankLinkForModal(cam.Id, cam.Rank, MaxMinRank[0], MaxMinRank[1]));
                        str.Add(StatusTypeText.GetStatusText(cam.Status));
                        str.Add(LinkGenerator.GetGeneratedDetailsEditModelDeleteLink("Details", "Edit", "Campus", cam.Id, cam.Name));
                        data.Add(str);
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                    _logger.Error(ex);
                }
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        #endregion

        #region Operational Function

        #region Save Operation

        [HttpGet]
        public ActionResult Create()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                if (TempData["CampusAdd"] != null)
                {
                    ViewBag.SuccessMessage = TempData["CampusAdd"];   
                }
                ViewBag.organizationlist = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.BranchId = new SelectList(new[] { new { ID = "", Name = "Select Branch" } }, "Id", "Name", 1);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Campus campusObj, string organizationId, string BranchId, string[] RoomNo, int[] ClassCapacity, int[] ExamCapacity)
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            IList<CampusRoom> campusRooms = new List<CampusRoom>();
            try
            {
                for (int i = 0; i < RoomNo.Length; i++)
                {
                    CampusRoom cr = new CampusRoom()
                    {
                        RoomNo = RoomNo[i],
                        ClassCapacity = ClassCapacity[i],
                        ExamCapacity = ExamCapacity[i]
                    };
                    campusRooms.Add(cr);
                }
                campusObj.CampusRooms = campusRooms;
                Branch branch = null;
                long selectedBranchId = 0;
                long.TryParse(BranchId, out selectedBranchId);
                branch = _branchService.GetBranch(selectedBranchId);
                campusObj.Branch = branch;
                bool isSuccess = _campusService.Save(campusObj);
                if (isSuccess)
                {
                    TempData["CampusAdd"] = "Campus Added Successfully";
                    return RedirectToAction("Create");
                }
                else
                {
                    ViewBag.ErrorMessage = "Campus Add Failed";
                    //ViewBag.CAMPUSROOMS = campusRooms;
                    //ViewBag.organizationlist = new SelectList(
                    //    _organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                    //ViewBag.BranchId =
                    //    new SelectList(
                    //        _branchService.LoadAuthorizedBranch(userMenu,
                    //            _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), null, null, false), "Id",
                    //        "Name", BranchId);
                    //return View();
                }
            }
            catch (ValueNotAssignedException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Some value is not correctly assigned";
                //ViewBag.CAMPUSROOMS = campusRooms;
                //ViewBag.organizationlist = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu),
                //    "Id", "ShortName");
                //ViewBag.BranchId =
                //    new SelectList(
                //        _branchService.LoadAuthorizedBranch(userMenu,
                //            _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), null, null, false), "Id",
                //        "Name", BranchId);
            }
            catch (DuplicateEntryException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                //ViewBag.CAMPUSROOMS = campusRooms;
                //ViewBag.organizationlist = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu),
                //    "Id", "ShortName");
                //ViewBag.BranchId =
                //    new SelectList(
                //        _branchService.LoadAuthorizedBranch(userMenu,
                //            _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), null, null, false), "Id",
                //        "Name", BranchId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                //ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                //ViewBag.CAMPUSROOMS = campusRooms;
                //ViewBag.organizationlist = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu),
                //    "Id", "ShortName");
                //ViewBag.BranchId =
                //    new SelectList(
                //        _branchService.LoadAuthorizedBranch(userMenu,
                //            _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), null, null, false), "Id",
                //        "Name", BranchId);
            }
            finally
            {
                ViewBag.CAMPUSROOMS = campusRooms;
                ViewBag.organizationlist = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                ViewBag.BranchId = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(organizationId)), null, null, false), "Id", "Name", BranchId);
            }
            return View();
        }

        #endregion

        #region Update Operation

        [HttpGet]
        public ActionResult Edit(long id)
        {
            ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");
            try
            {
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                Campus campusObj = _campusService.GetCampus(id);
                ViewBag.organizationlist = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName", campusObj.Branch.Organization.Id);
                ViewBag.BranchId = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(campusObj.Branch.Organization.Id)), null, null, false), "Id", "Name", campusObj.Branch.Id);

                IList<CampusRoom> campusRooms = campusObj.CampusRooms;
                ViewBag.CAMPUSROOMS = campusRooms;
                return View(campusObj);
            }
            catch (DataNotFoundException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Edit(long id, Campus campusObj, string organizationId, string BranchId, int[] OldIdList, string[] RoomNo, int[] ClassCapacity, int[] ExamCapacity)
        {
            IList<CampusRoom> campusRooms = new List<CampusRoom>();
            long selectedBranchId = 0;
            long selectedorgId = 0;
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                for (int i = 0; i < RoomNo.Length; i++)
                {
                    CampusRoom cr = new CampusRoom();

                    if (OldIdList != null & OldIdList.Count() > i)
                    {
                        cr = _campusRoomService.GetCampusRoom(OldIdList[i]);
                        cr.RoomNo = RoomNo[i];
                        cr.ClassCapacity = ClassCapacity[i];
                        cr.ExamCapacity = ExamCapacity[i];
                    }
                    else
                    {
                        cr = new CampusRoom()
                        {
                            RoomNo = RoomNo[i],
                            ClassCapacity = ClassCapacity[i],
                            ExamCapacity = ExamCapacity[i]
                        };
                    }

                    campusRooms.Add(cr);
                }
                campusObj.CampusRooms = campusRooms;

                Branch branch = null;
                long.TryParse(BranchId, out selectedBranchId);
                long.TryParse(organizationId, out selectedorgId);

                branch = _branchService.GetBranch(selectedBranchId);
                campusObj.Branch = branch;

                _campusService.Update(id, campusObj);
                ViewBag.SuccessMessage = "Successfully updated.";
            }
            catch (ValueNotAssignedException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (DuplicateEntryException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }

            finally
            {
                ViewBag.organizationlist = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName", selectedorgId);
                ViewBag.BranchId = new SelectList(_branchService.LoadAuthorizedBranch(userMenu, _commonHelper.ConvertIdToList(Convert.ToInt64(selectedorgId)), null, null, false), "Id", "Name", selectedBranchId);
                ViewBag.CAMPUSROOMS = campusRooms;
                ViewBag.StatusList = new SelectList(_commonHelper.GetStatus(), "Value", "Key");

            }
            return View(campusObj);

        }
        
        #endregion

        #region Delete Operation

        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                Campus campus = _campusService.GetCampus(id);
                bool isSuccess = _campusService.Delete(id, campus);

                if (isSuccess)
                {
                    ViewBag.SuccessMessage = "Campus deleted successfully";
                    return Json(new Response(true, "Campus sucessfully deleted."));
                }

                return Json(new Response(false, "Campus delete failed"));
            }
            catch (DependencyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return Json(new Response(false, ex.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Details Operation

        public ActionResult Details(int id)
        {
            try
            {
                var campusObj = _campusService.GetCampus(id);
                if (campusObj != null)
                {
                    campusObj.CreateByText = _userService.GetUserNameByAspNetUserId(campusObj.CreateBy);
                    campusObj.ModifyByText = _userService.GetUserNameByAspNetUserId(campusObj.ModifyBy);
                }
                return View(campusObj);
            }
            catch (DataNotFoundException ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.Message = WebHelper.SetExceptionMessage(ex);
                return View();
            }
        }

        #endregion

        #region Rank Operation

        public ActionResult RankChange(int id, int current, string action)
        {
            try
            {
                Campus campusObje = _campusService.GetCampus(Convert.ToInt64(id));
                if (campusObje == null)
                {
                    return HttpNotFound();
                }
                int newRank;
                if (action == "up")
                    newRank = campusObje.Rank - 1;
                else
                    newRank = campusObje.Rank + 1;
                var materialGroupOldObj = _campusService.GetCampusByRankNextOrPrevious(newRank, action);
                //LoadByRank(newRank);
                materialGroupOldObj.Rank = campusObje.Rank;
                campusObje.Rank = newRank;
                bool isSuccessfullyUpdateRank = _campusService.UpdateRank(materialGroupOldObj, campusObje);
                if (isSuccessfullyUpdateRank)
                {
                    return Json("Successfully changed");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(WebHelper.CommonErrorMessage);
        }

        #endregion

        #endregion

        #region Reports

        [HttpGet]
        public ActionResult RoomReport()
        {
            CampusRoomReportView campusRoomReportData = new CampusRoomReportView();
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                campusRoomReportData.OrgList = new SelectList(_organizationService.LoadAuthorizedOrganization(userMenu), "Id", "ShortName");
                campusRoomReportData.BranchList = new SelectList(new[] { new { ID = "", Name = "Select Branch" } }, "Id", "Name");
                List<Campus> calmpusList = new List<Campus>();
                campusRoomReportData.CampusList = new SelectList(calmpusList, "Id", "Name");
                campusRoomReportData.CampusRoomList = new List<CampusRoom>();
                return View(campusRoomReportData);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                IList<Branch> branchList = new List<Branch>();
                campusRoomReportData.BranchList = new SelectList(branchList, "Id", "Name");
                List<Campus> calmpusList = new List<Campus>();
                campusRoomReportData.CampusList = new SelectList(calmpusList, "Id", "Name");
                campusRoomReportData.CampusRoomList = new List<CampusRoom>();
                return View(campusRoomReportData);
            }
        }
        
        public ActionResult GetCampusRooms(string branchId, string[] selectedValues)
        {
            IList<CampusRoom> reportData = new List<CampusRoom>();
            try
            {
                if (selectedValues == null || selectedValues.Contains(SelectionType.SelelectAll.ToString()))
                {
                    long selectedBranchId = 0;
                    long.TryParse(branchId, out selectedBranchId);
                    reportData = _campusRoomService.LoadCampusRoom(selectedBranchId);
                    return PartialView("Partial/_CampusRoomReport", reportData);
                }

                reportData = _campusRoomService.LoadCampusRoom(selectedValues);
                return PartialView("Partial/_CampusRoomReport", reportData);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                return PartialView("Partial/_CampusRoomReport", reportData);
            }
        }

        #endregion 
    }
}