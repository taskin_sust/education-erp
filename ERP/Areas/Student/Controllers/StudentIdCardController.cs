﻿using FluentNHibernate.Testing.Values;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using UdvashERP.App_code;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.MediaDb;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Administration;
using UdvashERP.App_Start;
using UdvashERP.Services.Helper;
using UdvashERP.Services.MediaServices;
using UdvashERP.Services.Students;
using System.Text.RegularExpressions;
using OfficeOpenXml.FormulaParsing.Utilities;

namespace UdvashERP.Areas.Student.Controllers
{
    [UerpArea("Student")]
    [Authorize]
    [AuthorizeAccess]
    public class StudentIdCardController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("StudentArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly IStudentIdCardService _studentIdCardService;
        private readonly ICommonHelper _commonHelper;
        private readonly IBatchService _batchService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;
        private readonly IBranchService _branchService;
        private readonly ICampusService _campusService;
        private readonly ICourseService _courseService;
        private readonly IStudentProgramService _studentProgramService;
        private readonly IProgramBranchSessionService _programBranchSessionService;
        private readonly IStudentCourseDetailsService _studentCourseDetailsService;
        private readonly IMaterialDistributionService _materialDistributionService;
        private readonly IOrganizationService _organizationService;
        private List<UserMenu> _userMenu;
        private readonly IStudentImagesMediaService _studentImagesMediaService;
        private IServiceBlockService _serviceBlockService;

        public StudentIdCardController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _studentIdCardService = new StudentIdCardService(session);
                _commonHelper = new CommonHelper();
                _batchService = new BatchService(session);
                _programService = new ProgramService(session);
                _sessionService = new SessionService(session);
                _branchService = new BranchService(session);
                _campusService = new CampusService(session);
                _courseService = new CourseService(session);
                _programBranchSessionService = new ProgramBranchSessionService(session);
                _studentProgramService = new StudentProgramService(session);
                _studentCourseDetailsService = new StudentCourseDetailsService(session);
                _materialDistributionService = new MaterialDistributionService(session);
                _organizationService = new OrganizationService(session);
                _serviceBlockService=new ServiceBlockService(session);
                _studentImagesMediaService = new StudentImagesMediaService();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
        }

        #endregion

        #region Ajax Request Function
        
        #endregion

        #region Generate ID Card

        public ActionResult GenerateIdCard()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return View();
        }

        [HttpGet]
        public ActionResult GenerateIdCardReport()
        {
            return RedirectToAction("GenerateIdCard");
        }

        [HttpPost]
        public ActionResult GenerateIdCardReport(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, string[] informationViewList, string[] batchDays, string[] batchTime, /*int paymentStatus, int imageStatus,*/ int printingStatus)
        {
            try
            {
                
                ViewBag.programId = programId;
                ViewBag.sessionId = sessionId;
                ViewBag.branchId = branchId;
                ViewBag.campusId = campusId;
                ViewBag.batchName = batchName;
                ViewBag.course = course;
                ViewBag.printingStatus = printingStatus;
                ViewBag.informationViewList = informationViewList;
                ViewBag.batchDays = batchDays;
                ViewBag.batchTime = batchTime;
                ViewBag.PageSize = Constants.PageSize;
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
            }
            return View();
        }

        #region Rendering Data Table

        [HttpPost]
        public JsonResult StudentProgramListForIdCardResult(int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, string[] informationViewList, string[] batchDays, string[] batchTime, int printingStatus)
        {
            var data = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];
                    }
                    orderBy = "A.PrnNo";
                    orderDir = "ASC";

                    #region Service Block Operation

                    IList<ServiceBlock> blockedContent = _serviceBlockService.LoadContentTypeList(programId, sessionId, (int)BlockService.IdCard);

                    #endregion

                    recordsTotal = _studentProgramService.GetAuthorizedStudentProgramCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, course, printingStatus,null,blockedContent.ToList());

                    IList<StudentIdCardListDto> studentProgramList = _studentIdCardService.LoadStudentProgram(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, course, printingStatus, start, length, orderBy, orderDir, null, true, blockedContent.ToList()).ToList();
                    recordsFiltered = recordsTotal;
                    int sl = start + 1;
                    foreach (var c in studentProgramList)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());

                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Program Roll":
                                        str.Add(c.PrnNo);
                                        break;
                                    case "Student Name":
                                        str.Add(c.FullName);
                                        break;
                                    case "Father Name":
                                        str.Add(c.FatherName);
                                        break;
                                    case "Mobile Personal":
                                        str.Add(c.Mobile);
                                        break;
                                    case "Mobile Guardian":
                                        str.Add(c.GuardiansMobile1);
                                        break;
                                    case "Branch":
                                        str.Add(c.Branch);
                                        break;
                                    case "Batch":
                                        str.Add(c.Batch);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        var printcount = "0";
                        if (c.PrintingCount != null)
                        {
                            printcount = c.PrintingCount.ToString();
                        }
                        str.Add(printcount);
                        string chkBox = "";
                        chkBox += "<input type='checkbox' name='check" + c.Id + "' id='" + c.Id + "' class='innerchk' />";
                        str.Add(chkBox);
                        sl++;
                        data.Add(str);
                    }
                }

                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });


        }

        #endregion

        #region Export ID Card Result To Excel

        [HttpGet]
        public ActionResult IdCardResultToExcel(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, int printingStatus, string[] batchDays = null, string[] batchTime = null)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                var authprogramIds = _programService.LoadAuthorizedProgram(_userMenu).Select(x => x.Id).ToArray();
                var authsessionIds = _sessionService.LoadAuthorizedSession(_userMenu, null).Select(x => x.Id).ToArray();
                var authbranchIds = _branchService.LoadAuthorizedBranch(_userMenu).Select(x => x.Id).ToArray();
                #region Service Block Operation

                IList<ServiceBlock> blockedContent = _serviceBlockService.LoadContentTypeList(programId, sessionId, (int)BlockService.IdCard);

                #endregion
                List<string> headerList = new List<string>();
                List<string> columnList = new List<string>();
                List<string> footerList = new List<string>();
                headerList.Add(ErpInfo.OrganizationNameFull);
                headerList.Add("Id Card Generation List");
                columnList.Add("Program Roll");
                columnList.Add("Student Name");
                columnList.Add("Father's Name");
                columnList.Add("Mobile(Student)");
                columnList.Add("Mobile(Gurdian)");
                columnList.Add("Branch");
                columnList.Add("Batch");
                columnList.Add("Count");
                footerList.Add("");
                var distributionXlsList = new List<List<object>>();
                IList<StudentIdCardListDto> studentProgramList = _studentIdCardService.LoadStudentProgram(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, course, printingStatus, 0, 0, "A.PrnNo", "ASC", null, true, blockedContent.ToList()).ToList();
                if (studentProgramList.Count > 0)
                {
                    foreach (var c in studentProgramList)
                    {
                        string programRoll = "",
                            studentName = "",
                            fatherName = "",
                            personalMobile = "",
                            gurdianMobile = "",
                            branchName = "",
                            batchNameValue = "",
                            //  studentPaymentStatus="",
                            //  studentImageStatus="",
                            printCount = "0";
                        programRoll = c.PrnNo;

                        if (c.FullName != null) studentName = c.FullName;
                        if (c.FatherName != null) fatherName = c.FatherName;
                        if (c.Mobile != null) personalMobile = c.Mobile;
                        if (c.GuardiansMobile1 != null) gurdianMobile = c.GuardiansMobile1;
                        if (c.Branch != null) branchName = c.Branch;
                        if (c.Batch != null) batchNameValue = c.Batch;
                        if (c.PrintingCount != null)
                        {
                            printCount = c.PrintingCount.ToString();
                        }

                        var xlsRow = new List<object>
                        {
                            programRoll,
                            studentName,
                            fatherName,
                            personalMobile,
                            gurdianMobile,
                            branchName,
                            batchNameValue,
                            printCount
                        };
                        distributionXlsList.Add(xlsRow);
                    }
                }

                ExcelGenerator.GenerateExcel(headerList, columnList, distributionXlsList, footerList, "idCard-Distribution-list__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }

            catch (Exception ex)
            {
                return View("AutoClose");
            }
        }

        #endregion

        #endregion

        #region ID Distribution

        public ActionResult DistributeIdCard()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                //IList<Program> programs = _programService.LoadAuthorizedProgram(_userMenu).OrderBy(x => x.Rank).ToList();
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
                List<SelectListItem> sessions = new List<SelectListItem>();
                sessions.Add(new SelectListItem
                {
                    Text = "Select Session",
                    Value = "0"
                });
                ViewBag.SessionId = new SelectList(sessions.ToList(), "Value", "Text");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                Console.WriteLine("{0}", ex.Message);
            }
            return View();
        }

        [HttpPost]
        public JsonResult DistributeIdCard(long programId, long sessionId, List<string> programRoll)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    programRoll.RemoveAll(str => string.IsNullOrEmpty(str));
                    var validProgramRolls = programRoll.ToList();
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;

                    IList<Branch> branches = _branchService.LoadAuthorizedBranch(_userMenu);
                    List<ResponseMessage> messageList = _studentIdCardService.DistributeIdCard(programId, sessionId, validProgramRolls, branches);
                    return Json(messageList);
                }
                else
                {
                    return Json(new Response(false, WebHelper.CommonErrorMessage));
                }
            }
            catch (Exception ex)
            {

                _logger.Error(ex);
                return Json(new Response(false, WebHelper.SetExceptionMessage(ex)));
            }
        }

        #endregion

        #region Print ID Card

        [HttpPost]
        public ActionResult PrintIdCard(long[] studentProgramIdList)
        {
            try
            {
                //List<StudentImages> mediaImageList = new List<StudentImages>();
                List<StudentProgram> generatedStudentPrograms = new List<StudentProgram>();
                var studentProgram = _studentIdCardService.LoadAllStudentProgramList(studentProgramIdList);
                foreach (var studentprogram in studentProgram)
                {
                    var studentIdCard = _studentIdCardService.getByStudentProgramId(studentprogram.Id);
                    if (studentIdCard == null)
                    {
                        StudentIdCard sic = new StudentIdCard();
                        sic.StudentProgram = studentprogram;
                        sic.IsDistributed = false;
                        sic.PrintCount = 1;
                        _studentIdCardService.Save(sic);
                    }
                    else
                    {
                        studentIdCard.Status = studentIdCard.Status;
                        studentIdCard.StudentProgram = studentIdCard.StudentProgram;
                        studentIdCard.IsDistributed = studentIdCard.IsDistributed;
                        studentIdCard.PrintCount = studentIdCard.PrintCount + 1;
                        _studentIdCardService.Update(studentIdCard);
                    }
                    //newly added code
                    StudentMediaImage mediaStudentImages = new StudentMediaImage();
                    if (studentprogram.StudentImage.Count > 0)
                    {
                        mediaStudentImages = _studentImagesMediaService.GetStudentImageMediaByRefId(
                         studentprogram.StudentImage[0].Id);
                        studentprogram.MediaStudentImage = mediaStudentImages;    
                    }
                    
                    //mediaImageList.Add(mediaStudentImages);
                    generatedStudentPrograms.Add(studentprogram);
                }
                //ViewBag.MediaImages = mediaImageList;
                return View(generatedStudentPrograms);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }
            return View();
        }

        #endregion

        #region Invididual ID Card Report

        [HttpGet]
        public ActionResult IndividualIdCard()
        {
            return View();
        }

        [HttpPost]
        public ActionResult IndividualIdCard(string stdProRoll)
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                ViewBag.ProgramRoll = stdProRoll;
                List<string> stdProRollStringList = new List<string>(); 
                List<StudentProgram> studentProgramList = new List<StudentProgram>();
                
                if (String.IsNullOrWhiteSpace(stdProRoll))
                {
                    throw new MessageException("Please insert program roll no.");
                }
                else
                {
                    stdProRoll = stdProRoll.Replace(",", " ").Trim();
                    var regex = new Regex(@"[ ]{1,}", RegexOptions.None);
                    stdProRoll = regex.Replace(stdProRoll, @",");
                    if (!String.IsNullOrEmpty(stdProRoll))
                    {
                        //stdProRollStringList = stdProRoll.Split(',').Where(x => x.Length == Convert.ToInt32(Constants.PrnNoLength)).ToList();
                        stdProRollStringList = stdProRoll.Split(',').ToList();
                    }
                }
                if (!stdProRollStringList.Any())
                {
                    throw new MessageException("Please insert valid program roll no.");
                }
                else
                {
                    stdProRollStringList = stdProRollStringList.Distinct().ToList();
                    studentProgramList = _studentIdCardService.LoadStudentProgramForIndividualIdCard(_userMenu, stdProRollStringList);
                    ViewBag.ProgramRoll = stdProRoll;
                }

                return View("PrintIdCard", studentProgramList);
            }
            catch (MessageException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (InvalidDataException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        #endregion

        #region ID Card Distribution List Report

        public ActionResult DistributionList()
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                ViewBag.OrganizationId = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu), "Id", "ShortName");
                ViewBag.ProgramId = new SelectList(new List<Program>(), "Id", "Name");
                //ViewBag.ProgramId = new SelectList(_programService.LoadAuthorizedProgram(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "Name");
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
                _logger.Error(ex);
            }

            return View();
        }


        [HttpGet]
        public ActionResult DistributionReport()
        {
            return RedirectToAction("DistributionList");
        }

        [HttpPost]
        public ActionResult DistributionReport(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, int distributionStatus, string[] informationViewList, string[] batchDays = null, string[] batchTime = null)
        {

            ViewBag.programId = programId;
            ViewBag.sessionId = sessionId;
            ViewBag.branchId = branchId;
            ViewBag.campusId = campusId;
            ViewBag.batchName = batchName;
            ViewBag.course = course;
            ViewBag.distributionStatus = distributionStatus;
            ViewBag.informationViewList = informationViewList;
            ViewBag.batchDays = batchDays;
            ViewBag.batchTime = batchTime;
            ViewBag.PageSize = Constants.PageSize;
            return View();
        }
        
        #region Render Data Table

        [HttpPost]
        public JsonResult StudentProgramListForDistribution(int draw, int start, int length, long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, int distributionStatus, string[] informationViewList, string[] batchDays = null, string[] batchTime = null)
        {
            var data = new List<object>();
            int recordsTotal = 0;
            long recordsFiltered = 0;
            if (Request.IsAjaxRequest())
            {
                try
                {
                    _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                    NameValueCollection nvc = Request.Form;
                    string orderBy = "";
                    string orderDir = "";
                    #region OrderBy and Direction
                    if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                    {
                        orderBy = nvc["order[0][column]"];
                        orderDir = nvc["order[0][dir]"];

                    }
                    orderBy = "A.PrnNo";
                    orderDir = "ASC";
                    #endregion

                    recordsTotal = _studentProgramService.GetAuthorizedStudentProgramCount(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, course, 0, distributionStatus);

                    IList<StudentIdCardListDto> studentProgramList = _studentIdCardService.LoadStudentProgram(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, course, 0, start, length, orderBy, orderDir, distributionStatus).ToList();
                    
                    recordsFiltered = recordsTotal;
                    int sl = start + 1;
                    foreach (var c in studentProgramList)
                    {
                        var str = new List<string>();
                        str.Add(sl.ToString());

                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Program Roll":
                                        str.Add(c.PrnNo);
                                        break;
                                    case "Student Name":
                                        str.Add(c.FullName);
                                        break;
                                    case "Father Name":
                                        str.Add(c.FatherName);
                                        break;
                                    case "Mobile Personal":
                                        str.Add(c.Mobile);
                                        break;
                                    case "Mobile Guardian":
                                        str.Add(c.GuardiansMobile1);
                                        break;
                                    case "Branch":
                                        str.Add(c.Branch);
                                        break;
                                    case "Batch":
                                        str.Add(c.Batch);
                                        break;
                                    default:
                                        break;
                                }

                            }
                        }

                        sl++;
                        var printcount = "0";
                        if (c.PrintingCount != null)
                        {
                            printcount = c.PrintingCount.ToString();
                        }
                        str.Add(printcount);
                        data.Add(str);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        #endregion

        #region Export ID Card Distribution List To Excel

        [HttpGet]
        public ActionResult DistributionListToExcel(long programId, long sessionId, long[] branchId, long[] campusId, long[] batchName, long[] course, int distributionStatus, string[] batchDays = null, string[] batchTime = null, string[] informationViewList = null)
        {
            try
            {
                _userMenu = (List<UserMenu>)ViewBag.UserMenu;
                List<string> headerList = new List<string>();
                List<string> columnList = new List<string>();
                List<string> footerList = new List<string>();
                headerList.Add(ErpInfo.OrganizationNameFull);
                headerList.Add("Id Card Distribution List");
                if (informationViewList != null)
                {
                    foreach (var info in informationViewList)
                    {
                        columnList.Add(info);
                    }
                }
                columnList.Add("Count");
                footerList.Add("");
                var distributionXlsList = new List<List<object>>();
                IList<StudentIdCardListDto> studentProgramList = _studentIdCardService.LoadStudentProgram(_userMenu, programId, sessionId, branchId, campusId, batchDays, batchTime, batchName, course, 0, 0, 0, "A.PrnNo", "ASC", distributionStatus).ToList();
                if (studentProgramList.Count > 0)
                {
                    foreach (var c in studentProgramList)
                    {
                        var xlsRow = new List<object>();

                        if (informationViewList != null)
                        {
                            foreach (string informationView in informationViewList)
                            {
                                switch (informationView)
                                {
                                    case "Program Roll":
                                        xlsRow.Add(c.PrnNo);
                                        break;
                                    case "Student Name":
                                        xlsRow.Add(c.FullName);
                                        break;
                                    case "Father Name":
                                        xlsRow.Add(c.FatherName);
                                        break;
                                    case "Mobile Personal":
                                        xlsRow.Add(c.Mobile);
                                        break;
                                    case "Mobile Guardian":
                                        xlsRow.Add(c.GuardiansMobile1);
                                        break;
                                    case "Branch":
                                        xlsRow.Add(c.Branch);
                                        break;
                                    case "Batch":
                                        xlsRow.Add(c.Batch);
                                        break;
                                    default:
                                        break;
                                }

                            }
                        }

                        xlsRow.Add(c.PrintingCount.ToString());
                        distributionXlsList.Add(xlsRow);
                    }
                }

                ExcelGenerator.GenerateExcel(headerList, columnList, distributionXlsList, footerList, "idCard-Distribution-list__" + DateTime.Now.ToString("yyyyMMdd-HHmmss"));
                return View("AutoClose");
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #endregion
    }
}