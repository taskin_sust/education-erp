﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.AttendanceSynchronizerSettingTest
{

    [Trait("Area", "Hr")]
    [Trait("Service", "AttendanceSynchronizerSettingService")]
    public class AttendanceSynchronizerSettingDeleteTest : AttendanceSynchronizerSettingBaseTest
    {
        [Fact]
        public void Should_Delete_Successfully()
        {
            _attendanceSynchronizerSettingFactory.Create().Persist();
            _attendanceSynchronizerSettingFactory.Object.Status = AttendanceSynchronizerSetting.EntityStatus.Delete;
            Assert.Equal(_attendanceSynchronizerSettingService.GetAttendanceSynchronizerSetting(_attendanceSynchronizerSettingFactory.Object.Id).Status,
                _attendanceSynchronizerSettingFactory.Object.Status);
        }

        [Fact]
        public void Should_Return_InvalidData_Exception()
        {
            _attendanceSynchronizerSettingFactory.Create();
            _attendanceSynchronizerSettingFactory.Object.Status = AttendanceSynchronizerSetting.EntityStatus.Delete;
            Assert.Throws<InvalidDataException>(
                () => _attendanceSynchronizerSettingService.Delete(_attendanceSynchronizerSettingFactory.Object));
        }

    }
}
