﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Testing.Values;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using UdvashERP.Dao.Hr;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
namespace UdvashERP.Services.Hr
{

    public interface IMentorHistoryService : IBaseService
    {
        #region Operational Function
        bool Save(long mentorId, MentorHistory hrMentorHistory);
        bool AddMentorHistory(TeamMember member, int pin, string effectiveDate);
        bool UpdateMentorHistory(long id, int pin, string effectiveDate);
        bool DeleteMentorHistory(MentorHistory mentorHistory);
        #endregion

        #region Single Instances Loading Function
        MentorHistory GetMentorHistory(long id);
        MentorHistory GetTeamMemberMentorHistory(DateTime? searchingDateTime = null, long? teamMemberId = null, int? teamMemberPin = null); 

        #endregion

        #region List Loading Function

        List<MentorHistory> LoadMentorHistory(int teamMemberPin, DateTime? searchingDate = null );
        List<long> LoadMentorTeamMemberId(List<long> mentorIdList, DateTime searceingDate, List<long> givenTeamMemberIdList = null, List<int> givenTeamMemberPinList = null, bool isRetired = true, List<int> employmentStatusList = null);
        List<TeamMember> LoadMentorTeamMember(List<long> mentorIdList, DateTime searceingDate, List<long> givenTeamMemberIdList = null, List<int> givenTeamMemberPinList = null, List<int> employmentStatusList = null);

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion

        
    }
    public class MentorHistoryService : BaseService, IMentorHistoryService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private IEmploymentHistoryDao _hrEmploymentHistoryDao; 
        private IMentorHistoryDao _hrMentorHistoryDao;
        private ITeamMemberDao _teamMemberDao;
        public MentorHistoryService(ISession session)
        {
            Session = session;
            _hrEmploymentHistoryDao=new EmploymentHistoryDao(){Session = session};
            _hrMentorHistoryDao = new MentorHistoryDao() { Session = session };
            _teamMemberDao = new TeamMemberDao {Session =  session};
            _commonHelper = new CommonHelper();
        }
        #endregion

        #region Operational Functions

        public bool Save(long mentorId, MentorHistory hrMentorHistory)
        {
            try
            {
                var hrMentorHistoryLog = new MentorHistoryLog();
                object des, dep, org;
                _hrEmploymentHistoryDao.GetCurrentInformations(mentorId, out des, out dep, out org);
                if (des == null || dep == null || org == null)
                    throw new DataNotFoundException("Mentor designation, department or organization not found.");
                hrMentorHistory.MentorDesignation = (Designation) des;
                hrMentorHistory.MentorDepartment = (Department) dep;
                hrMentorHistory.MentorOrganization = (Organization) org;
                _hrMentorHistoryDao.Save(hrMentorHistory);

                hrMentorHistoryLog.Pin = hrMentorHistory.Pin;
                // hrMentorHistoryLog.MentorName = hrMentorHistory.MentorName;
                hrMentorHistoryLog.EffectiveDate = hrMentorHistory.EffectiveDate;
                hrMentorHistoryLog.MemberOrganizationId = hrMentorHistory.Organization.Id;
                hrMentorHistoryLog.MemberDesignationId = hrMentorHistory.Designation.Id;
                hrMentorHistoryLog.MemberDepartmentId = hrMentorHistory.Department.Id;
                hrMentorHistoryLog.TeamMemberId = hrMentorHistory.TeamMember.Id;
                hrMentorHistoryLog.MentorId = hrMentorHistory.Mentor.Id;
                hrMentorHistoryLog.MentorDesignationId = hrMentorHistory.MentorDesignation.Id;
                hrMentorHistoryLog.MentorDepartmentId = hrMentorHistory.MentorDepartment.Id;
                hrMentorHistoryLog.MentorOrganizationId = hrMentorHistory.MentorOrganization.Id;
                hrMentorHistoryLog.MentorHistoryId = hrMentorHistory.Id;
                hrMentorHistoryLog.CreationDate = DateTime.Now;
                hrMentorHistoryLog.CreateBy = GetCurrentUserId();
                Session.Save(hrMentorHistoryLog);
                return true;
            }

            catch (NullReferenceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddMentorHistory(TeamMember member, int pin, string effectiveDate)
        {
           ITransaction trans = null;
            try
            {
                CheckBeforeSaveOrAdd(member, pin, effectiveDate);
                TeamMember mentor = _teamMemberDao.GetMember(pin);
                using (trans = Session.BeginTransaction())
                {
                    var hrMentorHistory = new MentorHistory();
                    object memberDes, memberDep, memberOrg;
                    _hrEmploymentHistoryDao.GetCurrentInformations(member.Id, out memberDes, out memberDep, out memberOrg, Convert.ToDateTime(effectiveDate));
                    if (memberDes == null || memberDep == null || memberOrg == null)
                        throw new InvalidDataException("Member designation or department or organization not found.");
                     object mentorDes, mentorDep, mentorOrg;
                    _hrEmploymentHistoryDao.GetCurrentInformations(mentor.Id, out mentorDes, out mentorDep, out mentorOrg, Convert.ToDateTime(effectiveDate));
                    if (mentorDes == null || mentorDep == null || mentorOrg == null)
                        throw new InvalidDataException("Mentor designation or department or organization not found.");
                    hrMentorHistory.EffectiveDate = Convert.ToDateTime(effectiveDate);
                    hrMentorHistory.TeamMember = member;
                    hrMentorHistory.Department = (Department) memberDep;
                    hrMentorHistory.Designation = (Designation) memberDes;
                    hrMentorHistory.Organization = (Organization) memberOrg;
                    hrMentorHistory.Mentor = mentor;
                    // hrMentorHistory.MentorName = mentor.Name;
                    hrMentorHistory.Pin = mentor.Pin;
                    hrMentorHistory.MentorDepartment = (Department) mentorDep;
                    hrMentorHistory.MentorDesignation = (Designation) mentorDes;
                    hrMentorHistory.MentorOrganization = (Organization) mentorOrg;
                    hrMentorHistory.Status = MentorHistory.EntityStatus.Active;
                    _hrMentorHistoryDao.SaveOrUpdate(hrMentorHistory);
                    SaveMentorHistoryLog(hrMentorHistory);
                    trans.Commit();
                    return true;

                }
            }
            catch (NullReferenceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool UpdateMentorHistory(long id, int pin, string effectiveDate)
        {
            ITransaction trans = null;
            try
            {
                MentorHistory mentorHistory = GetMentorHistory(id);
                CheckBeforeSaveOrAdd(mentorHistory.TeamMember, pin, effectiveDate);
                bool canSaveLog = false;
                using (trans = Session.BeginTransaction())
                {
                    if (mentorHistory.Pin != pin)
                    {
                        TeamMember mentor = _teamMemberDao.GetMember(pin);
                        object mentorDes, mentorDep, mentorOrg;
                        _hrEmploymentHistoryDao.GetCurrentInformations(mentor.Id, out mentorDes, out mentorDep,
                            out mentorOrg, Convert.ToDateTime(effectiveDate));
                        if (mentorDes == null || mentorDep == null || mentorOrg == null)
                            throw new InvalidDataException("Mentor designation or department or organization not found.");
                        canSaveLog = true;

                        mentorHistory.Mentor = mentor;
                        // mentorHistory.MentorName = mentor.Name;
                        mentorHistory.Pin = mentor.Pin;
                        mentorHistory.MentorDepartment = (Department) mentorDep;
                        mentorHistory.MentorDesignation = (Designation) mentorDes;
                        mentorHistory.MentorOrganization = (Organization) mentorOrg;
                    }
                    if (Convert.ToDateTime(effectiveDate).Date != mentorHistory.EffectiveDate.Value.Date)
                    {
                        mentorHistory.EffectiveDate = Convert.ToDateTime(effectiveDate);
                        canSaveLog = true;
                    }

                    _hrMentorHistoryDao.SaveOrUpdate(mentorHistory);

                    if (canSaveLog)
                        SaveMentorHistoryLog(mentorHistory);

                    trans.Commit();
                    return true;

                }
            }
            catch (NullReferenceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
        }

        public bool DeleteMentorHistory(MentorHistory mentorHistory)
        {
            ITransaction trans = null;
            try
            {
                CheckBeforeDelete(mentorHistory.TeamMember);
                using (trans = Session.BeginTransaction())
                {
                    mentorHistory.Status = MentorHistory.EntityStatus.Delete;
                    trans.Commit();
                    return true;

                }
            }
            catch (Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw ex;
            }
        }

        private void CheckBeforeDelete(TeamMember teamMember)
        {
            int numberOfMentorHostory =
                teamMember.MentorHistory.Where(x => x.Status != MentorHistory.EntityStatus.Delete).ToList().Count;
            if (numberOfMentorHostory <= 1)
                throw new DependencyException("Team member must have atleast one Mentor!!");
        }

        public MentorHistory GetMentorHistory(long id)
        {
            return Session.QueryOver<MentorHistory>().Where(x => x.Id == id).SingleOrDefault<MentorHistory>();
        }

        

        private void SaveMentorHistoryLog(MentorHistory hrMentorHistory)
        {
            var hrMentorHistoryLog = new MentorHistoryLog();
            hrMentorHistoryLog.Pin = hrMentorHistory.Pin;
           // hrMentorHistoryLog.MentorName = hrMentorHistory.MentorName;
            hrMentorHistoryLog.EffectiveDate = hrMentorHistory.EffectiveDate;
            hrMentorHistoryLog.MemberOrganizationId = hrMentorHistory.Organization.Id;
            hrMentorHistoryLog.MemberDesignationId = hrMentorHistory.Designation.Id;
            hrMentorHistoryLog.MemberDepartmentId = hrMentorHistory.Department.Id;
            hrMentorHistoryLog.TeamMemberId = hrMentorHistory.TeamMember.Id;
            hrMentorHistoryLog.MentorId = hrMentorHistory.Mentor.Id;
            hrMentorHistoryLog.MentorDesignationId = hrMentorHistory.MentorDesignation.Id;
            hrMentorHistoryLog.MentorDepartmentId = hrMentorHistory.MentorDepartment.Id;
            hrMentorHistoryLog.MentorOrganizationId = hrMentorHistory.MentorOrganization.Id;
            hrMentorHistoryLog.MentorHistoryId = hrMentorHistory.Id;
            hrMentorHistoryLog.CreationDate = DateTime.Now;
            hrMentorHistoryLog.CreateBy = GetCurrentUserId();
            Session.Save(hrMentorHistoryLog);
        }

        private void CheckBeforeSaveOrAdd(TeamMember member, int pin, string effectiveDate)
        {
            if(member == null)
                throw new InvalidDataException("Invalid member!!");
            TeamMember mentorByDate = _teamMemberDao.GetMember(pin, joiningDate: Convert.ToDateTime(effectiveDate));
            if(mentorByDate == null)
                throw new InvalidDataException("This mentor is not available during this time!!");
            if (member.Pin == pin)
                throw new InvalidDataException("Member can't be his\\her own mentor!!");
        }

        #endregion

        #region Single Instances Loading Function
        public MentorHistory GetTeamMemberMentorHistory(DateTime? searchingDateTime = null, long? teamMemberId = null,
            int? teamMemberPin = null)
        {
            try
            {
                if (teamMemberId != null || teamMemberPin != null)
                {
                    DateTime? date = DateTime.Now;

                    if (searchingDateTime != null)
                        date = searchingDateTime;
                    return _hrMentorHistoryDao.GetTeamMemberMentorHistory(date, teamMemberId, teamMemberPin);
                }
                else
                {
                    throw new MessageException("Team Member Id and Team Member Pin is empty");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        #endregion

        #region List Loading Function
        public List<TeamMember> LoadMentorTeamMember(List<long> mentorIdList, DateTime searceingDate, List<long> givenTeamMemberIdList = null, List<int> givenTeamMemberPinList = null, List<int> employmentStatusList = null)
        {
            try
            {
                return _hrMentorHistoryDao.LoadMentorTeamMember(mentorIdList, searceingDate, givenTeamMemberIdList, givenTeamMemberPinList,employmentStatusList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public List<long> LoadMentorTeamMemberId(List<long> mentorIdList, DateTime searceingDate, List<long> givenTeamMemberIdList = null, List<int> givenTeamMemberPinList = null, bool isRetired = true, List<int> employmentStatusList = null )
        {
            try
            {
                return _hrMentorHistoryDao.LoadMentorTeamMemberId(mentorIdList, searceingDate, givenTeamMemberIdList, givenTeamMemberPinList,isRetired, employmentStatusList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }
        public List<MentorHistory> LoadMentorHistory(int teamMemberPin, DateTime? searchingDate = null)
        {
            try
            {
                return _hrMentorHistoryDao.LoadMentorHistory(teamMemberPin, searchingDate).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}
