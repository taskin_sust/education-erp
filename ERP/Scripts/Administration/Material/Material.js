﻿$(document).ready(function () {
    var successMess = "Material Add Successfully";
    var errorMess = "Material Add Failed";
    var updateMess = "Material Update Successfully";
    var updateErrorMess = "Material Update Failed";
    var commonError = "Failed";
    var ajaxError = "Slow Internet Connection";
    // Add button click handler
    $(document).on('click', '.addButton', function () {
        var $template = $('#optionTemplate'),
            $clone = $template
                .clone()
                .removeClass('hide')
                .removeAttr('id')
               .insertBefore($template),
            $option = $clone.find('[name="option[]"]');
        $clone.find("input.name").addClass('extraMaterialNameField');
        $clone.find("input.shortName").addClass('extraMaterialShortNameField');
        // Add new field
    });
    // Remove button click handler
    $(document).on('click', '.removeButton', function () {
        var $row = $(this).parents('.form-group'),
            $option = $row.find('[name="option[]"]');
        // Remove element containing the option
        $row.remove();

    });
    $(document).on('change', '#Organization', function (event) {
        $('#Program').empty();
        $('#Program').append("<option value=''>Select program</option>");
        $('#MaterialType').empty();
        $('#MaterialType').append("<option value=''>Select MaterialType</option>");
        $('#Session').empty();
        $('#Session').append("<option value=''>Select session</option>");
        var organization = $('#Organization').val();
        if (organization.length > 0) {
            $.ajax({
                //url: "/Administration/Material/GetProgram",
                type: "POST",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadProgram",
                data: { organizationIds: organization,isAuthorized:true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (result) {
                    //$('#Program').html(data);
                    //$.unblockUI();

                    $.unblockUI();
                    if (result.IsSuccess) {
                        $.each(result.returnProgramList, function (i, v) {
                            $('#Program').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    else {
                        $.fn.customMessage({
                            displayMessage: result.Message,
                            displayMessageType: "error",
                        });
                    }

                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        }
 
    });

    $(document).on('change', '#Organization', function (event) {
        $('#Program').empty();
        $('#Program').append("<option value=''>Select program</option>");
        $('#MaterialType').empty();
        $('#MaterialType').append("<option value=''>Select MaterialType</option>");
        var organization = $('#Organization').val();
        $.ajax({
            url: "/Administration/Material/GetMaterialType",
            type: "POST",
            data: { organization: organization },
            beforeSend: function () {
                $.blockUI({
                    timeout: 0,
                    message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                });
            },
            success: function (data) {
                $('#MaterialType').html(data);
                $.unblockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (data) {
                $.fn.customMessage({
                    displayMessage: ajaxError,
                    displayMessageType: "error",
                    displayTime: 5000
                });
            }
        });
    });

    $(document).on('change', '#Program', function (event) {
        $('#Session').empty();
        $('#Session').append("<option value=''>Select session</option>");
        var organizationIds = $('#Organization').val();
        var program = $('#Program').val();
            $.ajax({
                //url: "/Administration/Material/GetSession",
                type: "POST",
                url: $("body").attr("data-project-root") + "Administration/CommonAjax/LoadSession",
                data: { organizationIds: organizationIds, programIds: program, isAuthorized: true },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (response) {
                    //$('#Session').html(data);
                    //$.unblockUI();
                    if (response.IsSuccess) {
                        $.each(response.returnSessionList, function (i, v) {
                            $('#Session').append($('<option>').text(v.Text).attr('value', v.Value));
                        });
                    }
                    else {
                        $.fn.customMessage({
                            displayMessage: response.Message,
                            displayMessageType: "error",
                        });
                    }

                },
                complete: function () {
                    $.unblockUI();
                },
                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
    });

    $(document).on('click', '#submitValue', function (bool) {
        $('.field-validation-error').remove();
        var materialName = {};
        var materialShortName = {};
        var ii = 0;
        var organizationId = $('#Organization').val();
        var programId = $('#Program').val();
        var sessionId = $('#Session').val();
        var materialTypeId = $('#MaterialType').val();
        console.log(programId);
        var isSuccess = true;
        $('#materialName .extraMaterialNameField').each(function (index, value) {
            if ($(this).val().length == 0) {
                $(this).css('border-color', 'red');
                var customValidationMssageForEmptyMaterialName = '<span data-valmsg-replace="true" data-valmsg-for="Name" class="field-validation-error"><span for="Name" class="">Material Name</span></span>';
                $(this).after(customValidationMssageForEmptyMaterialName);
                isSuccess = false;
            } else {
                $(this).css('border-color', '#cccccc');
                materialName[index] = $(this).val().toString();
                isSuccess = true;
            }
        });
        $('#materialName .extraMaterialShortNameField').each(function (index, value) {
            if ($(this).val().length == 0) {
                $(this).css('border-color', 'red');
                var customValidationMssageForEmptyMaterialShortName = '<span data-valmsg-replace="true" data-valmsg-for="ShortName" class="field-validation-error"><span for="ShortName" class="">Material Short Name</span></span>';
                $(this).after(customValidationMssageForEmptyMaterialShortName);
                isSuccess = false;
            } else {
                $(this).css('border-color', '#cccccc');
                materialShortName[index] = $(this).val().toString();
                isSuccess = true;
            }
        });

        if (organizationId.length <= 0) {
            window.showErrorMessageBelowCtrl("Organization", "", false);
            window.showErrorMessageBelowCtrl("Organization", "Please Select Organization.", true);
            isSuccess = false;
        }
        if (programId.length <= 0) {
            window.showErrorMessageBelowCtrl("Program", "", false);
            window.showErrorMessageBelowCtrl("Program", "Please Select Program.", true);
            isSuccess = false;
        }
        if (sessionId.length <= 0) {
            window.showErrorMessageBelowCtrl("Session", "", false);
            window.showErrorMessageBelowCtrl("Session", "Please Select Session.", true);
            isSuccess = false;
        }
        if (materialTypeId.length <= 0) {
            window.showErrorMessageBelowCtrl("MaterialType", "", false);
            window.showErrorMessageBelowCtrl("MaterialType", "Please Select Material Type.", true);
            isSuccess = false;
        }
        if (isSuccess == true) {
            console.log(materialName);
            $.ajax({
                url: "/Administration/Material/AddMaterials",
                type: "POST",
                data: { materialNames: JSON.stringify(materialName), materialShortNames: JSON.stringify(materialShortName), programId: programId, sessionId: sessionId, materialTypeId: materialTypeId },
                beforeSend: function () {
                    $.blockUI({
                        timeout: 0,
                        message: '<h1><img src="/Content/Image/ajax-loader.gif" /> Processing...</h1>'
                    });
                },
                success: function (data) {
                    if (data.IsSuccess) {
                        $.fn.customMessage({
                            displayMessage: successMess,
                            displayMessageType: "s",
                            displayTime: 5000
                        });
                        $.unblockUI();
                    } else {
                        $.fn.customMessage({
                            displayMessage: data.Message,
                            displayMessageType: "error",
                            displayTime: 5000
                        });

                        //$('.customMessage').append('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Failed!! Possible Duplicate Material Name Found</div>');
                        $.unblockUI();
                    }
                },
                complete: function () {
                    $.unblockUI();
                },

                error: function (data) {
                    $.fn.customMessage({
                        displayMessage: ajaxError,
                        displayMessageType: "error",
                        displayTime: 5000
                    });
                }
            });
        } else {
            //$.fn.customMessage({
            //    displayMessage: "Empty Input Field",
            //    displayMessageType: "w",
            //    displayTime: 5000
            //});
        }
    });
});

