using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{
    public interface ISpecificationTemplateDao : IBaseDao<SpecificationTemplate, long>
    {
        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        IList<SpecificationTemplateDto> LoadSpecificationTemplateList(int start, int length, string searchString="");
        #endregion

        #region Others Function
        bool IsDuplicateSpecificationTemplate(string specificationTemplateName, long id);
        int SpecificationTemplateRowCount(string searchString);

        #endregion

    }

    public class SpecificationTemplateDao : BaseDao<SpecificationTemplate, long>, ISpecificationTemplateDao
    {
        #region Properties & Object & Initialization
        #endregion

        #region Operational Function
        #endregion

        #region Single Instances Loading Function
        #endregion

        #region List Loading Function

        public IList<SpecificationTemplateDto> LoadSpecificationTemplateList(int start, int length, string searchString="")
        {
            string query = @"select x.SpecificationId,x.SpecificationName,x.SpecificationCriteriaName,x.AssignedItemName" + SpecificationTemplateSearchQuery(searchString);
            IQuery iQuery = Session.CreateSQLQuery(query);
            if (length > 0)
            {
                iQuery.SetFirstResult(start).SetMaxResults(length);
            }
            iQuery.SetResultTransformer(Transformers.AliasToBean<SpecificationTemplateDto>());
            var list = iQuery.List<SpecificationTemplateDto>().ToList();
            return list;
        }

        private string SpecificationTemplateSearchQuery(string searchString)
        {
            var query= @" from(SELECT st.Id 'SpecificationId',st.name 'SpecificationName',st.Status ,ISNULL(STUFF((
							SELECT  ','+ sc.Name
                            FROM UINV_SpecificationCriteria sc
                            WHERE st.Id=sc.SpecificationTemplateId
                            order by sc.Rank                                          
                            FOR XML PATH (''))
                            , 1, 1, ''),'')  AS SpecificationCriteriaName
							, ISNULL(STUFF(
                            (SELECT  ','+ i.Name
                            FROM UINV_Item i
                            WHERE st.Id=i.SpecificationTemplateId
                            order by i.Rank                                       
                            FOR XML PATH (''))
                            , 1, 1, ''),'')  AS AssignedItemName
                            FROM UINV_SpecificationTemplate st 
                            GROUP BY st.Id
							,st.name
                            ,st.Status
							)x 
							where 
							(x.SpecificationName like '%{0}%' or x.SpecificationCriteriaName like '%{0}%' or x.AssignedItemName like '%{0}%')
                            And x.Status=" + SpecificationTemplate.EntityStatus.Active;
            query = string.Format(query, searchString.Trim());
            return query;
        }

        #endregion

        #region Others Function
        public bool IsDuplicateSpecificationTemplate(string specificationTemplateName, long id)
        {
            var rowCount = 0;
            rowCount = id == 0 ? Session.QueryOver<SpecificationTemplate>().Where(x => x.Name == specificationTemplateName && x.Status != SpecificationTemplate.EntityStatus.Delete).RowCount() : Session.QueryOver<SpecificationTemplate>().Where(x => x.Name == specificationTemplateName && x.Status != SpecificationTemplate.EntityStatus.Delete && x.Id != id).RowCount();
            return rowCount > 0;
        }
        public int SpecificationTemplateRowCount(string searchString)
        {
            string query = @"select count(*)" + SpecificationTemplateSearchQuery(searchString);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }
        #endregion

        #region Helper Function

        #endregion


    }
}
