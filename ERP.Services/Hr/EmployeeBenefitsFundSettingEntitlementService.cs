using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Dao.Hr;
using UdvashERP.Dao.UInventory;
namespace UdvashERP.Services.Hr
{
    public interface IEmployeeBenefitsFundSettingEntitlementService : IBaseService
    {
        #region Operational Function

        bool SaveOrUpdate(EmployeeBenefitsFundSettingEntitlement prEmployeeBenefitsFundEntitlement);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
    public class EmployeeBenefitsFundSettingEntitlementService : BaseService, IEmployeeBenefitsFundSettingEntitlementService
    {
        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("HrService");
        #endregion

        #region Propertise & Object Initialization
        private readonly CommonHelper _commonHelper;
        private readonly IEmployeeBenefitsFundSettingEntitlementDao _prEmployeeBenefitsFundEntitlementDao;
        public EmployeeBenefitsFundSettingEntitlementService(ISession session)
        {
            Session = session;
            _commonHelper = new CommonHelper();
            _prEmployeeBenefitsFundEntitlementDao = new EmployeeBenefitsFundSettingEntitlementDao { Session = session };
        }
        #endregion

        #region Operational Functions

        public bool SaveOrUpdate(EmployeeBenefitsFundSettingEntitlement prEmployeeBenefitsFundEntitlement)
        {

            return true;
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function


        #endregion

        #region Helper function



        #endregion
    }
}

