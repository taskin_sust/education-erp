﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.BusinessModel.Dto;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Administration;

namespace UdvashERP.Services.Test.ObjectFactory.Administration
{
    public class CourseFactory : ObjectFactoryBase<Course>
    {
        #region Object Initialization

        private readonly ICourseService _courseService;
        private OrganizationFactory _organizationFactory;
        private ProgramFactory _programFactory;
        private SessionFactory _sessionFactory;

        public CourseFactory(IObjectFactoryBase caller, ISession session)
            : base(caller, session)
        {
            _courseService = new CourseService(session);

            _organizationFactory = new OrganizationFactory(this, session);
            _programFactory = new ProgramFactory(this, session);
            _sessionFactory = new SessionFactory(this, session);
        }

        #endregion

        #region Create

        public CourseFactory Create()
        {
            Object = CreateObject(
                "Course_" + Guid.NewGuid().ToString().Substring(0, 6),
                 DateTime.Now,
                 DateTime.Now,
                 3,
                 1,
                 1,
                 50,
                 10,
                 true,
                 true
                );
            SingleObjectList.Add(Object);
            return this;
        }
        public CourseFactory WithName(string name)
        {
            Object.Name = name;
            return this;
        }
        public CourseFactory WithStartDateAndEndDate(DateTime startDate, DateTime endDate)
        {
            Object.StartDate = startDate;
            Object.EndDate = endDate;
            return this;
        }

        public CourseFactory WithMaxMinSubject(int maxSubject, int officeMinSubject,
            int publicMinSubject)
        {
            Object.MaxSubject = maxSubject;
            Object.OfficeMinSubject = officeMinSubject;
            Object.PublicMinSubject = publicMinSubject;
            return this;
        }

        public CourseFactory WithMinPayment(decimal officeMinPayment, decimal publicMinPayment)
        {
            Object.OfficeMinPayment = officeMinPayment;
            Object.OfficeMinPayment = publicMinPayment;
            return this;
        }

        public CourseFactory WithOrganization(Organization organization)
        {
            Object.Organization = organization;
            return this;
        }
        public CourseFactory WithProgram(Program program)
        {
            Object.Program = program;
            return this;
        }
        public CourseFactory WithSession(Session session)
        {
            Object.RefSession = session;
            return this;
        }
        public CourseFactory WithOrganization()
        {
            var organization = _organizationFactory.Create().Persist().Object;
            Object.Organization = organization;
            return this;
        }
        public CourseFactory WithProgram()
        {
            Program program;
            Organization organization = Object.Organization;
            if (organization != null)
            {
                program=_programFactory.Create().WithOrganization(organization).Persist().Object;
            }
            else
            {
                organization = _organizationFactory.Create().Persist().Object;
                program = _programFactory.Create().WithOrganization(organization).Persist().Object;
            }
            Object.Program = program;
            return this;
        }

        public CourseFactory WithSession()
        {
            var sessionObj = _sessionFactory.Create().Persist().Object;
            Object.RefSession = sessionObj;
            return this;
        }
        public CourseFactory WithCourseSubject(List<CourseSubject> courseSubjects)
        {
            Object.CourseSubjects = courseSubjects;
            return this;
        }
        public CourseFactory CreateMore(int numberOfCourse, Program program, Session session, string[] name, DateTime[] startDate, DateTime[] endDate, int[] maxSubject, int[] officeMinSubject,
            int[] publicMinSubject, decimal[] officeMinPayment, decimal[] publicMinPayment, bool[] officeCompulsory, bool[] publicCompulsory, List<CourseSubject>[] courseSubjects)
        {
            ListStartIndex = ObjectList.Count;
            for (int i = 0; i < numberOfCourse; i++)
            {
                var course = CreateObject(name[i], startDate[i], endDate[i], maxSubject[i], officeMinSubject[i],
                    publicMinSubject[i], officeMinPayment[0], publicMinPayment[0],
                    officeCompulsory[i], publicCompulsory[i]);
                course.RefSession = session;
                course.Program = program;
                course.CourseSubjects = courseSubjects[i];
                ObjectList.Add(course);
            }
            return this;
        }

        private Course CreateObject(string name, DateTime startDate, DateTime endDate, int maxSubject, int officeMinSubject,
            int publicMinSubject, decimal officeMinPayment, decimal publicMinPayment, bool officeCompulsory, bool publicCompulsory)
        {
            var obj = new Course
            {
                Id = 0,
                CreateBy = 1,
                ModifyBy = 1,
                BusinessId = GetPcUserName(),
                Status = Course.EntityStatus.Active,
                Name = name,
                StartDate = startDate,
                EndDate = endDate,
                MaxSubject = maxSubject,
                OfficeMinSubject = officeMinSubject,
                PublicMinSubject = publicMinSubject,
                OfficeMinPayment = officeMinPayment,
                PublicMinPayment = publicMinPayment,
                OfficeCompulsory = officeCompulsory,
                PublicCompulsory = publicCompulsory
            };
            return obj;
        }

        public CourseFactory Persist()
        {
            if (SingleObjectList != null && SingleObjectList.Count > 0)
            {
                SingleObjectList.ForEach(x =>
                {
                    if (x.Id == 0)
                        _courseService.CourseSaveOrUpdate(x);
                });
            }

            if (ObjectList != null && ObjectList.Count > 0)
            {
                ObjectList.ForEach(x =>
                {
                    if (x.Id == 0)
                        _courseService.CourseSaveOrUpdate(x);
                });
            }
            return this;
        }
        #endregion

        #region Others

        #endregion

        #region Cleanup
        public void Cleanup()
        {
            DeleteAll();
            
        }
        #endregion
    }
}
