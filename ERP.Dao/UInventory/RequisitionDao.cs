using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Configuration;
using System.Web.UI;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Impl;
using NHibernate.Loader.Criteria;
using NHibernate.Persister.Entity;
using NHibernate.Transform;
using UdvashERP.BusinessModel.Dto.UInventory;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.UInventory;

namespace UdvashERP.Dao.UInventory
{

    public class ExpandObjectResultSetTransformers : IResultTransformer
    {
        public object TransformTuple(object[] tuple, string[] aliases)
        {
            var expando = new ExpandoObject();
            var dictionary = (IDictionary<string, object>)expando;
            for (int i = 0; i < tuple.Length; i++)
            {
                string alias = aliases[i];
                if (alias != null)
                {
                    dictionary[alias] = tuple[i];
                }
            }
            return expando;
        }
        public IList TransformList(IList collection)
        {
            return collection;
        }
    }

    public interface IRequisitionDao : IBaseDao<Requisition, long>
    {

        #region Operational Function
        #endregion

        #region Single Instances Loading Function


        #endregion

        #region List Loading Function

        IList<Requisition> LoadRequisition(int start, int length, string orderBy, string orderDir, List<long> branchIds,
                                            string itemName, string requiredDate, int status, bool isListForIssue);

        IList<dynamic> RequisitionListByDynamicQuery(string query);


        IList<RequisitionListReportDto> RequisitionReportByList(int start, int length, string orderBy, string orderDir, long[] bIds, long[] itemGroupIds, long[] itemIds,
                                                                    string[] programSession, int[] purposeIds, int[] status, DateTime dateFrom, DateTime dateTo);

        IList<dynamic> RequisitionReportByBranch(int start, int length, string orderBy, string orderDir
                                                , IList<string> branchNameList, long[] branchIds, long[] itemGroupIds
                                                , long[] itemIds, int[] purposeIds, string[] programSession
                                                , int[] status, DateTime dateFrom, DateTime dateTo);

        IList<dynamic> RequisitionReportByProgramSession(int start, int length, string orderBy, string orderDir, long[] bIds
                                                        , long[] itemGroupIds, long[] itemIds, int[] purposeIds, string[] programSessionNameList
                                                        , string[] programSession, int[] status, DateTime dateFrom, DateTime dateTo);

        #endregion

        #region Others Function

        bool CheckDuplicateRequisitionName(out string fieldName, string requisitionNo, long branchId = 0, long id = 0);

        int CountRequisitionSlip(IList<long> authBranchIds, string itemName, string requiredDate, int rqStatus, bool isListForIssue);

        int RequisitionReportByListCount(long[] branchIds, long[] itemGroupIds
                                        , long[] itemIds, string[] programSession, int[] purposeIds
                                        , int[] status, DateTime dateFrom, DateTime dateTo);

        int RequisitionReportByBranchCount(IList<string> bNames, long[] bIds, long[] itemGroupIds, long[] itemIds
                                            , string[] programSession, int[] purposeIds
                                            , int[] status, DateTime dateFrom, DateTime dateTo);
        int RequisitionReportByProgramSessionCount(string[] programSessionNameList, long[] bIds, long[] itemGroupIds
                                                    , long[] itemIds, string[] programSession, int[] purposeIds
                                                    , int[] status, DateTime dateFrom, DateTime dateTo);

        #endregion
    }

    public class RequisitionDao : BaseDao<Requisition, long>, IRequisitionDao
    {
        #region Properties & Object & Initialization

        public static IResultTransformer ExpandoObject;

        #endregion

        public RequisitionDao()
        {
            ExpandoObject = new ExpandObjectResultSetTransformers();
        }

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<Requisition> LoadRequisition(int start, int length, string orderBy, string orderDir, List<long> branchIds,
            string itemName, string requiredDate, int status, bool isListForIssue)
        {
            ICriteria criteria = GetRequisitionCriteria(orderBy, orderDir, branchIds, itemName, requiredDate, status, isListForIssue);
            var list = criteria.SetFirstResult(start).SetMaxResults(length).List<Requisition>();
            return list.Distinct().ToList<Requisition>();
        }

        public IList<RequisitionListReportDto> RequisitionReportByList(int start, int length, string orderBy, string orderDir, long[] bIds, long[] itemGroupIds,
            long[] itemIds, string[] programSession, int[] purposeIds, int[] status, DateTime dateFrom, DateTime dateTo)
        {
            string query = RequisitionReportByListQuery(orderBy, orderDir, bIds, itemGroupIds, itemIds, programSession, purposeIds, status, dateFrom, dateTo);
            var iQuery = Session.CreateSQLQuery(query);
            iQuery.SetFirstResult(start).SetMaxResults(length);
            iQuery.SetResultTransformer(Transformers.AliasToBean<RequisitionListReportDto>());
            var list = iQuery.List<RequisitionListReportDto>().ToList();
            return list;
        }

        public IList<dynamic> RequisitionReportByBranch(int start, int length, string orderBy, string orderDir, IList<string> branchNameList, long[] bIds, long[] itemGroupIds,
            long[] itemIds, int[] purposeIds, string[] programSession, int[] status, DateTime dateFrom, DateTime dateTo)
        {
            var query = GetRequisitionReportByBranchQuery(start, length, orderBy, orderDir, branchNameList, bIds, purposeIds, programSession, itemGroupIds
                                                            , itemIds, status, dateFrom, dateTo);
            IList<dynamic> list = RequisitionListByDynamicQuery(query);
            return list;
        }

        public IList<dynamic> RequisitionReportByProgramSession(int start, int length, string orderBy, string orderDir, long[] bIds, long[] itemGroupIds,
            long[] itemIds, int[] purposeIds, string[] programSessionNameList, string[] programSession, int[] status, DateTime dateFrom, DateTime dateTo)
        {
            var query = GetRequisitionReportByProgramSessionQuery(start, length, orderBy, orderDir, bIds
                                                                    , purposeIds, programSessionNameList, programSession
                                                                    , itemGroupIds, itemIds, status, dateFrom, dateTo);
            IList<dynamic> list = RequisitionListByDynamicQuery(query);
            return list;
        }

        #endregion

        #region Others Function/Count Query

        public int CountRequisitionSlip(IList<long> branchIds, string itemName, string requiredDate, int rqStatus, bool isListForIssue)
        {
            ICriteria criteria = GetRequisitionCriteria(string.Empty, string.Empty, branchIds.ToList(), itemName, requiredDate, rqStatus, isListForIssue, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        public int RequisitionReportByListCount(long[] bIds, long[] itemGroupIds,
            long[] itemIds, string[] programSession, int[] purposeIds, int[] status, DateTime dateFrom, DateTime dateTo)
        {
            string query = RequisitionReportByListQuery("", "", bIds, itemGroupIds, itemIds, programSession, purposeIds, status, dateFrom, dateTo, true);
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetTimeout(2700);
            var count = iQuery.UniqueResult<int>();
            return count;
        }

        public int RequisitionReportByBranchCount(IList<string> bNames, long[] bIds, long[] itemGroupIds, long[] itemIds, string[] programSession,
            int[] purposeIds, int[] status, DateTime dateFrom, DateTime dateTo)
        {
            string query = GetRequisitionReportByBranchQuery(0, 0, "", "", bNames, bIds, purposeIds, programSession,
                                                                itemGroupIds, itemIds, status, dateFrom, dateTo, true);
            var q = Session.CreateSQLQuery(query);
            q.SetTimeout(2700);
            var count = q.UniqueResult<int>();
            return count;
        }

        public int RequisitionReportByProgramSessionCount(string[] programSessionNameList, long[] bIds, long[] itemGroupIds,
            long[] itemIds, string[] programSession, int[] purposeIds, int[] status, DateTime dateFrom, DateTime dateTo)
        {
            var query = GetRequisitionReportByProgramSessionQuery(0, 0, "", "", bIds
                                                                    , purposeIds, programSessionNameList, programSession
                                                                    , itemGroupIds, itemIds, status, dateFrom, dateTo, true);
            var q = Session.CreateSQLQuery(query);
            q.SetTimeout(2700);
            var count = q.UniqueResult<int>();
            return count;
        }

        #endregion

        #region Helper Function

        public bool CheckDuplicateRequisitionName(out string fieldName, string name = null, long branchId = 0, long id = 0)
        {
            fieldName = "";

            var checkValue = false;
            ICriteria criteria = Session.CreateCriteria<Requisition>();
            criteria.Add(Restrictions.Not(Restrictions.Eq("Status", Requisition.EntityStatus.Delete)));
            if (id != 0)
            {
                criteria.Add(Restrictions.Not(Restrictions.Eq("Id", id)));
            }
            if (name != null)
            {
                criteria.Add(Restrictions.Eq("RequisitionNo", name));
                fieldName = "RequisitionNo";
            }
            if (branchId > 0)
            {
                criteria.CreateAlias("Branch", "branch").Add(Restrictions.Eq("branch.Id", branchId));
            }
            var list = criteria.List<Requisition>();
            if (list != null && list.Count > 0)
            {
                checkValue = true;
            }
            return checkValue;
        }

        private static string RequisitionReportByListQuery(string orderBy, string orderDir, long[] branchIds, long[] itemGroupIds, long[] itemIds, string[] programSession
                                                            , int[] purposeIds, int[] status, DateTime dateFrom, DateTime dateTo, bool isCountQuery = false)
        {
            string whereClause = " WHERE ";

            whereClause += " rq.CreationDate >= '" + dateFrom.ToShortDateString() + " 00:00:00' ";
            whereClause += " AND rq.CreationDate <= '" + dateTo.ToShortDateString() + " 23:59:59' ";

            if (status.Any() && status[0] > 0)
            {
                whereClause += " AND rq.RequisionStatus IN( " + string.Join(", ", status) + ")";
            }
            if (branchIds.Any() && branchIds[0] > 0)
            {
                whereClause += " AND b.Id IN( " + string.Join(", ", branchIds) + ")";
            }

            if (purposeIds.Any() && purposeIds[0] > 0)
            {
                whereClause += " AND rqd.Purpose IN( " + string.Join(", ", purposeIds) + ")";
            }

            if (programSession.Any() && programSession[0] != "0")
            {
                string psConditionsStr = "";

                foreach (var ps in programSession)
                {
                    if (string.IsNullOrWhiteSpace(ps.Trim()))
                        continue;
                    string[] psIds = ps.Split(new string[] {"::"}, StringSplitOptions.None);
                    if (psIds.Length == 2)
                    {
                        psConditionsStr += "(p.Id = " + psIds[0] + " AND s.Id = " + psIds[1] + ")";
                    }
                    else
                    {
                        psConditionsStr += "(p.Id IS NULL AND s.Id IS NULL)";
                    }
                    psConditionsStr += " OR ";
                }
                psConditionsStr = psConditionsStr.TrimEnd();
                if (!string.IsNullOrEmpty(psConditionsStr))
                {
                    psConditionsStr = psConditionsStr.Substring(0, psConditionsStr.Length - 2);
                    whereClause += " AND (" + psConditionsStr + ") ";
                }
            }

            if (itemIds.Any() && itemIds[0] > 0)
            {
                whereClause += " AND i.Id IN( " + string.Join(", ", itemIds) + ")";
            }
            if (itemGroupIds.Any() && itemGroupIds[0] > 0)
            {
                whereClause += " AND ig.Id IN( " + string.Join(", ", itemGroupIds) + ")";
            }

            string query = @" FROM UINV_Requisition rq 
                                INNER JOIN UINV_RequisitionDetails rqd ON rq.Id = rqd.RequisitionId 
                                INNER JOIN UINV_Item i ON rqd.ItemId = i.Id 
                                INNER JOIN UINV_ItemGroup ig ON i.ItemGroupId = ig.Id 
                                INNER JOIN Branch b ON rq.BranchId = b.Id 
                                LEFT OUTER JOIN Program p ON rqd.ProgramId = p.Id 
                                LEFT OUTER JOIN Session s ON rqd.SessionId = s.Id ";
            query += whereClause;
            if (!isCountQuery)
            {
                query = "SELECT DISTINCT rq.Id as Id, b.Name as Branch, rq.CreationDate as RequisitionDate," +
                        " rq.RequisitionNo as RequisitionNo, rq.RequisionStatus as RequisitionStatus "
                        + query;
                if (!string.IsNullOrEmpty(orderBy))
                {
                    query += " ORDER BY  rq." + orderBy + " " + orderDir + " ";
                }
                else
                {
                    query += " ORDER BY rq.Id ASC ";
                }
            }
            else
            {
                query = "SELECT COUNT( DISTINCT rq.Id ) " + query;
            }
            return query;
        }

        private ICriteria GetRequisitionCriteria(string orderBy, string orderDir, List<long> branchIds, string itemName, string requiredDate,
            int status, bool isForIssueList, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<Requisition>().Add(Restrictions.Not(Restrictions.Eq("Status", Requisition.EntityStatus.Delete)));
            if (branchIds.Count > 0)
            {
                criteria.CreateAlias("Branch", "branch").Add(Restrictions.Not(Restrictions.Eq("branch.Status", Branch.EntityStatus.Delete)));
                criteria.Add(Restrictions.In("branch.Id", branchIds));
            }
            if (!String.IsNullOrEmpty(itemName))
            {

                criteria.CreateAlias("RequisitionDetails", "reList").Add(Restrictions.Not(Restrictions.Eq("reList.Status", Item.EntityStatus.Delete)));
                criteria.CreateAlias("reList.Item", "item").Add(Restrictions.Not(Restrictions.Eq("item.Status", Item.EntityStatus.Delete)));
                criteria.Add(Restrictions.Like("item.Name", itemName, MatchMode.Anywhere));
            }
            if (!String.IsNullOrEmpty(requiredDate))
            {
                criteria.Add(Restrictions.Eq("RequiredDate", Convert.ToDateTime(requiredDate)));
            }
            if (status > 0)
            {
                if (isForIssueList)
                {
                    switch (status)
                    {
                        case 1:
                            criteria.Add(Restrictions.Eq("RequisionStatus", Convert.ToInt32(RequisitionStatus.Pending)));
                            break;
                        case 2:
                            criteria.Add(Restrictions.Eq("RequisionStatus", Convert.ToInt32(RequisitionStatus.Completed)));
                            break;
                        case 3:
                            criteria.Add(Restrictions.Eq("RequisionStatus", Convert.ToInt32(RequisitionStatus.Partially)));
                            break;
                    }
                }
                else
                {
                    switch (status)
                    {
                        case 1:
                            criteria.Add(Restrictions.Eq("RequisionStatus", Convert.ToInt32(RequisitionStatus.Pending)));
                            break;
                        case 2:
                            criteria.Add(Restrictions.In("RequisionStatus",
                                new List<int>()
                                {
                                    Convert.ToInt32(RequisitionStatus.Partially),
                                    Convert.ToInt32(RequisitionStatus.Completed)
                                }));
                            break;
                        case 3:
                            criteria.Add(Restrictions.Eq("RequisionStatus", Convert.ToInt32(RequisitionStatus.Cancelled)));
                            break;
                    }
                }
            }
            if (!countQuery)
            {
                if (!string.IsNullOrEmpty(orderBy))
                {

                    if (orderBy.Equals("Organization"))
                    {
                        if (branchIds.Count <= 0)
                            criteria.CreateAlias("Branch", "branch");

                        criteria.CreateAlias("branch.Organization", "organization");
                        criteria.AddOrder(orderDir == "ASC"
                            ? Order.Asc("organization.Name")
                            : Order.Desc("organization.Name"));
                    }
                    else if (orderBy.Equals("ItemName"))
                    {
                        if (string.IsNullOrEmpty(itemName))
                        {
                            criteria.CreateAlias("RequisitionDetails", "reList");
                            criteria.CreateAlias("reList.Item", "item");
                        }
                        criteria.AddOrder(orderDir == "ASC"
                            ? Order.Asc("item.Name")
                            : Order.Desc("item.Name"));
                    }
                    else
                    {
                        criteria.AddOrder(orderDir == "ASC"
                            ? Order.Asc(orderBy.Trim())
                            : Order.Desc(orderBy.Trim()));
                    }
                }
            }
            return criteria;
        }

        public IList<dynamic> RequisitionListByDynamicQuery(string query)
        {
            IQuery iQuery = Session.CreateSQLQuery(query);
            IList<dynamic> result = DynamicList(iQuery);
            return result;
        }
        public IList<dynamic> DynamicList(IQuery query)
        {
            var result = query.SetResultTransformer(ExpandoObject)
                        .List<dynamic>();
            return result;
        }

        #region RequisitionReportByBranch Query

        private string GetRequisitionReportByBranchQuery(int start, int length, string orderBy, string orderDir, IList<string> branchNameList
                                                            , long[] branchIds, int[] purposeIds, string[] psArray, long[] itemGroupIds
                                                            , long[] itemIds, int[] requisitionStatus, DateTime dateFrom, DateTime dateTo, bool countQuery = false)
        {

            #region Where Clause

            string whereClause = " WHERE ";

            whereClause += " r.CreationDate >= '" + dateFrom.ToShortDateString() + " 00:00:00' ";
            whereClause += " AND r.CreationDate <= '" + dateTo.ToShortDateString() + " 23:59:59' ";

            if (requisitionStatus.Any() && requisitionStatus[0] > 0)
            {
                whereClause += " AND r.RequisionStatus IN( " + string.Join(", ", requisitionStatus) + ")";
            }
            if (branchIds.Any() && branchIds[0] > 0)
            {
                whereClause += " AND br.Id IN( " + string.Join(", ", branchIds) + ")";
            }

            whereClause += GeneratePurposeClause(purposeIds, "rd");
            whereClause += GenerateProgramSessionClause(psArray, "p", "s");

            if (itemIds.Any() && itemIds[0] > 0)
            {
                whereClause += " AND i.Id IN( " + string.Join(", ", itemIds) + ")";
            }
            if (itemGroupIds.Any() && itemGroupIds[0] > 0)
            {
                whereClause += " AND ig.Id IN( " + string.Join(", ", itemGroupIds) + ")";
            }

            #endregion

            string query = @" FROM (";
            query += "SELECT "
                    + "ig.Name as ItemGroup"
                    + ", i.Name as ItemName"
                    + ", CASE WHEN p.Id IS NULL THEN 'N/A' ELSE (p.ShortName +' '+ s.Name) END as ProgramSession"
                    + ", br.Name as BranchName"
                    + ", SUM(rd.RequiredQuantity) as BranchQuantity"
                    + " FROM UINV_Requisition r "
                    + "INNER JOIN UINV_RequisitionDetails rd ON r.Id = rd.RequisitionId "
                    + "INNER JOIN Branch br ON r.BranchId = br.Id "
                    + "INNER JOIN UINV_Item i ON rd.ItemId = i.Id "
                    + "INNER JOIN UINV_ItemGroup ig ON i.ItemGroupId = ig.Id "
                    + "LEFT OUTER JOIN Program p ON rd.ProgramId = p.Id "
                    + "LEFT OUTER JOIN Session s ON rd.SessionId = s.Id ";

            query += whereClause;
            query += " GROUP BY ig.Name, i.Name, p.Id, p.ShortName, s.Name, br.Name, rd.RequiredQuantity ";
            query += ") src ";

            query += "pivot" +
                     "(" +
                     "SUM(BranchQuantity) " +
                     "FOR BranchName in (" + DynamicBranchListForByBranchReport(branchNameList) + ")" +
                     ")piv ";
            if (!countQuery)
            {
                if (!string.IsNullOrEmpty(orderBy))
                {
                    query += "ORDER BY piv." + orderBy + " " + orderDir + " OFFSET " + start + " ROWS FETCH FIRST " + length +
                         " ROWS ONLY";
                }
                else
                {
                    query += "ORDER BY piv.ItemGroup DESC OFFSET " + start + " ROWS FETCH FIRST " + length +
                         " ROWS ONLY";
                }
                var pivQuery = SumQueryForByBranchReport(branchNameList);
                query = "SELECT *, " + pivQuery + " AS Total " + query;
            }
            else
            {
                query = "SELECT COUNT(*) " + query + ";";
            }
            return query;
        }

        private string DynamicBranchListForByBranchReport(IEnumerable<string> branchNameList)
        {
            string branchNameStr = branchNameList.Aggregate("", (current, t) => current + ("[" + t + "],"));
            return branchNameStr.TrimEnd(',');
        }

        private string SumQueryForByBranchReport(IList<string> branchs)
        {
            string query = "";
            for (int i = 0; i < branchs.Count; i++)
            {
                query += "case when piv.[" + branchs[i] + "] IS NULL THEN 0 ELSE piv.[" + branchs[i] + "] end + ";
            }
            query = query.Trim();
            query = query.Remove(query.Length - 1);
            return query;
        }

        #endregion

        #region RequisitionReportByProgramSession Query

        private string GetRequisitionReportByProgramSessionQuery(int start, int length, string orderBy, string orderDir, long[] branchIds, int[] purposeIds
                                                            , string[] programSessionNameList, string[] programSessionArray, long[] itemGroupIds
                                                            , long[] itemIds, int[] requisitionStatus
                                                            , DateTime dateFrom, DateTime dateTo, bool countQuery = false)
        {
            #region Where Clause

            string whereClause = " WHERE ";

            whereClause += " r.CreationDate >= '" + dateFrom.ToShortDateString() + " 00:00:00' ";
            whereClause += " AND r.CreationDate <= '" + dateTo.ToShortDateString() + " 23:59:59' ";

            if (requisitionStatus.Any() && requisitionStatus[0] > 0)
            {
                whereClause += " AND r.RequisionStatus IN( " + string.Join(", ", requisitionStatus) + ")";
            }
            if (branchIds.Any() && branchIds[0] > 0)
            {
                whereClause += " AND br.Id IN( " + string.Join(", ", branchIds) + ")";
            }

            whereClause += GeneratePurposeClause(purposeIds, "rd");
            whereClause += GenerateProgramSessionClause(programSessionArray, "p", "s");

            if (itemIds.Any() && itemIds[0] > 0)
            {
                whereClause += " AND i.Id IN( " + string.Join(", ", itemIds) + ")";
            }
            if (itemGroupIds.Any() && itemGroupIds[0] > 0)
            {
                whereClause += " AND ig.Id IN( " + string.Join(", ", itemGroupIds) + ")";
            }

            #endregion
            string query = @" FROM (";
            query += "SELECT "
                    + "ig.Name as ItemGroup"
                    + ", i.Name as ItemName"
                    //+ ", p.ShortName +' '+ s.Name as ProgramSession"
                    + ", CASE WHEN p.ShortName Is NULL THEN 'N/A' ELSE p.ShortName +' '+ s.Name END as ProgramSession"
                    + ", SUM(rd.RequiredQuantity) as ProgramSessionQuantity"
                    + " FROM UINV_Requisition r "
                    + "INNER JOIN UINV_RequisitionDetails rd ON r.Id = rd.RequisitionId "
                    + "INNER JOIN Branch br ON r.BranchId = br.Id "
                    + "INNER JOIN UINV_Item i ON rd.ItemId = i.Id "
                    + "INNER JOIN UINV_ItemGroup ig ON i.ItemGroupId = ig.Id "
                    + "LEFT OUTER JOIN Program p ON rd.ProgramId = p.Id "
                    + "LEFT OUTER JOIN Session s ON rd.SessionId = s.Id ";

            query += whereClause;
            query += " GROUP BY ig.Name, i.Name, p.ShortName, s.Name, rd.RequiredQuantity ";
            query += ") src ";

            query += "pivot" +
                     "(" +
                     "SUM(ProgramSessionQuantity) " +
                     "FOR ProgramSession in (" + DynamicProgramSessionForByProgramSessionReport(programSessionNameList) + ")" +
                     ")piv ";
            if (!countQuery)
            {
                if (!string.IsNullOrEmpty(orderBy))
                {
                    query += "ORDER BY piv." + orderBy + " " + orderDir + " OFFSET " + start + " ROWS FETCH FIRST " + length +
                         " ROWS ONLY";
                }
                else
                {
                    query += "ORDER BY piv.ItemGroup OFFSET " + start + " ROWS FETCH FIRST " + length +
                         " ROWS ONLY";
                }
                var pivQuery = SumQueryForByProgramSessionReport(programSessionNameList);
                query = "SELECT *, " + pivQuery + " AS Total " + query;
            }
            else
            {
                query = "SELECT COUNT(*) " + query + ";";
            }
            return query;
        }

        private string DynamicProgramSessionForByProgramSessionReport(IList<string> programSessionNameList)
        {
            string programSessionString = "";

            for (int i = 0; i < programSessionNameList.Count; i++)
            {
                string[] psIds = programSessionNameList[i].Split(':');
                if (psIds.Length == 2)
                {
                    programSessionString += "[" + psIds[0] + " " + psIds[1] + "],";
                }
                else
                {
                    programSessionString += "[N/A],";
                }
            }
            return programSessionString.TrimEnd(',');
        }

        private string SumQueryForByProgramSessionReport(IList<string> programSessionNameList)
        {
            string query = "";
            for (int i = 0; i < programSessionNameList.Count; i++)
            {
                string[] psIds = programSessionNameList[i].Split(':');
                if (psIds.Length == 2)
                {
                    query += "case when piv.[" + psIds[0] + " " + psIds[1] + "] IS NULL THEN 0 ELSE piv.[" + psIds[0] + " " + psIds[1] + "] end + ";
                }
                else
                {
                    query += "case when piv.[N/A] IS NULL THEN 0 ELSE piv.[N/A] end + ";
                }
            }
            query = query.TrimEnd();
            return query.Remove(query.Length - 1);
        }

        #endregion

        private static string GeneratePurposeClause(int[] purposeIds, string cls)
        {
            string whereClause = "";
            if (purposeIds.Any() && !purposeIds.Contains(0))
            {
                if (purposeIds.Contains(-1))
                {
                    var list = purposeIds.ToList<int>();
                    list.RemoveAt(list.IndexOf(-1));
                    purposeIds = list.ToArray();
                    whereClause += purposeIds.Length > 0
                        ? " AND (" + cls + ".Purpose IN( " + string.Join(", ", purposeIds) + ") OR " + cls + ".Purpose IS NULL)"
                        : " AND " + cls + ".Purpose IS NULL ";
                }
                else
                {
                    whereClause += " AND " + cls + ".Purpose IN( " + string.Join(", ", purposeIds) + ")";
                }
            }
            return whereClause;
        }

        private static string GenerateProgramSessionClause(string[] psArray, string programCls, string sessionCls)
        {
            string whereClause = "";
            if (psArray.Any() && psArray[0] != "0")
            {
                string psConditionsStr = "";

                foreach (var ps in psArray)
                {
                    if (string.IsNullOrEmpty(ps.Trim()))
                        continue;
                    string[] psIds = ps.Split(new string[] { "::"}, StringSplitOptions.None);
                    if (psIds.Length == 2)
                    {
                        psConditionsStr += "(" + programCls + ".Id = " + psIds[0] + " AND " + sessionCls + ".Id = " + psIds[1] + ")";
                    }
                    else
                    {
                        psConditionsStr += "(" + programCls + ".Id IS NULL AND " + sessionCls + ".Id IS NULL)";
                    }
                    psConditionsStr += " OR ";
                }
                psConditionsStr = psConditionsStr.TrimEnd();
                if (!string.IsNullOrEmpty(psConditionsStr))
                {
                    psConditionsStr = psConditionsStr.Substring(0, psConditionsStr.Length - 2);
                    whereClause += " AND (" + psConditionsStr + ") ";
                }
            }
            return whereClause;
        }

        #endregion
    }
}
