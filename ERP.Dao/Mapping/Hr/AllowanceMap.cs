using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {
    
    
    public partial class AllowanceMap : BaseClassMap<Allowance,long> {
        
        public AllowanceMap() {
			Table("HR_Allowance");
			LazyLoad();
			References(x => x.TeamMember).Column("TeamMemberId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.DailyAllowanceDate).Column("DailyAllowanceDate");
			Map(x => x.IsApproved).Column("IsApproved");
			HasMany(x => x.AllowanceLog).KeyColumn("AllowanceId");
        }
    }
}
