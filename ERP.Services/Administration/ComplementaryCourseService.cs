﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions;
using log4net;
using Microsoft.AspNet.Identity;
using NHibernate;
using NHibernate.Exceptions;
using UdvashERP.Dao.Administration;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Services.Base;
using UdvashERP.Services.Helper;

namespace UdvashERP.Services.Administration
{


    public interface IComplementaryCourseService : IBaseService
    {
        #region Operational Function
        bool Save(long programId, long sessionId, long courseId, List<long> complementaryCourseIds);
        #endregion

        #region Single Instances Loading Function
        
        #endregion

        #region List Loading Function
        
        #endregion

        #region Others Function
        #endregion

        #region Helper Function
        
        #endregion

        #region Public API
        
        #endregion

        bool IsComplemented(long comId);
    }

    public class ComplementaryCourseService: BaseService, IComplementaryCourseService
    {


        #region Logger
        private readonly ILog _logger = LogManager.GetLogger("AdministrationService");
        private readonly IComplementaryCourseDao _complementaryCourseDao;
        private readonly IComplementaryCourseLogDao _complementaryCourseLogDao;
        private readonly IProgramDao _programDao;
        private readonly ISessionDao _sessionDao;
        private readonly ICourseDao _courseDao;
        private readonly ICommonHelper _commonHelper;
        #endregion

        #region Propertise & Object Initialization


        public ComplementaryCourseService(ISession session)
        {
            Session = session;
            _complementaryCourseDao = new ComplementaryCourseDao { Session = Session };
            _complementaryCourseLogDao = new ComplementaryCourseLogDao { Session = Session };
            _programDao  = new ProgramDao{Session =  Session};
            _sessionDao = new SessionDao { Session = Session };
            _courseDao = new CourseDao { Session = Session };
            _commonHelper = new CommonHelper();
        }
        #endregion

        public bool Save(long programId, long sessionId, long courseId, List<long> complementaryCourseIds)
        {
             ITransaction trans = null;
            try
            {
                //DoBeforSave(programId, sessionId, courseId, complementaryCourseIds);
                Course mainCourse = _courseDao.LoadById(courseId);
                Program program = _programDao.LoadById(programId);
                Session session = _sessionDao.LoadById(sessionId);
                List<ComplementaryCourse> deleteComplementaryCourses = _complementaryCourseDao.DeteteComplementaryCourseList(programId, sessionId, courseId, complementaryCourseIds);
                using (trans = Session.BeginTransaction())
                {

                    if (deleteComplementaryCourses.Count > 0)
                    {
                        foreach (var delComCourse in deleteComplementaryCourses)
                        {
                            ComplementaryCourseLog complementaryCourseLog = new ComplementaryCourseLog();
                            complementaryCourseLog.Status = ComplementaryCourseLog.EntityStatus.Delete;
                            complementaryCourseLog.ProgramId = delComCourse.Program.Id;
                            complementaryCourseLog.SessionId = delComCourse.Session.Id;
                            complementaryCourseLog.CourseId = delComCourse.Course.Id;
                            complementaryCourseLog.CompCourseId = delComCourse.CompCourse.Id;
                            _complementaryCourseLogDao.Save(complementaryCourseLog);

                            _complementaryCourseDao.Delete(delComCourse);
                        }
                    }

                    if (complementaryCourseIds!= null && complementaryCourseIds.Count > 0)
                    {
                        foreach (var comId in complementaryCourseIds)
                        {
                            if (!_complementaryCourseDao.AlreadyHasData(programId, sessionId, courseId, comId))
                            {

                                ComplementaryCourseLog complementaryCourseLog = new ComplementaryCourseLog();
                                complementaryCourseLog.Status = ComplementaryCourseLog.EntityStatus.Active;
                                complementaryCourseLog.ProgramId = programId;
                                complementaryCourseLog.SessionId = sessionId;
                                complementaryCourseLog.CourseId = courseId;
                                complementaryCourseLog.CompCourseId = comId;
                                _complementaryCourseLogDao.Save(complementaryCourseLog);

                                ComplementaryCourse comCourse = new ComplementaryCourse();
                                comCourse.Program = program;
                                comCourse.Session = session;
                                comCourse.Course = mainCourse;
                                comCourse.CompCourse = _courseDao.LoadById(comId);
                                _complementaryCourseDao.Save(comCourse);
                            }
                        }
                    }
                    trans.Commit();
                    return true;

                }
                
            }
            catch(Exception ex)
            {
                if (trans != null && trans.IsActive)
                    trans.Rollback();
                _logger.Error(ex);
                throw;
            }
           
        }

        private void DoBeforSave(long programId, long sessionId, long courseId, List<long> complementaryCourseIds)
        {
            Program program = _programDao.LoadById(programId);
            if (_complementaryCourseDao.IsComplementaryCourse(_commonHelper.ConvertIdToList(program.Organization.Id),
                _commonHelper.ConvertIdToList(programId), _commonHelper.ConvertIdToList(sessionId),courseId))
            {
                throw new Exception("This is a complementary course.");
            }
        }

        public bool IsComplemented(long comId)
        {
            return _complementaryCourseDao.IsComplemented(comId);
        }
    }
}
