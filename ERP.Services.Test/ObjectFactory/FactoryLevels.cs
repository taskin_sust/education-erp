﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UInventory;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using UdvashERP.Services.Test.ObjectFactory.UInventory;
using UdvashERP.Services.Test.ObjectFactory.UserAuth;

namespace UdvashERP.Services.Test.ObjectFactory
{
    public static class FactoryLevels
    {
        private static Dictionary<Type, int> list;
        static FactoryLevels()
        {
            list = new Dictionary<Type, int>();

            int level = 1;

            //Level 1
            list.Add(typeof(OrganizationFactory), level);
            list.Add(typeof(SessionFactory), level);
            list.Add(typeof(SubjectFactory), level);
            list.Add(typeof(ReferrerFactory), level);
            list.Add(typeof(InstituteCategoryFactory), level);            
            list.Add(typeof(AttendanceSynchronizerSettingFactory), level);
            list.Add(typeof(AreaControllersFactory), level);
            list.Add(typeof(MenuGroupFactory), level);
            list.Add(typeof(ItemGroupFactory), level);
            list.Add(typeof(SpecificationTemplateFactory), level);
            list.Add(typeof(SupplierFactory), level);
            list.Add(typeof(BankFactory), level);

            
            //Level 2
            level++;
            list.Add(typeof(ActionsFactory), level);
            list.Add(typeof(YearlyHolidayFactory), level);
            list.Add(typeof(ItemFactory), level);
            list.Add(typeof(SpecificationCriteriaFactory), level);
            list.Add(typeof(BankBranchFactory), level);
            list.Add(typeof(ChildrenAllowanceSettingFactory), level);
            list.Add(typeof(EmployeeBenefitsFundSettingFactory), level);
            list.Add(typeof(ExtradayAllowanceSettingFactory), level);
            list.Add(typeof(FestivalBonusSettingFactory), level);
            list.Add(typeof(NightWorkAllowanceSettingFactory), level);
            list.Add(typeof(OvertimeAllowanceSettingFactory), level);

            //Level 3
            level++;
            list.Add(typeof(ProgramFactory), level);
            list.Add(typeof(BranchFactory), level);
            list.Add(typeof(InstituteFactory), level);
            list.Add(typeof(DesignationFactory), level);
            list.Add(typeof(DepartmentFactory), level);
            list.Add(typeof(ShiftFactory), level);
            list.Add(typeof(HolidaySettingFactory), level);            
            list.Add(typeof(LeaveFactory), level);
            list.Add(typeof(ZoneSettingFactory), level);
            list.Add(typeof(MenuFactory), level);
            list.Add(typeof(CurrentStockSummaryFactory), level);
            list.Add(typeof(QuotationFactory), level);
            list.Add(typeof(GoodsTransferFactory), level);
            list.Add(typeof(SpecificationCriteriaOptionFactory), level);
            list.Add(typeof(SupplierBankDetailsFactory), level);
            list.Add(typeof(SupplierItemFactory), level);
            list.Add(typeof(EmployeeBenefitsFundSettingEntitlementFactory), level);


            //Level 4
            level++;
            list.Add(typeof(CampusFactory), level);
            list.Add(typeof(CourseFactory),level);
            list.Add(typeof(MaterialFactory), level);
            list.Add(typeof(GoodsIssueFactory), level);
            list.Add(typeof(WorkOrderFactory), level);
            list.Add(typeof(GoodsTransferDetailsFactory), level);
            list.Add(typeof(QuotationCriteriaFactory), level);
            list.Add(typeof(RequisitionFactory), level);
            list.Add(typeof(SupplierPriceQuoteFactory), level);
            list.Add(typeof(ProgramSessionItemFactory), level);
            list.Add(typeof(DailySupportAllowanceSettingFactory), level);

            
            //Level 5
            level++;
            list.Add(typeof(BatchFactory), level);
            list.Add(typeof(UserProfileFactory), level);
            list.Add(typeof(AttendanceSynchronizerFactory), level);
            list.Add(typeof(CampusRoomFactory),level);
            list.Add(typeof(CourseSubjectFactory), level);
            list.Add(typeof(GoodsIssueDetailsFactory), level);
            list.Add(typeof(GoodsReceiveFactory), level);
            list.Add(typeof(RequisitionDetailsFactory), level);
            list.Add(typeof(WorkOrderDetailsFactory), level);

            //Level 6
            level++;
            list.Add(typeof(LectureSettingFactory), level);
            list.Add(typeof(TeamMemberFactory), level);
            list.Add(typeof(AttendanceDeviceFactory), level);
            list.Add(typeof(GoodsReceiveDetailsFactory), level);
            list.Add(typeof(WorkOrderCriteriaFactory), level);
            list.Add(typeof(GoodsReturnDetailsFactory), level);
           // list.Add(typeof());

            //Level 7
            level++;
            list.Add(typeof(AcademicInfoFactory), level);
            list.Add(typeof(AllowanceFactory), level);
            list.Add(typeof(AttendanceAdjustmentFactory), level);
            list.Add(typeof(TrainingInfoFactory), level);
            list.Add(typeof(JobExperienceFactory), level);
            list.Add(typeof(ChildrenInfoFactory), level);
            list.Add(typeof(DayOffAdjustmentFactory), level);
            list.Add(typeof(EmploymentHistoryFactory), level);
            list.Add(typeof(HolidayWorkFactory), level);
            list.Add(typeof(NomineeInfoFactory), level);
            list.Add(typeof(LeaveApplicationFactory), level);
            list.Add(typeof(MaritalInfoFactory), level);
            list.Add(typeof(MemberLeaveSummaryFactory), level);
            list.Add(typeof(MemberOfficialDetailFactory), level);
            list.Add(typeof(MentorHistoryFactory), level);
            list.Add(typeof(NightWorkFactory), level);
            list.Add(typeof(OverTimeFactory), level);
            list.Add(typeof(SalaryHistoryFactory), level);
            list.Add(typeof(ShiftWeekendHistoryFactory), level);
            list.Add(typeof(TdsHistoryFactory), level);
            list.Add(typeof(AttendanceDeviceSynchronizationFactory), level);
            list.Add(typeof(GoodsReturnFactory), level);
            list.Add(typeof(DirectWorkOrderFactory), level);
            list.Add(typeof(AllowanceBlockFactory), level);
            list.Add(typeof(MemberLoanApplicationFactory), level);
            list.Add(typeof(MemberLoanRefundFactory), level);
            list.Add(typeof(SpecialAllowanceSettingFactory), level);
            list.Add(typeof(MemberEbfBalanceFactory), level);

            //Level 8
            level++;
            list.Add(typeof(MemberLoanFactory), level);
            list.Add(typeof(MemberLoanCsrFactory), level);
            list.Add(typeof(MemberLoanZakatFactory), level);


            /* //Increase level and add other factory. Keep same level factroy at same block. Divide block by level++; variable increment.
             * level++;
             * list.Add(typeof(Campus),level);
             */

        }

        public static int GetLevel(Type modelType)
        {
            if (list.Keys.Contains(modelType))
                return list[modelType];
            return -1;
        }
    }
}
