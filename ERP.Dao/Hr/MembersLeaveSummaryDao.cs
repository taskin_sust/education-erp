﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.ViewModel.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IMembersLeaveSummaryDao : IBaseDao<MemberLeaveSummary, long>
    {
        #region Operational Function

        #endregion

        #region Single Instances Loading Function
        MemberLeaveSummary GetLeaveSummary(long orgId,long memId, long leaveId);
        #endregion

        #region List Loading Function
        IList<MemberLeaveSummary> LoadByMemberAndYear(long memId, int year);
        IList<TeamMemberTransferLeaveDetailsViewModel> LoadMemberLeaveSumarryForTransfer(long memId, int year);
        List<MemberLeaveSummary> LoadMemberLeaveSummaryByOrgLeaveMemberYear(int year, long organizationId, List<long> teamMemberIdList, List<long> leaveIdList);

        #endregion

        #region Others Function

        bool HasTeamMeberHasAnyLeave(long memId, int year);

        #endregion

        #region Helper Function

        #endregion




        MemberLeaveSummary GetLeaveSummary(long orgId, long memId, long leaveId, int year);

        IList<MemberLeaveSummary> GetLeaveSummary(long orgId, long memId, int year);
        
    }
    public class MembersLeaveSummaryDao : BaseDao<MemberLeaveSummary, long>, IMembersLeaveSummaryDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public MemberLeaveSummary GetLeaveSummary(long orgId,long memId, long leaveId)
        {
            return
                Session.QueryOver<MemberLeaveSummary>()
                    .Where(x => x.TeamMember.Id == memId && x.Leave.Id == leaveId && x.Year == DateTime.Now.Year && x.Organization.Id==orgId)
                    .SingleOrDefault<MemberLeaveSummary>();
        }

        public IList<MemberLeaveSummary> LoadByMemberAndYear(long memId, int year)
        {
            return Session.QueryOver<MemberLeaveSummary>().Where(x => x.TeamMember.Id == memId && x.Year == year && x.Status==MemberLeaveSummary.EntityStatus.Active)
                .List<MemberLeaveSummary>();
        }

        public MemberLeaveSummary GetLeaveSummary(long orgId, long memId, long leaveId, int year)
        {
            return
                Session.QueryOver<MemberLeaveSummary>()
                    .Where(x => x.TeamMember.Id == memId && x.Leave.Id == leaveId && x.Year == year && x.Organization.Id == orgId)
                    .SingleOrDefault<MemberLeaveSummary>();
        }

        public IList<MemberLeaveSummary> GetLeaveSummary(long orgId, long memId, int year)
        {
            return
                Session.QueryOver<MemberLeaveSummary>()
                    .Where(x => x.TeamMember.Id == memId && x.Year == year && x.Organization.Id == orgId)
                    .List<MemberLeaveSummary>();
        }

        public IList<TeamMemberTransferLeaveDetailsViewModel> LoadMemberLeaveSumarryForTransfer(long memId, int year)
        {
            string query = "";
            query = @"Select 
                     mls.Id as LeaveSummayId
                     , l.Name as LeaveName
                     , l.Id as LeaveId
                     , l.IsCarryforward as IsCarryForward
                     , mls.CurrentYearLeave as CurrentYearleave
                     , mls.AvailableBalance as CurrentYearAvailableleave
                    from HR_MemberLeaveSummary as mls
                    inner join HR_Leave as l on l.Id = mls.LeaveId and l.Status = 1
                    where mls.TeamMemberId = " + memId + @"
                    and mls.Year = " + year;
            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<TeamMemberTransferLeaveDetailsViewModel>());
            List<TeamMemberTransferLeaveDetailsViewModel> list = iQuery.List<TeamMemberTransferLeaveDetailsViewModel>().ToList();
            return list;
        }

        public List<MemberLeaveSummary> LoadMemberLeaveSummaryByOrgLeaveMemberYear(int year, long organizationId, List<long> teamMemberIdList, List<long> leaveIdList)
        {
            return Session.QueryOver<MemberLeaveSummary>()
                    .Where(x => x.Year == year && x.Organization.Id == organizationId && x.Status == MemberLeaveSummary.EntityStatus.Active && x.TeamMember.Id.IsIn(teamMemberIdList) && x.Leave.Id.IsIn(leaveIdList))
                    .List<MemberLeaveSummary>().ToList();
        }

        #endregion

        #region Others Function

        public bool HasTeamMeberHasAnyLeave(long memId, int year)
        {
            int leaveSummaryCount = Session.QueryOver<MemberLeaveSummary>().Where(x => x.TeamMember.Id == memId && x.Year == year && x.Status == MemberLeaveSummary.EntityStatus.Active).List<MemberLeaveSummary>().Count;
            if (leaveSummaryCount > 0)
                return true;
            return false;
        }

        #endregion

        #region Helper Function

        #endregion


    }
}
