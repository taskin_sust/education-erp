﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Hr;
using Xunit;

namespace UdvashERP.Services.Test.Hr.HolidayWorkTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "HolidayWork")]
    public class HolidayWorkLoadTest : HolidayWorkBaseTest
    {
        #region Single Instances Load Test

        [Fact]
        public void HolidayWork_Single_Instance_Load_Test()
        {
            GetTeamMember();
            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);
            var entity = _holidayWorkService.GetById(_holidayWorkFactory.Object.Id);
            Assert.NotNull(entity);
            Assert.True(entity.Id > 0);
        }

        #endregion

        #region List Load Test

        [Fact]
        public void MentorHolidayWorkApproval_List_Load_Test()
        {
            GetTeamMember();

            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            var entity = _holidayWorkService.LoadMentorHolidayWorkApproval(mentor, (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture));
            Assert.Equal(entity.Count, 1);
        }

        [Fact]
        public void HrHolidayWorkApproval_Load_Test_Should_Successful()
        {
            GetTeamMember();
            //var userMenu = BuildUserMenu(_organizationFactory.SingleObjectList);
            var employment = teamMemberFactory.Object.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { organizationFactory.Object }, null, new List<Branch>() { employment.Campus.Branch });
            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            

            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            var entity = _holidayWorkService.LoadHrHolidayWorkApproval(userMenu, (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                    commonHelper.ConvertIdToList(organizationFactory.Object.Id));
            Assert.Equal(entity.Count, 1);
        }
        
        [Fact]
        public void HrHolidayWorkApprovalHistory_Load_Test_Should_Successful()
        {
            GetTeamMember();
            var employment = teamMemberFactory.Object.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { organizationFactory.Object }, null, new List<Branch>() { employment.Campus.Branch });
            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = Convert.ToDateTime(DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1)).Date;
            
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            var entity = _holidayWorkService.LoadHrHolidayWorkApprovalHistory(0, 10, "Pin", "ASC", userMenu, (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                    organizationFactory.Object.Id, null, null, null);
            Assert.Equal(entity.Count, 1);
        }
        
        [Fact]
        public void HrHolidayWorkApprovalHistoryRowCount_Test_Should_Successfully_Done()
        {
            GetTeamMember();
            var employment = teamMemberFactory.Object.EmploymentHistory.LastOrDefault(x => x.Status == EmploymentHistory.EntityStatus.Active);
            var userMenu = BuildUserMenu(new List<Organization>() { organizationFactory.Object }, null, new List<Branch>() { employment.Campus.Branch });
            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = Convert.ToDateTime(DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1)).Date;
            
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            var entity = _holidayWorkService.GetHrHolidayWorkApprovalHistoryRowCount(userMenu, (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                    organizationFactory.Object.Id, null, null, null);
            Assert.Equal(entity, 1);
        }
        
        #endregion

        #region Business Logic Test

        [Fact]
        public void InvalidDataException_MentorHolidayWorkApproval_Load_For_Date_Range_Order()
        {
            GetTeamMember();
            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<InvalidDataException>(() => _holidayWorkService.LoadMentorHolidayWorkApproval(mentor, (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    (DateTime)_holidayWorkFactory.Object.HolidayWorkDate.Value.AddDays(-5),
                                                                                    teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture)));
        }
        
        [Fact]
        public void InvalidDataException_MentorHolidayWorkApproval_Load_For_Mentor_Null()
        {
            GetTeamMember();
            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<NullReferenceException>(() => _holidayWorkService.LoadMentorHolidayWorkApproval(null, (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    (DateTime)_holidayWorkFactory.Object.HolidayWorkDate.Value.AddDays(1),
                                                                                    teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture)));
        }
        
        [Fact]
        public void ArgumentNullException_MentorHolidayWorkApproval_Load_For_Mentor_Empty()
        {
            GetTeamMember();
            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<InvalidDataException>(() => _holidayWorkService.LoadMentorHolidayWorkApproval(new TeamMember(), (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture)));
        }
        
        [Fact]
        public void InvalidDataException_HrHolidayWorkApproval_Load_For_Date_Range_Order()
        {
            GetTeamMember();
            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<InvalidDataException>(() => _holidayWorkService.LoadHrHolidayWorkApproval(userMenu, (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    (DateTime)_holidayWorkFactory.Object.HolidayWorkDate.Value.AddDays(-5),
                                                                                    teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                    commonHelper.ConvertIdToList(organizationFactory.Object.Id)));
        }
        
        [Fact]
        public void ArgumentNullException_HrHolidayWorkApproval_Load_For_UserMenu_Null()
        {
            GetTeamMember();
            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<ArgumentNullException>(() => _holidayWorkService.LoadHrHolidayWorkApproval(null, (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    (DateTime)_holidayWorkFactory.Object.HolidayWorkDate.Value.AddDays(-5),
                                                                                    teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                    commonHelper.ConvertIdToList(organizationFactory.Object.Id)));
        }
        
        [Fact]
        public void InvalidDataException_HrHolidayWorkApproval_Load_For_UserMenu_Null()
        {
            GetTeamMember();
            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<InvalidDataException>(() => _holidayWorkService.LoadHrHolidayWorkApproval(new List<UserMenu>(), (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    (DateTime)_holidayWorkFactory.Object.HolidayWorkDate.Value.AddDays(-5),
                                                                                    teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                    commonHelper.ConvertIdToList(organizationFactory.Object.Id)));
        }
        
        [Fact]
        public void InvalidDataException_HolidayWork_Approval_History_Load_For_Date_Range_Order()
        {
            GetTeamMember();

            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = Convert.ToDateTime(DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1)).Date;
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<InvalidDataException>(() => _holidayWorkService.LoadHrHolidayWorkApprovalHistory(0, 10, "Pin", "ASC", userMenu, (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                                            (DateTime)_holidayWorkFactory.Object.HolidayWorkDate.Value.AddDays(-5),
                                                                                                            teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                                            organizationFactory.Object.Id, null, null, null));
        }
        
        [Fact]
        public void InvalidDataException_HolidayWork_Approval_History_Load_For_UserMenu_Null()
        {
            GetTeamMember();

            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = Convert.ToDateTime(DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1)).Date;
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<InvalidDataException>(() => _holidayWorkService.LoadHrHolidayWorkApprovalHistory(0, 10, "Pin", "ASC", new List<UserMenu>(), (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                                            (DateTime)_holidayWorkFactory.Object.HolidayWorkDate, teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                                            organizationFactory.Object.Id, null, null, null));
        }
        
        [Fact]
        public void ArgumentNullException_HolidayWork_Approval_History_Load_For_UserMenu_Null()
        {
            GetTeamMember();

            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = Convert.ToDateTime(DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1)).Date;
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<ArgumentNullException>(() => _holidayWorkService.LoadHrHolidayWorkApprovalHistory(0, 10, "Pin", "ASC", null, (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                                            (DateTime)_holidayWorkFactory.Object.HolidayWorkDate, teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                                            organizationFactory.Object.Id, null, null, null));
        }
        
        [Fact]
        public void InvalidDataException_HolidayWork_Approval_History_Row_Count_For_DateRange_Order()
        {
            GetTeamMember();

            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = Convert.ToDateTime(DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1)).Date;
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<InvalidDataException>(() => _holidayWorkService.GetHrHolidayWorkApprovalHistoryRowCount(userMenu,
                                                                                                                (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                                                (DateTime)_holidayWorkFactory.Object.HolidayWorkDate.Value.AddDays(-5),
                                                                                                                teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                                                organizationFactory.Object.Id, null, null, null));
        }
        
        [Fact]
        public void InvalidDataException_HolidayWork_Approval_History_Row_Count_For_UserMenu_Null()
        {
            GetTeamMember();

            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = Convert.ToDateTime(DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1)).Date;
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<InvalidDataException>(() => _holidayWorkService.GetHrHolidayWorkApprovalHistoryRowCount(new List<UserMenu>(),
                                                                                                                (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                                                (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                                                teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                                                organizationFactory.Object.Id, null, null, null));
        }
        
        [Fact]
        public void ArgumentNullException_HolidayWork_Approval_History_Row_Count_For_UserMenu_Null()
        {
            GetTeamMember();

            _attendanceAdjustmentFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _attendanceAdjustmentFactory.Object.Date = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1);
            _attendanceAdjustmentFactory.Object.AdjustmentStatus = (int)AttendanceAdjustmentStatus.Approved;
            _attendanceAdjustmentService.SaveOrUpdate(_attendanceAdjustmentFactory.SingleObjectList);

            _holidayWorkFactory.Create().WithTeamMember(teamMemberFactory.Object);
            _holidayWorkFactory.Object.HolidayWorkDate = Convert.ToDateTime(DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 1)).Date;
            var userMenu = BuildUserMenu(organizationFactory.SingleObjectList);
            var mentor = _holidayWorkFactory.Object.TeamMember.MentorHistory.LastOrDefault().Mentor;
            _holidayWorkService.SaveHolidayWorkApproval(_holidayWorkFactory.SingleObjectList, mentor, true, false, userMenu);

            Assert.Throws<ArgumentNullException>(() => _holidayWorkService.GetHrHolidayWorkApprovalHistoryRowCount(null, (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    (DateTime)_holidayWorkFactory.Object.HolidayWorkDate,
                                                                                    teamMemberFactory.Object.Pin.ToString(CultureInfo.InvariantCulture),
                                                                                    organizationFactory.Object.Id, null, null, null));
        }
        
        #endregion
    }
}
