﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Test.Hr.ShiftTest;
using UdvashERP.Services.Test.ObjectFactory.Administration;
using UdvashERP.Services.Test.ObjectFactory.Hr;
using Xunit;

namespace UdvashERP.Services.Test.AdminisTration.BatchTest
{
    [Trait("Area", "Administration")]
    [Trait("Service", "Batch")]
    public class BatchSaveTest : BatchBaseTest
    {
        #region Basic Test

        [Fact]
        public void Save_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _batchService.Save(null));
        }

        [Fact]
        public void Should_Save_Successfully()
        {
            var batchFactory =
                _batchFactory.Create().WithNameAndRank(_name, 1)
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1)
                    .Persist();
            Batch batch = _batchService.GetBatch(batchFactory.Object.Id);
            Assert.NotNull(batch);
            Assert.True(batch.Id > 0);
        }

        [Fact]
        public void Save_Should_Check_Duplicate_Name_Entry()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, 1).Persist();
            var batch1 = batchFactory.Object;
            var batchFactory2 =
                _batchFactory.Create()
                    .WithProperties(batch1.Organization, batch1.Program, batch1.Session, batch1.Branch, batch1.Campus, _days, _startTime, _endTime, 100, 1, 1);
            Assert.Throws<DuplicateEntryException>(() => batchFactory2.Persist());
        }

        [Fact]
        public void Save_Should_Check_Empty_Day()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, "", _startTime, _endTime, 100, 1, (int)Gender.Male);
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }

        [Fact]
        public void Save_Should_Check_Empty_StartTime()
        {
            var batchFactory =
                _batchFactory.Create().WithNameAndRank(_name, 1)
                    .WithProperties(true, true, true, true, true, _days, null, _endTime, 100, 1, (int)Gender.Male);
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }

        [Fact]
        public void Save_Should_Check_Empty_EndTime()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, null, 100, 1, (int)Gender.Male);
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }

        [Fact]
        public void Save_Should_Check_Empty_Name()
        {
            var batchFactory =
                _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, (int)Gender.Male);
            batchFactory.Object.Name = "";
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }

        [Fact]
        public void Save_Should_Check_Empty_Capacity()
        {
            var batchFactory =
                _batchFactory.Create().WithNameAndRank(_name, 1)
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, null, 1, (int)Gender.Male);
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }

        [Fact]
        public void Save_Should_Check_Empty_VersionOfStudy()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, null, (int)Gender.Male);
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }

        [Fact]
        public void Save_Should_Check_Empty_Gender()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, null);
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }

        [Fact]
        public void Save_Should_Check_Empty_Program()
        {
            var batchFactory =
                _batchFactory.Create()
                    .WithProperties(true, false, true, true, true, _days, _startTime, _endTime, 100, 1, (int)Gender.Male);
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }

        [Fact]
        public void Save_Should_Check_Empty_Branch()
        {
            var batchFactory =
                _batchFactory.Create()
                    .WithProperties(true, true, true, false, true, _days, _startTime, _endTime, 100, 1, (int)Gender.Male);
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }

        [Fact]
        public void Save_Should_Check_Empty_Session()
        {
            var batchFactory =
                _batchFactory.Create()
                    .WithProperties(true, true, false, true, true, _days, _startTime, _endTime, 100, 1, (int)Gender.Male);
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }

        [Fact]
        public void Save_Should_Check_Empty_Campus()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, _startTime, _endTime, 100, 1, (int)Gender.Male);

            batchFactory.Object.Campus = null;
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }
        #endregion

        #region Business Logic

        [Fact]
        public void Should_check_datetime_properly()
        {
            var batchFactory = _batchFactory.Create()
                    .WithProperties(true, true, true, true, true, _days, DateTime.Now.Add(new TimeSpan(10, 10, 10)), DateTime.Now, 100, 1, (int)Gender.Male);
            Assert.Throws<MessageException>(() => batchFactory.Persist());
        }
        #endregion
    }
}
