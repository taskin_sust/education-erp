﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml.Serialization;
using log4net;
using Microsoft.AspNet.Identity;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Exam;
using UdvashERP.BusinessModel.Entity.Exam;
using UdvashERP.BusinessModel.Entity.Sms;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.ExamModuleView;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.BusinessRules.Exam;
using UdvashERP.MessageExceptions;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Exam;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Sms;
using UdvashERP.Services.Students;
using UdvashERP.Services.UserAuth;
using InvalidDataException = UdvashERP.MessageExceptions.InvalidDataException;

namespace UdvashERP.Areas.Exam.Controllers
{
    [Authorize]
    [AuthorizeAccess]
    [UerpArea("Exam")]
    public class MarksUploadController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("ExamArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private readonly ISessionService _sessionService;
        private readonly IProgramService _programService;
        private readonly IBranchService _branchService;
        private readonly ICourseService _courseService;
        private readonly IExamsService _examsService;
        private readonly IUserService _userService;
        private readonly IStudentProgramService _studentProgram;
        private readonly IExamsStudentMarksService _examsStudentMarksService;
        private readonly IOrganizationService _organizationService;
        private readonly ISmsService _smsService;
        private readonly IStudentExamService _studentExamService;
        private readonly IExamMcqQuestionSetService _examMcqQuestionSetService;
        private readonly CommonHelper _commonHelper;
        private List<UserMenu> _userMenu;
        List<MarksEntryReturnMessage> _returnVal = new List<MarksEntryReturnMessage>();
        List<TemporarySms> _tempListData = new List<TemporarySms>();

        public MarksUploadController()
        {
            var nHsession = NHibernateSessionFactory.OpenSession();

            _programService = new ProgramService(nHsession);
            _sessionService = new SessionService(nHsession);
            _courseService = new CourseService(nHsession);
            _examsService = new ExamsService(nHsession);
            _userService = new UserService(nHsession);
            _studentProgram = new StudentProgramService(nHsession);
            _examsStudentMarksService = new ExamsStudentMarksService(nHsession);
            _organizationService = new OrganizationService(nHsession);
            _studentExamService = new StudentExamService(nHsession);
            _examMcqQuestionSetService = new ExamMcqQuestionSetService(nHsession);
            _branchService = new BranchService(nHsession);
            _commonHelper = new CommonHelper();
            _smsService = new SmsService(nHsession);
        }

        public List<MarksEntryReturnMessage> ReturnValueList
        {
            get { return _returnVal; }
        }

        #endregion

        #region Marks Upload
        // GET: Exam/MarksUpload
        public ActionResult Index()
        {
            _userMenu = (List<UserMenu>)ViewBag.UserMenu;
            ViewBag.OrganizationList = new SelectList(_organizationService.LoadAuthorizedOrganization(_userMenu).OrderBy(x => x.Rank).ToList(), "Id", "ShortName");
            ViewBag.BoardExamList = new SelectList(_studentExamService.LoadBoardExam(true).ToList(), "Id", "Name");
            ViewBag.ProgramList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.SessionList = new SelectList(string.Empty, "Value", "Text");
            ViewBag.CourseList = new SelectList(string.Empty, "Value", "Text");

            return View();
            // return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UploadXml()  
        {
            try
            {
                var xmlFile = Request.Files[0];
                var formData = Request.Form;
                var marksUploadViewModel = new MarksUploadViewModel
                {
                    Organization = Convert.ToInt64(formData["Organization"]),
                    Program = Convert.ToInt64(formData["Program"]),
                    Session = Convert.ToInt64(formData["Session"]),
                    Course = Convert.ToInt64(formData["Course"]),
                    BoardExam = Convert.ToInt32(formData["BoardExam"]),
                    ExamCode = formData["ExamCode"],
                    ExamId = Convert.ToInt64(formData["ExamId"]),
                    FileField = xmlFile
                };

                if (ModelState.IsValid)
                {
                    if (marksUploadViewModel.FileField.ContentLength > 0 &&
                        marksUploadViewModel.FileField.ContentType.ToLower() == "text/xml")
                    {
                        try
                        {
                            List<MarksEntryReturnMessage> returnVal = UploadStudentMarks(marksUploadViewModel);

                            var returnMessageSuccessList = returnVal.Where(x => x.mesageType == 0).ToList();
                            var returnMessageSuccess = returnMessageSuccessList.Select(x => x.Prn).Distinct().ToList().Count;
                            var returnMessageSuccessUpdateList = returnVal.Where(x => x.mesageType == 1).ToList();
                            var returnMessageSuccessUpdate = returnMessageSuccessUpdateList.Select(x => x.Prn).Distinct().ToList().Count;

                            var successPrnListUpdate = string.Join(", ", returnMessageSuccessUpdateList.Select(x => x.Prn).Distinct().ToList());
                            var returnMessageFailList = returnVal.Where(x => x.mesageType == 2).ToList();
                            var returnMessageFail = returnMessageFailList;
                           
                            return Json(new
                            {
                                isSuccess = true,
                                returnMessageSuccess = returnMessageSuccess,
                                returnMessageSuccessUpdate = returnMessageSuccessUpdate,
                                successPrnListUpdate = successPrnListUpdate,
                                returnMessageFail = returnMessageFail                                
                            });
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("Inner Xml Error");
                            _logger.Error(ex);
                            SaveXmlFile(marksUploadViewModel.FileField);
                            return Json(new Response(false, " Invalid Xml File"));
                        }
                    }
                    else
                    {
                        return Json(new Response(false, " Invalid Xml File"));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Outer Xml Error");
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
            return Json(new Response(false, WebHelper.CommonErrorMessage));
        }

        public ActionResult MarksUploadLog()
        {
            try
            {
                return View();
            }
            
            catch (Exception ex)
            {
                ViewBag.MemberHasInfo = false;
                _logger.Error(ex);
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult MangeMarkdUploadLog(int draw, int start, int length, string markdUploadLogText)
        {
            int recordsTotal = 0;
            int recordsFiltered = 0;
            var data = new List<object>();
            try
            {
                NameValueCollection nvc = Request.Form;
                int sr = start + 1;
                string orderBy = "";
                string orderDir = "";

                #region OrderBy and Direction

                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "1":
                            orderBy = "Roll";
                            break;
                        case "2":
                            orderBy = "RegNo";
                            break;
                        case "3":
                            orderBy = "Board";
                            break;
                        case "4":
                            orderBy = "BarCode";
                            break;
                        case "5":
                            orderBy = "QrCode";
                            break;
                        case "6":
                            orderBy = "Gender";
                            break;
                        case "7":
                            orderBy = "Version";
                            break;
                        case "8":
                            orderBy = "Image";
                            break;
                        case "9":
                            orderBy = "Answer";
                            break;
                        case "10":
                            orderBy = "ObtainMark";
                            break;
                        case "11":
                            orderBy = "Message";
                            break;
                        case "12":
                            orderBy = "prnNo";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }


                #endregion

                recordsTotal = _examsStudentMarksService.CountExamsSudentMarksUploadData(markdUploadLogText);

                List<ExamsStudentMarksUploadRawData> listRawData = _examsStudentMarksService.LoadExamsSudentMarksUploadData(draw, start, length,orderBy, orderDir.ToUpper(), markdUploadLogText);//_examMcqQuestionSetService.LoadExamMcqQuestionSet(authorizeOrgIds,
                   
                recordsFiltered = recordsTotal;

                foreach (var c in listRawData)
                {
                    var str = new List<string>();
                    str.Add(c.Roll);
                    str.Add(c.RegNo);
                    str.Add(c.Board);
                    str.Add(c.BarCode);
                    str.Add(c.QrCode);
                    str.Add(c.Gender);
                    str.Add(c.Version);
                    str.Add(c.Image);
                    str.Add(c.Eiin);
                    str.Add(c.Answer);
                    str.Add(c.ObtainMark.ToString());
                    str.Add(c.Message);
                    str.Add(c.PrnNo);
                    data.Add(str);
                }

            }
            catch(Exception ex)
            {
                _logger.Error(ex);
            }

            return Json(new
            {
                draw = draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsFiltered,
                start = start,
                length = length,
                data = data
            });
        }

        #endregion

        #region Ajax Function
             [
             HttpPost]
        public ActionResult AjaxRequestForExamCode(string query, string programId, string sessionId, string courseId)
        {
            //bool? examTypeValue = (isMcq != "") ? Convert.ToBoolean(isMcq) : (bool?)null;
            try
            {
                if (!String.IsNullOrEmpty(programId) && !String.IsNullOrEmpty(sessionId) &&
                    !String.IsNullOrEmpty(courseId))
                {
                    programId = programId.Trim();
                    sessionId = sessionId.Trim();
                    courseId = courseId.Trim();
                }
                else
                {
                    programId = "-1";
                    sessionId = "-1";
                    courseId = "-1";
                }
                var examsList = new SelectList(_examsService.GetExamByProgramSessionCourseAutoComplete(
                    query, Convert.ToInt64(programId), Convert.ToInt64(sessionId), Convert.ToInt64(courseId), true), "Id", "Code");
                List<SelectListItem> selectExamList = examsList.ToList();
                return Json(new { returnList = selectExamList, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        [HttpPost]
        public ActionResult AjaxRequestForExamInformation(long id)
        {
            try
            {
                Exams exam = _examsService.GetExamById(id);

                var examinfo = new Dictionary<string, string>();

                if (exam != null)
                {
                    examinfo.Add("Name", exam.Name);

                    var examDetails = exam.ExamDetails;
                    var firstExamDetails = examDetails.FirstOrDefault();
                    
                    return Json(new { returnList = examinfo, IsSuccess = true });
                }
                else
                {
                    return Json(new { returnList = examinfo, IsSuccess = false });
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }
        #endregion

        #region Helper Function
        private List<MarksEntryReturnMessage> UploadStudentMarks(MarksUploadViewModel marksUploadViewModel)
        {
            IList<ExamsStudentMarks> studentMarksDetailsList = new List<ExamsStudentMarks>();
            Exams examinformation = _examsService.GetExamById(marksUploadViewModel.ExamId);
            if (examinformation != null)
            {
                if (examinformation.IsMcq && (examinformation.IsBanglaVersion || examinformation.IsEnglishversion))
                {
                    List<ExamsDetails> examsDetailses = examinformation.ExamDetails.Where(x => x.ExamType == ExamType.Mcq && x.SubjectType!=SubjectType.Multiple).OrderByDescending(y => y.IsCompulsary).ToList();                   
                    var totalQuestion = examsDetailses.Sum(item => item.TotalQuestion);
                    var maxSubject = examinformation.McqMaxSubjectCount;
                    int subjectType = examsDetailses.FirstOrDefault().SubjectType;
                    var isAcceptPartial = examinformation.IsAcceptPartialAnswer;

                    #region IsExamDetails Exits

                    if (examsDetailses.Any())
                    {
                        var xRoot = new XmlRootAttribute();
                        xRoot.ElementName = "StudentList";
                        xRoot.Namespace = "http://www.osl.one";
                        xRoot.IsNullable = true;

                        var serializer = new XmlSerializer(typeof(List<OmrStudent>), xRoot);
                        var reader = new StreamReader(marksUploadViewModel.FileField.InputStream);
                        var studentList = (List<OmrStudent>)serializer.Deserialize(reader);

                        //XElement element = XElement.Load(new StreamReader(marksUploadViewModel.FileField.InputStream));
                        //var students = element.Descendants("Student");
                       
                        //var smsXmlList = new List<MarksEntryByXmlSms>();

                        var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                        var authorizeOrg = _organizationService.LoadAuthorizedOrganization(userMenu);
                        var authorizeProgram = _programService.LoadAuthorizedProgram(userMenu);
                        var authorizeSession = _sessionService.LoadAuthorizedSession(userMenu);
                        var authorizeBranch = _branchService.LoadAuthorizedBranch(userMenu);

                        #region Looping Through Xml Student Obj

                        foreach (var student in studentList)
                        {
                            var examsStudentMarksUploadRawData = new ExamsStudentMarksUploadRawData();

                             var studentBoardRoll = student.Roll.Trim();
                             var studentBoardRegNo = student.RegistrationNo.Trim();
                             var board = student.Board.Trim();
                             var barCode = student.BarCode.Trim();
                             var qrCode = student.QrCode.Trim();
                             var gender = student.Gender.Trim();
                             var version = student.Version.Trim();
                             var image = student.Image.Trim();
                             var eiin = student.Eiin.Trim();
                             var answer = student.Answer.Trim();

                             if (!string.IsNullOrEmpty(studentBoardRoll)
                                 && !string.IsNullOrEmpty(studentBoardRegNo)
                                 && !string.IsNullOrEmpty(board)
                                 && !string.IsNullOrEmpty(barCode)
                                 //&& !string.IsNullOrEmpty(qrCode)
                                 //&& !string.IsNullOrEmpty(gender)
                                 //&& !string.IsNullOrEmpty(version)
                                 //&& !string.IsNullOrEmpty(image)
                                 //&& !string.IsNullOrEmpty(eiin)
                                 //&& !string.IsNullOrEmpty(answer)
                                 )
                            {
                                try
                                {
                                    #region Make log Object

                                    examsStudentMarksUploadRawData.Roll = studentBoardRoll;
                                    examsStudentMarksUploadRawData.RegNo = studentBoardRegNo;
                                    examsStudentMarksUploadRawData.Board = board;
                                    examsStudentMarksUploadRawData.BarCode = barCode;
                                    examsStudentMarksUploadRawData.QrCode = qrCode;
                                    examsStudentMarksUploadRawData.Gender = gender;
                                    examsStudentMarksUploadRawData.Version = version;
                                    examsStudentMarksUploadRawData.Answer = answer;
                                    examsStudentMarksUploadRawData.Image = image;
                                    examsStudentMarksUploadRawData.Eiin = eiin;

                                    #endregion

                                    List<QuestionAnswerDto> examMcqQuestionSets =
                                        _examMcqQuestionSetService.LoadExamMcqQuestionSet(examinformation.Id, barCode);

                                    if (examMcqQuestionSets.Any())
                                    {
                                        StudentProgram studentProgram =
                                            _studentProgram.GetStudentProgram(
                                                Convert.ToInt64(marksUploadViewModel.Program),
                                                Convert.ToInt64(marksUploadViewModel.Session), board,
                                                Convert.ToInt64(marksUploadViewModel.BoardExam), studentBoardRoll,
                                                studentBoardRegNo);


                                        if (studentProgram != null)
                                        {
                                            examsStudentMarksUploadRawData.PrnNo = studentProgram.PrnNo;

                                            var studentAnsArrayFromXml = student.Answer.Split(new string[] { "," },
                                                StringSplitOptions.None);
                                            studentAnsArrayFromXml =
                                                studentAnsArrayFromXml.Skip(0).Take(totalQuestion).ToArray();

                                            studentMarksDetailsList = GenerateStudentMarkDetailsList(studentProgram,
                                                examsDetailses,
                                                studentAnsArrayFromXml, examMcqQuestionSets, subjectType,
                                                isAcceptPartial, maxSubject);
                                            if (studentMarksDetailsList.Any())
                                            {
                                                decimal sumofObtainedMarks =
                                                    studentMarksDetailsList.Sum(x => x.ObtainMark);
                                                examsStudentMarksUploadRawData.ObtainMark = sumofObtainedMarks;
                                            }


                                            List<TemporarySms> temporarySmsList =
                                                GenerateTemporarySmsDataList(studentMarksDetailsList,
                                                    subjectType,
                                                    studentProgram);

                                        }
                                        else
                                        {
                                            examsStudentMarksUploadRawData.Message = "Invalid Student: Roll-" +
                                                                                     studentBoardRoll + ",  Reg No.-" +
                                                                                     studentBoardRegNo;
                                            _returnVal.Add(new MarksEntryReturnMessage
                                            {
                                                mesageType = 2,
                                                message =
                                                    "Invalid Student: Roll-" + student.Roll + ",  Reg No.-" +
                                                    studentBoardRegNo
                                            });
                                        }
                                    }
                                    else
                                    {
                                        examsStudentMarksUploadRawData.Message = "Unregistered Sheet: " +
                                                                                 studentBoardRoll + ",  Reg No.-" +
                                                                                 studentBoardRegNo;

                                        _returnVal.Add(new MarksEntryReturnMessage
                                        {
                                            mesageType = 2,
                                            message =
                                                "Unregistered Sheet: " + studentBoardRoll + ",  Reg No.-" +
                                               studentBoardRegNo
                                        });

                                    }
                                    //    String studentCheckReturn = ValidationCheck(examinformation, marksUploadViewModel, studentBoardRoll, studentBoardRegNo, code, board, gender, version); 

                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(ex);
                                    //   examsStudentMarksUploadRawData.Message = WebHelper.CommonErrorMessage;

                                    _returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        mesageType = 2,
                                        message = WebHelper.CommonErrorMessage + ": Roll-" +
                                            studentBoardRoll + ",  Reg No.-" +
                                            studentBoardRegNo
                                    });
                                }
                            }
                            else
                            {
                                _returnVal.Add(new MarksEntryReturnMessage { mesageType = 2, message = "Invalid Xml Tag" });
                            }
                             _examsStudentMarksService.SaveMarksUploadRawData(examsStudentMarksUploadRawData);
                        }
                        _smsService.SaveTemporarySmsData(_tempListData);
                        #endregion
                    }
                    else
                    {
                        _returnVal.Add(new MarksEntryReturnMessage { mesageType = 2, message = "This is not mcq exam" });
                    }

                    #endregion
                }
                else
                {
                    _returnVal.Add(new MarksEntryReturnMessage { mesageType = 2, message = "This is not mcq exam" });
                }
            }
            else
            {
                _returnVal.Add(new MarksEntryReturnMessage { mesageType = 2, message = "Exam information not found" });
            }
            return ReturnValueList;
        }

        

        private IList<ExamsStudentMarks> GenerateStudentMarkDetailsList(StudentProgram studentProgram,
            List<ExamsDetails> examsDetailses, string[] studentAnsArrayFromXml, List<QuestionAnswerDto> examMcqQuestionSets, 
            int subjectType, bool isAcceptPartial, int maxSubject)
        {
            try
            {
                IList<ExamsStudentMarks> studentMarksDetailsList = new List<ExamsStudentMarks>();

                foreach (var ed in examsDetailses)
                {
                    string subjectName = "";
                    subjectName = subjectType == SubjectType.Combined ? "Combined" : ed.Subject.Name;
                    if (examMcqQuestionSets.Count == ed.TotalQuestion)
                    {
                        if (examMcqQuestionSets.Count == studentAnsArrayFromXml.Count())
                        {
                            var examsStudentMarks = new ExamsStudentMarks();
                            ExamStudentCalculatedMarks examStudentCalculatedMarks = CalculateMarks(
                                examMcqQuestionSets, ed, studentAnsArrayFromXml, isAcceptPartial);
                            examsStudentMarks.ExamsDetails = ed;
                            examsStudentMarks.StudentProgram = studentProgram;
                            examsStudentMarks.ObtainMark = examStudentCalculatedMarks.ObtainMark;
                            examsStudentMarks.TotalCorrectAnswer = examStudentCalculatedMarks.TotalCorrectAnswer;
                            examsStudentMarks.TotalWrongAnswer = examStudentCalculatedMarks.TotalWrongAnswer;
                            examsStudentMarks.TotalAnswered = examStudentCalculatedMarks.TotalCorrectAnswer +
                                                              examStudentCalculatedMarks.TotalWrongAnswer;
                            examsStudentMarks.TotalSkip = ed.TotalQuestion -
                                                          (examStudentCalculatedMarks.TotalCorrectAnswer +
                                                           examStudentCalculatedMarks.TotalWrongAnswer);
                            examsStudentMarks.SetCode = examMcqQuestionSets.FirstOrDefault().SetId;
                            studentMarksDetailsList.Add(examsStudentMarks);
                        }
                        else
                        {
                            _returnVal.Add(new MarksEntryReturnMessage
                            {
                                mesageType = 2,
                                message = "Invalid Answer String, Program Roll:" + studentProgram.PrnNo + " Exam Code: " + examsDetailses[0].Exams.Code,
                                Prn = studentProgram.PrnNo
                            });
                        }
                    }
                    else
                    {
                        _returnVal.Add(new MarksEntryReturnMessage
                        {
                            mesageType = 2,
                            message = "Invalid Answer Count, Program Roll:" + studentProgram.PrnNo + " Exam Code: " + examsDetailses[0].Exams.Code,
                            Prn = studentProgram.PrnNo
                        });
                    }
                }

                if (studentMarksDetailsList.Count > maxSubject)
                {
                    studentMarksDetailsList = new List<ExamsStudentMarks>();
                    _returnVal.Add(new MarksEntryReturnMessage
                    {
                        mesageType = 2,
                        message = "Disqualify Program Roll:" + studentProgram.PrnNo + " Exam Code: " + examsDetailses[0].Exams.Code,
                        Prn = studentProgram.PrnNo
                    });
                }


                return studentMarksDetailsList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private ExamStudentCalculatedMarks CalculateMarks(List<QuestionAnswerDto> examMcqQuestionSets, ExamsDetails ed, 
            string[] studentAnsArrayFromXml, bool isAcceptPartial)
        {
            float marksGet = 0;
            float nMarks = 0;
            int totalCorrectAnswer = 0;
            int totalWrongAnswer = 0;


            if (isAcceptPartial)
            {
                for (int i = 0; i < examMcqQuestionSets.Count; i++)
                {
                    if (examMcqQuestionSets[i].CorrectAnswer.Trim().Contains(studentAnsArrayFromXml[i]))
                    {
                        marksGet = marksGet + (float)(ed.MarksPerQuestion);
                        totalCorrectAnswer++;
                    }
                    else
                    {
                        if (studentAnsArrayFromXml[i].Trim() != "")
                        {
                            nMarks = nMarks + (float)(ed.MarksPerQuestion * (ed.NegativeMark / 100));
                            totalWrongAnswer++;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < examMcqQuestionSets.Count; i++)
                {
                    if (examMcqQuestionSets[i].CorrectAnswer == studentAnsArrayFromXml[i])
                    {
                        marksGet = marksGet + (float)(ed.MarksPerQuestion);
                        totalCorrectAnswer++;
                    }
                    else
                    {
                        if (studentAnsArrayFromXml[i].Trim() != "")
                        {
                            nMarks = nMarks + (float)(ed.MarksPerQuestion * (ed.NegativeMark / 100));
                            totalWrongAnswer++;
                        }
                    }
                }
            }
            decimal obtainMarks = (decimal)(marksGet - nMarks);
            if (obtainMarks < 0)
            {
                obtainMarks = 0;
            }
            return new ExamStudentCalculatedMarks() { ObtainMark = obtainMarks, TotalCorrectAnswer = totalCorrectAnswer, TotalWrongAnswer = totalWrongAnswer };

        }

        private List<TemporarySms> GenerateTemporarySmsDataList(IList<ExamsStudentMarks> studentMarksDetailsList, int subjectType, StudentProgram studentProgram)
        {
            #region Looping through studentMarksDetailsList
            try
            {
                if (studentMarksDetailsList.Any())
                {
                    bool isAbleSms = true;
                    try
                    {
                        foreach (var smd in studentMarksDetailsList)
                        {
                            string subjectName = "";
                            subjectName = subjectType == SubjectType.Combined ? "Combined" : smd.ExamsDetails.Subject.Name;
                            ExamsStudentMarks isAlreadyExistMarks =
                                _examsStudentMarksService
                                    .LoadExamsStudentMarksByExamsDetailsStudentProgram(
                                        smd.ExamsDetails.Id, smd.StudentProgram.Id);
                            if (isAlreadyExistMarks != null)
                            {
                                //isAbleSms = false;
                                var tempOldMarks = isAlreadyExistMarks.ObtainMark;
                                isAlreadyExistMarks.ObtainMark = smd.ObtainMark;
                                isAlreadyExistMarks.TotalCorrectAnswer = smd.TotalCorrectAnswer;
                                isAlreadyExistMarks.TotalWrongAnswer = smd.TotalWrongAnswer;
                                isAlreadyExistMarks.TotalAnswered = smd.TotalAnswered;
                                isAlreadyExistMarks.TotalSkip = smd.TotalSkip;
                                bool isUpdate =
                                    _examsStudentMarksService.Update(isAlreadyExistMarks);
                                if (isUpdate)
                                {
                                    if (tempOldMarks == smd.ObtainMark)
                                    {
                                        isAbleSms = false;
                                    }
                                    _returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        mesageType = 1,
                                        message =
                                            "Student Answer Update Success for prnNo: " +
                                            smd.StudentProgram.Student.RegistrationNo + " and exam code: " +
                                            smd.ExamsDetails.Exams.Code + " and subject: " +
                                            subjectName,
                                        Prn = smd.StudentProgram.PrnNo
                                    });
                                }
                                else
                                {
                                    isAbleSms = false;
                                    _returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        mesageType = 2,
                                        message =
                                            "Student Answer Update fail for prnNo: " +
                                            smd.StudentProgram.PrnNo + " and exam code: " +
                                            smd.ExamsDetails.Exams.Code + " and subject: " +
                                            subjectName,
                                        Prn = smd.StudentProgram.PrnNo

                                    });
                                }
                            }
                            else
                            {
                                bool isSave = _examsStudentMarksService.Save(smd);
                                if (isSave)
                                {
                                    _returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        mesageType = 0,
                                        message =
                                            "Student Answer Save Success for prnNo: " +
                                            smd.StudentProgram.PrnNo + " and exam code: " +
                                            smd.ExamsDetails.Exams.Code + " and subject: " +
                                            subjectName,
                                        Prn = smd.StudentProgram.PrnNo
                                    });
                                }
                                else
                                {
                                    isAbleSms = false;
                                    _returnVal.Add(new MarksEntryReturnMessage
                                    {
                                        mesageType = 2,
                                        message =
                                            "Student Answer Save fail for prnNo: " +
                                            smd.StudentProgram.PrnNo + " and exam code: " +
                                            smd.ExamsDetails.Exams.Code + " and subject: " +
                                            subjectName,
                                        Prn = smd.StudentProgram.PrnNo
                                    });
                                }
                            }
                        }
                        if (isAbleSms)
                        {
                            _tempListData.Add(new TemporarySms()
                            {
                                Student = studentProgram.Student,
                                StudentProgram = studentProgram,
                                Exams = studentMarksDetailsList[0].ExamsDetails.Exams,
                                ExamType = ExamType.Mcq,
                                Status = 1
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        _returnVal.Add(new MarksEntryReturnMessage
                        {
                            mesageType = 2,
                            message = WebHelper.CommonErrorMessage + " For Student Program Roll: " + studentProgram.PrnNo,
                            Prn = studentProgram.PrnNo
                        });
                    }
                }
                return _tempListData;
            }
            catch (Exception wx)
            {
                _logger.Error(wx);
                throw;
            }

            #endregion
        }
        private void SaveXmlFile(HttpPostedFileBase xmlFile)
        {
            try
            {
                var loginUser =
                    _userService.LoadAspNetUserById(
                        Convert.ToInt64(IdentityExtensions.GetUserId(System.Web.HttpContext.Current.User.Identity)));
                var actualPath = FileUpload.XmlFileValidationCheck(loginUser.FullName, xmlFile);
            }
            catch (Exception ex)
            {
                _logger.Error("Xml File Error On Saving");
                _logger.Error(ex);
            }

        } 
        #endregion

    }
}