﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Microsoft.AspNet.Identity;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.BusinessRules.Hr;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Hr.Controllers
{
    [UerpArea("Hr")]
    [Authorize]
    [AuthorizeAccess]
    public class LoanManagementController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Propertise/Services/Dao & Initialization

        private readonly ICommonHelper _commonHelper;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IMemberLoanApplicationService _memberLoanApplicationService;
        private readonly IMemberLoanService _memberLoanService;
        private readonly IOrganizationService _organizationService;
        private readonly IDepartmentService _departmentService;
        private List<UserMenu> authorizeMenu;
        public LoanManagementController()
        {
            var session = NHibernateSessionFactory.OpenSession();
            _commonHelper = new CommonHelper();
            _teamMemberService = new TeamMemberService(session);
            _memberLoanApplicationService = new MemberLoanApplicationService(session);
            _memberLoanService = new MemberLoanService(session);
            _organizationService = new OrganizationService(session);
            _departmentService = new DepartmentService(session);
        }

        #endregion

        #region Save Operation

        public ActionResult Create()
        {
            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                ViewBag.PaymentTypeList = new SelectList(_commonHelper.LoadEmumToDictionary<PaymentType>(new List<int>() { (int)PaymentType.Daily }), "Key", "Value");
                ViewBag.FundedByList = new SelectList(_commonHelper.LoadEmumToDictionary<FundedBy>(), "Key", "Value");
                int year = DateTime.Now.Year;
                DateTime lastDay = new DateTime(year, 12, 31);
                ViewBag.EffectiveDate = DateTime.Now.ToString("MMMM yyyy");
                ViewBag.CloasingDate = lastDay.ToString("MMMM yyyy");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = WebHelper.SetExceptionMessage(ex);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LoanManagementViewModel loanManagement, bool hasZakat, bool hasCsr, bool hasLoan)
        {

            var userMenu = (List<UserMenu>)ViewBag.UserMenu;
            try
            {
                MemberLoanApplication memberLoanApplication = new MemberLoanApplication();
                memberLoanApplication.TeamMember =
                    _teamMemberService.GetByPin(Convert.ToInt32(loanManagement.Pin.Trim()));
                memberLoanApplication.RequestedAmount = loanManagement.RequestedAmount;
                memberLoanApplication.LoanStatus = (int)LoanStatus.Approved;
                memberLoanApplication.Remarks = loanManagement.Reason;

                if (!hasZakat && !hasCsr && !hasLoan)
                {
                    throw new InvalidDataException("Select at least one of zakat, csr or loan");
                }

                //Zak at
                if (hasZakat)
                {
                    MemberLoanZakat memberLoanZakat = new MemberLoanZakat();
                    memberLoanZakat.Status = MemberLoanZakat.EntityStatus.Active;
                    memberLoanZakat.CreateBy = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    memberLoanZakat.ModifyBy = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    memberLoanZakat.PaymentType = loanManagement.ZakatPaymentType;
                    memberLoanZakat.Amount = loanManagement.ZakatAmount;
                    var zakatEffectiveDate = loanManagement.ZakatEffectiveDate;
                    var zakatClosingDate = GetLastDateOfMonth(loanManagement.ZakatClosingDate);
                    memberLoanZakat.EffectiveDate = zakatEffectiveDate;
                    memberLoanZakat.ClosingDate = zakatClosingDate;
                    int noOfZakatMonths = ((zakatClosingDate.Year - zakatEffectiveDate.Year) * 12 + zakatClosingDate.Month - zakatEffectiveDate.Month) + 1;
                    memberLoanZakat.TotalAmount = CalculateTotalAmount(memberLoanZakat.PaymentType, memberLoanZakat.Amount, noOfZakatMonths);//loanManagement.ZakatAmount * noOfZakatMonths);
                    memberLoanZakat.ApprovedDate = DateTime.Now;
                    memberLoanZakat.FundedBy = loanManagement.ZakatFundedBy;
                    memberLoanZakat.MemberLoanApplication = memberLoanApplication;
                    memberLoanApplication.MemberLoanZakat.Add(memberLoanZakat);
                }

                //Csr
                if (hasCsr)
                {
                    MemberLoanCsr memberLoanCsr = new MemberLoanCsr();
                    memberLoanCsr.Status = MemberLoanCsr.EntityStatus.Active;
                    memberLoanCsr.CreateBy = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    memberLoanCsr.ModifyBy = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    memberLoanCsr.PaymentType = loanManagement.CsrPaymentType;
                    memberLoanCsr.Amount = loanManagement.CsrAmount;
                    var csrEffectiveDate = loanManagement.CsrEffectiveDate;
                    var csrClosingDate = GetLastDateOfMonth(loanManagement.CsrClosingDate);
                    memberLoanCsr.EffectiveDate = csrEffectiveDate;
                    memberLoanCsr.ClosingDate = csrClosingDate;
                    int noOfCsrMonths = ((csrClosingDate.Year - csrEffectiveDate.Year) * 12 + csrClosingDate.Month - csrEffectiveDate.Month) + 1;
                    memberLoanCsr.TotalAmount = CalculateTotalAmount(memberLoanCsr.PaymentType, memberLoanCsr.Amount, noOfCsrMonths);//loanManagement.ZakatAmount * noOfCsrMonths;
                    memberLoanCsr.ApprovedDate = DateTime.Now;
                    memberLoanCsr.MemberLoanApplication = memberLoanApplication;
                    memberLoanApplication.MemberLoanCsr.Add(memberLoanCsr);
                }

                //Loan
                if (hasLoan)
                {
                    MemberLoan memberLoan = new MemberLoan();
                    memberLoan.Status = MemberLoanCsr.EntityStatus.Active;
                    memberLoan.CreateBy = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    memberLoan.ModifyBy = Convert.ToInt64(System.Web.HttpContext.Current.User.Identity.GetUserId());
                    memberLoan.LoanApprovedAmount = loanManagement.NewLoanApprovedAmount;
                    memberLoan.MonthlyRefund = loanManagement.NewMonthlyRefundAmount;
                    memberLoan.RefundStart = loanManagement.NewRefundStartDate;
                    memberLoan.ApprovedDate = DateTime.Now;
                    memberLoan.MemberLoanApplication = memberLoanApplication;
                    memberLoanApplication.MemberLoan.Add(memberLoan);
                }
                _memberLoanApplicationService.SaveOrUpdate(memberLoanApplication);
                return Json(new { IsSuccess = true, Message = "Loan application successfully submitted." });
            }
            catch (InvalidDataException ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new { IsSuccess = false, Message = WebHelper.SetExceptionMessage(ex) });
            }
            return View();
        }

        #endregion

        #region Index/Manage Page



        #endregion

        #region Delete Operation


        #endregion

        #region Update Operation


        #endregion

        #region Helper Function

        //From Now-- calculate last day of month for closing date as per Imran's vi requirement
        private DateTime GetLastDateOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }
        private decimal CalculateTotalAmount(int paymentType, decimal amount, int monthDiff)
        {
            float md = monthDiff;

            decimal totalAmount = 0;
            if (paymentType == 1) { totalAmount = Convert.ToInt64(amount); }
            else if (paymentType == 3) { totalAmount = Convert.ToInt64(amount * monthDiff); }
            else if (paymentType == 4)
            {
                if (monthDiff > 12 && Math.Ceiling(md / 12) > 1)
                {
                    totalAmount = Convert.ToInt64(Math.Ceiling((float)monthDiff / 12)) * amount;
                }
                else
                {
                    totalAmount = Convert.ToInt64(amount);
                }
            }
            return totalAmount;
        }

        private void Initialize()
        {
            authorizeMenu = (List<UserMenu>)ViewBag.UserMenu;
            var authOrgList = _organizationService.LoadAuthorizedOrganization(authorizeMenu);
            ViewBag.organizationList = new SelectList(authOrgList, "Id", "ShortName");
            ViewBag.depatrtmentList = new SelectList(String.Empty, "Key", "Value");
            ViewBag.branchList = new SelectList(String.Empty, "key", "value");

            //ViewBag.DepartmentId = new SelectList(_departmentService.LoadDepartment(authOrgList.Select(x => x.Id).ToList()), "Id", "Name");
            ViewBag.statusList = new SelectList(_commonHelper.LoadEmumToDictionary<LoanStatus>(), "key", "value");
        }

        #region Ajax functions

        public JsonResult GetCurrentLoan(int pin)
        {
            try
            {
                var currentLoanBalance = _memberLoanService.GetCurrentLoan(pin);
                var currentMonthlyRefund = _memberLoanService.GetCurrentMonthlyRefund(pin);
                return Json(new { CurrentLoanBalance = currentLoanBalance, CurrentMonthlyRefund = currentMonthlyRefund, IsSuccess = true });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.ErrorMessage = "Problem Occurred During Load Organization";
                return Json(new Response(false, WebHelper.CommonErrorMessage));
            }
        }

        #endregion

        #endregion


        #region LoanApplication History

        public ActionResult LoanApplicationHistory()
        {
            try
            {
                Initialize();
                return PartialView("Partial/_LoanApplicationHistory");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        #endregion

        #region Datatable

        [HttpPost]
        public JsonResult LoanApplicationHistroyPost(int draw, int start, int length, int? pin, long? organizationId = null,
            long? departmentId = null, long? branchId = null, int? loanStatus = null)
        {
            try
            {
                var data = new List<object>();
                var recordsTotal = 0;
                var recordsFiltered = 0;
                var userMenu = (List<UserMenu>)ViewBag.UserMenu;
                IList<MemberLoanApplicationHistoryDto> memberLoanApplications = _memberLoanApplicationService.LoadMemberLoanApplication(start, length, userMenu, pin, organizationId, departmentId, branchId, loanStatus);
                recordsTotal = _memberLoanApplicationService.GetMemberLoanApplicationCount(userMenu, pin, organizationId, departmentId, branchId, loanStatus);
                recordsFiltered = recordsTotal;

                foreach (var loanAppObj in memberLoanApplications)
                {
                    var str = new List<string>();
                    str.Add(loanAppObj.Pin.ToString());
                    str.Add(loanAppObj.Name);
                    str.Add(loanAppObj.Department);
                    str.Add(loanAppObj.Branch);
                    str.Add(loanAppObj.Organization);
                    //str.Add(loanAppObj.RequestedAmount.ToString(CultureInfo.InvariantCulture));
                    str.Add(loanAppObj.LoanApprovedAmount.ToString(CultureInfo.InvariantCulture));
                    //str.Add(loanAppObj.MonthlyRefund.ToString(CultureInfo.InvariantCulture));
                    //str.Add(loanAppObj.RefundStart == DateTime.MinValue ? "" : loanAppObj.RefundStart.ToString("MMM, yyyy"));
                    str.Add(loanAppObj.Reason);
                    str.Add(loanAppObj.ModificationDate.ToString("dd MMM,yyyy"));
                    str.Add(loanAppObj.LastModifiedBy);
                    str.Add(LinkGenerator.GetDetailsLink("Details", "LoanManagement", Convert.ToInt64(loanAppObj.MemberLeaveApplicationId)));
                    data.Add(str);
                }
                return Json(new
                 {
                     draw = draw,
                     recordsTotal = recordsTotal,
                     recordsFiltered = recordsFiltered,
                     start = start,
                     length = length,
                     data = data
                 });
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }

        }

        public ActionResult Details(long id)
        {
            try
            {
                MemberLoanDetailsDto loanDetailsDto = _memberLoanService.GenerateLoanDetailByMemberPin(id);

                return View(loanDetailsDto);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Loan Opening Balance

        public ActionResult LoanOpening()
        {
            try
            {

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        public ActionResult LoanOpening(List<LoanOpeningViewModel> openingViewModel)
        {
            try
            {
                List<MemberLoan> memberLoans = BuildMemberLoan(openingViewModel);
                _memberLoanService.SaveOrUpdate(memberLoans);
                return Json(new Response(true, "Loan Approved Successfully"));
            }
            catch (InvalidDataException ide)
            {
                _logger.Error(ide);
                return Json(new Response(false, ide.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Json(new Response(false, ex.Message));
            }
        }

        private List<MemberLoan> BuildMemberLoan(List<LoanOpeningViewModel> openingViewModel)
        {
            if (openingViewModel.Count == 0 || openingViewModel == null) throw new InvalidDataException("Invalid data exception");
            List<MemberLoan> memberLoans = new List<MemberLoan>();
            foreach (var loanOpeningViewModel in openingViewModel)
            {
                var memberLoanApplication = new MemberLoanApplication();
                memberLoanApplication.LoanStatus = (int)LoanStatus.Approved;
                memberLoanApplication.Remarks = loanOpeningViewModel.Remarks;
                memberLoanApplication.RequestedAmount = loanOpeningViewModel.LoanBalance;
                memberLoanApplication.TeamMember = _teamMemberService.GetByPin(loanOpeningViewModel.Pin);

                var memberLoan = new MemberLoan();
                memberLoan.MonthlyRefund = loanOpeningViewModel.AdjustmentBalance;
                memberLoan.RefundStart = loanOpeningViewModel.AdjustmentStart;
                memberLoan.LoanApprovedAmount = loanOpeningViewModel.LoanBalance;
                memberLoan.ApprovedDate = DateTime.Now;
                memberLoan.MemberLoanApplication = memberLoanApplication;

                memberLoans.Add(memberLoan);
            }
            return memberLoans;
        }

        #endregion

    }

}