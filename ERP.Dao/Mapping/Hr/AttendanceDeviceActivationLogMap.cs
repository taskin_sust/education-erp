using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {


    public partial class AttendanceDeviceActivationLogMap : BaseClassMap<AttendanceDeviceActivationLog,long>
    {
        
        public AttendanceDeviceActivationLogMap() {
			Table("HR_AttendanceDeviceActivationLog");
			LazyLoad();
			References(x => x.AttendanceDevice).Column("AttendanceDeviceId");
			References(x => x.AttendanceSynchronizer).Column("AttendanceSynchronizerId");
			Map(x => x.Rank).Column("Rank");
			Map(x => x.Name).Column("Name");
			Map(x => x.DeviceStatus).Column("DeviceStatus");
        }
    }
}
