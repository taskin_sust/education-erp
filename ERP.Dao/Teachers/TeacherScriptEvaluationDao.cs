﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Students;
using UdvashERP.BusinessModel.Entity.Teachers;
using UdvashERP.BusinessModel.Entity.UserAuth;

namespace UdvashERP.Dao.Teachers
{
    public interface ITeacherScriptEvaluationDao : IBaseDao<TeacherScriptEvaluation, long>
    {
        #region Single Instance Loading function
        TeacherScriptEvaluation GetTeacherScriptEvaluation();
        #endregion
    }

    public class TeacherScriptEvaluationDao:BaseDao<TeacherScriptEvaluation,long>,ITeacherScriptEvaluationDao
    {
        #region Single Instance Loading function
        public TeacherScriptEvaluation GetTeacherScriptEvaluation()
        {
            return Session.QueryOver<TeacherScriptEvaluation>()
                    .Where(x => x.Status == TeacherScriptEvaluation.EntityStatus.Active).OrderBy(tx => tx.SerialNumber).Desc.Take(1)
                    .SingleOrDefault<TeacherScriptEvaluation>();

        }
        #endregion
    }


}
