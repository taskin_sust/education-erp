﻿using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using UdvashERP.Dao.UserAuth;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.Services.Base;

namespace UdvashERP.Services.UserAuth
{
    public interface ILoginActivityLogService : IBaseService
    {
        #region Operational Function

        void Save(LoginActivityLog lAl);

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        IList<LoginActivityLog> LoadLoginActivityLog(int start, int length, string orderBy, string orderDir, string userName, int operationStatus, string ipAddress);

        #endregion

        #region Others Function

        int LoginActivityLogRowCount(string userName, int operationStatus, string ipAddress);

        #endregion  
    }

    public class LoginActivityLogService:BaseService,ILoginActivityLogService
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("UserAuthService");

        #endregion

        #region Propertise & Object Initialization

        private readonly ILoginActivityLogDao _loginActivityLogDao;
        
        public LoginActivityLogService(ISession session)
        {
            Session = session;
            _loginActivityLogDao = new LoginActivityLogDao() { Session = session };
        }

        #endregion

        #region Operational Function

        public void Save(LoginActivityLog lAl)
        {
            try
            {
                _loginActivityLogDao.SaveLog(lAl);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        public IList<LoginActivityLog> LoadLoginActivityLog(int start, int length, string orderBy, string orderDir, string userName, int operationStatus, string ipAddress)
        {
            try
            {
                return _loginActivityLogDao.LoadLoginActivityLog(start, length, orderBy, orderDir, userName, operationStatus, ipAddress);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }

        #endregion

        #region Others Function

        public int LoginActivityLogRowCount(string userName, int operationStatus, string ipAddress)
        {
            try
            {
                return _loginActivityLogDao.LoginActivityLogRowCount(userName, operationStatus, ipAddress);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }
        }

        #endregion   
    }
}
