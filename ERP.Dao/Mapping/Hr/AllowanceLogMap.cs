using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using UdvashERP.Dao.Mapping.Base;
using UdvashERP.BusinessModel.Entity.Hr; 

namespace UdvashERP.Dao.Mapping.Hr {

   
    public partial class AllowanceLogMap : ClassMap<AllowanceLog>
    {
        
        public AllowanceLogMap() {
			Table("HR_AllowanceLog");
			LazyLoad();
			
			References(x => x.Allowance).Column("AllowanceId");
			Map(x => x.DailyAllowanceDate).Column("DailyAllowanceDate");
			Map(x => x.IsApproved).Column("IsApproved");

            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.BusinessId);
            Map(x => x.CreationDate);
            Map(x => x.CreateBy);
        }
    }
}
