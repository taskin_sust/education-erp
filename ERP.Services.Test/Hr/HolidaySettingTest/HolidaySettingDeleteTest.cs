﻿using System;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.HolidaySettingTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "HolidaySetting")]
    public class HolidaySettingDeleteTest : HolidaySettingBaseTest
    {
        #region Basic Test
        
        [Fact]
        public void Should_Delete_Successfully()
        {
            var dFactory = _holidaySettingFactory.Create().WithNameAndRank(_name, 1).WithOrganization().WithHolidayRepeatationType((int)HolidayRepeatationType.Once)
                                .WithHolidayType((int)HolidayTypeEnum.Gazetted).Persist();
            HolidaySetting hrHolidaySetting = _holidaySettingService.GetHolidaySetting(dFactory.Object.Id);
            hrHolidaySetting.DateFrom = DateTime.Now.AddDays(1);
            var success = _holidaySettingService.Delete(hrHolidaySetting.Id);
            Assert.True(success);
            hrHolidaySetting = _holidaySettingService.GetHolidaySetting(dFactory.Object.Id);
            Assert.Null(hrHolidaySetting);

        }

        [Fact]
        public void Delete_Should_Return_Null_Exception()
        {
            Assert.Throws<NullObjectException>(() => _holidaySettingService.Delete(0));
        }
        
        [Fact]
        public void Delete_Holiday_Setting_Should_Not_Delete_Organization()
        {
            var dFactory = _holidaySettingFactory.Create().WithNameAndRank(_name, 1).WithOrganization().WithHolidayRepeatationType((int)HolidayRepeatationType.Once)
                                .WithHolidayType((int)HolidayTypeEnum.Gazetted).Persist();
            HolidaySetting hrHolidaySetting = _holidaySettingService.GetHolidaySetting(dFactory.Object.Id);
            hrHolidaySetting.DateFrom = DateTime.Now.AddDays(1);
            var organizationId = hrHolidaySetting.Organization.Id;
            _holidaySettingService.Delete(hrHolidaySetting.Id);
            var organization = _organizationService.LoadById(organizationId);
            Assert.NotNull(organization);
        }

        #endregion
    }
}
