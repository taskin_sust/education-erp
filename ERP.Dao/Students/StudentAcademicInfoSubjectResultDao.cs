﻿using System;
using NHibernate;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Students;

namespace UdvashERP.Dao.Students
{
    public interface IStudentAcademicInfoSubjectResultDao : IBaseDao<StudentAcademicInfoSubjectResult, long>
    {        
        #region Operational Function
        bool SaveBoardResult(StudentAcademicInfoSubjectResult subjectResultObj);
        bool DeleteSubjectResult(StudentAcademicInfoBaseData existData);
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion
    }

    public class StudentAcademicInfoSubjectResultDao : BaseDao<StudentAcademicInfoSubjectResult, long>, IStudentAcademicInfoSubjectResultDao
    {
        #region Propertise & Object Initialization

        #endregion

        #region Operational Function
        public bool SaveBoardResult(StudentAcademicInfoSubjectResult subjectResultObj)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    Session.Save(subjectResultObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                return false;
            }
        }
        public bool DeleteSubjectResult(StudentAcademicInfoBaseData existData)
        {
            ITransaction transaction = null;
            try
            {
                using (transaction = Session.BeginTransaction())
                {
                    var sResult = Session.QueryOver<StudentAcademicInfoSubjectResult>().Where(x => x.StudentAcademicInfoBaseData.Id == existData.Id).List<StudentAcademicInfoSubjectResult>();
                    foreach (var x in sResult)
                    {
                        existData.StudentAcademicInfoSubjectResult.Remove(x);
                    }

                    Session.SaveOrUpdate(existData);
                    //foreach (StudentAcademicInfoSubjectResult deSubjectResult in existData.StudentAcademicInfoSubjectResult)
                    //{
                    //    Session.Delete(deSubjectResult);
                    //    transaction.Commit();
                    //}
                    //Session.Save(subjectResultObj);
                    transaction.Commit();
                    return true;
                }
            }
            catch (Exception)
            {
                if (transaction != null && transaction.IsActive)
                    transaction.Rollback();
                return false;
            }
        }
        #endregion

        #region Single Instances Loading Function

        #endregion

        #region List Loading Function

        #endregion

        #region Others Function

        #endregion

        #region Helper Function

        #endregion
    }
}
