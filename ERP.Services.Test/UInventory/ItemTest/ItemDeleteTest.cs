﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules.UInventory;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.UInventory.ItemTest
{
    [Trait("Area", "UInventory")]
    [Trait("Service", "Item")]
    public class ItemDeleteTest : ItemBaseTest
    {
        #region Basic Test

        [Fact]
        public void Should_Delete_Successfully()
        {
            var itemFactory = _itemFactory.Create().WithOrganization().WithItemGroup().Persist();
            var isDelete = _itemService.IsDelete(itemFactory.Object.Id);
            Assert.True(isDelete);
        }

        [Fact]
        public void Should_Delete_Return_Null_Exception()
        {
            var itemFactory = _itemFactory.Create().WithItemGroup().WithOrganization().Persist();
            Assert.Throws<NullObjectException>(() => _itemService.IsDelete(itemFactory.Object.Id + 1));
        }

        [Fact]
        public void Should_Delete_Dependency_Exception()
        {
            organizationFactory.Create().Persist();
            _itemFactory.CreateWith(0, Guid.NewGuid().ToString(), (int)ItemType.ProgramItem, (int)ItemUnit.Feet,
                                    (int)CostBearer.Corporate).WithOrganization(organizationFactory.Object).WithItemGroup().Persist();
            _programSessionItemFactory.Create()
                .WithProgramAndSession(organizationFactory.Object)
                .WithItem(_itemFactory.Object)
                .Persist();
            Assert.Throws<DependencyException>(() => _itemService.IsDelete(_itemFactory.Object.Id));
        }

        #endregion
    }
}