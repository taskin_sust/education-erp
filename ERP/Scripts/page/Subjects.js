﻿$(document).ready(function () {
$("#ProgramId").change(function () {
    $('.checkbox').prop('checked', false);
    var program = $("#ProgramId").val();
    if (program != "") {
        var data = { "ProgramId": program };
        $.ajax({
            type: "post",
            url: "/Subject/GetSubject",
            contenttype: "application/json; charset=utf-8",
            data: data,
            success: function (result) {


                $.each(result, function (index) {
                    $('.checkbox[value="' + result[index].Id + '"]').prop('checked', true);
                });



            },
            error: function (result) {
                alert("Unable to get program.");
            }
        });
    }
    else {

    }
});
//select/deselect all
$("#allPrograms").click(function () {
    if ($(this).is(":checked")) {
        var c = this.checked;
        $('.checkbox').prop('checked', c);
    }
    else {
        var c = this.checked;
        $('.checkbox').prop('checked', c);
    }
});
});