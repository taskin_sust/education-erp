﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UdvashERP
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "UdvashERP.Controllers" } 
            );
            routes.MapRoute(
                name: "PermissionDenied",
                url: "Home/PermissionDenied"
            );
            routes.MapRoute(
                name: "Login",
                url: "Account/Login"
            );
            routes.MapRoute(
                name: "LogOff",
                url: "Account/LogOff"
            );
            routes.MapRoute(
                name: "Manage",
                url: "Account/Manage"
            );
        }
    }
}
