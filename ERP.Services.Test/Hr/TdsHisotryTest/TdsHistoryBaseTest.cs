﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.Test.ObjectFactory.Hr;

namespace UdvashERP.Services.Test.Hr.TdsHisotryTest
{
    public class TdsHistoryBaseTest : TestBase, IDisposable
    {
        #region Propertise & Object Initialization
        public const int MentorId = 2;
        protected readonly CommonHelper CommonHelper;
        protected readonly ITdsHistoryService _tdsHistoryService;
        protected readonly TeamMemberFactory _teamMemberFactory;
        public readonly TdsHistoryFactory _tdsHistoryFactory;
        private ISession _session;

        #endregion

        #region

        public TdsHistoryBaseTest()
        {
            _session = NHibernateSessionFactory.OpenSession();
            CommonHelper = new CommonHelper();
            _tdsHistoryService = new TdsHistoryService(_session);
            _tdsHistoryFactory = new TdsHistoryFactory(null, _session);
            _teamMemberFactory = new TeamMemberFactory(null, _session);
        }

        public const string MemberEntityNotFound = "MemberEntityNotFound";
        public const string NullObjectError = "NullObject";
       
        public void Dispose()
        {
            _tdsHistoryFactory.LogCleanUp(_tdsHistoryFactory);
            _tdsHistoryFactory.CleanUp();
            base.Dispose();
        }

        #endregion

    }
}
