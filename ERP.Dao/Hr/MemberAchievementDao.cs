﻿using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using NHibernate.Util;
using UdvashERP.BusinessModel.Dto.Hr;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.Dao.Base;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.Hr;

namespace UdvashERP.Dao.Hr
{
    public interface IMemberAchievementDao : IBaseDao<MemberAchievement, long>
    {

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        MemberAchievement GetMemberAchievement(long teamMemberId, int year, int month);

        #endregion

        #region List Loading Function

        IList<MemberAchievement> LoadMemberAchievementList(int start, int length, string orderBy, string orderDir, int year, int month, List<int> memberPinList = null, List<long> authoMemberIdList = null);
        IList<MyAchievementReport> LoadMyAchievementReports(long teamMemberId,  string dateFrom, string dateTo);  
        #endregion

        #region Others Function

        int GetMemberAchievementCount(string orderBy, string orderDir, int year, int month, List<int> memberPinList = null, List<long> authoMemberIdList = null);

        #endregion

        #region Helper Function

        #endregion
        
    }
    public class MemberAchievementDao : BaseDao<MemberAchievement, long>, IMemberAchievementDao
    {

        #region Propertise & Object Initialization

        #endregion

        #region Operational Function

        #endregion

        #region Single Instances Loading Function

        public MemberAchievement GetMemberAchievement(long teamMemberId, int year, int month)
        {
            MonthsOfYear monthOfYear = (MonthsOfYear) month;
            return Session.QueryOver<MemberAchievement>().Where(x => x.Status != MemberAchievement.EntityStatus.Delete && x.Year == year && x.Month == monthOfYear && x.TeamMember.Id == teamMemberId).SingleOrDefault();
        }

        #endregion

        #region List Loading Function


        public IList<MemberAchievement> LoadMemberAchievementList(int start, int length, string orderBy, string orderDir, int year, int month, List<int> memberPinList = null, List<long> authoMemberIdList = null)
        {
            ICriteria criteria = GetMemberAchievementCriteria(orderBy, orderDir, year, month, memberPinList, authoMemberIdList);
            return criteria.SetFirstResult(start).SetMaxResults(length).List<MemberAchievement>();
        }

        public IList<MyAchievementReport> LoadMyAchievementReports(long teamMemberId, string dateFrom, string dateTo)
        {
            string query = @"SET ANSI_WARNINGS OFF
                                DECLARE @tempdate AS TABLE ([Date] datetime);
                                DECLARE  @StartDate DATETIME
                                        ,@EndDate DATETIME
		                                ,@TempStartDate DATETIME
		                                ,@TeamMemberId BIGINT;

                                SET DATEFIRST 6;
                                SELECT   @EndDate = '" + dateTo + @"'
                                        ,@StartDate = '" + dateFrom + @"'
		                                ,@TempStartDate = '" + dateFrom + @"'
		                                ,@TeamMemberId = " + teamMemberId + @";

                                while (@TempStartDate <= @EndDate)
                                BEGIN
                                    INSERT INTO @tempdate([Date]) VALUES (@TempStartDate);
                                    SET @TempStartDate = dateadd(DAY, 1, @TempStartDate)
                                END

                                Select * INTO #FilterTeamMemberTemp From (Select 
	                                currentPosition.[TeamMemberId] as TeamMemberId
	                                ,o.Id as OrganizationId
                                    ,dp.Name as Designation
	                                from (
	                                Select * from (
		                                Select [CampusId]
			                                ,[DesignationId]
			                                ,[DepartmentId]
			                                ,[TeamMemberId]
			                                ,[EffectiveDate]
			                                ,[EmploymentStatus]
			                                ,RANK() OVER(PARTITION BY TeamMemberId ORDER BY EffectiveDate DESC) AS R  
		                                from [dbo].[HR_EmploymentHistory] Where [Status] = 1 AND EffectiveDate <=@EndDate AND [TeamMemberId] = @TeamMemberId
	                                ) as a WHERE a.R = 1
	                                ) as currentPosition
	                                Left Join [dbo].[HR_Designation] AS dp ON dp.Id = currentPosition.DesignationId and dp.Status  =  " + Department.EntityStatus.Active + @"
	                                Left Join [dbo].[Organization] AS o ON o.Id = dp.OrganizationId AND o.Status = " + Organization.EntityStatus.Active + @"
	                                WHere 	1=1  AND EmploymentStatus!= " + (int)MemberEmploymentStatus.Retired + @" ) as a

                                Select a.* 
	                                  ,b.[TotalWorkingHour] as TpHour
                                      ,b.[TotalCompletePoint]
                                      ,b.[CompleteTaskCount]
                                      ,b.[RunningTaskCount]
	                                  ,cast( b.[TotalWorkingHour]/a.MyWorkingDay as numeric(18,2)) AS MyAvgWorkingTpHour
                                from (
	                                Select a.*,DATEPART(yyyy,a.YearMonth) AS YearColumn ,DATEPART(mm,a.YearMonth) AS MonthColumn from (
		                                Select a.TeamMemberId
                                        ,a.Designation
		                                ,DATEADD(MONTH, DATEDIFF(MONTH, 0, a.Date), 0) as YearMonth
		                                --,(COUNT(a.TeamMemberId)-(SUM(a.holiday)+SUM(CASE WHEN a.holiday=1 THEN 0 ELSE a.Weekend END ))) as TotalWorkingDay
		                                ,SUM(a.WorkingDay) as TotalWorkingDay
		                                ,SUM(a.StandardDayHour) as StandardHour
		                                ,SUM(a.noofWorkingDay+a.noofExtraDay)  as  MyWorkingDay
		                                ,SUM(a.WorkingDayHour+a.ExtraDayHour) as MyStandardHour
		                                ,SUM(a.leave) as  LeaveDay
		                                ,SUM(a.noofExtraDay) as  ExtraDay
		                                ,SUM(CASE WHEN a.LateEntry IS NULL THEN 0 ELSE 1 END ) as LateEntryDay
		                                ,SUM(CASE WHEN a.EarlyLeave IS NULL THEN 0 ELSE 1 END ) as EarlyLeaveDay
		                                ,cast(round(SUM(a.WorkingdHour),2) as numeric(18,2)) AS MyHour
		                                ,cast(round(SUM(a.WorkingdHour)/SUM(a.noofWorkingDay+a.noofExtraDay),2) as numeric(18,2)) AS MyAvgWorkingHour
		                                ,cast(SUM(cast(a.LateEntryMinute as decimal(18,2)))/60.0 as decimal(18,2)) as LateHour
		                                ,cast(SUM(cast(a.EarlyLeaveMinute as decimal(18,2)))/60.0 as decimal(18,2)) as EarlyHour
		                                ,cast(SUM(cast(a.OvertimeMinute as decimal(18,2)))/60.0 as decimal(18,2)) as OvertimeHour
		                                from (	
			                                Select a.*,ats.AttendanceDate 
				                                --,((1-(a.holiday+(CASE WHEN a.holiday=1 THEN 0 ELSE a.Weekend END))) * a.StandardHour) as 
				                                , (CASE WHEN a.holiday=1 OR a.Weekend = 1 THEN 0 ELSE 1 END) as WorkingDay
				                                , ((CASE WHEN a.holiday=1 OR a.Weekend = 1 THEN 0 ELSE 1 END)*a.StandardHour) as StandardDayHour
				                                ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
					                                THEN null
					                                ELSE ats.EarlyEntry
				                                END) AS EarlyEntry

					                                ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
					                                THEN null
					                                ELSE ats.LateEntry
				                                END) AS LateEntry

					                                ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
						                                THEN null
						                                ELSE DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, ats.LateEntry), 0), ats.LateEntry) 
					                                END) AS LateEntryMinute

					                                ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
						                                THEN null
						                                ELSE ats.EarlyLeave
					                                END) AS EarlyLeave

					                                ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
						                                THEN null
						                                ELSE DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, ats.EarlyLeave), 0), ats.EarlyLeave) 
					                                END) AS EarlyLeaveMinute

					                                ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
					                                THEN null
					                                ELSE ats.LateLeave
				                                END) AS LateLeave

				                                ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
					                                THEN null
					                                ELSE ats.TotalOvertime
				                                END) AS TotalOvertime

				                                ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND (a.holiday=1 OR a.Weekend=1 OR a.leave=1 )
					                                THEN null
					                                ELSE DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, ats.TotalOvertime), 0), ats.TotalOvertime) 
				                                END) AS OvertimeMinute

				                                ,ats.InTime
				                                ,ats.OutTime
				                                 ,(CASE WHEN ats.AttendanceDate IS NULL AND a.holiday=0 AND a.Weekend=0 AND a.leave=0 AND (do.DayOffStatus != 1 or  do.DayOffStatus IS NULL)
					                                THEN 1
					                                ELSE 0
				                                END) AS absentCount

				                                ,(CASE WHEN ats.AttendanceDate IS NOT NULL AND a.leave=0 AND hw.ApprovalType IS NULL
					                                THEN 1
					                                ELSE 0
				                                END) AS noofWorkingDay	
											
				                                ,((CASE WHEN ats.AttendanceDate IS NOT NULL AND a.leave=0 AND hw.ApprovalType IS NULL
					                                THEN 1
					                                ELSE 0
				                                END)*a.StandardHour) AS WorkingDayHour
			
				                                ,(CASE 
					                                WHEN hw.ApprovalType IS NULL THEN 0
					                                 WHEN hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Half + @" THEN 0.5
					                                WHEN hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Full + @" THEN 1
					                                ELSE 0
				                                END) AS noofExtraDay
				                                ,((CASE 
					                                WHEN hw.ApprovalType IS NULL THEN 0
					                                WHEN hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Half + @" THEN 0.5
					                                WHEN hw.ApprovalType = " + (int)HolidayWorkApprovalStatus.Full + @" THEN 1
					                                ELSE 0
				                                END)*a.StandardHour) AS ExtraDayHour
				                                ,CAST(DATEDIFF(MINUTE,ats.InTime,ats.OutTime) AS DECIMAL(18,2))/ 60  as WorkingdHour
			                                from (
				                                select a.*
					                                --,h.Name as holiday
					                                ,(CASE WHEN h.Name IS NULL
							                                THEN 0
							                                ELSE 1
						                                END) AS holiday

						                                ,(CASE WHEN h.HolidayType = " + (int)HolidayTypeEnum.Management + @"
							                                THEN 1
							                                ELSE 0
						                                END) AS ManagementHoliday
						                                ,(CASE WHEN h.HolidayType =  " + (int)HolidayTypeEnum.Gazetted + @"
							                                THEN 1
							                                ELSE 0
						                                END) AS GazettedHoliday
						                                ,(CASE WHEN w.Weekend IS NULL THEN 0 ELSE 
								                                CASE WHEN w.Weekend=datepart(dw,a.Date) THEN 1 ELSE 0 END
							                                END) AS Weekend
						                                ,(CASE WHEN l.LeaveStatus IS NULL
							                                THEN 0
							                                ELSE 1
						                                END) AS leave
						                                ,w.StandardHour as StandardHour
			
				                                from (
					                                Select * from  @tempdate,#FilterTeamMemberTemp
				                                ) as a
				                                Left JOIN [dbo].[HR_HolidaySetting] as h ON a.OrganizationId = h.OrganizationId AND a.Date>=h.DateFrom AND a.Date<=h.DateTo AND h.Status = 1
				                                LEFT JOIN (
					                                Select a.*, DATEDIFF(MINUTE,s.StartTime,s.EndTime)/60 as StandardHour from(
						                                select * from (
							                                Select [TeamMemberId], Date
											                                ,EffectiveDate
											                                ,[Weekend]
											                                ,ShiftId
											                                ,RANK() OVER(PARTITION BY TeamMemberId,date ORDER BY EffectiveDate DESC) AS R   
							                                from (
									                                Select [TeamMemberId]
											                                ,Date
											                                ,EffectiveDate
											                                ,[Weekend]
											                                ,b.ShiftId
										                                from @tempdate as a,[dbo].[HR_ShiftWeekendHistory] as b 			
										                                Where [Status] = 1 AND [TeamMemberId] = @TeamMemberId
										                                AND a.Date>=EffectiveDate
								                                ) as a 
							                                ) as a WHERE a.R = 1
						                                ) as a
						                                Left JOIN [dbo].[HR_Shift] as s ON s.Id = a.ShiftId
				                                ) AS w on w.TeamMemberId = a.TeamMemberId and w.Date = a.Date
				                                LEFT JOIN (
					                                Select [TeamMemberId],[DateFrom],[DateTo],[LeaveStatus] from [dbo].[HR_LeaveApplication] Where [LeaveStatus] = 2 AND [TeamMemberId] = @TeamMemberId
				                                ) as l  ON l.[TeamMemberId] = a.TeamMemberId AND a.Date>=l.DateFrom AND a.Date<=l.DateTo
			                                ) as a
			                                left join (
				                                Select * from [dbo].[HR_AttendanceSummary] 
					                                Where AttendanceDate>=@StartDate and AttendanceDate<=@EndDate AND [TeamMemberId] = @TeamMemberId
			                                ) as ats ON ats.TeamMemberId  = a.TeamMemberId AND a.Date = ats.AttendanceDate
			                                left join (
				                                Select [ApprovedTime],[TeamMemberId],[OverTimeDate] from [dbo].[HR_Overtime]
					                                Where OverTimeDate>=@StartDate and OverTimeDate<=@EndDate AND [TeamMemberId] = @TeamMemberId
			                                ) as ot ON ot.TeamMemberId  = a.TeamMemberId AND a.Date = ot.OverTimeDate  
			                                left join (
				                                Select [ApprovalType],[TeamMemberId],[HolidayWorkDate] from [dbo].[HR_HolidayWork]
					                                Where [HolidayWorkDate]>=@StartDate and [HolidayWorkDate]<=@EndDate AND [Status] =  " + HolidayWork.EntityStatus.Active + @" AND [TeamMemberId] = @TeamMemberId
			                                ) as hw ON hw.TeamMemberId  = a.TeamMemberId AND a.Date = hw.HolidayWorkDate

			                                left join (
				                                Select [DayOffStatus],[TeamMemberId],[DayOffDate],[InsteadOfDate] from [dbo].[HR_DayOffAdjustment]
					                                Where [DayOffDate]>=@StartDate and [DayOffDate]<=@EndDate AND [DayOffStatus] =  " + (int)DayOffAdjustmentStatus.Approved + @" AND [TeamMemberId] = @TeamMemberId
			                                ) as do ON do.TeamMemberId  = a.TeamMemberId AND a.Date = do.DayOffDate
			                                -- order by a.Date ASC
		                                ) as a group by a.TeamMemberId, a.Designation,DATEADD(MONTH, DATEDIFF(MONTH, 0, a.Date), 0)
	                                ) as a 
                                ) as a 
                                LEFT JOIN [dbo].[TM_MamberAchievement] as b ON a.TeamMemberId = b.TeamMemberId AND a.YearColumn = b.Year AND a.MonthColumn = b.Month
                                 Where a.MyWorkingDay > 0
                                ORDER BY a.YearMonth DESC 

                                IF EXISTS(SELECT * FROM #FilterTeamMemberTemp) DROP TABLE #FilterTeamMemberTemp;";


            IQuery iQuery = Session.CreateSQLQuery(query);
            iQuery.SetResultTransformer(Transformers.AliasToBean<MyAchievementReport>());
            iQuery.SetTimeout(5000);
            return iQuery.List<MyAchievementReport>();
        }

        #endregion

        #region Others Function

        public int GetMemberAchievementCount(string orderBy, string orderDir, int year, int month, List<int> memberPinList = null, List<long> authoMemberIdList = null)
        {
            ICriteria criteria = GetMemberAchievementCriteria(orderBy, orderDir, year, month, memberPinList, authoMemberIdList, true);
            criteria.SetProjection(Projections.RowCount());
            return Convert.ToInt32(criteria.UniqueResult());
        }

        #endregion

        #region Helper Function

        private ICriteria GetMemberAchievementCriteria(string orderBy, string orderDir, int year, int month, List<int> memberPinList = null, List<long> authoMemberIdList = null, bool countQuery = false)
        {
            ICriteria criteria = Session.CreateCriteria<MemberAchievement>().Add(Restrictions.Not(Restrictions.Eq("Status", MemberAchievement.EntityStatus.Delete)));
            criteria.CreateAlias("TeamMember", "teamMember");
            //if (teamMemberPin != SelectionType.SelelectAll)
            //{
            //    criteria.Add(Restrictions.Eq("teamMember.Pin", teamMemberPin));
            //}
            if (memberPinList != null && memberPinList.Any() && !memberPinList.Contains(SelectionType.SelelectAll))
            {
                criteria.Add(Restrictions.In("teamMember.Pin", memberPinList));
            }
            if (authoMemberIdList != null && authoMemberIdList.Any())
            {
                criteria.Add(Restrictions.In("teamMember.Id", authoMemberIdList));
            }
            if (year != SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("Year", year));
            }
            if (month != SelectionType.SelelectAll)
            {
                criteria.Add(Restrictions.Eq("Month", (MonthsOfYear)month));
            }
            if (!countQuery)
            {
                if (!String.IsNullOrEmpty(orderBy))
                {
                    if (orderBy == "Year" || orderBy == "Month")
                    {
                        if (orderDir == "ASC")
                        {
                            criteria.AddOrder(Order.Asc("Year"));
                            criteria.AddOrder(Order.Asc("Month"));
                        }
                        else
                        {
                            criteria.AddOrder(Order.Desc("Year"));
                            criteria.AddOrder(Order.Desc("Month"));
                        }
                    }
                    else
                        criteria.AddOrder(orderDir == "ASC" ? Order.Asc(orderBy.Trim()) : Order.Desc(orderBy.Trim()));
                }
            }
            return criteria;
        }

        #endregion
        
    }

}
