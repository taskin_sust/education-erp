﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UdvashERP.App_code;
using UdvashERP.App_Start;
using UdvashERP.BusinessRules;
using UdvashERP.BusinessRules.CustomAttributes;
using UdvashERP.MessageExceptions;
using UdvashERP.BusinessModel.Entity;
using UdvashERP.BusinessModel.Entity.Administration;
using UdvashERP.BusinessModel.Entity.Base;
using UdvashERP.BusinessModel.Entity.Common;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel;
using UdvashERP.App_Start;
using UdvashERP.Services.Administration;
using UdvashERP.Services.Common;
using UdvashERP.Services.Helper;
using UdvashERP.Services.UserAuth;

namespace UdvashERP.Areas.Administration.Controllers
{
    [UerpArea("Administration")]
    [Authorize]
    [AuthorizeAccess]
    public class ActionsController : Controller
    {
        #region Logger
        private ILog logger = LogManager.GetLogger("AdministrationArea");
        #endregion

        #region Objects/Propertise/Services/Dao & Initialization
        private readonly ICommonHelper _commonHelper;
        private readonly IAreaControllersService _areacontrollersService;
        private readonly IActionsService _actionsService;
        public ActionsController()
        {
            try
            {
                var session = NHibernateSessionFactory.OpenSession();
                _actionsService = new ActionsService(session);
                _areacontrollersService = new AreaControllersService(session);
                _commonHelper = new CommonHelper();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                if (ex.InnerException != null)
                {
                    logger.Error("Actions controller Constructor Can't be Initialize  due to " + ex.InnerException);
                }
                else
                {
                    logger.Error("Actions controller Constructor Can't be Initialize  due to " + ex.Message);
                }
                Console.WriteLine("{0}", ex.Message);
            }

        }
        #endregion

        #region Index/Manage
        public ActionResult Index()
        {
            SelectList acList = null;
            try
            {
                ViewBag.STATUSTEXT = _commonHelper.GetStatus();
                acList = new SelectList(_areacontrollersService.LoadActive(), "Id", "Name");
                List<SelectListItem> _list = acList.ToList();
                _list.Insert(0, new SelectListItem() { Value = "", Text = "Select Controllers" });
                ViewBag.ControllersFiter = acList;
                ViewBag.PageSize = Constants.PageSize;
                ViewBag.CurrentPage = 1;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
                Console.WriteLine("{0}", ex.Message);
            }
            ViewBag.PageSize = Constants.PageSize;
            return View();
        }
        #endregion

        #region Save Operation
        public ActionResult Create()
        {
            try
            {
                SelectList acList = new SelectList(_areacontrollersService.LoadActive(), "Id", "Name");
                List<SelectListItem> _list = acList.ToList();
                _list.Insert(0, new SelectListItem() { Value = "", Text = "Select Controllers" });
                ViewBag.acList = new SelectList(_list, "Value", "Text");

            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
                Console.WriteLine("{0}", ex.Message);
            }
            return View();
        }
        [HttpPost]
        public ActionResult Create(ActionsViewModel actionsViewModel)
        {
            try
            {
                SelectList acList = new SelectList(_areacontrollersService.LoadActive(), "Id", "Name");
                List<SelectListItem> _list = acList.ToList();
                _list.Insert(0, new SelectListItem() { Value = "0", Text = "Select Controllers" });
                ViewBag.acList = new SelectList(_list, "Value", "Text");

                if (ModelState.IsValid)
                {
                    Actions actionsObj = new Actions();
                    actionsObj.AreaControllers = null;

                    if (actionsViewModel.ControllersId != 0)
                    {
                        AreaControllers acControllers = _areacontrollersService.LoadById(actionsViewModel.ControllersId);
                        if (acControllers != null)
                            actionsObj.AreaControllers = acControllers;
                    }

                    actionsObj.Name = actionsViewModel.Name;

                    _actionsService.Save(actionsObj);
                    ViewBag.SuccessMessage = "Actions Addded Successfully";
                    ModelState.Clear();
                    return View();
                }

            }
            catch (DuplicateEntryException ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            return View();
        }
        #endregion

        #region Update Operation
        public ActionResult Edit(long id)
        {
            try
            {
                Actions actions = _actionsService.LoadById(id);
                if (actions == null)
                    return HttpNotFound();

                var list = _areacontrollersService.LoadActive();
                ViewBag.acList = new SelectList(list, "Id", "Name", actions.AreaControllers.Id);
                ActionsViewModel actionsViewModel = new ActionsViewModel();
                actionsViewModel.ControllersId = actions.AreaControllers.Id;
                actionsViewModel.Id = actions.Id;
                actionsViewModel.Name = actions.Name;

                //if (actions.AreaControllers != null)
                //{
                //    actionsViewModel.ControllersId = actions.AreaControllers.Id;
                //}
                return View(actionsViewModel);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
                Console.WriteLine("{0}", ex.Message);
                return View();
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(long id, ActionsViewModel actionsViewModel)
        {
            try
            {
                Actions actions = _actionsService.LoadById(id);

                if (actions == null)
                    return HttpNotFound();

                var list = _areacontrollersService.LoadActive();
                ViewBag.acList = new SelectList(list, "Id", "Name", actions.AreaControllers.Id);

                //SelectList acList = new SelectList(_areacontrollersService.LoadActive(), "Id", "Name");
                //List<SelectListItem> _list = acList.ToList();
                //_list.Insert(0, new SelectListItem() {Value = "0", Text = "Select Controllers"});
                //ViewBag.acList = new SelectList(_list, "Value", "Text");


                actions.Name = actionsViewModel.Name;
                actions.AreaControllers = null;

                if (actionsViewModel.ControllersId != 0)
                {
                    AreaControllers ac = _areacontrollersService.LoadById(actionsViewModel.ControllersId);
                    if (ac != null)
                    {
                        actions.AreaControllers = ac;
                    }
                }
                var success=_actionsService.Update(id, actions);
                ViewBag.SuccessMessage = "Actions update Successfully";
                return View(actionsViewModel);
            }
            catch (DuplicateEntryException ex)
            {
                logger.Error(ex);
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
                Console.WriteLine("{0}", ex.Message);
                return View();
            }
        }
        #endregion

        #region Delete Operation
        public ActionResult Delete(long id)
        {
            try
            {
                Actions actions = _actionsService.LoadById(id);
                if (actions == null)
                    return HttpNotFound();
                return View(actions);

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = WebHelper.CommonErrorMessage;
                logger.Error(ex);
                return View();
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Actions actions)
        {
            Actions deleteActions = _actionsService.LoadById(actions.Id);
            var success=_actionsService.Delete(deleteActions.Id, deleteActions);
            return RedirectToAction("Index");
        }
        #endregion

        #region Details Operation
        public ActionResult Details(long id)
        {
            var _actionsObj = _actionsService.LoadById(id);
            return View(_actionsObj);
        }
        #endregion

        #region Render Data Table
        public JsonResult ActionsList(int draw, int start, int length, string name, string controllersId, string status, string rank)
        {
            NameValueCollection nvc = Request.Form;

            string orderBy = "";
            string orderDir = "";
            var getData = new List<object>();
            try
            {
                if (!string.IsNullOrEmpty(nvc["order[0][column]"]))
                {
                    orderBy = nvc["order[0][column]"];
                    orderDir = nvc["order[0][dir]"];
                    switch (orderBy)
                    {
                        case "0":
                            orderBy = "Name";
                            break;
                        case "1":
                            orderBy = "Rank";
                            break;
                        case "2":
                            orderBy = "Status";
                            break;
                        default:
                            orderBy = "";
                            break;
                    }
                }
                int recordsTotal = _actionsService.ActionsRowCount(name, controllersId, status, rank);
                long recordsFiltered = recordsTotal;

                var _actionsList = _actionsService.LoadActive(start, length, orderBy, orderDir.ToUpper(), name, controllersId, status, rank).ToList();

                Actions obj = null;
                int[] maxMinRank = new int[2];
                maxMinRank[0] = 1;
                maxMinRank[1] = _actionsService.GetMaximumRank(obj);

                foreach (var actions in _actionsList)
                {
                    var str = new List<string>();
                    str.Add(actions.Name);
                    str.Add(actions.AreaControllers.Name);

                    string rankbtn = actions.Rank.ToString();

                    if (actions.Rank > maxMinRank[0])
                    {
                        rankbtn += "<a href='#'  id='" + actions.Id + "' class='glyphicon glyphicon-arrow-up' data-action='up'></a>";
                    }
                    if (actions.Rank < maxMinRank[1])
                    {
                        rankbtn += "<a href='#'  id='" + actions.Id + "' class='glyphicon glyphicon-arrow-down' data-action='down'></a>";
                    }

                    str.Add(rankbtn);
                    str.Add(StatusTypeText.GetStatusText(actions.Status));

                    str.Add("<a href='" + Url.Action("Details", "Actions") + "?Id=" + actions.Id + "' data-id='" + actions.Id + "' class='glyphicon glyphicon-th-list'></a>&nbsp;&nbsp; " +
                        "<a href='" + Url.Action("Edit", "Actions") + "?Id=" + actions.Id + "' data-id='" + actions.Id + "' class='glyphicon glyphicon-pencil'> </a>&nbsp;&nbsp; " +
                            "<a href='" + Url.Action("Delete", "Actions") + "?Id=" + actions.Id + "' data-id='" + actions.Id + "' class='glyphicon glyphicon-trash'></a>"
                            );
                    getData.Add(str);
                }

                return Json(new
                {
                    draw = draw,
                    recordsTotal = recordsTotal,
                    recordsFiltered = recordsFiltered,
                    start = start,
                    length = length,
                    data = getData

                });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return Json(new
            {
                draw = draw,
                recordsTotal = 0,
                recordsFiltered = 0,
                start = start,
                length = length,
                data = getData,
                isSuccess = false
            });


        }
        #endregion

        #region Helper Function

        private void GetStatusText()
        {
            ViewBag.STATUSTEXT = _commonHelper.GetStatus();
        }

        #endregion
    }
}