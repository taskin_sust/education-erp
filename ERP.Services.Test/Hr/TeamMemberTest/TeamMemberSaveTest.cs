﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.TeamMemberTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "TeamMember")]
    public class TeamMemberSaveTest : TeamMemberBaseTest
    {
        [Fact]
        public void Should_Return_Null_Exception()
        {
            DateTime startDate = DateTime.Now.AddYears(-3);
            DateTime endDate = DateTime.Now.AddYears(-1);
            Assert.Throws<NullObjectException>(() => teamMemberService.SaveNew(null, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")));
        }
        [Fact]
        public void Should_Return_Check_StartDate_Property()
        {
            DateTime startDate = DateTime.Now.AddYears(-3);
            DateTime endDate = DateTime.Now.AddYears(-1);
            teamMemberFactory.Create()
                .WithEmploymentHistory(employmentHistoryFactory.ObjectList)
                .WithShiftWeekend(shiftWeekendHistoryFactory.ObjectList)
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(mentorHistoryFactory.ObjectList);
            Assert.Throws<DataNotFoundException>(() => teamMemberService.SaveNew(teamMemberFactory.Object, "", endDate.ToString("yyyy-MM-dd")));
        }

        [Fact]
        public void Should_Return_Check_EndDate_Property()
        {
            DateTime startDate = DateTime.Now.AddYears(-3);
            DateTime endDate = DateTime.Now.AddYears(-1);
            teamMemberFactory.Create()
                .WithEmploymentHistory(employmentHistoryFactory.ObjectList)
                .WithShiftWeekend(shiftWeekendHistoryFactory.ObjectList)
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(mentorHistoryFactory.ObjectList);
            Assert.Throws<DataNotFoundException>(() => teamMemberService.SaveNew(teamMemberFactory.Object, startDate.ToString("yyyy-MM-dd"), ""));
        }

        [Fact]
        public void Should_Return_Check_Duplicate()
        {
            DateTime startDate = DateTime.Now.AddYears(-3);
            DateTime endDate = DateTime.Now.AddYears(-1);

            //TODO:Initially create team member without mentor for testing. Mentor as a team member  
            var mentorObj = teamMemberService.GetMember(1);

            var mentorEmployment = mentorObj.EmploymentHistory.OrderByDescending(x => x.EffectiveDate).FirstOrDefault();

            organizationFactory.Create().Persist();
            employmentHistoryFactory.CreateMore(1, organizationFactory.Object);
            maritalInfoFactory.CreateMore(1);
            shiftWeekendHistoryFactory.CreateMore(1, organizationFactory.Object);

            var dept = employmentHistoryFactory.ObjectList.Select(x => x.Department).FirstOrDefault();
            var designation = employmentHistoryFactory.ObjectList.Select(x => x.Designation).FirstOrDefault();
            mentorHistoryFactory.CreateMore(1, mentorObj, dept, designation, organizationFactory.Object
                , mentorEmployment.Department, mentorEmployment.Designation, mentorEmployment.Department.Organization);

            var tmFactory1 = teamMemberFactory.Create()
                .WithEmploymentHistory(employmentHistoryFactory.ObjectList)
                .WithShiftWeekend(shiftWeekendHistoryFactory.ObjectList)
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(mentorHistoryFactory.ObjectList)
                .Persist();

            var tmFactory2 = teamMemberFactory.CreateWith(
                teamMemberFactory.Object.Id,
                teamMemberFactory.GetNewPin() + 1
                , tmFactory1.Object.PersonalContact
                , "MIKE TYSON"
                , "টাইসন"
                , "https://www.facebook.com/"
                , "tyson_usa"
                ,"tyson@onnorokom.com"
                , (int)Gender.Male
                , (int)Religion.Islam
                , (int)BloodGroup.Bpos
                , (int)Nationality.Bangladeshi
                ,"11235813213455"
                , "112358"
                , "c1498982"
                , DateTime.Now
                , DateTime.Now
                , "H-22,R-3 Dhaka"
                , "H-22,R-09,Dhaka"
                ,"ONNOROKOM GROUP"
                , "EMPLOYEE"
                , "01811419555"
                , "সিনিওর টাইসন"
                , "SR.TYSON"
                , "সুইফট"
                , "SWIFT"
                , "01811111111"
                , "NIL"
                ,"NIL"
                ,"")
                .WithEmploymentHistory(employmentHistoryFactory.ObjectList)
                .WithShiftWeekend(shiftWeekendHistoryFactory.ObjectList)
                .WithEffectiveStartAndEndDate(startDate, endDate)
                .WithMaritalInfo(maritalInfoFactory.ObjectList)
                .WithMentorHistory(mentorHistoryFactory.ObjectList);
            Assert.Throws<DuplicateEntryException>(() => teamMemberService.SaveNew(tmFactory2.Object, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")));

        }
        [Fact]
        public void Should_Save_Successfully()
        {
            var teamMember = PersistTeamMember();
            Assert.NotNull(teamMember.Id);
            Assert.NotEqual(teamMember.Id, 0);
        }
    }
}
