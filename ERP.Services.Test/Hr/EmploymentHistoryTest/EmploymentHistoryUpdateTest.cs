﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessRules;
using UdvashERP.MessageExceptions;
using Xunit;

namespace UdvashERP.Services.Test.Hr.EmploymentHistoryTest
{
    [Trait("Area", "Hr")]
    [Trait("Service", "EmploymentHistory")]
    public class EmploymentHistoryUpdateTest : EmploymentHistoryBaseTest
    {
        #region Basic Test

        [Fact]
        public void EmploymentHistory_UpdateEmploymentHistory_Should_Pass_Successfully()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryService.AddEmploymentHistory(teamMember, designation.Organization.Id, campus.Branch.Id
                                                            , campus.Id, dept.Id, designation.Id
                                                            , _employmentHistoryFactory.Object.EmploymentStatus
                                                            , _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture));

            _employmentHistoryService.UpdateEmploymentHistory(_employmentHistoryFactory.Object, campus.Id, dept.Id,
                                                                designation.Id, (int)MemberEmploymentStatus.Permanent,
                                                                _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture));
            Assert.True(true);
        }

        #endregion

        #region Business Logic

        [Fact]
        public void InvalidDataException_UpdateEmploymentHistory_For_Duplicate_Date()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryService.AddEmploymentHistory(teamMember, designation.Organization.Id, campus.Branch.Id
                                                            , campus.Id, dept.Id, designation.Id
                                                            , _employmentHistoryFactory.Object.EmploymentStatus
                                                            , _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture));

            Assert.Throws<InvalidDataException>(() => _employmentHistoryService.UpdateEmploymentHistory(_employmentHistoryFactory.Object, campus.Id, dept.Id,
                                                                designation.Id, (int)MemberEmploymentStatus.Permanent,
                                                                _employmentHistoryFactory.Object.EffectiveDate.ToString(CultureInfo.InvariantCulture)));
        }


        [Fact]
        public void InvalidDataException_UpdateEmploymentHistory_For_Campus_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryService.AddEmploymentHistory(teamMember, designation.Organization.Id, campus.Branch.Id
                                                            , campus.Id, dept.Id, designation.Id
                                                            , _employmentHistoryFactory.Object.EmploymentStatus
                                                            , _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture));

            Assert.Throws<InvalidDataException>(() => _employmentHistoryService.UpdateEmploymentHistory(_employmentHistoryFactory.Object, 0, dept.Id,
                                                                designation.Id, (int)MemberEmploymentStatus.Permanent,
                                                                _employmentHistoryFactory.Object.EffectiveDate.ToString(CultureInfo.InvariantCulture)));
        }

        [Fact]
        public void InvalidDataException_UpdateEmploymentHistory_For_Department_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryService.AddEmploymentHistory(teamMember, designation.Organization.Id, campus.Branch.Id
                                                            , campus.Id, dept.Id, designation.Id
                                                            , _employmentHistoryFactory.Object.EmploymentStatus
                                                            , _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture));

            Assert.Throws<InvalidDataException>(() => _employmentHistoryService.UpdateEmploymentHistory(_employmentHistoryFactory.Object, campus.Id, 0,
                                                                designation.Id, (int)MemberEmploymentStatus.Permanent,
                                                                _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture)));
        }


        [Fact]
        public void InvalidDataException_UpdateEmploymentHistory_For_Designation_Null()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryService.AddEmploymentHistory(teamMember, designation.Organization.Id, campus.Branch.Id
                                                            , campus.Id, dept.Id, designation.Id
                                                            , _employmentHistoryFactory.Object.EmploymentStatus
                                                            , _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture));

            Assert.Throws<InvalidDataException>(() => _employmentHistoryService.UpdateEmploymentHistory(_employmentHistoryFactory.Object, campus.Id, dept.Id,
                                                                0, (int)MemberEmploymentStatus.Permanent,
                                                                _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture)));
        }

        [Fact]
        public void InvalidDataException_UpdateEmploymentHistory_For_Invalid_EmploymentStatus()
        {
            var teamMember = PersistTeamMember(MentorId);
            var designation = teamMember.EmploymentHistory[0].Designation;
            var dept = teamMember.EmploymentHistory[0].Department;
            var campus = teamMember.EmploymentHistory[0].Campus;
            _employmentHistoryFactory.Create().WithCampus(campus)
                .WithDepartment(dept).WithDesignation(designation).WithTeamMember(teamMemberFactory.Object);
            _employmentHistoryService.AddEmploymentHistory(teamMember, designation.Organization.Id, campus.Branch.Id
                                                            , campus.Id, dept.Id, designation.Id
                                                            , _employmentHistoryFactory.Object.EmploymentStatus
                                                            , _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture));

            Assert.Throws<InvalidDataException>(() => _employmentHistoryService.UpdateEmploymentHistory(_employmentHistoryFactory.Object, campus.Id, dept.Id,
                                                                designation.Id, 0,
                                                                _employmentHistoryFactory.Object.EffectiveDate.AddDays(1).ToString(CultureInfo.InvariantCulture)));
        }

        #endregion
    }
}
