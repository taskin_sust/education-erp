﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UdvashERP.Areas.Student.Models
{
    public class ViewBoardRollViewModel
    {
        [Required(ErrorMessage = "You need to select organization")]
        public long OrganizationId { get; set; }

        [Required(ErrorMessage = "You need to select program")]
        public long ProgramId { get; set; }

        [Required(ErrorMessage = "You need to select session")]
        public long SessionId { get; set; }

        public string[] InformationViewList { get; set; }

        [Required(ErrorMessage = "You need to select branch")]
        public long[] SelectedBranch { get; set; }

        [Required(ErrorMessage = "You need to select campus")]
        public long[] SelectedCampus { get; set; }

        [Required(ErrorMessage = "You need to select batch day")]
        public string[] SelectedBatchDays { get; set; }

        [Required(ErrorMessage = "You need to select batch time")]
        public string[] SelectedBatchTime { get; set; }

        [Required(ErrorMessage = "You need to select batch")]
        public long[] SelectedBatch { get; set; }
        public int SelectedStatus { get; set; }  
    }
}