﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using NHibernate;
using UdvashERP.BusinessModel.Entity.Hr;
using UdvashERP.BusinessModel.Entity.UserAuth;
using UdvashERP.BusinessModel.ViewModel.Hr;
using UdvashERP.Services.Helper;
using UdvashERP.Services.Hr;
using UdvashERP.Services.MediaServices;

namespace UdvashERP.Api.Controllers
{
    public class AttendanceDeviceTypeApiController : Controller
    {
        #region Logger

        private readonly ILog _logger = LogManager.GetLogger("HrArea");

        #endregion

        #region Objects/Properties/Services/Dao & Initialization

        private List<UserMenu> authorizeMenu;
        private ISession _session;
        private ISession _sessionMedia;

        private readonly IAttendanceDeviceTypeService _attendanceDeviceTypeService;
        private readonly ITeamMemberService _teamMemberService;
        private readonly IMemberFingerPrintService _memberFingerPrintService;
        private readonly ITeamMemberImagesMediaService _teamMemberImagesMediaService;
        #endregion

        #region Constructor

        public AttendanceDeviceTypeApiController()
        {
            _session = NHibernateSessionFactory.OpenSession();
            _sessionMedia = NHibernateMediaSessionFactory.OpenSession();
            _attendanceDeviceTypeService = new AttendanceDeviceTypeService(_session);
            _teamMemberService = new TeamMemberService(_session);
            _memberFingerPrintService = new MemberFingerPrintService(_session);
            _teamMemberImagesMediaService = new TeamMemberImagesMediaService(_sessionMedia);
        }

        #endregion

        #region Index


        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetAttendanceDeviceTypeInfo()
        {
            dynamic result = null;
            try
            {
                IList<AttendanceDeviceType> list = _attendanceDeviceTypeService.LoadAttendanceDeviceType();
                result = list.Select(x =>
                   new { Id = x.Id, ModelName = x.Name }).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Operational Function

        #region Save

        #endregion

        #region Edit

        #endregion

        #region Delete

        #endregion

        #region Details

        #endregion

        #endregion

        #region Helper


        #endregion
    }
}